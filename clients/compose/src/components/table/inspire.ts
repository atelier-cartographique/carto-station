/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import tr, { fromRecord, formatDate } from 'sdi/locale';
import { DIV, SPAN, A, NODISPLAY } from 'sdi/components/elements';
import {
    Inspire,
    FreeText,
    isAnchor,
    ResponsibleOrganisation,
    isTemporalExtent,
    TemporalReference,
    MdPointOfContact,
} from 'sdi/source';
import { scopeOption } from 'sdi/lib';

import {
    getSelectedMetadataRow,
    getDatasetMetadata,
    getPersonOfContact,
    getResponsibleOrg,
} from '../../queries/metadata';
import { addMapLayer, resetLegendEditor, setLayout } from '../../events/app';
import { AppLayout } from '../../shape/types';
import { makeIcon, makeLabel } from '../button';
import { fromNullable } from 'fp-ts/lib/Option';
import { icon } from 'sdi/components/button';

const logger = debug('sdi:table/inspire');

const okButton = makeLabel('add', 1, () => tr.compose('addToLegend'));
const closeButton = makeIcon('close', 3, 'times', {
    text: () => tr.compose('close'),
    position: 'bottom-left',
});

const renderFreeText = (ft: FreeText, className?: string) => {
    if (isAnchor(ft)) {
        return A({ href: ft.href, className }, fromRecord(ft.text));
    }

    return SPAN({ className }, fromRecord(ft));
};

const renderTemporalReference = (t: TemporalReference) => {
    if (isTemporalExtent(t)) {
        return SPAN(
            {},
            tr.compose('extentBegin'),
            SPAN({}, formatDate(new Date(t.begin))),
            tr.compose('extentEnd'),
            SPAN({}, formatDate(new Date(t.end)))
        );
    }
    return (
        SPAN({}, tr.compose('lastModified')), formatDate(new Date(t.revision))
    );
};

const renderContact = (id: number) =>
    getPersonOfContact(id).fold(
        NODISPLAY({ key: `inspire-renderContact-${id}` }),
        (ro: MdPointOfContact) =>
            DIV(
                { className: 'ro-block', key: `inspire-renderContact-${id}` },
                renderFreeText(ro.organisation.name, 'ro-org-name'),
                SPAN('ro-contact-name', ro.contactName)
            )
    );

const renderOrg = (id: number) =>
    getResponsibleOrg(id).fold(
        NODISPLAY({ key: `inspire-renderOrg-${id}` }),
        (ro: ResponsibleOrganisation) =>
            DIV(
                { className: 'ro-block', key: `inspire-renderOrg-${id}` },
                renderFreeText(ro.organisation.name, 'ro-org-name'),
                SPAN('ro-contact-name', ro.organisation.contactName),
                SPAN('ro-role-code', ro.roleCode)
            )
    );

const renderInspireMD = (i: Inspire) =>
    DIV(
        'app-split-wrapper inspire-wrapper',
        DIV(
            'app-split-header',
            DIV(
                'app-split-title inspire-title',
                SPAN('', icon('table'), renderFreeText(i.resourceTitle))
            ),
            DIV(
                'app-split-tools',
                okButton(() => {
                    addMapLayer(i);
                    resetLegendEditor();
                    setLayout(AppLayout.LegendEditor);
                }),
                closeButton(() => setLayout(AppLayout.LayerSelect))
            )
        ),
        DIV(
            'inspire-selected-items',
            DIV(
                {},
                i.topicCategory.map(a => SPAN({}, a))
            ),
            DIV({}, i.geometryType),
            DIV(
                {},
                SPAN({}, tr.compose('lastModified')),
                renderTemporalReference(i.temporalReference)
            ),
            DIV('abstract', renderFreeText(i.resourceAbstract)),
            DIV(
                {},
                SPAN('label', tr.compose('responsibleAndContact')),
                i.metadataPointOfContact.map(renderContact),
                i.responsibleOrganisation.map(renderOrg)
            )
        )
    );

const render = () =>
    scopeOption()
        .let('row', fromNullable(getSelectedMetadataRow()))
        .let('md', ({ row }) => getDatasetMetadata(row.from as string))
        .fold(DIV('inspire-wrapper empty'), ({ md }) => renderInspireMD(md));

export default render;

logger('loaded');
