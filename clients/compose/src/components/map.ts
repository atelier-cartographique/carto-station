/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { ReactNode } from 'react';

import { DIV } from 'sdi/components/elements';
import { mapRightToolsSimplified, renderScaleline } from 'sdi/map/controls';

import { IMapOptions, create, zoomToScale } from 'sdi/map';
import {
    getCurrentBaseLayers,
    getMapInfo,
    getMapLayerInfo,
} from '../queries/app';
import { signalReadyMap } from '../events/app';
import { getScaleLine, getView, getZoom } from '../queries/map';
import { updateMapView, setScaleLine } from '../events/map';
import tr, { formatNumber } from 'sdi/locale';

// import baseLayerSwitch from './map-info/base-layer-switch';
import { dispatchK } from 'sdi/shape';

const logger = debug('sdi:comp:map');
export const mapName = 'main-view';

const options: IMapOptions = {
    getMapLayerInfo,
    getView,
    getMapInfo,
    setScaleLine,
    element: null,
    getBaseLayer: getCurrentBaseLayers,
    updateView: updateMapView,
};

let mapSetTarget: (t: HTMLElement | null) => void;
let mapUpdate: () => void;

const attachMap = (element: HTMLElement | null) => {
    if (!mapUpdate) {
        const { update, setTarget } = create(mapName, {
            ...options,
            element,
        });
        mapSetTarget = setTarget;
        mapUpdate = update;
        signalReadyMap();
    }
    if (element) {
        logger('mapSetTarget');
        mapSetTarget(element);
    }
};

const renderZoomLevel = () => {
    const z = Math.round(getZoom() * 100) / 100;
    return DIV(
        'map__zoom-level',
        `${tr.compose('scale')}: 1/${formatNumber(zoomToScale(z))} (zoom: ${z})`
    );
};

const render = () => {
    if (mapUpdate) {
        mapUpdate();
    }
    const overlays: ReactNode[] = [];
    overlays.push(
        DIV(
            'scale-and-zoom',
            renderScaleline(getScaleLine()),
            renderZoomLevel()
        )
    );

    return DIV(
        { className: 'map-wrapper', key: 'the-map-wrapper' },
        DIV({
            id: mapName,
            className: 'map',
            ref: attachMap,
        }),
        // baseLayerSwitch(),
        mapRightToolsSimplified(dispatchK('port/map/view')),
        ...overlays
    );
};

export default render;

logger('loaded');
