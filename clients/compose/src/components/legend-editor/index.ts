/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { DIV, LABEL, NODISPLAY, SPAN } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import {
    FeatureCollection,
    ILayerInfo,
    makeRecord,
    MessageRecord,
} from 'sdi/source';

import {
    getLegendType,
    getSelectedMainName,
    getStyle,
} from '../../queries/legend-editor';
import {
    getLayout,
    getSynteticLayerInfoOption,
    getCurrentSynteticLayerInfoOption,
    getLayerData,
} from '../../queries/app';
import { setLayout } from '../../events/app';
import { ensureSelectedFeature } from '../../events/feature-config';
import selectMain from './select-main';
import selectType from './select-type';
import { renderStyleSettings, renderLabelSettings } from './tools-main';
import continuous from './select-item-continuous';
import discrete from './select-item-discrete';
import { AppLayout } from '../../shape/types';
import { makeIcon, makeLabelAndIcon } from '../button';
import {
    getLayerPropertiesKeys,
    getLayerPropertiesKeysForNumbers,
    Nullable,
} from 'sdi/util';
import { moveGroupDown, moveGroupUp } from '../../events/legend-editor';
import { Getter, inputColor, renderSelect, Setter } from 'sdi/components/input';
import { setoidString } from 'fp-ts/lib/Setoid';
import { none, some } from 'fp-ts/lib/Option';
import { getAlias, getLang } from 'sdi/app';

import { setLegendLabel } from '../../events/legend-editor';
import { wrapTranslatableInput } from '../translatable';
import { attrOptions, inputText } from 'sdi/components/input';
import { icon } from 'sdi/components/button';

const logger = debug('sdi:legend-editor');

type TextGetter = () => string;
type TextSetter = (a: string) => void;

export type LegendPage = 'legend' | 'tools';

export type PointConfigSelected = 'label' | 'marker';

export interface ILegend {
    currentPage: LegendPage;
}

export interface ILegendEditor {
    mainSelected: number;
    itemSelected: number;
    styleGroupSelected: number;
    styleGroupEditedValue: Nullable<number | string>;
    pointConfig: PointConfigSelected;
    autoClassValue: number;
    autoClassNbDecimals: number;
}

export const initialLegendEditorState = (): ILegendEditor => ({
    mainSelected: -1,
    itemSelected: -1,
    styleGroupSelected: -1,
    pointConfig: 'marker',
    styleGroupEditedValue: null,
    autoClassValue: 2,
    autoClassNbDecimals: 2,
});

export const editRecord = (
    get: Getter<MessageRecord>,
    set: Setter<MessageRecord>,
    text: string
) => {
    const lc = getLang();
    const rec = { ...get() };

    rec[lc] = text;
    set(rec);
};

const closeButton = makeIcon('close', 3, 'times', {
    position: 'bottom-left',
    text: () => tr.core('close'),
});
const switchTableButton = makeLabelAndIcon(
    'layerInfoSwitchTable',
    3,
    'table',
    () => tr.compose('displayTableView')
);
const switchMapButton = makeLabelAndIcon('layerInfoSwitchMap', 3, 'map', () =>
    tr.compose('displayMapView')
);

const moveUpBtn = makeIcon('move-up', 3, 'arrow-up', {
    position: 'left',
    text: () => tr.core('move-up'),
});
const moveDownBtn = makeIcon('move-down', 3, 'arrow-down', {
    position: 'left',
    text: () => tr.core('move-down'),
});

export const renderUpAndDownBtns = (id: number, length: number) => {
    if (length === 1) {
        return NODISPLAY();
    } else if (id === 0) {
        return moveDownBtn(() => moveGroupDown(id));
    } else if (id === length - 1) {
        return moveUpBtn(() => moveGroupUp(id));
    } else {
        return SPAN(
            'actions',
            moveUpBtn(() => moveGroupUp(id)),
            moveDownBtn(() => moveGroupDown(id))
        );
    }
};

const renderCloseButton = () =>
    closeButton(() => setLayout(AppLayout.MapAndInfo));

const renderSwitchTableButton = () =>
    switchTableButton(() => {
        setLayout(AppLayout.LegendEditorAndTable);
    }, 'table');

const renderSwitchMapButton = () =>
    switchMapButton(() => {
        setLayout(AppLayout.LegendEditor);
    }, 'map');

const renderMapTableSwitch = () => {
    const l = getLayout();
    if (AppLayout.LegendEditor === l) {
        return renderSwitchTableButton();
    }
    return renderSwitchMapButton();
};

const layerTableSwitch = (lid: string) =>
    getSynteticLayerInfoOption(lid).map(() => renderMapTableSwitch());

const layerName = (lid: string) =>
    getSynteticLayerInfoOption(lid).map(({ name }) =>
        DIV('layer-name', fromRecord(name))
    );

const renderHeader = (_lid: string) => {
    return DIV(
        'app-split-header',
        DIV('app-split-title', icon('table'), SPAN('', layerName(_lid))),
        DIV('app-split-tools', layerTableSwitch(_lid), renderCloseButton())
    );
};

export const renderInputColor = (
    get: Getter<string>,
    set: Setter<string>,
    label: string
) => DIV('style-tool color', LABEL('input-label', label, inputColor(get, set)));

const selectProp = (set: TextSetter) =>
    renderSelect<string>(
        'select-prop-for-label',
        getAlias,
        set,
        setoidString,
        tr.compose('columnName')
    );

const getPropList = (layerData: FeatureCollection) =>
    getLayerPropertiesKeys(layerData).concat('-').reverse();

const getFilteredPropList = (layerData: FeatureCollection) => {
    const legendType = getLegendType();
    if (legendType === 'continuous') {
        return getLayerPropertiesKeysForNumbers(layerData)
            .concat('-')
            .reverse();
    }
    return getPropList(layerData);
};

export const renderPropSelect = (set: TextSetter, get: TextGetter) =>
    getCurrentSynteticLayerInfoOption()
        .chain(({ metadata }) =>
            getLayerData(metadata.uniqueResourceIdentifier).getOrElse(none)
        )
        .fold(NODISPLAY(), layerData => {
            return selectProp(set)(getPropList(layerData), some(get()));
        });

export const renderFilteredPropSelect = (set: TextSetter, get: TextGetter) =>
    getCurrentSynteticLayerInfoOption()
        .chain(({ metadata }) =>
            getLayerData(metadata.uniqueResourceIdentifier).getOrElse(none)
        )
        .fold(NODISPLAY(), layerData => {
            return selectProp(set)(getFilteredPropList(layerData), some(get()));
        });

const getLabel = (info: ILayerInfo) =>
    info.legend !== null ? info.legend : makeRecord();
const setLabel = (info: ILayerInfo) => (r: MessageRecord) =>
    setLegendLabel(info.id, r);

export const inputLabel = () =>
    getCurrentSynteticLayerInfoOption().map(({ info, name }) => {
        const key = `layer_legend_label_${info.id}_${getLang()}`;
        return wrapTranslatableInput(
            getLabel(info),
            key,
            inputText(
                attrOptions(
                    key,
                    () => fromRecord(getLabel(info)),
                    l => editRecord(() => getLabel(info), setLabel(info), l),
                    {
                        className: key,
                        placeholder: fromRecord(name),
                    }
                )
            )
        );
    });

const column = (
    className: string,
    title: string,
    ...children: React.ReactNode[]
): React.ReactNode =>
    DIV(
        `column ${className}`,
        DIV('column__title', DIV('title', title)),
        ...children
    );

const renderBody = () => {
    const style = getStyle();
    const legendType = getLegendType();

    if (style !== null) {
        if (legendType === 'simple') {
            return DIV(
                'app-split-main',
                column('picker', tr.compose('legendTypeSelect'), selectType()),
                column('settings', tr.compose('style'), renderStyleSettings()),
                column(
                    'label',
                    tr.compose('legendLabelHeader'),
                    renderLabelSettings()
                )
            );
        } else {
            const propName = getSelectedMainName();
            if (propName === '') {
                return DIV(
                    'app-split-main',
                    column(
                        'picker',
                        tr.compose('legendTypeSelect'),
                        selectType()
                    ),
                    column(
                        'settings',
                        tr.compose('columnPicker'),
                        selectMain()
                    ),
                    column(
                        'label',
                        tr.compose('legendLabelHeader'),
                        renderLabelSettings()
                    )
                );
            } else {
                if (legendType === 'discrete') {
                    return DIV(
                        'app-split-main',
                        column('picker', `${getAlias(propName)}`, discrete()),
                        column(
                            'settings',
                            tr.compose('style'),
                            renderStyleSettings()
                        ),
                        column(
                            'label',
                            tr.compose('legendLabelHeader'),
                            renderLabelSettings()
                        )
                    );
                } else {
                    return DIV(
                        'app-split-main',
                        column('picker', `${getAlias(propName)}`, continuous()),
                        column(
                            'settings',
                            tr.compose('style'),
                            renderStyleSettings()
                        ),
                        column(
                            'label',
                            tr.compose('legendLabelHeader'),
                            renderLabelSettings()
                        )
                    );
                }
            }
        }
    }

    return DIV({});
};

export const loaderAnim = () => DIV('loader-anim__small');

const render = () =>
    getCurrentSynteticLayerInfoOption().fold(
        DIV('app-split-wrapper legend-builder'),
        ({ info }) => {
            ensureSelectedFeature();
            return DIV(
                'app-split-wrapper legend-builder',
                renderHeader(info.id),
                renderBody()
            );
        }
    );

export default render;

logger('loaded');
