/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { ReactNode } from 'react';
import { fromNullable, none } from 'fp-ts/lib/Option';

import tr from 'sdi/locale';
import { DIV, NodeOrOptional, SPAN } from 'sdi/components/elements';

import {
    setAlphaColorForGroups,
    setPositionForLabel,
} from '../../events/legend-editor';
import {
    getFillColorForGroup,
    getGeometryType,
    getLegendType,
    getMarkerCodepoint,
    getMarkerCodepointForGroup,
    getPositionForLabel,
    getSelectedStyleGroup,
} from '../../queries/legend-editor';
import { renderLine, renderLineForGroup } from './tool-line';
import { renderPolygon, renderPolygonForGroup } from './tool-polygon';
import { renderMarker, renderMarkerForGroup } from './tool-point';
import {
    propSelectForLabel,
    fontColor,
    fontSize,
    offsetX,
    offsetY,
    labelResolution,
} from './tool-input';
import { layerOpacitySelector } from 'sdi/components/input';

export const renderStyleSettings = () => {
    const lt = getLegendType();
    const idx = getSelectedStyleGroup();

    const tool = (
        simple: () => React.ReactNode,
        group: (a: number) => React.ReactNode,
        layerStyle: NodeOrOptional //style for all the layer (even if there are categories/intervals)
    ): NodeOrOptional => {
        if (lt === 'simple') {
            return simple();
        } else if (idx >= 0) {
            return group(idx);
        }
        return layerStyle;
    };

    const polygonGlobalType = DIV(
        'polygon-layer-type',
        DIV('helptext', tr.compose('styleLayerHelper')),
        DIV('layer-opacity-title', tr.compose('layerOpacityHelper')),
        layerOpacitySelector(getFillColorForGroup, setAlphaColorForGroups)
    );

    switch (getGeometryType()) {
        case 'Point':
        case 'MultiPoint':
            return tool(renderMarker, renderMarkerForGroup, none);
        case 'LineString':
        case 'MultiLineString':
            return tool(renderLine, renderLineForGroup, none);
        case 'Polygon':
        case 'MultiPolygon':
            return tool(
                renderPolygon,
                renderPolygonForGroup,
                polygonGlobalType
            );
        case null:
            return DIV({}, 'ERROR - No Geometry Type');
    }
};

const renderPointLabelPosition = () => {
    const pos = getPositionForLabel();
    let cp = getMarkerCodepoint(0);
    if (0 === cp) {
        cp = getMarkerCodepointForGroup(0, 0);
        if (0 === cp) {
            cp = 0xf111;
        }
    }
    const setPos = (p: typeof pos) => () => {
        setPositionForLabel(p);
    };
    const elem = (p: typeof pos) => {
        const className = p === pos ? `label active ${p}` : `label ${p}`;
        return DIV({ className, onClick: setPos(p) });
    };

    return DIV(
        { className: 'style-tool label-position' },
        SPAN({ className: 'style-tool-label' }, tr.compose('labelPostion')),
        DIV(
            { className: 'label-position-widget' },
            DIV({ className: 'row' }, elem('above')),
            DIV(
                { className: 'row' },
                elem('left'),
                DIV({ className: 'picto' }, String.fromCodePoint(cp)),
                elem('right')
            ),
            DIV({ className: 'row' }, elem('under'))
        )
    );
};

const wrapper = (...children: ReactNode[]) =>
    DIV({ className: 'column__body label ' }, ...children);

const labelHelptext = () =>
    DIV({ className: 'helptext' }, tr.compose('helptextForLabel'));

export const renderLabelSettings = () =>
    fromNullable(getGeometryType()).map(gt => {
        switch (gt) {
            case 'Point':
            case 'MultiPoint':
                return wrapper(
                    labelHelptext(),
                    propSelectForLabel(),
                    fontColor(),
                    fontSize(),
                    renderPointLabelPosition(),
                    offsetX(),
                    offsetY(),
                    labelResolution()
                );

            case 'LineString':
            case 'MultiLineString':
                return wrapper(
                    labelHelptext(),
                    propSelectForLabel(),
                    fontColor(),
                    fontSize(),
                    // offsetX(),
                    offsetY(),
                    labelResolution()
                );

            case 'Polygon':
            case 'MultiPolygon':
                return wrapper(
                    labelHelptext(),
                    propSelectForLabel(),
                    fontColor(),
                    fontSize(),
                    labelResolution()
                );
        }
    });
