/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { DETAILS, DIV, LABEL, SPAN, SUMMARY } from 'sdi/components/elements';
import {
    MessageRecord,
    isContinuous,
    ContinuousInterval,
    ContinuousStyle,
    foldRemote,
} from 'sdi/source';
import tr, { fromRecord } from 'sdi/locale';
import {
    attrOptions,
    inputNumber,
    inputText,
    options,
} from 'sdi/components/input';

import { editRecord, inputLabel, loaderAnim } from '.';
import * as queries from '../../queries/legend-editor';
import * as events from '../../events/legend-editor';
import { makeIcon, makeLabelAndIcon } from '../button';
import { renderInputColor, renderUpAndDownBtns } from '.';
import { wrapTranslatableInput } from '../translatable';
import {
    loadDataInterval,
    setAutoClassNbDecimals,
    setAutoClassValue,
} from 'compose/src/events/legend-editor-continuous';
import { icon } from 'sdi/components/button';

const makeClassesButton = makeLabelAndIcon('validate', 2, 'check', () =>
    tr.core('validate')
);
const addGroupButton = makeLabelAndIcon('add', 2, 'plus', () =>
    tr.compose('addInterval')
);

const renderAdd = (groups: ContinuousInterval[]) =>
    addGroupButton(() => {
        events.addItem();
        events.selectStyleGroup(groups.length - 1);
    });

const removeGroupButton = makeIcon('clear', 3, 'trash', {
    text: () => tr.compose('remove'),
    position: 'left',
});
const closeGroupButton = makeIcon('close', 3, 'times', {
    text: () => tr.core('close'),
    position: 'left',
});

const renderIntervalActive = (i: ContinuousInterval, idx: number) => {
    const getLabel = () => i.label;
    const setLabel = (r: MessageRecord) => {
        events.setLabelForStyleInterval(idx, r);
    };
    const setLow = (val: number) => {
        if (!isNaN(val)) {
            events.setInterval(idx, val, i.high);
        }
    };
    const setHigh = (val: number) => {
        if (!isNaN(val)) {
            events.setInterval(idx, i.low, val);
        }
    };
    const k = `interval_${idx}`;

    const groupTitle = () => {
        const title = fromRecord(getLabel());
        return title === '' ? tr.compose('styleGroupDefaultName') : title;
    };

    return DIV(
        'group unfolded active',
        DIV(
            'group-title',
            SPAN('label', groupTitle()),
            DIV(
                'actions',
                removeGroupButton(() => events.removeItem(idx)),
                closeGroupButton(() => events.clearStyleGroup())
            )
        ),
        wrapTranslatableInput(
            getLabel(),
            k,
            inputText(
                attrOptions(
                    k,
                    () => groupTitle(),
                    t => editRecord(getLabel, setLabel, t),
                    {
                        className: k,
                        placeholder: groupTitle(),
                    }
                )
            )
        ),
        DIV(
            'style-tool interval low',
            LABEL(
                'input-label',
                tr.compose('lowValue'),
                inputNumber(
                    options(`input-int-low-${idx}`, () => i.low, setLow)
                )
            )
        ),
        DIV(
            'style-tool interval high',
            LABEL(
                'input-label',
                tr.compose('highValue'),
                inputNumber(
                    options(`input-int-high-${idx}`, () => i.high, setHigh)
                )
            )
        )
    );
};

const renderIntervalFolded = (
    i: ContinuousInterval,
    key: number,
    len: number
) => {
    let title = fromRecord(i.label);
    if (title === '') {
        title = tr.compose('styleGroupDefaultName');
    }
    return DIV(
        {
            key,
            className: 'group folded interactive',
            onClick: () => events.selectStyleGroup(key),
        },
        DIV(
            'group-title',
            SPAN('label', title),
            DIV('actions', renderUpAndDownBtns(key, len))
        )
    );
};

const renderInterval =
    (current: number) => (i: ContinuousInterval, key: number, len: number) => {
        if (key === current) {
            return renderIntervalActive(i, key);
        }
        return renderIntervalFolded(i, key, len);
    };

const renderAutoClass = (style: ContinuousStyle) =>
    DETAILS(
        'style-tool autoclass continuous',
        SUMMARY('', icon('cogs'), SPAN('label', tr.compose('autoClass'))),
        DIV(
            'autoclass-picker',
            LABEL(
                'input-label',
                tr.compose('classNumber'),
                inputNumber(
                    options(
                        `input-auto-class-nb`,
                        queries.getAutoClassValue,
                        setAutoClassValue
                    )
                )
            )
        ),
        DIV(
            'nb-decimals',
            LABEL(
                'input-label',
                tr.compose('nbDecimals'),
                inputNumber(
                    options(
                        'input-nb-decimals',
                        queries.getAutoClassNbDecimals,
                        setAutoClassNbDecimals
                    )
                )
            )
        ),
        renderInputColor(
            queries.getFirstGroupColor,
            events.setFirstGroupColor,
            tr.compose('firstInterval')
        ),
        renderInputColor(
            queries.getLastGroupColor,
            events.setLastGroupColor,
            tr.compose('lastInterval')
        ),
        makeClassesButton(() => {
            loadDataInterval(style.propName);
        })
    );

const renderAddBtns = (style: ContinuousStyle) =>
    DIV('add-btn', renderAdd(style.intervals), renderAutoClass(style));

const renderInputLabelWrapper = () =>
    DIV(
        'legend-label-input__wrapper',
        DIV('input-label', tr.compose('legendLabelValue')),
        inputLabel()
    );

const renderList = (style: ContinuousStyle) =>
    DIV(
        'column__body value-picker-groups',
        renderInputLabelWrapper(),
        ...style.intervals.map((int, i, array) =>
            renderInterval(queries.getSelectedStyleGroup())(
                int,
                i,
                array.length
            )
        ),
        renderAddBtns(style)
    );

const renderGroups = () => {
    const style = queries.getStyle();
    if (style && isContinuous(style)) {
        return foldRemote(
            () => renderList(style),
            () =>
                DIV(
                    'loader loader__small',
                    loaderAnim(),
                    tr.compose('loadingData')
                ),
            (err: string) => DIV('error', err),
            () => renderList(style)
        )(queries.getDataInterval());
    }

    return DIV('column__body value-picker-groups');
};

const render = () => renderGroups();

export default render;
