/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { DIV, SPAN } from 'sdi/components/elements';
import { StyleConfig, SubType } from 'sdi/source';
import tr from 'sdi/locale';

import * as queries from '../../queries/legend-editor';
import { selectDiscrete } from '../../events/legend-editor-discrete';
import { selectContinuous } from '../../events/legend-editor-continuous';
import * as events from '../../events/legend-editor';
import { inputLabel } from '.';
import { nameToString } from 'sdi/components/button/names';

const logger = debug('sdi:legend-editor/select-type');

// Holds the old simple style to be able to go back
let prevSimpleStyle: StyleConfig;

const selectSimpleType = () => {
    events.selectSimple(prevSimpleStyle);
};

const backupSimpleStyle = () => {
    const style = queries.getStyle();

    if (style !== null) {
        prevSimpleStyle = style;
    }
};

const selectDiscreteType = () => {
    if (queries.getLegendType() === 'simple') {
        backupSimpleStyle();
    }
    selectDiscrete();
};

const selectContinuousType = () => {
    if (queries.getLegendType() === 'simple') {
        backupSimpleStyle();
    }
    selectContinuous();
};

const renderInputLabelWrapper = () =>
    DIV(
        'legend-label-input__wrapper',
        DIV('input-label', tr.compose('legendLabelValue')),
        inputLabel()
    );

const dynamicCheckbox = (selectedLegendType: SubType, legendType: SubType) =>
    SPAN(
        'fa icon',
        nameToString(
            selectedLegendType === legendType ? 'check-square' : 'SquareEThin'
        )
    );

const dynamicClass = (selectedLegendType: SubType, legendType: SubType) =>
    selectedLegendType === legendType ? 'active' : '';

export const render = () => {
    const legendType = queries.getLegendType();

    const options = [
        DIV(
            {
                className: dynamicClass(legendType, 'simple'),
            },
            DIV(
                { onClick: () => selectSimpleType() },
                dynamicCheckbox(legendType, 'simple'),
                tr.compose('legendTypeSimple')
            ),
            legendType === 'simple' ? renderInputLabelWrapper() : ''
        ),

        DIV(
            {
                className: dynamicClass(legendType, 'discrete'),
                onClick: () => selectDiscreteType(),
            },
            dynamicCheckbox(legendType, 'discrete'),
            tr.compose('legendTypeDiscrete')
        ),

        DIV(
            {
                className: dynamicClass(legendType, 'continuous'),
                onClick: () => selectContinuousType(),
            },
            dynamicCheckbox(legendType, 'continuous'),
            tr.compose('legendTypeContinuous')
        ),
    ];

    return DIV({ className: 'column__body legend-picker' }, ...options);
};

export default render;

logger('loaded');
