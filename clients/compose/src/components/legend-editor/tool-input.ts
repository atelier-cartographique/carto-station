/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
// import * as Color from 'color';

import tr from 'sdi/locale';
import {
    BUTTON,
    DIV,
    LABEL,
    NodeOrOptional,
    SPAN,
} from 'sdi/components/elements';
import { inputNumber, InputOptions } from 'sdi/components/input';
import { PatternAngle } from 'sdi/source';
import { PictoKey, renderPicto } from '../pictos';

import {
    getDashStyle,
    getDashStyleForGroup,
    getFillColor,
    getFillColorForGroup,
    getFontColor,
    getFontSize,
    getMarkerCodepoint,
    getMarkerCodepointForGroup,
    getMarkerColor,
    getMarkerColorForGroup,
    getMarkerSize,
    getMarkerSizeForGroup,
    getOffsetXForLabel,
    getOffsetYForLabel,
    getPattern,
    getPatternAngle,
    getPatternAngleForGroup,
    getPatternColor,
    getPatternColorForGroup,
    getPatternForGroup,
    getPatternWidth,
    getPatternWidthForGroup,
    getPropNameForLabel,
    getStrokeColor,
    getStrokeColorForGroup,
    getStrokeWidth,
    getStrokeWidthForGroup,
    getZoomForLabel,
} from '../../queries/legend-editor';
import {
    setDashStyle,
    setDashStyleForGroup,
    setFillColor,
    setFillColorForGroup,
    setFontColor,
    setFontSize,
    setMarkerCodepoint,
    setMarkerCodepointForGroup,
    setMarkerColor,
    setMarkerColorForGroup,
    setMarkerSize,
    setMarkerSizeForGroup,
    setOffsetXForLabel,
    setOffsetYForLabel,
    setPattern,
    setPatternAngle,
    setPatternAngleForGroup,
    setPatternColor,
    setPatternColorForGroup,
    setPatternForGroup,
    setPatternWidth,
    setPatternWidthForGroup,
    setPropNameForLabel,
    setStrokeColor,
    setStrokeColorForGroup,
    setStrokeWidth,
    setStrokeWidthForGroup,
    setZoomForLabel,
} from '../../events/legend-editor';

import { renderInputColor, renderPropSelect } from '.';
import { getCurrentLayerId } from 'compose/src/queries/app';
import { ComposeMessageKey } from 'compose/src/locale';
import { mapPictos, nameToString } from 'sdi/components/button/names';
import { pictoFontName } from 'sdi/app';
import { sameArrays } from 'sdi/util';
import { icon } from 'sdi/components/button';

const logger = debug(`sdi:legend-editor/tool-input`);

type Getter<T> = () => T;
type Setter<T> = (v: T) => void;

type NumberGetter = () => number;
type NumberSetter = (a: number) => void;

// export const makeColor = (i: string | number, a?: number) => {
//     try {
//         if (a) {
//             return colord(i).alpha(a);
//         }
//         return colord(i);
//     } catch (e) {
//         return colord('rgb(0,0,0)');
//     }
// };

interface Selection<T> {
    [k: string]: T;
}

const renderSelection =
    <T>(series: Selection<T>[], render: (s: string) => NodeOrOptional) =>
    (className: string, get: Getter<T>, set: Setter<T>) => {
        const selected = get();

        const renderItem = (k: string, val: T) =>
            DIV(
                {
                    key: `serie-item-${k}`,
                    className: val === selected ? 'selected' : '',
                    onClick: () => set(val),
                },
                render(k)
            );

        const renderSerie = (serie: Selection<T>) =>
            DIV(
                'serie',
                Object.keys(serie).map(k => renderItem(k, serie[k]))
            );

        return DIV({ className }, DIV('library', ...series.map(renderSerie)));
    };

const renderInputIcon = renderSelection(
    [
        // {
        //     [String.fromCodePoint(0xf111)]: 0xf111,
        //     [String.fromCodePoint(0xf1db)]: 0xf1db,
        //     [String.fromCodePoint(0xf10c)]: 0xf10c,
        //     [String.fromCodePoint(0xf192)]: 0xf192,
        // },

        Object.fromEntries(
            mapPictos('circle', (name, value) => [nameToString(name), value])
        ),
        Object.fromEntries(
            mapPictos('triangle', (name, value) => [nameToString(name), value])
        ),
        Object.fromEntries(
            mapPictos('square', (name, value) => [nameToString(name), value])
        ),
        Object.fromEntries(
            mapPictos('losange', (name, value) => [nameToString(name), value])
        ),
        Object.fromEntries(
            mapPictos('pentagon', (name, value) => [nameToString(name), value])
        ),
        Object.fromEntries(
            mapPictos('hexagon', (name, value) => [nameToString(name), value])
        ),
    ],
    picto =>
        SPAN(
            {
                style: {
                    fontFamily: pictoFontName,
                },
            },
            picto
        )
);

const renderInputPatternAngle = renderSelection<PatternAngle>(
    [
        {
            '0°': 0,
            '45°': 45,
            '90°': 90,
            '135°': 135,
        },
    ],
    x => x
);

const renderInputNumberAttrs = (
    className: string,
    label: string,
    { key, get, set, attrs }: InputOptions<number>
) => {
    const update = (newVal: number) => {
        if (!isNaN(newVal)) {
            set(newVal);
        }
    };

    return DIV(
        `style-tool ${className}`,
        LABEL(
            'input-label',
            label,
            inputNumber({
                get,
                set: update,
                key: `legend-editor-input-nb-${key}-${getCurrentLayerId()}`,
                attrs,
                monitor: update,
            })
        )
    );
};

const renderInputNumber = (
    className: string,
    label: string,
    get: NumberGetter,
    set: NumberSetter
) =>
    renderInputNumberAttrs(className, label, {
        key: `legend-editor-input-nb-${label}-${getCurrentLayerId()}`,
        get,
        set,
        attrs: {},
    });

export const lineWidth = () => {
    return renderInputNumber(
        'size',
        tr.compose('lineWidth'),
        () => getStrokeWidth(1),
        (n: number) => setStrokeWidth(n)
    );
};

export const lineColor = () =>
    renderInputColor(getStrokeColor, setStrokeColor, tr.compose('lineColor'));

type DashStyle = [ComposeMessageKey, number[], PictoKey];
const DASH_STYLES: DashStyle[] = [
    ['line-style/solid', [], 'pic-line-line'],
    ['line-style/dash', [3, 3], 'pic-line-dash'],
    ['line-style/dot', [1, 1], 'pic-line-dot'],
    ['line-style/dash-dot', [3, 1, 1, 1], 'pic-line-dash-dot'],
    ['line-style/dash-dot-dot', [3, 1, 1, 1, 1, 1], 'pic-line-dash-dot-dot'],
];
const dashName = (dashStyle: DashStyle) => dashStyle[0];
const dashCode = (dashStyle: DashStyle) => dashStyle[1];
const dashPicto = (dashStyle: DashStyle) => dashStyle[2];

// const dashPatEq = (a: number[], b: number[]) =>
//     a.length === b.length && zip(a, b).every(([a, b]) => a === b);
// const findDashStyle = (pat: number[]) =>
//     fromNullable(DASH_STYLES.find(([_, ns]) => dashPatEq(pat, ns)));

const isSelectedDash = (dashStyle: DashStyle, get: () => number[]) =>
    sameArrays(get(), dashCode(dashStyle)) ? 'selected' : '';

const dashSelectBtn = (
    set: (n: number[]) => void,
    get: () => number[],
    dashStyle: DashStyle
) =>
    BUTTON(
        {
            key: `${dashName(dashStyle)}`,
            className: `btn ${isSelectedDash(dashStyle, get)}`,
            title: tr.compose(dashName(dashStyle)),
            onClick: () => set(dashCode(dashStyle)),
        },
        renderPicto(dashPicto(dashStyle))
    );

const renderDashInput = (get: () => number[], set: (n: number[]) => void) =>
    DIV(
        'style-tool dash',
        LABEL(
            'input-label',
            tr.compose('line-style/label'),
            DIV(
                'input-line__wrapper',
                ...DASH_STYLES.map(ds => dashSelectBtn(set, get, ds))
            )
        )
    );

export const lineDashStyle = () => renderDashInput(getDashStyle, setDashStyle);

export const fillColor = () =>
    renderInputColor(getFillColor, setFillColor, tr.compose('fillColor'));

export const pattern = () => {
    const p = getPattern();
    if (!p) {
        return DIV(
            'style-tool hatch-wrapper',
            DIV(
                {
                    className: 'hatch-pattern inactive',
                    onClick: () => setPattern(true),
                },
                icon('square-o'),
                tr.compose('usePattern')
            )
        );
    }
    return DIV(
        'style-tool hatch-wrapper',
        DIV(
            {
                className: 'hatch-pattern active',
                onClick: () => setPattern(false),
            },
            icon('check-square-o'),
            tr.compose('usePattern')
        ),
        renderInputPatternAngle('angle', getPatternAngle, setPatternAngle),
        renderInputColor(
            getPatternColor,
            setPatternColor,
            tr.compose('pattern-style/color')
        ),
        renderInputNumber(
            'pattern-width',
            tr.compose('pattern-style/width'),
            getPatternWidth,
            setPatternWidth
        )
    );
};

export const lineWidthForGroup = (idx: number) => {
    return renderInputNumber(
        'size',
        tr.compose('lineWidth'),
        () => getStrokeWidthForGroup(idx, 1),
        (n: number) => setStrokeWidthForGroup(idx, n)
    );
};

export const lineColorForGroup = (idx: number) =>
    renderInputColor(
        () => getStrokeColorForGroup(idx),
        c => setStrokeColorForGroup(idx, c),
        tr.compose('lineColor')
    );

export const fillColorForGroup = (idx: number) =>
    renderInputColor(
        () => getFillColorForGroup(idx),
        c => setFillColorForGroup(idx, c),
        tr.compose('fillColor')
    );

export const lineDashStyleForGroup = (idx: number) =>
    renderDashInput(
        () => getDashStyleForGroup(idx),
        pat => setDashStyleForGroup(idx, pat)
    );

export const patternForGroup = (idx: number) => {
    const p = getPatternForGroup(idx);
    if (!p) {
        return DIV(
            'style-tool hatch-wrapper',
            DIV(
                {
                    className: 'hatch-pattern inactive',
                    onClick: () => setPatternForGroup(idx, true),
                },
                icon('square-o'),
                tr.compose('usePattern')
            )
        );
    }
    return DIV(
        'style-tool hatch-wrapper',
        DIV(
            {
                className: 'hatch-pattern active',
                onClick: () => setPatternForGroup(idx, false),
            },
            icon('check-square-o'),
            tr.compose('usePattern')
        ),
        renderInputPatternAngle(
            'angle',
            () => getPatternAngleForGroup(idx),
            v => setPatternAngleForGroup(idx, v)
        ),
        renderInputColor(
            () => getPatternColorForGroup(idx),
            v => setPatternColorForGroup(idx, v),
            tr.compose('pattern-style/color')
        ),
        renderInputNumber(
            'pattern-width',
            tr.compose('pattern-style/width'),
            () => getPatternWidthForGroup(idx),
            v => setPatternWidthForGroup(idx, v)
        )
    );
};

export const fontColor = () =>
    renderInputColor(getFontColor, setFontColor, tr.compose('fontColor'));

export const fontSize = () => {
    return renderInputNumberAttrs('size', tr.compose('fontSize'), {
        key: 'fontSize',
        get: () => getFontSize(),
        set: (n: number) => setFontSize(n),
        attrs: { min: 0 },
    });
};

export const offsetX = () => {
    return renderInputNumber(
        'size',
        tr.compose('offsetX'),
        () => getOffsetXForLabel(),
        (n: number) => setOffsetXForLabel(n)
    );
};

export const offsetY = () => {
    return renderInputNumber(
        'size',
        tr.compose('offsetY'),
        () => getOffsetYForLabel(),
        (n: number) => setOffsetYForLabel(n)
    );
};

export const labelResolution = () => {
    return renderInputNumberAttrs('size', tr.compose('labelZoom'), {
        key: `labelZoom`,
        get: () => getZoomForLabel(),
        set: (n: number) => setZoomForLabel(n),
        attrs: { step: 0.2, min: 0, max: 30 },
    });
};

export const propSelectForLabel = () =>
    renderPropSelect(setPropNameForLabel, getPropNameForLabel);

export const markerSizeForGroup = (idx: number) => {
    return renderInputNumber(
        'size',
        tr.compose('size'),
        () => getMarkerSizeForGroup(idx),
        (n: number) => setMarkerSizeForGroup(idx, n)
    );
};

export const markerColorForGroup = (idx: number) =>
    renderInputColor(
        () => getMarkerColorForGroup(idx),
        c => setMarkerColorForGroup(idx, c),
        tr.compose('pointColor')
    );

export const markerCodepointForGroup = (idx: number) => {
    return renderInputIcon(
        'style-tool picto-collection code',
        () => getMarkerCodepointForGroup(idx),
        (n: number) => setMarkerCodepointForGroup(idx, n)
    );
};

export const markerSize = () => {
    return renderInputNumber(
        'size',
        tr.compose('size'),
        () => getMarkerSize(),
        (n: number) => setMarkerSize(n)
    );
};

export const markerColor = () =>
    renderInputColor(getMarkerColor, setMarkerColor, tr.compose('pointColor'));

export const markerCodepoint = () => {
    return renderInputIcon(
        'style-tool picto-collection code',
        () => getMarkerCodepoint(),
        (n: number) => setMarkerCodepoint(n)
    );
};

logger('loaded');
