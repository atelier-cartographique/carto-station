/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { KeyboardEvent } from 'react';
import { DIV, NodeOrOptional, SPAN } from 'sdi/components/elements';
import {
    attrOptions,
    inputNumber,
    inputText,
    renderSelectFilter,
    renderSelectFilterAdd,
} from 'sdi/components/input';
import tr, { fromRecord, Translated } from 'sdi/locale';
import { isENTER } from 'sdi/components/keycodes';
import {
    isDiscrete,
    MessageRecord,
    DiscreteGroup,
    FieldsDescriptor,
    streamFieldName,
    streamFieldType,
    Feature,
    DiscreteStyle,
    foldRemote,
} from 'sdi/source';
import {
    addDiscreteStyleGroupValue,
    loadDistinctValues,
    makeGroupTitle,
    removeDiscreteStyleGroupValue,
    setDistinctValuesFromData,
} from '../../events/legend-editor-discrete';
import * as queries from '../../queries/legend-editor';
import { makeIcon, makeLabelAndIcon } from '../button';
import {
    getCurrenFields,
    getCurrentLayerData,
    getCurrentSynteticLayerInfoOption,
    getFeaturePropValues,
} from 'compose/src/queries/app';
import { fromNullable, fromPredicate, none, Option } from 'fp-ts/lib/Option';
import { tryNumber, tryString } from 'sdi/util';
import { findTerm, getTermList } from 'sdi/app';
import { setoidNumber } from 'fp-ts/lib/Setoid';
import { iife } from 'sdi/lib';
import { inputLabel, loaderAnim, renderUpAndDownBtns } from '.';
import { editRecord } from '.';
import { identity } from 'fp-ts/lib/function';
import * as events from '../../events/legend-editor';
import { wrapTranslatableInput } from '../translatable';

const logger = debug('sdi:legend-editor/select-type');

const addGroupButton = makeLabelAndIcon('add', 2, 'plus', () =>
    tr.compose('styleGroupAdd')
);
const addTermButton = makeIcon('add', 2, 'check', {
    position: 'top-left',
    text: () => tr.compose('addThisTermToGroup'),
});
const deleteValueButton = (fieldValue: string) =>
    makeIcon('clear', 3, 'times', {
        position: 'bottom-left',
        text: () => `${tr.core('remove')}: ${fieldValue}` as Translated,
    });

const setStyleGroupTitle = (k: number) => (title: MessageRecord) =>
    events.setLabelForStyleGroup(k, title);

const renderAdd = (groups: DiscreteGroup[]) => {
    return addGroupButton(() => {
        events.addItem();
        events.selectStyleGroup(groups.length - 1);
    });
};

const renderStyleGroupValue =
    (field: FieldsDescriptor) => (value: string | number, k: number) => {
        const fieldValue = iife(() => {
            switch (streamFieldType(field)) {
                case 'term':
                    return typeof value === 'number'
                        ? termToString(value)
                        : termToString(parseInt(value, 10));
                default:
                    return value.toString();
            }
        });

        return DIV(
            'tag small',
            SPAN('tag__value', fieldValue),
            deleteValueButton(fieldValue)(() =>
                removeDiscreteStyleGroupValue(k)
            )
        );
    };

const styleGroupValueAdd = () => {
    const value = queries.getStyleGroupEditedValue();
    const style = queries.getStyle();

    if (value != null && style && isDiscrete(style)) {
        const groups: DiscreteGroup[] = style.groups;

        if (groups.every(group => group.values.indexOf(value) === -1)) {
            addDiscreteStyleGroupValue(value);
            events.setStyleGroupEditedValue('');
        }
    }
};

const styleGroupValueKeyHandler = (e: KeyboardEvent<HTMLInputElement>) => {
    if (isENTER(e)) {
        styleGroupValueAdd();
    }
};

const renderInputText = (curVal: Option<string>, key: string) =>
    DIV(
        'input-text',
        wrapTranslatableInput(
            { fr: 'test', nl: 'testnl' },
            key,
            inputText(
                attrOptions(
                    `StyleGroupUnfoldedTerm_${key} `,
                    () => curVal.getOrElse(''),
                    s => events.setStyleGroupEditedValue(s),
                    {
                        onKeyDown: styleGroupValueKeyHandler,
                    }
                )
            )
        )
    );

const renderInputFilterSelectText = renderSelectFilterAdd(
    identity,
    events.setStyleGroupEditedValue,
    events.setStyleGroupEditedText,
    () => tr.compose('selectValue')
);

const unselectedValues = (
    features: Feature[],
    field: string,
    group: DiscreteGroup
) =>
    getFeaturePropValues(features, field).filter(
        v => group.values.indexOf(v) === -1
    );

const renderFilterInputOrStr = (
    curVal: Option<string>,
    group: DiscreteGroup,
    field: string,
    key: string
) =>
    getCurrentLayerData().map(ldEither =>
        ldEither.fold(
            () => renderInputText(curVal, field),
            ldOpt =>
                ldOpt.fold(renderInputText(curVal, field), ld =>
                    DIV(
                        'input-select-text',
                        renderInputFilterSelectText(
                            unselectedValues(ld.features, field, group),
                            curVal,
                            key
                        )
                    )
                )
        )
    );

const renderStringInput = (
    field: FieldsDescriptor,
    key: number,
    group: DiscreteGroup
) => {
    const curVal = tryString(queries.getStyleGroupEditedValue());
    return DIV(
        {
            className: 'add-term__wrapper',
            key: `StyleGroupUnfoldedTerm_${key}`,
        },
        renderFilterInputOrStr(
            curVal,
            group,
            streamFieldName(field),
            `select-prop-${key}`
        ),
        addTermButton(styleGroupValueAdd)
    );
};

const renderNumberInput = (_field: FieldsDescriptor, key: number) =>
    tryNumber(queries.getStyleGroupEditedValue())
        .map<NodeOrOptional>(curVal =>
            DIV(
                'add-term__wrapper',
                inputNumber({
                    get: () => curVal,
                    set: events.setStyleGroupEditedValue,
                    key: `StyleGroupUnfoldedTerm_${key} `,
                    attrs: {
                        onKeyDown: styleGroupValueKeyHandler,
                    },
                    monitor: events.setStyleGroupEditedValue,
                }),
                addTermButton(styleGroupValueAdd)
            )
        )
        .getOrElse(
            inputNumber({
                get: () => 0,
                set: events.setStyleGroupEditedValue,
                key: `StyleGroupUnfoldedTerm_${key} `,
                attrs: {
                    placeholder: tr.compose('addTerm'),
                },
                monitor: events.setStyleGroupEditedValue,
            })
        );

const termToString = (tid: number) =>
    findTerm(tid)
        .map<string>(t => fromRecord(t.name))
        .getOrElse(tid.toString());

const termSelectFilter = renderSelectFilter<number>(
    setoidNumber,
    tid => DIV({}, termToString(tid)),
    events.setStyleGroupEditedValue,
    termToString,
    () => tr.compose('selectValue')
);

const termSelected = fromPredicate<number>(id => id >= 0);

const renderTermInput = (_field: FieldsDescriptor, _key: number) =>
    tryNumber(queries.getStyleGroupEditedValue())
        .map<NodeOrOptional>(curVal =>
            DIV(
                'add-term__wrapper',
                termSelectFilter(
                    getTermList().map(t => t.id),
                    termSelected(curVal)
                ),
                addTermButton(styleGroupValueAdd)
            )
        )
        .getOrElse(
            termSelectFilter(
                getTermList().map(t => t.id),
                none
            )
        );

const renderDetailedInput = (
    field: FieldsDescriptor,
    group: DiscreteGroup,
    key: number
): NodeOrOptional => {
    switch (streamFieldType(field)) {
        case 'term':
            return renderTermInput(field, key);
        case 'number':
            return renderNumberInput(field, key);
        default:
            return renderStringInput(field, key, group);
    }
};

const groupTitle = (group: DiscreteGroup) => {
    const title = fromRecord(group.label);
    return title === '' ? tr.compose('styleGroupDefaultName') : title;
};
const removeGroupButton = makeIcon('clear', 3, 'trash', {
    text: () => tr.compose('removeGroup'),
    position: 'left',
});
const closeGroupButton = makeIcon('close', 3, 'times', {
    text: () => tr.core('close'),
    position: 'left',
});

const renderStyleGroupUnfolded = (
    propName: string,
    group: DiscreteGroup,
    key: number
) => {
    return getCurrenFields()
        .chain(fields => {
            return fromNullable(
                fields.find(field => streamFieldName(field) === propName)
            );
        })
        .map(field => {
            const editTitle = wrapTranslatableInput(
                group.label,
                `input-wrapper ${key}`,
                inputText(
                    attrOptions(
                        `StyleGroupUnfolded_${key}`,
                        () => makeGroupTitle(group, propName),
                        t =>
                            editRecord(
                                () => group.label,
                                setStyleGroupTitle(key),
                                t
                            ),
                        {
                            className: `StyleGroupUnfolded_${key}`,
                            placeholder: tr.compose('styleGroupDefaultName'),
                        }
                    )
                )
            );

            const input = renderDetailedInput(field, group, key);
            return DIV(
                {
                    key,
                    className: 'group unfolded active',
                },
                DIV(
                    'group-title',
                    SPAN('label', groupTitle(group)),
                    DIV(
                        'actions',
                        removeGroupButton(() => events.removeItem(key)),
                        closeGroupButton(() => events.clearStyleGroup())
                    )
                ),
                editTitle,
                DIV(
                    'tag__list',
                    DIV('tag-list-title', tr.compose('filteredTerms')),
                    ...group.values.map(renderStyleGroupValue(field))
                ),
                input
            );
        });
};

const renderStyleGroupFolded = (
    group: DiscreteGroup,
    key: number,
    len: number
) =>
    DIV(
        {
            key,
            className: 'group folded interactive',
            onClick: () => events.selectStyleGroup(key),
        },
        DIV(
            'group-title',
            SPAN('label', groupTitle(group)),
            DIV('actions', renderUpAndDownBtns(key, len))
        )
    );

const renderStyleGroup =
    (propName: string) => (group: DiscreteGroup, k: number, len: number) => {
        if (k === queries.getSelectedStyleGroup()) {
            return renderStyleGroupUnfolded(propName, group, k);
        } else {
            return renderStyleGroupFolded(group, k, len);
        }
    };

const makeClassesButton = makeLabelAndIcon('validate', 2, 'cogs', () =>
    tr.compose('autoClass')
);
const renderAutoClass = (style: DiscreteStyle) =>
    DIV(
        'style-tool autoclass',
        makeClassesButton(() => {
            getCurrentSynteticLayerInfoOption().map(li => {
                li.metadata.dataStreamUrl
                    ? loadDistinctValues(style.propName)
                    : setDistinctValuesFromData(style.propName);
            });
        })
    );

const renderAddBtns = (style: DiscreteStyle) =>
    DIV('add-btn', renderAdd(style.groups), renderAutoClass(style));

const renderInputLabelWrapper = () =>
    DIV(
        'legend-label-input__wrapper',
        DIV('input-label', tr.compose('legendLabelValue')),
        inputLabel()
    );

const renderList = (style: DiscreteStyle) =>
    DIV(
        'column__body value-picker-groups',
        renderInputLabelWrapper(),
        ...style.groups.map((g, i, array) =>
            renderStyleGroup(style.propName)(g, i, array.length)
        ),
        renderAddBtns(style)
    );

const render = () => {
    const style = queries.getStyle();
    if (style && isDiscrete(style)) {
        return foldRemote(
            () => renderList(style),
            () =>
                DIV(
                    'loader loader__small',
                    loaderAnim(),
                    tr.compose('loadingData')
                ),
            (err: string) => DIV('error', `ERROR: ${err}`),
            () => renderList(style)
        )(queries.getDistinctValues());
    }
    return DIV('column__body value-picker-groups');
};

export default render;

logger('loaded');
