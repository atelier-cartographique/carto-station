import { DIV, H1 } from 'sdi/components/elements';
import tr from 'sdi/locale';

const render = () =>
    DIV({}, H1({}, tr.compose('studio')), DIV({}, tr.compose('loadingData')));

export default render;
