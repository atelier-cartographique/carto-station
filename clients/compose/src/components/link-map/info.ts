import { DIV, H2 } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';

import { loadPublishedMaps, setLayout } from 'compose/src/events/app';
import { AppLayout } from 'compose/src/shape/types';
import { getLinkedMaps } from 'compose/src/queries/map-info';
import { makeLabelAndIcon } from '../button';

const addButton = makeLabelAndIcon('add', 2, 'plus', () =>
    tr.compose('compose:addLinkedMap')
);

const renderLinks = () =>
    getLinkedMaps('forward').map(link => DIV('link', fromRecord(link.title)));

const renderLinksBackward = () =>
    getLinkedMaps('backward').map(link =>
        DIV('link backward', fromRecord(link.title))
    );

const renderAddButton = () =>
    DIV(
        {},
        addButton(() =>
            loadPublishedMaps().then(() => setLayout(AppLayout.MapLink))
        )
    );

export const render = () =>
    DIV(
        'tab__section map-related-maps',
        H2({}, tr.compose('relatedMapsLabel')),
        DIV(
            'description-wrapper',
            DIV('helptext', tr.compose('compose:infoRelatedMap'))
        ),
        renderAddButton(),
        DIV('related-maps', ...renderLinks(), ...renderLinksBackward())
    );
