import { DIV, H1, H2, NODISPLAY, SPAN } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { IMapInfo } from 'sdi/source';

import { setLayout } from 'compose/src/events/app';
import { AppLayout } from 'compose/src/shape/types';
import {
    getLinkedMaps,
    getPublishedUnlinkedMaps,
} from 'compose/src/queries/map-info';
import { makeIcon, makeLabelAndIcon } from '../button';
import { addLinkMap, removeLinkMap } from 'compose/src/events/map-info';
import { nameToString } from 'sdi/components/button/names';

const linkButton = makeIcon('add', 2, 'link', {
    text: () => tr.compose('add'),
    position: 'top-right',
});

const unlinkButton = makeIcon('remove', 2, 'unlink', {
    text: () => tr.compose('remove'),
    position: 'top-right',
});
const backButton = makeLabelAndIcon('navigate', 1, 'times-circle', () =>
    tr.compose('close')
);

const mapTitle = (title: string) => DIV('map-title', title);
const mapStatus = (status: string) => SPAN(`status-label `, status);

const unlinkAction = (minfo: IMapInfo) =>
    DIV(
        'link-action',
        unlinkButton(() => removeLinkMap(minfo.id))
    );

const linkAction = (minfo: IMapInfo) =>
    DIV(
        'link-action',
        linkButton(() => addLinkMap(minfo.id))
    );

const renderStatusIcon = (mapInfo: IMapInfo) =>
    SPAN(
        'fa icon',
        mapInfo.status === 'draft' ? nameToString('lock') : '',
        mapInfo.status === 'published' ? nameToString('user-plus') : '',
        mapInfo.status === 'saved' ? nameToString('save') : ''
    );

const renderMap = (mapInfo: IMapInfo) => {
    const mid = mapInfo.id;
    const title = fromRecord(mapInfo.title);
    const status = mapInfo.inPublicGroup
        ? tr.core('public_published')
        : tr.core('internally_published');
    return DIV(
        {
            key: mid,
            className: 'map-list--item',
        },
        mapTitle(title),
        DIV(
            `map-status ${mapInfo.status}`,
            renderStatusIcon(mapInfo),
            mapStatus(status)
        )
    );
};

const renderLinkedMap = (minfo: IMapInfo, editable: boolean) =>
    DIV(
        'linked-map',
        editable ? unlinkAction(minfo) : NODISPLAY(),
        renderMap(minfo)
    );

const renderUnlinkedMap = (minfo: IMapInfo) =>
    DIV('un-linked-map', linkAction(minfo), renderMap(minfo));

const renderLinked = () =>
    DIV(
        'linked-map-list--forward',
        H2({}, tr.compose('compose:linkedMaps')),
        DIV(
            'map-list-wrapper',
            DIV(
                'map-list',
                ...getLinkedMaps('forward').map(m => renderLinkedMap(m, true))
            )
        )
    );
const renderLinkedBackward = () =>
    DIV(
        'linked-map-list--backward',
        H2({}, tr.compose('compose:linkedMapsFromOthers')),
        DIV(
            'map-list-wrapper backward',
            DIV(
                'map-list',
                ...getLinkedMaps('backward').map(m => renderLinkedMap(m, false))
            )
        )
    );

const renderUnlinked = () =>
    DIV(
        'un-linked-map-list',
        DIV(
            'map-list-wrapper',
            H2({}, tr.compose('compose:publishedUnlinkedMaps')),
            DIV(
                'map-list',
                ...getPublishedUnlinkedMaps().map(renderUnlinkedMap)
            )
        )
    );

export const render = () =>
    DIV(
        'select-related-maps-wrapper',
        H1({}, tr.compose('selectLinkedMap')),
        backButton(() => setLayout(AppLayout.MapAndInfo)),
        DIV(
            'map-selector-wrapper',
            renderUnlinked(),
            DIV('linked-map-list', renderLinked(), renderLinkedBackward())
        )
    );
