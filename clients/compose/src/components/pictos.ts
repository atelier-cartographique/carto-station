import { createElement } from 'react';
import { DIV } from 'sdi/components/elements';

export type PreserveAspectRatio =
    | 'none'
    | 'xMinYMin'
    | 'xMidYMin'
    | 'xMaxYMin'
    | 'xMinYMid'
    | 'xMidYMid'
    | 'xMaxYMid'
    | 'xMinYMax'
    | 'xMidYMax'
    | 'xMaxYMax';

const pictos = {
    'pic-line-dash': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            {
                width: '20.015625mm',
                height: '1.158203mm',
                viewBox: '0 0 20.015625 1.158203',
                version: '1.1',
                id: 'ppic-line-dash-svg5',
                preserveAspectRatio,
            },
            createElement('defs', { id: 'ppic-line-dash-defs2' }),
            createElement(
                'g',
                {
                    id: 'ppic-line-dash-layer1',
                    transform: 'translate(-43.199219,-93.603516)',
                },
                createElement('path', {
                    id: 'path890-0-9-4-9-3-1',
                    d: 'm 48.554687,93.603516 v 1.158203 h 3.947266 v -1.158203 z',
                }),
                createElement('path', {
                    id: 'path890-0-9-5-5',
                    d: 'm 43.199219,93.603516 v 1.158203 h 3.947265 v -1.158203 z',
                }),
                createElement('path', {
                    id: 'path890-0-9-4-9-5-8',
                    d: 'm 53.910156,93.603516 v 1.158203 h 3.947266 v -1.158203 z',
                }),
                createElement('path', {
                    id: 'path890-0-9-4-9-5-8-9',
                    d: 'm 59.265625,93.603516 v 1.158203 h 3.949219 v -1.158203 z',
                })
            )
        ),
    'pic-line-dash-dot-dot': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            {
                width: '20.003906mm',
                height: '1.1582mm',
                viewBox: '0 0 20.003906 1.1582',
                version: '1.1',
                id: 'ppic-line-dash-dot-dot-svg5',
                preserveAspectRatio,
            },
            createElement('defs', { id: 'ppic-line-dash-dot-dot-defs2' }),
            createElement(
                'g',
                {
                    id: 'ppic-line-dash-dot-dot-layer1',
                    transform: 'translate(-43.199219,-102.86719)',
                },
                createElement('path', {
                    id: 'path890-0-9',
                    d: 'm 43.199219,102.86719 v 1.1582 h 3.947265 v -1.1582 z',
                }),
                createElement('path', {
                    id: 'path890-3-7-1',
                    d: 'm 48.65625,102.86719 v 1.1582 h 1.140625 v -1.1582 z',
                }),
                createElement('path', {
                    id: 'path890-3-7-1-5',
                    d: 'm 51.306641,102.86719 v 1.1582 h 1.138671 v -1.1582 z',
                }),
                createElement('path', {
                    id: 'path890-0-9-4',
                    d: 'm 53.955078,102.86719 v 1.1582 h 3.949219 v -1.1582 z',
                }),
                createElement('path', {
                    id: 'path890-3-7-1-9',
                    d: 'm 59.414062,102.86719 v 1.1582 h 1.138672 v -1.1582 z',
                }),
                createElement('path', {
                    id: 'path890-3-7-1-5-2',
                    d: 'm 62.0625,102.86719 v 1.1582 h 1.140625 v -1.1582 z',
                })
            )
        ),
    'pic-line-dot': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            {
                width: '20.003906mm',
                height: '1.166015mm',
                viewBox: '0 0 20.003906 1.166015',
                version: '1.1',
                id: 'ppic-line-dot-svg5',
                preserveAspectRatio,
            },
            createElement('defs', { id: 'ppic-line-dot-defs2' }),
            createElement(
                'g',
                {
                    id: 'ppic-line-dot-layer1',
                    transform: 'translate(-43.199219,-88.964844)',
                },
                createElement('path', {
                    id: 'path890-3-7-1-8',
                    d: 'm 48.587891,88.972656 v 1.158203 h 1.140625 v -1.158203 z',
                }),
                createElement('path', {
                    id: 'path890-3-7-1-5-25',
                    d: 'm 51.283203,88.972656 v 1.158203 h 1.140625 v -1.158203 z',
                }),
                createElement('path', {
                    id: 'path890-3-7-1-8-0',
                    d: 'm 43.199219,88.972656 v 1.158203 h 1.138672 v -1.158203 z',
                }),
                createElement('path', {
                    id: 'path890-3-7-1-5-25-2',
                    d: 'm 45.894531,88.972656 v 1.158203 h 1.138672 v -1.158203 z',
                }),
                createElement('path', {
                    id: 'path890-3-7-1-9-4',
                    d: 'm 59.369141,88.972656 v 1.158203 h 1.138671 v -1.158203 z',
                }),
                createElement('path', {
                    id: 'path890-3-7-1-5-2-9',
                    d: 'm 62.0625,88.972656 v 1.158203 h 1.140625 v -1.158203 z',
                }),
                createElement('path', {
                    id: 'path890-3-7-1-8-0-0',
                    d: 'm 53.978516,88.964844 v 1.158203 h 1.138671 v -1.158203 z',
                }),
                createElement('path', {
                    id: 'path890-3-7-1-5-25-2-1',
                    d: 'm 56.673828,88.964844 v 1.158203 H 57.8125 v -1.158203 z',
                })
            )
        ),
    'pic-line-dash-dot': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            {
                width: '20.003906mm',
                height: '1.158203mm',
                viewBox: '0 0 20.003906 1.158203',
                version: '1.1',
                id: 'ppic-line-dash-dot-svg5',
                preserveAspectRatio,
            },
            createElement('defs', { id: 'ppic-line-dash-dot-defs2' }),
            createElement(
                'g',
                {
                    id: 'ppic-line-dash-dot-layer1',
                    transform: 'translate(-43.199219,-98.236328)',
                },
                createElement('path', {
                    id: 'path890-0-9-4-9',
                    d: 'm 51.226562,98.236328 v 1.158203 h 3.947266 v -1.158203 z',
                }),
                createElement('path', {
                    id: 'path890-0-9-5',
                    d: 'm 43.199219,98.236328 v 1.158203 h 3.947265 v -1.158203 z',
                }),
                createElement('path', {
                    id: 'path890-3-7-1-7',
                    d: 'm 48.617187,98.236328 v 1.158203 h 1.138672 v -1.158203 z',
                }),
                createElement('path', {
                    id: 'path890-3-7-1-7-4',
                    d: 'm 56.644531,98.236328 v 1.158203 h 1.140625 v -1.158203 z',
                }),
                createElement('path', {
                    id: 'path890-0-9-4-9-5',
                    d: 'm 59.253906,98.236328 v 1.158203 h 3.949219 v -1.158203 z',
                })
            )
        ),
    'pic-line-line': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            {
                width: '20.009766mm',
                height: '1.158203mm',
                viewBox: '0 0 20.009765 1.158203',
                version: '1.1',
                id: 'ppic-line-line-svg5',
                preserveAspectRatio,
            },
            createElement('defs', { id: 'ppic-line-line-defs2' }),
            createElement(
                'g',
                {
                    id: 'ppic-line-line-layer1',
                    transform: 'translate(-43.199219,-84.339844)',
                },
                createElement('path', {
                    id: 'path890-8',
                    d: 'm 43.199219,84.339844 v 1.158203 h 20.009765 v -1.158203 z',
                })
            )
        ),
};

type Picto = typeof pictos;
export type PictoKey = keyof Picto;

export const renderPicto = (
    key: PictoKey,
    preserveAspectRatio = 'xMidYMid' as PreserveAspectRatio
) =>
    DIV(
        { className: `picto-compose ${key}`, key },
        pictos[key](preserveAspectRatio)
    );

export default renderPicto;
