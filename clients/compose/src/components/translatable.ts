/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { ReactNode } from 'react';

import { getLang, setLang } from 'sdi/app';
import tr from 'sdi/locale';
import { ILayerInfo, MessageRecord, MessageRecordLang } from 'sdi/source';
import { DIV, NodeOrOptional } from 'sdi/components/elements';

import { makeIcon } from './button';

export type EditableState = {
    [k: string]: {
        isFirstEditing: boolean;
    };
};

const sl = (lc: MessageRecordLang) => () => {
    setLang(lc);
};

const isTranslated = (rec: MessageRecord) => {
    // FIXME(pm) I can't really work on this now, but oh boy this is ugly
    if (rec.fr && rec.fr.length > 0 && rec.nl && rec.nl.length > 0) {
        return true;
    }
    return false;
};

const langSwitch = () => {
    const lc = getLang();
    switch (lc) {
        case 'fr':
            return sl('nl');
        case 'nl':
            return sl('fr');
        default:
            return sl('fr');
    }
};

const trButton = makeIcon('translate', 3, 'comment', {
    position: 'top-left',
    text: () => tr.core('language'),
});

const wrapEditableImpl = (
    rec: MessageRecord,
    k: string,
    child: ReactNode,
    kind: 'input' | 'textarea'
) =>
    DIV(
        {
            className: `translatable ${kind}`,
            key: k,
        },
        child,
        trButton(
            langSwitch(),
            isTranslated(rec) ? 'translated' : 'not-translated'
        )
    );

export const wrapTranslatableInput = (
    rec: MessageRecord,
    k: string,
    child: ReactNode
) => wrapEditableImpl(rec, k, child, 'input');

export const wrapTranslatableText = (
    rec: MessageRecord,
    k: string,
    child: ReactNode
) => wrapEditableImpl(rec, k, child, 'textarea');

// not better than the "isTranslated" above but it will do the job for now...
const isTranslatedFeatureView = (info: ILayerInfo) => {
    if (info.featureViewOptions.type === 'default') {
        return false;
    } else {
        const translatedIn = getLang() === 'fr' ? 'nl' : 'fr';
        return info.featureViewOptions.rows.filter(r => r.lang === translatedIn)
            .length > 0
            ? true
            : false;
    }
};

export const wrapTranslatableFeatureView = (
    key: string,
    child: NodeOrOptional,
    info: ILayerInfo
) =>
    DIV(
        {
            className: `translatable textarea`,
            key,
        },
        child,
        trButton(
            langSwitch(),
            isTranslatedFeatureView(info) ? 'translated' : 'not-translated'
        )
    );
