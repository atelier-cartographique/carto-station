import { fromNullable } from 'fp-ts/lib/Option';

import { ILayerInfo } from 'sdi/source';
import { DIV } from 'sdi/components/elements';
import featureView from 'sdi/components/feature-view';
import timeserie from 'sdi/components/timeserie';

import {
    getCurrentLayerId,
    getCurrentFeature,
    getCurrentSynteticLayerInfoOption,
} from '../queries/app';
import { dispatchTimeserie, loadData } from '../events/timeserie';
import { getData, queryTimeserie } from '../queries/timeserie';
import tr from 'sdi/locale';
import { wrapTranslatableFeatureView } from './translatable';

const tsPlotter = timeserie(
    queryTimeserie,
    getData,
    getCurrentLayerId,
    dispatchTimeserie,
    loadData
);
const noView = () => DIV('feature-view');

const withInfo = (info: ILayerInfo) =>
    fromNullable(getCurrentFeature()).fold(noView(), feature =>
        featureView(info.featureViewOptions, feature, tsPlotter)
    );

const render = () =>
    getCurrentSynteticLayerInfoOption().fold(noView(), ({ info }) =>
        DIV(
            'feature-preview',
            wrapTranslatableFeatureView(
                'feature-preview-wrapper',
                DIV('column__title', tr.compose('featurePreview')),
                info
            ),
            withInfo(info)
        )
    );

export default render;
