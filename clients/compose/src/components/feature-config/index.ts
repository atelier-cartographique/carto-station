/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { DIV, SPAN } from 'sdi/components/elements';
import tr from 'sdi/locale';

import { setLayout } from '../../events/app';

import { AppLayout } from '../../shape/types';
import { makeIcon } from '../button';
import config from './config';
import { ensureTableSelection } from 'compose/src/events/feature-config';
import { icon } from 'sdi/components/button';

const logger = debug('sdi:feature-config');

export interface FeatureConfig {
    currentRow: number;
    editedValue: string | null;
}

export const initialFeatureConfigState = (): FeatureConfig => ({
    currentRow: -1,
    editedValue: null,
});

const closeButton = makeIcon('close', 3, 'times', {
    text: () => tr.compose('backToMap'),
    position: 'left',
});

const renderHeader = () =>
    DIV(
        'app-split-header',
        DIV(
            'app-split-title',
            icon('pencil-alt'),
            SPAN('editor-title', tr.compose('displayFeatureEdit'))
        ),

        DIV(
            'app-header-close',
            closeButton(() => {
                setLayout(AppLayout.LegendEditor);
            })
        )
    );

const render = () => {
    ensureTableSelection();
    return DIV('app-split-wrapper feature-config', renderHeader(), config());
};

export default render;

logger('loaded');
