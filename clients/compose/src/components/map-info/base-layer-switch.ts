/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { DETAILS, DIV, H2, H3, SUMMARY } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import {
    IBaseLayerGroup,
    IMapBaseLayer,
    IMapBaselayerOrGroup,
} from 'sdi/source';

import {
    getBaseLayersForService,
    getBaseLayerServices,
    getBaseLayerGroups,
    layerOrGroupName,
    getCurrentBaseLayersOrGroups,
} from '../../queries/app';
import {
    addMapBaseLayerGroup,
    addMapBaseLayerInSelection,
    delGroupLayerInSelection,
    delMapBaseLayerInSelection,
    setMapBaseLayerOrGroup,
    setoidBaseLayerOrGroup,
} from '../../events/app';
import { renderCheckboxLeft, renderSelect } from 'sdi/components/input';
import {
    getCurrentBaseLayerSelection,
    getCurrentBaseLayerSelectionIds,
    isBaseLayerSelected,
} from 'compose/src/queries/map-info';
import { notEmpty } from 'sdi/util';

const logger = debug('sdi:map-info/base-layer-switch');

const renderBaseLayer = (baseLayer: IMapBaseLayer) =>
    renderCheckboxLeft(
        'base-layer-item',
        () => fromRecord(baseLayer.name),
        select =>
            select
                ? addMapBaseLayerInSelection(baseLayer.id)
                : delMapBaseLayerInSelection(baseLayer.id)
    )(isBaseLayerSelected(baseLayer));

const renderGroup = (group: IBaseLayerGroup) => {
    const selected = getCurrentBaseLayerSelectionIds().includes(group.id);
    return renderCheckboxLeft(
        'base-layer-item',
        () => fromRecord(group.name),
        select =>
            select
                ? addMapBaseLayerGroup(group.id)
                : delGroupLayerInSelection(group.id)
    )(selected);
};
const renderGroups = () =>
    notEmpty(getBaseLayerGroups()).map(groups =>
        DETAILS(
            { className: 'webservice', key: `renderService-groups` },
            SUMMARY(
                '',
                DIV(
                    { className: 'webservice-name' },
                    tr.compose('baseLayerGroup')
                )
            ),
            ...groups.map(renderGroup)
        )
    );

const renderLayerList = (service: string) =>
    DIV(
        'base-layer-list',
        ...getBaseLayersForService(service).map(bl => renderBaseLayer(bl))
    );

const renderService = (service: string) =>
    DETAILS(
        { className: 'webservice', key: `renderService-${service}` },
        SUMMARY('', DIV({ className: 'webservice-name' }, service)),
        renderLayerList(service)
    );

const renderDefault = () =>
    DIV(
        'selected-base-layers',
        H3('', tr.compose('defaultBaseLayersTitle')),
        renderSelect<IMapBaselayerOrGroup>(
            'select-base-layer',
            layerOrGroupName,
            setMapBaseLayerOrGroup,
            setoidBaseLayerOrGroup()
        )(getCurrentBaseLayerSelection(), getCurrentBaseLayersOrGroups())
    );

const render = () =>
    DIV(
        'basemap-select',
        H2('', tr.compose('changeBackgroundMap')),
        renderDefault(),
        DIV(
            'available-base-layers',
            H3('', tr.compose('availableBaselayersTitle')),
            DIV('helptext', tr.compose('selectBaselayesr')),
            getBaseLayerServices().map(renderService),
            renderGroups()
        )
    );

export default render;

logger('loaded');
