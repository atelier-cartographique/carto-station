/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { FormEvent } from 'react';

import { DIV, H1, IMG, INPUT, SPAN } from 'sdi/components/elements';
import { IMapInfo, makeRecord } from 'sdi/source';
import tr, { formatDate, fromRecord } from 'sdi/locale';
import { getLang } from 'sdi/app';

import { getMapInfo } from '../../queries/app';
import {
    toDataURL,
    removeMapInfoIllustration,
    setMapTitle,
    setMapDescription,
} from '../../events/app';
import mapInfoQueries from '../../queries/map-info';
import mapInfoEvents from '../../events/map-info';
import { wrapTranslatableText } from '../translatable';
import { DataUrl, MapInfoIllustrationState } from '../../shape/types';
import { makeLabel, makeRemove } from '../button';
import { inputLongText } from 'sdi/components/input';
import { editRecord } from '../legend-editor';

const logger = debug('sdi:map-info/info');

let selectedImage: File | null;
let selectedImageDataUrl: DataUrl | null;

const getInfo = <T>(a: (b: IMapInfo) => T, c: T): T => {
    const minfo = getMapInfo();
    if (minfo) {
        return a(minfo);
    }
    return c;
};

const getTitle = () => getInfo(m => m.title, makeRecord());

const getDescription = () => getInfo(m => m.description, makeRecord());

// const toP = (p: string) => P({}, p);

const uploadButton = makeLabel('upload', 1, () => tr.compose('validate'));

// const formatTitle: HFn<HTMLElement> = (props, title) => {
//     const t = `${title}`;
//     const isEmpty = t.trim().length === 0;
//     const text = isEmpty ? tr.compose('emptyMapTitle') : t;
//     return H1(props, text);
// };

// const formatDescription: HFn<HTMLElement> = (props, description) => {
//     const d = `${description}`;
//     const isEmpty = d.trim().length === 0;
//     const elems = isEmpty
//         ? tr.compose('emptyMapDescription')
//         : d.split('\n').map(toP);
//     const html = renderToStaticMarkup(DIV({}, ...elems));
//     return DIV({
//         className: 'map-description',
//         dangerouslySetInnerHTML: { __html: html },
//         ...props,
//     });
// };

const clearSelectedImage = () => {
    selectedImage = null;
    selectedImageDataUrl = null;
    mapInfoEvents.showImg();
};

const uploadSelectedImage = () => {
    if (selectedImage !== null) {
        mapInfoEvents.uploadImg(selectedImage);
    }
};

const setSelectedImage = (img: File) => {
    selectedImageDataUrl = null;
    selectedImage = img;

    toDataURL(selectedImage)
        .then((dataUrl: DataUrl) => {
            selectedImageDataUrl = dataUrl;
            mapInfoEvents.showSelectedImg();
        })
        .catch(() => mapInfoEvents.showImg());

    mapInfoEvents.generatingSelectedImgPreview();
};

const renderMapIllustrationToolbar = (src: string | undefined) => {
    const state = mapInfoQueries.getState();

    if (state === MapInfoIllustrationState.showSelectedImage) {
        const clearPreviewButton = makeRemove(
            `renderMapIllustrationToolbar-clear-${src}`,
            2,
            () => tr.compose('cancel'),
            () => tr.compose('rmvMsgRemoveImage')
        )(() => clearSelectedImage());

        const label = DIV({ className: 'label' }, tr.compose('imagePreview'));

        const validatePreviewButton = uploadButton(() => uploadSelectedImage());

        return DIV(
            'uploader-wrapper',
            clearPreviewButton,
            label,
            validatePreviewButton
        );
    } else if (
        state === MapInfoIllustrationState.generateSelectedImagePreview
    ) {
        const label = DIV('label', tr.compose('imageGeneratingPreview'));
        return DIV(
            'uploader-wrapper',
            SPAN({ className: 'loader-spinner' }),
            label
        );
    } else if (state === MapInfoIllustrationState.uploadSelectedImage) {
        const label = DIV({ className: 'label' }, tr.compose('imageUploading'));
        return DIV(
            'uploader-wrapper',
            SPAN({ className: 'loader-spinner' }),
            label
        );
    } else {
        const uploadField = INPUT({
            type: 'file',
            name: 'map-info-illustration-image',
            onChange: (e: FormEvent<HTMLInputElement>) => {
                if (
                    e &&
                    e.currentTarget.files &&
                    e.currentTarget.files.length > 0
                ) {
                    setSelectedImage(e.currentTarget.files[0]);
                } else {
                    clearSelectedImage();
                }
            },
        });

        if (src && src !== '') {
            const removeIllustrationButton = makeRemove(
                `renderMapIllustrationToolbar-remove-${src}`,
                2,
                () => tr.compose('remove'),
                () => tr.compose('rmvMsgRemoveImage')
            )(() => removeMapInfoIllustration());

            const label = DIV(
                { className: 'label' },
                tr.compose('mapInfoChangeIllustration')
            );

            return DIV(
                'uploader-wrapper',
                removeIllustrationButton,
                label,
                uploadField
            );
        } else {
            const label = DIV('label', tr.compose('mapInfoAddIllustration'));

            return DIV({ className: 'uploader-wrapper' }, label, uploadField);
        }
    }
};

const renderMapIllustrationImg = (src: DataUrl | string | undefined | null) => {
    const state = mapInfoQueries.getState();
    let className = '';

    if (
        state === MapInfoIllustrationState.showSelectedImage ||
        state === MapInfoIllustrationState.uploadSelectedImage
    ) {
        className = 'preview';
        src = selectedImageDataUrl;
        if (src) {
            return DIV('map-illustration', IMG({ className, src, alt: '' }));
        } else {
            return DIV('map-illustration', IMG({ className, alt: '' }));
        }
    } else if (
        state === MapInfoIllustrationState.generateSelectedImagePreview
    ) {
        return DIV({ className: 'empty-image' }, '');
    } else if (src && src !== '') {
        return DIV('map-illustration', IMG({ className, src, alt: '' }));
    } else {
        return DIV('empty-image', '');
    }
};

const renderMapIllustration = (url: string | undefined) => {
    return DIV(
        'map-illustration__wrapper',
        renderMapIllustrationImg(`${url}?size=large`),
        renderMapIllustrationToolbar(url)
        // '~ADD CAPTION INPUT '
    );
};

const inputTitle = (key: string) =>
    H1(
        '',
        wrapTranslatableText(
            getTitle(),
            key,
            inputLongText(
                () => fromRecord(getTitle()),
                t => editRecord(getTitle, setMapTitle, t),
                {
                    className: key,
                    key: `${key}-${getLang()}`,
                    placeholder: tr.compose('emptyMapTitle'),
                    rows: 3,
                }
            )
        )
    );

const inputDescription = (key: string, placeholder: string) =>
    wrapTranslatableText(
        getDescription(),
        key,
        inputLongText(
            () => fromRecord(getDescription()),
            d => editRecord(getDescription, setMapDescription, d),
            {
                className: key,
                key: `${key}-${getLang()}`,
                placeholder,
                rows: 12,
            }
        )
    );

const render = (mapInfo: IMapInfo) =>
    DIV(
        'map-infos',
        // mapPublishingTools(mapInfo),
        inputTitle(`map_info_title`),
        // editable(`map_info_title`, getTitle, setMapTitle, formatTitle)(),

        DIV(
            'map-date',
            DIV('map-date-label', tr.compose('lastModified')),
            DIV('map-date-value', formatDate(new Date(mapInfo.lastModified)))
        ),

        renderMapIllustration(mapInfo.imageUrl),

        inputDescription(
            `map_info_description`,
            tr.compose('emptyMapDescription')
        )
    );

export default render;

logger('loaded');
