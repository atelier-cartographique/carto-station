/*
 *  Copyright (C) 2019 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Option, some } from 'fp-ts/lib/Option';
import { DIV, A, TABLE, TR, TD, H2, H3, SPAN } from 'sdi/components/elements';
import { helpText } from 'sdi/components/helptext';
import { IMapInfo, MapStatus } from 'sdi/source';
import tr from 'sdi/locale';
import { getRoot } from 'sdi/app';

import { makeRemoveLabelAndIcon, makeRemove, makeRemoveIcon } from '../button';
import mapInfoEvents from '../../events/map-info';
import { deleteAllMapVersionsFromId, deleteMapFromId } from '../../events/app';
import { renderCategories } from './map-category';
import {
    getVersion,
    getVersionOf,
    hasPublishedVersion,
} from 'compose/src/queries/app';
import { datetimeBEFormated } from 'sdi/util';
import { navigateMap } from '../../events/route';
import { icon } from 'sdi/components/button';

const renderPublishRemove = () =>
    DIV(
        '',
        DIV('', tr.compose('warningPublishRemoving')),
        tr.compose('publishedOverwriteMore'),
        icon('archive'),
        tr.compose('publishedOverwriteMore2')
    );

const publishBtn = makeRemove(
    'publish',
    1,
    // 'users',
    () => tr.compose('compose:publishMap'),
    () => renderPublishRemove()
);
const unpublishBtn = makeRemove(
    'unpublish',
    2,
    () => tr.compose('compose:unpublishMap'),
    () => tr.compose('warningPublishRemoving')
);
// const overwritePublishBtn = makeRemoveLabelAndIcon(
//     'publish-again',
//     1,
//     'users',
//     // () => tr.compose('compose:overwritePublicMap'),
//     // () => tr.compose('warningPublishedOverwrite')
//     () => tr.compose('compose:publishMap'),
//     () => tr.compose('warningPublishedOverwrite')
// );
const draftToSavedBtn = makeRemoveIcon(
    'save-draft',
    3,
    'archive',
    () => tr.compose('warningSavedOverwrite'),
    {
        position: 'top-left',
        text: () => tr.compose('draftToSaved'),
    }
);
const publishedToSavedBtn = makeRemoveIcon(
    'save-published',
    3,
    'archive',
    () => tr.compose('warningSavedOverwrite'),
    {
        position: 'top-left',
        text: () => tr.compose('publishedToSaved'),
    }
);
const savedToDraftBtn = makeRemoveIcon(
    'edit-saved',
    3,
    'pencil-alt',
    () => tr.compose('warningDraftRemoving'),
    {
        position: 'top-left',
        text: () => tr.compose('backToSavedMap'),
    }
);
const publishedToDraftBtn = makeRemoveIcon(
    'edit-published',
    3,
    'pencil-alt',
    () => tr.compose('warningDraftRemoving'),
    {
        position: 'top-left',
        text: () => tr.compose('backToPublishedMap'),
    }
);

// const renderStatus = (info: IMapInfo) => {
//     if (hasPublishedVersion(info)) {
//         return DIV(
//             '',
//             overwritePublishBtn(() => mapInfoEvents.mapStatus('published')),
//             unpublishBtn(() => {
//                 // mapInfoEvents.mapStatus('draft')
//                 getVersion('published').map(toDel =>
//                     deleteMapFromId(toDel.id as string)
//                 );
//             })
//         );
//     }
//     return publishBtn(() => mapInfoEvents.mapStatus('published'));
// };

const renderUnpublish = (info: IMapInfo) => {
    if (hasPublishedVersion(info)) {
        return unpublishBtn(() => {
            // mapInfoEvents.mapStatus('draft')
            getVersion('published').map(toDel =>
                deleteMapFromId(toDel.id as string)
            );
        });
    }
    return '';
};

const renderPublish = (info: IMapInfo) => {
    if (hasPublishedVersion(info)) {
        // return overwritePublishBtn(() => mapInfoEvents.mapStatus('published'));
        return publishBtn(() => mapInfoEvents.mapStatus('published'));
    }
    return publishBtn(() => mapInfoEvents.mapStatus('published'));
};

const renderStatusHelptext = (info: IMapInfo) => {
    if (hasPublishedVersion(info)) {
        return helpText(tr.compose('compose:helptext:mapPublished'));
    }
    return helpText(tr.compose('compose:helptext:mapUnpublished'));
};

// const renderPreviewLink = ({ id }: IMapInfo) =>
//     DIV(
//         'preview-link',
//         A(
//             {
//                 title: tr.compose('preview'),
//                 target: '_blank',
//                 href: `${getRoot()}view/${id}`,
//             },
//             tr.compose('preview')
//         )
//     );

const renderRemove = (mapInfo: IMapInfo) =>
    DIV(
        'remove-map',
        makeRemoveLabelAndIcon(
            `remove-map-${mapInfo.id}`,
            2,
            'trash',
            () => tr.compose('compose:removeMap'),
            () => tr.compose('rmvMsgRemoveMap')
        )(() => deleteAllMapVersionsFromId(mapInfo.id as string))
    );

const versionHelper = (mapStatus: MapStatus) => {
    switch (mapStatus) {
        case 'draft':
            return tr.compose('draftVersionHelper');
        case 'published':
            return tr.compose('publishedVersionHelper');
        case 'saved':
            return tr.compose('savedVersionHelper');
    }
};

const versionTitle = (mapStatus: MapStatus, mapOpt: Option<IMapInfo>) =>
    DIV(
        'version-title',
        DIV('label', tr.compose(mapStatus)),
        DIV('helptext', versionHelper(mapStatus)),
        mapOpt.map(m =>
            DIV('version-date', datetimeBEFormated(new Date(m.lastModified)))
        )
    );

const actionClass = (mapInfo: IMapInfo, status: MapStatus) =>
    getVersionOf(mapInfo, status).isSome() ? '' : 'disabled';

const previewLinkTD = (mapInfo: IMapInfo, mapStatus: MapStatus) =>
    TD(
        'preview-link',
        getVersionOf(mapInfo, mapStatus).fold(icon('eye', 'disabled'), info =>
            A(
                {
                    title: tr.compose('preview'),
                    target: '_blank',
                    href: `${getRoot()}view/${info.id}`,
                },
                icon('eye')
            )
        )
    );

const versionsActions = (mapInfo: IMapInfo) =>
    DIV(
        'versions__wrapper actions',
        TABLE(
            '',
            TR(
                `saved ${actionClass(mapInfo, 'saved')}`,
                TD('', icon('archive')),
                TD('version', versionTitle('saved', getVersion('saved'))),

                previewLinkTD(mapInfo, 'saved'),
                TD('save-version', '-'),
                TD(
                    'edit',
                    savedToDraftBtn(() => {
                        mapInfoEvents.fromSavedToDraft();
                        navigateMap(mapInfo.id);
                    })
                ),
                TD('', '-')
            ),
            TR(
                `draft ${actionClass(mapInfo, 'draft')}`,
                TD('', icon('pencil-alt')),
                TD('version', versionTitle('draft', some(mapInfo))),
                previewLinkTD(mapInfo, 'draft'),
                TD(
                    'save-version',
                    draftToSavedBtn(() => mapInfoEvents.mapStatus('saved'))
                ),
                TD('edit', '-'),
                TD('', renderPublish(mapInfo))
            ),
            TR(
                `published ${actionClass(mapInfo, 'published')}`,
                TD('', icon('users')),
                TD(
                    'version',
                    versionTitle('published', getVersion('published'))
                ),
                previewLinkTD(mapInfo, 'published'),
                TD(
                    'save-version',
                    publishedToSavedBtn(() => {
                        mapInfoEvents.fromPublishedToSaved();
                    })
                ),
                TD(
                    'edit',
                    publishedToDraftBtn(() => {
                        mapInfoEvents.fromPublishedToDraft();
                        navigateMap(mapInfo.id);
                    })
                ),
                TD('', renderUnpublish(mapInfo))
            )
        )
    );

const renderPublicationStatus = (mapInfo: IMapInfo) =>
    hasPublishedVersion(mapInfo)
        ? DIV(
              `map-status published`,
              icon('user-plus'),
              mapInfo.inPublicGroup
                  ? SPAN(`status-label `, tr.core('public_published'))
                  : SPAN(`status-label `, tr.core('internally_published'))
          )
        : DIV(
              `map-status draft`,
              icon('lock'),
              SPAN(`status-label `, tr.core('private'))
          );

const mapPublishingTools = (mapInfo: IMapInfo) =>
    DIV(
        'map-publishing-tools',
        H2('', tr.compose('publishingOptions')),

        DIV(
            'tab__section',
            versionsActions(mapInfo),
            DIV(
                'publication-status__wrapper',
                renderPublicationStatus(mapInfo),
                renderStatusHelptext(mapInfo)
            ),
            renderRemove(mapInfo)
        ),

        DIV(
            'tab__section',
            H3('', tr.compose('compose:mapCategory')),
            renderCategories(mapInfo)
        )
    );

export default mapPublishingTools;

// const hasCategory =
//     (c: string, info: IMapInfo) => info.categories.indexOf(c) >= 0;

// const renderCategories =
//     (info: IMapInfo) => {
//         const categories = queries.getCategories();
//         const elements = categories.map((cat) => {
//             if (hasCategory(cat.id, info)) {
//                 return (
//                     DIV({
//                         className: 'category selected interactive',
//                         onClick: () => events.removeCategory(cat.id),
//                     }, fromRecord(cat.name)));
//             }
//             return (
//                 DIV({
//                     className: 'category interactive',
//                     onClick: () => events.addCategory(cat.id),
//                 }, fromRecord(cat.name)));
//         });

//         return (
//             DIV({ className: 'category-wrapper' }, ...elements));
//     };
