/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { BUTTON, DIV, NodeOrOptional } from 'sdi/components/elements';
import { IMapInfo } from 'sdi/source';
import tr, { Translated } from 'sdi/locale';

import { getMapInfoOpt, getSelectedTab } from '../../queries/app';
import { renderLinkInfo } from '../link-map';
import { selectTab } from '../../events/app';
import { TabChoice } from '../../shape/types';
import info from './info';
import attachments from './attachments';
import legend, { renderListForFeatureView } from './legend';
import mapPublishingTools from './publishing-tools';
import baseLayerSwitch from './base-layer-switch';
import { icon } from 'sdi/components/button';

const logger = debug('sdi:map-info');

// const sideBarHeader = (mi: IMapInfo) =>
//     DIV({ className: 'sidebar-header' }, mapPublishingTools(mi));

// const sideBarMain = (mi: IMapInfo) =>
//     DIV(
//         { className: 'sidebar-main' },
//         info(mi),
//         renderLinkInfo(),
//         attachments(mi),
//         legend(mi)
//     );

const btnTab = (label: Translated, onClick: () => void, className: string) =>
    BUTTON(
        {
            className,
            onClick,
        },
        icon('circle'),
        label
    );
const isSelectedClass = (tab: TabChoice) =>
    getSelectedTab() === tab ? 'step current' : 'step';

const renderTabHeader = () => {
    return DIV(
        'steps__list',
        btnTab(
            tr.compose('editorial'),
            () => selectTab('editorial'),
            `${isSelectedClass('editorial')}`
        ),
        btnTab(
            tr.compose('tabLegend'),
            () => selectTab('layers'),
            `${isSelectedClass('layers')}`
        ),
        btnTab(
            tr.compose('tabBaseLayer'),
            () => selectTab('baselayers'),
            `${isSelectedClass('baselayers')}`
        ),
        btnTab(
            tr.compose('tabFeature'),
            () => selectTab('feature-info'),
            `${isSelectedClass('feature-info')}`
        ),
        btnTab(
            tr.compose('relations'),
            () => selectTab('relations'),
            `${isSelectedClass('relations')}`
        ),
        btnTab(
            tr.compose('publication'),
            () => selectTab('publication'),
            `${isSelectedClass('publication')}`
        )
    );
};

const renderTab = (content: NodeOrOptional) => DIV('tab__content', content);

const sideBarWithTabs = (mi: IMapInfo) => {
    switch (getSelectedTab()) {
        case 'editorial':
            return renderTab(info(mi));
        case 'layers':
            return renderTab(legend(mi));
        case 'baselayers':
            return renderTab(baseLayerSwitch());
        case 'relations':
            return renderTab(DIV('', renderLinkInfo(), attachments(mi)));
        case 'publication':
            return renderTab(DIV('', mapPublishingTools(mi)));
        case 'feature-info':
            return renderTab(DIV('', renderListForFeatureView()));
    }
};

const render = () =>
    getMapInfoOpt().fold(DIV({ className: 'sidebar-right map-legend' }), mi =>
        DIV(
            { className: 'sidebar-right map-legend' },
            renderTabHeader(),
            sideBarWithTabs(mi)
        )
    );

export default render;

logger('loaded');
