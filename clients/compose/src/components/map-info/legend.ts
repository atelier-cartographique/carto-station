/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { fromNullable, fromPredicate, none } from 'fp-ts/lib/Option';
import { setoidNumber } from 'fp-ts/lib/Setoid';

import {
    ILayerInfo,
    getMessageRecord,
    Inspire,
    IMapInfo,
    isSimple,
} from 'sdi/source';
import tr, { formatNumber, fromRecord, Translated } from 'sdi/locale';
import { DIV, H2, NodeOrOptional, SPAN } from 'sdi/components/elements';
import { divTooltipTop } from 'sdi/components/tooltip';
import legendPoint from 'sdi/components/legend/legend-point';
import legendLinestring from 'sdi/components/legend/legend-linestring';
import legendPolygon from 'sdi/components/legend/legend-polygon';
import { renderCheckbox, renderSelect } from 'sdi/components/input';
import { IconName } from 'sdi/components/button/names';
import {
    reasonableScaleList,
    scaleList,
    scaleToZoom,
    tryScale,
    tryZoom,
    zoomToRoundScale,
} from 'sdi/map';
import { tryNumber } from 'sdi/util';

import {
    getLayerData,
    getSynteticLayerInfoOption,
    getCurrentLayerId,
    getSelectedVisibilityOption,
    getLayerWithInfo,
    getMapInfoOpt,
    findLayerInfo,
} from '../../queries/app';
import { getDatasetMetadata, getPath } from '../../queries/metadata';
import { getLegendVisibility } from '../../queries/legend-editor';
import { getView } from '../../queries/map';
import {
    moveLayerDown,
    moveLayerUp,
    removeLayer,
    resetLegend,
    setLegendVisibility,
    setZoomRange,
} from '../../events/legend-editor';
import { clearCurrentStreamData } from '../../events/table';
import { resetFeatureConfigEditor } from '../../events/feature-config';
import {
    setLayerVisibility,
    setCurrentLayerId,
    setLayout,
    resetLegendEditor,
    selectCurrentLayer,
    toggleVisibilityOption,
    toggleLayerInfo,
} from '../../events/app';
import { AppLayout } from '../../shape/types';
import { makeIcon, makeLabelAndIcon, makeRemoveIcon } from '../button';
import featureView from '../feature-view';
import { identity } from 'fp-ts/lib/function';
import { getDomain } from '../../queries/metadata';
import { reverse } from 'fp-ts/lib/Array';
import { icon } from 'sdi/components/button';

const addButton = makeLabelAndIcon('add', 1, 'plus', () =>
    tr.compose('addLayer')
);

const upButton = makeIcon('move-up', 3, 'arrow-up', {
    position: 'top',
    text: () => tr.compose('moveLayerUp'),
});
const downButton = makeIcon('move-down', 3, 'arrow-down', {
    position: 'top',
    text: () => tr.compose('moveLayerDown'),
});
// const featureEditButton = makeIcon('edit', 3, 'list-alt', {
//     position: 'top-right',
//     text: () => tr.compose('displayFeatureEdit'),
// });
// const editButton = makeIcon('edit', 3, 'pencil');
// const resetLegendButton = makeRemoveIcon('reset', 3, 'refresh', () => tr.compose('rmvMsgResetLegend'));

const logger = debug('sdi:info/legend');

const renderLayerTitle = (info: ILayerInfo) => {
    const md = getDatasetMetadata(info.metadataId).fold(null, identity);
    const label =
        md === null
            ? ('' as Translated)
            : fromRecord(getMessageRecord(md.resourceTitle));

    const wrap = wrapItem(md, (): boolean => {
        const zoom = getView().zoom;
        const minZoom = fromNullable(info.minZoom).getOrElse(0);
        const maxZoom = fromNullable(info.maxZoom).getOrElse(30);

        return info.visible && zoom >= minZoom && zoom <= maxZoom;
    });

    return DIV(
        {
            className: 'table-name',
            title: label,
        },
        icon('table'),
        SPAN('label', ...wrap(label))

        // divTooltipTop(
        //     label,
        //     {},
        //     icon('table'),
        //     SPAN('label', ...wrap(label))
        // )
    );
};

const wrapItem =
    (md: Inspire | null, visible: () => boolean) =>
    (...nodes: React.ReactNode[]) =>
        fromNullable(md).fold(nodes, md =>
            getLayerData(md.uniqueResourceIdentifier).fold(
                err => [
                    SPAN(
                        {
                            className: 'load-error',
                            title: err,
                        },
                        ...nodes
                    ),
                ],
                o =>
                    o.fold(
                        nodes.concat(
                            SPAN({
                                className: visible() ? 'loader-spinner' : '',
                            })
                        ),
                        () => nodes
                    )
            )
        );

const renderLegendItem = (info: ILayerInfo): NodeOrOptional => {
    const md = getDatasetMetadata(info.metadataId);
    switch (info.style.kind) {
        case 'polygon-continuous':
        case 'polygon-discrete':
        case 'polygon-simple':
            return legendPolygon(info.style, info, md);
        case 'point-discrete':
        case 'point-simple':
        case 'point-continuous':
            return legendPoint(info.style, info, md);
        case 'line-simple':
        case 'line-discrete':
        case 'line-continuous':
            return legendLinestring(info.style, info, md);
        default:
            throw new Error('UnknownStyleKind');
    }
};

const renderVisible = (info: ILayerInfo) => {
    const isVisible = info.visible;
    const icon: IconName = isVisible ? 'eye' : 'eye-slash';
    const toolTipMessage = isVisible
        ? tr.compose('makeUnvisible')
        : tr.compose('makeVisible');
    const labelMessage = isVisible
        ? tr.compose('layerIsVisible')
        : tr.compose('layerIsHidden');
    const buttonFn = makeIcon('view', 3, icon, {
        position: 'top-right',
        text: toolTipMessage,
    });
    return DIV(
        'edit-layer-visibility',
        SPAN('label', labelMessage),
        buttonFn(() => setLayerVisibility(info.id, !isVisible))
    );
};

const handleFeatureEditButton = (info: ILayerInfo) => () => {
    setCurrentLayerId(info.id);
    resetFeatureConfigEditor();
    setLayout(AppLayout.FeatureConfig);
};

// const renderFeatureEditButton = (info: ILayerInfo) =>
//     DIV(
//         'edit-feature-infos',
//         featureEditButton(handleFeatureEditButton(info)),
//         SPAN('label', tr.compose('editFeatureLabel'))
//     );

const handleResetButton = (info: ILayerInfo) => () => {
    setCurrentLayerId(info.id);
    resetLegendEditor();
    resetLegend();
};

// const renderResetLegendButton = (info: ILayerInfo) => {
//     const button = makeRemoveLabelAndIcon(
//         `reset-layer-${info.id}`,
//         3,
//         'times',
//         () => tr.compose('resetLegend'),
//         () => tr.compose('rmvMsgResetLegend')
//         // { position: 'top', text: tr.compose('resetLegend') }
//     );
//     return button(handleResetButton(info));
// };

const renderResetLegendButton = (info: ILayerInfo) => {
    const button = makeRemoveIcon(
        `reset-layer-${info.id}`,
        3,
        'undo',
        () => tr.compose('rmvMsgResetLegend'),
        { position: 'top', text: tr.compose('resetLegend') }
    );
    return button(handleResetButton(info));
};

const maxScale = scaleList.reduce(
    (acc, v) => Math.min(acc, v),
    Number.POSITIVE_INFINITY
); //max uses min because scale is in fact the denominator of the scale
const minScale = scaleList.reduce(
    (acc, v) => Math.max(acc, v),
    Number.NEGATIVE_INFINITY
);

const renderScale = (scale: number) => {
    if (scale === minScale || scale === maxScale) {
        return tr.compose('noRestriction');
    }
    return `1/${formatNumber(scale)}`;
};

const renderZoomRange = (lid: string) => {
    const g = () => getSynteticLayerInfoOption(lid).map(s => s.info);

    const getMin = () => g().map(i => i.minZoom || 0);
    const getMinScale = () =>
        getMin()
            .chain(z => tryZoom(z).map(z => zoomToRoundScale(z)))
            .chain(s => tryNumber(s));

    const getMax = () => g().map(i => i.maxZoom || 30);
    const getMaxScale = () =>
        getMax()
            .chain(z => tryZoom(z).map(z => zoomToRoundScale(z)))
            .chain(s => tryNumber(s));

    const setMin = (n: number) => {
        tryScale(n).map(s =>
            setZoomRange(scaleToZoom(s), getMax().getOrElse(30))
        );
    };

    const setMax = (n: number) => {
        tryScale(n).map(s =>
            setZoomRange(getMin().getOrElse(0), scaleToZoom(s))
        );
    };

    const minScaleList = [minScale]
        .concat(reasonableScaleList)
        .filter((v, i, arr) => arr.indexOf(v) === i);
    const minSelect = renderSelect(
        `min-zoom-${lid}`,
        renderScale,
        setMin,
        setoidNumber
    );
    const maxScaleList = [maxScale]
        .concat(reasonableScaleList.reverse())
        .filter((v, i, arr) => arr.indexOf(v) === i);
    const maxSelect = renderSelect(
        `max-zoom-${lid}`,
        renderScale,
        setMax,
        setoidNumber
    );

    return DIV(
        'zoom-range__wrapper',
        DIV('zoom-range__label', tr.compose('zoomRangeLabel')),
        DIV(
            'zoom-range',
            divTooltipTop(
                tr.compose('tooltip:zoomMin'),
                { className: 'zoom-range-input-box' },
                DIV('label', tr.compose('minZoomShort')),
                minSelect(minScaleList, getMinScale())
                // minSelect(reasonableScaleList.concat(minScale), getMinScale()),
            ),
            divTooltipTop(
                tr.compose('tooltip:zoomMax'),
                { className: 'zoom-range-input-box' },
                DIV('label', tr.compose('maxZoomShort')),
                maxSelect(maxScaleList, getMaxScale())
            )
        )
    );
};

const renderDeleteButton = (info: ILayerInfo) => {
    const removeButton = makeRemoveIcon(
        `legend::renderDeleteButton-${info.id}`,
        3,
        'trash',
        () => tr.compose('rmvMsgDeletLegendItem'),
        { position: 'top-left', text: tr.compose('remove') }
    );
    return DIV(
        'remove-layer',
        removeButton(() => {
            removeLayer(info);
            setLayout(AppLayout.MapAndInfo);
        })
    );
};

const renderUpButton = (info: ILayerInfo) =>
    upButton(() => {
        moveLayerUp(info.id);
    });

const renderDownButton = (info: ILayerInfo) =>
    downButton(() => {
        moveLayerDown(info.id);
    });

const renderUpButtonDisabled = () => upButton(() => void 0, 'disabled');
const renderDownButtonDisabled = () => downButton(() => void 0, 'disabled');

const setLegendHidding = (hidding: boolean) =>
    hidding === true ? setLegendVisibility(false) : setLegendVisibility(true);
const legendVisibilityCheckbox = renderCheckbox(
    'legend-visibility-checkbox',
    () => tr.compose('hideLegend'),
    setLegendHidding
);

const renderOrder = (info: ILayerInfo, idx: number, len: number) => {
    const order: React.ReactNode[] = [];
    if (len === 1) {
        order.push();
    } else if (idx === 0) {
        order.push(renderUpButtonDisabled(), renderDownButton(info));
    } else if (idx === len - 1) {
        order.push(renderUpButton(info), renderDownButtonDisabled());
    } else {
        order.push(renderUpButton(info), renderDownButton(info));
    }
    return DIV('order-box', ...order);
};

const triggerToolsButton = makeIcon('edit', 3, 'cog', {
    position: 'left',
    text: () => tr.compose('visibilityOptions'),
});
const triggerInfoButton = makeIcon('info', 3, 'info-circle', {
    position: 'left',
    text: () => tr.core('layerInfo'),
});

const renderTools = (info: ILayerInfo) =>
    DIV(
        'legend-block-tools',
        DIV('column__title', icon('cog'), tr.compose('visibilityOptions')),
        renderVisible(info),
        renderZoomRange(info.id),
        legendVisibilityCheckbox(!getLegendVisibility())
    );

const renderInfo = (info: ILayerInfo) =>
    getDatasetMetadata(info.metadataId).map(metadata => {
        const rid = fromNullable(metadata.resourceIdentifier);
        // const label = fromRecord(getMessageRecord(metadata.resourceTitle));
        const source = `${getDomain(rid)}/${getPath(rid)}`;
        // const date = formatDate(new Date(metadata.metadataDate));

        return DIV(
            'legend-block-infos',
            DIV(
                'info source',
                SPAN('label', `${tr.compose('layer')} : `),
                SPAN('value', source)
            )
            // DIV(
            //     'info date',
            //     SPAN('label', `${tr.compose('date')} : `),
            //     SPAN('value', date)
            // )
        );
    });

const renderRemoveBtns = (info: ILayerInfo) =>
    DIV(
        'reset__wrapper',
        renderResetLegendButton(info),
        renderDeleteButton(info)
    );

const sameLayerPred = (layerId: string) =>
    fromPredicate<string>(lid => lid === layerId);

const renderVisibilityOptions = (info: ILayerInfo) =>
    getSelectedVisibilityOption().chain(selected =>
        sameLayerPred(info.id)(selected).map(() => renderTools(info))
    );

const renderLayerInfo = (info: ILayerInfo) =>
    getLayerWithInfo().chain(selected =>
        sameLayerPred(info.id)(selected).map(() => renderInfo(info))
    );

const renderLayer = (layerId: string, idx: number, layers: string[]) =>
    findLayerInfo(layerId).map(info => {
        const items = renderLegendItem(info);
        const legendLabel = isSimple(info.style)
            ? none
            : DIV('legend-label', fromNullable(info.legend).map(fromRecord));
        const active = getCurrentLayerId() === info.id ? 'active' : '';
        return DIV(
            {
                className: `legend-block ${active}`,
                // probably hacky : couldn't intergate handleEditLayer
                onClick: () => selectCurrentLayer(info.id),
            },
            DIV(
                'title__wrapper',
                renderLayerTitle(info),
                renderOrder(info, idx, layers.length),
                // featureEditButton(handleFeatureEditButton(info)),
                triggerToolsButton(() => toggleVisibilityOption(info.id)),
                triggerInfoButton(() => toggleLayerInfo(info.id)),
                renderRemoveBtns(info)
            ),
            renderLayerInfo(info),
            renderVisibilityOptions(info),
            DIV('legend-items__wrapper', legendLabel, items)
        );
    });
const renderAddButton = () => {
    return addButton(() => {
        clearCurrentStreamData();
        setLayout(AppLayout.LayerSelect);
    });
};

const renderLayerForFeatureView = (layerId: string) =>
    findLayerInfo(layerId).map(info => {
        const isActive = getCurrentLayerId() === info.id;
        const activeClassName = isActive ? 'active' : '';
        const withView = fromPredicate((active: boolean) => active);
        return DIV(
            {
                className: `legend-block ${activeClassName}`,
                onClick: handleFeatureEditButton(info),
            },
            DIV('title__wrapper', renderLayerTitle(info)),
            renderLegendItem(info),
            withView(isActive).map(featureView)
        );
    });

export const renderListForFeatureView = () =>
    getMapInfoOpt().map(mi =>
        DIV(
            'map-layers',
            H2({}, tr.compose('displayFeatureEdit')),
            DIV('helptext', tr.compose('helpText:featureEdit')),
            ...reverse(mi.layers).map(renderLayerForFeatureView)
        )
    );

const render = (mapInfo: IMapInfo) =>
    DIV(
        'map-layers',
        H2({}, tr.compose('mapLegend')),
        DIV(
            'description-wrapper',
            DIV('helptext', tr.compose('compose:addLayerInfo'))
        ),
        renderAddButton(),
        DIV(
            'legend-block__wrapper',
            ...reverse(mapInfo.layers).map(renderLayer)
        )
    );

export default render;

logger('loaded');
