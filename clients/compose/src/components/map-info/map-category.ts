/*
 *  Copyright (C) 2023 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// import * as debug from 'debug';
// import { FormEvent } from 'react';

import { DIV, SPAN } from 'sdi/components/elements';
import { Category, IMapInfo } from 'sdi/source';
import tr, { fromRecord } from 'sdi/locale';
import { setoidIdentified } from 'sdi/util';

import { getMapCategories, getUnselectedCategories } from '../../queries/app';
import { addCategory, removeCategory } from '../../events/app';
import { makeIcon } from '../button';
import { renderSelect } from 'sdi/components/input';
import { some } from 'fp-ts/lib/Option';

const categoryToString = (cat: Category) => fromRecord(cat.name);
const renderItem = (cat: Category) =>
    cat.id === PSEUDO_SELECTED_ID
        ? tr.compose('selectCategory')
        : categoryToString(cat);

const renderCatSelector = renderSelect(
    'select-category',
    renderItem,
    cat => addCategory(cat.id),
    setoidIdentified<Category>()
);

const removeCatBtn = makeIcon('clear', 3, 'times', {
    position: 'top-right',
    text: () => tr.core('remove'),
});

const PSEUDO_SELECTED_ID = 'pseudo-selected-category';

const catPseudoSelected: Category = {
    id: PSEUDO_SELECTED_ID,
    name: { fr: 'cat', nl: 'cat' },
};

export const renderCategories = (info: IMapInfo) => {
    const mapCategories = getMapCategories(info).map(cat =>
        DIV(
            'tag category selected interactive',
            removeCatBtn(() => removeCategory(cat.id)),
            SPAN('tag__value', fromRecord(cat.name))
        )
    );
    const otherCategories = renderCatSelector(
        [catPseudoSelected].concat(getUnselectedCategories(info)),
        some(catPseudoSelected)
    );

    return DIV(
        'category-wrapper',
        // H2('category-title', tr.compose('category')),
        DIV('tag__list', ...mapCategories),
        otherCategories
    );
};
