/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { fromPredicate } from 'fp-ts/lib/Either';
import { compose } from 'fp-ts/lib/function';

import tr, { fromRecord } from 'sdi/locale';
import { BUTTON, DIV, H1, H3, IMG, P, SPAN } from 'sdi/components/elements';
import { IMapInfo } from 'sdi/source';

import { getUserMaps, getVersionOf } from '../../queries/app';
import { newMap } from '../../events/app';
import { makeLabel } from '../button';
import { navigateMap } from '../../events/route';
import { stringToParagraphs, uniqIdentified } from 'sdi/util';
import { getUserId } from 'sdi/app';
import { fromNullable } from 'fp-ts/lib/Option';
import { sortMap } from '../../queries/map-info';
import { icon } from 'sdi/components/button';

const selectMap = (mid?: string) => () => {
    if (mid) {
        navigateMap(mid);
    }
};

const addMap = () => {
    newMap();
};

const mapTitle = (title: string) => H3('map-title', title);

// const mapStatus = (status: string) => SPAN(`status-label `, status);

const isGreaterThan100 = fromPredicate<string, string>(
    rec => rec.length >= 100,
    rec => rec
);

const trimDescription = (record: string) =>
    isGreaterThan100(record).fold(
        rec => rec,
        rec => `${rec.substr(0, 100)}...`
    );

const description = compose(
    (s: string) => P({}, s),
    trimDescription,
    fromRecord
);

const addButton = makeLabel('add', 1, () => tr.compose('createMap'));

const renderSidebar = () =>
    DIV(
        'sidebar',
        H1('dashboard-title', tr.compose('myMaps')),
        DIV('app-infos', stringToParagraphs(tr.compose('infoStudio'))),
        addButton(addMap)
    );

const mapImage = (imageUrl: string) =>
    IMG({ src: `${imageUrl}?size=small`, alt: '' });

const mapNoImage = () => DIV('no-image', icon('image'));

const imgWrapper = (map: IMapInfo) =>
    DIV('map-tile-img', fromNullable(map.imageUrl).foldL(mapNoImage, mapImage));

const renderPublicationStatus = (mapInfo: IMapInfo) =>
    getVersionOf(mapInfo, 'published').fold(
        DIV(
            `map-status draft`,
            icon('lock'),
            SPAN(`status-label `, tr.core('private'))
        ),

        pub =>
            DIV(
                `map-status published`,
                icon('user-plus'),
                pub.inPublicGroup
                    ? SPAN(`status-label `, tr.core('public_published'))
                    : SPAN(`status-label `, tr.core('internally_published'))
            )
    );

const renderMap = (map: IMapInfo) => {
    const mid = map.id;
    const title = fromRecord(map.title);
    return BUTTON(
        {
            key: mid,
            className: 'map-tile',
            onClick: selectMap(map.id),
        },
        getUserId().map(
            () => renderPublicationStatus(map)
            // DIV(
            //     `map-status ${map.status}`,
            //     renderStatusIcon(map),
            //     mapStatus(tr.compose(map.status))
            // )
        ),
        imgWrapper(map),
        mapTitle(title),
        description(map.description)
    );
};

const filteredWorkMaps = () =>
    uniqIdentified(getUserMaps()).filter(m => m.status === 'draft');

const renderMaps = () => {
    const maps = filteredWorkMaps();
    const sortedMaps = sortMap(maps);
    return sortedMaps.map(renderMap);
};

const render = () =>
    DIV(
        'dashboard-maps map-navigator ',
        renderSidebar(),
        DIV('map-list__wrapper', DIV('map-list', renderMaps()))
    );

export default render;
