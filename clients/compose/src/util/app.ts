/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { parse as parseUrl } from 'url';
import { none, Option } from 'fp-ts/lib/Option';
import { IShape } from 'sdi/shape';

import { tryNumber } from 'sdi/util';

const logger = debug('sdi:util/app');

const getNumberParameters = (
    query: { [key: string]: string | string[] | unknown },
    ...param: readonly string[]
): { [key: typeof param[number]]: Option<number> } =>
    param.reduce(
        (acc, val) => ({
            ...acc,
            val: val in query ? tryNumber(query[val]) : none,
        }),
        {}
    );

export const processQuery = (state: IShape) => {
    const location = document.location;
    const url = parseUrl(location.href, true);

    if (url.query) {
        const q = url.query;

        // // Current User
        // if ('user' in q) {
        //     state['app/user'] = q.user;
        // }

        // Map View State
        const mapView = state['port/map/view'];
        if ('srs' in q && q.srs === mapView.srs) {
            const coord = getNumberParameters(q, 'lat', 'lon');
            coord.lat.chain(lat =>
                coord.lon.map(lon => (mapView.center = [lon, lat]))
            );
            getNumberParameters(q, 'zoom').zoom.map(z => (mapView.zoom = z));
            getNumberParameters(q, 'rotation').rotation.map(
                r => (mapView.rotation = r)
            );

            state['port/map/view'] = mapView;
        }
    }
};

logger('loaded');
