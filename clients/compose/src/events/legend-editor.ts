/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { fromNullable } from 'fp-ts/lib/Option';

import { assign, dispatch } from 'sdi/shape';
import { getLang } from 'sdi/app';
import {
    addDefaultGroupStyle,
    addDefaultIntervalStyle,
    ContinuousInterval,
    defaultStyle,
    DiscreteGroup,
    getGroup,
    getInterval,
    ILayerInfo,
    isContinuous,
    isDiscrete,
    isLabeled,
    isLineStyle,
    isMarkered,
    isPolygonStyle,
    isSimple,
    LineDiscreteGroup,
    LineInterval,
    MessageRecord,
    PointDiscreteGroup,
    LabelStyle,
    PointMarker,
    PointStyleConfig,
    PolygonDiscreteGroup,
    PolygonInterval,
    StyleConfig,
    PatternAngle,
    makeRecord,
    SubType,
    PatternStyle,
    isPointSimple,
} from 'sdi/source';

import {
    getCurrentLayerId,
    getCurrentLayerIdOpt,
    getCurrentLayerInfo,
} from '../queries/app';
import {
    getFirstGroupColor,
    getGeometryType,
    getLastGroupColor,
    getSelectedMainName,
} from '../queries/legend-editor';

import { clearDataInterval } from './legend-editor-continuous';
import { clearDistinctValues } from './legend-editor-discrete';
import { updateAlpha } from 'sdi/components/input';
import { editLayerInfo, updateCurrentMap } from './app';
import { scopeOption } from 'sdi/lib';

const logger = debug('sdi:events/legend-editor');

export const saveStyle = (lid: string, style: StyleConfig) =>
    editLayerInfo(lid, info => ({ ...info, style }));

type StyleEditFn = <T extends StyleConfig>(g: T) => T;
type PointStyleEditFn = <T extends PointStyleConfig>(g: T) => T;

const defaultPointLabel = (): LabelStyle => ({
    propName: makeRecord(),
    align: 'center',
    baseline: 'alphabetic',
    resLimit: 3,
    color: '#000000',
    size: 12,
});

const defaultPointMarker = (): PointMarker => ({
    codePoint: 0xf111,
    color: '#000000',
    size: 10,
});

const updateGroupStyle = <T extends DiscreteGroup | ContinuousInterval>(
    idx: number,
    update: (g: T) => void
) =>
    getCurrentLayerInfo().map(({ id, style }) => {
        if (isDiscrete(style)) {
            const group = getGroup(style, idx);
            if (group) {
                update(<T>group);
            }
            // const groups = style.groups.map((group, i) =>
            //     i === idx ? (update(<T>group) as typeof group) : group
            // );
            saveStyle(id, style);
        } else if (isContinuous(style)) {
            const interval = getInterval(style, idx);
            if (interval) {
                update(<T>interval);
            }
            saveStyle(id, style);
        }
    });

const updateStyle = <T extends StyleConfig>(f: (g: T) => T) =>
    getCurrentLayerInfo().map(({ id, style }) => {
        saveStyle(id, f(style as T)); // this is massive hack, but in the middle of a refacto...
    });

//     {
//     const lid = getCurrentLayerId();
//     if (lid) {
//         dispatch('data/maps', maps => {
//             const layer = getLayer(getCurrentMap(maps), lid);
//             if (layer) {
//                 const style = { ...layer.style };
//                 if (isDiscrete(style)) {
//                     f(style, lid);
//                 } else if (isContinuous(style)) {
//                     f(style, lid);
//                 } else {
//                     f(style, lid);
//                 }
//                 saveMap(lid, style);
//             }
//             return maps;
//         });
//     }
// };

const updateStyleLabel = (f: StyleEditFn) => {
    updateStyle(style =>
        style.label ? f(style) : f({ ...style, label: defaultPointLabel() })
    );
};

const updatePointStyleMarker = (f: PointStyleEditFn) =>
    updateStyle(style =>
        isPointSimple(style)
            ? style.marker
                ? f(style)
                : f({ ...style, marker: defaultPointMarker() })
            : style
    );
//     {
//     updateStyle((s, lid) => {
//         if (isPointStyle(s) && isSimple(s)) {
//             if (!isMarkered(s)) {
//                 s.marker = defaultPointMarker();
//             }
//             f(s, lid);
//         }
//     });
// };

export const setMainName = (propName: string) => {
    if (propName !== getSelectedMainName()) {
        updateStyle(s => {
            if (isContinuous(s) || isDiscrete(s)) {
                s.propName = propName;
            }
            return s;
        });
    }
};

export const resetLegend = () => {
    clearDataInterval();
    clearDistinctValues();
    const lid = getCurrentLayerId();
    const gt = getGeometryType();
    if (lid && gt) {
        saveStyle(lid, defaultStyle(gt));
    }
};

export const resetLegendForTypeAndCol = (
    subtype: SubType,
    propName: string
) => {
    const lid = getCurrentLayerId();
    const gt = getGeometryType();
    if (lid && gt) {
        saveStyle(lid, defaultStyle(gt, subtype, propName));
    }
};

export const setZoomRange = (
    minZoom: number | undefined,
    maxZoom: number | undefined
) =>
    getCurrentLayerIdOpt().map(id =>
        editLayerInfo(id, info => ({ ...info, maxZoom, minZoom }))
    );

export const setLegendVisibility = (visibleLegend: boolean) =>
    getCurrentLayerIdOpt().map(id =>
        editLayerInfo(id, info => ({ ...info, visibleLegend }))
    );

export const setExportable = (id: string, exportable: boolean) =>
    editLayerInfo(id, info => ({ ...info, exportable }));

export const setLegendLabel = (id: string, legend: MessageRecord) =>
    editLayerInfo(id, info => ({ ...info, legend }));

export const moveLayerUp = (lid: string) =>
    updateCurrentMap(info => {
        const currentIndex = info.layers.indexOf(lid);
        if (currentIndex >= 0 && currentIndex < info.layers.length - 1) {
            info.layers[currentIndex] = info.layers[currentIndex + 1];
            info.layers[currentIndex + 1] = lid;
        }
        return info;
    });

export const moveLayerDown = (lid: string) =>
    updateCurrentMap(info => {
        const currentIndex = info.layers.indexOf(lid);
        if (currentIndex >= 0 && currentIndex < info.layers.length - 1) {
            info.layers[currentIndex] = info.layers[currentIndex - 1];
            info.layers[currentIndex - 1] = lid;
        }
        return info;
    });

export const removeLayer = (info: ILayerInfo) =>
    updateCurrentMap(map => {
        const currentIndex = map.layers.indexOf(info.id);
        if (currentIndex >= 0) {
            map.layers.splice(currentIndex, 1);
        }
        return map;
    });

// Simple Style
export const setStrokeWidth = (strokeWidth: number) =>
    updateStyle(s =>
        isSimple(s) && (isPolygonStyle(s) || isLineStyle(s))
            ? {
                  ...s,
                  strokeWidth,
              }
            : s
    );

export const setStrokeColor = (strokeColor: string) =>
    updateStyle(s =>
        isSimple(s) && (isPolygonStyle(s) || isLineStyle(s))
            ? {
                  ...s,
                  strokeColor,
              }
            : s
    );

export const setDashStyle = (dash: number[]) =>
    updateStyle(s =>
        isSimple(s) && isLineStyle(s)
            ? {
                  ...s,
                  dash,
              }
            : s
    );

export const setFillColor = (fillColor: string) =>
    updateStyle(s =>
        isSimple(s) && isPolygonStyle(s)
            ? {
                  ...s,
                  fillColor,
              }
            : s
    );

export const setFirstGroupColor = (color: string) => {
    assign('component/legend-editor/interval-color/begin', color);
};

export const setLastGroupColor = (color: string) => {
    assign('component/legend-editor/interval-color/end', color);
};

const updatePattern = (
    pat: PatternStyle | undefined,
    u: Partial<PatternStyle>
): PatternStyle => {
    if (pat === undefined) {
        return {
            angle: 45,
            color: 'rgb(0,0,0)',
            width: 1,
            ...u,
        };
    }
    return { ...pat, ...u };
};

export const setPattern = (p: boolean) =>
    updateStyle(s =>
        isSimple(s) && isPolygonStyle(s)
            ? { ...s, pattern: p ? updatePattern(s.pattern, {}) : undefined }
            : s
    );

export const setPatternAngle = (angle: PatternAngle) =>
    updateStyle(s =>
        isSimple(s) && isPolygonStyle(s)
            ? { ...s, pattern: updatePattern(s.pattern, { angle }) }
            : s
    );

export const setPatternColor = (color: string) =>
    updateStyle(s =>
        isSimple(s) && isPolygonStyle(s)
            ? { ...s, pattern: updatePattern(s.pattern, { color }) }
            : s
    );

export const setPatternWidth = (width: number) =>
    updateStyle(s =>
        isSimple(s) && isPolygonStyle(s)
            ? { ...s, pattern: updatePattern(s.pattern, { width }) }
            : s
    );

// Group Style

export const setLabelForStyleGroup = (idx: number, label: MessageRecord) => {
    updateGroupStyle(idx, g => {
        g.label = label;
    });
};

export const setLabelForStyleInterval = (idx: number, label: MessageRecord) => {
    updateGroupStyle(idx, i => {
        i.label = label;
    });
};

export const setInterval = (idx: number, low: number, high: number) => {
    updateGroupStyle(idx, (g: ContinuousInterval) => {
        g.low = low;
        g.high = high;
    });
};

export const setStrokeWidthForGroup = (idx: number, w: number) =>
    updateStyle(style => {
        if (isLineStyle(style) || isPolygonStyle(style)) {
            if (isDiscrete(style)) {
                const group = getGroup(style, idx);
                if (group) {
                    (<LineDiscreteGroup | PolygonDiscreteGroup>(
                        group
                    )).strokeWidth = w;
                }
            } else if (isContinuous(style)) {
                const group = getInterval(style, idx);
                if (group) {
                    (<PolygonInterval | LineInterval>group).strokeWidth = w;
                }
            }
        }
        return style;
    });

export const setStrokeColorForGroup = (idx: number, c: string) =>
    updateStyle(style => {
        if (isLineStyle(style) || isPolygonStyle(style)) {
            if (isDiscrete(style)) {
                const group = getGroup(style, idx);
                if (group) {
                    (<LineDiscreteGroup | PolygonDiscreteGroup>(
                        group
                    )).strokeColor = c;
                }
            } else if (isContinuous(style)) {
                const group = getInterval(style, idx);
                if (group) {
                    (<PolygonInterval | LineInterval>group).strokeColor = c;
                }
            }
        }
        return style;
    });

export const setDashStyleForGroup = (idx: number, dash: number[]) => {
    updateStyle(style => {
        if (isLineStyle(style)) {
            if (isDiscrete(style)) {
                const group = getGroup(style, idx);
                if (group) {
                    (<LineDiscreteGroup>group).dash = dash;
                }
            } else if (isContinuous(style)) {
                const group = getInterval(style, idx);
                if (group) {
                    (<LineInterval>group).dash = dash;
                }
            }
        }
        return style;
    });
};

export const setFillColorForGroup = (idx: number, c: string) => {
    logger(`setFillColorForGroup ${idx} ${c}`);
    updateStyle(style => {
        if (isPolygonStyle(style)) {
            if (isDiscrete(style)) {
                const group = getGroup(style, idx);
                if (group) {
                    (<PolygonDiscreteGroup>group).fillColor = c;
                }
            } else if (isContinuous(style)) {
                const group = getInterval(style, idx);
                if (group) {
                    (<PolygonInterval>group).fillColor = c;
                }
            }
        }
        return style;
    });
};

export const setAlphaColorForGroups = (alpha: number) => {
    updateStyle(style => {
        if (isPolygonStyle(style)) {
            if (isDiscrete(style)) {
                style.groups.map((_, idx) => {
                    const group = getGroup(style, idx);
                    if (group) {
                        const c = (<PolygonDiscreteGroup>group).fillColor;
                        (<PolygonDiscreteGroup>group).fillColor = updateAlpha(
                            c,
                            alpha
                        );
                    }
                });
            } else if (isContinuous(style)) {
                style.intervals.map((_, idx) => {
                    const group = getInterval(style, idx);
                    if (group) {
                        const c = (<PolygonInterval>group).fillColor;
                        (<PolygonInterval>group).fillColor = updateAlpha(
                            c,
                            alpha
                        );
                    }
                });
            }
        }
        return style;
    });
};

export const setPatternForGroup = (idx: number, p: boolean) => {
    updateStyle(style => {
        if (isPolygonStyle(style)) {
            if (isDiscrete(style)) {
                const group = getGroup(
                    style,
                    idx
                ) as PolygonDiscreteGroup | null;
                if (group) {
                    if (p) {
                        group.pattern = updatePattern(group.pattern, {});
                    } else {
                        delete group.pattern;
                    }
                }
            } else if (isContinuous(style)) {
                const group = getInterval(style, idx) as PolygonInterval | null;
                if (group) {
                    if (p) {
                        group.pattern = updatePattern(group.pattern, {});
                    } else {
                        delete group.pattern;
                    }
                }
            }
        }
        return style;
    });
};

const setPatternPropForGroup = (idx: number, prop: Partial<PatternStyle>) => {
    updateStyle(style => {
        if (isPolygonStyle(style)) {
            if (isDiscrete(style)) {
                const group = getGroup(
                    style,
                    idx
                ) as PolygonDiscreteGroup | null;
                if (group) {
                    group.pattern = updatePattern(group.pattern, prop);
                }
            } else if (isContinuous(style)) {
                const group = getInterval(style, idx) as PolygonInterval | null;
                if (group) {
                    group.pattern = updatePattern(group.pattern, prop);
                }
            }
        }
        return style;
    });
};

export const setPatternAngleForGroup = (idx: number, angle: PatternAngle) =>
    setPatternPropForGroup(idx, { angle });
export const setPatternColorForGroup = (idx: number, color: string) =>
    setPatternPropForGroup(idx, { color });
export const setPatternWidthForGroup = (idx: number, width: number) =>
    setPatternPropForGroup(idx, { width });

// Point

export const setPointConfig = (pointConfig: 'label' | 'marker') =>
    dispatch('component/legend-editor', state => ({
        ...state,
        pointConfig,
    }));

export const setMarkerColor = (color: string) =>
    updatePointStyleMarker(s =>
        isMarkered(s)
            ? {
                  ...s,
                  marker: { ...s.marker, color },
              }
            : s
    );

export const setMarkerSize = (size: number) =>
    updatePointStyleMarker(s =>
        isMarkered(s)
            ? {
                  ...s,
                  marker: { ...s.marker, size },
              }
            : s
    );

export const setMarkerCodepoint = (codePoint: number) =>
    updatePointStyleMarker(s =>
        isMarkered(s)
            ? {
                  ...s,
                  marker: { ...s.marker, codePoint },
              }
            : s
    );

export const setFontColor = (color: string) =>
    updateStyleLabel(s =>
        isLabeled(s) ? { ...s, label: { ...s.label, color } } : s
    );

export const setFontSize = (size: number) =>
    updateStyleLabel(s =>
        isLabeled(s) ? { ...s, label: { ...s.label, size } } : s
    );

export const setPropNameForLabel = (name: string) =>
    updateStyleLabel(s =>
        isLabeled(s)
            ? {
                  ...s,
                  label: {
                      ...s.label,
                      propName: { ...s.label.propName, [getLang()]: name },
                  },
              }
            : s
    );

export const setPositionForLabel = (
    pos: 'above' | 'under' | 'left' | 'right'
) =>
    updateStyleLabel(style => {
        if (isLabeled(style)) {
            switch (pos) {
                case 'above':
                    style.label.align = 'center';
                    style.label.baseline = 'bottom';
                    style.label.yOffset = style.label.size * -1;
                    break;
                case 'under':
                    style.label.align = 'center';
                    style.label.baseline = 'top';
                    style.label.yOffset = style.label.size;
                    break;
                case 'left':
                    style.label.align = 'end';
                    style.label.baseline = 'middle';
                    break;
                case 'right':
                    style.label.align = 'start';
                    style.label.baseline = 'middle';
                    break;
            }
        }
        return style;
    });

export const setOffsetXForLabel = (xOffset: number) =>
    updateStyleLabel(s =>
        isLabeled(s)
            ? {
                  ...s,
                  label: { ...s.label, xOffset },
              }
            : s
    );

export const setOffsetYForLabel = (yOffset: number) =>
    updateStyleLabel(s =>
        isLabeled(s)
            ? {
                  ...s,
                  label: { ...s.label, yOffset },
              }
            : s
    );

//export const setResolutionForLabel = (r: number) => {
//     updateStyleLabel((s: StyleConfig) => {
//         if (isLabeled(s)) {
//             s.label.resLimit = r;
//         }
//     });
// }
export const setZoomForLabel = (resLimit: number) =>
    updateStyleLabel(s =>
        isLabeled(s)
            ? {
                  ...s,
                  label: { ...s.label, resLimit },
              }
            : s
    );

// Marker
export const setMarkerColorForGroup = (idx: number, c: string) =>
    updateGroupStyle(idx, (g: PointDiscreteGroup) => (g.marker.color = c));

export const setMarkerSizeForGroup = (idx: number, sz: number) =>
    updateGroupStyle(idx, (g: PointDiscreteGroup) => {
        g.marker.size = sz;
    });

export const setMarkerCodepointForGroup = (idx: number, c: number) =>
    updateGroupStyle(idx, (g: PointDiscreteGroup) => {
        g.marker.codePoint = c;
    });

export const addItem = () =>
    updateStyle(style =>
        fromNullable(getGeometryType())
            .map(() => {
                if (isDiscrete(style)) {
                    return addDefaultGroupStyle({ ...style });
                } else if (isContinuous(style)) {
                    return addDefaultIntervalStyle(
                        { ...style },
                        0,
                        10,
                        getFirstGroupColor(),
                        getLastGroupColor()
                    );
                }
                return style;
            })
            .getOrElse(style)
    );

export const removeItem = (k: number) =>
    updateStyle(style => {
        //  ouch, cant use filter https://github.com/microsoft/TypeScript/issues/44373
        if (isDiscrete(style)) {
            style.groups.splice(k, 1);
        } else if (isContinuous(style)) {
            style.intervals.splice(k, 1);
        }
        return style;
    });

export const selectStyleGroup = (k: number) => {
    dispatch('component/legend-editor', state => {
        state.styleGroupSelected = k;
        return state;
    });
};
export const clearStyleGroup = () => {
    dispatch('component/legend-editor', state => {
        state.styleGroupSelected = -1;
        return state;
    });
};

export const setStyleGroupEditedValue = (v: number | string) => {
    dispatch('component/legend-editor', state => {
        state.styleGroupEditedValue = v;
        return state;
    });
};
export const setStyleGroupEditedText = (v: string) => {
    dispatch('component/legend-editor', state => {
        state.styleGroupEditedValue = v;
        return state;
    });
};

export const resetStyleGroupEditedValue = () => {
    dispatch('component/legend-editor', state => {
        state.styleGroupEditedValue = null;
        return state;
    });
};

export const moveGroupUp = (index: number) =>
    getCurrentLayerIdOpt().map(id =>
        editLayerInfo(id, info => {
            if (isDiscrete(info.style)) {
                if (index >= 0) {
                    const next = index - 1;
                    const row = info.style.groups[index];
                    info.style.groups[index] = info.style.groups[next];
                    info.style.groups[next] = row;
                }
            }
            if (isContinuous(info.style)) {
                if (index >= 0) {
                    const next = index - 1;
                    const row = info.style.intervals[index];
                    info.style.intervals[index] = info.style.intervals[next];
                    info.style.intervals[next] = row;
                }
            }
            return info;
        })
    );

export const moveGroupDown = (index: number) =>
    getCurrentLayerIdOpt().map(id =>
        editLayerInfo(id, info => {
            if (isDiscrete(info.style)) {
                if (index >= 0 && index + 1 < info.style.groups.length) {
                    const next = index + 1;
                    const row = info.style.groups[index];
                    info.style.groups[index] = info.style.groups[next];
                    info.style.groups[next] = row;
                }
            }
            if (isContinuous(info.style)) {
                if (index >= 0 && index + 1 < info.style.intervals.length) {
                    const next = index + 1;
                    const row = info.style.intervals[index];
                    info.style.intervals[index] = info.style.intervals[next];
                    info.style.intervals[next] = row;
                }
            }
            return info;
        })
    );

// TODO: finish this function or delete it. nw
//export const increaseOrder = (k: number) => {
//     const lid = getCurrentLayerId();
//     if (lid) {
//         dispatch('data/maps', (maps: IMapInfo[]) => {
//             const layer = getLayer(getCurrentMap(maps), lid);
//             const gt = getGeometryType();
//             if (layer && gt) {
//                 const gidx = getSelectedStyleGroup();
//                 const group = getDiscreteStyleGroup(layer, gidx);

//                 if (group) {
//                     group.order;
//                 }
//                 saveMap(lid, layer.style);
//             }
//             return maps;
//         });
//     }
// }

export const selectSimple = (style?: StyleConfig) =>
    scopeOption()
        .let('id', getCurrentLayerIdOpt())
        .let('gt', fromNullable(getGeometryType()))
        .map(({ id, gt }) =>
            saveStyle(
                id,
                fromNullable(style).getOrElse(defaultStyle(gt, 'simple'))
            )
        );

logger('loaded');
