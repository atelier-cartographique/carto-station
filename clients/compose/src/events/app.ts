/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { v4 as uuid } from 'uuid';

import { assign, assignK, dispatch, observe } from 'sdi/shape';
import {
    defaultStyle,
    Feature,
    ILayerInfo,
    IMapInfo,
    Inspire,
    MessageRecord,
    makeRecord,
    FieldsDescriptorList,
    fetchIO,
    StreamingMetaIO,
    IMapBaselayerOrGroup,
    getMapBaseLayerSelection,
} from 'sdi/source';
import { getApiUrl } from 'sdi/app';
import { addLayer, removeLayerAll, addFeaturesToLayer } from 'sdi/map';
import {
    addStream,
    clearStreams,
    mapStream,
    pushStreamExtent,
} from 'sdi/geodata-stream';
import { isNotNullNorUndefined, ensureArray } from 'sdi/util';

import {
    fetchAlias,
    fetchAllDatasetMetadata,
    fetchCategories,
    fetchDatasetMetadata,
    fetchLayer,
    postLayerInfo,
    postMap,
    putMap,
    deleteMap,
    fetchAllLinks,
    fetchAllMaps,
    fetchBaseLayerAll,
    fetchBaseLayerGroups,
    fetchLayerInfoForMap,
    putLayerInfo,
    fetchBaseLayerSelection,
} from '../remote';
import {
    getCurrentMap,
    getSynteticLayerInfoOption,
    getLayerData,
    getMap,
    getSelectedVisibilityOption,
    getVersion,
    getLayerWithInfo,
    getMapInfoOpt,
    findBaseLayerGroup,
    findBaseLayer,
    makeLayerCodename,
    findLayerInfo,
    getCurrentLayerIdOpt,
    getMapInfo,
    getRecommendedBaseLayersAndGroups,
} from '../queries/app';
import { getView } from '../queries/map';

import { AppLayout, TabChoice } from '../shape/types';
import { initialLegendEditorState } from '../components/legend-editor/index';
import { navigateMap, navigateHome } from './route';
import { mapName } from '../components/map';
import { fromNullable, fromPredicate } from 'fp-ts/lib/Option';
import { Extent } from 'ol/extent';
import { activity } from 'sdi/activity';
import { catOptions } from 'fp-ts/lib/Array';
import { updateMapView } from './map';
import { Setoid } from 'fp-ts/lib/Setoid';

const logger = debug('sdi:events/app');
export const activityLogger = activity('compose');

observe('app/current-layer', cl => logger(`app/current-layer = ${cl}`));

export const toDataURL = (f: File) => {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = () => {
            const result = reader.result;
            if (result) {
                resolve(result);
            } else {
                reject();
            }
        };
        reader.onerror = reject;
        reader.onabort = reject;
        reader.readAsDataURL(f);
    });
};

observe('port/map/view', view =>
    fromNullable(view.extent).map(e =>
        mapStream(e, ({ uri, lid }, extent) =>
            loadLayerDataExtent(lid, uri, extent)
        )
    )
);

const loadLayerInfos = (mid: string) => {
    logger('loadLayerInfos ', mid);
    return fetchLayerInfoForMap(mid).then(infos => {
        logger('fetchLayerInfoForMap ', mid, infos.length);
        dispatch('data/layer-infos', state => state.concat(...infos));
        return infos;
    });
};

const loadMapFromInfo = (info: IMapInfo) => {
    logger('loadMapFromInfo', info.id);
    removeLayerAll(mapName);
    clearStreams();
    loadLayerInfos(info.id).then(infos => infos.map(loadLayer(info)));
    // info.layers.forEach(loadLayer(info));
    loadLinks(info.id);
};
export const loadBaseLayersGroups = (url: string) => {
    fetchBaseLayerGroups(url).then(assignK('data/baselayergroups'));
};

export const loadBaseLayerSelection = (url: string) => {
    fetchBaseLayerSelection(url).then(baselayers =>
        updateCurrentMap(m => ({
            ...m,
            baseLayersSelection: baselayers,
        }))
    );
};

const layerInRange = (info: ILayerInfo) => {
    const low = fromNullable(info.minZoom).getOrElse(0);
    const high = fromNullable(info.maxZoom).getOrElse(30);
    const zoom = getView().zoom;
    return info.visible && zoom > low && zoom < high;
};

const whenInRange = fromPredicate(layerInRange);

const { markVisibilityPending, delVisibilityPending, isVisibilityPending } =
    (() => {
        const register: Set<string> = new Set();

        const hash = (a: string, b: string) => `${a}/${b}`;
        const markVisibilityPending = (mid: string, lid: string) =>
            register.add(hash(mid, lid));

        const delVisibilityPending = (mid: string, lid: string) =>
            register.delete(hash(mid, lid));

        const isVisibilityPending = (mid: string, lid: string) =>
            register.has(hash(mid, lid));

        return {
            markVisibilityPending,
            delVisibilityPending,
            isVisibilityPending,
        };
    })();

const prepareStreamedLayer = (
    mapInfo: IMapInfo,
    layerInfo: ILayerInfo,
    md: Inspire,
    fields: FieldsDescriptorList
) => {
    dispatch('data/layers', state => {
        if (!(md.uniqueResourceIdentifier in state)) {
            state[md.uniqueResourceIdentifier] = {
                type: 'FeatureCollection',
                fields,
                features: [],
            };
        }
        return state;
    });
    if (layerInfo.visible && layerInRange(layerInfo)) {
        fromNullable(getView().extent).map(e => {
            loadLayerDataExtent(layerInfo.id, md.uniqueResourceIdentifier, e);
        });
    } else {
        markVisibilityPending(mapInfo.id, layerInfo.id);
    }
};

const loadLayer = (mapInfo: IMapInfo) => (layerInfo: ILayerInfo) => {
    fetchDatasetMetadata(getApiUrl(`metadatas/${layerInfo.metadataId}`))
        .then(md => {
            dispatch('data/datasetMetadata', state =>
                state.filter(i => i.id != md.id).concat(md)
            );

            addLayer(
                mapName,
                () => getSynteticLayerInfoOption(layerInfo.id),
                () => getLayerData(md.uniqueResourceIdentifier)
            );

            if (isNotNullNorUndefined(md.dataStreamUrl)) {
                addStream({
                    uri: md.uniqueResourceIdentifier,
                    lid: layerInfo.id,
                });

                fetchIO(StreamingMetaIO, md.dataStreamUrl)
                    .then(({ fields }) => {
                        prepareStreamedLayer(mapInfo, layerInfo, md, fields);
                    })
                    .catch(() => {
                        logger(
                            `[ERROR] failed to get fields for ${md.uniqueResourceIdentifier}`
                        );
                        prepareStreamedLayer(mapInfo, layerInfo, md, []);
                    });
            } else {
                loadLayerData(md.uniqueResourceIdentifier);
            }
        })
        .catch(err =>
            logger(`Failed to load MD ${layerInfo.metadataId}: ${err}`)
        );
};

const loadLayerDataExtent = (layerId: string, url: string, bbox: Extent) =>
    getSynteticLayerInfoOption(layerId).map(({ info }) =>
        whenInRange(info).map(info => {
            pushStreamExtent(bbox, { lid: layerId, uri: url });
            fetchLayer(
                `${url}?bbox=${bbox[0]},${bbox[1]},${bbox[2]},${bbox[3]}`
            )
                .then(layer => {
                    if (layer.features !== null) {
                        addFeaturesToLayer(mapName, info, layer.features);
                        dispatch('data/layers', state => {
                            if (url in state) {
                                state[url].features = state[
                                    url
                                ].features.concat(layer.features);
                            } else {
                                state[url] = layer;
                            }
                            return state;
                        });
                    }
                })
                .catch(err => {
                    logger(`Failed to load features at ${url} due to ${err}`);
                    dispatch('remote/errors', state => ({
                        ...state,
                        [url]: `${err}`,
                    }));
                });
        })
    );

const loadLayerData = (url: string) => {
    logger(`loadLayerData(${url})`);
    fetchLayer(url)
        .then(layer => {
            dispatch('data/layers', state => {
                logger(`Put layer ${url} on state`);
                state[url] = layer;
                return state;
            });
        })
        .catch(err => {
            logger(`Failed to load layer at ${url} due to ${err}`);
            dispatch('remote/errors', state => ({ ...state, [url]: `${err}` }));
        });
};

const makeMap = (): Omit<IMapInfo, 'id' | 'id_origin'> => {
    return {
        status: 'draft',
        title: makeRecord(),
        description: makeRecord(),
        attachments: [],
        layers: [],
        categories: [],
        lastModified: Date.now(),
        url: '',
        baseLayer: 'urbis.irisnet.be/urbis_gray',
        baseLayersSelection: getRecommendedBaseLayersAndGroups(),
    };
};

// Load the maps of the user
export const loadAllUserMaps = () => {
    fetchAllMaps(getApiUrl('maps/compose'))
        .then(assignK('data/maps'))
        .catch(err => logger('error loading maps', err));
};
// Load all published maps (so you can link them)
export const loadPublishedMaps = () =>
    fetchAllMaps(getApiUrl('maps/published')).then(newMaps =>
        dispatch('data/maps', data =>
            data
                .filter(umap => newMaps.findIndex(nm => nm.id === umap.id) < 0)
                .concat(newMaps)
        )
    );

const loadLinks = (mid: string) =>
    fetchAllLinks(getApiUrl(`map/links?mid=${mid}`)).then(links => {
        dispatch('data/links', data => ({ ...data, [mid]: links }));
    });

export const setLayout = (l: AppLayout) => {
    dispatch('app/layout', () => l);
};

export const signalReadyMap = () => {
    dispatch('app/map-ready', () => true);
};

export const loadAllBaseLayers = (url: string) => {
    fetchBaseLayerAll(url).then(assignK('data/baselayers'));
};

export const loadCategories = (url: string) => {
    fetchCategories(url).then(categories => {
        dispatch('data/categories', () => categories);
    });
};

export const loadAlias = (url: string) => {
    fetchAlias(url).then(alias => {
        dispatch('data/alias', () => alias);
    });
};

export const loadDatasetMetadata = (url: string) => {
    fetchDatasetMetadata(url).then(md =>
        dispatch('data/datasetMetadata', state =>
            state.filter(i => i.id !== md.id).concat([md])
        )
    );
};

export const loadAllDatasetMetadata = (done?: () => void) => {
    dispatch('component/table/features', ts => ({ ...ts, loaded: 'loading' }));

    fetchAllDatasetMetadata(getApiUrl('metadatas')).then(mds => {
        assign('data/datasetMetadata', mds);
        dispatch('component/table/features', ts => ({
            ...ts,
            loaded: 'done',
        }));
        if (done) {
            done();
        }
    });

    // fetchAllDatasetMetadata(getApiUrl('metadatas'))(
    //     frame => {
    //         dispatch('data/datasetMetadata', state =>
    //             uniqInspire(state.concat(frame.results))
    //         );
    //         dispatch('component/table/features', ts => ({
    //             ...ts,
    //             loaded: 'loading',
    //         }));
    //         dispatch('component/splash', () =>
    //             Math.floor((frame.page * 100) / frame.total)
    //         );
    //     },
    //     () => {
    //         dispatch('component/table/features', ts => ({
    //             ...ts,
    //             loaded: 'done',
    //         }));
    //         if (done) {
    //             done();
    //         }
    //     }
    // );
};

export const editLayerInfo = (
    lid: string,
    update: (i: ILayerInfo) => ILayerInfo
) =>
    findLayerInfo(lid).map(info =>
        putLayerInfo(update(info))
            .then(info => {
                dispatch('data/layer-infos', state =>
                    state.filter(({ id }) => id !== info.id).concat(info)
                );
                updateMapView({ dirty: 'style' });
            })
            .catch(err => logger('failed saving style', err))
    );

// export const setLayerVisibility = (id: string, visible: boolean) => {
//     const mid = getCurrentMap();
//     dispatch('data/maps', maps => {
//         const mapInfo = maps.find(m => m.id === mid);
//         if (mapInfo) {
//             mapInfo.layers.forEach(l => {
//                 if (l.id === id) {
//                     l.visible = visible;
//                     if (isVisibilityPending(mapInfo.id, l.id)) {
//                         fromNullable(getView().extent).map(e =>
//                             mapStream(e, ({ uri, lid }, extent) =>
//                                 loadLayerDataExtent(lid, uri, extent)
//                             )
//                         );
//                         delVisibilityPending(mapInfo.id, l.id);
//                     } else if (!visible) {
//                         markVisibilityPending(mapInfo.id, l.id);
//                     }
//                     setTimeout(() => {
//                         putMap(getApiUrl(`maps/${mid}`), mapInfo);
//                     }, 1);
//                 }
//             });
//         }
//         return maps;
export const editCurrentLayerInfo = (update: (i: ILayerInfo) => ILayerInfo) =>
    getCurrentLayerIdOpt().map(id => editLayerInfo(id, update));

export const setLayerVisibility = (id: string, visible: boolean) =>
    editLayerInfo(id, info => {
        fromNullable(getMapInfo()).map(mapInfo => {
            if (isVisibilityPending(mapInfo.id, id)) {
                fromNullable(getView().extent).map(e =>
                    mapStream(e, ({ uri, lid }, extent) =>
                        loadLayerDataExtent(lid, uri, extent)
                    )
                );
                delVisibilityPending(mapInfo.id, id);
            } else if (!visible) {
                markVisibilityPending(mapInfo.id, id);
            }
            // setTimeout(() => {
            //     putMap(getApiUrl(`maps/${mapInfo.id}`), mapInfo);
            // }, 1);
        });
        return { ...info, visible };
    });
// {
//     // const mid = ;
//     fromNullable(getMap(mid));
//     dispatch('data/maps', maps => {
//         // const idx = maps.findIndex(m => m.id === mid);
//         // if (idx !== -1) {
//         //     const m = maps[idx];
//         //     m.layers.forEach((l) => {
//         //         if (l.id === id) {
//         //             l.visible = visible;
//         //         }
//         //     });
//         //     setTimeout(() => {
//         //         putMap(getApiUrl(`maps/${mid}`), m);
//         //     }, 1);
//         // }
//         const mapInfo = maps.find(m => m.id === mid);
//         if (mapInfo) {
//             mapInfo.layers.forEach(layerId => {
//                 if (layerId === id) {
//                     l.visible = visible;
//                     if (isVisibilityPending(mapInfo.id, l.id)) {
//                         fromNullable(getView().extent).map(e =>
//                             mapStream(e, ({ uri, lid }, extent) =>
//                                 loadLayerDataExtent(lid, uri, extent)
//                             )
//                         );
//                         delVisibilityPending(mapInfo.id, l.id);
//                     } else if (!visible) {
//                         markVisibilityPending(mapInfo.id, l.id);
//                     }
//                     setTimeout(() => {
//                         putMap(getApiUrl(`maps/${mid}`), mapInfo);
//                     }, 1);
//                 }
//             });
//         }
//         return maps;
//     });
// }

export const setCurrentMapId = (id: string) => {
    clearMap();
    dispatch('app/current-map', () => id);
    getMapInfoOpt().map(loadMapFromInfo);
};

export const clearMap = () => {
    dispatch('app/current-map', () => null);
    dispatch('app/current-layer', () => null);
    dispatch('app/current-feature', () => null);
};

export const clearLayer = () => {
    assign('app/current-layer', null);
};

export const setCurrentLayerId = (id: string) => {
    dispatch('app/current-layer', () => id);
    dispatch('app/current-feature', () => null);
    dispatch('component/table/features', state => {
        state.selected = [];
        return state;
    });
};

export const selectCurrentLayer = (id: string) => {
    setCurrentLayerId(id);
    resetLegendEditor();
    setLayout(AppLayout.LegendEditor);
};

export const setCurrentFeatureData = (data: Feature) => {
    dispatch('app/current-feature', () => data);
};

export const unsetCurrentFeatureData = () => {
    dispatch('app/current-feature', () => null);
};

export const setMapTitle = (r: MessageRecord) => {
    const mid = getCurrentMap();
    dispatch('data/maps', maps => {
        const idx = maps.findIndex(m => m.id === mid);
        if (idx !== -1) {
            const m = maps[idx];
            m.title = r;
            setTimeout(() => {
                putMap(getApiUrl(`maps/${mid}`), m);
            }, 1);
        }
        return maps;
    });
};

export const setMapDescription = (r: MessageRecord) => {
    const mid = getCurrentMap();
    dispatch('data/maps', maps => {
        const idx = maps.findIndex(m => m.id === mid);
        if (idx !== -1) {
            const m = maps[idx];
            m.description = r;
            setTimeout(() => {
                putMap(getApiUrl(`maps/${mid}`), m);
            }, 1);
        }
        return maps;
    });
};

const makeNewLayer = (id: string, metadata: Inspire): ILayerInfo => ({
    id,
    metadataId: metadata.id,
    visible: true,
    featureViewOptions: { type: 'default' },
    style: defaultStyle(metadata.geometryType),
    group: null,
    legend: null,
    minZoom: 0,
    maxZoom: 30,
    visibleLegend: true,
    opacitySelector: false,
    switchVisibility: false,
});

export const addMapLayer = (metadata: Inspire) =>
    fromNullable(getMapInfo()).map(info => {
        const layerId = uuid();
        const layerInfo = makeNewLayer(layerId, metadata);
        const mapInfo = { ...info, layers: info.layers.concat(layerId) };

        postLayerInfo(getApiUrl(`layerinfos`), layerInfo)
            .then(result => {
                logger(`Recorded Layer ${result.id} / ${result.metadataId}`);
                dispatch('data/layer-infos', state => state.concat(result));
            })
            .then(() => putMap(getApiUrl(`maps/${mapInfo.id}`), mapInfo))
            .then(() =>
                dispatch('data/maps', maps =>
                    maps.filter(({ id }) => id !== mapInfo.id).concat(mapInfo)
                )
            )
            .then(() =>
                findLayerInfo(layerId).map(result => {
                    addLayer(
                        mapName,
                        () => getSynteticLayerInfoOption(result.id),
                        () => getLayerData(metadata.uniqueResourceIdentifier)
                    );
                    setCurrentLayerId(result.id);
                    loadLayer(mapInfo)(result);
                })
            )
            .catch(err => logger(`addMapLayer layer info ${err}`));
    });

export const newMap = () => {
    postMap(getApiUrl(`maps`), makeMap()).then(map => {
        if (map.id) {
            const mid = map.id;
            dispatch('data/maps', state => state.concat([map]));
            dispatch('app/user', user => {
                if (user && user.id) {
                    user.maps = user.maps.concat([mid]);
                }
                return user;
            });
            navigateMap(mid);
        }
    });
};

export const deleteMapFromId = (id: string) => {
    getMap(id).map(m => {
        if (m.status === 'draft') {
            navigateHome();
        }
    });
    deleteMap(getApiUrl(`maps/${id}`))
        .then(() =>
            dispatch('data/maps', state => state.filter(m => m.id !== id))
        )
        .catch(err => logger(`Failed to delete map ${id} ${err}`));
};
export const deleteAllMapVersionsFromId = (id: string) => {
    getVersion('published').map(pub => deleteMapFromId(pub.id));
    getVersion('saved').map(saved => deleteMapFromId(saved.id));
    deleteMapFromId(id);
};

const editCurrentMap = (mapper: (i: IMapInfo) => IMapInfo) =>
    fromNullable(getCurrentMap()).map(mid =>
        dispatch('data/maps', maps => {
            const idx = maps.findIndex(m => m.id === mid);
            if (idx !== -1) {
                maps[idx] = mapper(maps[idx]);
                setTimeout(() => {
                    putMap(getApiUrl(`maps/${mid}`), maps[idx]);
                }, 1);
            }
            return maps;
        })
    );

export const removeCategory = (c: string) =>
    editCurrentMap(info => {
        info.categories = info.categories.filter(mc => mc !== c);
        return info;
    });

export const addCategory = (c: string) =>
    editCurrentMap(info => {
        info.categories = info.categories.filter(mc => mc !== c).concat(c);
        return info;
    });

export const removeMapInfoIllustration = () =>
    editCurrentMap(info => {
        info.imageUrl = undefined;
        return info;
    });

// For map baselayers ---------------------------
const setMapBaseLayer = (id: string) =>
    makeLayerCodename(id).map(codename =>
        editCurrentMap(info => {
            info.baseLayer = [codename];
            return info;
        })
    );

const setMapBaseLayerGroup = (id: string) => {
    const selection = findBaseLayerGroup(id)
        .map(g =>
            catOptions(
                g.layers.map(l =>
                    findBaseLayer(l).chain(layer => makeLayerCodename(layer.id))
                )
            )
        )
        .getOrElse([]);
    editCurrentMap(info => {
        info.baseLayer = selection;
        return info;
    });
};
export const setMapBaseLayerOrGroup = (layerOrGroup: IMapBaselayerOrGroup) => {
    switch (layerOrGroup.tag) {
        case 'layer':
            setMapBaseLayer(layerOrGroup.id);
            break;

        case 'group':
            setMapBaseLayerGroup(layerOrGroup.id);
            break;
    }
};

const delMapBaseLayer = (id: string) =>
    editCurrentMap(info => {
        info.baseLayer = ensureArray(info.baseLayer).filter(b => b !== id);
        return info;
    });

// For map baselayer selection

const isSameBaseLayerOrGroup = (
    a: IMapBaselayerOrGroup,
    b: IMapBaselayerOrGroup
) => a.tag == b.tag && a.id == b.id;

export const setoidBaseLayerOrGroup = <
    T extends IMapBaselayerOrGroup
>(): Setoid<T> => ({
    equals: (a: IMapBaselayerOrGroup, b: IMapBaselayerOrGroup) =>
        isSameBaseLayerOrGroup(a, b),
});

const addMapBaseLayerSelection = (layerOrGroup: IMapBaselayerOrGroup) =>
    editCurrentMap(info => {
        info.baseLayersSelection = [layerOrGroup].concat(
            getMapBaseLayerSelection(info).filter(
                b => !isSameBaseLayerOrGroup(b, layerOrGroup)
            )
        );
        return info;
    }).getOrElse(void 0);

const delMapBaseLayerSelection = (layerOrGroup: IMapBaselayerOrGroup) =>
    editCurrentMap(info => {
        info.baseLayersSelection = getMapBaseLayerSelection(info).filter(
            b => !isSameBaseLayerOrGroup(b, layerOrGroup)
        );
        return info;
    });

export const addMapBaseLayerGroup = (id: string) =>
    addMapBaseLayerSelection({ tag: 'group', id });
export const addMapBaseLayerInSelection = (id: string) =>
    addMapBaseLayerSelection({ tag: 'layer', id });
export const delMapBaseLayerInSelection = (id: string) =>
    delMapBaseLayerSelection({ tag: 'layer', id });
export const delGroupLayerInSelection = (id: string) =>
    delMapBaseLayerSelection({ tag: 'group', id });
// ----------------------------- end map baselayer selection fuctions

export const delMapBaseLayerInService = (service: string) => (layer: string) =>
    delMapBaseLayer(`${service}/${layer}`);

export const resetLegendEditor = () => {
    dispatch('component/legend-editor', () => initialLegendEditorState());
};

export const selectTab = (tab: TabChoice) => {
    clearLayer();
    setLayout(AppLayout.MapAndInfo);
    assign('app/tab/select', tab);
};

const selectVisibilityOption = (layerId: string) => {
    deselectLayerInfo();
    assign('app/visibility-options/select', layerId);
    selectCurrentLayer(layerId);
};
const selectLayerWithInfo = (layerId: string) => {
    deselectVisibilityOption();
    assign('app/layer-info/select', layerId);
    selectCurrentLayer(layerId);
};

export const toggleVisibilityOption = (layerId: string) =>
    getSelectedVisibilityOption().foldL(
        () => selectVisibilityOption(layerId),
        lid =>
            layerId === lid
                ? deselectVisibilityOption()
                : selectVisibilityOption(layerId)
    );

export const deselectVisibilityOption = () =>
    assign('app/visibility-options/select', null);

export const toggleLayerInfo = (layerId: string) =>
    getLayerWithInfo().foldL(
        () => selectLayerWithInfo(layerId),
        lid =>
            layerId === lid ? deselectLayerInfo() : selectLayerWithInfo(layerId)
    );

export const deselectLayerInfo = () => assign('app/layer-info/select', null);

export const updateCurrentMap = (update: (m: IMapInfo) => IMapInfo) =>
    fromNullable(getMapInfo())
        .map(update)
        .map(info =>
            putMap(getApiUrl(`maps/${info.id}`), {
                ...info,
                lastModified: Date.now(),
            })
                .then(info =>
                    dispatch('data/maps', state =>
                        state.filter(({ id }) => id !== info.id).concat(info)
                    )
                )
                .catch(err => logger('failed to update map', err))
        );

logger('loaded');
