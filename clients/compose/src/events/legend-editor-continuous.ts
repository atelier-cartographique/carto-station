/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { fromNullable } from 'fp-ts/lib/Option';
import { scopeOption } from 'sdi/lib';

import { assign, dispatch } from 'sdi/shape';
import {
    addDefaultIntervalStyle,
    defaultStyle,
    ILayerInfo,
    isContinuous,
    makeRecord,
    remoteLoading,
    RemoteResource,
    remoteSuccess,
    remoteError,
    remoteNone,
} from 'sdi/source';

import {
    getCurrentLayerIdOpt,
    getCurrentSynteticLayerInfoOption,
} from '../queries/app';

import {
    getAutoClassNbDecimals,
    getAutoClassValue,
    getFirstGroupColor,
    getGeometryType,
    getLastGroupColor,
} from '../queries/legend-editor';
import { DataInterval, fetchDataInterval } from '../remote';
import { editLayerInfo } from './app';
import { resetLegendForTypeAndCol, saveStyle } from './legend-editor';

export const getContinuousStyleInterval = (
    layer: ILayerInfo | null,
    idx: number | null
) => {
    if (
        layer &&
        idx !== null &&
        isContinuous(layer.style) &&
        idx < layer.style.intervals.length
    ) {
        return layer.style.intervals[idx];
    }

    return null;
};

export const setDataInterval = (rdi: RemoteResource<DataInterval>) =>
    assign('component/legend-editor/data-interval', rdi);

export const clearDataInterval = () =>
    assign('component/legend-editor/data-interval', remoteNone);

export const loadDataInterval = (propName: string) => {
    setDataInterval(remoteLoading);
    getCurrentSynteticLayerInfoOption().map(li =>
        fromNullable(li.metadata.dataStreamUrl).map(url =>
            fetchDataInterval(url, propName)
                .then(i => {
                    resetLegendForTypeAndCol('continuous', propName);
                    setDataInterval(remoteSuccess(i));
                    makeContinuousClasses(i);
                })
                .catch(err => setDataInterval(remoteError(err)))
        )
    );
};

// round to 2 significant digit
const roundToN = (num: number, decimals: number) => {
    const d = Math.pow(10, decimals);
    return Math.round(num * d) / d;
};

const increase = (value: number, nbDecimals: number) => {
    if (Math.abs(Math.round(value)) > 0) {
        return value + 1;
    }
    return roundToN(value + Math.pow(0.1, nbDecimals), nbDecimals);
};

const continuousClassInner = (
    n: number,
    layer: ILayerInfo,
    dataInterval: DataInterval
) => {
    const style = { ...layer.style };
    if (isContinuous(style)) {
        const step = (dataInterval.max - dataInterval.min) / n;
        const decimals = getAutoClassNbDecimals();
        for (let i = 0; i < n; i += 1) {
            addDefaultIntervalStyle(
                style,
                i,
                n,
                getFirstGroupColor(),
                getLastGroupColor()
            );
            const interval = style.intervals[i];
            interval.low = dataInterval.min + i * step;
            interval.high =
                i == n - 1
                    ? increase(interval.low + step, decimals) // increase highest value of highest interval to include the highest value of the data)
                    : interval.low + step;
            const label = `${roundToN(interval.low, decimals)} - ${roundToN(
                interval.high,
                decimals
            )}`;
            interval.label = makeRecord(label, label, label);
        }
    }
};

export const selectContinuous = () =>
    scopeOption()
        .let('id', getCurrentLayerIdOpt())
        .let('gt', fromNullable(getGeometryType()))
        .map(({ id, gt }) => saveStyle(id, defaultStyle(gt, 'continuous')));

export const setAutoClassValue = (n: number) => {
    dispatch('component/legend-editor', state => {
        state.autoClassValue = Math.max(n, 2);
        return state;
    });
};

export const setAutoClassNbDecimals = (n: number) => {
    dispatch('component/legend-editor', state => {
        state.autoClassNbDecimals = n;
        return state;
    });
};

export const makeContinuousClasses = (interval: DataInterval) =>
    getCurrentLayerIdOpt().map(id =>
        editLayerInfo(id, info => {
            continuousClassInner(getAutoClassValue(), info, interval);
            return info;
        })
    );
