/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { loop, getApiUrl, loadTermsAndDomains, getLang } from 'sdi/app';
import { DIV, SPAN, NodeOrOptional, BUTTON } from 'sdi/components/elements';
import header from 'sdi/components/header';
import footer from 'sdi/components/footer';
import splash from 'sdi/components/splash';
import tr from 'sdi/locale';

import map from './components/map';
import tableAttributes from './components/table/features';
// import tableAttributesEditable from './components/table/feature-collection-editable';
import tableLayers from './components/table/layers';
import tableInspire from './components/table/inspire';
import {
    loadCategories,
    loadAlias,
    loadAllUserMaps,
    loadAllDatasetMetadata,
    activityLogger,
    loadAllBaseLayers,
    loadBaseLayersGroups,
} from './events/app';
import { getLayout } from './queries/app';
import * as mapEvents from './events/map';
import info from './components/map-info';
import dashboard from './components/dashboard';
import legendEditor from './components/legend-editor';
import featureConfig from './components/feature-config';
import composeSplash from './components/splash';
import { renderLinkSelect } from './components/link-map';

// import viewInspire from './components/layer/view-inspire';
// import featureEdit from './components/layer/edit';

import upload from './components/upload';
import { AppLayout } from './shape/types';
import { navigateHome } from './events/route';
import { visitAction, langAction } from 'sdi/activity';
import { maintenance } from 'sdi/components/maintenance';
import { icon } from 'sdi/components/button';

const logger = debug('sdi:app');

const renderAppListingButton = () => {
    const l = getLayout();
    if (l !== AppLayout.Dashboard && l !== AppLayout.Splash) {
        return DIV(
            'app-listwrapper',
            BUTTON(
                {
                    className: 'navigate app-listview',
                    onClick: navigateHome,
                },
                icon('ellipsis-h'),
                SPAN('label', tr.compose('myMaps'))
            )
        );
    }
    return DIV({});
};

const wrappedMain = (name: string, ...elements: NodeOrOptional[]) =>
    DIV(
        'app-inner compose-inner',
        header('compose', {
            navigateHome,
            appElement: renderAppListingButton(),
        }),
        maintenance('compose'),
        DIV(`main ${name}`, ...elements),
        footer()
    );

const renderSplash = () => wrappedMain('splash', splash(composeSplash()));

const renderDashboard = () => wrappedMain('dashboard', dashboard());

const renderMapFs = () => wrappedMain('map-fs', map());

const renderMapAndInfo = () => wrappedMain('map-and-info', map(), info());

const renderTableFs = () => wrappedMain('table-fs', tableAttributes());

const renderMapAndTable = () =>
    wrappedMain(
        'map-and-table',
        DIV('vertical-split', map(), tableAttributes())
    );

// const renderLayerSelect = () =>
//     wrappedMain(
//         'map-and-layer-selector',
//         DIV('vertical-split', map(), tableLayers()),

//         info()
//     );

// const renderLayerSelectAndInspire = () =>
//     wrappedMain(
//         'map-and-layer-selector-and-inspire',
//         DIV(
//             'vertical-split',
//             DIV('snail', map(), tableInspire()),
//             tableLayers()
//         ),

//         info()
//     );

const renderLayerSelect = () =>
    wrappedMain('layer-selector', tableLayers(), info());

const renderLayerSelectAndInspire = () =>
    wrappedMain(
        'layer-selector-preview',
        DIV('vertical-split', tableLayers(), tableInspire()),
        info()
    );

const renderLegendEditor = () =>
    wrappedMain(
        'map-and-style-tools',
        DIV('vertical-split', map(), legendEditor()),
        info()
    );

const renderLegendEditorAndTable = () =>
    wrappedMain(
        'attributes-and-style-tools',
        DIV('vertical-split', tableAttributes(), legendEditor()),
        info()
    );

const renderFeatureConfig = () =>
    wrappedMain(
        'feature-config',
        DIV('vertical-split', tableAttributes(), featureConfig()),
        // featureView()
        // renderListForFeatureView()
        info()
    );

const renderUpload = () => wrappedMain('upload', upload());

const renderLinkMap = () => wrappedMain('link-map', renderLinkSelect());

const renderMain = () => {
    const layout = getLayout();
    switch (layout) {
        case AppLayout.Splash:
            return renderSplash();
        case AppLayout.Dashboard:
            return renderDashboard();
        case AppLayout.MapFS:
            return renderMapFs();
        case AppLayout.MapAndTable:
            return renderMapAndTable();
        case AppLayout.MapAndInfo:
            return renderMapAndInfo();
        case AppLayout.TableFs:
            return renderTableFs();
        case AppLayout.LayerSelect:
            return renderLayerSelect();
        case AppLayout.LayerSelectAndInspire:
            return renderLayerSelectAndInspire();
        case AppLayout.LegendEditor:
            return renderLegendEditor();
        case AppLayout.LegendEditorAndTable:
            return renderLegendEditorAndTable();
        case AppLayout.FeatureConfig:
            return renderFeatureConfig();
        case AppLayout.Upload:
            return renderUpload();
        case AppLayout.MapLink:
            return renderLinkMap();
    }
};

const effects = () => {
    mapEvents.updateMapView({ dirty: 'geo' });
    loadCategories(getApiUrl(`categories`));
    loadAlias(getApiUrl(`alias`));
    // loadAllLinks(); // We now only load links for a map (see loadMap) - no need to load all links
    loadAllUserMaps();
    // baseLayers.forEach(id => loadBaseLayer(id, getApiUrl(`wmsconfig/${id}`)));
    loadAllBaseLayers(getApiUrl('wmsconfig/'));
    loadBaseLayersGroups(getApiUrl(`wmsgroups/`));

    loadAllDatasetMetadata(navigateHome);
    tr.init_edited();
    loadTermsAndDomains();
    activityLogger(visitAction());
    activityLogger(langAction(getLang()));
};

const app = loop('compose', renderMain, effects);
export default app;

logger('loaded');
