/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { Option, none, some, fromNullable } from 'fp-ts/lib/Option';
import { Either, right, left } from 'fp-ts/lib/Either';

import { query, queryK } from 'sdi/shape';
import {
    getMessageRecord,
    FeatureCollection,
    IMapInfo,
    Feature,
    MapStatus,
    IMapBaseLayer,
    IMapBaselayerOrGroup,
} from 'sdi/source';
import { getUserData, SyntheticLayerInfo } from 'sdi/app';

import { getDatasetMetadata } from './metadata';
import { scopeOption } from 'sdi/lib';
import {
    ensureArray,
    getFeaturePropOption,
    getLayerFields,
    tryString,
} from 'sdi/util';
import { catOptions, index } from 'fp-ts/lib/Array';
import { fromRecord, Translated } from 'sdi/locale';

type FCContext = (fc: FeatureCollection) => FeatureCollection;

export const mapReady = () => {
    return query('app/map-ready');
};

export const getLayout = () => {
    return query('app/layout');
};

export const getMaps = () => {
    return query('data/maps');
};

export const findLayerInfo = (lid: string) =>
    fromNullable(query('data/layer-infos').find(({ id }) => id === lid));

export const getUserMaps = () => {
    const dataMaps = getMaps();
    const findMap = (id: string) =>
        fromNullable(dataMaps.find(m => m.id === id));
    return getUserData().fold([], ({ maps }) =>
        maps.reduce<IMapInfo[]>(
            (acc, id) => findMap(id).fold(acc, m => acc.concat([m])),
            []
        )
    );
};

export const getLayerData = (
    id: string
): Either<string, Option<FeatureCollection>> => {
    const layers = query('data/layers');
    const errors = query('remote/errors');
    if (id in layers) {
        return right(some<FeatureCollection>(layers[id]));
    } else if (id in errors) {
        return left(errors[id]);
    }
    return right(none);
};

export const getCurrentLayerData = () =>
    fromNullable(getCurrentLayerId())
        .chain(id =>
            getSynteticLayerInfoOption(id).map(
                linfo => linfo.metadata.uniqueResourceIdentifier
            )
        )
        .map(getLayerData);

export const getFeaturePropValues = (features: Feature[], field: string) => {
    const values: string[] = [];
    features.map(f =>
        getFeaturePropOption(f, field).chain(p =>
            tryString(p).map(p => {
                if (!values.includes(p)) {
                    values.push(p);
                }
            })
        )
    );
    return values;
};

export const getLayerDataWithContext = (id: string, context: FCContext) => {
    const layers = query('data/layers');
    if (id in layers) {
        return context(layers[id]);
    }
    return null;
};

export const getMap = (mid: string) => {
    const maps = getMaps();
    return fromNullable(maps.find(m => m.id === mid));
};

export const getCurrentMap = () => query('app/current-map');

export const getCurrentMapOpt = () => fromNullable(getCurrentMap());

export const getMapInfo = () => {
    const mid = getCurrentMap();
    const info = getMaps().find(m => m.id === mid);
    return info !== undefined ? info : null;
};
export const getMapInfoOpt = () => fromNullable(getMapInfo());

export const getMapLayerInfo = () =>
    getMapInfoOpt()
        .map(info => catOptions(info.layers.map(findLayerInfo)))
        .getOrElse([]);

export const getSynteticLayerInfoOption = (
    layerId: string
): Option<SyntheticLayerInfo> =>
    scopeOption()
        .let('layerInfo', findLayerInfo(layerId))
        .let('metadata', ({ layerInfo }) =>
            getDatasetMetadata(layerInfo.metadataId)
        )
        .map(({ layerInfo, metadata }) => ({
            name: getMessageRecord(metadata.resourceTitle),
            info: layerInfo,
            metadata,
        }));

export const getCurrentLayerId = () => query('app/current-layer');

export const getCurrentLayerIdOpt = () => fromNullable(getCurrentLayerId());

export const getCurrentLayerInfo = () =>
    getCurrentLayerIdOpt().chain(findLayerInfo);

export const getCurrentSynteticLayerInfoOption = () =>
    fromNullable(query('app/current-layer')).chain(getSynteticLayerInfoOption);

export const getCurrenFields = () =>
    getCurrentSynteticLayerInfoOption()
        .chain(({ metadata }) =>
            fromNullable(
                query('data/layers')[metadata.uniqueResourceIdentifier]
            )
        )
        .chain(getLayerFields);

export const getCurrentFeature = () => {
    return query('app/current-feature');
};
export const getCurrentBaseLayerNames = () =>
    getMapInfoOpt()
        .map(info => ensureArray(info.baseLayer))
        .getOrElse([]);

export const getCurrentBaseLayerNamesInService = (service: string) =>
    catOptions(getCurrentBaseLayerNames().map(baseLayerParts))
        .filter(parts => parts.service === service)
        .map(({ layer }) => layer);

const baseLayerParts = (
    codename: string
): Option<{ service: string; layer: string }> => {
    const parts = codename.split('/');
    if (parts.length === 2) {
        return some({ service: parts[0], layer: parts[1] });
    }
    return none;
};

export const getBaseLayers = queryK('data/baselayers');

export const findBaseLayer = (codename: string) =>
    baseLayerParts(codename).chain(({ layer, service }) =>
        fromNullable(getBaseLayers().find(({ id }) => id === service)).chain(
            service =>
                fromNullable(
                    service.layers.find(({ codename }) => codename === layer)
                )
        )
    );
export const findBaseLayerFromId = (id: string) =>
    fromNullable(
        getBaseLayers()
            .reduce((acc, val) => acc.concat(val.layers), [] as IMapBaseLayer[])
            .find(l => l.id == id)
    );
export const findLayerService = (layer: IMapBaseLayer) => {
    const layersInServices = getBaseLayers();
    const serviceIndex = layersInServices.findIndex(l =>
        l.layers.includes(layer)
    );
    return layersInServices[serviceIndex].id;
};

// TODO: this is uggly, but don't have time to change backend to not need this (nw)
// There are two differents codenames. Here we need the one including service name.
export const makeLayerCodename = (layerId: string) => {
    const layer = findBaseLayerFromId(layerId);
    return layer.map(l => {
        const service = findLayerService(l);
        return `${service}/${l.codename}`;
    });
};

export const getBaseLayerGroups = () => query('data/baselayergroups');

export const findBaseLayerGroup = (id: string) =>
    fromNullable(getBaseLayerGroups().find(lg => lg.id == id));

export const layerOrGroupName = (layerOrGroup: IMapBaselayerOrGroup) => {
    switch (layerOrGroup.tag) {
        case 'layer':
            return findBaseLayerFromId(layerOrGroup.id)
                .map(l => fromRecord(l.name))
                .getOrElse('' as Translated);
        case 'group':
            return findBaseLayerGroup(layerOrGroup.id)
                .map(g => fromRecord(g.name))
                .getOrElse('' as Translated);
    }
};

export const getBaseLayersInfo = (codenames: string[]) =>
    catOptions(codenames.map(findBaseLayer));

export const getCurrentBaseLayers = () =>
    getBaseLayersInfo(getCurrentBaseLayerNames());

export const getCurrentBaseLayersOrGroups =
    (): Option<IMapBaselayerOrGroup> => {
        const layers = getCurrentBaseLayers();
        if (layers.length > 1) {
            const groups = getBaseLayerGroups().filter(g =>
                layers.every(l => g.layers.includes(l.codename))
            );
            if (groups.length >= 1) {
                return index(0, groups).map(g => ({ tag: 'group', id: g.id }));
            }
        }
        return index(0, layers).map(l => ({ tag: 'layer', id: l.id }));
    };

export const getBaseLayerServices = () => getBaseLayers().map(s => s.id);

const makeBaselayerOrGroup = (
    tag: 'layer' | 'group',
    id: string
): IMapBaselayerOrGroup => ({ tag, id });

export const getRecommendedBaseLayersAndGroups = () => {
    const layers = getBaseLayers().reduce(
        (acc, val) =>
            acc.concat(
                val.layers
                    .filter(l => l.recommended)
                    .map(l => makeBaselayerOrGroup('layer', l.id))
            ),
        [] as IMapBaselayerOrGroup[]
    );
    const groups = getBaseLayerGroups()
        .filter(blg => blg.recommended)
        .map(g => makeBaselayerOrGroup('group', g.id));
    return layers.concat(groups);
};

export const getBaseLayersForService = (serviceId: string) =>
    fromNullable(getBaseLayers().find(s => s.id === serviceId))
        .map(s => s.layers)
        .getOrElse([]);

export const getCategories = () => {
    return query('data/categories');
};

export const getSelectedTab = () => query('app/tab/select');

export const getMapCategories = (mapInfo: IMapInfo) => {
    return getCategories().filter(
        cat => mapInfo.categories.indexOf(cat.id) >= 0
    );
};

export const getUnselectedCategories = (mapInfo: IMapInfo) => {
    return getCategories().filter(
        cat => mapInfo.categories.indexOf(cat.id) < 0
    );
};

export const getSplash = () => {
    return query('component/splash');
};

export const getVersionOf = (map: IMapInfo, status: MapStatus) => {
    if (map.status === status) {
        return some(map);
    }
    const mapVersion = getMaps().find(
        info => info.status == status && info.id_origin == map.id_origin
    );
    return fromNullable(mapVersion);
};

export const getVersion = (status: MapStatus) =>
    getMapInfoOpt().chain(current => getVersionOf(current, status));

export const hasPublishedVersion = (info: IMapInfo) =>
    getVersionOf(info, 'published').isSome();
// getMaps().filter(
//     m => m.id_origin == info.id_origin && m.status === 'published'
// ).length > 0;

export const hasSavedVersion = (info: IMapInfo) =>
    getMaps().filter(m => m.id_origin == info.id_origin && m.status === 'saved')
        .length > 0;

export const getSelectedVisibilityOption = () =>
    fromNullable(query('app/visibility-options/select'));

export const getLayerWithInfo = () =>
    fromNullable(query('app/layer-info/select'));
