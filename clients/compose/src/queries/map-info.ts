/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { query } from 'sdi/shape';
import { getMapBaseLayerSelection, IMapBaseLayer, IMapInfo } from 'sdi/source';
import { fromNullable } from 'fp-ts/lib/Option';
import { mapOption } from 'fp-ts/lib/Array';
import { fromRecord } from 'sdi/locale';

import { getCurrentMapOpt, getMapInfoOpt, getMaps } from './app';
import { notEmpty } from 'sdi/util';

const queries = {
    getState() {
        return query('app/map-info/illustration');
    },
};

export type LinkDirection = 'forward' | 'backward';

const formatLink = (maps: Readonly<IMapInfo[]>) => (mid: string) =>
    fromNullable(maps.find(m => m.id === mid));

export const getLinks = (ld: LinkDirection) => {
    const allLinks = query('data/links');
    return getCurrentMapOpt()
        .map(mid => {
            if (!(mid in allLinks)) {
                return [];
            }
            const links = allLinks[mid];
            return links.filter(
                link => mid === (ld === 'forward' ? link.source : link.target)
            );
        })
        .getOrElse([]);
};

const mapOrder = (a: IMapInfo, b: IMapInfo) => {
    const aTitle = fromRecord(a.title);
    const bTitle = fromRecord(b.title);
    if (aTitle === '' && bTitle === '') {
        return 0;
    } else if (aTitle === '') {
        return 1;
    } else if (bTitle === '') {
        return -1;
    } else {
        return fromRecord(a.title).localeCompare(fromRecord(b.title));
    }
};

export const sortMap = (list: IMapInfo[]) => list.sort(mapOrder);

export const getLinkedMaps = (ld: LinkDirection): IMapInfo[] => {
    const fm = formatLink(getMaps());
    const mids = getLinks(ld).map(link =>
        ld === 'forward' ? link.target : link.source
    );
    return sortMap(mapOption(mids, fm)).filter(m => m.status === 'published');
};

export const getMapInfo = (mid: string) =>
    fromNullable(getMaps().find(m => m.id === mid));

export const getUnLinkedMaps = () => {
    const allLinks = query('data/links');
    const maps = getMaps();
    return getCurrentMapOpt()
        .map(mid => {
            if (!(mid in allLinks)) {
                return maps;
            }
            const links = allLinks[mid];
            const isUnlinked = (m: IMapInfo) =>
                undefined === links.find(l => l.target === m.id);

            return sortMap(maps.filter(isUnlinked).filter(m => m.id !== mid));
        })
        .getOrElse([]);
};

export const getPublishedUnlinkedMaps = () =>
    sortMap(getUnLinkedMaps().filter(m => m.status === 'published'));

export const getDraftUnlinkedMaps = () =>
    sortMap(getUnLinkedMaps().filter(m => m.status === 'draft'));

export const getCurrentBaseLayerSelection = () =>
    getMapInfoOpt()
        .chain(info => notEmpty(getMapBaseLayerSelection(info)))
        .getOrElse([]);

export const getCurrentBaseLayerSelectionIds = () =>
    getCurrentBaseLayerSelection().map(bl => bl.id);

export const isBaseLayerSelected = (baseLayer: IMapBaseLayer) =>
    notEmpty(getCurrentBaseLayerSelectionIds())
        .map(selection => selection.includes(baseLayer.id))
        .getOrElse(false);

export default queries;
