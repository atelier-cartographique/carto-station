/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { query } from 'sdi/shape';
import {
    getCurrentSynteticLayerInfoOption,
    getCurrentLayerId,
    getLayerData,
    getCurrentLayerInfo,
} from './app';
import {
    GeometryType,
    getGroup as getSourceGroup,
    getInterval,
    getSubtype,
    isContinuous,
    isDiscrete,
    isLabeled,
    isMarkered,
    isSimple,
    LineDiscreteGroup,
    LineInterval,
    LineStyleConfig,
    PointDiscreteGroup,
    PointStyleConfig,
    PolygonDiscreteGroup,
    PolygonInterval,
    PolygonStyleConfig,
    StyleConfig,
    StyleGroupType,
    PatternAngle,
} from 'sdi/source';
import { getLang } from 'sdi/app';
import { identity } from 'fp-ts/lib/function';
import { fromNullable } from 'fp-ts/lib/Option';

type ValueType = string | boolean | number;

interface GroupGetFn<T extends StyleGroupType, RT> {
    (g: T): RT;
}

interface StyleGetFn<T extends StyleConfig, RT> {
    (g: T): RT;
}

const getStyleForGroup = <T extends StyleGroupType, RT>(
    idx: number,
    defVal: RT,
    f: GroupGetFn<T, RT>
) =>
    getCurrentSynteticLayerInfoOption()
        .map(({ info }) => info.style)
        .fold(defVal, style => {
            if (isDiscrete(style)) {
                const group = getSourceGroup(style, idx);
                if (group) {
                    return f(<T>group);
                }
            } else if (isContinuous(style)) {
                const interval = getInterval(style, idx);
                if (interval) {
                    return f(<T>interval);
                }
            }
            return defVal;
        });

// const getGroupStyles = <T extends StyleGroupType, RT>(
//     defVal: RT,
//     f: GroupGetFn<T, RT>
// ) =>
//     getCurrentLayerInfo()
//         .map(({ info }) => info.style)
//         .fold(defVal, style => {
//             if (isDiscrete(style)) {
//                 style.groups.map((_, idx) => {
//                     const group = getSourceGroup(style, idx);
//                     if (group) {
//                         return f(<T>group);
//                     }
//                     return defVal;
//                 });
//             } else if (isContinuous(style)) {
//                 style.intervals.map((_, idx) => {
//                     const interval = getInterval(style, idx);
//                     if (interval) {
//                         return f(<T>interval);
//                     }
//                     return defVal;
//                 });
//             }
//             return defVal;
//         });

// if (info) {
//     const style = info.style;
//     if (isDiscrete(style)) {
//         const group = getGroup(style, idx);
//         if (group) {
//             return f(<T>group);
//         }
//     }
//     else if (isContinuous(style)) {
//         const interval = getInterval(style, idx);
//         if (interval) {
//             return f(<T>interval);
//         }
//     }
// }
// return defVal;

const getFromStyle = <T extends StyleConfig, RT>(
    defVal: RT,
    f: StyleGetFn<T, RT>
) =>
    getCurrentSynteticLayerInfoOption()
        .map(({ info }) => <T>info.style)
        .fold(defVal, s => f(s));

// {
//     const { info } = getCurrentLayerInfo();
//     if (info) {
//         const style = <T>(info.style);
//         return f(style);
//     }
//     return defVal;
// };

export const getLegendVisibility = () =>
    getCurrentLayerInfo()
        .map(info => info.visibleLegend)
        .getOrElse(true);

export const getGeometryType = (): GeometryType | null =>
    getCurrentSynteticLayerInfoOption()
        .map(({ metadata }) => metadata.geometryType)
        .fold(null, identity);

export const getSelectedMainName = () => {
    const style = getStyle();

    if (style !== null && (isDiscrete(style) || isContinuous(style))) {
        return style.propName;
    }

    return '';
};

export const getValues = (column: string): ValueType[] => {
    const lid = getCurrentLayerId();
    if (lid) {
        return getLayerData(lid).fold(
            () => [],
            o =>
                o.fold([], layer =>
                    layer.features
                        .map(f => {
                            const props = f.properties;
                            if (props && column in props) {
                                return props[column];
                            }
                            return null;
                        })
                        .filter(v => v !== null)
                )
        );
    }
    return [];
};

export const getStyle = () =>
    getCurrentLayerInfo()
        .map(info => info.style)
        .toNullable();

export const getGroup = (idx: number) => {
    const style = getStyle();
    if (style) {
        if (isDiscrete(style)) {
            return getSourceGroup(style, idx);
        } else if (isContinuous(style)) {
            return getInterval(style, idx);
        }
    }
    return null;
};

export const getStrokeWidthForGroup = (idx: number, defVal = 1) =>
    getStyleForGroup(
        idx,
        defVal,
        (
            g:
                | LineDiscreteGroup
                | PolygonDiscreteGroup
                | PolygonInterval
                | LineInterval
        ) => g.strokeWidth
    );

export const getFirstGroupColor = (defVal = '#f5abf0') =>
    fromNullable(
        query('component/legend-editor/interval-color/begin')
    ).getOrElse(defVal);

export const getLastGroupColor = (defVal = '#00729a') =>
    fromNullable(query('component/legend-editor/interval-color/end')).getOrElse(
        defVal
    );

export const getStrokeColorForGroup = (idx: number, defVal = '#000') =>
    getStyleForGroup(
        idx,
        defVal,
        (
            g:
                | LineDiscreteGroup
                | PolygonDiscreteGroup
                | PolygonInterval
                | LineInterval
        ) => g.strokeColor
    );

export const getDashStyleForGroup = (idx: number, defVal = [] as number[]) =>
    getStyleForGroup(
        idx,
        defVal,
        (g: LineDiscreteGroup | LineInterval) => g.dash
    );

export const getFillColorForGroup = (idx: number, defVal = '#000') =>
    getStyleForGroup(
        idx,
        defVal,
        (g: PolygonDiscreteGroup | PolygonInterval) => g.fillColor
    );

export const getPatternForGroup = (idx: number, defVal = false) =>
    getStyleForGroup(
        idx,
        defVal,
        (g: PolygonDiscreteGroup | PolygonInterval) => g.pattern !== undefined
    );

export const getPatternAngleForGroup = (
    idx: number,
    defVal = 0 as PatternAngle
) =>
    getStyleForGroup(idx, defVal, (g: PolygonDiscreteGroup | PolygonInterval) =>
        g.pattern ? g.pattern.angle : defVal
    );

export const getPatternColorForGroup = (idx: number, defVal = 'black') =>
    getStyleForGroup(idx, defVal, (g: PolygonDiscreteGroup | PolygonInterval) =>
        g.pattern ? g.pattern.color : defVal
    );

export const getPatternWidthForGroup = (idx: number, defVal = 1) =>
    getStyleForGroup(idx, defVal, (g: PolygonDiscreteGroup | PolygonInterval) =>
        g.pattern ? g.pattern.width : defVal
    );

// Point Globals
export const getPointConfig = () =>
    query('component/legend-editor').pointConfig;

export const getStrokeWidth = (defVal = 1) => {
    const f: StyleGetFn<LineStyleConfig | PolygonStyleConfig, number> = s =>
        isSimple(s) ? s.strokeWidth : defVal;
    return getFromStyle(defVal, f);
};

export const getStrokeColor = (defVal = '#000000') =>
    getFromStyle(defVal, (s: LineStyleConfig | PolygonStyleConfig) =>
        isSimple(s) ? s.strokeColor : defVal
    );

export const getDashStyle = (defVal = [] as number[]): number[] =>
    getFromStyle(defVal, (s: LineStyleConfig) =>
        isSimple(s) ? s.dash : defVal
    );

export const getFillColor = (defVal = '#000000') =>
    getFromStyle(defVal, (s: PolygonStyleConfig) =>
        isSimple(s) ? s.fillColor : defVal
    );

export const getPattern = () =>
    getFromStyle(false, (s: PolygonStyleConfig) =>
        isSimple(s) ? s.pattern : false
    );

export const getPatternAngle = () =>
    getFromStyle(0, (s: PolygonStyleConfig) =>
        isSimple(s) && s.pattern ? s.pattern.angle : 0
    );
export const getPatternColor = () =>
    getFromStyle('black', (s: PolygonStyleConfig) =>
        isSimple(s) && s.pattern ? s.pattern.color : 'black'
    );

export const getPatternWidth = () =>
    getFromStyle(1, (s: PolygonStyleConfig) =>
        isSimple(s) && s.pattern ? s.pattern.width : 1
    );

export const getMarkerColor = (defVal = '#000000') =>
    getFromStyle(defVal, (s: PointStyleConfig) =>
        isMarkered(s) ? s.marker.color : defVal
    );

export const getMarkerSize = (defVal = 10) =>
    getFromStyle(defVal, (s: PointStyleConfig) =>
        isMarkered(s) ? s.marker.size : defVal
    );

export const getMarkerCodepoint = (defVal = 0xf111) =>
    getFromStyle(defVal, (s: PointStyleConfig) =>
        isMarkered(s) ? s.marker.codePoint : defVal
    );

export const getFontColor = (c = '#000000') =>
    getFromStyle(c, (s: StyleConfig) => (isLabeled(s) ? s.label.color : c));

export const getFontSize = (sz = 12) =>
    getFromStyle(sz, (s: StyleConfig) => (isLabeled(s) ? s.label.size : sz));

export const getPropNameForLabel = (l = '') =>
    getFromStyle(l, (s: StyleConfig) => {
        if (isLabeled(s)) {
            return s.label.propName[getLang()] || l;
        }
        return l;
    });

export const getPositionForLabel = (
    pos: 'above' | 'under' | 'left' | 'right' = 'under'
) =>
    getFromStyle(pos, (s: StyleConfig) => {
        if (isLabeled(s)) {
            const { baseline, align } = s.label;
            if ('center' === align) {
                if ('bottom' === baseline) {
                    return 'above';
                } else if ('top' === baseline) {
                    return 'under';
                }
            } else if ('middle' === baseline) {
                if ('end' === align) {
                    return 'left';
                } else if ('start' === align) {
                    return 'right';
                }
            }
        }
        return pos;
    });

export const getOffsetXForLabel = (x = 0) =>
    getFromStyle(x, (s: StyleConfig) => {
        if (isLabeled(s)) {
            return s.label.xOffset || x;
        }
        return x;
    });

export const getOffsetYForLabel = (y = 0) =>
    getFromStyle(y, (s: StyleConfig) => {
        if (isLabeled(s)) {
            return s.label.yOffset || y;
        }
        return y;
    });

export const getZoomForLabel = (zoom = 3) =>
    getFromStyle(zoom, (s: StyleConfig) => {
        if (isLabeled(s)) {
            return s.label.resLimit || zoom;
        }
        return zoom;
    });

// Marker For Group

export const getMarkerColorForGroup = (idx: number, defVal = '#000000') =>
    getStyleForGroup(idx, defVal, (g: PointDiscreteGroup) => g.marker.color);

export const getMarkerSizeForGroup = (idx: number, defVal = 10) =>
    getStyleForGroup(idx, defVal, (g: PointDiscreteGroup) => g.marker.size);

export const getMarkerCodepointForGroup = (idx: number, defVal = 0xf111) =>
    getStyleForGroup(
        idx,
        defVal,
        (g: PointDiscreteGroup) => g.marker.codePoint
    );

export const getLegendType = () => {
    const style = getStyle();

    if (style !== null) {
        return getSubtype(style);
    }

    return 'simple';
};

export const getSelectedStyleGroup = () =>
    query('component/legend-editor').styleGroupSelected;

export const getStyleGroupEditedValue = () =>
    query('component/legend-editor').styleGroupEditedValue;

export const getAutoClassValue = () =>
    query('component/legend-editor').autoClassValue;

export const getAutoClassNbDecimals = () =>
    query('component/legend-editor').autoClassNbDecimals;

export const getDataInterval = () =>
    query('component/legend-editor/data-interval');

export const getDistinctValues = () =>
    query('component/legend-editor/distinct-values');
