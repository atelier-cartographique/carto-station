import * as debug from 'debug';
import { fromNullable, none, some, Option } from 'fp-ts/lib/Option';

import { query, queryK, subscribe } from 'sdi/shape';
import {
    TemporalReference,
    FreeText,
    isAnchor,
    isTemporalExtent,
    Inspire,
} from 'sdi/source';
import tr, { fromRecord, formatDate, MessageKey } from 'sdi/locale';
import {
    TableDataType,
    TableDataRow,
    TableSource,
    tableQueries,
} from 'sdi/components/table';
import { getAliasOption } from 'sdi/app';

const logger = debug('sdi:queries/metadata');

const layerKeys: MessageKey[] = [
    'title',
    'geometryType',
    // tr.meta('layerId'),
    'md/domain',
    'md/path',
    // tr.meta('publicationStatus'),
    'temporalReference',
    'md/module',
    // tr.meta('pointOfContact'),
    // tr.meta('responsibleOrganisation'),
];

const loadLayerListKeys = () =>
    layerKeys.map(k => getAliasOption(k).getOrElse(tr.core(k)));

// metadata list
// const loadLayerListKeys = () => [
//     tr.compose('layerId'),
//     tr.compose('publicationStatus'),
//     tr.compose('geometryType'),
//     tr.compose('title'),
//     tr.compose('temporalReference'),
//     // tr.compose('pointOfContact'),
//     // tr.compose('responsibleOrganisation'),
// ];

const loadLayerListTypes = (): TableDataType[] => [
    'string',
    'string',
    'string',
    'string',
    'string',
    'string',
    // 'string',
];

export const getTemporalReference = (t: TemporalReference) => {
    if (isTemporalExtent(t)) {
        return formatDate(new Date(Date.parse(t.end)));
    }
    return formatDate(new Date(Date.parse(t.revision)));
};

const splitURL = (rid: string) => {
    const splitStr = rid.split('://', 2);
    if (splitStr.length === 2) {
        const domAndPath = splitStr[1].split('/', 2);
        if (domAndPath.length === 2) {
            return some({
                module: splitStr[0],
                dom: domAndPath[0],
                path: domAndPath[1],
            });
        }
    }
    return none;
};

const getModule = (rid: Option<string>) =>
    rid
        .map(id => splitURL(id).fold('', domAndPath => domAndPath.module))
        .getOrElse('');
export const getDomain = (rid: Option<string>) =>
    rid
        .map(id => splitURL(id).fold('', domAndPath => domAndPath.dom))
        .getOrElse('');
export const getPath = (rid: Option<string>) =>
    rid
        .map(id => splitURL(id).fold('', domAndPath => domAndPath.path))
        .getOrElse('');

const getLayerListData = (mds: Readonly<Inspire[]>): TableDataRow[] => {
    const getFreeText = (ft: FreeText) => {
        if (isAnchor(ft)) {
            return fromRecord(ft.text);
        }

        return fromRecord(ft);
    };

    return mds.map(md => {
        const rid = fromNullable(md.resourceIdentifier);
        const cells = [
            getFreeText(md.resourceTitle),
            md.geometryType,
            getDomain(rid),
            getPath(rid),
            getTemporalReference(md.temporalReference),
            getModule(rid),
            // md.published
            //     ? tr.compose('publishedMetadata')
            //     : tr.compose('draftMetadata'),
            // md.metadataPointOfContact.reduce((acc, poc, idx) => {
            //     const sep = idx === 0 ? '' : ', ';
            //     return `${acc}${sep}${poc.contactName}`;
            // }, ''),
            // md.responsibleOrganisation.reduce((acc, ri, idx) => {
            //     const sep = idx === 0 ? '' : '; ';
            //     return acc + sep + getFreeText(ri.organisationName);
            // }, ''),
        ];
        return { from: md.id, cells };
    });
};

export const getTableSource = subscribe(
    'data/datasetMetadata',
    state =>
        ({
            kind: 'local',
            data: getLayerListData(state),
            keys: loadLayerListKeys(),
            types: loadLayerListTypes(),
        } as TableSource),
    'app/lang'
);

export const getDatasetMetadata = (id: string) =>
    fromNullable(query('data/datasetMetadata').find(md => md.id === id));

export const metadataTableQueries = tableQueries(
    queryK('component/table/metadata'),
    getTableSource
);

export const getSelectedMetadata = () => metadataTableQueries.getSelected();

export const getMetadataRow = (idx: number) => metadataTableQueries.getRow(idx);

export const getSelectedMetadataRow = () =>
    getMetadataRow(getSelectedMetadata());

export const getPersonOfContact = (id: number) =>
    fromNullable(query('data/md/poc').find(poc => poc.id === id));

export const getResponsibleOrg = (id: number) =>
    fromNullable(query('data/md/org').find(org => org.id === id));

logger('loaded');
