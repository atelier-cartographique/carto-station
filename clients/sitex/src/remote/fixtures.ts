export const typo = [
    { id: 1, name: { fr: 'typo1' } },
    { id: 2, name: { fr: 'typo2' } },
    { id: 3, name: { fr: 'typo3' } },
    { id: 4, name: { fr: 'typo4' } },
    { id: 5, name: { fr: 'typo5' } },
];

export const statecode = [
    {
        id: 1,
        name: {
            fr: "bon",
            nl: "goed"
        }
    },
    {
        id: 3,
        name: {
            fr: "ruine",
            nl: "op instorten"
        }
    },
    {
        id: 2,
        name: {
            fr: "dégradé",
            nl: "vervallen"
        }
    }
]

export const occups = [
    { code: '01.00.00.00', name: {}, keywords: [] },
    {
        code: '01.01.00.00',
        name: { fr: 'logement', nl: 'woning' },
        keywords: [],
    },
    { code: '01.02.00.00', name: { fr: 'kot', nl: 'kot' }, keywords: [] },
    {
        code: '01.03.00.00',
        name: { fr: 'meubl\u00e9', nl: 'gemeubelde woning' },
        keywords: [],
    },
    {
        code: '01.04.00.00',
        name: { fr: 'seniorie', nl: 'rustoord' },
        keywords: [],
    },
    {
        code: '01.05.00.00',
        name: { fr: 'orphelinat', nl: 'weeshuis' },
        keywords: [],
    },
    {
        code: '01.06.00.00',
        name: { fr: 'remise de jardin', nl: 'tuinhuisje' },
        keywords: [],
    },
    {
        code: '02.00.00.00',
        name: {
            fr: 'h\u00f4tellerie (g\u00e9n\u00e9ral)',
            nl: 'hotelwezen (algemeen)',
        },
        keywords: [],
    },
    {
        code: '02.01.00.00',
        name: { fr: 'pension', nl: 'pension' },
        keywords: [],
    },
    { code: '02.01.01.00', name: {}, keywords: [] },
    { code: '02.01.02.00', name: {}, keywords: [] },
    { code: '02.01.03.00', name: {}, keywords: [] },
    { code: '02.01.04.00', name: {}, keywords: [] },
    { code: '02.01.05.00', name: {}, keywords: [] },
    {
        code: '02.02.00.00',
        name: { fr: 'flat-h\u00f4tel', nl: 'flat-hotel' },
        keywords: [],
    },
    {
        code: '02.03.00.00',
        name: { fr: 'auberge de jeunesse', nl: 'jeugdherberg' },
        keywords: [],
    },
    { code: '02.04.00.00', name: { fr: 'motel', nl: 'motel' }, keywords: [] },
    {
        code: '03.00.00.00',
        name: { fr: 'bureaux (g\u00e9n\u00e9ral)', nl: 'kantoren (algemeen)' },
        keywords: [],
    },
    {
        code: '03.00.00.99',
        name: { fr: 'bureaux inoccup\u00e9s', nl: 'leegstaande kantoren' },
        keywords: [],
    },
    {
        code: '03.01.00.00',
        name: { fr: 'secteur public belge', nl: 'Belgische openbare sector' },
        keywords: [],
    },
    {
        code: '03.01.01.00',
        name: { fr: 'national', nl: 'nationaal' },
        keywords: [],
    },
    {
        code: '03.01.02.00',
        name: {
            fr: 'R\u00e9gion de Bruxelles-Capitale',
            nl: 'Brussels Hoofdstedelijk Gewest',
        },
        keywords: [],
    },
    {
        code: '03.01.03.00',
        name: { fr: 'R\u00e9gion Wallonne', nl: 'Waals Gewest' },
        keywords: [],
    },
    {
        code: '03.01.04.00',
        name: { fr: 'R\u00e9gion Flamande', nl: 'Vlaams Gewest' },
        keywords: [],
    },
    {
        code: '03.01.05.00',
        name: {
            fr: 'communaut\u00e9 fran\u00e7aise',
            nl: 'Franse Gemeenschap',
        },
        keywords: [],
    },
    {
        code: '03.01.07.00',
        name: {
            fr: 'secteur public - autres',
            nl: 'Belgische openbare sector - andere',
        },
        keywords: [],
    },
    {
        code: '03.02.00.00',
        name: {
            fr: 'union europ\u00e9enne (g\u00e9n\u00e9ral)',
            nl: 'europese unie (algemeen)',
        },
        keywords: [],
    },
    { code: '03.02.01.00', name: { fr: 'europe', nl: 'europa' }, keywords: [] },
    { code: '03.02.01.01', name: {}, keywords: [] },
    { code: '03.02.01.02', name: {}, keywords: [] },
    { code: '03.02.02.00', name: {}, keywords: [] },
    { code: '03.02.03.00', name: { fr: 'europe', nl: 'europa' }, keywords: [] },
    { code: '03.02.04.00', name: {}, keywords: [] },
    {
        code: '03.03.00.00',
        name: {
            fr: 'secteur public international',
            nl: 'internationale openbare sector',
        },
        keywords: [],
    },
    {
        code: '03.05.00.00',
        name: { fr: 'mutuelle', nl: 'ziekenfonds' },
        keywords: [],
    },
    { code: '03.06.00.00', name: {}, keywords: [] },
    {
        code: '03.06.01.00',
        name: { fr: 'union professionnelle', nl: 'unie (beroepsvereniging)' },
        keywords: [],
    },
    {
        code: '03.06.02.00',
        name: {
            fr: 'union \u00e0 but \u00e9conomique',
            nl: 'unie (economische doeleinden)',
        },
        keywords: [],
    },
    {
        code: '03.06.03.00',
        name: { fr: 'syndicat', nl: 'vakbond' },
        keywords: [],
    },
    {
        code: '03.06.04.00',
        name: { fr: 'union sportive', nl: 'sportunie' },
        keywords: [],
    },
    {
        code: '03.06.05.00',
        name: { fr: 'union culturelle', nl: 'cultuurunie' },
        keywords: [],
    },
    {
        code: '03.06.06.00',
        name: { fr: 'parti politique', nl: 'politieke partij' },
        keywords: [],
    },
    {
        code: '03.06.07.00',
        name: {
            fr: "union d'aide \u00e0 la personne",
            nl: 'unie hulp aan de persoon',
        },
        keywords: [],
    },
    { code: '03.06.08.00', name: {}, keywords: [] },
    { code: '03.06.08.01', name: {}, keywords: [] },
    { code: '03.06.08.02', name: {}, keywords: [] },
    { code: '03.06.09.00', name: {}, keywords: [] },
    { code: '03.06.09.01', name: {}, keywords: [] },
    { code: '03.06.09.02', name: {}, keywords: [] },
    { code: '03.06.09.03', name: {}, keywords: [] },
    { code: '03.07.00.00', name: {}, keywords: [] },
    { code: '03.07.02.00', name: {}, keywords: [] },
    {
        code: '03.07.02.01',
        name: { fr: 'avocat', nl: 'advokaat' },
        keywords: [],
    },
    {
        code: '03.07.02.02',
        name: { fr: 'notaire', nl: 'notaris' },
        keywords: [],
    },
    {
        code: '03.07.02.03',
        name: { fr: 'g\u00e9om\u00e8tre', nl: 'landmeter' },
        keywords: [],
    },
    {
        code: '03.07.02.04',
        name: { fr: 'urbaniste', nl: 'urbanist' },
        keywords: [],
    },
    {
        code: '03.07.02.05',
        name: { fr: 'comptable', nl: 'boekhouder' },
        keywords: [],
    },
    {
        code: '03.07.02.06',
        name: { fr: 'traducteur', nl: 'vertaler' },
        keywords: [],
    },
    {
        code: '03.07.02.07',
        name: {
            fr: 'professions lib\u00e9rales non-m\u00e9dicales - autres',
            nl: 'niet-medische vrije beroepen - andere',
        },
        keywords: [],
    },
    {
        code: '03.08.00.00',
        name: { fr: 'entreprise - bureaux', nl: 'onderneming - kantoren' },
        keywords: [],
    },
    { code: '03.09.00.00', name: {}, keywords: [] },
    {
        code: '03.09.01.00',
        name: {
            fr: 'promotion immobili\u00e8re',
            nl: 'promotie immobili\u00ebn',
        },
        keywords: [],
    },
    {
        code: '03.09.02.00',
        name: { fr: 'informatique', nl: 'informatica' },
        keywords: [],
    },
    { code: '03.09.04.00', name: {}, keywords: [] },
    { code: '03.09.04.01', name: { fr: 'conseil', nl: 'raad' }, keywords: [] },
    {
        code: '03.09.04.02',
        name: { fr: 'publicit\u00e9', nl: 'publiciteit' },
        keywords: [],
    },
    {
        code: '03.09.04.03',
        name: {
            fr: 'services aux entreprises - autres',
            nl: 'diensten verleend aan de ondernemingen - andere',
        },
        keywords: [],
    },
    { code: '03.09.04.04', name: {}, keywords: [] },
    { code: '03.09.04.05', name: {}, keywords: [] },
    { code: '03.09.05.00', name: {}, keywords: [] },
    { code: '03.10.00.00', name: {}, keywords: [] },
    {
        code: '03.10.01.00',
        name: {
            fr: 'bureaux : act. mult et vari\u00e9es (une soci\u00e9t\u00e9)',
            nl: 'kantoren: versch. en gemengde activiteiten (een vennootschap)',
        },
        keywords: [],
    },
    {
        code: '03.10.02.00',
        name: {
            fr: 'bureaux : act. mult.et vari\u00e9es (plus.soc./typ.bur)',
            nl: 'kantoren: gemengde en verschillende activiteiten (meerdere venn./type kantoor)',
        },
        keywords: [],
    },
    { code: '03.11.00.00', name: {}, keywords: [] },
    {
        code: '03.11.04.00',
        name: {
            fr: 'bureaux accessoires \u00e0 un \u00e9quipement',
            nl: 'kantoren horend bij een uitrusting',
        },
        keywords: [],
    },
    { code: '03.12.00.00', name: {}, keywords: [] },
    { code: '03.13.00.00', name: {}, keywords: [] },
    {
        code: '03.20.00.00',
        name: {
            fr: 'sc(activit\u00e9 inconnue)',
            nl: 'cv (onbekende activiteit)',
        },
        keywords: [],
    },
    { code: '04.00.00.00', name: {}, keywords: [] },
    {
        code: '04.00.01.99',
        name: { fr: 'vide (usine)', nl: 'fabriek (leegstaand)' },
        keywords: [],
    },
    {
        code: '04.00.02.99',
        name: { fr: 'vide (atelier)', nl: 'werkplaats (leegstaand)' },
        keywords: [],
    },
    {
        code: '04.00.03.99',
        name: { fr: 'vide (entrep\u00f4t)', nl: 'opslagruimte (leegstaande)' },
        keywords: [],
    },
    {
        code: '04.01.00.00',
        name: { fr: 'horticulture', nl: 'tuinbouw' },
        keywords: [],
    },
    {
        code: '04.01.01.00',
        name: { fr: 'horticulture', nl: 'tuinbouw' },
        keywords: [],
    },
    {
        code: '04.01.02.00',
        name: { fr: '\u00e9levage', nl: 'veeteelt' },
        keywords: [],
    },
    {
        code: '04.02.00.00',
        name: { fr: 'aliments', nl: 'voedingswaren' },
        keywords: [],
    },
    {
        code: '04.02.01.00',
        name: { fr: 'viandes industrie', nl: 'vleesindustrie' },
        keywords: [],
    },
    {
        code: '04.02.02.00',
        name: { fr: 'poisson industrie', nl: 'visindustrie' },
        keywords: [],
    },
    {
        code: '04.02.03.00',
        name: {
            fr: 'l\u00e9gumes (conservation, transformation)',
            nl: 'groenten (verwerking, bewaring)',
        },
        keywords: [],
    },
    {
        code: '04.02.04.00',
        name: { fr: 'lait (industrie)', nl: 'melk (industrie)' },
        keywords: [],
    },
    {
        code: '04.02.05.00',
        name: {
            fr: 'industries alimentaires, autres',
            nl: 'voedingsindustrie, andere',
        },
        keywords: [],
    },
    {
        code: '04.02.06.00',
        name: { fr: 'boissons (industrie)', nl: 'drank (industrie)' },
        keywords: [],
    },
    {
        code: '04.03.00.00',
        name: { fr: 'tabac (industrie)', nl: 'tabak (industrie)' },
        keywords: [],
    },
    {
        code: '04.04.00.00',
        name: { fr: 'tissage', nl: 'weverij' },
        keywords: [],
    },
    {
        code: '04.05.00.00',
        name: { fr: 'habillement (industrie)', nl: 'kledingindustrie' },
        keywords: [],
    },
    { code: '04.06.00.00', name: { fr: 'cuir', nl: 'leder' }, keywords: [] },
    {
        code: '04.06.01.00',
        name: {
            fr: 'maroquinerie (fabrication)',
            nl: 'lederwaren (productie)',
        },
        keywords: [],
    },
    {
        code: '04.06.02.00',
        name: { fr: 'chaussures (fabrication)', nl: 'schoenen (productie)' },
        keywords: [],
    },
    { code: '04.07.00.00', name: { fr: 'bois', nl: 'hout' }, keywords: [] },
    {
        code: '04.07.01.00',
        name: {
            fr: 'panneaux en bois (fabrication)',
            nl: 'houtenpanelen (productie)',
        },
        keywords: [],
    },
    {
        code: '04.07.02.00',
        name: { fr: 'menuiseries', nl: 'schrijnwerkerij' },
        keywords: [],
    },
    {
        code: '04.07.03.00',
        name: {
            fr: "bois (fabrication d'objet divers en bois)",
            nl: 'houten voorwerpen (productie)',
        },
        keywords: [],
    },
    {
        code: '04.08.00.00',
        name: { fr: 'papier (industrie)', nl: 'papierindustrie' },
        keywords: [],
    },
    {
        code: '04.09.00.00',
        name: { fr: 'reproduction', nl: 'reproductie' },
        keywords: [],
    },
    {
        code: '04.09.01.00',
        name: { fr: '\u00e9dition', nl: 'uitgeverij' },
        keywords: [],
    },
    {
        code: '04.09.02.00',
        name: { fr: 'imprimerie', nl: 'drukkerij' },
        keywords: [],
    },
    {
        code: '04.09.03.00',
        name: {
            fr: "enregistrements (reproduction d')",
            nl: 'opnamereproductie',
        },
        keywords: [],
    },
    {
        code: '04.10.00.00',
        name: { fr: 'chimie : industrie', nl: 'chemische industrie' },
        keywords: [],
    },
    {
        code: '04.10.01.00',
        name: { fr: 'plastiques (fabrication)', nl: 'plastic (productie)' },
        keywords: [],
    },
    {
        code: '04.10.02.00',
        name: { fr: 'vernis (fabrication)', nl: 'vernis (productie)' },
        keywords: [],
    },
    {
        code: '04.10.03.00',
        name: {
            fr: 'pharmaceutique (industrie)',
            nl: 'farmaceutische industrie',
        },
        keywords: [],
    },
    {
        code: '04.10.04.00',
        name: { fr: 'savons (fabrication)', nl: 'zepen (productie)' },
        keywords: [],
    },
    {
        code: '04.10.05.00',
        name: { fr: 'huiles essentielles', nl: 'vluchtige olie\u00ebn' },
        keywords: [],
    },
    {
        code: '04.10.06.00',
        name: { fr: 'fibres synth\u00e9tiques', nl: 'synthetische vezels' },
        keywords: [],
    },
    {
        code: '04.11.00.00',
        name: { fr: 'plastiques', nl: 'plastic' },
        keywords: [],
    },
    {
        code: '04.12.00.00',
        name: {
            fr: 'produits min\u00e9raux non-m\u00e9talliques',
            nl: 'andere minerale non-ferro producten',
        },
        keywords: [],
    },
    { code: '04.12.01.00', name: { fr: 'verre', nl: 'glas' }, keywords: [] },
    {
        code: '04.12.02.00',
        name: { fr: 'c\u00e9ramique', nl: 'keramische producten' },
        keywords: [],
    },
    {
        code: '04.12.03.00',
        name: { fr: 'pl\u00e2tre', nl: 'plaaster' },
        keywords: [],
    },
    {
        code: '04.12.04.00',
        name: { fr: 'pl\u00e2tre', nl: 'plaaster' },
        keywords: [],
    },
    { code: '04.12.05.00', name: { fr: 'pierre', nl: 'steen' }, keywords: [] },
    {
        code: '04.12.06.00',
        name: {
            fr: 'min\u00e9raux non m\u00e9talliques, autres',
            nl: 'non-ferro minerale producten, andere',
        },
        keywords: [],
    },
    {
        code: '04.13.00.00',
        name: { fr: 'm\u00e9taux', nl: 'metaal' },
        keywords: [],
    },
    {
        code: '04.14.00.00',
        name: { fr: 'machines (fabrication)', nl: 'machines' },
        keywords: [],
    },
    {
        code: '04.14.01.00',
        name: { fr: 'appareils domestiques', nl: 'huishoudtoestellen' },
        keywords: [],
    },
    {
        code: '04.14.02.00',
        name: {
            fr: "machines d'usage sp\u00e9cifique",
            nl: 'machines voor specifiek gebruik',
        },
        keywords: [],
    },
    {
        code: '04.14.03.00',
        name: { fr: 'machines (fabrication )', nl: 'machines' },
        keywords: [],
    },
    {
        code: '04.15.00.00',
        name: { fr: 'mat\u00e9riel informatique', nl: 'informaticamateriaal' },
        keywords: [],
    },
    {
        code: '04.16.00.00',
        name: { fr: 'machines \u00e9lectriques', nl: 'electrische machines' },
        keywords: [],
    },
    { code: '04.16.01.00', name: { fr: 'lampes', nl: 'lampen' }, keywords: [] },
    {
        code: '04.16.02.00',
        name: {
            fr: 'mat\u00e9riel \u00e9lectrique',
            nl: 'electrisch materiaal',
        },
        keywords: [],
    },
    {
        code: '04.17.00.00',
        name: { fr: 't\u00e9l\u00e9vision', nl: 'televisie' },
        keywords: [],
    },
    {
        code: '04.18.00.00',
        name: { fr: 'pr\u00e9cision', nl: 'precisie-instrumenten' },
        keywords: [],
    },
    {
        code: '04.18.01.00',
        name: {
            fr: 'orthop\u00e9die mat\u00e9riel',
            nl: 'orthopedie instrumenten -',
        },
        keywords: [],
    },
    {
        code: '04.18.02.00',
        name: {
            fr: 'techniques : instruments -',
            nl: 'technieken: instrumenten -',
        },
        keywords: [],
    },
    {
        code: '04.18.03.00',
        name: {
            fr: 'photographique : mat\u00e9riel',
            nl: 'fotografie : materiaal',
        },
        keywords: [],
    },
    {
        code: '04.18.04.00',
        name: { fr: 'horlogerie', nl: 'uurwerkmaker' },
        keywords: [],
    },
    {
        code: '04.19.00.00',
        name: { fr: 'v\u00e9hicules automobiles', nl: 'voertuigen' },
        keywords: [],
    },
    {
        code: '04.20.00.00',
        name: { fr: 'transport : mat\u00e9riel', nl: 'vervoer: materiaal' },
        keywords: [],
    },
    { code: '04.21.00.00', name: {}, keywords: [] },
    {
        code: '04.21.01.00',
        name: { fr: 'meubles', nl: 'meubelen' },
        keywords: [],
    },
    {
        code: '04.21.02.00',
        name: { fr: 'pierres pr\u00e9cieuses', nl: 'edelstenen' },
        keywords: [],
    },
    {
        code: '04.21.03.00',
        name: { fr: 'musique : instruments', nl: 'muziek: instrumenten' },
        keywords: [],
    },
    { code: '04.21.04.00', name: {}, keywords: [] },
    {
        code: '04.21.05.00',
        name: { fr: 'jouets', nl: 'speelgoed' },
        keywords: [],
    },
    {
        code: '04.21.06.00',
        name: { fr: 'industries diverses', nl: 'industri\u00eben diverse' },
        keywords: [],
    },
    {
        code: '04.22.00.00',
        name: {
            fr: 'recyclables : mati\u00e8res',
            nl: 'rycycleerbare materialen',
        },
        keywords: [],
    },
    { code: '04.23.00.00', name: {}, keywords: [] },
    {
        code: '04.23.01.00',
        name: { fr: '\u00e9lectricit\u00e9', nl: 'electriciteit' },
        keywords: [],
    },
    { code: '04.23.02.00', name: { fr: 'gaz', nl: 'gas' }, keywords: [] },
    { code: '04.24.00.00', name: { fr: 'eau', nl: 'water' }, keywords: [] },
    {
        code: '04.25.00.00',
        name: {
            fr: 'entrepreneur g\u00e9n\u00e9ral (ateliers)',
            nl: 'algemeen ondernemer (werkplaatsen, ateliers)',
        },
        keywords: [],
    },
    {
        code: '04.26.00.00',
        name: { fr: 'v\u00e9hicules', nl: 'voertuigen' },
        keywords: [],
    },
    {
        code: '04.27.00.00',
        name: { fr: 'commerce de gros', nl: 'groothandel' },
        keywords: [],
    },
    {
        code: '04.27.01.00',
        name: {
            fr: 'produits agricoles (commerce de gros)',
            nl: 'landbouwproducten (groothandel)',
        },
        keywords: [],
    },
    {
        code: '04.27.02.00',
        name: {
            fr: 'produits alimentaires  (commerce de gros)',
            nl: 'voedingsproducten',
        },
        keywords: [],
    },
    {
        code: '04.27.03.00',
        name: {
            fr: 'non aliment : biens de cons. (commerce de gros)',
            nl: 'verbruiksgoederen : niet-voedingsgericht',
        },
        keywords: [],
    },
    {
        code: '04.27.04.00',
        name: { fr: 'grossiste', nl: 'groothandelaar' },
        keywords: [],
    },
    {
        code: '04.28.00.00',
        name: { fr: 'transport', nl: 'vervoer' },
        keywords: [],
    },
    {
        code: '04.28.01.00',
        name: { fr: 'transport ferroviaire', nl: 'spoorwegvervoer' },
        keywords: [],
    },
    {
        code: '04.28.02.00',
        name: { fr: 'd\u00e9m\u00e9nageur', nl: 'verhuizer' },
        keywords: [],
    },
    { code: '04.28.03.00', name: {}, keywords: [] },
    { code: '04.28.04.00', name: {}, keywords: [] },
    { code: '04.29.00.00', name: {}, keywords: [] },
    {
        code: '04.29.01.00',
        name: { fr: 'garde-meubles', nl: 'meubelbewaarplaatsen' },
        keywords: [],
    },
    {
        code: '04.29.02.00',
        name: {
            fr: 'transports :  autres services annexes',
            nl: 'vervoer : andere verwante diensten',
        },
        keywords: [],
    },
    {
        code: '04.30.00.00',
        name: { fr: 'services', nl: 'diensten' },
        keywords: [],
    },
    {
        code: '04.30.01.00',
        name: { fr: 'nettoyage', nl: 'reiniging' },
        keywords: [],
    },
    {
        code: '04.30.02.00',
        name: { fr: 'transport : moyen', nl: 'vervoer : middel' },
        keywords: [],
    },
    {
        code: '04.30.03.00',
        name: { fr: 'machines (location)', nl: 'machines' },
        keywords: [],
    },
    {
        code: '04.30.04.00',
        name: {
            fr: 'personnels : biens',
            nl: 'huishoudelijk gebruik : goederen',
        },
        keywords: [],
    },
    { code: '04.31.00.00', name: {}, keywords: [] },
    {
        code: '04.31.01.00',
        name: { fr: 'plombier (atelier)', nl: 'loodgieter (atelier)' },
        keywords: [],
    },
    {
        code: '04.31.02.00',
        name: {
            fr: 'sculpteur-artiste (atelier)',
            nl: 'beeldhouwer-kunstenaar (atelier)',
        },
        keywords: [],
    },
    {
        code: '04.31.03.00',
        name: {
            fr: 'peintre (mural) : atelier',
            nl: 'schilder (muur) atelier',
        },
        keywords: [],
    },
    {
        code: '04.31.04.00',
        name: {
            fr: 'c\u00e9ramiste (atelier)',
            nl: 'werkplaats van de keramist',
        },
        keywords: [],
    },
    {
        code: '04.31.05.00',
        name: { fr: 'potier (atelier)', nl: 'pottenbakker (atelier)' },
        keywords: [],
    },
    {
        code: '04.31.06.00',
        name: { fr: 'vitrier (atelier)', nl: 'atelier van glasbewerker' },
        keywords: [],
    },
    {
        code: '04.31.07.00',
        name: { fr: 'confection : atelier', nl: 'atelier confectie' },
        keywords: [],
    },
    {
        code: '04.31.08.00',
        name: { fr: 'photo : labo', nl: 'foto: labo' },
        keywords: [],
    },
    {
        code: '04.31.10.00',
        name: { fr: 'atelier (autre)', nl: 'werkplaats: andere' },
        keywords: [],
    },
    { code: '04.32.00.00', name: {}, keywords: [] },
    {
        code: '04.32.01.00',
        name: {
            fr: "entrep\u00f4t \u00e0 l'\u00e9tage d'un commerce au rez",
            nl: 'opslagplaats op een verdieping van een handelszaak op het gelijkvloers',
        },
        keywords: [],
    },
    {
        code: '04.32.02.00',
        name: {
            fr: 'entrep\u00f4t annexe \u00e0 une usine',
            nl: 'opslagplaats bij een fabriek',
        },
        keywords: [],
    },
    {
        code: '04.32.03.00',
        name: {
            fr: 'entrep\u00f4t annexe \u00e0 un atelier artisanal',
            nl: 'opslagplaats horend bij een ambachtelijke werkplaats',
        },
        keywords: [],
    },
    {
        code: '04.32.04.00',
        name: {
            fr: 'archives : entrep\u00f4t',
            nl: 'archieven : opslagplaats',
        },
        keywords: [],
    },
    {
        code: '04.32.10.00',
        name: {
            fr: 'entrep\u00f4t seul, non commerce de gros',
            nl: 'afzonderlijke opslagplaats, geen groothandel',
        },
        keywords: [],
    },
    { code: '04.33.00.00', name: {}, keywords: [] },
    {
        code: '04.33.01.00',
        name: {
            fr: 'laboratoire de recherche, autres',
            nl: 'onderzoekslaboratoria: andere',
        },
        keywords: [],
    },
    {
        code: '04.33.02.00',
        name: {
            fr: "laboratoire d'analyses, autre",
            nl: 'laboratorium voor ontledingen: andere',
        },
        keywords: [],
    },
    {
        code: '04.33.10.00',
        name: {
            fr: 'laboratoires : autres ou activit\u00e9s inconnues',
            nl: 'laboratoria: andere of onbekende activiteiten',
        },
        keywords: [],
    },
    { code: '04.34.00.00', name: {}, keywords: [] },
    {
        code: '04.34.01.00',
        name: {
            fr: 'bureaux accessoires \u00e0 une industrie',
            nl: 'kantoren bij een industrie horend',
        },
        keywords: [],
    },
    {
        code: '04.34.03.00',
        name: {
            fr: 'bureaux access.\u00e0  entrep\u00f4t ou  comm. de gros',
            nl: 'kantoren horend bij opslagplaats of groothandel',
        },
        keywords: [],
    },
    { code: '05.00.00.00', name: {}, keywords: [] },
    {
        code: '05.00.00.99',
        name: {
            fr: 'surface commerciale inoccup\u00e9e',
            nl: 'leegstaande handelsruimte',
        },
        keywords: [],
    },
    {
        code: '05.01.00.00',
        name: { fr: 'hypermarch\u00e9', nl: 'hypermarkt' },
        keywords: [],
    },
    { code: '05.01.01.00', name: {}, keywords: [] },
    { code: '05.02.00.00', name: {}, keywords: [] },
    {
        code: '05.02.01.00',
        name: { fr: 'produits alimentaires', nl: 'voedingswaren' },
        keywords: [],
    },
    {
        code: '05.02.01.01',
        name: { fr: 'p\u00e2tisserie', nl: 'banketbakkerij' },
        keywords: [],
    },
    {
        code: '05.02.01.02',
        name: { fr: 'sp\u00e9culoos', nl: 'speculoos' },
        keywords: [],
    },
    {
        code: '05.02.01.03',
        name: { fr: 'charcuterie', nl: 'slagerij' },
        keywords: [],
    },
    {
        code: '05.02.01.04',
        name: { fr: 'poissonnier', nl: 'vishandel' },
        keywords: [],
    },
    {
        code: '05.02.01.05',
        name: { fr: 'traiteur', nl: 'traiteur' },
        keywords: [],
    },
    {
        code: '05.02.01.06',
        name: { fr: 'superette', nl: 'superette' },
        keywords: [],
    },
    {
        code: '05.02.01.07',
        name: { fr: 'night-shop', nl: 'nachtwinkel' },
        keywords: [],
    },
    {
        code: '05.02.01.08',
        name: { fr: 'di\u00e9t\u00e9tique', nl: 'dieetvoeding' },
        keywords: [],
    },
    { code: '05.02.01.09', name: { fr: 'vin', nl: 'wijn' }, keywords: [] },
    {
        code: '05.02.01.10',
        name: {
            fr: 'produits alimentaires - autres',
            nl: 'voedingswaren - andere',
        },
        keywords: [],
    },
    {
        code: '05.02.01.11',
        name: { fr: "restaurant d'h\u00f4tel", nl: 'restaurant van een hotel' },
        keywords: [],
    },
    { code: '05.02.01.12', name: { fr: 'snack', nl: 'snack' }, keywords: [] },
    {
        code: '05.02.01.13',
        name: { fr: 'friterie', nl: 'frituur' },
        keywords: [],
    },
    {
        code: '05.02.01.14',
        name: { fr: 'tea-room', nl: 'tea-room' },
        keywords: [],
    },
    { code: '05.02.01.15', name: {}, keywords: [] },
    { code: '05.02.01.16', name: {}, keywords: [] },
    {
        code: '05.02.01.20',
        name: { fr: 'triperie', nl: 'slagerswinkel' },
        keywords: [],
    },
    {
        code: '05.02.02.00',
        name: { fr: '\u00e9quipement de la maison', nl: 'uitrusting huis' },
        keywords: [],
    },
    {
        code: '05.02.02.01',
        name: { fr: 'cuisines \u00e9quip\u00e9es', nl: 'ingerichte keukens' },
        keywords: [],
    },
    { code: '05.02.02.02', name: { fr: 'tapis', nl: 'tapijt' }, keywords: [] },
    { code: '05.02.02.03', name: {}, keywords: [] },
    {
        code: '05.02.02.04',
        name: {
            fr: "tissus d'ameublement (ex: Heytens, A la Tentation)",
            nl: 'stof voor bekleding',
        },
        keywords: [],
    },
    {
        code: '05.02.02.05',
        name: { fr: 'sanitaire', nl: 'sanitair' },
        keywords: [],
    },
    {
        code: '05.02.02.06',
        name: { fr: 'quincaillerie', nl: 'ijzerwaren' },
        keywords: [],
    },
    {
        code: '05.02.02.07',
        name: { fr: 'cuisine (appareils)', nl: 'keuken (toestellen)' },
        keywords: [],
    },
    { code: '05.02.02.08', name: {}, keywords: [] },
    {
        code: '05.02.02.09',
        name: { fr: '\u00e9lectricit\u00e9', nl: 'electriciteit' },
        keywords: [],
    },
    {
        code: '05.02.02.10',
        name: { fr: 'vaisselle', nl: 'keukengerei' },
        keywords: [],
    },
    {
        code: '05.02.02.11',
        name: { fr: 'brocanteur', nl: 'handelaar in rariteiten' },
        keywords: [],
    },
    { code: '05.02.02.12', name: {}, keywords: [] },
    {
        code: '05.02.02.13',
        name: { fr: 'tableaux', nl: 'schilderijen' },
        keywords: [],
    },
    {
        code: '05.02.02.14',
        name: { fr: 'souvenir', nl: 'souvenir' },
        keywords: [],
    },
    { code: '05.02.02.15', name: { fr: 'jardin', nl: 'tuin' }, keywords: [] },
    { code: '05.02.02.16', name: {}, keywords: [] },
    {
        code: '05.02.02.17',
        name: { fr: "produit d'entretien", nl: 'onderhoudsproduct' },
        keywords: [],
    },
    {
        code: '05.02.02.18',
        name: { fr: 'informatique', nl: 'informatica' },
        keywords: [],
    },
    {
        code: '05.02.02.19',
        name: {
            fr: '\u00e9quipement de la maison - autres',
            nl: 'huisuitrusting - andere',
        },
        keywords: [],
    },
    {
        code: '05.02.02.20',
        name: {
            fr: 'articles de m\u00e9nage de luxe',
            nl: 'huishoudartikelen (luxe)',
        },
        keywords: [],
    },
    {
        code: '05.02.02.21',
        name: { fr: 'construction : mat\u00e9riel', nl: 'bouw : materiaal' },
        keywords: [],
    },
    {
        code: '05.02.03.00',
        name: {
            fr: 'services \u00e0 la personne (g\u00e9n\u00e9ral)',
            nl: 'diensten voor de persoon (algemeen)',
        },
        keywords: [],
    },
    { code: '05.02.03.01', name: {}, keywords: [] },
    { code: '05.02.03.02', name: {}, keywords: [] },
    { code: '05.02.03.03', name: {}, keywords: [] },
    { code: '05.02.03.04', name: {}, keywords: [] },
    { code: '05.02.03.05', name: { fr: 'tabac', nl: 'tabak' }, keywords: [] },
    {
        code: '05.02.03.06',
        name: { fr: 'pharmacie', nl: 'apoteek' },
        keywords: [],
    },
    {
        code: '05.02.03.07',
        name: { fr: 'optique', nl: 'optiek' },
        keywords: [],
    },
    {
        code: '05.02.03.08',
        name: { fr: 'coiffeur', nl: 'kapsalon' },
        keywords: [],
    },
    {
        code: '05.02.03.09',
        name: { fr: 'wasserette', nl: 'wasserette' },
        keywords: [],
    },
    {
        code: '05.02.03.10',
        name: { fr: 'serrurier (avec vitrine)', nl: 'slotenmaker' },
        keywords: [],
    },
    {
        code: '05.02.03.11',
        name: { fr: 'pompes fun\u00e8bres', nl: 'begrafenisondernemer' },
        keywords: [],
    },
    {
        code: '05.02.03.12',
        name: {
            fr: 'services \u00e0 la personne - autres',
            nl: 'diensten voor de persoon - andere',
        },
        keywords: [],
    },
    {
        code: '05.02.03.20',
        name: { fr: 'mercerie', nl: 'gaven- en bandwinkel' },
        keywords: [],
    },
    {
        code: '05.02.03.21',
        name: { fr: 'tissus pour la confection', nl: 'confectiestoffen' },
        keywords: [],
    },
    { code: '05.02.03.22', name: { fr: 'cuir', nl: 'leder' }, keywords: [] },
    {
        code: '05.02.03.23',
        name: { fr: 'gants', nl: 'handschoenen' },
        keywords: [],
    },
    {
        code: '05.02.03.24',
        name: { fr: 'pedicure', nl: 'voetschoonheidsverzorging' },
        keywords: [],
    },
    {
        code: '05.02.03.25',
        name: { fr: 'pressing', nl: 'stomerij' },
        keywords: [],
    },
    {
        code: '05.02.03.26',
        name: { fr: 'articles en laine', nl: 'artikelen in wol' },
        keywords: [],
    },
    {
        code: '05.02.04.00',
        name: {
            fr: 'loisirs (g\u00e9n\u00e9ral)',
            nl: 'vrije tijd (algemeen)',
        },
        keywords: [],
    },
    { code: '05.02.04.01', name: { fr: 'sport', nl: 'sport' }, keywords: [] },
    {
        code: '05.02.04.02',
        name: { fr: 'p\u00eache', nl: 'visvangst' },
        keywords: [],
    },
    {
        code: '05.02.04.03',
        name: { fr: 'animal de compagnie', nl: 'dier' },
        keywords: [],
    },
    {
        code: '05.02.04.04',
        name: { fr: 'papeterie', nl: 'papierhandel' },
        keywords: [],
    },
    { code: '05.02.04.05', name: {}, keywords: [] },
    {
        code: '05.02.04.06',
        name: { fr: 'disquaire', nl: 'platenhandel' },
        keywords: [],
    },
    {
        code: '05.02.04.07',
        name: { fr: 'musique instruments', nl: 'muziekinstrumenten' },
        keywords: [],
    },
    {
        code: '05.02.04.08',
        name: { fr: 'jouet', nl: 'speelgoed' },
        keywords: [],
    },
    {
        code: '05.02.04.09',
        name: { fr: 'mat\u00e9riel pour artistes', nl: 'kunstmateriaal' },
        keywords: [],
    },
    {
        code: '05.02.04.10',
        name: { fr: 'loisirs - autres', nl: 'vrije tijd en cultuur - andere' },
        keywords: [],
    },
    { code: '05.02.04.11', name: {}, keywords: [] },
    {
        code: '05.02.04.20',
        name: { fr: 'scoutisme :articles', nl: 'padvinderij: artikelen' },
        keywords: [],
    },
    {
        code: '05.02.04.21',
        name: { fr: "toilettage d'animaux", nl: 'toilet maken van dieren' },
        keywords: [],
    },
    {
        code: '05.02.04.22',
        name: { fr: 'timbres-poste', nl: 'postzegels' },
        keywords: [],
    },
    {
        code: '05.02.04.23',
        name: { fr: 'journaux', nl: 'kranten' },
        keywords: [],
    },
    { code: '05.02.05.00', name: {}, keywords: [] },
    {
        code: '05.02.05.01',
        name: { fr: 'mazout', nl: 'stookolie' },
        keywords: [],
    },
    {
        code: '05.02.05.02',
        name: { fr: 'station-service', nl: 'benzinestation' },
        keywords: [],
    },
    {
        code: '05.02.05.03',
        name: { fr: 'voiture : vente', nl: 'auto' },
        keywords: [],
    },
    {
        code: '05.02.05.04',
        name: { fr: 'v\u00e9lo', nl: 'fiets' },
        keywords: [],
    },
    {
        code: '05.02.05.05',
        name: { fr: 'car-wash', nl: 'car-wash' },
        keywords: [],
    },
    {
        code: '05.02.05.06',
        name: { fr: 'service minute', nl: 'snel-service' },
        keywords: [],
    },
    {
        code: '05.02.05.07',
        name: { fr: 'v\u00e9lo (accessoires)', nl: 'fiets-onderdeel' },
        keywords: [],
    },
    {
        code: '05.02.05.08',
        name: {
            fr: 'mat\u00e9riels de transport - autres',
            nl: 'vervoermateriaal - andere',
        },
        keywords: [],
    },
    { code: '05.02.06.00', name: {}, keywords: [] },
    {
        code: '05.02.06.01',
        name: { fr: 'agence de banque', nl: 'bankagentschap' },
        keywords: [],
    },
    {
        code: '05.02.06.02',
        name: { fr: 'mutuelle', nl: 'ziekenfonds' },
        keywords: [],
    },
    {
        code: '05.02.06.03',
        name: { fr: 'agence de voyages', nl: 'reisagentschap' },
        keywords: [],
    },
    {
        code: '05.02.06.04',
        name: { fr: 'immobilier', nl: 'immobili\u00ebn' },
        keywords: [],
    },
    {
        code: '05.02.06.05',
        name: { fr: 'tierc\u00e9', nl: 'paardenwedren' },
        keywords: [],
    },
    {
        code: '05.02.06.06',
        name: { fr: 'agence matrimoniale', nl: 'huwelijkskantoor' },
        keywords: [],
    },
    {
        code: '05.02.06.07',
        name: { fr: 'interim', nl: 'interim' },
        keywords: [],
    },
    {
        code: '05.02.06.08',
        name: {
            fr: 'imprimerie (commerce de d\u00e9tail)',
            nl: 'drukkerij (detailhandel)',
        },
        keywords: [],
    },
    {
        code: '05.02.06.09',
        name: {
            fr: 'services \u00e0 caract\u00e8re commercial - autres',
            nl: 'diensten met handelskarakter - andere',
        },
        keywords: [],
    },
    {
        code: '05.02.06.10',
        name: { fr: 'discoth\u00e8que', nl: 'discoteek' },
        keywords: [],
    },
    {
        code: '05.02.06.11',
        name: { fr: 'vid\u00e9o-club', nl: 'vid\u00e9o-club' },
        keywords: [],
    },
    {
        code: '05.02.06.12',
        name: { fr: 'peepshow', nl: 'peepshow' },
        keywords: [],
    },
    {
        code: '05.02.06.13',
        name: {
            fr: 'services \u00e0 caract\u00e8re commercial : autres',
            nl: 'diensten met handelskarakter : andere',
        },
        keywords: [],
    },
    { code: '05.02.06.20', name: { fr: 'fax', nl: 'fax' }, keywords: [] },
    {
        code: '05.02.06.21',
        name: {
            fr: 'bordel type dame dans la vitrine',
            nl: 'bordeel: type dame in uitstalraam',
        },
        keywords: [],
    },
    { code: '05.03.00.00', name: {}, keywords: [] },
    { code: '05.11.00.00', name: {}, keywords: [] },
    {
        code: '05.11.02.00',
        name: {
            fr: 'bureaux accessoires \u00e0 un commerce de d\u00e9tail',
            nl: 'kantoren horend bij een kleinhandel',
        },
        keywords: [],
    },
    { code: '06.00.00.00', name: {}, keywords: [] },
    {
        code: '06.01.00.00',
        name: { fr: 'maternelle', nl: 'peutertuin' },
        keywords: [],
    },
    {
        code: '06.02.00.00',
        name: { fr: 'primaire', nl: 'lager' },
        keywords: [],
    },
    {
        code: '06.03.00.00',
        name: { fr: 'secondaire', nl: 'middelbaar' },
        keywords: [],
    },
    {
        code: '06.04.00.00',
        name: { fr: 'universit\u00e9', nl: 'universiteit' },
        keywords: [],
    },
    {
        code: '06.05.00.00',
        name: { fr: 'institut sup\u00e9rieur', nl: 'hoger instituut' },
        keywords: [],
    },
    {
        code: '06.06.00.00',
        name: { fr: 'enseignement sp\u00e9cial', nl: 'bijzonder onderwijs' },
        keywords: [],
    },
    {
        code: '06.07.00.00',
        name: { fr: 'acad\u00e9mie', nl: 'academie' },
        keywords: [],
    },
    {
        code: '06.08.00.00',
        name: { fr: 'formation (autres)', nl: 'vorming (andere)' },
        keywords: [],
    },
    { code: '06.09.00.00', name: {}, keywords: [] },
    { code: '07.00.00.00', name: {}, keywords: [] },
    {
        code: '07.01.00.00',
        name: { fr: 'h\u00f4pital', nl: 'hospitaal' },
        keywords: [],
    },
    {
        code: '07.02.00.00',
        name: { fr: 'polyclinique', nl: 'polykliniek' },
        keywords: [],
    },
    {
        code: '07.03.00.00',
        name: { fr: 'maison m\u00e9dicale', nl: 'medisch huis' },
        keywords: [],
    },
    { code: '07.04.00.00', name: {}, keywords: [] },
    {
        code: '07.04.01.00',
        name: {
            fr: 'profession para-m\u00e9dicale',
            nl: 'para-medisch beroep',
        },
        keywords: [],
    },
    {
        code: '07.04.01.01',
        name: { fr: 'podologue', nl: 'podoloog' },
        keywords: [],
    },
    {
        code: '07.04.01.02',
        name: { fr: 'dentiste', nl: 'tandarts' },
        keywords: [],
    },
    {
        code: '07.04.01.03',
        name: { fr: 'kin\u00e9', nl: 'kine' },
        keywords: [],
    },
    {
        code: '07.04.01.04',
        name: { fr: 'v\u00e9t\u00e9rinaire', nl: 'dierenarts' },
        keywords: [],
    },
    {
        code: '07.04.01.05',
        name: {
            fr: 'profession lib\u00e9rale m\u00e9dicale - autres',
            nl: 'medisch vrij beroep - andere',
        },
        keywords: [],
    },
    { code: '07.05.00.00', name: {}, keywords: [] },
    { code: '08.00.00.00', name: {}, keywords: [] },
    {
        code: '08.01.00.00',
        name: { fr: 'cr\u00eache', nl: 'kribbe' },
        keywords: [],
    },
    { code: '08.02.00.00', name: { fr: 'cpas', nl: 'ocmw' }, keywords: [] },
    {
        code: '08.03.00.00',
        name: { fr: 'maison de jeunes', nl: 'jeugdhuis' },
        keywords: [],
    },
    {
        code: '08.04.00.00',
        name: { fr: 'culture', nl: 'cultuur' },
        keywords: [],
    },
    { code: '08.05.00.00', name: {}, keywords: [] },
    { code: '08.06.00.00', name: {}, keywords: [] },
    { code: '08.07.00.00', name: {}, keywords: [] },
    { code: '09.00.00.00', name: {}, keywords: [] },
    {
        code: '09.01.00.00',
        name: { fr: 'mus\u00e9e', nl: 'museum' },
        keywords: [],
    },
    {
        code: '09.02.00.00',
        name: { fr: 'th\u00e9\u00e2tre', nl: 'theater' },
        keywords: [],
    },
    {
        code: '09.03.00.00',
        name: { fr: 'centre culturel', nl: 'cultureel centrum' },
        keywords: [],
    },
    {
        code: '09.04.00.00',
        name: { fr: 'cin\u00e9ma', nl: 'cinema' },
        keywords: [],
    },
    { code: '09.05.00.00', name: {}, keywords: [] },
    { code: '09.05.01.00', name: { fr: 'squash', nl: 'squash' }, keywords: [] },
    { code: '09.05.02.00', name: { fr: 'gym', nl: 'gym' }, keywords: [] },
    {
        code: '09.05.03.00',
        name: { fr: 'piscine couverte', nl: 'overdekt zwembad' },
        keywords: [],
    },
    { code: '09.05.04.00', name: { fr: 'judo', nl: 'judo' }, keywords: [] },
    {
        code: '09.05.05.00',
        name: { fr: 'bowling', nl: 'bowling' },
        keywords: [],
    },
    { code: '09.05.06.00', name: { fr: 'sport', nl: 'sport' }, keywords: [] },
    { code: '09.05.07.00', name: {}, keywords: [] },
    {
        code: '09.06.00.00',
        name: { fr: 'salle de r\u00e9union', nl: 'vergaderzaal' },
        keywords: [],
    },
    {
        code: '09.07.00.00',
        name: {
            fr: 'dessin : cours priv\u00e9s, loisirs',
            nl: 'tekenlessen (vrijteijdsbesteding, priv\u00e9)',
        },
        keywords: [],
    },
    {
        code: '09.08.00.00',
        name: { fr: 'auto-\u00e9cole', nl: 'autorijschool' },
        keywords: [],
    },
    { code: '09.11.00.00', name: {}, keywords: [] },
    {
        code: '09.11.05.00',
        name: { fr: 'centre culturel', nl: 'Cultureel centrum' },
        keywords: [],
    },
    { code: '09.11.06.00', name: { fr: 'sports', nl: 'sport' }, keywords: [] },
    { code: '09.11.07.00', name: {}, keywords: [] },
    { code: '10.00.00.00', name: {}, keywords: [] },
    {
        code: '10.01.00.00',
        name: { fr: '\u00e9glise', nl: 'kerk' },
        keywords: [],
    },
    { code: '10.02.00.00', name: { fr: 'temple', nl: 'tempel' }, keywords: [] },
    {
        code: '10.03.00.00',
        name: { fr: 'synagogue', nl: 'synagoog' },
        keywords: [],
    },
    {
        code: '10.04.00.00',
        name: { fr: 'mosqu\u00e9e', nl: 'moskee' },
        keywords: [],
    },
    {
        code: '10.05.00.00',
        name: { fr: 'culte - autre', nl: 'godsdientsbeoefening - andere' },
        keywords: [],
    },
    { code: '10.06.00.00', name: {}, keywords: [] },
    { code: '11.00.00.00', name: {}, keywords: [] },
    { code: '11.01.00.00', name: {}, keywords: [] },
    { code: '11.01.01.00', name: { fr: 'foire', nl: 'beurs' }, keywords: [] },
    {
        code: '11.01.02.00',
        name: { fr: 'parlement', nl: 'parlement' },
        keywords: [],
    },
    {
        code: '11.01.02.01',
        name: { fr: 'Parlement europ\u00e9en', nl: 'europees parlement' },
        keywords: [],
    },
    {
        code: '11.01.03.00',
        name: { fr: 'gendarmerie', nl: 'rijkswacht' },
        keywords: [],
    },
    { code: '11.01.04.00', name: { fr: 'gare', nl: 'station' }, keywords: [] },
    { code: '11.01.05.00', name: { fr: 'tec', nl: 'tec' }, keywords: [] },
    {
        code: '11.01.06.00',
        name: {
            fr: "\u00e9quip.d'utilit\u00e9 publ. d'int\u00e9r\u00eat f\u00e9d. ou r\u00e9g.:autre",
            nl: 'uitrusting voor openbaar nut van Federaal of gewestelijk belang : andere',
        },
        keywords: [],
    },
    { code: '11.02.00.00', name: {}, keywords: [] },
    {
        code: '11.02.01.00',
        name: { fr: 'maison communale', nl: 'gemeentehuis' },
        keywords: [],
    },
    {
        code: '11.02.01.01',
        name: { fr: 'commune', nl: 'gemeentelijk bestuur' },
        keywords: [],
    },
    { code: '11.02.02.00', name: { fr: 'poste', nl: 'post' }, keywords: [] },
    {
        code: '11.02.03.00',
        name: { fr: 'police', nl: 'politie' },
        keywords: [],
    },
    {
        code: '11.02.04.00',
        name: { fr: 'pompiers', nl: 'brandweer' },
        keywords: [],
    },
    {
        code: '11.02.05.00',
        name: { fr: 'belgacom', nl: 'Belgacom' },
        keywords: [],
    },
    {
        code: '11.02.06.00',
        name: {
            fr: "\u00e9quip.d'utilit\u00e9 publ. d'int\u00e9r\u00eat communal : autre",
            nl: 'uitrusting voor openbaar nut : andere',
        },
        keywords: [],
    },
    { code: '11.03.00.00', name: {}, keywords: [] },
    { code: '12.00.00.00', name: {}, keywords: [] },
    {
        code: '12.01.00.00',
        name: { fr: 'parking public', nl: 'openbare parking' },
        keywords: [],
    },
    {
        code: '12.02.00.00',
        name: { fr: 'parking priv\u00e9', nl: 'private parking' },
        keywords: [],
    },
    {
        code: '13.00.00.00',
        name: {
            fr: 'sans utilisation apparente',
            nl: 'zonder duidelijk gebruik',
        },
        keywords: [],
    },
    {
        code: '14.00.00.00',
        name: { fr: 'box garage', nl: 'garage box' },
        keywords: [],
    },
    {
        code: '15.00.00.00',
        name: { fr: 'cimeti\u00e8re', nl: 'kerkhof' },
        keywords: [],
    },
    {
        code: '16.00.00.00',
        name: {
            fr: 'd\u00e9pot ou entrepot \u00e0 ciel ouvert',
            nl: 'loods of opslagplaats in de open lucht',
        },
        keywords: [],
    },
    {
        code: '17.00.00.00',
        name: {
            fr: 'parking \u00e0 ciel ouvert',
            nl: 'parking in de open lucht',
        },
        keywords: [],
    },
    { code: '18.00.00.00', name: {}, keywords: [] },
    { code: '19.00.00.00', name: {}, keywords: [] },
    {
        code: '19.01.00.00',
        name: { fr: 'consulat', nl: 'consulaat' },
        keywords: [],
    },
    {
        code: '19.02.00.00',
        name: {
            fr: 'repr\u00e9sentation \u00e9trang\u00e8re',
            nl: 'buitenlandse vertegenwoordiging',
        },
        keywords: [],
    },
    { code: '19.02.01.00', name: {}, keywords: [] },
    {
        code: '20.00.00.00',
        name: { fr: 'immat\u00e9riel', nl: 'NULL' },
        keywords: [],
    },
    {
        code: '20.01.00.00',
        name: { fr: 'agence pub', nl: 'NULL' },
        keywords: [],
    },
    {
        code: '20.02.00.00',
        name: { fr: 'Call Center', nl: 'NULL' },
        keywords: [],
    },
    {
        code: '20.03.00.00',
        name: { fr: 'Centre formation', nl: 'NULL' },
        keywords: [],
    },
    { code: '20.04.00.00', name: { fr: 'R&D', nl: 'NULL' }, keywords: [] },
    { code: '21.00.00.00', name: {}, keywords: [] },
    { code: '21.01.00.00', name: {}, keywords: [] },
    { code: '21.02.00.00', name: {}, keywords: [] },
    { code: '21.03.00.00', name: {}, keywords: [] },
    { code: '21.04.00.00', name: {}, keywords: [] },
    { code: '21.05.00.00', name: {}, keywords: [] },
    { code: '21.06.00.00', name: {}, keywords: [] },
    { code: '21.07.00.00', name: {}, keywords: [] },
    { code: '21.08.00.00', name: {}, keywords: [] },
    { code: '21.09.00.00', name: {}, keywords: [] },
    { code: '21.10.00.00', name: {}, keywords: [] },
    { code: '21.11.00.00', name: {}, keywords: [] },
    { code: '21.12.00.00', name: {}, keywords: [] },
    { code: '21.13.00.00', name: {}, keywords: [] },
    { code: '21.14.00.00', name: {}, keywords: [] },
    { code: '21.15.00.00', name: {}, keywords: [] },
    { code: '21.16.00.00', name: {}, keywords: [] },
    { code: '21.17.00.00', name: {}, keywords: [] },
    { code: '21.18.00.00', name: {}, keywords: [] },
    { code: '21.19.00.00', name: {}, keywords: [] },
    { code: '21.20.00.00', name: {}, keywords: [] },
    { code: '21.21.00.00', name: {}, keywords: [] },
    { code: '21.22.00.00', name: {}, keywords: [] },
    { code: '21.23.00.00', name: {}, keywords: [] },
    { code: '21.24.00.00', name: {}, keywords: [] },
    { code: '21.25.00.00', name: {}, keywords: [] },
    { code: '21.26.00.00', name: {}, keywords: [] },
    { code: '21.27.00.00', name: {}, keywords: [] },
    { code: '22.00.00.00', name: {}, keywords: [] },
    { code: '22.01.00.00', name: {}, keywords: [] },
    { code: '22.02.00.00', name: {}, keywords: [] },
    { code: '22.03.00.00', name: {}, keywords: [] },
    { code: '22.04.00.00', name: {}, keywords: [] },
    { code: '22.05.00.00', name: {}, keywords: [] },
    { code: '22.06.00.00', name: {}, keywords: [] },
    { code: '22.07.00.00', name: {}, keywords: [] },
    { code: '22.08.00.00', name: {}, keywords: [] },
    { code: '22.09.00.00', name: {}, keywords: [] },
    { code: '23.00.00.00', name: {}, keywords: [] },
    { code: '24.00.00.00', name: {}, keywords: [] },
    { code: '25.00.00.00', name: {}, keywords: [] },
    { code: '25.01.00.00', name: {}, keywords: [] },
    { code: '25.02.00.00', name: {}, keywords: [] },
    { code: '25.03.00.00', name: {}, keywords: [] },
    { code: '25.04.00.00', name: {}, keywords: [] },
    { code: '25.05.00.00', name: {}, keywords: [] },
    { code: '26.00.00.00', name: {}, keywords: [] },
    { code: '26.01.00.00', name: {}, keywords: [] },
    { code: '26.02.00.00', name: {}, keywords: [] },
    { code: '26.03.00.00', name: {}, keywords: [] },
    { code: '26.04.00.00', name: {}, keywords: [] },
    { code: '26.05.00.00', name: {}, keywords: [] },
    { code: '26.06.00.00', name: {}, keywords: [] },
    { code: '27.00.00.00', name: {}, keywords: [] },
    { code: '28.00.00.00', name: {}, keywords: [] },
    { code: '28.01.00.00', name: {}, keywords: [] },
    { code: '28.02.00.00', name: {}, keywords: [] },
    { code: '28.03.00.00', name: {}, keywords: [] },
    { code: '28.04.00.00', name: {}, keywords: [] },
    { code: '28.05.00.00', name: {}, keywords: [] },
    { code: '28.06.00.00', name: {}, keywords: [] },
    { code: '29.00.00.00', name: {}, keywords: [] },
    { code: '29.01.00.00', name: {}, keywords: [] },
    { code: '29.02.00.00', name: {}, keywords: [] },
    { code: '29.03.00.00', name: {}, keywords: [] },
    { code: '30.00.00.00', name: {}, keywords: [] },
    { code: '30.01.00.00', name: {}, keywords: [] },
    { code: '30.02.00.00', name: {}, keywords: [] },
    { code: '30.03.00.00', name: {}, keywords: [] },
    { code: '31.00.00.00', name: {}, keywords: [] },
    { code: '31.01.00.00', name: {}, keywords: [] },
    { code: '31.02.00.00', name: {}, keywords: [] },
    { code: '31.03.00.00', name: {}, keywords: [] },
    { code: '31.04.00.00', name: {}, keywords: [] },
    { code: '32.00.00.00', name: {}, keywords: [] },
    { code: '32.01.00.00', name: {}, keywords: [] },
    { code: '32.02.00.00', name: {}, keywords: [] },
    { code: '32.03.00.00', name: {}, keywords: [] },
    { code: '32.04.00.00', name: {}, keywords: [] },
    { code: '32.05.00.00', name: {}, keywords: [] },
    { code: '32.06.00.00', name: {}, keywords: [] },
    { code: '32.07.00.00', name: {}, keywords: [] },
    { code: '33.00.00.00', name: {}, keywords: [] },
    { code: '34.00.00.00', name: {}, keywords: [] },
    { code: '34.01.00.00', name: {}, keywords: [] },
    { code: '34.02.00.00', name: {}, keywords: [] },
    { code: '35.00.00.00', name: {}, keywords: [] },
    { code: '36.00.00.00', name: {}, keywords: [] },
    { code: '37.00.00.00', name: {}, keywords: [] },
    { code: '37.01.00.00', name: {}, keywords: [] },
    { code: '37.02.00.00', name: {}, keywords: [] },
    { code: '37.03.00.00', name: {}, keywords: [] },
    { code: '37.04.00.00', name: {}, keywords: [] },
    { code: '38.00.00.00', name: {}, keywords: [] },
    {
        code: '40.00.00.00',
        name: { fr: 'Haute technologie', nl: 'Hight Tech' },
        keywords: [],
    },
    {
        code: '41.00.00.00',
        name: { fr: 'bureau dans appartement', nl: 'NULL' },
        keywords: [],
    },
];
