import { getLang } from 'sdi/app';
import { BUTTON, DIV, INPUT, NodeOrOptional } from 'sdi/components/elements';
import tr, { concat, fromRecord } from 'sdi/locale';
import { foldRemote } from 'sdi/source';
import {
    reloadBlocks,
    setExtraLayerTab,
    setLegendType,
    toggleExtraLayerVisibility,
} from '../events';
import { SitexMessageKey } from '../locale';
import {
    findExtraMetadata,
    getBlocks,
    getExtraLayers,
    getExtraLayerTab,
    getLegendType,
    getView,
    LegendType,
    withNetworkMedium,
    extraLayerInfo,
} from '../queries';
import colors from '../queries/map/colors';
import { makeLabel } from 'sdi/components/button';
import { noop, notEmpty } from 'sdi/util';
import { SitexLayerInfo } from '../remote';
import { legendRenderer } from 'sdi/components/legend';

const pattern = (color: string) =>
    `repeating-linear-gradient(-45deg, ${color}, ${color} 1.5px, #fff 1.5px, #fff 5px)`;

const item = (color: string, labelKey: SitexMessageKey) =>
    DIV(
        'legend-item polygon',
        DIV({
            className: 'item-style',
            style: {
                background: pattern(color),
                border: `1px solid ${colors.grey}`,
            },
        }),
        DIV('item-label', tr.sitex(labelKey))
    );

const renderLegendSync = () =>
    DIV(
        'legend-body',
        item(colors.blue, 'SyncedElements'),
        item(colors.red, 'not-yet-syncedElements')
    );

const renderLegendStatus = () =>
    DIV(
        'legend-body',
        item(colors.grey, 'datastateInitial'),
        item(colors.yellow, 'datastateOngoing'),
        item(colors.blue, 'datastateValidated')
    );

const reloadBlocksBtn = makeLabel('load', 2, () => tr.sitex('clickToRefresh'));

const renderReloadBlocks = () =>
    withNetworkMedium().map(qual =>
        reloadBlocksBtn(reloadBlocks, `reload-blocks network-${qual}`)
    );

const renderBlockHeaderWithBlock = (_data: unknown, timestamp: number) => {
    const dtf = new Intl.DateTimeFormat(getLang(), {
        dateStyle: 'full',
        timeStyle: 'long',
    });
    const datetime = new Date(timestamp);
    return DIV(
        'block-info',
        DIV(
            'info-text',
            concat(tr.sitex('blocks-last-update'), ': ', dtf.format(datetime))
        ),
        renderReloadBlocks()
    );
};
const renderBlockHeaderLoading = () =>
    DIV('block-info', DIV('info-text', tr.sitex('blocksAreLoading')));

const renderBlockHeaderError = (_err: unknown) =>
    DIV(
        'block-info',
        DIV('info-text', tr.sitex('blocksCouldNotBeLoaded')),
        renderReloadBlocks()
    );

const renderBlockHeaderNone = () =>
    DIV(
        'block-info',
        DIV('info-text', tr.core('loadingData')),
        renderReloadBlocks()
    ); // FIXME

const renderBlockHeader = foldRemote(
    renderBlockHeaderNone,
    renderBlockHeaderLoading,
    renderBlockHeaderError,
    renderBlockHeaderWithBlock
);

const renderLegendBlocks = () => {
    return DIV(
        'legend-body',
        renderBlockHeader(getBlocks()),
        item(colors.transparent, 'block-done-0'),
        item(colors.yellow, 'block-done-30'),
        item(colors.green, 'block-done-70'),
        item(colors.blue, 'block-done-100')
    );
};

const title = (legendType: LegendType) => {
    switch (legendType) {
        case 'sync':
            return tr.sitex('legend/sync');
        case 'blocks':
            return tr.sitex('legend/blocks');
        case 'status':
            return tr.sitex('legend/status');
    }
};

const renderTab = (legendType: LegendType, current: LegendType) =>
    legendType === current
        ? BUTTON(`tab tab-selected tab-${legendType}`, title(legendType))
        : BUTTON(
              {
                  className: `tab  tab-${legendType} active`,
                  onClick: () => {
                      setLegendType(legendType);
                      setExtraLayerTab(false);
                  },
              },
              title(legendType)
          );

const renderExtraTab = (selected: boolean) =>
    BUTTON(
        {
            className: `tab tab-extra ${selected ? 'tab-selected' : 'active'}`,
            onClick: () => setExtraLayerTab(!selected),
        },
        tr.sitex('tab/extra-layers')
    );

const renderHeader = (current: LegendType) =>
    DIV(
        'tabs__list legend-header',
        renderTab('sync', current),
        renderTab('status', current),
        renderTab('blocks', current),
        notEmpty(getExtraLayers()).map(() => renderExtraTab(getExtraLayerTab()))
    );

const wrap = (current: LegendType, node: NodeOrOptional) =>
    DIV('legend__wrapper', renderHeader(current), node);

const extraRender = legendRenderer(findExtraMetadata, getView, noop);

const renderExtraLayer = (info: SitexLayerInfo & { visible: boolean }) =>
    DIV(
        'extra-layer  sidebar__wrapper  styles-wrapper',
        DIV(
            'map-legend__wrapper',
            DIV(
                'name',
                INPUT({
                    type: 'checkbox',
                    checked: info.visible,
                    onChange: event =>
                        toggleExtraLayerVisibility(
                            info,
                            event.currentTarget.checked
                        ),
                }),
                fromRecord(info.name)
            ),

            ...extraRender([extraLayerInfo(info)])
        )
    );

const renderLegendExtraLayers = () =>
    DIV('legend-body', ...getExtraLayers().map(renderExtraLayer));

export const renderLegend = () => {
    if (getExtraLayerTab()) {
        return wrap(getLegendType(), renderLegendExtraLayers());
    }
    switch (getLegendType()) {
        case 'sync':
            return wrap('sync', renderLegendSync());
        case 'blocks':
            return wrap('blocks', renderLegendBlocks());
        case 'status':
            return wrap('status', renderLegendStatus());
    }
};

export default renderLegend;
