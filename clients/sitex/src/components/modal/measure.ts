import { fromNullable, none, Option, some } from 'fp-ts/lib/Option';
import { DIV, H2 } from 'sdi/components/elements';
import { IMapOptions, create } from 'sdi/map';
import tr from 'sdi/locale';
import { ModalRender } from 'sdi/components/modal';
import { makeLabel } from 'sdi/components/button';
import { noop } from 'sdi/util';
import {
    BaseLayerCode,
    getMeasureBaseLayers,
    getMeasureBaseLayerCode,
    getMeasured,
    getMeasureInteraction,
    getMeasureMapInfo,
    getMeasureLayerInfos,
    getMeasureSnapLayer,
    getMeasureType,
    getMeasureView,
    measureMapName,
} from 'sitex/src/queries';
import {
    clearMeasure,
    setMeasureBaseLayerCode,
    switchMeasureType,
    updateMeasureCoordinates,
    updateMeasureView,
} from 'sitex/src/events';
import {
    buttonStartMeasureLine,
    buttonStartMeasurePolygon,
    buttonStopMeasure,
} from '../buttons';
import { iife } from 'sdi/lib';

const closeModalButton = makeLabel('close', 2, () => tr.core('close'));

let mapUpdate: Option<() => void> = none;

const attachMap = (element: HTMLElement | null) => {
    fromNullable(element).map(element => {
        const options: IMapOptions = {
            element: null,
            getBaseLayer: getMeasureBaseLayers,
            getView: getMeasureView,
            getMapInfo: getMeasureMapInfo,
            getMapLayerInfo: getMeasureLayerInfos,

            updateView: updateMeasureView,
            setScaleLine: noop,
        };
        const { update, setTarget, measurable } = create(measureMapName, {
            ...options,
            element,
        });

        measurable(
            {
                stopMeasuring: noop,
                snap: {
                    layer: getMeasureSnapLayer,
                    pixelTolerance: 20,
                },
                updateMeasureCoordinates,
            },
            getMeasureInteraction
        );

        mapUpdate = some(update);
        setTarget(element);
    });
};
const renderMeasured = () =>
    DIV(`measure-box`, DIV('measure-value', getMeasured()));

const renderBaseLayerSwitch = () => {
    const [other, className] = iife((): [BaseLayerCode, string] => {
        if (getMeasureBaseLayerCode() === 'urbis_gray') {
            return ['urbis_ortho', 'switch-background-ortho'];
        }
        return ['urbis_gray', 'switch-background-bn'];
    });
    return DIV(
        { className: 'switcher' },
        DIV({ className, onClick: () => setMeasureBaseLayerCode(other) })
    );
};

const renderMeasureActions = () =>
    DIV(
        'actions',
        getMeasureType() === 'Polygon'
            ? buttonStartMeasurePolygon(noop, 'disabled')
            : buttonStartMeasurePolygon(switchMeasureType),
        getMeasureType() === 'LineString'
            ? buttonStartMeasureLine(noop, 'disabled')
            : buttonStartMeasureLine(switchMeasureType),
        buttonStopMeasure(clearMeasure)
    );

const renderBody = () => {
    mapUpdate.map(u => u());
    return DIV(
        {
            className: 'map-wrapper',
            'aria-label': `${tr.core('map')}`,
        },
        DIV('tools', renderMeasureActions()),
        renderMeasured(),
        renderBaseLayerSwitch(),
        DIV({
            id: measureMapName,
            key: measureMapName,
            className: 'map',
            ref: attachMap,
        })
    );
};

const renderHeaderModal = () => H2({}, tr.sitex('measure/title'));

const renderFooterModal = (close: () => void) =>
    DIV('', closeModalButton(close));

export const renderMeasure: ModalRender = {
    header: renderHeaderModal,
    footer: renderFooterModal,
    body: renderBody,
};
