import { DIV, H1, H2, BUTTON } from 'sdi/components/elements';
import tr from 'sdi/locale';

const side = () =>
    DIV(
        'side__wrapper',
        H1('', tr.sitex('syncTitle')),
        DIV(
            'summary__wrapper',
            H2('', tr.sitex('syncSummary')),
            DIV('', '~nb building total'),
            DIV('', '~nb building edited')
        ),
        DIV(
            'map-legend',
            H2('', tr.core('legend')),
            DIV('', '~Edited'),
            DIV('', '~Warning (to be defined)')
        ),
        DIV(
            'sync-widget',
            H2('', '~Network Status'),
            DIV('', '~You are off-line'),
            DIV('', '~You are on-line', BUTTON('', '~Synchronize now'))
        )
    );

const mapWrapper = () => DIV('mapmap', '~map');

export const renderSync = () => DIV('sync', mapWrapper(), side());

export default renderSync;
