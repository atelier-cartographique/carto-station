import { DIV, H1, H2 } from 'sdi/components/elements';
import tr, { formatNumber } from 'sdi/locale';
import { markdown } from 'sdi/ports/marked';
import { navigateGroups, syncAll } from '../events';
import {
    getOnlineQuality,
    getSitexBuildings,
    getSitexParcels,
    getSurveyType,
    getTouchedBuildings,
    getTouchedParcels,
    getUrbisBuildings,
    getUrbisParcels,
} from '../queries';
import { buttonNavigateGroups, buttonSyncElements } from './buttons';
import mainMap from './main-map';
import renderLegend from './legend';

const noBlockSelected = () =>
    DIV('summary summary--empty', markdown(tr.sitex('noBlockSelected')));

const globalBuildingSummary = () =>
    DIV(
        'summary summary--global',
        H2('', tr.sitex('SelectionSummary')),
        DIV(
            'kv__wrapper',
            DIV('key', tr.sitex('NumberOfLoadedSitexBuildings')),
            DIV('value', formatNumber(getSitexBuildings().length))
        )
        // DIV(
        //     'kv__wrapper',
        //     DIV('key', tr.sitex('NumberOfLoadedUrbisBuildings')),
        //     DIV('value', formatNumber(getUrbisBuildings().length))
        // )
    );

const globalParcelSummary = () =>
    DIV(
        'summary summary--global',
        H2('', tr.sitex('SelectionSummary')),
        DIV(
            'kv__wrapper',
            DIV('key', tr.sitex('NumberOfLoadedSitexParcels')),
            DIV('value', formatNumber(getSitexParcels().length))
        )
        // DIV(
        //     'kv__wrapper',
        //     DIV('key', tr.sitex('NumberOfLoadedUrbisParcels')),
        //     DIV('value', formatNumber(getUrbisParcels().length))
        // )
    );

const globalPublicSpaceSummary = () =>
    DIV(
        'summary summary--global',
        H2('', tr.sitex('SelectionSummary'))
        // TODO
    );

const globalSummary = () =>
    getSurveyType().map(surveyType => {
        switch (surveyType) {
            case 'building':
                return globalBuildingSummary();
            case 'parcel':
                return globalParcelSummary();
            case 'public-space':
                return globalPublicSpaceSummary();
        }
    });

const renderGlobalSummary = () =>
    getUrbisBuildings().length > 0 || getUrbisParcels().length > 0
        ? globalSummary()
        : noBlockSelected();

const renderNetworkStatus = () => {
    switch (getOnlineQuality()) {
        case 'good':
            return DIV(
                'network-status status-good',
                buttonSyncElements(syncAll)
            );
        case 'medium':
            return DIV(
                'network-status status-medium',
                buttonSyncElements(syncAll)
            );
        case 'bad':
            return DIV(
                'network-status status-bad',
                tr.sitex('statusOnlineBadSync')
            );
        case 'critical':
            return DIV(
                'network-status status-critical',
                tr.sitex('statusOfflineSync')
            );
    }
};

const renderSyncBlock = () =>
    DIV(
        'sync__wrapper',
        H2('', tr.sitex('ToBeSynchronized')),
        DIV(
            'sync__content',
            DIV(
                'kv__wrapper sync-item building',
                DIV('key', tr.sitex('buildings')),
                DIV('value', formatNumber(getTouchedBuildings().length))
            ),
            DIV(
                'kv__wrapper sync-item building',
                DIV('key', tr.sitex('parcels')),
                DIV('value', formatNumber(getTouchedParcels().length))
            ),
            DIV(
                'kv__wrapper sync-item building',
                DIV('key', tr.sitex('publicSpaces')),
                DIV('value', '0')
            )
        ),
        renderNetworkStatus()
    );

const renderSyncWrapper = () =>
    getTouchedBuildings().length > 0 || getTouchedParcels().length > 0
        ? renderSyncBlock()
        : '';

const renderSidebar = () =>
    DIV(
        'sidebar',
        H1('', tr.sitex('sitexName')),
        renderGlobalSummary(),
        renderLegend(),
        renderSyncWrapper(),
        DIV('group-manager', buttonNavigateGroups(navigateGroups))
    );

const render = () => DIV('index', renderSidebar(), mainMap());

export default render;
