import { DIV } from 'sdi/components/elements';

import tools from './tools';
import map from './map';

const renderGroups = () => DIV('groups', tools(), map());

export default renderGroups;
