import { Option } from 'fp-ts/lib/Option';
import { DIV } from 'sdi/components/elements';
import tr from 'sdi/locale';
import {
    navigateGroups,
    navigateProp,
} from 'sitex/src/events';
import {
    BuildingProp,
} from 'sitex/src/remote';
import { btnNextInput, buttonNavigateGroups } from '../../buttons';
import { nextProp } from './common';

export type BuildingGroupWidgetState = {
    lookup: string | null;
};

export const defaultBuildingGroupWidgetState =
    (): BuildingGroupWidgetState => ({
        lookup: null,
    });

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const renderGroupInput = (prop: BuildingProp, _opt: Option<string>) => {
    return DIV(
        'input input__wrapper--group',
        DIV(
            'info',
            tr.sitex('groups/inBuildingView'),
            buttonNavigateGroups(navigateGroups)
        ),
        DIV(
            'input__actions',
            nextProp(prop).map(next => btnNextInput(() => navigateProp(next)))
        )
    );
};
