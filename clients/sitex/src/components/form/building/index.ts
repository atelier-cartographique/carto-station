import { DIV } from 'sdi/components/elements';
import { renderFormBuildingInfo } from './infos';
import { renderFormBuildingInput } from './editor';
import { getCurrentBuildingForm } from 'sitex/src/queries';

const renderNotLoaded = () => DIV('form form--building', 'OOps');

export const renderBuilding = () =>
    getCurrentBuildingForm().foldL(renderNotLoaded, () =>
        DIV(
            'form form--building',
            renderFormBuildingInfo(),
            renderFormBuildingInput()
        )
    );

export default renderBuilding;
