import tr, { formatNumber } from 'sdi/locale';
import { DIV, H1 } from 'sdi/components/elements';
import {
    CompressesedAddress,
    CompressesedAddressComp,
} from 'sitex/src/queries';

// fixme gather duplicate code fragments from sitex components,
//       mostly render functions.
//       good places would be in fields or in info header (here)

// fixme typo in function name and argument type
export const renderCommpressedAddressComp = (addr: CompressesedAddressComp) =>
    DIV(
        'compressed-address',
        addr.comps
            .map(
                ({ streetName, numbers }) =>
                    `${streetName} ${numbers.join(' - ')}`
            )
            .join('; '),
        `, ${addr.municipality}`
    );

export const renderUrbisAddress = (address: CompressesedAddress) =>
    DIV(
        `address__container  compress-${address.comps.length}`,
        H1('', ...address.comps.map(renderCommpressedAddressComp)),
        DIV(
            'capakey',
            `${tr.sitex('localCadastralplot')} : ${address.capakeys.join('; ')}`
        )
    );

export const renderUrbisID = (urbisId: number) =>
    DIV('info-id', `${tr.sitex('urbisID')} : ${urbisId}`);

export const renderGroundArea = (area: number) =>
    DIV('info-area', `${tr.sitex('groundarea')} : ${formatNumber(area)}`);
