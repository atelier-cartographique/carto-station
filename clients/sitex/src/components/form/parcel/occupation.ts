import { index } from 'fp-ts/lib/Array';
import { fromNullable, Option } from 'fp-ts/lib/Option';
import { DIV, H2, LABEL, SPAN } from 'sdi/components/elements';
import { noop, tryNumber, withM2 } from 'sdi/util';

import {
    clearParcelOccupationLookup,
    clearParcelOccupationPrefix,
    insertNewParcelOccupation,
    navigateProp,
    prefixToCode,
    removeParcelOccupation,
    setParcelFormFieldValue,
    setParcelOccupationIndex,
    setParcelOccupationPrefix,
} from 'sitex/src/events';
import {
    findOccupationByCode,
    getCurrentParcelForm,
    getOccupationIndex,
    getOccupationPrefix,
    getParcelOccupationArea,
    getParcelRemainingArea,
    getParcelUnbuiltArea,
} from 'sitex/src/queries';
import {
    newParcelOccupation,
    ParcelOccupation,
    ParcelOccupationIO,
    ParcelProp,
} from 'sitex/src/remote';
import {
    CodePrefix,
    parcelOccupationValue,
    renderOccupationBreadcrumb,
    renderOccupationLookup,
    renderOccupationOptions,
    renderOccupationVacant,
    updateParcelOccupation,
} from '../../field';
import {
    btnNextInput,
    buttonAddOccupParcel,
    buttonDeleteOccupation,
} from '../../buttons';
import tr, { fromRecord } from 'sdi/locale';
import {
    inputLongText,
    inputNumber,
    inputText,
    options,
} from 'sdi/components/input';
import { nextProp } from './common';

type OccupUpdate = (o: ParcelOccupation) => void;

const initial = (prop: ParcelProp) => {
    const set = setParcelFormFieldValue(prop);
    return DIV(
        'empty occupation',
        H2('', tr.sitex('occupancys')),
        DIV(
            'input',
            buttonAddOccupParcel(() => {
                getCurrentParcelForm().map(form =>
                    set(
                        parcelOccupationValue([
                            newParcelOccupation(getParcelRemainingArea(form)),
                        ])
                    )
                );
                setParcelOccupationIndex(0);
                clearParcelOccupationPrefix();
            })
        )
    );
};

const occupationCodeBreadcrumb = (
    occup: ParcelOccupation,
    update: OccupUpdate
) =>
    renderOccupationBreadcrumb(
        occup.occupcode,
        getOccupationPrefix('parcel'),
        (n, parts) => () => {
            const newCode = prefixToCode(parts.slice(0, n) as CodePrefix);
            if (newCode === null) {
                clearParcelOccupationPrefix();
                clearParcelOccupationLookup();
            } else {
                setParcelOccupationPrefix(newCode);
                clearParcelOccupationLookup();
                update({ ...occup, occupcode: newCode });
            }
        }
    );

const occupationOptions = (occup: ParcelOccupation, update: OccupUpdate) =>
    renderOccupationOptions('parcel', occup, code => {
        update({ ...occup, occupcode: code });
        setParcelOccupationPrefix(code);
        clearParcelOccupationLookup();
    });

const occupationLookup = (occup: ParcelOccupation, update: OccupUpdate) =>
    renderOccupationLookup('parcel', (code: string) => {
        update({ ...occup, occupcode: code });
        setParcelOccupationPrefix(code);
        clearParcelOccupationLookup();
    });

const renderOccupationCode = (occup: ParcelOccupation, update: OccupUpdate) =>
    DIV(
        'step-occupation-code__wrapper',
        H2('subtitle', tr.sitex('occupationCode')),
        DIV(
            'occup-code__wrapper',
            occupationCodeBreadcrumb(occup, update),
            occupationOptions(occup, update)
        ),
        occupationLookup(occup, update)
    );

const renderArea = (
    occup: ParcelOccupation,
    occupIndex: number,
    update: OccupUpdate
) =>
    LABEL(
        '',
        inputNumber(
            options<number>(
                `parcel-occupation-area-${occupIndex}`,
                () => fromNullable(occup.area),
                area => update({ ...occup, area })
            )
        ),
        SPAN('', tr.sitex('occupationArea'))
    );

const renderVacant = (occup: ParcelOccupation, update: OccupUpdate) =>
    renderOccupationVacant(occup, () =>
        update({
            ...occup,
            vacant: !fromNullable(occup.vacant).getOrElse(false),
        })
    );

const renderDescription = (
    occup: ParcelOccupation,
    occupIndex: number,
    update: OccupUpdate
) =>
    LABEL(
        'description',
        inputLongText(
            () => occup.description ?? '',
            description => update({ ...occup, description }),
            { key: `occup-description-${occupIndex}` }
        ),
        SPAN('', tr.sitex('occupationDescription'))
    );

const renderOwner = (
    occup: ParcelOccupation,
    occupIndex: number,
    update: OccupUpdate
) =>
    LABEL(
        '',
        inputText(
            options<string>(
                `occup-description-${occupIndex}`,
                () => fromNullable(occup.owner),
                owner => update({ ...occup, owner })
            )
        ),
        SPAN('', tr.sitex('owner'))
    );

const renderNb = (
    title: string,
    value: Option<number>,
    update: (n: number) => void
) =>
    LABEL(
        '',
        inputNumber(
            options<number>(
                `parcel-occupation-nb-${title}`,
                () => value,
                update
            )
        ),
        SPAN('', title)
    );

const renderLevelInfos = (
    occup: ParcelOccupation,
    occupIndex: number,
    update: OccupUpdate
) =>
    DIV(
        'level-infos',
        H2('subtitle', tr.sitex('occupancyInfos')),
        renderArea(occup, occupIndex, update),
        renderVacant(occup, update),
        DIV(
            'occup-input-row',
            renderNb(
                tr.sitex('nbparkcar'),
                fromNullable(occup.nbparkcar),
                nbparkcar => update({ ...occup, nbparkcar })
            ),
            renderNb(
                tr.sitex('nbparkbike'),
                fromNullable(occup.nbparkbike),
                nbparkbike => update({ ...occup, nbparkbike })
            ),
            renderNb(
                tr.sitex('nbparkpmr'),
                fromNullable(occup.nbparkpmr),
                nbparkpmr => update({ ...occup, nbparkpmr })
            ),
            renderNb(
                tr.sitex('nbelecplug'),
                fromNullable(occup.nbelecplug),
                nbelecplug => update({ ...occup, nbelecplug })
            )
        ),
        renderOwner(occup, occupIndex, update),
        renderDescription(occup, occupIndex, update)
    );

const renderlevelActions = (occup: ParcelOccupation, occupIndex: number) =>
    DIV(
        'level-actions',
        ParcelOccupationIO.decode(occup).fold(
            () => buttonAddOccupParcel(noop, 'disabled error'),
            () =>
                buttonAddOccupParcel(() => {
                    const newIndex = occupIndex + 1;
                    insertNewParcelOccupation(newIndex);
                    setParcelOccupationIndex(newIndex);
                    clearParcelOccupationPrefix();
                })
        )
    );

const renderMainInput = (
    prop: ParcelProp,
    occup: ParcelOccupation,
    occupIndex: number,
    update: OccupUpdate
) =>
    DIV(
        'input__wrapper--occupation',
        DIV(
            'level-content',
            renderLevelInfos(occup, occupIndex, update),
            renderOccupationCode(occup, update)
        ),
        renderlevelActions(occup, occupIndex),
        nextProp(prop).map(next => btnNextInput(() => navigateProp(next)))
    );

const renderHeader = (occup: ParcelOccupation, i: number, isSelected: string) =>
    DIV(
        {
            className: `occup-summary ${isSelected}`,
            onClick: () => {
                setParcelOccupationIndex(i);
                fromNullable(occup.occupcode).foldL(
                    clearParcelOccupationPrefix,
                    setParcelOccupationPrefix
                );
            },
        },
        DIV(
            'code',
            fromNullable(occup.occupcode)
                .chain(findOccupationByCode)
                .map<string | null>(({ name }) => fromRecord(name))
                .getOrElse(occup.occupcode)
        ),
        tryNumber(occup.area).map(area => DIV('area', withM2(area))),
        buttonDeleteOccupation(() => removeParcelOccupation(i))
    );

export const renderSurfaceAreaAndTotalOccupiedInfo = () =>
    getCurrentParcelForm().map(form =>
        DIV(
            'area-box',
            DIV(
                'total',
                tr.sitex('occupationTotalArea'),
                withM2(getParcelOccupationArea(form))
            ),
            fromNullable(form.builtArea.value).map(area =>
                DIV(
                    '',
                    DIV(
                        'un-built',
                        tr.sitex('unbuiltArea'),
                        withM2(getParcelUnbuiltArea(form))
                    ),
                    DIV('built', tr.sitex('builtArea'), withM2(area))
                )
            )
        )
    );

export const renderOccupationInput = (
    prop: ParcelProp,
    opt: Option<ParcelOccupation[]>
) => {
    const occups = opt.getOrElse([]);
    if (occups.length === 0) {
        return initial(prop);
    }
    const occupIndex = getOccupationIndex('parcel');
    return index(occupIndex, occups).map(() => {
        const update = (o: ParcelOccupation) => {
            const set = setParcelFormFieldValue(prop);
            const newValue = occups.map((occup, i) => {
                if (i === occupIndex) {
                    return o;
                }
                return occup;
            });
            set(updateParcelOccupation(newValue));
        };

        const renderInput = renderMainInput(
            prop,
            occups[occupIndex],
            occupIndex,
            update
        );

        const elements = occups.map((occup, i) => {
            const isSelected = i === occupIndex ? 'selected' : '';
            return renderHeader(occup, i, isSelected);
        });

        return DIV(
            'occups parcel',
            DIV(
                'occup-summary__wrapper',
                H2('', tr.sitex('occupancys')),
                renderSurfaceAreaAndTotalOccupiedInfo(),
                DIV('occup-levels', ...elements.reverse())
            ),
            renderInput
        );
    });
};

export default renderOccupationInput;
