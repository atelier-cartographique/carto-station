import { flatten, index } from 'fp-ts/lib/Array';
import { ParcelProp } from 'sitex/src/remote';
import { parcelPropGroups } from '.';

export const nextProp = (prop: ParcelProp) => {
    const props = flatten(parcelPropGroups.map(({ props }) => props));
    const propIndex = props.indexOf(prop);

    return index(propIndex + 1, props);
};
