import { DIV } from 'sdi/components/elements';
import { ParcelProp } from 'sitex/src/remote';
import { SitexMessageKey } from 'sitex/src/locale';
import { renderFormParcelInfo } from './infos';
import { renderFormParcelInput } from './editor';
import { fromNullable } from 'fp-ts/lib/Option';

export type GroupName = 'general' | 'secondary' | 'details';

export type ParcelPropGroup = {
    name: GroupName;
    props: ParcelProp[];
};

export const parcelPropGroups: ParcelPropGroup[] = [
    {
        name: 'general',
        props: [
            'fenced',
            'project',
            'groundarea',
            'situation',
            'lining',
            'imperviousarea',
            'geostate',
        ],
    },
    {
        name: 'secondary',
        props: ['occupancys', 'geom', 'files'],
    },
    {
        name: 'details',
        props: ['description', 'internaldesc', 'versiondesc', 'datastate'],
    },
];

export const findParcelPropGroup = (groupName: GroupName) =>
    fromNullable(parcelPropGroups.find(({ name }) => name === groupName));

export const propParcelToMessageKey = (prop: ParcelProp): SitexMessageKey => {
    // TODO
    return prop as SitexMessageKey;
};

export const renderParcel = () =>
    DIV('form form--parcel', renderFormParcelInfo(), renderFormParcelInput());

export default renderParcel;
