import { DIV } from 'sdi/components/elements';

export const render = () => DIV('form public-space', 'public-space form');

export default render;
