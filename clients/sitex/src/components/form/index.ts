import { getSurveyType } from 'sitex/src/queries';
import building from './building';
import parcel from './parcel';
import publicSpace from './public-space';

export default () =>
    getSurveyType().map(surveyType => {
        switch (surveyType) {
            case 'building':
                return building();
            case 'parcel':
                return parcel();
            case 'public-space':
                return publicSpace();
        }
    });
