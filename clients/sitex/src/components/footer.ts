import { DIV, SPAN } from 'sdi/components/elements';
import { markdown } from 'sdi/ports/marked';
import tr from 'sdi/locale';

const credits = () => SPAN('credits', markdown(tr.sitex('sitexCredit')));

const render = () => DIV('footer', credits());

export default render;
