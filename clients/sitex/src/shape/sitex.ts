import { Nullable } from 'sdi/util';
import {
    Feature,
    FeatureCollection,
    remoteNone,
    RemoteResource,
    uuid,
} from 'sdi/source';
import { Layout, OccupNode } from '../events';
import {
    BaseLayerCode,
    BuildingFormData,
    initialMesureState,
    LegendType,
    ParcelFormData,
} from '../queries';
import type {
    Building,
    BuildingFileArray,
    Dictionary,
    Occupation,
    Parcel,
    ParcelFileArray,
    SitexFile,
    SurveyProp,
    SurveyType,
    UrbisAddress,
    BuildingGroup,
    BlockBuildingGroup,
    SitexLayerInfo,
} from '../remote';
import {
    BuildingGroupWidgetState,
    defaultBuildingGroupWidgetState,
} from '../components/form/building/group';

import type { Status } from 'sitex/src/components/status';

import {
    defaultInteraction,
    IMapViewData,
    Interaction,
    InteractionMeasure,
} from 'sdi/map';
import { defaultMapView } from './map';
import {
    defaultOccupationWidgetState,
    OccupationWidgetState,
} from '../components/field';

declare module 'sdi/shape' {
    export interface IShape {
        'app/layout': Layout;
        'app/fullscreen': boolean;
        'app/status': Status[];
        'app/online': number;
        'app/legend': LegendType;
        'form/survey-type': Nullable<SurveyType>;
        'form/id': Nullable<uuid>;
        'form/prop': Nullable<SurveyProp>;

        'form/block/current': Nullable<number>;
        'form/buildings': BuildingFormData[];
        'form/building/current': Nullable<BuildingFormData>;
        'form/building/occupation': OccupationWidgetState;
        'form/building/files': BuildingFileArray;
        'form/building/upload-file': RemoteResource<SitexFile>;
        'form/building/group': BuildingGroupWidgetState;
        'form/parcels': ParcelFormData[];
        'form/parcel/current': Nullable<ParcelFormData>;
        'form/parcel/occupation': OccupationWidgetState;
        'form/parcel/files': ParcelFileArray;
        'form/parcel/upload-file': RemoteResource<SitexFile>;

        'form/geometry/interaction': Interaction;
        'form/geometry/view': IMapViewData;

        'form/groups/interaction': Interaction;
        'form/groups/view': IMapViewData;
        'form/groups/current': Nullable<string>; // an idbuild for a sitex building
        'form/groups/name': Nullable<string>;

        'history/building': RemoteResource<Building[]>;
        'history/parcel': RemoteResource<Parcel[]>;

        'data/buildings': Building[];
        'data/parcels': Parcel[];
        'data/urbis-buildings': Feature[];
        'data/urbis-address': UrbisAddress[];
        'data/urbis-parcels': Feature[];

        'data/occupations': Occupation[];
        'data/occupation-nodes': OccupNode[];
        'data/typology': Dictionary;
        'data/statecode': Dictionary;
        'data/observation': Dictionary;
        'data/building-group': BuildingGroup[];
        'data/building-groups-suggestion': BlockBuildingGroup[];
        'data/blocks': RemoteResource<FeatureCollection>;

        'measure/interaction': InteractionMeasure;
        'measure/view': IMapViewData;
        'measure/baselayer': BaseLayerCode;

        'extra/layers/info': (SitexLayerInfo & { visible: boolean })[];
        'extra/layers/data': RemoteResource<Record<string, FeatureCollection>>;
        'extra/layers/tab': boolean;
    }
}

export const defaultAppShape = () => ({
    'app/layout': 'splash' as Layout,
    'app/fullscreen': false,
    'app/status': [],
    'app/online': 100,
    'app/legend': 'sync' as LegendType,
    'form/survey-type': 'building' as SurveyType,
    'form/id': null,
    'form/prop': null,
    'form/block/current': null,
    'form/building/current': null,
    'form/building/occupation': defaultOccupationWidgetState(),
    'form/building/files': [],
    'form/building/upload-file': remoteNone,
    'form/building/group': defaultBuildingGroupWidgetState(),
    'form/buildings': [],
    'form/parcel/current': null,
    'form/parcel/occupation': defaultOccupationWidgetState(),
    'form/parcel/files': [],
    'form/parcel/upload-file': remoteNone,
    'form/parcels': [],
    'form/geometry/interaction': defaultInteraction(),
    'form/geometry/view': defaultMapView(),
    'form/groups/interaction': defaultInteraction(),
    'form/groups/view': defaultMapView(),
    'form/groups/current': null,
    'form/groups/name': null,
    'history/building': remoteNone,
    'history/parcel': remoteNone,
    'data/buildings': [],
    'data/parcels': [],
    'data/urbis-buildings': [],
    'data/urbis-address': [],
    'data/urbis-parcels': [],
    'data/occupations': [],
    'data/occupation-nodes': [],
    'data/typology': [],
    'data/statecode': [],
    'data/observation': [],
    'data/building-group': [],
    'data/building-groups-suggestion': [],
    'data/blocks': remoteNone,
    'measure/interaction': initialMesureState(),
    'measure/view': defaultMapView(),
    'measure/baselayer': 'urbis_ortho' as BaseLayerCode,
    'extra/layers/info': [],
    'extra/layers/data': remoteNone,
    'extra/layers/tab': false,
});
