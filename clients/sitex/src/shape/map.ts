import {
    IMapViewData,
    Interaction,
    defaultInteraction,
    FeaturePath,
    nullFeaturePath,
} from 'sdi/map';
import { Inspire, MultiPolygon } from 'sdi/source';
import { Nullable } from 'sdi/util';
import { BaseLayerCode } from '../queries';

declare module 'sdi/shape' {
    export interface IShape {
        'port/map/view': IMapViewData;
        'port/map/interaction': Interaction;
        'port/map/selection': FeaturePath;
        'port/map/create': Nullable<MultiPolygon>;
        'port/map/baselayer': BaseLayerCode;

        'data/metadatas': Inspire[];
}
}

export const defaultMapView = (): IMapViewData => ({
    dirty: 'geo',
    srs: 'EPSG:31370',
    center: [148885, 170690],
    rotation: 0,
    zoom: 8.2,
    feature: null,
    extent: null,
});


export const defaultMapState = () => ({
    'port/map/view': defaultMapView(),
    'port/map/interaction': defaultInteraction(),
    'port/map/selection': nullFeaturePath(),
    'port/map/create': null,
    'port/map/baselayer': 'urbis_gray' as BaseLayerCode,
    'data/metadatas': [],
});
