import { InteractionMeasure, measureQueryFactory } from 'sdi/map';
import { queryK } from 'sdi/shape';
import { IMapInfo } from 'sdi/source';
import { getSurveyType } from './app';
import { wmsUrbisGray, wmsUrbisOrtho } from './map';
import urbisBuilding from './map/urbis-building';
import urbisParcel from './map/urbis-parcel';
import { SurveyType } from '../remote';

export const measureMapName = 'measure-map';

export const getMeasureInteraction = queryK('measure/interaction');

export const initialMesureState = (): InteractionMeasure => ({
    label: 'measure',
    state: {
        geometryType: 'Polygon',
        coordinates: [],
    },
});

export const { getMeasured } = measureQueryFactory(getMeasureInteraction);

export const getMeasureView = queryK('measure/view');

export const getMeasureBaseLayerCode = queryK('measure/baselayer');

export const getMeasureBaseLayers = () => {
    switch (getMeasureBaseLayerCode()) {
        case 'urbis_gray':
            return [wmsUrbisGray];
        case 'urbis_ortho':
            return [wmsUrbisOrtho];
    }
};

const isVisible = (surveyType: SurveyType) =>
    getSurveyType()
        .map(st => st === surveyType)
        .getOrElse(false);

export const getMeasureLayerInfos = () => [
    urbisBuilding.layer(isVisible('building')),
    urbisParcel.layer(isVisible('parcel')),
];

export const getMeasureMapInfo = (): IMapInfo => ({
    baseLayer: 'urbis.irisnet.be/urbis_ortho',
    id: measureMapName,
    id_origin: measureMapName,
    url: `/none`,
    lastModified: Date.now(),
    status: 'published',
    title: { fr: 'Sitex', nl: 'Sitex' },
    description: { fr: 'Sitex', nl: 'Sitex' },
    categories: [],
    attachments: [],
    layers: getMeasureLayerInfos().map(info => info.id),
});

export const getMeasureType = () => getMeasureInteraction().state.geometryType;

export const getMeasureSnapLayer = () =>
    getSurveyType()
        .map(st => {
            switch (st) {
                case 'building':
                    return urbisBuilding.name;
                case 'parcel':
                    return urbisParcel.name;
                default:
                    return '';
            }
        })
        .getOrElse('');
