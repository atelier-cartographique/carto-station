import { ILayerInfo, Inspire, PolygonInterval, StyleConfig } from 'sdi/source';
import { SurveyType } from 'sitex/src/remote';
import { getLegendType, getSurveyType } from '../app';
import colors from './colors';

const name = 'urbis-block';

const groupStyle = (
    low: number,
    high: number,
    color: string
): PolygonInterval => ({
    low,
    high,
    fillColor: 'transparent',
    strokeColor: colors.grey,
    strokeWidth: 1,
    pattern: { angle: 45, width: 0.5, color },
    label: {},
});

const styleSimple = (): StyleConfig => ({
    kind: 'polygon-simple',
    label: {
        size: 12,
        yOffset: 0,
        baseline: 'bottom',
        propName: { fr: 'id', nl: 'id' },
        align: 'center',
        resLimit: 36 / 6,
        color: colors.darkTerracota,
    },
    fillColor: colors.transparent,
    strokeColor: colors.darkTerracota,
    strokeWidth: 1,
});

const styleProc = (st: SurveyType): StyleConfig => ({
    kind: 'polygon-continuous',
    label: {
        size: 12,
        yOffset: 0,
        baseline: 'bottom',
        propName: { fr: 'id', nl: 'id' },
        align: 'center',
        resLimit: 36 / 6,
        color: colors.darkTerracota,
    },
    propName: `${st}_occupancy_percentage`,
    intervals: [
        groupStyle(0, 0.1, colors.transparent),
        groupStyle(0.1, 30, colors.yellow),
        groupStyle(20, 70, colors.green),
        groupStyle(99, 1000000000, colors.blue),
    ],
});

const style = (st: SurveyType) => {
    if (getLegendType() === 'blocks') {
        return styleProc(st);
    }
    return styleSimple();
};

const metadata: Inspire = {
    id: name,
    geometryType: 'MultiPolygon',
    resourceTitle: { fr: name, nl: name },
    resourceAbstract: { fr: name, nl: name },
    uniqueResourceIdentifier: name,
    topicCategory: [],
    keywords: [],
    geographicBoundingBox: { west: 0.0, north: 0.0, east: 0.0, south: 0.0 },
    temporalReference: { creation: 'NOW', revision: 'NOW' },
    responsibleOrganisation: [1],
    metadataPointOfContact: [1],
    metadataDate: 'NOW',
    published: false,
    dataStreamUrl: null,
    maintenanceFrequency: 'unknown',
};

const layer = (): ILayerInfo => ({
    id: name,
    legend: null,
    group: null,
    metadataId: name,
    featureViewOptions: { type: 'default' },
    visible: true,
    visibleLegend: true,
    opacitySelector: false,
    minZoom: 8,
    style: style(getSurveyType().getOrElse('building')),
});

export default {
    name,
    style,
    metadata,
    layer,
};
