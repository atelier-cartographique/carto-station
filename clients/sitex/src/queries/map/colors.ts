export default {
    transparent: 'rgba(255,255,255,0.1)',
    white: 'white',
    grey: 'grey',
    blue: '#13BBEE',
    red: '#d53e2a',
    yellow: '#fdc300',
    green: '#457437',
    darkTerracota: '#562821',
};
