import { colord } from 'colord';
import { queryK } from 'sdi/shape';
import {
    ILayerInfo,
    IMapInfo,
    Inspire,
    PolygonDiscreteGroup,
    StyleConfig,
} from 'sdi/source';

const name = 'GroupsMap';

const groupStyle = (
    values: string[],
    fillColor: string,
    strokeColor = '#282828'
): PolygonDiscreteGroup => ({
    values,
    fillColor,
    strokeColor,
    strokeWidth: 0.5,
    pattern: {
        angle: 45,
        width: 1,
        color: strokeColor,
    },
    label: {},
});

const ALPHA = 0.4;

const style: StyleConfig = {
    kind: 'polygon-discrete',

    propName: 'prop',
    groups: [
        groupStyle(
            ['selected-feature'],
            colord('#0068d5').alpha(ALPHA).toHex(),
            '#38853b'
        ),
        groupStyle(['selected-group'], colord('#468bc7').alpha(ALPHA).toHex()),
        groupStyle(['some'], colord('#b31c05').alpha(ALPHA).toHex()),
        groupStyle(['none'], colord('#877775').alpha(ALPHA).toHex()),
    ],
};

const metadata: Inspire = {
    id: name,
    geometryType: 'MultiPolygon',
    resourceTitle: { fr: name, nl: name },
    resourceAbstract: { fr: name, nl: name },
    uniqueResourceIdentifier: name,
    topicCategory: [],
    keywords: [],
    geographicBoundingBox: { west: 0.0, north: 0.0, east: 0.0, south: 0.0 },
    temporalReference: { creation: 'NOW', revision: 'NOW' },
    responsibleOrganisation: [1],
    metadataPointOfContact: [1],
    metadataDate: 'NOW',
    published: false,
    dataStreamUrl: null,
    maintenanceFrequency: 'unknown',
};

const layer = (): ILayerInfo => ({
    id: name,
    legend: null,
    group: null,
    metadataId: name,
    featureViewOptions: { type: 'default' },
    visible: true,
    visibleLegend: true,
    style,
    opacitySelector: false,
});

const getMapInfo = (): IMapInfo => ({
    baseLayer: 'urbis.irisnet.be/urbis_gray',
    id: name,
    id_origin: name,
    url: `/none`,
    lastModified: Date.now(),
    status: 'published',
    title: { fr: 'Groups', nl: 'Groups' },
    description: { fr: 'Editor', nl: 'Groups' },
    categories: [],
    attachments: [],
    layers: [name],
});

const getLayerInfos = (): ILayerInfo[] => [layer()]

const getView = queryK('form/groups/view');
const getInteraction = queryK('form/groups/interaction');

export default {
    name,
    getMapInfo,
    getLayerInfos,
    getView,
    getInteraction,
    layer,
    metadata,
};
