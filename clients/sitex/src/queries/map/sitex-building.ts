import { query } from 'sdi/shape';
import {
    ILayerInfo,
    Inspire,
    PolygonDiscreteGroup,
    StyleConfig,
} from 'sdi/source';
import { getSurveyType } from '..';
import { getLegendType } from '../app';
import colors from './colors';

const name = 'sitex-building';

const dynStrokeColor = () => {
    switch (query('port/map/baselayer')) {
        case 'urbis_gray':
            return colors.darkTerracota;
        case 'urbis_ortho':
            return colors.white;
    }
};

const group = (value: string, color: string): PolygonDiscreteGroup => ({
    label: {},
    values: [value],
    fillColor: 'transparent',
    strokeColor: dynStrokeColor(),
    strokeWidth: 0.8,
    pattern: {
        angle: 45,
        color,
        width: 1,
    },
});

const styleNone: StyleConfig = {
    kind: 'polygon-simple',
    fillColor: colors.transparent,
    strokeColor: colors.grey,
    strokeWidth: 1,
};

const styleSync = (): StyleConfig => ({
    kind: 'polygon-discrete',
    propName: 'appStatus',
    groups: [group('old', colors.blue), group('new', colors.red)],
});

const styleStatus = (): StyleConfig => ({
    kind: 'polygon-discrete',
    propName: 'datastate',
    groups: [
        group('D', colors.grey), // draft
        group('O', colors.yellow), // ongoing
        group('V', colors.blue), // validated
    ],
});

const style = () => {
    switch (getLegendType()) {
        case 'blocks':
            return styleNone;
        case 'status':
            return styleStatus();
        case 'sync':
            return styleSync();
    }
};

const metadata: Inspire = {
    id: name,
    geometryType: 'MultiPolygon',
    resourceTitle: { fr: name, nl: name },
    resourceAbstract: { fr: name, nl: name },
    uniqueResourceIdentifier: name,
    topicCategory: [],
    keywords: [],
    geographicBoundingBox: { west: 0.0, north: 0.0, east: 0.0, south: 0.0 },
    temporalReference: { creation: 'NOW', revision: 'NOW' },
    responsibleOrganisation: [1],
    metadataPointOfContact: [1],
    metadataDate: 'NOW',
    published: false,
    dataStreamUrl: null,
    maintenanceFrequency: 'unknown',
};

const layer = (): ILayerInfo => ({
    id: name,
    legend: null,
    group: null,
    metadataId: name,
    featureViewOptions: { type: 'default' },
    visible: getSurveyType()
        .map(st => st === 'building')
        .getOrElse(false),
    visibleLegend: true,
    opacitySelector: false,
    style: style(),
});

export default {
    name,
    style,
    metadata,
    layer,
};
