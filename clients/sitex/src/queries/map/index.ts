import * as debug from 'debug';
import { fromNullable } from 'fp-ts/lib/Option';
import { ILayerInfo, IMapBaseLayer, IMapInfo, Inspire } from 'sdi/source';
import { query, queryK } from 'sdi/shape';
import urbisBlock from './urbis-block';
import urbisBuilding from './urbis-building';
import sitexBuilding from './sitex-building';
import urbisParcel from './urbis-parcel';
import sitexParcel from './sitex-parcel';
import geometry from './geometry';
import { SitexLayerInfo } from 'sitex/src/remote';

export const logger = debug('sdi:queries/map');

export type BaseLayerCode = 'urbis_gray' | 'urbis_ortho';

export const getView = queryK('port/map/view');

export const getInteraction = queryK('port/map/interaction');

export const getBaseLayerCode = queryK('port/map/baselayer');

export const wmsUrbisGray: IMapBaseLayer = {
    id: 'urbis_gray',
    recommended: true,
    codename: 'urbis_gray',
    name: {
        fr: 'Urbis Gray',
        nl: 'Urbis Gray',
    },
    srs: 'EPSG:31370',
    url: 'https://geoservices-urbis.irisnet.be/geoserver/ows',
    params: {
        VERSION: '1.1.1',
        LAYERS: {
            fr: 'UrbisAdm:Ss,urbisFRGray',
            nl: 'UrbisAdm:Ss,urbisNLGray',
        },
    },
};

export const wmsUrbisOrtho: IMapBaseLayer = {
    id: 'urbis_ortho',
    recommended: true,
    codename: 'urbis_ortho',
    name: {
        fr: 'Urbis Ortho',
        nl: 'Urbis Ortho',
    },
    srs: 'EPSG:31370',
    url: 'https://geoservices-urbis.irisnet.be/geoserver/urbisgrid/ows',
    params: {
        VERSION: '1.1.1',
        LAYERS: {
            fr: 'Ortho',
            nl: 'Ortho',
        },
    },
};

export const getBaseLayers = () => {
    switch (getBaseLayerCode()) {
        case 'urbis_gray':
            return [wmsUrbisGray];
        case 'urbis_ortho':
            return [wmsUrbisOrtho];
    }
};

export const mainMapName = 'MainMap';

export const getLayerInfos = (): ILayerInfo[] =>
    getExtraLayers()
        .map(extraLayerInfo)
        .concat(
            urbisBlock.layer(),
            urbisBuilding.layer(),
            sitexBuilding.layer(),
            urbisParcel.layer(),
            sitexParcel.layer(),
            geometry.layer()
        );

export const getMapInfo = (): IMapInfo => ({
    baseLayer: 'urbis.irisnet.be/urbis_gray',
    id: mainMapName,
    id_origin: mainMapName,
    url: `/none`,
    lastModified: Date.now(),
    status: 'published',
    title: { fr: 'Sitex', nl: 'Sitex' },
    description: { fr: 'Sitex', nl: 'Sitex' },
    categories: [],
    attachments: [],
    layers: getLayerInfos().map(info => info.id),
});

export const getSelection = queryK('port/map/selection');

export const getSelected = () => ({
    layerId: null,
    featureId: null,
});

export const getSitexBuildings = queryK('data/buildings');

export const getUrbisBuildings = queryK('data/urbis-buildings');

export const getSitexParcels = queryK('data/parcels');

export const getUrbisParcels = queryK('data/urbis-parcels');

export const getCreateGeometry = () => fromNullable(query('port/map/create'));

export const getBlocks = queryK('data/blocks');

export const getExtraLayers = queryK('extra/layers/info');

export const makeExtraLayerId = (info: SitexLayerInfo) =>
    `${info.data.schema}/${info.data.table}`;

export const extraLayerInfo = (
    info: SitexLayerInfo & { visible: boolean }
): ILayerInfo => ({
    id: makeExtraLayerId(info),
    legend: null,
    group: null,
    metadataId: makeExtraLayerId(info),
    featureViewOptions: { type: 'default' },
    visible: info.visible,
    visibleLegend: true,
    opacitySelector: false,
    // minZoom: 8,
    style: info.style,
});
export const extraLayerMetadata = (info: SitexLayerInfo): Inspire => ({
    id: makeExtraLayerId(info),
    geometryType: info.geometryType,
    resourceTitle: info.name,
    resourceAbstract: info.name,
    uniqueResourceIdentifier: makeExtraLayerId(info),
    topicCategory: [],
    keywords: [],
    geographicBoundingBox: { west: 0.0, north: 0.0, east: 0.0, south: 0.0 },
    temporalReference: { creation: 'NOW', revision: 'NOW' },
    responsibleOrganisation: [1],
    metadataPointOfContact: [1],
    metadataDate: 'NOW',
    published: false,
    dataStreamUrl: null,
    maintenanceFrequency: 'unknown',
});

export const findExtraLayer = (id: string) =>
    fromNullable(getExtraLayers().find(info => makeExtraLayerId(info) === id));

export const findExtraMetadata = (id: string) =>
    findExtraLayer(id).map(extraLayerMetadata);

logger('loaded');
