import { ILayerInfo, Inspire, StyleConfig } from 'sdi/source';

const name = 'urbis-parcel';

const style: StyleConfig = {
    kind: 'polygon-simple',
    fillColor: 'rgba(173, 101, 12, 0.1)',
    strokeColor: 'rgba(255, 50, 50, 1)',
    strokeWidth: 2,
};

const metadata: Inspire = {
    id: name,
    geometryType: 'MultiPolygon',
    resourceTitle: { fr: name, nl: name },
    resourceAbstract: { fr: name, nl: name },
    uniqueResourceIdentifier: name,
    topicCategory: [],
    keywords: [],
    geographicBoundingBox: { west: 0.0, north: 0.0, east: 0.0, south: 0.0 },
    temporalReference: { creation: 'NOW', revision: 'NOW' },
    responsibleOrganisation: [1],
    metadataPointOfContact: [1],
    metadataDate: 'NOW',
    published: false,
    dataStreamUrl: null,
    maintenanceFrequency: 'unknown',
};

const layer = (visible = false): ILayerInfo => ({
    id: name,
    legend: null,
    group: null,
    metadataId: name,
    featureViewOptions: { type: 'default' },
    visible,
    visibleLegend: true,
    opacitySelector: false,
    style,
});

export default {
    name,
    style,
    metadata,
    layer,
};
