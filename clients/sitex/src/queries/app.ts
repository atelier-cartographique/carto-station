import { fromNullable, fromPredicate } from 'fp-ts/lib/Option';
import { query, queryK } from 'sdi/shape';
import { remoteToOption } from 'sdi/source';
import {
    buildingFormDataIsTouched,
    buildingFormDataIsUntouched,
    parcelFormDataIsTouched,
    parcelFormDataIsUntouched,
} from './field';

export const getLayout = () => query('app/layout');

export const getSurveyType = () => fromNullable(query('form/survey-type'));

export const getSurveyProp = () => fromNullable(query('form/prop'));

export const getFormId = () => fromNullable(query('form/id'));

export const findUrbisBuilding = (urbiId: number) =>
    fromNullable(query('data/urbis-buildings').find(({ id }) => id === urbiId));

export const findUrbisParcel = (urbiId: number) =>
    fromNullable(query('data/urbis-parcels').find(({ id }) => id === urbiId));

export const getBuildings = queryK('form/buildings');

export const getTouchedBuildings = () =>
    getBuildings().filter(buildingFormDataIsTouched);

export const getUntouchedBuildings = () =>
    getBuildings().filter(buildingFormDataIsUntouched);

export const getTouchedParcels = () =>
    query('form/parcels').filter(parcelFormDataIsTouched);

export const getUntouchedParcels = () =>
    query('form/parcels').filter(parcelFormDataIsUntouched);

export const getFullScreen = queryK('app/fullscreen');

export const getStatus = queryK('app/status');

type OnlineQuality = 'critical' | 'bad' | 'medium' | 'good';

export const computeOnlineQuality = (score: number): OnlineQuality => {
    if (score < 25) {
        return 'critical';
    } else if (score < 50) {
        return 'bad';
    } else if (score < 75) {
        return 'medium';
    }
    return 'good';
};

export const getOnlineQuality = () => computeOnlineQuality(query('app/online'));

export const withNetworkGood = () =>
    fromPredicate<OnlineQuality>(q => q === 'good')(getOnlineQuality());

export const withNetworkMedium = () =>
    fromPredicate<OnlineQuality>(q => q === 'good' || q === 'medium')(
        getOnlineQuality()
    );

export const getBuildingHistory = () =>
    remoteToOption(query('history/building'));

export const getParcelHistory = () => remoteToOption(query('history/parcel'));

export type LegendType = 'sync' | 'blocks' | 'status';

export const getLegendType = queryK('app/legend');

export const getExtraLayerTab = queryK('extra/layers/tab');
