import { fromNullable, some } from 'fp-ts/lib/Option';
import { getLang } from 'sdi/app';
import { query } from 'sdi/shape';
import type { CodePrefix } from '../components/field';
import {
    BuildingOccupation,
    Occupation,
    ValidOccupationCode,
    SurveyType,
} from '../remote';
import { longestSequence } from '../port/lcs';
import { fromRecord } from 'sdi/locale';
import { tryNumber } from 'sdi/util';
import { iife } from 'sdi/lib';
import { ParcelFormData } from './field';
import { index } from 'fp-ts/lib/Array';
import { codeToPrefix } from '../events';

const OCUUPATION_SPLIT = 50;

const getOccupations = () => [...query('data/occupations')];

const filterBuildingOccupation = (o: Occupation) =>
    tryNumber(o.code.slice(0, 2))
        .map(n => n < OCUUPATION_SPLIT)
        .getOrElse(false);

const filterParcelOccupation = (o: Occupation) =>
    tryNumber(o.code.slice(0, 2))
        .map(n => n >= OCUUPATION_SPLIT)
        .getOrElse(false);

const occupations = (st: SurveyType) => {
    const filter = iife(() => {
        switch (st) {
            case 'building':
                return filterBuildingOccupation;
            case 'parcel':
                return filterParcelOccupation;
            case 'public-space':
                return () => false;
        }
    });
    return getOccupations().filter(filter);
};

const filterOccupationsByString = (st: SurveyType, prefix: string) =>
    occupations(st).filter(o => o.code.startsWith(prefix));

export const filterOccupations = (st: SurveyType, prefix: CodePrefix) => {
    switch (prefix.length) {
        case 0:
            return occupations(st).filter(
                ({ code }) =>
                    code.endsWith('.00.00.00') || code.endsWith('.00.00.99')
            );
        case 1:
            return filterOccupationsByString(st, prefix.join('.')).filter(
                ({ code }) => code.endsWith('.00.00') || code.endsWith('.00.99')
            );
        case 2:
            return filterOccupationsByString(st, prefix.join('.')).filter(
                ({ code }) => code.endsWith('.00') || code.endsWith('.99')
            );
        case 3:
            return filterOccupationsByString(st, prefix.join('.'));
        case 4:
            return [];
    }
};

const getOccupationAt = (i: number) => index(i, getOccupations());

const getOccupationNode = (code: string) =>
    fromNullable(query('data/occupation-nodes').find(n => n.code === code));

export const getOccupationParent = (occupation: Occupation) =>
    getOccupationNode(occupation.code)
        .chain(({ parent }) => fromNullable(parent))
        .chain(getOccupationAt);

export const findOccupationRoot = (occupation: Occupation) => {
    const prefix = codeToPrefix(occupation.code);
    if (prefix.length <= 1) {
        return some(occupation);
    }
    return findOccupationByCode(`${prefix[0]}.00.00.00`);
};

export const proceedBuildingOccupationToUpdate = (
    occups: BuildingOccupation[] | null
) => fromNullable(occups).getOrElse([]);

export const findOccupationByCode = (code: ValidOccupationCode) =>
    fromNullable(query('data/occupations').find(o => o.code === code));

const computeScore = ([lcs, diff]: ReturnType<typeof longestSequence>) => {
    const base = lcs.length;
    const continuous = diff.reduce(
        ({ max, count }, [op]) => {
            if (op === 'id') {
                return {
                    max: Math.max(count + 1, max),
                    count: count + 1,
                };
            }
            return { max, count: 0 };
        },
        {
            max: 0,
            count: 0,
        }
    ).max;
    return base * continuous;
};

export const lookupOccupation = (st: SurveyType, token: string) => {
    const tokList = token.split('');
    if (tokList.length === 0) {
        return [];
    }
    const lang = getLang();
    const collator = new Intl.Collator(lang, {
        usage: 'search',
        sensitivity: 'base',
    });
    const results = occupations(st)
        .map<[number, Occupation]>(occup => {
            const keywordScore =
                occup.keywords.length === 0
                    ? 0
                    : occup.keywords.reduce(
                          (acc, keyword) =>
                              Math.max(
                                  acc,
                                  computeScore(
                                      longestSequence(
                                          tokList,
                                          fromRecord(keyword).split(''),
                                          collator
                                      )
                                  )
                              ),
                          0
                      );
            const nameScore = computeScore(
                longestSequence(
                    tokList,
                    fromRecord(occup.name).split(''),
                    collator
                )
            );
            return [Math.max(keywordScore, nameScore * 2), occup];
        })
        .filter(([score]) => score > 0)
        .sort((a, b) => b[0] - a[0]);

    return results
        .slice(0, 7)
        .map(([, occup]) => occup)
        .sort((a, b) => fromRecord(a.name).length - fromRecord(b.name).length);
};

export const compressBuilding = (occups: BuildingOccupation[]) =>
    occups.reduce((acc, occup, i) => {
        if (i === 0) {
            return [[occup]];
        }
        const previousRow = acc[acc.length - 1];
        const previousOccup = previousRow[previousRow.length - 1];
        if (previousOccup.occupcode === occup.occupcode) {
            previousRow.push(occup);
        } else {
            acc.push([occup]);
        }
        return acc;
    }, [] as BuildingOccupation[][]);

export const getParcelOccupationArea = (form: ParcelFormData) =>
    fromNullable(form.occupancys.value)
        .map(occups =>
            occups.map(occup => occup.area).reduce((acc, n) => acc + n, 0)
        )
        .getOrElse(0);
