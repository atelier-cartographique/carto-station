import { fromNullable, none, some } from 'fp-ts/lib/Option';
import { right } from 'fp-ts/lib/Either';

import {
    addLayer,
    checkMap,
    FetchData,
    removeLayer,
    viewEventsFactory,
    nullFeaturePath,
    defaultInteraction,
    removeLayerAll,
    withInteraction,
    InteractionMark,
} from 'sdi/map';
import * as debug from 'debug';
import {
    assign,
    assignK,
    dispatch,
    dispatchK,
    observe,
    query,
} from 'sdi/shape';
import {
    FeatureCollection,
    remoteError,
    remoteLoading,
    remoteSuccess,
    getMessageRecord,
    Feature,
    DirectGeometryObject,
    MultiPolygon,
    remoteToOption,
    remoteNone,
} from 'sdi/source';
import { condOnce, noop, tryNumber, tryString } from 'sdi/util';
import {
    findBuildingForm,
    getTouchedBuildings,
    getUntouchedBuildings,
    mainMapName,
    getSurveyType,
    getUntouchedParcels,
    getTouchedParcels,
    findParcelForm,
    getCurrentBuildingForm,
    getCreateGeometry,
    BuildingFormData,
    getGroupSuggestionByBuilding,
    getCurrentParcelForm,
    BaseLayerCode,
    makeExtraLayerId,
    getExtraLayers,
    extraLayerInfo,
} from '../queries';
import { updateNumber } from 'sitex/src/components/field';
import {
    fetchAllBlock,
    fetchExtraLayerData,
    fetchExtraLayerInfoList,
    SitexLayerInfo,
    SurveyType,
} from '../remote';
import {
    loadUrbisAndSitexBuildingInBlock,
    loadUrbisAddressInBlock,
    loadUrbisAndSitexParcelInBlock,
    createBuildingFormFromGeometry,
    getParcelBuiltArea,
} from './app';
import { navigatePreview } from './route';
import urbisBlock from '../queries/map/urbis-block';
import urbisBuilding from '../queries/map/urbis-building';
import urbisParcel from '../queries/map/urbis-parcel';
import geometryMap from '../queries/map/geometry';
import sitexBuilding from '../queries/map/sitex-building';
import sitexParcel from '../queries/map/sitex-parcel';
import { createParcelFormFromGeometry } from '.';
import { iife } from 'sdi/lib';

export const logger = debug('sdi:events/map');

export const setView = dispatchK('port/map/view');

export const setInteraction = dispatchK('port/map/interaction');

export const updateView = viewEventsFactory(setView).updateMapView;

export const setEditorView = dispatchK('form/geometry/view');

export const updateEditorView = viewEventsFactory(setEditorView).updateMapView;

export const setBaseLayerCode = (code: BaseLayerCode) => {
    assign('port/map/baselayer', code);
    setView(view => ({
        ...view,
        dirty: 'style',
    }));
};

export const loadRemoteBlocks = () => {
    const resource = query('data/blocks');
    if (resource.tag !== 'none') {
        return resource.tag;
    }
    const dispatch = assignK('data/blocks');
    dispatch(remoteLoading);
    fetchAllBlock()
        .then(fc => {
            dispatch(remoteSuccess(fc));
        })
        .catch(err => dispatch(remoteError(err)));
    return 'loading';
};

export const reloadBlocks = () => {
    assign('data/blocks', remoteNone);
    loadRemoteBlocks();
};

const mountBlockLayer = () => {
    removeLayer(mainMapName, urbisBlock.name);

    const fetcher: FetchData = () =>
        right(remoteToOption(query('data/blocks')));

    addLayer(
        mainMapName,
        () =>
            some({
                name: getMessageRecord(urbisBlock.metadata.resourceTitle),
                info: urbisBlock.layer(),
                metadata: urbisBlock.metadata,
            }),
        fetcher
    );
};

const baseMountLayer = () =>
    checkMap(mainMapName)
        .map(() => {
            mountBlockLayer();

            const emptyFetcher: FetchData = () =>
                right(some({ type: 'FeatureCollection', features: [] }));
            addLayer(
                mainMapName,
                () =>
                    some({
                        name: getMessageRecord(
                            urbisBuilding.metadata.resourceTitle
                        ),
                        info: urbisBuilding.layer(),
                        metadata: urbisBuilding.metadata,
                    }),
                emptyFetcher
            );
            addLayer(
                mainMapName,
                () =>
                    some({
                        name: getMessageRecord(
                            urbisParcel.metadata.resourceTitle
                        ),
                        info: urbisParcel.layer(),
                        metadata: urbisParcel.metadata,
                    }),
                emptyFetcher
            );
            addLayer(
                mainMapName,
                () =>
                    some({
                        name: getMessageRecord(
                            sitexBuilding.metadata.resourceTitle
                        ),
                        info: sitexBuilding.layer(),
                        metadata: sitexBuilding.metadata,
                    }),
                emptyFetcher
            );
            addLayer(
                mainMapName,
                () =>
                    some({
                        name: getMessageRecord(
                            sitexParcel.metadata.resourceTitle
                        ),
                        info: sitexParcel.layer(),
                        metadata: sitexParcel.metadata,
                    }),
                emptyFetcher
            );
            addLayer(
                mainMapName,
                () =>
                    some({
                        name: getMessageRecord(
                            geometryMap.metadata.resourceTitle
                        ),
                        info: geometryMap.layer(),
                        metadata: geometryMap.metadata,
                    }),
                emptyFetcher
            );
            return true;
        })
        .getOrElse(false);

export const updateSitexBuildingLayer = () =>
    checkMap(mainMapName).map(() => {
        removeLayer(mainMapName, sitexBuilding.name);

        const defaultGeometry: DirectGeometryObject = {
            type: 'Point',
            coordinates: [0, 0],
        };
        const syncedFeatures: Feature[] = getUntouchedBuildings().map(
            ({ idbuild, geom, datastate }) => ({
                type: 'Feature',
                id: idbuild,
                geometry: geom.value ?? defaultGeometry,
                properties: {
                    appStatus: 'old',
                    datastate: datastate.value,
                },
            })
        );
        const editedFeatures: Feature[] = getTouchedBuildings().map(
            ({ idbuild, geom, datastate }) => ({
                type: 'Feature',
                id: idbuild,
                geometry: geom.value ?? defaultGeometry,
                properties: {
                    appStatus: 'new',
                    datastate: datastate.value,
                },
            })
        );
        addLayer(
            mainMapName,
            () =>
                some({
                    name: getMessageRecord(
                        sitexBuilding.metadata.resourceTitle
                    ),
                    info: sitexBuilding.layer(),
                    metadata: sitexBuilding.metadata,
                }),
            () =>
                right(
                    some({
                        type: 'FeatureCollection',
                        features: syncedFeatures.concat(editedFeatures),
                    })
                )
        );
    });

export const updateSitexParcelLayer = () =>
    checkMap(mainMapName).map(() => {
        removeLayer(mainMapName, sitexParcel.name);

        const defaultGeometry: DirectGeometryObject = {
            type: 'Point',
            coordinates: [0, 0],
        };
        const syncedFeatures: Feature[] = getUntouchedParcels().map(
            ({ idparcel, geom, datastate }) => ({
                type: 'Feature',
                id: idparcel,
                geometry: geom.value ?? defaultGeometry,
                properties: {
                    appStatus: 'old',
                    datastate: datastate.value,
                },
            })
        );
        const editedFeatures: Feature[] = getTouchedParcels().map(
            ({ idparcel, geom, datastate }) => ({
                type: 'Feature',
                id: idparcel,
                geometry: geom.value ?? defaultGeometry,
                properties: {
                    appStatus: 'new',
                    datastate: datastate.value,
                },
            })
        );

        addLayer(
            mainMapName,
            () =>
                some({
                    name: getMessageRecord(sitexParcel.metadata.resourceTitle),
                    info: sitexParcel.layer(),
                    metadata: sitexParcel.metadata,
                }),
            () =>
                right(
                    some({
                        type: 'FeatureCollection',
                        features: syncedFeatures.concat(editedFeatures),
                    })
                )
        );
    });

export const mountLayers = condOnce(baseMountLayer);

export const setCurrentFormBuilding = (form: BuildingFormData) => {
    if (form.u2building.value && !form.u2block.value) {
        const blockInfo = getGroupSuggestionByBuilding(form.u2building.value);
        if (blockInfo) {
            form.u2block = updateNumber(blockInfo.blockId);
        }
    }
    assign('form/building/current', form);
    assign('form/block/current', form.u2block.value);
};

export const setSelection = (layerId: string, featureId: string | number) =>
    assign('port/map/selection', { layerId, featureId });

export const clearSelection = () =>
    assign('port/map/selection', nullFeaturePath());

export const selectFeature = (layerId: string, featureId: string | number) => {
    if (layerId === urbisBlock.name) {
        tryNumber(featureId).map(fid => {
            assign('form/block/current', fid);
            loadUrbisAndSitexBuildingInBlock(fid);
            loadUrbisAndSitexParcelInBlock(fid);
            loadUrbisAddressInBlock(fid);
        });
    } else if (layerId === sitexBuilding.name) {
        tryString(featureId)
            .chain(findBuildingForm)
            .map(form => {
                setCurrentFormBuilding({ ...form });
                navigatePreview('building', form.idbuild);
                setSelection(layerId, featureId);
            });
    } else if (layerId === sitexParcel.name) {
        tryString(featureId)
            .chain(findParcelForm)
            .map(form => {
                form.builtArea = updateNumber(
                    Math.round(getParcelBuiltArea(form))
                );
                assign('form/parcel/current', { ...form });
                navigatePreview('parcel', form.idparcel);
                setSelection(layerId, featureId);
            });
    }
};

export const mountEditorLayers = (): Promise<unknown> => {
    const inner = (resolve: (value: unknown) => void): unknown =>
        checkMap(geometryMap.name)
            .map(() => {
                removeLayerAll(geometryMap.name);
                getSurveyType()
                    .chain(st =>
                        st === 'public-space'
                            ? none
                            : st === 'building'
                            ? some(urbisBuilding)
                            : some(urbisParcel)
                    )
                    .map(ns => {
                        const features = iife(() => {
                            if (ns.name === urbisBuilding.name) {
                                return query(
                                    'data/urbis-buildings'
                                ) as Feature[];
                            }
                            return query('data/urbis-parcels') as Feature[];
                        });
                        const urbisFetcher: FetchData = () =>
                            right(
                                some({ type: 'FeatureCollection', features })
                            );
                        addLayer(
                            geometryMap.name,
                            () =>
                                some({
                                    name: getMessageRecord(
                                        ns.metadata.resourceTitle
                                    ),
                                    info: ns.layer(),
                                    metadata: ns.metadata,
                                }),
                            urbisFetcher
                        );
                    });

                geometryMap.incrFeatureId();
                const emptyFetcher: FetchData = () =>
                    right(some({ type: 'FeatureCollection', features: [] }));
                const fetcher: FetchData = getSurveyType()
                    .map(surveyType => {
                        switch (surveyType) {
                            case 'building': {
                                const featureCollectionOpt =
                                    getCurrentBuildingForm()
                                        .chain(form =>
                                            fromNullable(form.geom.value)
                                        )
                                        .map<FeatureCollection>(geometry => ({
                                            type: 'FeatureCollection',
                                            features: [
                                                {
                                                    type: 'Feature',
                                                    id: geometryMap.getFeatureId(),
                                                    geometry,
                                                    properties: {},
                                                },
                                            ],
                                        }));
                                return (() =>
                                    right(
                                        featureCollectionOpt
                                    )) as unknown as FetchData;
                            }
                            case 'parcel': {
                                const featureCollectionOpt =
                                    getCurrentParcelForm()
                                        .chain(form =>
                                            fromNullable(form.geom.value)
                                        )
                                        .map<FeatureCollection>(geometry => ({
                                            type: 'FeatureCollection',
                                            features: [
                                                {
                                                    type: 'Feature',
                                                    id: geometryMap.getFeatureId(),
                                                    geometry,
                                                    properties: {},
                                                },
                                            ],
                                        }));
                                return (() =>
                                    right(
                                        featureCollectionOpt
                                    )) as unknown as FetchData;
                            }
                            case 'public-space':
                                return emptyFetcher;
                        }
                    })
                    .getOrElse(emptyFetcher);
                addLayer(
                    geometryMap.name,
                    () =>
                        some({
                            name: getMessageRecord(
                                geometryMap.metadata.resourceTitle
                            ),
                            info: geometryMap.layer(),
                            metadata: geometryMap.metadata,
                        }),
                    fetcher
                );

                resolve(0);
            })
            .getOrElseL(() => setTimeout(() => inner(resolve), 60));
    return new Promise(inner);
};

export const startEditingGeometry = () =>
    assign('form/geometry/interaction', {
        label: 'modify',
        state: {
            geometryType: 'MultiPolygon',
            selected: geometryMap.getFeatureId(),
            centerOnSelected: true,
        },
    });

export const stopEditingGeometry = () =>
    assign('form/geometry/interaction', defaultInteraction());

export const startCreatingGeometry = () =>
    assign('port/map/interaction', {
        label: 'create',
        state: {
            geometryType: 'MultiPolygon',
        },
    });

export const resetMapInteraction = () =>
    assign('port/map/interaction', defaultInteraction());

export const setCreateGeometry = (geometry: MultiPolygon) => {
    assign('port/map/create', geometry);
    const createFeatureId = 1;
    removeLayer(mainMapName, geometryMap.name);
    const fetcher: FetchData = () =>
        right(
            some({
                type: 'FeatureCollection',
                features: [
                    {
                        type: 'Feature',
                        id: createFeatureId,
                        geometry,
                        properties: {},
                    },
                ],
            })
        );
    const info = () =>
        some({
            name: getMessageRecord(geometryMap.metadata.resourceTitle),
            info: geometryMap.layer(),
            metadata: geometryMap.metadata,
        });
    addLayer(mainMapName, info, fetcher);
    assign('port/map/interaction', {
        label: 'modify',
        state: {
            geometryType: 'MultiPolygon',
            selected: createFeatureId,
            centerOnSelected: true,
        },
    });
};

export const updateCreateGeometry = (geom: MultiPolygon) => {
    assign('port/map/create', geom);
};

export const clearCreateGeometry = () => assign('port/map/create', null);

export const createFormFromGeometry = (surveyType: SurveyType) =>
    getCreateGeometry().chain(geom => {
        clearGeometryLayer();
        switch (surveyType) {
            case 'building':
                return some(createBuildingFormFromGeometry(geom));
            case 'parcel':
                return some(createParcelFormFromGeometry(geom));
            case 'public-space':
                return none;
        }
    });

export const clearGeometryLayer = () => {
    removeLayer(mainMapName, geometryMap.name);
};

export const startMark = () =>
    setInteraction(s => {
        if (s.label === 'mark') {
            return { ...s, state: { ...s.state, started: true } };
        }
        return s;
    });

export const endMark = () => setInteraction(() => defaultInteraction());

observe(
    'port/map/interaction',
    withInteraction<InteractionMark>(
        'mark',
        ({ state }) => {
            const now = Date.now();
            const endTime = state.endTime;
            if (!state.started && endTime > now) {
                // we've got a `putMark` action
                window.setTimeout(
                    () =>
                        dispatch('port/map/interaction', interaction => {
                            if (
                                interaction.label === 'mark' &&
                                interaction.state.endTime === endTime
                            ) {
                                return defaultInteraction();
                            }
                            return interaction;
                        }),
                    endTime - now
                );
            }
        },
        noop
    )
);

export const loadExtraLayers = () =>
    fetchExtraLayerInfoList().then(listInfo => {
        assign(
            'extra/layers/info',
            listInfo.map(info => ({ ...info, visible: true }))
        );
        listInfo
            .reduce(
                (acc, info) =>
                    acc.then(() =>
                        fetchExtraLayerData(info).then(data =>
                            dispatch('extra/layers/data', state => {
                                switch (state.tag) {
                                    case 'error':
                                        break;
                                    case 'none':
                                    case 'loading':
                                        return remoteSuccess({
                                            [makeExtraLayerId(info)]: data,
                                        });
                                    case 'success':
                                        return remoteSuccess({
                                            ...state.data,
                                            [makeExtraLayerId(info)]: data,
                                        });
                                }

                                return state;
                            })
                        )
                    ),
                Promise.resolve()
            )
            .then(mountExtraLayers);
    });

export const mountExtraLayers = () =>
    checkMap(mainMapName).map(() =>
        remoteToOption(query('extra/layers/data')).foldL(
            () => {
                setTimeout(mountExtraLayers, 500);
            },
            data => {
                getExtraLayers().map(extra => {
                    const extraId = makeExtraLayerId(extra);
                    removeLayer(mainMapName, extraId);

                    const fetcher: FetchData = () =>
                        right(fromNullable(data[extraId]));
                    addLayer(
                        mainMapName,
                        () =>
                            some({
                                name: extra.name,
                                info: extraLayerInfo(extra),
                                metadata: urbisBuilding.metadata,
                            }),
                        fetcher
                    );
                });
            }
        )
    );

export const toggleExtraLayerVisibility = (
    info: SitexLayerInfo,
    visible: boolean
) =>
    dispatch('extra/layers/info', state =>
        state.map(stateInfo =>
            makeExtraLayerId(stateInfo) === makeExtraLayerId(info)
                ? { ...stateInfo, visible }
                : stateInfo
        )
    );

logger('loaded');
