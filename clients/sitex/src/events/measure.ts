import bbox from '@turf/bbox';
import { fromEither, none, some } from 'fp-ts/lib/Option';
import {
    addLayer,
    checkMap,
    measureEventsFactory,
    viewEventsFactory,
} from 'sdi/map';
import { assign, dispatch, dispatchK, query } from 'sdi/shape';
import { tryMultiPolygon } from 'sdi/util';
import { register } from '../components/modal';
import { renderMeasure } from '../components/modal/measure';
import {
    BaseLayerCode,
    getBuildingFormFieldValue,
    getMeasureInteraction,
    getMeasureType,
    getParcelFormFieldValue,
    getSurveyType,
    measureMapName,
} from '../queries';
import urbisBuilding from '../queries/map/urbis-building';
import urbisParcel from '../queries/map/urbis-parcel';
import { FeatureCollection, getMessageRecord } from 'sdi/source';
import { right } from 'fp-ts/lib/Either';

export const setMeasureInteraction = dispatchK('measure/interaction');

export const clearMeasure = () =>
    dispatch('measure/interaction', ({ label, state }) => ({
        label,
        state: {
            ...state,
            coordinates: [],
        },
    }));

export const {
    startMeasureLength,
    startMeasureArea,
    stopMeasure,
    updateMeasureCoordinates,
} = measureEventsFactory(setMeasureInteraction, getMeasureInteraction);

export const switchMeasureType = () => {
    const mt = getMeasureType();
    clearMeasure();
    if (mt === 'LineString') {
        startMeasureArea();
    } else {
        startMeasureLength();
    }
};

export const setMeasureView = dispatchK('measure/view');

export const updateMeasureView =
    viewEventsFactory(setMeasureView).updateMapView;

export const [_closeMeasure, openMeasureModal] = register(
    'measure',
    renderMeasure
);

export const openMeasure = () => {
    openMeasureModal();
    getSurveyType()
        .chain(st => {
            switch (st) {
                case 'building':
                    return fromEither(getBuildingFormFieldValue('geom'));
                case 'parcel':
                    return fromEither(getParcelFormFieldValue('geom'));
                case 'public-space':
                    return none;
            }
        })
        .map(field => {
            if (field.tag === 'geometry') {
                field.value.chain(tryMultiPolygon).map(geometry => {
                    const interval = setInterval(() => {
                        if (interval) {
                            checkMap(measureMapName).map(() => {
                                clearInterval(interval);
                                updateMeasureView({
                                    dirty: 'geo/extent',
                                    extent: bbox(geometry),
                                });
                                mountLayers();
                            });
                        }
                    }, 100);
                });
            }
        });
};

const mountLayers = () =>
    checkMap(measureMapName).map(() => {
        mountUrbisBuildingLayer();
        mountUrbisParcelLayer();
    });

const mountUrbisParcelLayer = () => {
    const features = query('data/urbis-parcels');
    addLayer(
        measureMapName,
        () =>
            some({
                name: getMessageRecord(urbisParcel.metadata.resourceTitle),
                info: urbisParcel.layer(),
                metadata: urbisParcel.metadata,
            }),
        () =>
            right(
                some({
                    type: 'FeatureCollection',
                    features,
                } as FeatureCollection)
            )
    );
};

const mountUrbisBuildingLayer = () => {
    const features = query('data/urbis-buildings');
    addLayer(
        measureMapName,
        () =>
            some({
                name: getMessageRecord(urbisBuilding.metadata.resourceTitle),
                info: urbisBuilding.layer(),
                metadata: urbisBuilding.metadata,
            }),
        () =>
            right(
                some({
                    type: 'FeatureCollection',
                    features,
                } as FeatureCollection)
            )
    );
};

export const setMeasureBaseLayerCode = (code: BaseLayerCode) =>
    assign('measure/baselayer', code);
