import { assign, dispatch } from 'sdi/shape';
import { error, info } from '../components/status';

let timer: number | null = null;

export const statusError = (content: string) =>
    dispatch('app/status', ss => ss.concat(error(content)));

export const statusInfo = (content: string) => {
    if (timer !== null) {
        clearTimeout(timer);
    }
    timer = window.setTimeout(
        () => dispatch('app/status', ss => ss.filter(s => s.tag !== 'info')),
        6000
    );
    dispatch('app/status', ss => ss.concat(info(content)));
};

export const clearStatus = () => assign('app/status', []);

export const removeStatus = (i: number) =>
    dispatch('app/status', ss => ss.slice(0, i).concat(ss.slice(i + 1)));
