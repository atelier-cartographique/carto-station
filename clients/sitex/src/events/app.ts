import * as debug from 'debug';
import { catOptions } from 'fp-ts/lib/Array';
import { fromEither, fromNullable, Option } from 'fp-ts/lib/Option';
import turfArea from '@turf/area';
import turfBooleanIntersects from '@turf/boolean-intersects';
import turfIntersection from '@turf/intersect';
import { iife, scopeOption } from 'sdi/lib';
import tr from 'sdi/locale';
import { addFeaturesToLayer, toLonLat } from 'sdi/map';
import online from 'sdi/online';
import { assign, assignK, dispatch, query } from 'sdi/shape';
import {
    DirectGeometryObjectIO,
    Feature,
    getFeatureProp,
    MultiPolygon,
    MultiPolygonIO,
    onValidationError,
    remoteError,
    remoteLoading,
    remoteSuccess,
    uuid,
} from 'sdi/source';
import {
    updateBuildingOccupation,
    updateGeometry,
    updateGeoState,
    updateNumber,
    updateParcelOccupation,
} from '../components/field';
import {
    buildingFormData,
    mainMapName,
    buildingFormDataToBuilding,
    BuildingFormData,
    getTouchedBuildings,
    parcelFormData,
    ParcelFormData,
    getTouchedParcels,
    parcelFormDataToParcel,
    setBuildingGroups,
    getLayout,
    getFormId,
    getOnlineQuality,
    computeOnlineQuality,
    LegendType,
    getCurrentBuildingForm,
    getCurrentParcelForm,
} from '../queries';
import sitexBuilding from '../queries/map/sitex-building';
import urbisBuilding from '../queries/map/urbis-building';
import urbisParcel from '../queries/map/urbis-parcel';
import sitexParcel from '../queries/map/sitex-parcel';
import {
    Building,
    BuildingIO,
    fetchBuildingInBlock,
    fetchParcelInBlock,
    fetchUrbisAddressInBlock,
    fetchOccupations,
    fetchTypology,
    fetchStatecode,
    fetchUrbisBuildingInBlock,
    newBuilding,
    postBuilding,
    fetchUrbisParcelInBlock,
    newParcel,
    Parcel,
    postParcel,
    ParcelIO,
    fetchBuildingHistory,
    fetchBuildingGroup,
    fetchBuildingGroupSuggestion,
    SurveyType,
    fetchParcelHistory,
    fetchObservationStatus,
    Occupation,
    BuildingOccupation,
    ParcelOccupation,
} from '../remote';
import {
    updateBlockBuildingGroupsState,
    setCurrentFormBuilding,
    navigateIndex,
    navigatePreview,
    codeToPrefix,
    setBuildingFormFieldValue,
} from 'sitex/src/events';
import { updateSitexBuildingLayer, updateSitexParcelLayer } from './map';
import type { Layout } from './route';
import { statusError, statusInfo } from './status';
import { setParcelFormFieldValue } from './field';

const logger = debug('sdi:events/app');

export const setLayout = (layout: Layout) => assign('app/layout', layout);

export const setSurveyType = assignK('form/survey-type');
export const resetSurveyType = () => setSurveyType(null);

export const setSurveyProp = assignK('form/prop');
export const resetSurveyProp = () => setSurveyProp(null);

export const setFormId = assignK('form/id');
export const resetFormId = () => setFormId(null);

export const resetForm = () => {
    resetSurveyType();
    resetSurveyProp();
    resetFormId();
    assign('form/building/current', null);
    assign('form/parcel/current', null);
    assign('form/block/current', null);
};

export interface OccupNode {
    code: string;
    parent: number | null;
}

const makeOccupNodes = (occupations: Occupation[]) =>
    occupations.map<OccupNode>(({ code }) => {
        const prefix = codeToPrefix(code);
        const parent = iife(() => {
            switch (prefix.length) {
                case 0:
                case 1:
                    return null;
                case 2: {
                    const pat = `${prefix[0]}.00.00.00`;
                    return (
                        occupations.findIndex(({ code }) => code === pat) ??
                        null
                    );
                }
                case 3: {
                    const pat = `${prefix[0]}.${prefix[1]}.00.00`;
                    return (
                        occupations.findIndex(({ code }) => code === pat) ??
                        null
                    );
                }
                case 4: {
                    const pat = `${prefix[0]}.${prefix[1]}.${prefix[2]}.00`;
                    return (
                        occupations.findIndex(({ code }) => code === pat) ??
                        null
                    );
                }
            }
        });
        return { code, parent };
    });

export const loadOccupations = () =>
    fetchOccupations().then(occups => {
        assign('data/occupations', occups);
        assign('data/occupation-nodes', makeOccupNodes(occups));
    });

export const loadTypology = () =>
    fetchTypology().then(terms => assign('data/typology', terms));

export const loadStatecode = () =>
    fetchStatecode().then(codes => assign('data/statecode', codes));

export const loadObservation = () =>
    fetchObservationStatus().then(codes => assign('data/observation', codes));

export const loadBuildingGroup = () =>
    fetchBuildingGroup().then(groups => setBuildingGroups(groups));

export const loadBuildingGroupInBlock = (blockId: number) =>
    fetchBuildingGroupSuggestion(blockId).then(groups =>
        updateBlockBuildingGroupsState(blockId, groups)
    );

const onceUrbisBuildings = iife(() => {
    const rec: number[] = [];

    return (blockId: number, f: () => void) => {
        if (rec.indexOf(blockId) < 0) {
            rec.push(blockId);
            f();
        }
    };
});

const onceUrbisParcels = iife(() => {
    const rec: number[] = [];

    return (blockId: number, f: () => void) => {
        if (rec.indexOf(blockId) < 0) {
            rec.push(blockId);
            f();
        }
    };
});

const featureToBuilding = (f: Feature): Option<Building> => {
    const geom = f.geometry;
    const idbuild = f.id;
    return fromNullable(f.properties)
        .map(props => ({
            ...props,
            idbuild,
            geom,
            recordstate: fromNullable(props.recordstate).getOrElse('A'),
        }))
        .chain(props =>
            fromEither(
                BuildingIO.decode(props).mapLeft(
                    onValidationError(BuildingIO, 'featureToBuilding')
                )
            )
        );
};

const featureToParcel = (f: Feature): Option<Parcel> => {
    const geom = f.geometry;
    const idparcel = f.id;
    return fromNullable(f.properties)
        .map(props => ({
            ...props,
            idparcel,
            geom,
            recordstate: fromNullable(props.recordstate).getOrElse('A'),
        }))
        .chain(props =>
            fromEither(
                ParcelIO.decode(props).mapLeft(
                    onValidationError(ParcelIO, 'featureToParcel')
                )
            )
        )
        .map(parcel => ({ ...parcel, builtArea: null }));
};

export const loadBuildingInBlock = (blockId: number) =>
    fetchBuildingInBlock(blockId)
        .then(({ features }) => {
            addFeaturesToLayer(
                mainMapName,
                sitexBuilding.layer(),
                features.map(feature => ({
                    ...feature,
                    properties: {
                        appStatus: 'old',
                        datastate: getFeatureProp(feature, 'datastate', null),
                    },
                }))
            );
            const buildings = catOptions(features.map(featureToBuilding));
            const forms = buildings.map(buildingFormData);
            const ids = buildings.map(b => b.idbuild);
            const buildingFilter = (b: Building | BuildingFormData) =>
                ids.indexOf(b.idbuild) < 0;
            dispatch('data/buildings', bs =>
                bs.filter(buildingFilter).concat(buildings)
            );
            dispatch('form/buildings', fs =>
                fs.filter(buildingFilter).concat(forms)
            );
        })
        .then(() => loadBuildingGroupInBlock(blockId))
        .catch(err => logger('error loading buildings', err));

export const loadUrbisAndSitexBuildingInBlock = (blockId: number) =>
    onceUrbisBuildings(blockId, () =>
        fetchUrbisBuildingInBlock(blockId)
            .then(buildings => {
                addFeaturesToLayer(
                    mainMapName,
                    urbisBuilding.layer(),
                    buildings.features
                );
                dispatch('data/urbis-buildings', features => {
                    const listBuilding = features.concat(buildings.features);
                    updateBlockBuildingGroupsState(blockId, null, listBuilding);
                    return listBuilding;
                });

                statusInfo(
                    `${tr.sitex('loaded')} ${
                        buildings.features.length
                    } ${tr.sitex('buildingsFromUrbis')} `
                );
            })
            .then(() => loadBuildingInBlock(blockId))
            .catch(err => logger('error loading buildings', err))
    );

export const loadParcelInBlock = (blockId: number) =>
    fetchParcelInBlock(blockId)
        .then(({ features }) => {
            addFeaturesToLayer(
                mainMapName,
                sitexParcel.layer(),
                features.map(feature => ({
                    ...feature,
                    properties: {
                        appStatus: 'old',
                        datastate: getFeatureProp(feature, 'datastate', null),
                    },
                }))
            );
            const parcels = catOptions(features.map(featureToParcel));
            const forms = parcels.map(parcelFormData);
            const ids = parcels.map(p => p.idparcel);
            const parcelFilter = (p: Parcel | ParcelFormData) =>
                ids.indexOf(p.idparcel) < 0;
            dispatch('data/parcels', ps =>
                ps.filter(parcelFilter).concat(parcels)
            );
            dispatch('form/parcels', ps =>
                ps.filter(parcelFilter).concat(forms)
            );
        })
        .catch(err => logger('error loading parcels', err));

export const loadUrbisAndSitexParcelInBlock = (blockId: number) =>
    onceUrbisParcels(blockId, () =>
        fetchUrbisParcelInBlock(blockId)
            .then(parcels => {
                addFeaturesToLayer(
                    mainMapName,
                    urbisParcel.layer(),
                    parcels.features
                );
                dispatch('data/urbis-parcels', features =>
                    features.concat(parcels.features)
                );

                statusInfo(
                    `${tr.sitex('loaded')} ${
                        parcels.features.length
                    } ${tr.sitex('parcelsFromUrbis')} `
                );
            })
            .then(() => loadParcelInBlock(blockId))
            .catch(err => logger('error loading parcels', err))
    );

export const loadUrbisAddressInBlock = (blockId: number) =>
    fetchUrbisAddressInBlock(blockId)
        .then(urbisAddress => {
            dispatch('data/urbis-address', address =>
                address.concat(urbisAddress)
            );
        })
        .catch(err => logger('error loading address', err));

const findUrbisBuildingsFromGeometry = (ref: MultiPolygon) => {
    const intersects = (geom: MultiPolygon) => turfBooleanIntersects(ref, geom);

    return query('data/urbis-buildings').filter(f =>
        MultiPolygonIO.decode(f.geometry).map(intersects).getOrElse(false)
    );
};

export const getBuiltArea = (mpoly: MultiPolygon) =>
    findUrbisBuildingsFromGeometry(mpoly).reduce(
        (acc, f) =>
            acc +
            fromEither(MultiPolygonIO.decode(f.geometry))
                .chain(upoly => fromNullable(turfIntersection(upoly, mpoly)))
                .chain(f =>
                    fromEither(
                        DirectGeometryObjectIO.decode(f.geometry).map(geom =>
                            turfArea(toLonLat(geom))
                        )
                    )
                )
                .getOrElse(0),

        0
    );

export const getParcelBuiltArea = (form: ParcelFormData) =>
    MultiPolygonIO.decode(form.geom.value).map(getBuiltArea).getOrElse(0);

// export cons

export const createBuildingFormFromGeometry = (mpoly: MultiPolygon) => {
    const form = buildingFormData(newBuilding());
    form.geom = updateGeometry(mpoly);
    form.geostate = updateGeoState('D');
    form.groundarea = updateNumber(Math.round(turfArea(toLonLat(mpoly))));
    setCurrentFormBuilding(form);
    dispatch('form/buildings', bs => bs.concat({ ...form }));
    updateSitexBuildingLayer();
    return form.idbuild;
};

export const createParcelFormFromGeometry = (mpoly: MultiPolygon) => {
    const form = parcelFormData(newParcel());
    form.geom = updateGeometry(mpoly);
    form.geostate = updateGeoState('D');
    form.groundarea = updateNumber(Math.round(turfArea(toLonLat(mpoly))));
    assign('form/parcel/current', form);
    dispatch('form/parcels', bs => bs.concat({ ...form }));
    updateSitexParcelLayer();
    return form.idparcel;
};

export const enterFullScreen = () =>
    document.body
        .requestFullscreen()
        .then(() => assign('app/fullscreen', true))
        .catch(err => logger('failed to get fullscreen', err));

export const exitFullScreen = () => {
    assign('app/fullscreen', false);
    document.exitFullscreen();
};

const saveBuilding = (building: Building) =>
    postBuilding(building)
        .then(feature =>
            featureToBuilding(feature)
                .map(data => {
                    dispatch('form/buildings', bs =>
                        bs
                            .filter(({ idbuild }) => idbuild !== data.idbuild)
                            .concat(buildingFormData(data))
                    );
                    dispatch('data/buildings', bs =>
                        bs
                            .filter(({ idbuild }) => idbuild !== data.idbuild)
                            .concat(data)
                    );
                    updateSitexBuildingLayer();
                })
                .getOrElseL(() => statusError('~failed to parse building'))
        )
        .catch(err => {
            statusError(tr.sitex('status/error/save-building'));
            logger('error saving building', err);
        });

export const syncBuildings = () => {
    const buildings = getTouchedBuildings().map(buildingFormDataToBuilding);
    const next = () => fromNullable(buildings.pop()).map(saveBuilding);
    const process = (): void => {
        next().map(p => p.then(process));
    };
    process();
};

const saveParcel = (parcel: Parcel) =>
    postParcel(parcel)
        .then(feature =>
            featureToParcel(feature)
                .map(data => {
                    dispatch('form/parcels', bs =>
                        bs
                            .filter(
                                ({ idparcel }) => idparcel !== data.idparcel
                            )
                            .concat(parcelFormData(data))
                    );
                    dispatch('data/parcels', bs =>
                        bs
                            .filter(
                                ({ idparcel }) => idparcel !== data.idparcel
                            )
                            .concat(data)
                    );
                    updateSitexParcelLayer();
                })
                .getOrElseL(() => statusError('~failed to parse parcel'))
        )
        .catch(err => {
            statusError(tr.sitex('status/error/save-parcel'));
            logger('error saving parcel', err);
        });

export const syncParcels = () => {
    const parcels = getTouchedParcels().map(parcelFormDataToParcel);
    const next = () => fromNullable(parcels.pop()).map(saveParcel);
    const process = (): void => {
        next().map(p => p.then(process));
    };
    process();
};

export const syncAll = () => {
    const buildings = getTouchedBuildings().map(buildingFormDataToBuilding);
    const nextBuilding = () => fromNullable(buildings.pop()).map(saveBuilding);

    const parcels = getTouchedParcels().map(parcelFormDataToParcel);
    const nextParcel = () => fromNullable(parcels.pop()).map(saveParcel);

    const process = (): void => {
        nextBuilding().map(p => p.then(process));
        nextParcel().map(p => p.then(process));
    };
    process();
};

export const monitorOnline = () =>
    online(quality => {
        if (getOnlineQuality() !== computeOnlineQuality(quality)) {
            assign('app/online', quality);
        }
    });

export const loadBuildingHistory = (id: uuid) => {
    assign('history/building', remoteLoading);
    fetchBuildingHistory(id)
        .then(h => assign('history/building', remoteSuccess(h)))
        .catch(err => assign('history/building', remoteError(`${err}`)));
};

export const loadParcelHistory = (id: uuid) => {
    assign('history/parcel', remoteLoading);
    fetchParcelHistory(id)
        .then(ps =>
            assign(
                'history/parcel',
                remoteSuccess(ps.map(p => ({ ...p, builtArea: null })))
            )
        )
        .catch(err => assign('history/parcel', remoteError(`${err}`)));
};

export const switchSurveyType = (st: SurveyType) => {
    if (getLayout() === 'index') {
        navigateIndex(st);
    } else if (getLayout() === 'preview') {
        getFormId().foldL(
            () => navigateIndex(st),
            id => navigatePreview(st, id)
        );
    }
};

// TODO: later
// export const clearUntouchedBuildings = () => {
//     dispatch('form/buildings', forms =>
//         forms.filter(buildingFormDataIsTouched)
//     );
//     updateSitexBuildingLayer();
// };

// export const clearUntouchedParcels = () => {
//     dispatch('form/parcels', forms => forms.filter(parcelFormDataIsTouched));
//     updateSitexParcelLayer();
// };

export const setLegendType = (legendType: LegendType) => {
    assign('app/legend', legendType);
    dispatch('port/map/view', view => ({ ...view, dirty: 'style' }));
};

export const updateBuildingAreas = () => {
    scopeOption()
        .let('form', getCurrentBuildingForm())
        .let('poly', ({ form }) =>
            fromEither(MultiPolygonIO.decode(form.geom.value))
        )
        .map(({ poly, form }) => {
            const area = Math.round(turfArea(toLonLat(poly)) * 100) / 100;
            setBuildingFormFieldValue('groundarea')(updateNumber(area));

            fromNullable(form.occupancys.value).map(occups => {
                const levels = occups.reduce((acc, o) => {
                    acc.add(o.level);
                    return acc;
                }, new Set<number>());
                const newOccupations: BuildingOccupation[] = [];
                levels.forEach(level => {
                    const levelOccups = occups.filter(o => o.level === level);
                    const total = levelOccups.reduce(
                        (acc, o) => acc + o.area,
                        0
                    );
                    const scale = area / total;
                    levelOccups.forEach(occup => {
                        newOccupations.push({
                            ...occup,
                            area: occup.area * scale,
                        });
                    });
                });

                setBuildingFormFieldValue('occupancys')(
                    updateBuildingOccupation(newOccupations)
                );
            });
        });
};

export const updateParcelAreas = () => {
    scopeOption()
        .let('form', getCurrentParcelForm())
        .let('poly', ({ form }) =>
            fromEither(MultiPolygonIO.decode(form.geom.value))
        )
        .map(({ poly, form }) => {
            const builtArea = getBuiltArea(poly);
            const totalArea = Math.round(turfArea(toLonLat(poly)) * 100) / 100;
            const occupArea = totalArea - builtArea;

            setParcelFormFieldValue('groundarea')(updateNumber(totalArea));

            fromNullable(form.occupancys.value).map(occups => {
                const total = occups.reduce((acc, o) => acc + o.area, 0);
                const scale = occupArea / total;
                const newOccupations: ParcelOccupation[] = [];
                occups.forEach(occup => {
                    newOccupations.push({
                        ...occup,
                        area: occup.area * scale,
                    });
                });

                setParcelFormFieldValue('occupancys')(
                    updateParcelOccupation(newOccupations)
                );
            });
        });
};

export const setExtraLayerTab = assignK('extra/layers/tab');

logger('loaded');
