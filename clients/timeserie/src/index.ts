import { displayException } from 'sdi/app';
import { configure, defaultShape, IShape } from 'sdi/shape';
import { AppConfigIO, getMessage, source } from 'sdi/source';
import { app } from './app';
import './locale';
import {
    defaultTimeserieShape,
    defaultAppShape,
    defaultMapState,
    defaultTableShape,
    defaultGeocoderState,
} from './shape';

export const main = (SDI: any) =>
    AppConfigIO.decode(SDI).fold(
        errors => {
            const textErrors = errors.map(e => getMessage(e.value, e.context));
            displayException(textErrors.join('\n'));
        },
        config => {
            const initialState: IShape = {
                'app/codename': 'timeserie',
                ...defaultShape(config),
                ...defaultAppShape(),
                ...defaultMapState(),
                ...defaultTableShape(),
                ...defaultTimeserieShape(),
                ...defaultGeocoderState(),
            };

            const stateActions = source<IShape>(['app/lang']);
            const store = stateActions(initialState);
            configure(store);
            const start = app(config.args)(store);
            start();
        }
    );
