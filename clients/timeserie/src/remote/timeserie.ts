import * as io from 'io-ts';
import { MessageRecordIO } from 'sdi/source/io/io';
import { fetchIO } from 'sdi/source';
import { getApiUrl } from 'sdi/app';
import { fromNullable } from 'fp-ts/lib/Option';
import { ParameterType } from '../types';

// tslint:disable-next-line:variable-name
const GroupDataIO = io.interface(
    {
        id: io.Integer,
        name: MessageRecordIO,
        sort: io.number,
    },
    'GroupDataIO'
);

// tslint:disable-next-line:variable-name
const NormIO = io.interface(
    {
        options: io.dictionary(io.string, io.any),
        values: io.dictionary(io.string, io.number),
    },
    'NormIO'
);

const NormNamesIO = io.dictionary(io.string, MessageRecordIO);

export type GroupData = io.TypeOf<typeof GroupDataIO>;
export type Norm = io.TypeOf<typeof NormIO>;
export type NormNames = io.TypeOf<typeof NormNamesIO>;

export const getNormOption =
    <T>(key: string) =>
    (norm: Norm) =>
        fromNullable<T>(norm.options[key]);

// tslint:disable-next-line:variable-name
const ParameterIO = io.interface(
    {
        id: io.Integer,
        code: io.union([io.string, io.null]),
        name: MessageRecordIO,
        unit: io.union([io.string, io.null]),
        groups: io.array(GroupDataIO),
        norms: io.array(NormIO),
    },
    'ParameterIO'
);

// tslint:disable-next-line:variable-name
const ParameterListIO = io.array(ParameterIO, 'ParameterListIO');

export type Parameter = io.TypeOf<typeof ParameterIO>;
export type ParameterList = io.TypeOf<typeof ParameterListIO>;

export const fetchParameterListSurface = () =>
    fetchIO(ParameterListIO, getApiUrl('geodata/water/surface/parameters/'));

export const fetchParameterListgroundQuality = () =>
    fetchIO(
        ParameterListIO,
        getApiUrl('geodata/water/ground/parameters/quality/')
    );

export const fetchParameterListgroundQuantity = () =>
    fetchIO(
        ParameterListIO,
        getApiUrl('geodata/water/ground/parameters/quantity/')
    );

// tslint:disable-next-line:variable-name
const RemoteTimeserieDataPointIO = io.tuple(
    [io.string, io.number, io.number],
    'TimeserieDataPointIO'
);

// tslint:disable-next-line:variable-name
const RemoteTimeserieDataIO = io.array(
    RemoteTimeserieDataPointIO,
    'TimeserieDataIO'
);

export type RemoteTimeserieDataPoint = io.TypeOf<
    typeof RemoteTimeserieDataPointIO
>;
export type RemoteTimeserieData = io.TypeOf<typeof RemoteTimeserieDataIO>;

export type TimeserieDataPoint = [number, number, number];
export type TimeserieData = TimeserieDataPoint[];

export const tsTime = (p: TimeserieDataPoint) => p[0];
export const tsValue = (p: TimeserieDataPoint) => p[1];
export const tsFactor = (p: TimeserieDataPoint) => p[2];

export const fetchTimeserieSurface = (
    station: number | string,
    parameter: number
) =>
    fetchIO(
        RemoteTimeserieDataIO,
        getApiUrl(`geodata/water/surface/timeserie/${station}/${parameter}`)
    );

export const fetchTimeserieGroundQuality = (
    station: number | string,
    parameter: number
) =>
    fetchIO(
        RemoteTimeserieDataIO,
        getApiUrl(
            `geodata/water/ground/timeserie/quality/${station}/${parameter}`
        )
    );

export const fetchTimeserieGroundQuantity = (
    station: number | string,
    parameter: number
) =>
    fetchIO(
        RemoteTimeserieDataIO,
        getApiUrl(
            `geodata/water/ground/timeserie/quantity/${station}/${parameter}`
        )
    );

export const fetchNormNames = () =>
    fetchIO(NormNamesIO, getApiUrl('geodata/water/info/norms'));

// tslint:disable-next-line:variable-name
const WaterBodyIO = io.interface(
    {
        kind: io.union([io.literal('ground'), io.literal('surface')]),
        id: io.number,
        name: MessageRecordIO,
        code: io.string,
    },
    'WaterBodyIO'
);

// tslint:disable-next-line:variable-name
const WaterBodyListIO = io.array(WaterBodyIO);

export type WaterBodySelection = {
    wb: WaterBody[];
    pType: ParameterType;
};

export type WaterBody = io.TypeOf<typeof WaterBodyIO>;
export type WaterBodyList = io.TypeOf<typeof WaterBodyListIO>;

export const fetchWaterBodies = () =>
    fetchIO(WaterBodyListIO, getApiUrl('geodata/water/info/waterbodies'));

const QuantParamIO = io.dictionary(io.string, io.number, 'QuantParamIO');
export const QuanParamResponseIO = io.interface(
    {
        tag: io.literal('count'),
        parameters: QuantParamIO,
    },
    'QuanParamResponseIO'
);
export type QuantParam = io.TypeOf<typeof QuantParamIO>;

const QuantStationIO = io.dictionary(io.string, io.number, 'QuantStationIO');
export const QuanStationResponseIO = io.interface(
    {
        tag: io.literal('count'),
        stations: QuantStationIO,
    },
    'QuanStationResponseIO'
);
export type QuantStation = io.TypeOf<typeof QuantStationIO>;

export const fetchQuantParamSurface = (id: number) =>
    fetchIO(
        QuanParamResponseIO,
        getApiUrl(`geodata/water/surface/timeserie/parameter/count/${id}`)
    ).then(({ parameters }) => parameters);

export const fetchQuantParamGround = (id: number) =>
    fetchIO(
        QuanParamResponseIO,
        getApiUrl(`geodata/water/ground/timeserie/parameter/count/${id}`)
    ).then(({ parameters }) => parameters);

export const fetchQuantStationSurface = (id: number) =>
    fetchIO(
        QuanStationResponseIO,
        getApiUrl(`geodata/water/surface/timeserie/station/count/${id}`)
    ).then(({ stations }) => stations);

export const fetchQuantStationGroundQual = (id: number) =>
    fetchIO(
        QuanStationResponseIO,
        getApiUrl(`geodata/water/ground/timeserie/station/count/${id}`)
    ).then(({ stations }) => stations);
