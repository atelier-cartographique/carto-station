import { makeLabelAndIcon, makeIcon, makeLabel } from 'sdi/components/button';
import tr, { fromRecord } from 'sdi/locale';
import { divTooltipTopRight } from 'sdi/components/tooltip';
import {
    navigateIndex,
    navigatePlot,
    navigateTable,
    getConfig,
    encodeConfig,
    Config,
    Level,
    navigateConfig,
    Mode,
} from '../events/route';
import {
    getLevel,
    getSelectedParameterList,
    getFeatureCollectionSurface,
    getFeatureCollectionGQuality,
    getFeatureCollectionGQuantity,
    getSurfaceParameterList,
    getGroundQualityParameterList,
    getGroundQuantityParameterList,
    findStationName,
    getDisplayMode,
    findParameter,
    getSelectedStationName,
    getSelectedParameter,
    getMode,
} from '../queries/timeserie';
import { DIV, A, SPAN, H3 } from 'sdi/components/elements';
import { getApiUrl, getLang } from 'sdi/app';
import { nameToString } from 'sdi/components/button/names';
import {
    clearHighlightStation,
    clearHighligthParameter,
    clearPointSelection,
    clearSelectedParameter,
    clearStationSelection,
    defaultSelectedParameter,
    lidToParameterType,
    setDisplayMode,
} from '../events/timeserie';
import { fromNullable } from 'fp-ts/lib/Option';
import { activityLogger } from '../events/app';
import { exportAction } from 'sdi/activity';
import { ClassAttributes, AllHTMLAttributes, ReactNode } from 'react';
import { renderRadioIn, renderRadioOut } from 'sdi/components/input';

const levelList: Level[] = ['surface', 'ground'];
const levels = (l: Level) =>
    l === 'surface'
        ? DIV({}, tr.ts('surfaceWater'))
        : DIV({}, tr.ts('groundWater'));
const selectLevel = (l: Level) => {
    clearStationSelection();
    navigateIndex(l, getMode());
};

export const levelSwitch = () =>
    getLevel().map(l =>
        renderRadioIn(
            'level-switch',
            levels,
            selectLevel,
            'switch'
        )(levelList, l)
    );

const modes = (m: Mode) =>
    m === 'station' ? DIV({}, tr.ts('perstation')) : DIV({}, tr.ts('perparam'));
const selectMode = (m: Mode) => {
    clearStationSelection();
    getLevel().map(l => navigateIndex(l, m));
};

export const modeSwitch = () =>
    renderRadioOut(
        'level-switch',
        modes,
        selectMode
    )(['station', 'param'], getMode());

export const deselectParamButton = makeIcon('cancel', 3, 'times', {
    position: 'bottom-right',
    text: () => tr.ts('deselectParameter'),
});
export const deselectStationButton = makeIcon('cancel', 3, 'times', {
    position: 'bottom-right',
    text: () => tr.ts('deselectStation'),
});
export const deselectBigButton = makeLabelAndIcon('cancel', 2, 'times', () =>
    tr.core('cancel')
);

// Home - Index button
const renderButtonIndex = makeLabelAndIcon('navigate', 2, 'home', () =>
    tr.ts('backHome')
);
export const renderBackHomeBtn = () =>
    DIV(
        { className: 'back-home' },
        renderButtonIndex(() => getLevel().map(l => navigateIndex(l)))
    );

const surfaceButton = makeLabelAndIcon('navigate', 2, 'exchange-alt', () =>
    tr.ts('seeSurfaceWater')
);
const groundButton = makeLabelAndIcon('navigate', 2, 'exchange-alt', () =>
    tr.ts('seeGroundWater')
);

export const renderChangeLevelBtn = () =>
    getLevel().map(l => {
        switch (l) {
            case 'ground':
                return DIV(
                    { className: 'change-level' },
                    surfaceButton(() => navigateIndex('surface'))
                );
            case 'surface':
                return DIV(
                    { className: 'change-level' },
                    groundButton(() => navigateIndex('ground'))
                );
        }
    });

// change per param switch button

const modeStationButton = makeLabelAndIcon('navigate', 2, 'exchange-alt', () =>
    tr.ts('searchByStation')
);

const modeParamButton = makeLabelAndIcon('navigate', 2, 'exchange-alt', () =>
    tr.ts('searchByParam')
);

export const renderChangeModeButton = (
    level: Level,
    config: Config,
    mode: Mode
) => {
    switch (mode) {
        case 'param':
            return DIV(
                { className: 'change-mode' },
                modeStationButton(() =>
                    navigateConfig(level, config, 'station')
                )
            );
        case 'station':
            return DIV(
                { className: 'change-mode' },
                modeParamButton(() => navigateConfig(level, config, 'param'))
            );
    }
};

// Plot button
const noop = () => void 0;

const buttonPlot = makeLabelAndIcon('navigate', 1, 'chart-line', () =>
    tr.ts('chart')
);
export const renderButtonPlot = () => {
    const len = getSelectedParameterList().length;
    if (len > 0) {
        return buttonPlot(() => {
            getLevel().map(l =>
                getConfig().map(c => navigatePlot(l, c, getMode()))
            );
            // clearWindowStart();
            // clearWindowEnd();
        }, 'active');
    }
    return buttonPlot(noop, 'inactive');
};

const buttonPlotSmall = makeIcon('navigate', 1, 'chart-line', {
    position: 'right',
    text: () => tr.ts('chart'),
});
export const renderButtonPlotSmall = () =>
    buttonPlotSmall(() =>
        getLevel().map(l => getConfig().map(c => navigatePlot(l, c, getMode())))
    );

// Table button
const buttonTable = makeLabelAndIcon('navigate', 1, 'table', () =>
    tr.ts('table')
);
export const renderButtonTable = () => {
    if (getSelectedParameterList().length > 0) {
        return buttonTable(() =>
            getLevel().map(l =>
                getConfig().map(c => navigateTable(l, c, getMode()))
            )
        );
    }
    return buttonTable(noop, 'inactive');
};

const buttonTableSmall = makeIcon('navigate', 1, 'table', {
    position: 'right',
    text: () => tr.ts('table'),
});
export const renderButtonTableSmall = () =>
    buttonTableSmall(() =>
        getLevel().map(l =>
            getConfig().map(c => navigateTable(l, c, getMode()))
        )
    );

export const renderDeselectAllStation = makeLabel('reset', 2, () =>
    tr.ts('deSelectAllStation')
);
export const renderSelectAllStation = makeLabel('reset', 2, () =>
    tr.ts('selectAllStation')
);

// Params button
export const renderButtonParams = makeLabelAndIcon(
    'navigate',
    1,
    'chevron-left',
    () => tr.ts('chooseParams')
);

// Zoom in cluster button
export const renderButtonZoomCluster = makeLabelAndIcon(
    'confirm',
    2,
    'search',
    () => tr.ts('clusterZoom')
);

// Reset date button
export const renderButtonInitDates = makeIcon('reset', 3, 'sync', {
    position: 'right',
    text: () => tr.ts('resetDate'),
});

export const renderButtonTableModeStations = makeLabel('select', 1, () =>
    tr.ts('perStation')
);

export const renderButtonTableModeParameters = makeLabel('select', 1, () =>
    tr.ts('perParameter')
);

const btnPerStation = () =>
    renderButtonTableModeStations(() => {
        clearHighligthParameter();
        clearSelectedParameter();
        clearPointSelection();
        setDisplayMode('per-station');
    });
const btnPerParam = () =>
    renderButtonTableModeParameters(() => {
        setDisplayMode('per-parameter');
        clearHighlightStation();
        clearPointSelection();
        defaultSelectedParameter();
    });

const stationLabel = () =>
    H3(
        'table-title',
        SPAN('key_label', `${tr.ts('tableStationLabel')} : `),
        getSelectedStationName().map(name => SPAN('key_value', name))
    );

const paramLabel = () =>
    H3(
        'table-title',
        SPAN('key_label', `${tr.ts('tableParameterLabel')} : `),
        getSelectedParameter().chain(({ kind, id }) =>
            findParameter(kind, id).map(p =>
                SPAN('key_value', `${fromRecord(p.name)} (${p.unit})`)
            )
        )
    );

export const renderDisplaySwitch = () => {
    const displayMode = getDisplayMode();
    switch (displayMode) {
        case 'per-station':
            return DIV('title-switch', stationLabel(), btnPerParam());
        case 'per-parameter':
            return DIV('title-switch', paramLabel(), btnPerStation());
    }
};

// button export CSV

const stationNames = (names: number[]) =>
    names.reduce((acc, val) => `${acc}-${findStationName(val)}`, '');

const trackedExport = (
    config: Config,
    props: ClassAttributes<HTMLAnchorElement> &
        AllHTMLAttributes<HTMLAnchorElement>,
    ...children: ReactNode[]
) =>
    getLevel().map(level =>
        A(
            {
                ...props,
                target: '_blank',
                onClick: () =>
                    activityLogger(exportAction(stationNames(config.s), level)),
                onKeyUp: () =>
                    activityLogger(exportAction(stationNames(config.s), level)),
            },
            ...children
        )
    );

export const renderButtonCSV = () =>
    DIV(
        { className: 'btn btn-navigate  btn-2 label-and-icon' },
        // renderButtonCSV(() => '')
        getConfig().map(config =>
            trackedExport(
                config,
                {
                    href: getApiUrl(
                        `geodata/water/csv/${getLang()}/${encodeConfig(config)}`
                    ),
                },
                tr.ts('exportCSV')
            )
        ),
        DIV({ className: 'icon' }, nameToString('download'))
    );

const selectAllStations = ({ k, p, w }: Config) =>
    fromNullable(k)
        .chain(lidToParameterType)
        .chain(kind => {
            switch (kind) {
                case 'surface':
                    return getFeatureCollectionSurface();
                case 'ground-quality':
                    return getFeatureCollectionGQuality();
                case 'ground-quantity':
                    return getFeatureCollectionGQuantity();
            }
        })
        .map<Config>(fc => ({
            k,
            p,
            w,
            s: fc.features.map(({ id }) =>
                typeof id === 'number' ? id : parseInt(id, 10)
            ),
        }));

const selectAllParameters =
    (stationId: number) =>
    ({ k, w }: Config) =>
        fromNullable(k)
            .chain(lidToParameterType)
            .map(kind => {
                switch (kind) {
                    case 'surface':
                        return getSurfaceParameterList();
                    case 'ground-quality':
                        return getGroundQualityParameterList();
                    case 'ground-quantity':
                        return getGroundQuantityParameterList();
                }
            })
            .map<Config>(parameters => ({
                k,
                w,
                s: [stationId],
                p: parameters.map(par => par.id),
            }));

export const renderButtonCSVAllStations = () =>
    DIV(
        { className: 'btn btn-navigate  btn-2 label-and-icon' },
        getConfig()
            .chain(selectAllStations)

            .map(config =>
                trackedExport(
                    config,
                    {
                        href: getApiUrl(
                            `geodata/water/csv/${getLang()}/${encodeConfig(
                                config
                            )}`
                        ),
                    },
                    tr.ts('exportAllStations')
                )
            ),

        DIV({ className: 'icon' }, nameToString('download'))
    );

export const renderButtonCSVAllParameters = (stationId: number) =>
    divTooltipTopRight(
        tr.ts('exportAllParameters'),
        {},
        DIV(
            {
                className: 'btn icon-only btn-3',
                onClick: e => e.stopPropagation(),
            },
            getConfig()
                .chain(selectAllParameters(stationId))
                .map(config =>
                    trackedExport(
                        config,
                        {
                            href: getApiUrl(
                                `geodata/water/csv/${getLang()}/${encodeConfig(
                                    config
                                )}`
                            ),
                        },
                        DIV({ className: 'icon' }, nameToString('download'))
                    )
                )
        )
    );

// close button

export const renderButtonCollapse = makeIcon('close', 3, 'chevron-down', {
    position: 'left',
    text: () => tr.core('collapse'),
}); // TODO: check position

// expand button

export const renderButtonExpand = makeIcon('open', 3, 'chevron-right', {
    position: 'left',
    text: () => tr.core('expand'),
}); // TODO: check position

export const infoBtn = makeIcon('info', 3, 'info-circle', {
    text: () => tr.core('info'),
    position: 'top-right',
});
