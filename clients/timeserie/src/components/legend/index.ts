import { DIV } from 'sdi/components/elements';
import { getDisplayMode } from '../../queries/timeserie';

import renderParamsLegend from './param-legend';
import renderStationLegend from './station-legend';

const render = () => {
    switch (getDisplayMode()) {
        case 'per-station':
            return DIV(
                'per-station',
                renderStationLegend(),
                renderParamsLegend()
            );
        case 'per-parameter':
            return DIV(
                'per-parameter',
                renderParamsLegend(),
                renderStationLegend()
            );
    }
};

export default render;
