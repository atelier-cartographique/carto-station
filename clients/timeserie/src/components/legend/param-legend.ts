import { none, some } from 'fp-ts/lib/Option';

import tr, { fromRecord } from 'sdi/locale';
import { DIV, H3, NodeOrOptional, SPAN } from 'sdi/components/elements';
import { renderCheckbox } from 'sdi/components/input/checkbox';
import { ParameterSelection, TimeserieDataStatus } from '../../../src/types';
import { Parameter } from '../../../src/remote/timeserie';
import {
    getSelectedParameterList,
    getColor,
    getSelectedKind,
    isSelectedNorm,
    getNormName,
    getParamUnit,
    getPlotNorms,
    getTimeserieDataStatus,
    getDisplayMode,
    isSelectedParam,
    isHighlightedParam,
    ParameterWithKind,
    hasParamInfoOpt,
} from '../../queries/timeserie';
import {
    deselectNorm,
    highlightAndSelectParam,
    selectNorm,
    switchParamInfo,
} from '../../events/timeserie';

import { PlotNorm } from '../plot/types';
import stats, { renderDataNotInWindow, renderDataNotOnStation } from '../stats';
import { renderStyleLegendItem } from './plot';
import { infoBtn, renderButtonCSVAllStations } from '../button';

const normClassName = (norm: PlotNorm, { id }: Parameter) =>
    isSelectedNorm({ paramId: id, norm })
        ? 'legend-item__norms selected'
        : 'legend-item__norms';

const renderNormCheckbox = (paramId: number, norm: PlotNorm) =>
    renderCheckbox(
        'norm-switch',
        () => tr.ts('showNorm'),
        v => (v ? selectNorm(paramId, norm) : deselectNorm(paramId, norm))
    );

const renderNormItemStyle = (i: number) =>
    DIV(
        'item-style',
        DIV({
            className: 'item-style--element',
            style: {
                borderBottomColor: getColor(i),
            },
        })
    );

const renderNormItem = (norm: PlotNorm, param: Parameter, i: number) =>
    DIV(
        `${normClassName(norm, param)}`,
        renderNormItemStyle(i),
        DIV(
            'norm__item',
            DIV('norm__label', getNormName(norm.name)),
            DIV('norm__value', norm.value),
            DIV(
                'norm-checkbox__wrapper',
                renderNormCheckbox(
                    param.id,
                    norm
                )(isSelectedNorm({ paramId: param.id, norm }))
            )
        )
    );

const renderNameLegendItem = (param: Parameter) =>
    DIV(
        'item-label',
        fromRecord(param.name),
        getParamUnit(param).map(u => SPAN('item-unit', `\u00A0(${u}) `))
    );

const renderParamNorm = (param: Parameter, i: number) =>
    DIV('', ...getPlotNorms(param.id).map(n => renderNormItem(n, param, i)));

const renderStyleLegendItemNoStyle = () =>
    DIV(
        { className: 'item-style' },
        DIV({
            className: 'item-style--element',
        })
    );

const selectedClass = (param: ParameterSelection) =>
    (isSelectedParam(param.id) && getDisplayMode() === 'per-parameter') ||
    (isHighlightedParam(param) && getDisplayMode() === 'per-station')
        ? 'active'
        : 'inactive';

// const onChange = (paramId: number) => (event: 'expand' | 'collapse') =>
//     event == 'collapse'
//         ? clearSelectedParameter()
//         : highlightAndSelectParam(paramId);

const renderParamHeader = (param: ParameterWithKind, i: number) =>
    DIV(
        {
            className: 'legend-item__title',
            onClick: () => highlightAndSelectParam(param.kind, param.id),
        },
        renderStyleLegendItem(i, 'per-station'),
        infoBtn(() => switchParamInfo({ id: param.id, kind: param.kind })),
        renderNameLegendItem(param)
    );

const renderLegendItemHasData = (param: ParameterWithKind, i: number) =>
    DIV(
        {
            key: `${i}-${param.id}`,
            className: `plot-legend-item ${selectedClass({
                id: param.id,
                kind: param.kind,
            })}`,
        },
        renderParamHeader(param, i),
        hasParamInfoOpt(param).map(() => stats(param)),
        // renderCollapsibleWrapper(
        //     `param-${param.id}`,
        //     renderParamHeader(param, i),
        //     stats(param),
        //     onChange(param.id)
        // ),
        renderParamNorm(param, i)
    );

const renderLegendItemUnquantified = () =>
    getSelectedKind().chain(k => {
        if (k === 'ground-quality' || k === 'surface') {
            return some(
                DIV(
                    'plot-legend-item unquantified',
                    DIV(
                        'legend-item__title ',
                        renderStyleLegendItemNoStyle(),
                        DIV('item-label', tr.ts('unquantified'))
                    )
                )
            );
        }
        return none;
    });

const renderLegendItemNoData = (param: Parameter, message: NodeOrOptional) =>
    DIV(
        {
            className: 'plot-legend-item plot-legend-item--no-data ',
            key: `rp - ${param.id} `,
        },
        DIV('legend-item__title', renderNameLegendItem(param)),
        message
    );

const legendItemDataNotInWindow = (param: Parameter) =>
    renderLegendItemNoData(param, renderDataNotInWindow(param));

const renderLegendItemStationHasNoData = (param: Parameter) =>
    renderLegendItemNoData(param, renderDataNotOnStation());

const renderDefaultItem = (
    param: ParameterWithKind,
    i: number,
    status: TimeserieDataStatus
) => {
    switch (getDisplayMode()) {
        case 'per-parameter':
            return renderLegendItemHasData(param, i);
        case 'per-station': {
            switch (status) {
                case 'has-data':
                    return renderLegendItemHasData(param, i);
                case 'no-data':
                    return renderLegendItemStationHasNoData(param);
                case 'not-in-window':
                    return legendItemDataNotInWindow(param);
            }
        }
    }
};

export const renderLegendItem = (param: ParameterWithKind, i: number) => {
    const tsStatus = getTimeserieDataStatus(param.id);

    return renderDefaultItem(param, i, tsStatus);
};

// const legendList = () => {
//     let index = -1;
//     return DIV(
//         'legend-items__wrapper',
//         // getSelectedParameterList().map(renderLegendItem),
//         getSelectedParameterList().map(param => {
//             switch (getDisplayMode()) {
//                 case 'per-parameter':
//                     index = index + 1;
//                     return renderLegendItemHasData(param, index);
//                 case 'per-station': {
//                     switch (getTimeserieDataStatus(param.id)) {
//                         case 'has-data':
//                             index = index + 1;
//                             return renderLegendItemHasData(param, index);
//                         case 'no-data':
//                             return renderLegendItemStationHasNoData(param);
//                         case 'not-in-window':
//                             return legendItemDataNotInWindow(param);
//                     }
//                 }
//             }
//         }),
//         renderLegendItemUnquantified()
//     )
// }

const renderParamsLegend = () =>
    DIV(
        'legend__group legend--params',
        H3({}, tr.ts('parameter')),
        DIV(
            'legend-items__wrapper',
            getSelectedParameterList().map(renderLegendItem),
            renderLegendItemUnquantified()
        ),
        DIV('actions', renderButtonCSVAllStations())
    );

export default renderParamsLegend;
