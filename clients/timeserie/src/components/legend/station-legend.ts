import tr from 'sdi/locale';
import {
    getSelectedStationList,
    getStationName,
    getSelectedStationIndex,
    getDisplayMode,
    hasStationInfoOpt,
    isHighlightedStation,
} from '../../queries/timeserie';
import {
    setSelectedStationIndex,
    switchHighligthStation,
    switchStationInfo,
} from '../../events/timeserie';

import { StationSelection } from 'timeserie/src/types';
import { renderCurrentFeatureInfo } from '../info/map';
import { tryNumber } from 'sdi/util';
import { infoBtn, renderButtonCSVAllParameters } from '../button';
import { DIV, H3 } from 'sdi/components/elements';
import { renderStyleLegendItem } from './plot';

const renderStationNameLegendItem = (station: StationSelection) =>
    DIV('item-label', getStationName(station));

const renderStationHeader = (station: StationSelection, i: number) =>
    DIV(
        {
            className: 'legend-item__title',
            onClick: () => {
                setSelectedStationIndex(i);
                switchHighligthStation(station);
            },
        },
        renderStyleLegendItem(i, 'per-parameter'),
        DIV(
            'actions',
            tryNumber(station.id).map(renderButtonCSVAllParameters),
            infoBtn(() => switchStationInfo(station))
        ),
        renderStationNameLegendItem(station)
    );

// const legendStationItemDataNotInWindow = (station: StationSelection) =>
//     DIV(
//         {
//             className: 'plot-legend-item plot-legend-item--no-data',
//             key: `rp - ${station.id}`,
//         },
//         DIV(
//             { className: 'legend-item__title' },
//             renderStationNameLegendItem(station)
//         )
//     );

// const renderStationLegendItemHasNoData = (station: StationSelection) =>
//     DIV(
//         {
//             className: 'plot-legend-item plot-legend-item--no-data',
//             key: `rp - ${station.id}`,
//         },
//         DIV(
//             {
//                 className: 'legend-item__title',
//             },

//             renderStationNameLegendItem(station)
//         )
//     );

const isStationSelected = (stationIndex: number) =>
    tryNumber(stationIndex)
        .map(id => id === getSelectedStationIndex())
        .getOrElse(false);

const selectedStationClass = (
    station: StationSelection,
    stationIndex: number
) =>
    (isStationSelected(stationIndex) && getDisplayMode() === 'per-station') ||
    (isHighlightedStation(station) && getDisplayMode() === 'per-parameter')
        ? 'active'
        : 'inactive';

const renderStationLegendItemHasData = (station: StationSelection, i: number) =>
    DIV(
        `station-item ${station.id} ${selectedStationClass(station, i)}`,
        renderStationHeader(station, i),
        hasStationInfoOpt(station).map(() => renderCurrentFeatureInfo())
    );

const renderStationLegend = () =>
    DIV(
        'legend__group stations__list',
        H3({}, tr.ts('selectedStations')),
        DIV(
            'legend-items__wrapper',
            getSelectedStationList().map(renderStationLegendItemHasData)
        )
    );

export default renderStationLegend;
