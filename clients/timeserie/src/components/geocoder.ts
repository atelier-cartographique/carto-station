/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// import { ChangeEvent, KeyboardEvent } from 'react';

// import { DIV, INPUT, SPAN } from 'sdi/components/elements';
// import { isENTER } from 'sdi/components/keycodes';

// import { centerOn, updateGeocoderInput, clearGeocoderResponse, searchAddress } from '../events/map';
// import { getGeocoderResponse } from '../queries/map';
// import { IUgWsResult, IUgWsAddress, IUgWsResponse } from 'sdi/ports/geocoder';
// import { makeIcon } from 'sdi/components/button';
// import tr from 'sdi/locale';

// const updateAddress = (e: ChangeEvent<HTMLInputElement>) => {
//     updateGeocoderInput(e.target.value);
// };

// const addressToString = (a: IUgWsAddress) => {
//     return `${a.street.name} ${a.number}, ${a.street.postCode} ${a.street.municipality}`;
// };

// const renderResults = (results: IUgWsResult[]) => {
//     return results.map((result, key) => {
//         const coords: [number, number] = [result.point.x, result.point.y];
//         return DIV({ className: 'adress-result', key },
//             SPAN({
//                 onClick: () => {
//                     clearGeocoderResponse();
//                     centerOn(coords)
//                 },
//             }, addressToString(result.address)));
//     });
// };

// const btnSearch = makeIcon('search', 3, 'search', { position: 'top', text: () => tr.core('search') });

// const renderInput = () => DIV({ className: 'tool geocoder' },
//     DIV({ className: 'tool-body adress' },
//         INPUT({
//             type: 'text',
//             name: 'adress',
//             placeholder: `${tr.core('address')}`,
//             onChange: updateAddress,
//             onKeyPress: (e: KeyboardEvent<HTMLInputElement>) => {
//                 if (isENTER(e)) {
//                     searchAddress();
//                 }
//             },
//         }),
//         btnSearch(searchAddress),
//     ));

// const renderResponse = (
//     response: IUgWsResponse,
// ) => DIV({ className: 'tool geocoder' },
//     DIV({ className: 'tool-body adress' }, renderResults(response.result)));

// const render = () =>
//     getGeocoderResponse()
//         .foldL(renderInput, renderResponse);

// export default render;
