import { DIV, H1, H2, SPAN } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { markdown } from 'sdi/ports/marked';

const renderHelptextHome1 = () =>
    H2('pitch', markdown(tr.ts('helptext:init1')));

const renderHelptextHome2 = () => H2('pitch sub-pitch', tr.core('loadingData'));

// const renderSwitchLevel = () =>
//     DIV(
//         'switch',
//         BUTTON(
//             {
//                 className: 'disabled',
//                 // onClick: () => '',
//             },
//             tr.ts('surfaceWater')
//         ),
//         BUTTON(
//             {
//                 className: 'disabled',
//                 // onClick: () => ''
//             },
//             tr.ts('groundWater')
//         )
//     );

const render = () =>
    DIV(
        'content content--index-init',
        H1({}, SPAN('bruwater-logo'), tr.ts('appName')),
        renderHelptextHome1(),
        renderHelptextHome2(),
        DIV('pitch', tr.ts('canTakeTime')),
        DIV('loader-spinner')
        // renderSwitchLevel()
    );

export default render;
