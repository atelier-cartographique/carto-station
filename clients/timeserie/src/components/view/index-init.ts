import { BUTTON, DIV, H1, H2, SPAN } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { navigateIndex } from 'timeserie/src/events/route';
import { clearStationSelection } from 'timeserie/src/events/timeserie';
import { markdown } from 'sdi/ports/marked';
import { setFocusId } from 'sdi/app/events';

const renderHelptextHome1 = () =>
    H2('pitch', markdown(tr.ts('helptext:init1')));
const renderHelptextHome2 = () =>
    H2('pitch sub-pitch', markdown(tr.ts('helptext:init2')));

const renderSwitchLevel = () =>
    DIV(
        'switch',
        BUTTON(
            {
                onClick: () => {
                    clearStationSelection();
                    navigateIndex('surface');
                    setFocusId(`head selected`);
                },
            },
            tr.ts('surfaceWater')
        ),
        BUTTON(
            {
                onClick: () => {
                    clearStationSelection();
                    navigateIndex('ground');
                    setFocusId(`head selected`);
                },
            },
            tr.ts('groundWater')
        )
    );

const render = () =>
    DIV(
        'content content--index-init',
        H1({}, SPAN('bruwater-logo'), tr.ts('appName')),
        renderHelptextHome1(),
        renderHelptextHome2(),
        renderSwitchLevel()
    );

export default render;
