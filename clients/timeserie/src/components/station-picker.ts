import { Option } from 'fp-ts/lib/Option';
import { makeLabel } from 'sdi/components/button';
import { DIV, H2, NodeOrOptional, UL } from 'sdi/components/elements';
import { inputText } from 'sdi/components/input';
import tr from 'sdi/locale';
import { Feature } from 'sdi/source';
import { tryNumber } from 'sdi/util';
import {
    getFilteredGQuality,
    getFilteredGQuantity,
    getFilteredSurface,
    getLevel,
    getMode,
    getSelectedStationList,
    getStationName,
    getStationSearch,
} from 'timeserie/src/queries/timeserie';
import {
    configRemoveStation,
    navigateInPlace,
    onlyForStationModeOpt,
} from '../events/route';
import { selectFeature, setStationSearch } from '../events/timeserie';
import { ParameterType, StationSelection } from '../types';
import { deselectStationButton } from './button';
import {
    renderFeatureGroundQual,
    renderFeatureGroundQuan,
    renderFeatureSurface,
} from './legend/select';

const unselectButton = (id: number) =>
    deselectStationButton(() =>
        getLevel().chain(level =>
            configRemoveStation(id).map(config =>
                navigateInPlace(level, config, getMode())
            )
        )
    );

const renderSelectedStation = (ss: StationSelection) =>
    DIV(
        { className: 'tag BKPstation--selected', key: `rs-${ss.id}` },
        tryNumber(ss.id).map(sid => unselectButton(sid)),
        DIV('station__label', getStationName(ss))
    );

const checkNoStation = (stationList: Option<NodeOrOptional[]>) => {
    const renderNoStation = DIV('no-stations', tr.ts('noStationForParam'));
    return stationList.fold(renderNoStation, list =>
        list.length > 0 ? list : renderNoStation
    );
};

export const renderSurfaceStationList = () =>
    DIV(
        'picker-stations',
        DIV(
            'selector-stations',
            UL(
                'station__list',
                checkNoStation(
                    getFilteredSurface().map(fs =>
                        fs.map(f =>
                            renderFeatureSurface(f, 'surface' as ParameterType)
                        )
                    )
                )
            )
        )
    );

const renderGroundQualityStationList = () =>
    DIV(
        'selector-stations',
        UL(
            'station__list',
            checkNoStation(
                getFilteredGQuality().map(fs =>
                    fs.map(f =>
                        renderFeatureGroundQual(
                            f,
                            'ground-quality' as ParameterType
                        )
                    )
                )
            )
        )
    );

const renderGroundQuantityStationList = () =>
    DIV(
        'selector-stations',
        UL(
            'station__list',
            getFilteredGQuantity().map(fs =>
                fs.map(f =>
                    renderFeatureGroundQuan(
                        f,
                        'ground-quantity' as ParameterType
                    )
                )
            )
        )
    );

// export const renderGroundStationList = () => [
//     renderGroundQualityStationList(),
//     renderGroundQuantityStationList(),
// ];
export const renderGroundStationList = () =>
    DIV(
        'picker-stations',
        renderGroundQualityStationList(),
        onlyForStationModeOpt(getMode()).map(() =>
            renderGroundQuantityStationList()
        )
    );

const selectAllStationBtn = makeLabel('select', 2, () =>
    tr.ts('selectAllStation')
);
const renderAllStationsBtn = (kind: ParameterType, fcOpt: Option<Feature[]>) =>
    selectAllStationBtn(() => fcOpt.map(fc => fc.map(selectFeature(kind))));

const filterInput = () =>
    inputText({
        key: 'parameter-search',
        get: getStationSearch,
        set: setStationSearch,
        attrs: {
            id: 'parameter-search',
            placeholder: tr.ts('filter'),
        },
        monitor: e => setStationSearch(e),
    });

export const stationPicker = () =>
    DIV(
        'pickers__wrapper pickers__wrapper--station ',
        getLevel().map(level => {
            if (level === 'ground') {
                return DIV(
                    'select-wrapper',
                    DIV(
                        'input-wrapper',
                        filterInput(),
                        renderAllStationsBtn(
                            'ground-quality',
                            getFilteredGQuality()
                        )
                    ),
                    renderGroundStationList()
                );
            } else {
                return DIV(
                    'select-wrapper',
                    DIV(
                        'input-wrapper',
                        filterInput(),
                        renderAllStationsBtn('surface', getFilteredSurface())
                    ),
                    renderSurfaceStationList()
                );
            }
        }),
        DIV(
            'selected-stations',
            H2('subtitle', tr.ts('stationsToDisplay')),
            DIV(
                'tag__list station__list',
                getSelectedStationList().map(renderSelectedStation)
            )
        )
    );

export default stationPicker;
