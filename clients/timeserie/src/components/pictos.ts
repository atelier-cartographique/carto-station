
import {Option, some, none} from 'fp-ts/lib/Option';
import { createElement } from 'react';
import { DIV } from 'sdi/components/elements';

export type PreserveAspectRatio = 
 | 'none'
 | 'xMinYMin'
 | 'xMidYMin'
 | 'xMaxYMin'
 | 'xMinYMid'
 | 'xMidYMid'
 | 'xMaxYMid'
 | 'xMinYMax'
 | 'xMidYMax'
 | 'xMaxYMax' ;


const pictos = {
"pic-BEBR_Bruxellien_Brusseliaan_5" : (preserveAspectRatio: PreserveAspectRatio) => createElement("svg", {"version": "1.1","x": "0px","y": "0px","viewBox": "0 0 37.3 37.3",id: 'Calque_2_00000029755133170651681550000004107998344638823581_',"space": "preserve", preserveAspectRatio }, createElement("style", {"type": "text/css"}, "	.ppic-BEBR_Bruxellien_Brusseliaan_5-st0{fill:none;}	.ppic-BEBR_Bruxellien_Brusseliaan_5-st1{fill:#9FD1B6;}	.ppic-BEBR_Bruxellien_Brusseliaan_5-st2{fill:#00799C;}#Calque_2_00000029755133170651681550000004107998344638823581_{ enable-background:new 0 0 37.3 37.3; }"),
createElement("rect", {"y": "0","className": "ppic-BEBR_Bruxellien_Brusseliaan_5-st0","width": "37.3","height": "37.3"}),
createElement("polygon", {"className": "ppic-BEBR_Bruxellien_Brusseliaan_5-st1","points": "9.6,7.6 14.6,4.9 20,6.1 24,2.5 27,5.4 27.4,9.7 26.3,12.8 31.5,16.1 33.3,22.6 30.9,22.8 29.6,25.2   34.2,28.2 21,34.7 20.7,32.9 13.2,31.4 11.6,27 9.8,26.6 10.6,23.8 7.2,25 2.2,22.5 4.2,19.2 7.5,18.5 8,14.4 6.6,12.8 9.2,9.7 "}),
createElement("rect", {"y": "0","className": "ppic-BEBR_Bruxellien_Brusseliaan_5-st0","width": "37.3","height": "37.3"}),
createElement("polygon", {"className": "ppic-BEBR_Bruxellien_Brusseliaan_5-st2","points": "29.6,25.2 30.9,22.8 33.3,22.6 31.5,16.1 30.4,15.4 27.7,24.1 28.9,25 27.6,25.2 25.6,28.5 26,25   23.8,24 26.4,23.8 29.7,14.9 26.3,12.8 27.4,9.7 27,5.4 25.6,4.1 24.9,6.4 22.6,7.9 20.7,9.7 21.2,11.3 20.4,13.9 21,16.1 21,18.7   20.2,21.6 20.2,17.7 19.7,16.9 19,15.3 19.4,12.5 19.1,11.8 12.8,25.8 13.4,27.2 16.1,26.3 14,29 16.2,28.6 13.2,31.4 20.7,32.9   21,34.7 34.2,28.2 "})),
"pic-BEBR_Socle_Sokkel_2" : (preserveAspectRatio: PreserveAspectRatio) => createElement("svg", {"version": "1.1","x": "0px","y": "0px","viewBox": "0 0 37.3 37.3",id: 'Calque_2_00000073714006444196532500000012255692863847481783_',"space": "preserve", preserveAspectRatio }, createElement("style", {"type": "text/css"}, "	.ppic-BEBR_Socle_Sokkel_2-st0{fill:none;}	.ppic-BEBR_Socle_Sokkel_2-st1{fill:#9FD1B6;}	.ppic-BEBR_Socle_Sokkel_2-st2{fill:#00799C;}#Calque_2_00000073714006444196532500000012255692863847481783_{ enable-background:new 0 0 37.3 37.3; }"),
createElement("rect", {"y": "0","className": "ppic-BEBR_Socle_Sokkel_2-st0","width": "37.3","height": "37.3"}),
createElement("polygon", {"className": "ppic-BEBR_Socle_Sokkel_2-st1","points": "9.6,7.6 14.6,4.9 20,6.1 24,2.5 27,5.4 27.4,9.7 26.3,12.8 31.5,16.1 33.3,22.6 30.9,22.8 29.6,25.2   34.2,28.2 21,34.7 20.7,32.9 13.2,31.4 11.6,27 9.8,26.6 10.6,23.8 7.2,25 2.2,22.5 4.2,19.2 7.5,18.5 8,14.4 6.6,12.8 9.2,9.7 "}),
createElement("polygon", {"className": "ppic-BEBR_Socle_Sokkel_2-st2","points": "21,34.7 31,29.8 21.8,24.7 21.2,19.5 19.9,19.5 19.1,21.4 17.2,22.7 8.9,22.4 6.5,24.6 7.2,25   10.6,23.8 9.8,26.6 11.6,27 13.2,31.4 20.7,32.9 "})),
"pic-brussels" : (preserveAspectRatio: PreserveAspectRatio) => createElement("svg", {"id": "ppic-brussels-Calque_2","data-name": "Calque 2","viewBox": "0 0 37.3 37.3", preserveAspectRatio }, createElement("defs", {}, createElement("style", {}, "      .ppic-brussels-cls-1 {        fill: none;      }      .ppic-brussels-cls-2 {        fill: #00799c;      }    ")),
createElement("g", {"id": "ppic-brussels-Calque_2-2","data-name": "Calque 2"}, createElement("g", {}, createElement("rect", {"className": "ppic-brussels-cls-1","width": "37.3","height": "37.3"}),
createElement("polygon", {"className": "ppic-brussels-cls-2","points": "9.56 7.61 14.57 4.91 19.97 6.07 23.95 2.47 27.04 5.43 27.42 9.67 26.27 12.75 31.54 16.09 33.34 22.65 30.89 22.77 29.61 25.22 34.24 28.17 21 34.73 20.74 32.93 13.16 31.38 11.62 27.02 9.82 26.63 10.59 23.8 7.25 24.96 2.24 22.52 4.16 19.18 7.51 18.53 8.02 14.42 6.61 12.75 9.18 9.67 9.56 7.61"})))),
"pic-non renseigné" : (preserveAspectRatio: PreserveAspectRatio) => createElement("svg", {"id": "ppic-nonrenseigné-Calque_2","data-name": "Calque 2","viewBox": "0 0 37.3 37.3", preserveAspectRatio }, createElement("defs", {}, createElement("style", {}, "      .ppic-nonrenseigné-cls-1 {        stroke: #00799c;        stroke-linecap: round;        stroke-linejoin: round;        stroke-width: 4px;      }      .ppic-nonrenseigné-cls-1, .ppic-nonrenseigné-cls-2 {        fill: none;      }    ")),
createElement("g", {"id": "ppic-nonrenseigné-Calque_2-2","data-name": "Calque 2"}, createElement("g", {}, createElement("rect", {"className": "ppic-nonrenseigné-cls-2","width": "37.3","height": "37.3"}),
createElement("g", {}, createElement("rect", {"className": "ppic-nonrenseigné-cls-2","width": "37.3","height": "37.3"}),
createElement("g", {}, createElement("polygon", {"className": "ppic-nonrenseigné-cls-2","points": "9.56 7.61 14.57 4.91 19.97 6.07 23.95 2.47 27.04 5.43 27.42 9.67 26.27 12.75 31.54 16.09 33.34 22.65 30.89 22.77 29.61 25.22 34.24 28.17 21 34.73 20.74 32.93 13.16 31.38 11.62 27.02 9.82 26.63 10.59 23.8 7.25 24.96 2.24 22.52 4.16 19.18 7.51 18.53 8.02 14.42 6.61 12.75 9.18 9.67 9.56 7.61"}),
createElement("line", {"className": "ppic-nonrenseigné-cls-1","x1": "23.85","y1": "4.43","x2": "23.85","y2": "4.43"}),
createElement("line", {"className": "ppic-nonrenseigné-cls-1","x1": "18.34","y1": "7.34","x2": "18.34","y2": "7.34"}),
createElement("line", {"className": "ppic-nonrenseigné-cls-1","x1": "12.54","y1": "7.64","x2": "12.54","y2": "7.64"}),
createElement("line", {"className": "ppic-nonrenseigné-cls-1","x1": "8.71","y1": "12.23","x2": "8.71","y2": "12.23"}),
createElement("line", {"className": "ppic-nonrenseigné-cls-1","x1": "8.25","y1": "18.04","x2": "8.25","y2": "18.04"}),
createElement("line", {"className": "ppic-nonrenseigné-cls-1","x1": "4.13","y1": "22.17","x2": "4.13","y2": "22.17"}),
createElement("line", {"className": "ppic-nonrenseigné-cls-1","x1": "9.78","y1": "24.15","x2": "9.78","y2": "24.15"}),
createElement("line", {"className": "ppic-nonrenseigné-cls-1","x1": "11.77","y1": "29.35","x2": "11.77","y2": "29.35"}),
createElement("line", {"className": "ppic-nonrenseigné-cls-1","x1": "17.12","y1": "31.03","x2": "17.12","y2": "31.03"}),
createElement("line", {"className": "ppic-nonrenseigné-cls-1","x1": "22.62","y1": "32.71","x2": "22.62","y2": "32.71"}),
createElement("line", {"className": "ppic-nonrenseigné-cls-1","x1": "27.06","y1": "30.57","x2": "27.06","y2": "30.57"}),
createElement("line", {"className": "ppic-nonrenseigné-cls-1","x1": "31.64","y1": "28.13","x2": "31.64","y2": "28.13"}),
createElement("line", {"className": "ppic-nonrenseigné-cls-1","x1": "27.21","y1": "9.32","x2": "27.21","y2": "9.32"}),
createElement("line", {"className": "ppic-nonrenseigné-cls-1","x1": "30.57","y1": "15.29","x2": "30.57","y2": "15.29"}),
createElement("line", {"className": "ppic-nonrenseigné-cls-1","x1": "24.92","y1": "14.52","x2": "24.92","y2": "14.52"}),
createElement("line", {"className": "ppic-nonrenseigné-cls-1","x1": "32.25","y1": "21.1","x2": "32.25","y2": "21.1"}),
createElement("line", {"className": "ppic-nonrenseigné-cls-1","x1": "27.21","y1": "24.31","x2": "27.21","y2": "24.31"})))))),
"pic-BEBR_Socle_Sokkel_1" : (preserveAspectRatio: PreserveAspectRatio) => createElement("svg", {"version": "1.1","x": "0px","y": "0px","viewBox": "0 0 37.3 37.3",id: 'Calque_2_00000003074032235717149970000004314435752694864831_',"space": "preserve", preserveAspectRatio }, createElement("style", {"type": "text/css"}, "	.ppic-BEBR_Socle_Sokkel_1-st0{fill:none;}	.ppic-BEBR_Socle_Sokkel_1-st1{fill:#9FD1B6;}	.ppic-BEBR_Socle_Sokkel_1-st2{fill:#00799C;}#Calque_2_00000003074032235717149970000004314435752694864831_{ enable-background:new 0 0 37.3 37.3; }"),
createElement("rect", {"y": "0","className": "ppic-BEBR_Socle_Sokkel_1-st0","width": "37.3","height": "37.3"}),
createElement("polygon", {"className": "ppic-BEBR_Socle_Sokkel_1-st1","points": "9.6,7.6 14.6,4.9 20,6.1 24,2.5 27,5.4 27.4,9.7 26.3,12.8 31.5,16.1 33.3,22.6 30.9,22.8 29.6,25.2   34.2,28.2 21,34.7 20.7,32.9 13.2,31.4 11.6,27 9.8,26.6 10.6,23.8 7.2,25 2.2,22.5 4.2,19.2 7.5,18.5 8,14.4 6.6,12.8 9.2,9.7 "}),
createElement("polygon", {"className": "ppic-BEBR_Socle_Sokkel_1-st2","points": "30.9,22.8 33.3,22.6 31.5,16.1 26.3,12.8 27.4,9.7 27,5.4 24,2.5 20,6.1 14.6,4.9 9.6,7.6 9.2,9.7   6.6,12.8 8,14.4 7.5,18.5 4.2,19.2 2.2,22.5 6.1,24.4 8.7,21.9 17.7,22.1 19.3,20.8 20.6,17.7 21.7,17.7 23.2,24.5 31.7,29.4   34.2,28.2 29.6,25.2 "})),
"pic-autre" : (preserveAspectRatio: PreserveAspectRatio) => createElement("svg", {"id": "ppic-autre-Calque_2","data-name": "Calque 2","viewBox": "0 0 37.3 37.3", preserveAspectRatio }, createElement("defs", {}, createElement("style", {}, "      .ppic-autre-cls-1 {        fill: #9fd1b6;      }      .ppic-autre-cls-2 {        stroke: #00799c;        stroke-linecap: round;        stroke-linejoin: round;        stroke-width: 4px;      }      .ppic-autre-cls-2, .ppic-autre-cls-3 {        fill: none;      }    ")),
createElement("g", {"id": "ppic-autre-Calque_2-2","data-name": "Calque 2"}, createElement("g", {}, createElement("rect", {"className": "ppic-autre-cls-3","width": "37.3","height": "37.3"}),
createElement("polygon", {"className": "ppic-autre-cls-1","points": "9.56 7.61 14.57 4.91 19.97 6.07 23.95 2.47 27.04 5.43 27.42 9.67 26.27 12.75 31.54 16.09 33.34 22.65 30.89 22.77 29.61 25.22 34.24 28.17 21 34.73 20.74 32.93 13.16 31.38 11.62 27.02 9.82 26.63 10.59 23.8 7.25 24.96 2.24 22.52 4.16 19.18 7.51 18.53 8.02 14.42 6.61 12.75 9.18 9.67 9.56 7.61"}),
createElement("line", {"className": "ppic-autre-cls-2","x1": "23.85","y1": "24.76","x2": "23.85","y2": "24.76"}),
createElement("line", {"className": "ppic-autre-cls-2","x1": "27.52","y1": "16.2","x2": "27.52","y2": "16.2"}),
createElement("line", {"className": "ppic-autre-cls-2","x1": "20.79","y1": "11.01","x2": "20.79","y2": "11.01"}),
createElement("line", {"className": "ppic-autre-cls-2","x1": "14.37","y1": "12.84","x2": "14.37","y2": "12.84"}),
createElement("line", {"className": "ppic-autre-cls-2","x1": "11.62","y1": "21.4","x2": "11.62","y2": "21.4"})))),
"pic-ZEN" : (preserveAspectRatio: PreserveAspectRatio) => createElement("svg", {"id": "ppic-ZEN-Calque_2","data-name": "Calque 2","viewBox": "0 0 37.3 37.3", preserveAspectRatio }, createElement("defs", {}, createElement("style", {}, "      .ppic-ZEN-cls-1 {        fill: #9fd1b6;      }      .ppic-ZEN-cls-2 {        fill: none;      }      .ppic-ZEN-cls-3 {        fill: #00799c;      }    ")),
createElement("g", {"id": "ppic-ZEN-Calque_2-2","data-name": "Calque 2"}, createElement("g", {}, createElement("rect", {"className": "ppic-ZEN-cls-2","width": "37.3","height": "37.3"}),
createElement("polygon", {"className": "ppic-ZEN-cls-1","points": "9.56 7.61 14.57 4.91 19.97 6.07 23.95 2.47 27.04 5.43 27.42 9.67 26.27 12.75 31.54 16.09 33.34 22.65 30.89 22.77 29.61 25.22 34.24 28.17 21 34.73 20.74 32.93 13.16 31.38 11.62 27.02 9.82 26.63 10.59 23.8 7.25 24.96 2.24 22.52 4.16 19.18 7.51 18.53 8.02 14.42 6.61 12.75 9.18 9.67 9.56 7.61"}),
createElement("path", {"className": "ppic-ZEN-cls-3","d": "M23.44,14.16l3.88-5.67-.28-3.06-1.82-1.74-1.27,2.65-3.89,5.68c-.05,.07-.1,.15-.14,.23l-2.45,4.89c-.1,.21-.17,.43-.2,.66l-.57,4.82-3.99,7.51,.44,1.25,3.09,.63,4.17-7.84c.12-.22,.19-.46,.22-.71l.57-4.85,2.23-4.46Z"})))),
"pic-WOL" : (preserveAspectRatio: PreserveAspectRatio) => createElement("svg", {"id": "ppic-WOL-Calque_2","data-name": "Calque 2","viewBox": "0 0 37.3 37.3", preserveAspectRatio }, createElement("defs", {}, createElement("style", {}, "      .ppic-WOL-cls-1 {        fill: #9fd1b6;      }      .ppic-WOL-cls-2 {        fill: none;      }      .ppic-WOL-cls-3 {        fill: #00799c;      }    ")),
createElement("g", {"id": "ppic-WOL-Calque_2-2","data-name": "Calque 2"}, createElement("g", {}, createElement("rect", {"className": "ppic-WOL-cls-2","width": "37.3","height": "37.3"}),
createElement("polygon", {"className": "ppic-WOL-cls-1","points": "9.56 7.61 14.57 4.91 19.97 6.07 23.95 2.47 27.04 5.43 27.42 9.67 26.27 12.75 31.54 16.09 33.34 22.65 30.89 22.77 29.61 25.22 34.24 28.17 21 34.73 20.74 32.93 13.16 31.38 11.62 27.02 9.82 26.63 10.59 23.8 7.25 24.96 2.24 22.52 4.16 19.18 7.51 18.53 8.02 14.42 6.61 12.75 9.18 9.67 9.56 7.61"}),
createElement("path", {"className": "ppic-WOL-cls-3","d": "M24,26.92s.04-.03,.07-.05l-.79,1.58c-.49,.99-.09,2.19,.89,2.68,.29,.14,.59,.21,.89,.21,.73,0,1.44-.4,1.79-1.11l1.22-2.45c.26-.51,.28-1.11,.07-1.64l-1.55-3.87,.19-.45,2.57-3.14c.12-.14,.21-.3,.29-.48l1.12-2.6-3.42-2.17-1.26,2.94-2.57,3.14c-.12,.14-.21,.3-.29,.48l-1.51,3.53-2.92,1.22c-.47,.2-.85,.57-1.06,1.03l-1.22,2.75c-.45,1.01,0,2.19,1.02,2.64,.26,.12,.54,.17,.81,.17,.77,0,1.5-.44,1.83-1.19l.9-2.02,2.93-1.22Z"})))),
"pic-surface-other" : (preserveAspectRatio: PreserveAspectRatio) => createElement("svg", {"id": "ppic-surface-other-Calque_2","data-name": "Calque 2","viewBox": "0 0 37.3 37.3", preserveAspectRatio }, createElement("defs", {}, createElement("style", {}, "      .ppic-surface-other-cls-1 {        fill: #9fd1b6;      }      .ppic-surface-other-cls-2 {        stroke: #00799c;        stroke-linecap: round;        stroke-linejoin: round;        stroke-width: 4px;      }      .ppic-surface-other-cls-2, .ppic-surface-other-cls-3 {        fill: none;      }    ")),
createElement("g", {"id": "ppic-surface-other-Calque_2-2","data-name": "Calque 2"}, createElement("g", {}, createElement("rect", {"className": "ppic-surface-other-cls-3","width": "37.3","height": "37.3"}),
createElement("polygon", {"className": "ppic-surface-other-cls-1","points": "9.56 7.61 14.57 4.91 19.97 6.07 23.95 2.47 27.04 5.43 27.42 9.67 26.27 12.75 31.54 16.09 33.34 22.65 30.89 22.77 29.61 25.22 34.24 28.17 21 34.73 20.74 32.93 13.16 31.38 11.62 27.02 9.82 26.63 10.59 23.8 7.25 24.96 2.24 22.52 4.16 19.18 7.51 18.53 8.02 14.42 6.61 12.75 9.18 9.67 9.56 7.61"}),
createElement("line", {"className": "ppic-surface-other-cls-2","x1": "23.85","y1": "28.43","x2": "23.85","y2": "28.43"}),
createElement("line", {"className": "ppic-surface-other-cls-2","x1": "23.85","y1": "23.54","x2": "23.85","y2": "23.54"}),
createElement("line", {"className": "ppic-surface-other-cls-2","x1": "27.82","y1": "20.48","x2": "27.82","y2": "20.48"}),
createElement("line", {"className": "ppic-surface-other-cls-2","x1": "26.9","y1": "15.59","x2": "26.9","y2": "15.59"}),
createElement("line", {"className": "ppic-surface-other-cls-2","x1": "18.04","y1": "20.79","x2": "18.04","y2": "20.79"}),
createElement("line", {"className": "ppic-surface-other-cls-2","x1": "10.09","y1": "22.93","x2": "10.09","y2": "22.93"})))),
"pic-BEBR_Landenien_Landeniaan_3" : (preserveAspectRatio: PreserveAspectRatio) => createElement("svg", {"version": "1.1","x": "0px","y": "0px","viewBox": "0 0 37.3 37.3",id: 'Calque_2_00000001649933741733744930000011916722315923552425_',"space": "preserve", preserveAspectRatio }, createElement("style", {"type": "text/css"}, "	.ppic-BEBR_Landenien_Landeniaan_3-st0{fill:none;}	.ppic-BEBR_Landenien_Landeniaan_3-st1{fill:#00799C;}#Calque_2_00000001649933741733744930000011916722315923552425_{ enable-background:new 0 0 37.3 37.3; }"),
createElement("rect", {"y": "0","className": "ppic-BEBR_Landenien_Landeniaan_3-st0","width": "37.3","height": "37.3"}),
createElement("polygon", {"className": "ppic-BEBR_Landenien_Landeniaan_3-st1","points": "9.6,7.6 14.6,4.9 20,6.1 24,2.5 27,5.4 27.4,9.7 26.3,12.8 31.5,16.1 33.3,22.6 30.9,22.8 29.6,25.2   34.2,28.2 21,34.7 20.7,32.9 13.2,31.4 11.6,27 9.8,26.6 10.6,23.8 7.2,25 2.2,22.5 4.2,19.2 7.5,18.5 8,14.4 6.6,12.8 9.2,9.7 "})),
"pic-BEBR_Ypresien_Ieperiaan_4" : (preserveAspectRatio: PreserveAspectRatio) => createElement("svg", {"version": "1.1","x": "0px","y": "0px","viewBox": "0 0 37.3 37.3",id: 'Calque_2_00000177464846276901142100000005593369367097592223_',"space": "preserve", preserveAspectRatio }, createElement("style", {"type": "text/css"}, "	.ppic-BEBR_Ypresien_Ieperiaan_4-st0{fill:none;}	.ppic-BEBR_Ypresien_Ieperiaan_4-st1{fill:#9FD1B6;}	.ppic-BEBR_Ypresien_Ieperiaan_4-st2{fill:#00799C;}#Calque_2_00000177464846276901142100000005593369367097592223_{ enable-background:new 0 0 37.3 37.3; }"),
createElement("rect", {"y": "0","className": "ppic-BEBR_Ypresien_Ieperiaan_4-st0","width": "37.3","height": "37.3"}),
createElement("polygon", {"className": "ppic-BEBR_Ypresien_Ieperiaan_4-st1","points": "9.6,7.6 14.6,4.9 20,6.1 24,2.5 27,5.4 27.4,9.7 26.3,12.8 31.5,16.1 33.3,22.6 30.9,22.8 29.6,25.2   34.2,28.2 21,34.7 20.7,32.9 13.2,31.4 11.6,27 9.8,26.6 10.6,23.8 7.2,25 2.2,22.5 4.2,19.2 7.5,18.5 8,14.4 6.6,12.8 9.2,9.7 "}),
createElement("g", {}, createElement("polygon", {"className": "ppic-BEBR_Ypresien_Ieperiaan_4-st2","points": "23.3,6.6 25.3,3.8 24,2.5 20,6.1 14.6,4.9 9.6,7.6 9.2,9.7 6.6,12.8 8,14.4 7.6,17.5 10.2,16.9    8.9,15.7 11.3,14.5 9.9,13 12.1,12 13.2,9.9 10.4,9.4 14.8,7.9 18.8,8.4 20.9,6.9  "}),
createElement("polygon", {"className": "ppic-BEBR_Ypresien_Ieperiaan_4-st2","points": "3.9,19.6 5.2,20.4 5.2,19 4.2,19.2  "}))),
"pic-KAN" : (preserveAspectRatio: PreserveAspectRatio) => createElement("svg", {"id": "ppic-KAN-Calque_2","data-name": "Calque 2","viewBox": "0 0 37.3 37.3", preserveAspectRatio }, createElement("defs", {}, createElement("style", {}, "      .ppic-KAN-cls-1 {        fill: #9fd1b6;      }      .ppic-KAN-cls-2 {        fill: none;      }      .ppic-KAN-cls-3 {        fill: #00799c;      }    ")),
createElement("g", {"id": "ppic-KAN-Calque_2-2","data-name": "Calque 2"}, createElement("g", {}, createElement("rect", {"className": "ppic-KAN-cls-2","width": "37.3","height": "37.3"}),
createElement("polygon", {"className": "ppic-KAN-cls-1","points": "9.56 7.61 14.57 4.91 19.97 6.07 23.95 2.47 27.04 5.43 27.42 9.67 26.27 12.75 31.54 16.09 33.34 22.65 30.89 22.77 29.61 25.22 34.24 28.17 21 34.73 20.74 32.93 13.16 31.38 11.62 27.02 9.82 26.63 10.59 23.8 7.25 24.96 2.24 22.52 4.16 19.18 7.51 18.53 8.02 14.42 6.61 12.75 9.18 9.67 9.56 7.61"}),
createElement("path", {"className": "ppic-KAN-cls-3","d": "M25.04,6.98l1.14-2.38-2.23-2.13-2.1,1.89-.3,.63-4.71,5.29c-.16,.18-.29,.4-.38,.62l-2.45,6.54-1.36,.58c-.32,.14-.6,.36-.81,.64l-1.83,2.45c-.14,.19-.25,.41-.32,.63l-.77,2.62,1.66-.57-.77,2.83,1.8,.39,.38,1.08s0-.01,0-.02l1.43-4.85,1.3-1.73,1.65-.71c.5-.21,.89-.62,1.09-1.13l2.62-6.99,4.64-5.23c.12-.14,.23-.29,.31-.46Z"})))),

};

type Picto = typeof pictos;
type PictoKey = keyof Picto;

const isPictoKey = (name: string): name is PictoKey => name in pictos;

export const keyFromString = (
    name: string,
    prefix = 'pic-'
): Option<PictoKey> => {
    const key = `${prefix}${name}`;
    if (isPictoKey(key)) {
        return some(key);
    }
    return none;
};

export const renderPicto = (key: PictoKey, preserveAspectRatio = "xMidYMid" as PreserveAspectRatio) => DIV({className: `picto-infiltration ${key}`, key}, pictos[key](preserveAspectRatio))

export default renderPicto;
