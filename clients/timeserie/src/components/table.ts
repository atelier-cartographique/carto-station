import { DIV } from 'sdi/components/elements';
import { tableTimeserieQuery, getSource } from 'timeserie/src/queries/table';
import { tableTimeserieDispatch } from 'timeserie/src/events/table';
import { table } from 'sdi/components/table2';
import tr from 'sdi/locale';
import { noop } from 'sdi/util';
// import { renderDisplaySwitch } from './button';

// const renderDisplaySwitch = () => {
//     const tableMode = getTableMode();

//     switch (tableMode) {
//         case 'per-station':
//             return [stationLabel(), btnPerParam()];
//         case 'per-parameter':
//             return [paramLabel(), btnPerStation()];
//     }
// };

const rowHandler = noop;

const toolbar = () => DIV('table-toolbar', tr.ts('tableHelptext'));

const renderTable = table(
    tableTimeserieDispatch,
    tableTimeserieQuery,
    rowHandler
);

const render = () => DIV('content__body', renderTable(getSource(), toolbar()));

export default render;
