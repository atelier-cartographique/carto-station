import { ReactSVGElement } from 'react';
import * as debug from 'debug';
// import { some, none } from 'fp-ts/lib/Option';
import {
    getColor,
    getSelectedParameterList,
    getHighligthTimeserieData,
    getParamUnit,
    getHighlightedElements,
    getSelectedNorms,
    getDisplayMode,
    getSelectedParameter,
} from '../../queries/timeserie';
import { fromNullable } from 'fp-ts/lib/Option';
import {
    graphsize,
    innerPadding,
    LABELFONTSIZE,
    LinechartInfo,
    PlotData,
    PlotDataElement,
    YAXISDISTANCE,
} from './types';
import { group, line, text } from './svg';
import { notEmpty } from 'sdi/util';
import { getXWidth } from '.';
const logger = debug('sdi:timeserie/yaxis');

export const getMin = (values: number[]) =>
    notEmpty(values)
        .map(v => v.reduce((acc, val) => (val < acc ? val : acc)))
        .getOrElse(Number.MIN_VALUE);

export const getMax = (values: number[]) =>
    notEmpty(values)
        .map(v => v.reduce((acc, val) => (val > acc ? val : acc)))
        .getOrElse(Number.MAX_VALUE);

const FORMAT_AXIS = [
    [0.00001, 1, 1],
    [0.1, 0.005, 3],
    [1, 0.05, 2],
    [2, 0.1, 1],
    [20, 1, 0],
    [200, 10, 0],
    [2000, 100, 0],
    [20000, 1000, 0],
    [50000, 5000, 0],
    [100000, 10000, 0],
];

type FormatAndDivide = [number, (n: number) => string];

export const subdivision = (range: number) =>
    fromNullable(FORMAT_AXIS.find(([r]) => range < r))
        .map<FormatAndDivide>(([_, divider, toFixedArg]) => [
            divider,
            (n: number) => n.toFixed(toFixedArg),
        ])
        .getOrElse([1, (n: number) => n.toFixed(2)]);

/**
 * Height (y value) in pixels
 * @param value value in original unit
 * @param yUnit number of pixels by original unit
 * @param min minimal y value in original unit
 */
//
export const getYInPixel = (value: number, yUnit: number, min: number) => {
    return graphsize.height - ((value - min) * yUnit + innerPadding.bottom);
};

/**
 * Make an y axis for a multiple-y-axis linechart
 * @param dataElem data linked to this axis
 * @param axisIndex index of the (visible) axis
 * @param paramIndex index of the parameter (visible or not on the graph), to get the color and the unit
 * @param dataLength number of visible axis (= number of data elements with a length > 0)
 */
const axisY = (
    dataElem: PlotDataElement,
    axisIndex: number,
    paramIndex: number,
    data: PlotData
) => {
    const dataLength = data.filter(d => d.ts.length > 0).length;

    const color = getColor(paramIndex);
    const classSelected = getHighlightedElements()
        .map(ps => (ps.id === dataElem.identifier ? 'selected' : 'unselected'))
        .getOrElse('selected');

    const filteredNorms = getSelectedNorms(dataElem.norms);

    const values = dataElem.ts
        .map(point => point.value * point.factor)
        .concat(filteredNorms.map(n => n.value));
    const min = getMin(values);
    const max = getMax(values);
    const x = (1 - dataLength + axisIndex) * YAXISDISTANCE;

    const [divider, formatLabel] = subdivision(max - min);
    const minFloor = Math.floor(min / divider) * divider;
    const maxCeil = Math.ceil(max / divider) * divider;
    const range = maxCeil - minFloor > 0 ? maxCeil - minFloor : 1;
    const toPixelFactor =
        (graphsize.height - innerPadding.bottom - innerPadding.top) / range;

    const yHeight = graphsize.height;
    const paramUnit =
        getDisplayMode() === 'per-parameter'
            ? getSelectedParameter().chain(getParamUnit)
            : getParamUnit(getSelectedParameterList()[paramIndex]);
    const axis = [
        line(x, 0, x, yHeight, { stroke: color }),
        line(x - 3, 5, x, 0, { stroke: color }),
        line(x + 3, 5, x, 0, { stroke: color }),
        text(x, -5, `${paramUnit.getOrElse('')}`, 'middle', {
            // transform: `scale(1,-1), translate(0,-${yHeight * 2 + 4})`,
            fontSize: LABELFONTSIZE,
            fill: color,
        }),
    ];

    const highlighted = getHighligthTimeserieData().fold(false, ts =>
        ts.identifier === dataElem.identifier ? true : false
    );
    const nbOfDashes = Math.ceil(range / divider);
    const xWidth = getXWidth(data, false);
    for (let i = 0; i <= nbOfDashes; i++) {
        const y = minFloor + divider * i;
        const dist = getYInPixel(y, toPixelFactor, minFloor);
        if (dist > 0 && dist < yHeight) {
            if (highlighted || dataLength === 1) {
                axis.push(
                    line(x, dist, 0, dist, {
                        stroke: '#d9d9d9',
                        strokeDasharray: '2 4',
                    })
                );
                axis.push(line(0, dist, xWidth, dist, { stroke: '#d9d9d9' })); // GRID
            }
            axis.push(line(x, dist, x - 2, dist, { stroke: color }));
            axis.push(
                text(x - 5, dist + 2, formatLabel(y), 'end', {
                    // transform: `translate(0,-${dist * 2})`,
                    fontSize: LABELFONTSIZE,
                })
            );
        }
    }
    return group(axis, { className: `axis-${classSelected}` });
};

/**
 * Make an y axis for a simple-y-axis linechart
 * @param dataElem data linked to this axis
 * @param axisIndex index of the (visible) axis
 * @param paramIndex index of the parameter (visible or not on the graph), to get the color and the unit
 * @param dataLength number of visible axis (= number of data elements with a length > 0)
 */
export const simpleAxisY = (info: LinechartInfo, data: PlotData) => {
    const color = 'black';
    const max = info.yBiggestMax;
    const min = info.ySmallestMin;
    const [divider, formatLabel] = subdivision(max - min);
    const minFloor = Math.floor(min / divider) * divider;
    const maxCeil = Math.ceil(max / divider) * divider;
    const range = maxCeil - minFloor > 0 ? maxCeil - minFloor : 1;
    const toPixelFactor =
        (graphsize.height - innerPadding.bottom - innerPadding.top) / range;
    const x = 0;
    const yHeight = graphsize.height;
    const paramUnit = getSelectedParameter().chain(getParamUnit).getOrElse('');
    const axis = [
        line(x, 0, x, yHeight, { stroke: color }),
        line(x - 3, 5, x, 0, { stroke: color }),
        line(x + 3, 5, x, 0, { stroke: color }),
        text(x, -5, paramUnit, 'middle', {
            // transform: `scale(1,-1), translate(0,-${yHeight * 2 + 4})`,
            fontSize: LABELFONTSIZE,
            fill: color,
        }),
    ];

    const nbOfDashes = Math.ceil(range / divider);
    for (let i = 0; i <= nbOfDashes; i++) {
        const y = minFloor + divider * i;
        const dist = getYInPixel(y, toPixelFactor, minFloor);
        if (dist > 0 && dist < yHeight) {
            //GRID
            axis.push(
                line(0, dist, getXWidth(data, true), dist, {
                    stroke: '#d9d9d9',
                })
            );

            axis.push(line(x, dist, x - 2, dist, { stroke: color }));
            axis.push(
                text(x - 5, dist + 2, formatLabel(y), 'end', {
                    // transform: `translate(0,-${dist * 2})`,
                    fontSize: LABELFONTSIZE,
                })
            );
            // dist += divider * valToPixel;
        }
    }
    return group(axis, { className: `axis-selected` });
};

export const multiplesYAxis = (data: PlotData) => {
    let axisIndex = 0;
    let axis: ReactSVGElement[] = [];
    data.map((d, index) => {
        if (d.ts.length > 0) {
            axis = axis.concat([axisY(d, axisIndex, index, data)]);
            axisIndex += 1;
        }
    });
    return axis;
};

logger('loaded');
