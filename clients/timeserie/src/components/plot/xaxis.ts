import { fmtDateNb } from 'sdi/util';
import tr from 'sdi/locale';

import { getSelectedKind } from '../../queries/timeserie';
import {
    graphsize,
    innerPadding,
    LABELFONTSIZE,
    PlotData,
    PlotInterval,
} from './types';
import { line, text } from './svg';
import { getXWidth } from '.';

const MIN_TIMESTAMP = 0;
const MAX_TIMESTAMP = new Date().getTime();

const getFirst = (dates: number[]) =>
    dates.length === 0
        ? MIN_TIMESTAMP
        : dates.reduce((acc, val) => (val < acc ? val : acc), MAX_TIMESTAMP);

const getLast = (dates: number[]) =>
    dates.length === 0
        ? MAX_TIMESTAMP
        : dates.reduce((acc, val) => (val > acc ? val : acc), MIN_TIMESTAMP);

export const minX = (data: PlotData) => {
    const minimums = data
        .map(d => d.ts)
        .filter(ts => ts.length > 0)
        .map(ts => ts[0].date);
    return getFirst(minimums);
};
export const maxX = (data: PlotData) => {
    const maximums = data
        .map(d => d.ts)
        .filter(ts => ts.length > 0)
        .map(ts => ts[ts.length - 1].date);
    return getLast(maximums);
};

export const beginOfInterval = (date: number, interval: PlotInterval) => {
    const begin = new Date(date);
    switch (interval) {
        case 'Year':
            begin.setFullYear(begin.getFullYear(), 0, 1);
            break;
        case 'Month':
            begin.setMonth(begin.getMonth(), 1);
            break;
        case 'Day':
            begin.setHours(0);
            break;
        case 'Hour':
            begin.setMinutes(0);
            break;
    }
    return begin.getTime();
};

export const xMillisecondRange = (data: PlotData, interval: PlotInterval) => {
    const min = beginOfInterval(minX(data), interval);
    const max = maxX(data);
    if (max != min) {
        return max - min;
    } else {
        return 1;
    }
};

/**
 * The duration of the given interval in milliseconds
 * @param interval PlotInterval
 */
export const getIntervalTime = (interval: PlotInterval): number => {
    switch (interval) {
        case 'Hour':
            return 1000 * 60 * 60;
        case 'Day':
            return 1000 * 60 * 60 * 24;
        case 'Month':
            // TODO!!!
            return 1000 * 60 * 60 * 24 * 30;
        case 'Year':
            // TODO !!!
            return 1000 * 60 * 60 * 24 * 365;
    }
};

/**
 * Unit (in pixels) for one millisecond, for a given time interval
 * @param unit x-unit in pixels
 * @param interval PlotInterval
 */
export const toMillisecondUnit = (
    unit: number,
    interval: PlotInterval
): number => {
    switch (interval) {
        case 'Hour':
            return unit / getIntervalTime('Hour');
        case 'Day':
            return unit / getIntervalTime('Day');
        case 'Month':
            return unit / getIntervalTime('Month');
        case 'Year':
            return unit / getIntervalTime('Year');
    }
};

const incrementDate = (ts: number, interval: PlotInterval): number => {
    const date = new Date(ts);
    switch (interval) {
        case 'Year':
            date.setFullYear(date.getFullYear() + 1, 0, 1);
            break;
        case 'Month':
            date.setMonth(date.getMonth() + 1, 1);
            break;
        // logger(`date: ${date}, month: ${date.getMonth()}`);
        case 'Day':
            date.setDate(date.getDate() + 1);
            break;
        case 'Hour':
            date.setHours(date.getHours() + 1);
            break;
    }
    return date.getTime();
};

const dateLabel = (date: Date, interval: PlotInterval): string => {
    switch (interval) {
        case 'Hour':
            return `${date.getHours()}`;
        case 'Day':
            return `${fmtDateNb(date.getDate())}/${fmtDateNb(
                date.getMonth() + 1
            )}`;
        case 'Month': {
            const year = `${date.getFullYear()}`.slice(-2);
            return `${fmtDateNb(date.getMonth() + 1)}/${year}`;
        }
        case 'Year':
            return `${date.getFullYear()}`;
    }
};

/**
 * X value in pixels from value in milliseconds
 * @param value value in milliseconds
 * @param xUnit number of pixels by millisecond
 * @param min minimal y value in milliseconds
 */
export const getXInPixel = (value: number, xUnit: number, min: number) => {
    return (value - min) * xUnit + innerPadding.left;
};

export const axisX = (
    width: number,
    data: PlotData,
    interval: PlotInterval,
    simpleAxis: boolean
) => {
    const type = getSelectedKind().getOrElse('ground-quantity');
    const color = '#000000';
    const yHeight = graphsize.height;
    const range = getIntervalTime(interval);
    const nbUnit = Math.ceil(xMillisecondRange(data, interval) / range);
    const unit = (width - innerPadding.left - innerPadding.right) / nbUnit;
    const unitMilli = toMillisecondUnit(unit, interval);
    const minDateMilli = beginOfInterval(minX(data), interval);
    // const offSetX = (minDateMilli - Math.floor(minDateMilli)) * unit;
    // const realDataLength = data.filter(d => d.ts.length > 0).length;
    const xWidth = getXWidth(data, simpleAxis);

    const axis = [
        line(0, yHeight, xWidth, yHeight, { stroke: color }),
        line(xWidth - 5, yHeight + 3, xWidth, yHeight, { stroke: color }),
        line(xWidth - 5, yHeight - 3, xWidth, yHeight, { stroke: color }),
        text(xWidth, yHeight + 10, `${tr.ts(interval)}`, 'start', {
            // transform: `scale(1,-1), translate(0,20)`,
            fontSize: LABELFONTSIZE,
        }),
    ];
    if (interval === 'Hour') {
        const labelDate = new Date(minX(data));
        axis.push(
            text(
                xWidth,
                yHeight + 17,
                `${fmtDateNb(labelDate.getDate())}/${fmtDateNb(
                    labelDate.getMonth() + 1
                )}/${labelDate.getFullYear()}`,
                'middle',
                {
                    // transform: `scale(1,-1), translate(0,20)`,
                    fontSize: LABELFONTSIZE,
                }
            )
        );
    }

    let ts = beginOfInterval(minX(data), interval);
    let old_dist = 0;

    for (let i = 0; i <= nbUnit; i++) {
        // if (type === 'ground-quantity') {
        const dist = getXInPixel(ts, unitMilli, minDateMilli);
        axis.push(
            line(dist, yHeight, dist, 0, {
                stroke: '#d9d9d9',
            })
        );
        if (type == 'ground-quantity' && interval == 'Year' && i > 0) {
            axis.push(
                line(
                    dist - (dist - old_dist) / 2,
                    yHeight,
                    dist - (dist - old_dist) / 2,
                    0,
                    {
                        stroke: '#d9d9d9',
                        strokeDasharray: '4 2',
                    }
                )
            );
        }

        axis.push(line(dist, yHeight, dist, yHeight + 2, { stroke: color }));

        // if to many labels, just display one of the two and place one of the four lower.
        let disp = 'ok';
        let translation = 0;
        if (nbUnit > 25 && i % 4 != 0) {
            translation = 5;
        }
        if (nbUnit > 15 && i % 2 != 0) {
            disp = 'none';
        }
        axis.push(
            text(
                dist,
                yHeight + 11,
                dateLabel(new Date(ts), interval),
                'middle',
                {
                    transform: `translate(0,${translation})`,
                    fontSize: LABELFONTSIZE,
                    display: disp,
                }
            )
        );
        ts = incrementDate(ts, interval);
        old_dist = dist;
    }
    return axis;
};
