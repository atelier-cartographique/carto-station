export interface IDimensions {
    width: number;
    height: number;
}
export const graphsize: IDimensions = { width: 600, height: 200 };

export interface IPadding {
    top: number;
    right: number;
    bottom: number;
    left: number;
}

export const innerPadding: IPadding = {
    top: 10,
    right: 20,
    bottom: 10,
    left: 20,
};

export const padding: IPadding = { top: 25, right: 10, bottom: 25, left: 10 };

export interface PlotPoint {
    date: number;
    value: number;
    factor: number;
}

export interface PlotNorm {
    name: string;
    value: number;
}

export interface PlotDataElement {
    identifier: number;
    ts: PlotPoint[];
    norms: PlotNorm[];
    clusterized: boolean;
}

export type PlotData = PlotDataElement[];

export type PlotInterval = 'Hour' | 'Day' | 'Month' | 'Year';

export const YAXISDISTANCE = 27;
export const LABELFONTSIZE = '0.5em';
export const MAXLENGTH = 800;
export const MAXNBDASHES = 30;

export interface LinechartInfo {
    // timeserie: PlotPoint[];
    // id: number;
    // color: string;
    xUnit: number;
    xMin: number;
    yBiggestUnit: number;
    ySmallestMin: number;
    yBiggestMax: number;
    multiAxis: boolean;
}

export interface DataElementInfo {
    timeserie: PlotPoint[];
    id: number;
    color: string;
    yUnit: number;
    yMin: number;
}
