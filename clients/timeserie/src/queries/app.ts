import { query } from 'sdi/shape';
import { getUserData } from 'sdi/app';

export const getLayout = () => query('app/layout');

export const getUsername = () => getUserData().map(u => u.name);
