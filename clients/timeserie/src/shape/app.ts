import { Layout } from '../events/route';

// State Augmentation
declare module 'sdi/shape' {
    export interface IShape {
        'app/layout': Layout;
    }
}

export const defaultAppShape = () => ({
    'app/layout': 'splash' as Layout,
});
