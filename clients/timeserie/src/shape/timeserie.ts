import {
    ParameterList,
    TimeserieData,
    GroupData,
    NormNames,
    WaterBodyList,
    WaterBodySelection,
    QuantParam,
    QuantStation,
} from '../remote/timeserie';
import {
    ParameterSelection,
    StationSelection,
    TableMode,
    PointSelection,
    NormSelection,
    DateSelection,
    VisibleGroundLayer,
} from '../types';
import { Collection, Nullable } from 'sdi/util';
import { RemoteResource, FeatureCollection } from 'sdi/source';
import { Level, Mode } from '../events/route';

declare module 'sdi/shape' {
    export interface IShape {
        'data/parameters/surface': ParameterList;
        'data/parameters/ground/quality': ParameterList;
        'data/parameters/ground/quantity': ParameterList;
        'data/timeseries': Collection<RemoteResource<TimeserieData>>;
        'data/norms': NormNames;
        'timeserie/parameter/select': ParameterSelection[];
        'timeserie/parameter/select/highlighted': Nullable<ParameterSelection>; //highlighted on plot
        'timeserie/parameter/select/index': Nullable<number>; //selected to filter stations or plot/table by param
        'timeserie/parameter/select/info': Nullable<ParameterSelection>; //display info of parameter
        'timeserie/parameter/search': Nullable<string>;
        'timeserie/station/search': Nullable<string>;
        'timeserie/station/select': StationSelection[];
        'timeserie/station/select/index': number; // selected to filter params or plot/table by stations
        'timeserie/station/select/highlighted': Nullable<StationSelection>; // highlighted on plot
        'timeserie/station/select/info': Nullable<StationSelection>; //display info of parameter
        'timeserie/dates/start': Nullable<DateSelection>;
        'timeserie/dates/end': Nullable<DateSelection>;
        // 'timeserie/dates/window': Nullable<[Date, Date]>;
        'timeserie/point/select': Nullable<PointSelection>;
        'timeserie/norm/select': NormSelection[];
        'data/layer/surface': Nullable<FeatureCollection>;
        'data/layer/ground/quality': Nullable<FeatureCollection>;
        'data/layer/ground/quantity': Nullable<FeatureCollection>;
        'data/waterbodies': WaterBodyList;
        'timeserie/display/mode': TableMode;
        'timeserie/group/select': Nullable<GroupData>;
        'timeserie/level': Nullable<Level>;
        'timeserie/mode': Mode;
        'timeserie/visible/ground-layer': VisibleGroundLayer[];
        'timeserie/station/search/input': string;
        'timeserie/waterbody/select': Nullable<WaterBodySelection>;
        'timeserie/parameter/count': Collection<QuantParam>;
        'timeserie/station/count': Collection<QuantStation>;
    }
}

export const defaultTimeserieShape = () => ({
    'data/parameters/surface': [],
    'data/parameters/ground/quality': [],
    'data/parameters/ground/quantity': [],
    'data/timeseries': {},
    'data/norms': {},
    'data/waterbodies': [],
    'timeserie/parameter/select': [],
    'timeserie/parameter/select/highlighted': null,
    'timeserie/parameter/select/index': null,
    'timeserie/parameter/select/info': null,
    'timeserie/parameter/search': null,
    'timeserie/station/search': null,
    'timeserie/point/select': null,
    'timeserie/station/select': [],
    'timeserie/station/select/index': 0,
    'timeserie/station/select/highlighted': null,
    'timeserie/station/select/info': null,
    'timeserie/dates/start': null,
    'timeserie/dates/end': null,
    'timeserie/dates/window': null,
    'timeserie/norm/select': [],
    'data/layer/surface': null,
    'data/layer/ground/quality': null,
    'data/layer/ground/quantity': null,
    'timeserie/display/mode': 'per-station' as TableMode,
    'timeserie/group/select': null,
    'timeserie/level': null,
    'timeserie/mode': 'station' as Mode,
    'timeserie/visible/ground-layer': [
        'quality',
        'quantity',
    ] as VisibleGroundLayer[],
    'timeserie/station/search/input': '',
    'timeserie/waterbody/select': null,
    'timeserie/parameter/count': {},
    'timeserie/station/count': {},
});
