import { IUgWsResponse } from 'sdi/ports/geocoder';
import { Nullable } from 'sdi/util';

declare module 'sdi/shape' {
    export interface IShape {
        'port/geocoder/input': string;
        'port/geocoder/response': Nullable<IUgWsResponse>;
    }
}

export const defaultGeocoderState = () => ({
    'port/geocoder/input': '',
    'port/geocoder/response': null,
});
