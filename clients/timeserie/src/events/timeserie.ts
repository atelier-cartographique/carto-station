import * as debug from 'debug';
import { some, none, Option } from 'fp-ts/lib/Option';
import { scopeOption } from 'sdi/lib';
import { catOptions, index } from 'fp-ts/lib/Array';

import {
    assign,
    dispatchK,
    dispatch,
    query,
    assignK,
    dispatchAsync,
} from 'sdi/shape';
import { FeaturePath, getFeaturePath } from 'sdi/map';
import { remoteSuccess, remoteLoading, remoteError, Feature } from 'sdi/source';
import {
    updateCollection,
    isNotNullNorUndefined,
    tryNumber,
    noop,
} from 'sdi/util';

import {
    fetchParameterListSurface,
    fetchParameterListgroundQuality,
    fetchParameterListgroundQuantity,
    fetchTimeserieSurface,
    fetchTimeserieGroundQuality,
    fetchTimeserieGroundQuantity,
    GroupData,
    RemoteTimeserieData,
    TimeserieData,
    tsTime,
    fetchNormNames,
    fetchWaterBodies,
    WaterBody,
    fetchQuantParamSurface,
    fetchQuantParamGround,
    fetchQuantStationSurface,
    fetchQuantStationGroundQual,
} from '../remote/timeserie';

import { ParameterSelection, ParameterType, StationSelection } from '../types';
import { PlotNorm } from '../components/plot/types';
import {
    getSelectedKind,
    getSelectedParameter,
    getWindowStart,
    getWindowEnd,
    getLevel,
    getWaterbodyFeatures,
    getMode,
    timeserieKey,
    getSelectedParameterList,
    isHighlightedParam,
    isHighlightedStation,
    hasStationInfo,
    hasParamInfo,
    getSelectedStation,
    getSelectedStationList,
    getDisplayMode,
} from '../queries/timeserie';
import {
    Level,
    configAddWindow,
    navigateInPlace,
    Mode,
    configAddStations,
} from './route';
import { updateLayers } from './map';

const logger = debug('sdi:events/timeserie');

export const setParameterSearch = (name: string) =>
    assign('timeserie/parameter/search', name);
export const setStationSearch = (name: string) =>
    assign('timeserie/station/search', name);

export const loadAllParameters = () =>
    Promise.all([
        fetchParameterListSurface(),
        fetchParameterListgroundQuality(),
        fetchParameterListgroundQuantity(),
    ])
        .then(([surf, groudQual, groundQuant]) => {
            assign('data/parameters/surface', surf);
            assign('data/parameters/ground/quality', groudQual);
            assign('data/parameters/ground/quantity', groundQuant);
        })
        .catch(err => logger(`[ERROR] loadAllParameters: ${err}`));

export const loadNormNames = () =>
    Promise.all([fetchNormNames()])
        .then(([norms]) => assign('data/norms', norms))
        .catch(err => logger(`[ERROR] loadNormNames: ${err}`));

const { putInFlight, getInflight, clearInflight } = (() => {
    let inFlightTimeseries: string[] = [];

    const putInFlight = (key: string) => inFlightTimeseries.push(key);

    const getInflight = (key: string) => inFlightTimeseries.indexOf(key) >= 0;

    const clearInflight = (key: string) =>
        (inFlightTimeseries = inFlightTimeseries.filter(k => k !== key));

    return { putInFlight, getInflight, clearInflight };
})();

const dispatchTimeserie = dispatchK('data/timeseries');

/**
 * This function is called right after loading a new timeserie
 * @param ts
 */
const updateWindow = (ts: TimeserieData) => {
    if (ts.length === 0) {
        logger(`updateWindow on empty timeserie`);
        return;
    }
    const optStart = getWindowStart();
    const optEnd = getWindowEnd();
    const startIsAuto = optStart
        .map(({ isAutomatic }) => isAutomatic)
        .getOrElse(true);
    const endIsAuto = optEnd
        .map(({ isAutomatic }) => isAutomatic)
        .getOrElse(true);
    const firstRow = ts[0];
    const lastRow = ts[ts.length - 1];
    if (!startIsAuto && !endIsAuto) {
        return;
    } else if (!startIsAuto && endIsAuto) {
        const tsEnd = tsTime(lastRow);
        scopeOption()
            .let('start', optStart)
            .let('config', ({ start }) =>
                configAddWindow(start.date.getTime(), false, tsEnd, true)
            )
            .let('level', getLevel())
            .map(({ level, config }) =>
                navigateInPlace(level, config, getMode())
            );
    } else if (startIsAuto && !endIsAuto) {
        const tsStart = tsTime(firstRow);
        scopeOption()
            .let('end', optEnd)
            .let('config', ({ end }) =>
                configAddWindow(tsStart, true, end.date.getTime(), false)
            )
            .let('level', getLevel())
            .map(({ level, config }) =>
                navigateInPlace(level, config, getMode())
            );
    } else if (startIsAuto && endIsAuto) {
        const tsStart = tsTime(firstRow);
        const tsEnd = tsTime(lastRow);
        scopeOption()
            .let('config', configAddWindow(tsStart, true, tsEnd, true))
            .let('level', getLevel())

            .map(({ level, config }) =>
                navigateInPlace(level, config, getMode())
            );
    }
};

export const loadTimeserie = (
    t: ParameterType,
    station: number | string,
    parameter: number
) => {
    const key = timeserieKey(t, station, parameter);
    if (!getInflight(key)) {
        putInFlight(key);
        dispatchTimeserie(ts => updateCollection(ts, key, remoteLoading));
        const cons = (data: RemoteTimeserieData) => {
            const tsData: TimeserieData = data.map(([d, v, q]) => [
                Date.parse(d),
                v,
                q,
            ]);
            dispatchTimeserie(ts =>
                updateCollection(ts, key, remoteSuccess(tsData))
            );
            updateWindow(tsData);
        };
        const fetcher = (() => {
            switch (t) {
                case 'surface':
                    return fetchTimeserieSurface;
                case 'ground-quality':
                    return fetchTimeserieGroundQuality;
                case 'ground-quantity':
                    return fetchTimeserieGroundQuantity;
            }
        })();

        fetcher(station, parameter)
            .then(cons)
            .catch(err =>
                dispatchTimeserie(ts =>
                    updateCollection(
                        ts,
                        key,
                        remoteError(`Error fetching timeserie: ${err}`)
                    )
                )
            )
            .then(() => clearInflight(key));
    }
};

export const selectParameterIndex = (kind: ParameterType, id: number) => {
    setSelectedParameterIndex(
        getSelectedParameterList().findIndex(
            p => p.id === id && p.kind === kind
        )
    );
    getLevel().map(updateLayers);
};

export const selectParameter = (kind: ParameterType, id: number) => {
    dispatch('timeserie/parameter/select', ss =>
        ss.filter(s => s.id !== id || s.kind !== kind).concat({ kind, id })
    );
    selectParameterIndex(kind, id);
    updateTimeserie();
};

export const highlightAndSelectParam = (kind: ParameterType, id: number) => {
    const idInParamList = getSelectedParameterList().findIndex(
        p => p.id === id
    );
    setSelectedParameterIndex(idInParamList);
    switchHighligthParameter({ id, kind });

    // getSelectedParameterList().map(p => {
    //     setCollapsible(`param-${p.id}`, false);
    //     setCollapsible(`param-${param.id}`, true);
    // });
};

export const defaultSelectedParameter = () => {
    if (
        getSelectedParameter().isNone() &&
        getSelectedParameterList().length > 0
    ) {
        setSelectedParameterIndex(0);
    }
};
export const defaultSelectedStation = () => {
    if (getSelectedStation().isNone() && getSelectedStationList().length > 0) {
        setSelectedStationIndex(0);
    }
};

export const clearSelectedParameter = () => {
    clearHighligthParameter();
    assign('timeserie/parameter/select/index', null);
};

export const deselectParameter = (kind: ParameterType, id: number) =>
    dispatch('timeserie/parameter/select', ss =>
        ss.filter(s => s.id !== id || s.kind !== kind)
    );

export const clearParameterSelection = () =>
    assign('timeserie/parameter/select', []);

export const lidToParameterType = (lid: string): Option<ParameterType> => {
    switch (lid) {
        case 'surface':
        case 'ground-quality':
        case 'ground-quantity':
            return some(lid as ParameterType);
    }
    return none;
};

const selectStationFromPath = (fp: FeaturePath) => {
    clearPointSelection();
    return scopeOption()
        .let('fp', getFeaturePath(fp))
        .let('kind', ({ fp }) => lidToParameterType(fp.layerId))
        .map(({ fp, kind }) => {
            const id = fp.featureId;
            return { id, kind };
        });
};

export const isSelectedStation = (
    s: StationSelection,
    slist: StationSelection[]
) => slist.findIndex(station => isSameStation(s, station)) >= 0;

const isSameStation = (s1: StationSelection, s2: StationSelection) =>
    s1.id.toString() === s2.id.toString() && s1.kind === s2.kind;

export const selectStation = (stationSelection: StationSelection) =>
    dispatch('timeserie/station/select', ss =>
        ss
            .filter(s => !isSameStation(s, stationSelection))
            .filter(s => s.kind === stationSelection.kind)
            .concat(stationSelection)
    );

export const deselectStation = (stationSelection: StationSelection) =>
    dispatch('timeserie/station/select', ss =>
        ss.filter(s => !isSameStation(s, stationSelection))
    );

export const selectStations = (fps: FeaturePath[]) => {
    const selected = catOptions(fps.map(selectStationFromPath));
    const selectedIndex = query('timeserie/station/select/index');
    dispatch('timeserie/station/select', ss => {
        index(selectedIndex, ss).map(s => {
            const newIndex = selected.findIndex(ns => isSameStation(ns, s));

            assign(
                'timeserie/station/select/index',
                newIndex < 0 ? 0 : newIndex
            );
        });
        return selected;
    });

    getSelectedKind().map(kind =>
        getSelectedParameter().map(p => {
            if (kind !== p.kind) {
                clearParameterSelection();
                clearNormSelection();
                clearSelectedGroup();
            }
        })
    );

    updateTimeserie();
};

export const setSelectedStationIndex = assignK(
    'timeserie/station/select/index'
);
export const setHighlightStation = assignK(
    'timeserie/station/select/highlighted'
);
export const switchHighligthStation = (station: StationSelection) =>
    isHighlightedStation(station)
        ? clearHighlightStation()
        : setHighlightStation(station);

const setStationInfo = assignK('timeserie/station/select/info');
const clearStationInfo = () => assign('timeserie/station/select/info', null);

export const switchStationInfo = (station: StationSelection) =>
    hasStationInfo(station) ? clearStationInfo() : setStationInfo(station);

const setParamInfo = assignK('timeserie/parameter/select/info');
const clearParamInfo = () => assign('timeserie/parameter/select/info', null);

export const switchParamInfo = (param: ParameterSelection) =>
    hasParamInfo(param) ? clearParamInfo() : setParamInfo(param);

export const setSelectedParameterIndex = assignK(
    'timeserie/parameter/select/index'
);

export const highligthParameter = assignK(
    'timeserie/parameter/select/highlighted'
);

export const switchHighligthParameter = (param: ParameterSelection) =>
    isHighlightedParam(param)
        ? clearHighligthParameter()
        : highligthParameter(param);

export const clearHighligthParameter = () =>
    assign('timeserie/parameter/select/highlighted', null);

export const clearStationSelection = () =>
    assign('timeserie/station/select', []);

export const clearHighlightStation = () =>
    assign('timeserie/station/select/highlighted', null);

/**
 * Record data of the selected point on plot
 * @param id id of the timeserie
 * @param beginDate begin date of the cluster or date of the point
 * @param endDate date of the next point (= end of the cluster if there is one, this date not included)
 */
export const selectPoint = (
    id: number,
    start: number,
    end: number,
    x: number,
    y: number,
    color: string
) => assign('timeserie/point/select', { id, start, end, x, y, color });

export const clearPointSelection = () => assign('timeserie/point/select', null);

export const selectNorm = (paramId: number, norm: PlotNorm) => {
    dispatch('timeserie/norm/select', ss =>
        ss
            .filter(
                s =>
                    s.paramId !== paramId ||
                    s.norm.name !== norm.name ||
                    s.norm.value !== norm.value
            )
            .concat({ paramId, norm })
    );
    getDisplayMode() === 'per-parameter'
        ? getSelectedKind().map(kind => {
              highlightAndSelectParam(kind, paramId);
          })
        : noop;
};

export const deselectNorm = (paramId: number, norm: PlotNorm) =>
    dispatch('timeserie/norm/select', ss =>
        ss.filter(
            s =>
                s.paramId !== paramId ||
                s.norm.name !== norm.name ||
                s.norm.value !== norm.value
        )
    );

export const clearNormSelection = () => {
    assign('timeserie/norm/select', []);
};

export const selectWindowStart = (beginDate: Date, isAutomatic: boolean) =>
    assign('timeserie/dates/start', { date: beginDate, isAutomatic });
export const clearWindowStart = () => assign('timeserie/dates/start', null);

export const selectWindowEnd = (endDate: Date, isAutomatic: boolean) =>
    assign('timeserie/dates/end', { date: endDate, isAutomatic });
export const clearWindowEnd = () => assign('timeserie/dates/end', null);

export const clearWindow = () => {
    clearWindowEnd();
    clearWindowStart();
};

export const setDisplayMode = assignK('timeserie/display/mode');

export const setSelectedGroup = (group: GroupData) =>
    assign('timeserie/group/select', group);

export const clearSelectedGroup = () => assign('timeserie/group/select', null);

export const setLevel = (l: Level) => assign('timeserie/level', l);

export const setMode = (m: Mode) => assign('timeserie/mode', m);

const updateTimeserie = () => {
    // updateTimeseries
    query('timeserie/station/select').map(({ kind, id }) =>
        query('timeserie/parameter/select')
            .filter(p => p.kind === kind)
            .map(p => {
                const key = timeserieKey(kind, id, p.id);
                const resource = query('data/timeseries')[key];
                if (!isNotNullNorUndefined(resource)) {
                    loadTimeserie(kind, id, p.id);
                }
            })
    );
};

export const setStationSearchInput = assignK('timeserie/station/search/input');

// export const selectStationFromSearch = () => {};

export const loadWaterBodies = () =>
    fetchWaterBodies()
        .then(wbs => assign('data/waterbodies', wbs))
        .catch(err => logger('Failed loading water bodies', err));

export const selectFeature = (kind: ParameterType) => (feature: Feature) =>
    tryNumber(feature.id).map(id => selectStation({ kind, id }));
// scopeOption()
//     .let('id', tryNumber(feature.id))
//     // .let('level', getLevel())
//     .let('config', ({ id }) => configAddStations([id], kind))
//     // .map(({ level, config }) => navigateConfig(level, config));
//     .map(({ id }) => selectStation({ kind, id }));

export const selectWaterBody =
    (kind: ParameterType) => (waterbody: WaterBody[]) => {
        assign('timeserie/waterbody/select', { wb: waterbody, pType: kind });
        clearStationSelection();
        getLevel().map(l => {
            const ids = getWaterbodyFeatures(waterbody, kind)
                .map(f => tryNumber(f.id).getOrElse(-1))
                .filter(i => i >= 0);
            configAddStations(ids, kind).map(c =>
                navigateInPlace(l, c, getMode())
            );
        });
        // defaultSelectedStation();
    };

export const clearWaterBodySelection = () => {
    assign('timeserie/waterbody/select', null);
    clearStationSelection();
};

export const loadParameterCount = (level: Level, stationId: number) => {
    const key = stationId.toString();
    if (level === 'surface') {
        dispatchAsync('timeserie/parameter/count', state => {
            if (key in state) {
                return Promise.resolve(state);
            }
            return new Promise((resolve, reject) => {
                fetchQuantParamSurface(stationId)
                    .then(data => resolve(updateCollection(state, key, data)))
                    .catch(reject);
            });
        });
    } else if (level === 'ground') {
        dispatchAsync('timeserie/parameter/count', state => {
            if (key in state) {
                return Promise.resolve(state);
            }
            return new Promise((resolve, reject) => {
                fetchQuantParamGround(stationId)
                    .then(data => resolve(updateCollection(state, key, data)))
                    .catch(reject);
            });
        });
    }
};

export const loadStationCount = (level: Level, paramId: number) => {
    const key = paramId.toString();
    if (level === 'surface') {
        dispatchAsync('timeserie/station/count', state => {
            if (key in state) {
                return Promise.resolve(state);
            }
            return new Promise((resolve, reject) => {
                fetchQuantStationSurface(paramId)
                    .then(data => resolve(updateCollection(state, key, data)))
                    .then(() => updateLayers(level))
                    .catch(reject);
            });
        });
    } else if (level === 'ground') {
        dispatchAsync('timeserie/station/count', state => {
            if (key in state) {
                return Promise.resolve(state);
            }
            return new Promise((resolve, reject) => {
                fetchQuantStationGroundQual(paramId)
                    .then(data => resolve(updateCollection(state, key, data)))
                    .then(() => updateLayers(level))
                    .catch(reject);
            });
        });
    }
};

logger('loaded');
