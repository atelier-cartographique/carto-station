import * as debug from 'debug';

import { dispatchK, assign, dispatch, assignK } from 'sdi/shape';
import {
    fetchLayerSurface,
    fetchLayerGroundQuality,
    fetchLayerGroundQuantity,
    fetchHydroInfo,
    fetchDatasetMetadata,
    fetchLayer,
} from 'timeserie/src/remote/map';
import { ParameterType, waterMapId, VisibleGroundLayer } from '../types';
import { removeLayer, addLayer, viewEventsFactory, checkMap } from 'sdi/map';
import { some } from 'fp-ts/lib/Option';
import {
    layerInfo,
    fetchData,
    getGeocoderInput,
    findMetadata,
    findLayerData,
    getHydroLayerInfos,
} from '../queries/map';
import { getSelectedKind } from '../queries/timeserie';
import { Coordinate } from 'ol/coordinate';
import { getLang } from 'sdi/app';
import { queryService } from 'sdi/ports/geocoder';
import { noop, once, updateCollection } from 'sdi/util';
import { scopeOption } from 'sdi/lib';
import { right } from 'fp-ts/lib/Either';
import { getMessageRecord, ILayerInfo, Inspire } from 'sdi/source';
import { Level } from './route';

const logger = debug('sdi:events/map');
export const setView = dispatchK('port/map/view');

export const updateView = viewEventsFactory(setView).updateMapView;
export const setScaleLine = noop;

export const addStationLayer = (kind: ParameterType, filtered = false) =>
    checkMap(waterMapId)
        .map(() => {
            removeLayer(waterMapId, kind);
            addLayer(
                waterMapId,
                () => some(layerInfo(kind)),
                fetchData(kind, filtered)
            );
        })
        .getOrElseL(() =>
            window.setTimeout(() => addStationLayer(kind, filtered), 100)
        );

export const updateLayers = (level: Level) => {
    switch (level) {
        case 'surface':
            addStationLayer('surface', true);
            break;
        case 'ground':
            getSelectedKind().foldL(
                () => {
                    const ks = [
                        'ground-quality',
                        'ground-quantity',
                    ] as ParameterType[];
                    ks.map(k => addStationLayer(k, true));
                },
                k => addStationLayer(k, true)
            );
            break;
    }
};

export const mountGroundLayers = () => {
    addStationLayer('ground-quality');
    addStationLayer('ground-quantity');
};

export const unmountGroundLayer = () => {
    removeLayer(waterMapId, 'ground-quality');
    removeLayer(waterMapId, 'ground-quantity');
};

export const mountSurfaceLayers = () => {
    addStationLayer('surface');
};

export const unmountSurfaceLayer = () => {
    removeLayer(waterMapId, 'surface');
};

export const loadLayers = once(() => {
    fetchLayerSurface()
        .then(fc => {
            assign('data/layer/surface', fc);
        })
        .catch(err => logger(`ERROR loading surface layer: ${err}`));
    fetchLayerGroundQuality()
        .then(fc => {
            assign('data/layer/ground/quality', fc);
        })
        .catch(err => logger(`ERROR loading ground quality layer: ${err}`));
    fetchLayerGroundQuantity()
        .then(fc => {
            assign('data/layer/ground/quantity', fc);
        })
        .catch(err => logger(`ERROR loading ground quantity layer: ${err}`));

    // getLevel().map(level => {
    //     if (level === 'surface') {
    //         addStationLayer('surface');
    //     } else {
    //         addStationLayer('ground-quantity');
    //         addStationLayer('ground-quality');
    //     }
    // });
});

export const addVisibleGroundLayer = (gl: VisibleGroundLayer) =>
    dispatch('timeserie/visible/ground-layer', old =>
        old.filter(l => l !== gl).concat(gl)
    );

export const removeVisibleGroundLayer = (gl: VisibleGroundLayer) =>
    dispatch('timeserie/visible/ground-layer', old =>
        old.filter(l => l !== gl)
    );

export const centerOn = (center: Coordinate) =>
    updateView({
        dirty: 'geo',
        center,
        // zoom: Math.max(getView().zoom, 12),
    });
export const setZoom = (zoom: number) =>
    updateView({
        dirty: 'geo',
        zoom,
    });

export const updateGeocoderInput = assignK('port/geocoder/input');

export const searchAddress = () => {
    const input = getGeocoderInput();
    const lang = getLang();
    queryService(input, lang).then(assignK('port/geocoder/response'));
};

export const clearGeocoderResponse = () => {
    assign('port/geocoder/response', null);
};

const addHydroLayer = (info: ILayerInfo) =>
    scopeOption()
        // .let('info', findLayerInfo(layerId))
        .let('metadata', findMetadata(info.metadataId))
        .let('data', ({ metadata }) =>
            findLayerData(metadata.uniqueResourceIdentifier)
        )
        .map(({ metadata, data }) =>
            checkMap(waterMapId).foldL(
                () => {
                    logger('on attends');
                    window.setTimeout(() => addHydroLayer(info), 200);
                },
                () =>
                    addLayer(
                        waterMapId,
                        () =>
                            some({
                                name: getMessageRecord(metadata.resourceTitle),
                                info,
                                metadata,
                            }),
                        () => right(some(data))
                    )
            )
        );

const loadHydroLayers = (mds: Inspire[]) =>
    mds.map(md =>
        fetchLayer(md.id).then(fc =>
            dispatch('data/layers', layers =>
                updateCollection(layers, md.uniqueResourceIdentifier, fc)
            )
        )
    );

const loadHydroMetadata = (infos: ILayerInfo[]) =>
    infos.map(info =>
        fetchDatasetMetadata(info.metadataId).then(md => {
            dispatch('data/metadatas', mds => mds.concat(md));
            return md;
        })
    );

const updateHydroVisibility = () =>
    getHydroLayerInfos().forEach(info =>
        dispatch('water/hydro-layer/visible', s =>
            updateCollection(s, info.id, true)
        )
    );

export const loadHydro = once(() => {
    fetchHydroInfo()
        .then(infos => {
            assign('data/layer-infos', infos);

            return infos;
        })
        .then(loadHydroMetadata)
        .then(mds => Promise.all(mds))
        .then(loadHydroLayers)
        .then(prs => Promise.all(prs))
        .then(() => {
            updateHydroVisibility();
            getHydroLayerInfos().map(addHydroLayer);
        })
        .catch(err => logger(`Failed to load hydro`, err));
});

export const setHydroVisibility = (layerId: string, visible: boolean) =>
    dispatch('water/hydro-layer/visible', s =>
        updateCollection(s, layerId, visible)
    );

export const setInteraction = dispatchK('port/map/interaction');

logger('loaded');
