/*
 *  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { getApiUrl, loadAlias, loop } from 'sdi/app';
import { DIV, NodeOrOptional } from 'sdi/components/elements';
import header from 'sdi/components/header';
import footer from 'sdi/components/footer';

import { loadRoute } from './events/route';
import { getLayout } from './queries/app';

import home from './components/home';
import dashboard from './components/dashboard';
import { loadDashboards, loadDefaultDashboard } from './events/app';
import { loadAllActivity } from './events/statistics';
import { getBeginTsOrDefault, getEndTsOrDefault } from './queries/statistics';

const wrappedMain = (name: string, ...elements: NodeOrOptional[]) =>
    DIV(
        'app-inner statistics-inner',
        header('statistics'),
        DIV({ className: `main ${name}` }, ...elements),
        footer()
    );

const renderHome = () => wrappedMain('home', home());

const renderDashboard = () => wrappedMain('dashboard', dashboard());

const render = () => {
    const layout = getLayout();
    switch (layout) {
        case 'home':
            return renderHome();
        case 'dashboard':
            return renderDashboard();
    }
};

const effects = (initialRoute: string[]) => () => {
    loadAlias(getApiUrl(`alias`));
    loadDashboards()
        .then(loadDefaultDashboard)
        .then(() => loadRoute(initialRoute));
    loadAllActivity(getBeginTsOrDefault(), getEndTsOrDefault());
};

export const app = (initialRoute: string[]) =>
    loop('statistics', render, effects(initialRoute));
