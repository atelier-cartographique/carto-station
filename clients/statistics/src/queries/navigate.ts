import { fromNullable } from 'fp-ts/lib/Option';
import { query } from 'sdi/shape';
import { MessageRecord } from 'sdi/source';
import { Route } from '../events/route';

export const getNext = () => fromNullable(query('navigate/next'));

const links: [MessageRecord, Route][] = [[{ fr: 'Home', nl: 'Home' }, 'home']];

export const getLinks = () => links;
