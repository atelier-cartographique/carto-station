import { fromNullable } from 'fp-ts/lib/Option';
import * as io from 'io-ts';
import { ActivityActionNameIO } from 'sdi/source/io/activity';
import { i, l, u } from 'sdi/source/io/io';

const AggregationTypeIO = u([
    l('count'),
    l('sum'),
    l('average'),
    l('min'),
    l('max'),
]);

export type AggregationType = io.TypeOf<typeof AggregationTypeIO>;

// tslint:disable-next-line: variable-name
const DirectedContainerIO: io.Type<DirectedContainer> = io.recursion(
    'DirectedContainerIO',
    () =>
        i({
            type: l('DirectedContainer'),
            direction: DirectionIO,
            children: io.array(PanelIO),
            data: PanelDataIO,
        })
);

// tslint:disable-next-line: variable-name
const PanelIO: io.Type<Panel> = io.recursion('PannelIO', () =>
    u([WidgetIO, MultiWidgetIO, DirectedContainerIO])
);

// tslint:disable-next-line: variable-name
// export const OptionValueIO = u([io.string, io.number], 'OptionValueIO');
export const PanelDataIO = io.partial(
    {
        title: io.string,
        titleFR: io.string,
        titleNL: io.string,
    },
    'PanelDataIO'
);
export type PanelData = io.TypeOf<typeof PanelDataIO>;
type panelOptions = keyof PanelData;

export const getPanelOption = (optionName: panelOptions) => (panel: Panel) =>
    fromNullable(panel.data[optionName]);

// tslint:disable-next-line:variable-name
export const PageIO = i(
    {
        type: l('Page'),
        boxes: io.array(DirectedContainerIO),
        data: PanelDataIO,
    },
    'PageIO'
);
export type Page = io.TypeOf<typeof PageIO>;

export const LayoutIO = io.array(PageIO);
export type Layout = io.TypeOf<typeof LayoutIO>;

// tslint:disable-next-line: variable-name
export const DashboardIO = i({
    id: io.Integer,
    name: io.string,
    layout: LayoutIO,
});

export type Dashboard = io.TypeOf<typeof DashboardIO>;

export const DashboardListIO = io.array(DashboardIO);
export type DashboardList = io.TypeOf<typeof DashboardListIO>;

// tslint:disable-next-line: variable-name
export const WidgetDataIO = io.intersection([
    i({
        ns: io.string,
        action: ActivityActionNameIO,
    }),
    PanelDataIO,
]);
export type WidgetData = io.TypeOf<typeof WidgetDataIO>;

export const ListWidgetIO = i({
    type: l('Widget'),
    tag: l('list'),
    data: io.intersection([WidgetDataIO, i({ nb: io.number })]),
});
export const ListReversedWidgetIO = i({
    type: l('Widget'),
    tag: l('reversedlist'),
    data: io.intersection([WidgetDataIO, i({ nb: io.number })]),
});

export const ValueWidgetIO = i({
    type: l('Widget'),
    tag: l('value'),
    data: io.intersection([
        WidgetDataIO,
        i({ aggregation: AggregationTypeIO }),
    ]),
});

export const GraphWidgetIO = i({
    type: l('Widget'),
    tag: l('graph'),
    data: io.intersection([
        WidgetDataIO,
        i({
            graphtype: u([
                l('barchart'),
                l('piechart'),
                l('linechart'),
                l('time-barchart'),
            ]),
        }),
    ]),
});

export const MultiWidgetDataIO = io.intersection([
    i({
        ns: io.array(io.string),
        action: ActivityActionNameIO,
    }),
    PanelDataIO,
]);
export type MultiWidgetData = io.TypeOf<typeof MultiWidgetDataIO>;

export const ListMultiWidgetIO = i({
    type: l('MultiWidget'),
    tag: l('list'),
    data: io.intersection([MultiWidgetDataIO, i({ nb: io.number })]),
});
export const ListMultiReversedWidgetIO = i({
    type: l('MultiWidget'),
    tag: l('reversedlist'),
    data: io.intersection([MultiWidgetDataIO, i({ nb: io.number })]),
});

export const AppListMultiWidgetIO = i({
    type: l('MultiWidget'),
    tag: l('applist'),
    data: io.intersection([
        MultiWidgetDataIO,
        i({ aggregation: AggregationTypeIO }),
    ]),
});

export const ValueMultiWidgetIO = i({
    type: l('MultiWidget'),
    tag: l('value'),
    data: io.intersection([
        MultiWidgetDataIO,
        i({ aggregation: AggregationTypeIO }),
    ]),
});

export const GraphMultiWidgetIO = i({
    type: l('MultiWidget'),
    tag: l('graph'),
    data: io.intersection([
        MultiWidgetDataIO,
        i({
            graphtype: u([
                l('barchart'),
                l('piechart'),
                l('linechart'),
                l('time-barchart'),
            ]),
        }),
    ]),
});

export const WidgetIO = u([
    ListWidgetIO,
    ListReversedWidgetIO,
    ValueWidgetIO,
    GraphWidgetIO,
]);
export type Widget = io.TypeOf<typeof WidgetIO>;
export const MultiWidgetIO = u([
    ListMultiWidgetIO,
    ListMultiReversedWidgetIO,
    AppListMultiWidgetIO,
    ValueMultiWidgetIO,
    GraphMultiWidgetIO,
]);
export type MultiWidget = io.TypeOf<typeof MultiWidgetIO>;

export type Panel = Widget | DirectedContainer | MultiWidget;

export interface DirectedContainer {
    type: 'DirectedContainer';
    direction: Direction;
    children: Panel[];
    data: PanelData;
}

// tslint:disable-next-line: variable-name
export const DirectionIO = u([l('Vertical'), l('Horizontal')], 'DirectionIO');
export type Direction = io.TypeOf<typeof DirectionIO>;
