import { string as stringIO } from 'io-ts';
import { fetchIO, IUser, IUserIO, IUserList, IUserListIO } from 'sdi/source';
import { ActivityResult, ActivityResultIO } from 'sdi/source/io/activity';
import { DashboardList, DashboardListIO } from '../dashboard';

export const fetchUser = (url: string): Promise<IUser> => fetchIO(IUserIO, url);

export const fetchAllUsers = (url: string): Promise<IUserList> =>
    fetchIO(IUserListIO, url);

// export const fetchActivity =
//     (url: string, namespace: string, begin: number, end: number): Promise<ActivityResult> =>
//         fetchIO(ActivityResultIO, `${url}/${namespace}/${begin}/${end}`)

export const fetchAllActivity = (
    url: string,
    begin: number,
    end: number
): Promise<ActivityResult> =>
    fetchIO(ActivityResultIO, `${url}/${begin}/${end}`);

export const fetchDashboards = (url: string): Promise<DashboardList> =>
    fetchIO(DashboardListIO, url);

export const fetchDefaultDashboard = (): Promise<string> =>
    fetchIO(stringIO, `/activity/default_dashboard`);
