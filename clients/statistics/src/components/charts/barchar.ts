import { DIV } from 'sdi/components/elements';
import { BAR_COLOR, ChartElem } from '.';

const maxValue = (data: ChartElem[]) =>
    data.map(d => d.value).reduce((acc, val) => (val > acc ? val : acc), 0);

export const barChart = (data: ChartElem[]) => {
    // const yDistance = graphsize.height / data.length / 1.5;
    const maxWidth = maxValue(data);
    // const xFactor = graphsize.width / maxWidth;

    const bars = data.map(e => {
        // const xWidth = e.value * xFactor;
        const percentageWidth = (e.value / maxWidth) * 100;

        return DIV('barchart-line', [
            DIV('barchart-label', e.label),
            DIV(
                'barchart-bar__wrapper',
                DIV(
                    {
                        className: 'barchart-bar',
                        style: {
                            width: `${percentageWidth}%`,
                            backgroundColor: BAR_COLOR,
                            height: '1.5rem',
                        },
                    }

                    // we don't need a SVG for now
                    // svg(
                    //     [
                    //         rect(0, 0, xWidth, yDistance, {
                    //             stroke: BAR_COLOR,
                    //             fill: BAR_COLOR,
                    //             className: 'rect',
                    //         }),
                    //     ],
                    //     {
                    //         viewBox: `0 0 ${graphsize.width} ${yDistance}`,
                    //     }
                    // ),
                ),
                DIV('barchart-value', e.value)
            ),
        ]);
    });

    return DIV('barchart', bars);
};
