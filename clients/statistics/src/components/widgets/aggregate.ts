import { getActivitiesForActionRemote } from '../../queries/statistics';
import { foldRemote } from 'sdi/source';

import { DIV, H2, NodeOrOptional } from 'sdi/components/elements';
import { Activity, ActivityActionName } from 'sdi/source/io/activity/index';
import { AggregationType } from 'statistics/src/dashboard';
import { none } from 'fp-ts/lib/Option';
import tr from 'sdi/locale';

const renderCount = (activities: Activity[]) => DIV('count', activities.length);

export const aggregate = (
    namespaces: string | string[],
    action: ActivityActionName,
    aggregation: AggregationType
) =>
    foldRemote<Activity[], string, NodeOrOptional>(
        () => none,
        () =>
            DIV(
                { className: 'info' },
                DIV(
                    'loader',
                    tr.core('loadingData'),
                    DIV({ className: 'loader-spinner' })
                )
            ),
        err => DIV('error', `${tr.stat('errorFetchingData')} (${err})`),
        list => {
            switch (aggregation) {
                case 'count':
                    return renderCount(list);
                case 'average':
                    return 0; // TODO
                case 'sum':
                    return 0; // TODO
                case 'min':
                    return 0; // TODO
                case 'max':
                    return 0; // TODO
            }
        }
    )(getActivitiesForActionRemote(namespaces, action));

export const renderAggregation = (
    aggregation: AggregationType,
    namespaces: string | string[],
    actionName: ActivityActionName,
    title: string
) =>
    DIV(
        { className: 'container container--widget aggregation' },
        DIV(
            { className: 'widget__header' },
            H2({ className: 'widget__name' }, title)
        ),
        DIV(
            { className: 'widget__body' },
            aggregate(namespaces, actionName, aggregation)
        )
    );
