import { DIV, H1, SPAN } from 'sdi/components/elements';
import { inputDate, renderSelect } from 'sdi/components/input';
import {
    getBeginDateOrDefault,
    getEndDateOrDefault,
} from '../queries/statistics';
import { getDashboards, getSelectedDashboard } from '../queries/app';
import renderPanel from '../components/display';
import { navigateBegin, navigateDashboard, navigateEnd } from '../events/route';
import tr from 'sdi/locale';
import { SetoidDashboard } from '../events/app';

const inputDateStart = inputDate('start-date');
const inputDateEnd = inputDate('end-date');

const dateRange = () =>
    DIV(
        'date-range__wrapper',
        // SPAN('date-range__label', tr.stat('dateRangeFrom')),
        inputDateStart(
            new Date(0),
            getEndDateOrDefault(),
            getBeginDateOrDefault(),
            navigateBegin
        ),

        // SPAN('date-range__label', tr.stat('dateRangeTo')),
        SPAN('date-range__label', '-'),
        inputDateEnd(
            getBeginDateOrDefault(),
            new Date(),
            getEndDateOrDefault(),
            navigateEnd
        )
    );
const renderDashboardSelect = renderSelect(
    'dashboard__select',
    d => d.name,
    d => navigateDashboard(d.id),
    SetoidDashboard
);

const dashboardHeader = () =>
    DIV(
        'dashboard__header',
        H1('', tr.stat('appTitle')),
        dateRange(),
        getDashboards().map(list =>
            renderDashboardSelect(list, getSelectedDashboard())
        )
    );

const renderDashboard = () =>
    DIV(
        { className: 'main-app dashboard' },
        dashboardHeader(),
        // getDashboards().map(p => renderDashboardSelect(p, getSelectedDashboard())),
        getSelectedDashboard().map(renderPanel)
    );

export default renderDashboard;
