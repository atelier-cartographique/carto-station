import * as io from 'io-ts';
import { fetchIO } from 'sdi/source';
import {
    ProjectIO,
    InformationUnitNameIO,
    UnitIO,
    InformationUnitName,
} from 'angled-core/ui';
import { getApiUrl } from 'sdi/app';

// tslint:disable-next-line: variable-name
export const TransactionIO = io.interface({
    id: io.Integer,
    user: io.Integer,
    created_at: io.Integer,
    project: io.Integer,
    units: io.array(InformationUnitNameIO),
});

export type Transaction = io.TypeOf<typeof TransactionIO>;

// tslint:disable-next-line: variable-name
export const TimelineIO = io.array(TransactionIO);

export type Timeline = io.TypeOf<typeof TimelineIO>;

// tslint:disable-next-line: variable-name
export const UnitHistoryIO = io.array(UnitIO);

export type UnitHistory = io.TypeOf<typeof UnitHistoryIO>;

export const fetchTimeline = (id: number): Promise<Timeline> =>
    fetchIO(TimelineIO, getApiUrl(`geodata/angled/p/project/${id}/timeline/`));

export const fetchProjectAtTime = (id: number, timestamp: number) =>
    fetchIO(
        ProjectIO,
        getApiUrl(`geodata/angled/p/project/${id}/?at=${timestamp}`)
    );

export const fetchUnitHistory = (id: number, name: InformationUnitName) =>
    fetchIO(
        UnitHistoryIO,
        getApiUrl(`geodata/angled/p/project/${id}/unit/?name=${name}`)
    );
