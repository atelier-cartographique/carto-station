import { Unit, typeNameToName } from 'angled-core/ui';
import { fromNullable } from 'fp-ts/lib/Option';
import { query } from 'sdi/shape';

export const getModalAudienceUnit = () =>
    fromNullable(query('component/project/form/modal/audience'));

export const getModalTagUnit = () =>
    fromNullable(query('component/project/form/modal/tag'));

export const getUnitName = (unitData: Unit | Unit[]) => {
    const typeName = Array.isArray(unitData)
        ? unitData.length > 0
            ? unitData[0].unit
            : null
        : unitData.unit;
    if (typeName === null) {
        return 'unknown';
    }
    return typeNameToName[typeName];
};
