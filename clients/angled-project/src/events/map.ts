import { dispatch, assign } from 'sdi/shape';
import {
    FetchData,
    removeLayer,
    addLayer,
    singleClickInteraction,
} from 'sdi/map';
import {
    GeometryType,
    Inspire,
    StyleConfig,
} from 'sdi/source';
import { right } from 'fp-ts/lib/Either';
import { some } from 'fp-ts/lib/Option';
import { isPoint, isLine, isPolygon, toGeoJSON } from 'angled-core/project';
import { nameToCode } from 'sdi/components/button/names';
import { mapTemplate, layerTemplate } from 'angled-core/events/map';
import { PROJECT_SELECT_MAP } from 'angled-core/map';
import { getProjectsOpt } from '../queries/project';
import { iife } from 'sdi/lib';

////////// select map

const metadataTemplate = (geometryType: GeometryType): Inspire => ({
    id: geometryType,
    geometryType,
    resourceTitle: { fr: geometryType, nl: geometryType, en: geometryType },
    resourceAbstract: { fr: geometryType, nl: geometryType, en: geometryType },
    uniqueResourceIdentifier: geometryType,
    topicCategory: [],
    keywords: [],
    geographicBoundingBox: { west: 0.0, north: 0.0, east: 0.0, south: 0.0 },
    temporalReference: { creation: Date(), revision: Date() },
    responsibleOrganisation: [1],
    metadataPointOfContact: [1],
    metadataDate: Date(),
    published: false,
    dataStreamUrl: null,
    maintenanceFrequency: 'unknown',
});

// todo robin : I feel like this should not remain here
export const geometryTypeStyle = (geometryType: GeometryType): StyleConfig => {
    switch (geometryType) {
        case 'Point':
        case 'MultiPoint':
            return {
                kind: 'point-simple',
                marker: {
                    codePoint: nameToCode('circle'),
                    size: 12,
                    color: 'rgb(223,88,68)',
                },
            };
        case 'LineString':
        case 'MultiLineString':
            return {
                kind: 'line-simple',
                strokeColor: 'rgb(223,88,68)',
                strokeWidth: 1,
                dash: [],
            };
        case 'Polygon':
        case 'MultiPolygon':
            return {
                kind: 'polygon-simple',
                fillColor: 'transparent',
                pattern: {
                    angle: 45,
                    color: 'rgb(223,88,68)',
                    width: 1,
                },
                strokeColor: 'rgb(223,88,68)',
                strokeWidth: 1,
            };
    }
};

export const loadMaps = () =>
    dispatch('data/maps', state => state.concat([mapTemplate()]));

const layerInfoOption = (geometryType: GeometryType) => () =>
    some({
        name: { fr: geometryType, nl: geometryType, en: geometryType },
        info: layerTemplate(geometryTypeStyle)(geometryType),
        metadata: metadataTemplate(geometryType),
    });

const fetchData =
    (geometryType: GeometryType): FetchData =>
    () => {
        const filter = iife(() => {
            switch (geometryType) {
                case 'Point':
                case 'MultiPoint':
                    return isPoint;

                case 'LineString':
                case 'MultiLineString':
                    return isLine;
                case 'Polygon':
                case 'MultiPolygon':
                    return isPolygon;
            }
        });
        const projects = getProjectsOpt().map(projects =>
            toGeoJSON(projects.filter(filter))
        );
        return right(projects);
    };

export const addProjectsLayer = () => {
    assign('port/map/interaction', singleClickInteraction());
    const xs: GeometryType[] = [
        'MultiPoint',
        'MultiLineString',
        'MultiPolygon',
    ];
    const shouldRetry = xs
        .map(x => {
            removeLayer(PROJECT_SELECT_MAP, x);
            return addLayer(PROJECT_SELECT_MAP, layerInfoOption(x), fetchData(x));
        })
        .reduce((acc, o) => o.isNone() || acc, false);
    if (shouldRetry) {
        setTimeout(addProjectsLayer, 500);
    }
};
