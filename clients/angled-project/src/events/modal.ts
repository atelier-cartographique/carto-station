import * as debug from 'debug';

import {
    register,
    renderSaveFinalStep,
    renderNotifier,
    renderAudience,
    renderTag,
} from '../components/modal';
import {
    getDisplayProjectNotif,
    getUnreadNotifications,
} from 'angled-core/queries/sub';
import { markRead } from 'angled-core/events/sub';
import { iife } from 'sdi/lib';
import { nameToTypeName } from 'angled-core/ui';
import { fromNullable } from 'fp-ts/lib/Option';
import { makeSetReset } from 'sdi/shape';

const logger = debug('sdi:events/modal');

export const [closeSaveFinalStep, openSaveFinalStep] = register(
    'project/save-final-step',
    renderSaveFinalStep
);

const [, openNotifierImpl] = register('project/notifier', renderNotifier);

export const openNotifier = () => {
    openNotifierImpl();
    const t = Math.min(5, getDisplayProjectNotif().length);
    window.setTimeout(() => getUnreadNotifications().map(markRead), t * 1000);
};

export const openAudienceModal = iife(() => {
    const openers = Object.keys(nameToTypeName).map(name => {
        const [, open] = register(`project/audience/${name}`, renderAudience);
        return { name, open };
    });
    return (unitName: string) =>
        fromNullable(openers.find(({ name }) => name === unitName)).map(
            ({ open }) => open()
        );
});

export const openTagModal = iife(() => {
    const openers = Object.keys(nameToTypeName).map(name => {
        const [, open] = register(`project/tag/${name}`, renderTag);
        return { name, open };
    });
    return (unitName: string) =>
        fromNullable(openers.find(({ name }) => name === unitName)).map(
            ({ open }) => open()
        );
});

export const [setModalAudienceUnit, resetModalAudienceUnit] = makeSetReset(
    'component/project/form/modal/audience',
    null
);

export const [setModalTagUnit, resetModalTagUnit] = makeSetReset(
    'component/project/form/modal/tag',
    null
);

logger('loaded');
