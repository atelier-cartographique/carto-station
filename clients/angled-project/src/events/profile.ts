import { assign } from 'sdi/shape';
import { getCurrentPageNumber } from 'angled-project/src/queries/profile';

export const nextPannel = () => {
    assign('profile/current/page', getCurrentPageNumber() + 1);
};

export const prevPannel = () => {
    assign('profile/current/page', getCurrentPageNumber() - 1);
};
