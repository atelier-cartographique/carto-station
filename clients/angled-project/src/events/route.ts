import * as debug from 'debug';

import { index } from 'fp-ts/lib/Array';
import { fromEither, Option } from 'fp-ts/lib/Option';
import { literal, union, TypeOf } from 'io-ts';

import { Router, Path } from 'sdi/router';
import { tryNumber } from 'sdi/util';

import { loadProjects, setLayout, setProfileAndPannelNumber } from './app';
import { setCurrentProject } from './project';
import { getCurrentProfile, findProfile } from '../queries/profile';
import { resetDisplayAudience } from 'angled-core/events/ui';
import {
    loadAllBaseLayers,
    loadAllDatasetMetadata,
    loadLayerInfos,
} from 'angled-core/events/map';
import { getApiUrl } from 'sdi/app';
import {loadMaps, addProjectsLayer, geometryTypeStyle} from './map';

const logger = debug('sdi:route');

// tslint:disable-next-line: variable-name
const RouteIO = union([
    literal('index'),
    literal('form-select'),
    literal('form'),
    literal('review'),
]);
type Route = TypeOf<typeof RouteIO>;

const { home, route, navigate } = Router<Route>('angled-project');

const projectIdParser = (p: Path) => index(0, p).chain(tryNumber);

type NameAndPannelNumberResult = [number, Option<number>, Option<number>];

const projectNameAndPositionParser = (
    p: Path
): Option<NameAndPannelNumberResult> =>
    projectIdParser(p).map<NameAndPannelNumberResult>(pid => [
        pid,
        index(1, p).chain(tryNumber),
        index(2, p).chain(tryNumber),
    ]);

home('index', () => {
    setLayout('Home');
    loadAllBaseLayers(getApiUrl(`wmsconfig/`));
    loadAllDatasetMetadata();
    loadLayerInfos(geometryTypeStyle);
    loadMaps();
    loadProjects().then(addProjectsLayer);
});

route(
    'form',
    r =>
        r.map(([projectId, pageName, pannelNumber]) => {
            const targetProfile = pageName.foldL(
                getCurrentProfile,
                findProfile
            );
            setCurrentProject(projectId);
            resetDisplayAudience();

            targetProfile.foldL(
                () => setLayout('FormSelect'),
                p => {
                    setProfileAndPannelNumber(p.id, pannelNumber.getOrElse(0));
                    setLayout('Form');
                }
            );
        }),
    projectNameAndPositionParser
);

export const loadRoute = (initial: string[]) =>
    index(0, initial)
        .chain(prefix =>
            fromEither(
                RouteIO.decode(prefix).map(c => navigate(c, initial.slice(1)))
            )
        )
        .getOrElseL(navigateHome);

export const navigateHome = () => navigate('index', []);

export const navigateForm = (
    projectId: number,
    profileId: number,
    pannelNumber = 0
) => navigate('form', [projectId, profileId, pannelNumber]);

export const navigateFormNaked = (projectId: number) =>
    navigate('form', [projectId]);

logger('loaded');
