import { DIV } from 'sdi/components/elements';
import tr, { formatDate, Translated } from 'sdi/locale';
import { divTooltipTopRight } from 'sdi/components/tooltip';
import { fromNullable, some, none } from 'fp-ts/lib/Option';
import {
    getTimeline,
    getTimelineDateStart,
    getTimelineDateEnd,
    getCurrentTimelineId,
    getTimelineWindow,
} from '../../queries/project';
import {
    setCurrentTimelineId,
    loadCurrentProjectAtTime,
    setTimelineWindow,
    clearTimelineWindow,
} from '../../events/project';
import { Transaction } from '../../remote';
import { makeLabelAndIcon } from '../buttons';

export const setTimelineWindowOneDay = makeLabelAndIcon(
    'search',
    1,
    'search',
    () => tr.angled('lastDay')
);

export const setTimelineWindowOneWeek = makeLabelAndIcon(
    'search',
    1,
    'search',
    () => tr.angled('lastWeek')
);

export const resetTimelineWindow = makeLabelAndIcon('search', 1, 'search', () =>
    tr.angled('resetTimelineWindow')
);

export const setTimelineWindowButtonActive = (time: number | null) =>
    getTimelineWindow()
        .map(w => (w === time ? 'active' : ''))
        .getOrElse('');

export const resetTimelineWindowButtonActive = () =>
    fromNullable(getTimelineWindow()).fold('active', w =>
        w === null ? 'active' : ''
    );

export const setHighlightTimelineItem = (t: Transaction) =>
    getCurrentTimelineId()
        .chain(id => (id === t.id ? some('timeline__event--active') : none))
        .getOrElse('');

const onEventClick = (t: Transaction) => () => {
    setCurrentTimelineId(t.id);
    loadCurrentProjectAtTime();
};

const setEventPosition = (t: Transaction) => {
    const defaultDate: number = t.created_at;
    const maxDate = getTimelineDateEnd().fold(defaultDate, d => d);
    const minDate = getTimelineDateStart().fold(defaultDate, d => d);
    // I don't like dividedByZero exceptions, but better handling should be possible - pm
    const denom = maxDate - minDate !== 0 ? maxDate - minDate : 1;
    return ((t.created_at - minDate) / denom) * 100;
};

const renderEventDate = (t: Transaction, date: string) =>
    getCurrentTimelineId().chain(id =>
        id === t.id ? some(DIV('timeline__event-date', date)) : none
    );

const renderEvent = (tx: Transaction, zIndex: number) =>
    DIV(
        {
            className: `timeline__event ${setHighlightTimelineItem(tx)}`,
            key: `Event-${tx.id}`,
            onClick: onEventClick(tx),
            style: { left: setEventPosition(tx) + '%' },
        },
        renderEventDate(tx, formatDate(new Date(tx.created_at))),
        divTooltipTopRight(
            formatDate(new Date(tx.created_at)) as Translated,
            {},
            DIV({
                className: 'timeline__event-pointer',
                style: { zIndex },
            })
        )
    );

const WEEK_DURATION_MILLI = 7 * 24 * 60 * 60 * 1000;
const DAY_DURATION_MILLI = 24 * 60 * 60 * 1000;

export const timeline = () =>
    getTimeline().map(events =>
        DIV(
            'timeline__wrapper',
            DIV('timeline__prehistory'),
            DIV('timeline', ...events.map((t, i) => renderEvent(t, i))),
            DIV('timeline__future'),
            setTimelineWindowOneWeek(
                () => setTimelineWindow(WEEK_DURATION_MILLI),
                setTimelineWindowButtonActive(WEEK_DURATION_MILLI)
            ),
            setTimelineWindowOneDay(
                () => setTimelineWindow(DAY_DURATION_MILLI),
                setTimelineWindowButtonActive(DAY_DURATION_MILLI)
            ),
            resetTimelineWindow(
                clearTimelineWindow,
                resetTimelineWindowButtonActive()
            )
        )
    );
