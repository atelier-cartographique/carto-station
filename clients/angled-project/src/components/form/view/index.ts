import { ReactNode } from 'react';

import { NODISPLAY } from 'sdi/components/elements';
import { Collection } from 'sdi/util';

import { Project } from 'angled-core/ui';
import { PanelOptions } from 'angled-core/profile';

import { renderCost } from './cost';
import { renderHousing } from './housing';
import { renderSchool } from './school';
import { renderSchoolSite } from './school_site';
import { renderNova } from './nova';
import { fromNullable } from 'fp-ts/lib/Option';

export type ViewRender = (p: Project, o: PanelOptions) => ReactNode;

const views: Collection<ViewRender> = {
    cost: renderCost,
    housing: renderHousing,
    school: renderSchool,
    school_site: renderSchoolSite,
    nova: renderNova,
};

const renderView = (k: string, options: PanelOptions, p: Project) =>
    fromNullable(views[k]).fold(NODISPLAY(), render => render(p, options));

export default renderView;
