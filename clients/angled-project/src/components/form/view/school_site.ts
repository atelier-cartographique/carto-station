import { Project } from 'angled-core/ui';
import { DIV } from 'sdi/components/elements';
import { fromNullable, some, none } from 'fp-ts/lib/Option';
import { isNotNullNorUndefined } from 'sdi/util';
import { findSchoolSite } from 'angled-core/queries/ref';
import { SchoolSite } from 'angled-core/ref';
import { loadSchoolSite } from 'angled-core/events/ref';
import tr from 'sdi/locale';
import { PanelOptions } from 'angled-core/profile';

// a little dirty hack?
const loadingSchools: { [k: string]: boolean } = {};

const tryLoadSchoolSite = (id: number) => {
    const sid = id.toString();
    if (!(sid in loadingSchools)) {
        loadingSchools[sid] = true;
        loadSchoolSite(id);
    }
    return none;
};

const getSchoolSite = (p: Project) =>
    fromNullable(p.school_site)
        .chain(unit => (isNotNullNorUndefined(unit) ? some(unit.site) : none))
        .chain(id => {
            const ss = findSchoolSite(id);
            if (ss.isNone()) {
                return tryLoadSchoolSite(id);
            }
            return ss;
        });

const renderKV = <T>(k: string, v: T) =>
    DIV(
        { className: 'kv' },
        DIV({ className: 'k' }, k),
        DIV({ className: 'v' }, v)
    );

const renderSchoolSiteDetail = (ss: SchoolSite) =>
    DIV(
        { className: 'widget__body ss--inner' },
        renderKV(tr.core('name'), ss.name),
        renderKV(tr.core('language'), ss.lang),
        renderKV(tr.core('type'), ss.kind),
        renderKV(tr.angled('viewSSOrganizer'), ss.organizer),
        renderKV(tr.core('level'), ss.level),
        renderKV(tr.core('locality'), ss.locality),
        renderKV(tr.core('street'), ss.street)
    );

const title = (options: PanelOptions) =>
    fromNullable(options['title']).getOrElse(tr.angled('viewSchoolSite'));

export const renderSchoolSite = (p: Project, options: PanelOptions) =>
    DIV(
        { className: 'container container--widget project-view' },
        DIV(
            { className: 'widget__header' },
            DIV(
                { className: 'widget__header--top' },
                DIV({ className: 'widget__name' }, title(options))
            )
        ),
        DIV('extra-info', tr.angled('computedInfosExternalData')),
        getSchoolSite(p).map(renderSchoolSiteDetail)
    );
