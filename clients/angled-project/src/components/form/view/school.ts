import { PanelOptions } from 'angled-core/profile';
import { Project, UnitSchoolSeats } from 'angled-core/ui';
import { DIV, SPAN } from 'sdi/components/elements';
import { Option, fromNullable } from 'fp-ts/lib/Option';
import { sort } from 'fp-ts/lib/Array';
import { Ord } from 'fp-ts/lib/Ord';
import { findTerm } from 'angled-core/queries/universe';
import tr, { fromRecord, Translated } from 'sdi/locale';

const ordSchoolUnits: Ord<UnitSchoolSeats> = {
    equals: (a, b) => a.opening === b.opening,
    compare: (a, b) =>
        a.opening === b.opening ? 0 : a.opening < b.opening ? -1 : 1,
};

const levelString = (id: number) =>
    findTerm(id).fold('xxx' as Translated, t => fromRecord(t.name));

const renderKV = <T>(k: string, v: T) =>
    DIV(
        { className: 'kv' },
        DIV({ className: 'k' }, k),
        DIV({ className: 'v' }, v)
    );

const renderSeats = (units: UnitSchoolSeats[]) => {
    const sortedUnits = sort(ordSchoolUnits)(units);
    const openings = sortedUnits.map(u =>
        DIV(
            { className: 'opening' },
            SPAN({ className: 'year' }, `${u.opening} : `),
            SPAN(
                { className: 'quantity' },
                `${u.quantity} ${levelString(u.school_level_basic)}`
            )
        )
    );

    const total = units.reduce((acc, u) => acc + u.quantity, 0);
    const start = Math.min(...units.map(u => u.opening));
    const end = Math.max(...units.map(u => u.opening));

    return DIV(
        { className: 'widget__body' },
        renderKV(tr.angled('schoolSeatOpeningTotal'), total),
        renderKV(tr.angled('schoolSeatOpeningStart'), start),
        renderKV(tr.angled('schoolSeatOpeningEnd'), end),
        DIV(
            { className: 'kv' },
            DIV({ className: 'k' }, tr.angled('schoolSeatDispatch')),
            ...openings
        )
    );
};

const renderSeatsOpt = (opt: Option<UnitSchoolSeats[]>) => opt.map(renderSeats);

export const renderSchool = (p: Project, _options: PanelOptions) => {
    const seats = renderSeatsOpt(fromNullable(p.school_seats));
    return DIV(
        { className: 'container container--widget project-view' },
        DIV(
            { className: 'widget__header' },
            DIV(
                { className: 'widget__header--top' },
                DIV(
                    { className: 'widget__name' },
                    tr.angled('viewSchoolDispatch')
                )
            )
        ),
        DIV('extra-info', tr.angled('computedInfosExternalData')),
        seats
    );
};
