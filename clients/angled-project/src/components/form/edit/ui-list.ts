import { DIV } from 'sdi/components/elements';
import { getCurrentUnitNamesByPage } from 'angled-project/src/queries/profile';
import { InformationUnitName } from 'angled-core/ui';
import { selectUnit } from 'angled-project/src/events/project';
import { unitDisplayName } from 'angled-core/queries/app';
import { renderRadioIn } from 'sdi/components/input';
import { getSelectedUnit } from 'angled-project/src/queries/project';

const editUnitName =
    (
        stagedUnitNames: InformationUnitName[],
        selected: InformationUnitName | null
    ) =>
    (name: InformationUnitName) => {
        const enable = elem(name, stagedUnitNames) ? 'enabled' : '';
        const selectedClass = selected === name ? 'selected' : '';
        return DIV(
            {
                // className: `list__item list__item--drawer unit unit--${name} unit--edit ${enable} ${selectedClass}`,
                className: `unit unit--${name} unit--edit ${enable} ${selectedClass}`,
                // key: `editUnitName-inner-${name}-${index}`,
                onClick: () => selectUnit(name),
            },
            unitDisplayName(name)
        );
    };

// const editUnitName =
//     (name: InformationUnitName, index: number, enabled: string, selected: string) =>
//         DIV({
//             className: 'list__item unit__wrapper',
//             onClick: () => selectUnit(name),
//             key: `editUnitName-outter-${name}-${index}`,
//         },
//             DIV({ className: `unit unit--${name} unit--edit ${enabled} ${selected}`, key: `editUnitName-inner-${name}-${index}` }, unitDisplayName(name)),
//         );

const elem = // feature : mettre dans sdi/utils ?
    // Test if a value is a member of an array
    (value: any, a: any[]) => a.indexOf(value) !== -1;

export const renderList =
    (stagedUnitNames: InformationUnitName[]) =>
    (namesByPage: InformationUnitName[][]) =>
        namesByPage.map((names, index) => {
            const selected = getSelectedUnit().toNullable();
            return DIV(
                {
                    className: 'unit__page',
                    key: `editUnitPage-${name}-${index}`,
                },
                renderRadioIn(
                    `edit-unit-list`,
                    editUnitName(stagedUnitNames, selected),
                    selectUnit,
                    'radio'
                )(names, getSelectedUnit().toNullable())
                // names.map((name, i) => editUnitName(name, i, elem(name, stagedUnitNames) ? 'enabled' : '', selected === name ? 'selected' : ''))
            );
        });

export const editUiList = (stagedUnitNames: InformationUnitName[]) =>
    DIV(
        { className: 'editor__units' },
        // H2({ className: 'head-title' }, tr.angled('selectData')),
        renderList(stagedUnitNames)(getCurrentUnitNamesByPage())
    );
