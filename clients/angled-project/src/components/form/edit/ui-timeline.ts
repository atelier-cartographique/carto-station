import { DIV } from 'sdi/components/elements';
import tr, { formatDate, Translated } from 'sdi/locale';
import { divTooltipTopRight } from 'sdi/components/tooltip';

import {} from '../../../remote/timeline';
import {
    getUnitHistory,
    getTimelineUnitDateStart,
    getTimelineUnitDateEnd,
    getCurrentTransaction,
    getSelectedUnit,
} from '../../../queries/project';
import { Unit, InformationUnitName } from 'angled-core/ui';
import { some, none } from 'fp-ts/lib/Option';
import { index } from 'fp-ts/lib/Array';
import { unitDisplayName } from 'angled-core/queries/app';

export const setHighlightTimelineItem = (unit: Unit) =>
    getCurrentTransaction()
        .chain(tx =>
            tx.id === unit.transaction ? some('timeline__event--active') : none
        )
        .getOrElse('');

// const onEventClick =
//     (t: TimelineUnit, index: number) => {
//         setCurrentTimelineId(index);
//         setCurrentTimelineTime(Math.ceil(t.created_at) + 100); // it takes some milliseconds to really store the project...
//         loadCurrentProjectAtTime();
//     }

const setEventPosition = (unit: Unit) => {
    const defaultDate: number = unit.created_at;
    const maxDate = getTimelineUnitDateEnd().fold(defaultDate, d => d);
    const minDate = getTimelineUnitDateStart().fold(defaultDate, d => d);
    return (1 - (unit.created_at - minDate) / (maxDate - minDate)) * 100;
};

// const renderEventDate =
//     (id: number, date: string) =>
//         getCurrentTimelineId().map(
//             i => i === id ? DIV({ className: 'timeline__event-date' }, date) : NODISPLAY(),
//         );

const renderEventUnit = (unit: Unit) =>
    DIV(
        {
            className: `timeline__event${setHighlightTimelineItem(unit)}`,
            key: `Event-${index}`,
            // onClick: () => onEventClick(t, index),
            style: { left: setEventPosition(unit) + '%' },
        },
        // renderEventDate(index, formatDate(new Date(unit.created_at))),
        divTooltipTopRight(
            formatDate(new Date(unit.created_at)) as Translated,
            {},
            DIV({
                className: 'timeline__event-pointer',
                // style: { 'zIndex': getUnitHistory().length - index },
            })
        )
    );

const renderEventUnitMissing = (name: InformationUnitName) =>
    DIV({}, `${tr.core('loadingData')}: ${unitDisplayName(name)}`);

// This timeline doesn't work ATM, disabling - pm

export const _uiTimeline = () =>
    getSelectedUnit().map(name =>
        getUnitHistory().fold(renderEventUnitMissing(name), units =>
            DIV(
                'timeline__wrapper',
                DIV('timeline__prehistory'),
                DIV('timeline', ...units.map(renderEventUnit))
            )
        )
    );

export const uiTimeline = () => none;
