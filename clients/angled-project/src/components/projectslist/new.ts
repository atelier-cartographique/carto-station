import { fromPredicate } from 'fp-ts/lib/Option';

import { DIV } from 'sdi/components/elements';
import { attrOptions, inputText } from 'sdi/components/input';
import tr from 'sdi/locale';
import { MessageRecord } from 'sdi/source';
import { isUserLogged, getRoot } from 'sdi/app/queries';

import { makeLabelAndIcon, makeLabel } from '../buttons';
import {
    getNewName,
    getAddProjectHidden,
    getNewNameRecord,
} from '../../queries/project';
import {
    setNewName,
    setAddProjectHidden,
    addNewProject,
} from '../../events/project';

const btnTriggerNew = makeLabelAndIcon('add', 1, 'plus', () =>
    tr.angled('newProjectTitle')
);
const btnCreateNewProject = makeLabelAndIcon('save', 1, 'check', () =>
    tr.angled('saveProject')
);
const btnCancel = makeLabel('cancel', 2, () => tr.core('cancel'));

const setHiddenClass = () => (getAddProjectHidden() ? 'collapsed' : '');

const changeBtnStatus = () => {
    if (isUserLogged()) {
        return getAddProjectHidden() ? '' : 'hidden';
    } else {
        return 'inactive';
    }
};

const canAddProject = () => {
    if (isUserLogged()) {
        return getAddProjectHidden();
    } else {
        const loginUrl = `${getRoot()}login/angled-project`;
        window.location.assign(loginUrl);
        return false;
    }
};

const triggerNew = () =>
    canAddProject() ? setAddProjectHidden(false) : setAddProjectHidden(true);

const onProjectSave = () => {
    addNewProject();
    setAddProjectHidden(true);
    // FIX ME : should trigger profile selection then Editor
};

const cancelNew = () => setAddProjectHidden(true);

const noop = () => void 0;

const checkNameLength = fromPredicate<MessageRecord>(r => {
    const fr = r.fr;
    const nl = r.nl;
    if ((fr && fr.length > 1) || (nl && nl.length > 1)) {
        return true;
    }
    return false;
});

const inputs = () =>
    DIV(
        {},
        inputText(
            attrOptions(
                'project-new-name',
                getNewName('fr'),
                setNewName('fr'),
                {
                    placeholder: tr.angled('insertNameFR'),
                }
            )
        ),
        inputText(
            attrOptions(
                'project-new-name',
                getNewName('nl'),
                setNewName('nl'),
                {
                    placeholder: tr.angled('insertNameNL'),
                }
            )
        )
    );

const saveNewInactive = () =>
    DIV(
        { className: `save-project ${setHiddenClass()}` },
        inputs(),
        btnCreateNewProject(noop, 'inactive'),
        btnCancel(() => cancelNew())
    );

const saveNew = () =>
    DIV(
        { className: `save-project ${setHiddenClass()}` },
        inputs(),
        btnCreateNewProject(onProjectSave),
        btnCancel(() => cancelNew())
    );

const render = () =>
    DIV(
        { className: 'add-project' },
        btnTriggerNew(() => triggerNew(), changeBtnStatus()),
        getNewNameRecord()
            .chain(checkNameLength)
            .fold(saveNewInactive(), () => saveNew())
    );

export default render;
