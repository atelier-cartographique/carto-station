import { DIV } from 'sdi/components/elements';
import { Project } from 'angled-core/ui';
import { getProjects } from '../../queries/app';
import { navigateSelect } from 'angled-project/src/events/route';
import { fromNullable } from 'fp-ts/lib/Option';
import { optRec } from 'sdi/locale';

const renderProject = (p: Project) =>
    DIV(
        {
            className: 'projects-project',
            key: p.id,
            onClick: () => navigateSelect(p.id),
        },
        p.id,
        ' - nom: ',
        fromNullable(p.name).fold('', p => optRec(p.name)('fr').getOrElse('')),
        ' - nova: ',
        fromNullable(p.nova).fold('', n => n.nova),
        ' - capakey: ',
        fromNullable(p.nova).fold('', n => n.nova)
    );

const listProjects = () =>
    DIV({ className: 'projects-list' }, getProjects().map(renderProject));

export const render = () =>
    DIV(
        { className: 'projects-view' },
        'Projects list (for dev only)',
        listProjects()
    );

export default render;
