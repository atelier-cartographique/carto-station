import { DIV, H2, SPAN, NODISPLAY } from 'sdi/components/elements';
import { attrOptions, inputText, renderRadioIn } from 'sdi/components/input';
import { nameToString } from 'sdi/components/button/names';
import tr, { optRec } from 'sdi/locale';
import { getLang } from 'sdi/app/queries';
import { fromNullable, none, some } from 'fp-ts/lib/Option';

import { Project } from 'angled-core/ui';

import { navigateFormNaked } from '../../events/route';
import {
    getSearchInput,
    getFilteredProjects,
    getSearchUnit,
} from '../../queries/project';
import { setSearchInput, setSearchUnit } from '../../events/project';

export type SearchType =
    | 'capakey'
    | 'name'
    | 'nova'
    | 'id'
    | 'description'
    | 'geometry';

const renderRadioLabel = (unit: SearchType) => {
    const attrs = { className: 'radio__label' };
    switch (unit) {
        case 'capakey':
            return DIV(attrs, tr.angled(`labelUnit_capakey`));
        case 'name':
            return DIV(attrs, tr.angled(`labelUnit_name`));
        case 'nova':
            return DIV(attrs, tr.angled(`labelUnit_nova`));
        case 'id':
            return DIV(attrs, tr.angled('projectID'));
        case 'description':
            return DIV(attrs, tr.angled('labelUnit_description'));
        default:
            return NODISPLAY();
    }
};
// const searchRadio = renderRadio<SearchType>(setoidString, renderRadioLabel, setSearchUnit);
const searchRadio = renderRadioIn<SearchType>(
    'select-project-radio',
    renderRadioLabel,
    setSearchUnit,
    'radio'
);

const renderProjectName = (project: Project) =>
    DIV(
        {
            className: 'search-result__project-name',
        },
        fromNullable(project.name).fold('', p =>
            optRec(p.name)(getLang()).getOrElse('')
        )
    );

const renderProjectDescription = (project: Project) =>
    DIV(
        {
            className: 'search-result__project-description',
        },
        fromNullable(project.description).fold('', p =>
            optRec(p.body)(getLang()).getOrElse('')
        )
    );

const renderFoundProjects = (p: Project) =>
    DIV(
        {
            className: 'search-result__list-item',
            key: `FoundProject-${p.id}`,
            onClick: () => navigateFormNaked(p.id),
        },
        renderProjectName(p),
        DIV(
            { className: 'search-result__project-meta' },
            renderProjectDescription(p)
        )
    );

const countFoundProjects = (p: Project[]) => {
    return `${p.length} ${tr.angled('foundProjects', { value: p.length })}`;
};

const noFoundProjects = () => DIV({ className: 'search-result__noprojects' });

const foundProjects = (p: Project[]) =>
    DIV(
        { className: 'search-result' },
        H2({}, tr.angled('chooseResult')),
        DIV({ className: 'search-result__count' }, countFoundProjects(p)),
        DIV({ className: 'search-result__list' }, p.map(renderFoundProjects))
    );

const listFoundProjects = () =>
    getFilteredProjects()
        .chain(ps => (ps.length > 0 ? some(ps) : none))
        .fold(noFoundProjects(), foundProjects);

const searchInput = () =>
    DIV(
        { className: 'search-input-wrapper' },
        SPAN(
            { className: 'search-input-wrapper__picto' },
            nameToString('search')
        ),
        inputText(
            attrOptions('project-search', getSearchInput, setSearchInput, {
                placeholder: tr.angled('searchProjectPlaceholder'),
            })
        )
    );

const btnAndResults = () =>
    DIV(
        { className: 'post-input-wrapper' },
        DIV(
            { className: 'search-btn-list' },
            H2({}, tr.angled('searchBy')),
            searchRadio(
                ['name', 'nova', 'capakey', 'id', 'description'],
                getSearchUnit().getOrElse('name')
            )
        ),
        listFoundProjects()
    );

const render = () =>
    DIV({ className: '_project-search' }, searchInput(), btnAndResults());

export default render;
