import { DIV, H1 } from 'sdi/components/elements';

import renderNewProject from './new';
import renderSearchProject from './search';
import renderMap from '../map';
import tr from 'sdi/locale';
// import { stringToParagraphs } from 'sdi/util';
import { markdown } from 'sdi/ports/marked';

const appInfos = () =>
    DIV(
        { className: 'app-infos' },
        markdown(tr.angled('appDescriptionProjectsList'))
    );

const wrapperRight = () =>
    DIV(
        { className: 'wrapper wrapper--right' },
        appInfos(),
        renderSearchProject()
    );

const wrapperLeft = () =>
    DIV(
        { className: 'wrapper wrapper--left' },
        H1({}, tr.angled('appName')),
        renderNewProject(),
        renderMap()
    );

export const render = () =>
    DIV({ className: 'project-list' }, wrapperLeft(), wrapperRight());

export default render;
