import {
    getDisplayUnreadnotification,
    getNotificationsRemote,
} from 'angled-core/queries/sub';
import { DIV, NodeOrOptional, SPAN } from 'sdi/components/elements';
import { nameToString } from 'sdi/components/button/names';
import { openNotifier } from 'angled-project/src/events/modal';
import tr from 'sdi/locale';
import { foldRemote } from 'sdi/source';
import { Notif } from 'angled-core/sub';
import { fromPredicate } from 'fp-ts/lib/Option';

const initNotifications = () => DIV('app-listwrapper', 'init');

const loadNotifications = () => DIV('app-listwrapper loader-spinner');

const errorNotifications = () =>
    DIV('app-listwrapper', 'error loading notifications');

const withUnread = fromPredicate<unknown[]>(xs => xs.length > 0);

const notificationsLoaded = () => {
    return DIV(
        { className: 'notifier  app-listwrapper', onClick: openNotifier },
        SPAN('notifier__unread', nameToString('circle')),
        SPAN(
            'notifier__label',
            tr.angled('notifications'),
            withUnread(getDisplayUnreadnotification()).map(
                xs => `(${xs.length})`
            )
        )
    );
};

const remoteProcessor = foldRemote<Notif[], string, NodeOrOptional>(
    initNotifications,
    loadNotifications,
    errorNotifications,
    notificationsLoaded
);

export const renderNotifier = () => remoteProcessor(getNotificationsRemote());

export default renderNotifier;
