import * as debug from 'debug';
import { init } from 'sdi/components/modal';

const logger = debug('sdi:project/modal');

export const { render, register } = init();
export default render;

export { render as renderSaveFinalStep } from './save-final-step';
export { render as renderNotifier } from './notifier';
export { render as renderAudience } from './audience';
export { render as renderTag } from './tag';

logger('loaded');
