import { makeLabel } from '../buttons';
import tr from 'sdi/locale';
import { H2, DIV } from 'sdi/components/elements';
import { getModalAudienceUnit } from 'angled-project/src/queries/modal';
import { renderSelectAudiences } from '../audience/index';

const closeModalButton = makeLabel('close', 2, () => tr.core('close'));

const header = () => H2({}, tr.angled('manageAudiences'));

const footer = (close: () => void) =>
    DIV(
        'modal__footer__inner',
        // confirmButton(() => {
        //     close();
        //     getCurrentProjectId().map(p =>
        //         activityLogger(saveUnitsAction(p, getStagedUnits().length))
        //     );
        //     pushStaged();
        // }),
        closeModalButton(close)
    );

const body = () => [getModalAudienceUnit().map(renderSelectAudiences)];

export const render = { header, footer, body };
