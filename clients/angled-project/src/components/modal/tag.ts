import { makeLabel } from '../buttons';
import tr, { fromRecord } from 'sdi/locale';
import { H2, DIV } from 'sdi/components/elements';
import { Tag } from 'angled-core/ref';
import { Unit } from 'angled-core/ui';
import {
    addTagToUnit,
    removeTagFromUnit,
} from 'angled-project/src/events/project';
import { renderMultiSelect } from 'sdi/components/input';
import { setoidIdentified } from 'sdi/util';
import { getModalTagUnit } from 'angled-project/src/queries/modal';
import { getTagsForUnit, getTags } from 'angled-project/src/queries/project';

const closeModalButton = makeLabel('close', 2, () => tr.core('close'));

const header = () => H2({}, tr.angled('manageTags'));

const footer = (close: () => void) =>
    DIV('modal__footer__inner', closeModalButton(close));

const renderTagItem = (t: Tag) => DIV('tag-item', fromRecord(t.label));
const renderTagSelect = (unitData: Unit | Unit[]) =>
    renderMultiSelect<Tag>(
        setoidIdentified<Tag>(),
        renderTagItem,
        addTagToUnit(unitData),
        removeTagFromUnit(unitData)
    )(getTags(), getTagsForUnit(unitData));

const body = () => [getModalTagUnit().map(renderTagSelect)];

export const render = { header, footer, body };
