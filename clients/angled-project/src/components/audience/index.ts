import { DIV } from 'sdi/components/elements';
import { Unit } from 'angled-core/ui';
import { renderMultiSelect } from 'sdi/components/input';
import { setoidIdentified } from 'sdi/util';
import { fromRecord } from 'sdi/locale';
import { Audience } from 'angled-core/ref';
import {
    getAudiencesForUnit,
    getAudiencesForStaged,
} from 'angled-project/src/queries/project';
import { getAudienceList } from 'angled-core/queries/ref';
import {
    addAudienceToUnit,
    removeAudienceFromUnit,
    addAudienceToStaged,
    removeAudienceFromStaged,
} from 'angled-project/src/events/project';

const renderAudience = (audience: Audience) =>
    DIV({ className: 'audience--item' }, fromRecord(audience.name));

const renderSelect = (unitData: Unit | Unit[]) =>
    renderMultiSelect<Audience>(
        setoidIdentified(),
        renderAudience,
        addAudienceToUnit(unitData),
        removeAudienceFromUnit(unitData)
    );

export const renderSelectAudiences = (unitData: Unit | Unit[]) =>
    renderSelect(unitData)(getAudienceList(), getAudiencesForUnit(unitData));

const renderSelectPre = () =>
    renderMultiSelect<Audience>(
        setoidIdentified(),
        renderAudience,
        addAudienceToStaged,
        removeAudienceFromStaged
    );

export const renderSelectAudiencesPre = () =>
    renderSelectPre()(getAudienceList(), getAudiencesForStaged());
