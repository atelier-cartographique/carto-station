import { DIV, SPAN } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { fromNullable } from 'fp-ts/lib/Option';
import { nameToString } from 'sdi/components/button/names';

import { Project, InformationUnitName } from 'angled-core/ui';
// import { Profile } from 'angled-core/profile';
import { getSelectedUnit } from '../queries/project';

const appName = () => SPAN({}, tr.angled('appName'));
const projectName = (pj: Project) =>
    SPAN(
        {},
        fromNullable(pj.name).fold<string>('', u => fromRecord(u.name))
    );
// const profileName = (p: Profile) => SPAN({}, p.name);
const separator = () =>
    SPAN('separator', nameToString('caret-right'));

// export const breadcrumbProjectView =
//     (p: Profile, pj: Project) =>
//         DIV({ className: 'breadcrumb' },
//             appName(),
//             separator(),
//             projectName(pj),
//             separator(),
//             profileName(p),
//         );

export const breadcrumbProjectEdit = (pj: Project) =>
    getSelectedUnit().foldL(
        () => breadcrumbProjectEditWithoutIUnit(pj),
        unitName => breadcrumbProjectEditWithIUnit(pj, unitName)
    );

export const breadcrumbProjectEditWithoutIUnit = (pj: Project) =>
    DIV(
        { className: 'breadcrumb' },
        appName(),
        separator(),
        projectName(pj),
        separator(),
        tr.angled('selectUnitToEdit')
    );

const capitalize = (s: string) => s.charAt(0).toUpperCase() + s.slice(1); // todo fixme à déplacer dans utils ?

export const breadcrumbProjectEditWithIUnit = (
    pj: Project,
    unitName: InformationUnitName
) =>
    DIV(
        { className: 'breadcrumb' },
        tr.angled('appName'),
        separator(),
        projectName(pj),
        separator(),
        capitalize(unitName)
    );

export const breadcrumbReview = (pj: Project) =>
    DIV(
        { className: 'breadcrumb' },
        tr.angled('appName'),
        separator(),
        projectName(pj),
        separator(),
        tr.angled('reviewToPublish')
    );
