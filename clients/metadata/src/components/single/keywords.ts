import { BUTTON, DETAILS, DIV, SPAN, SUMMARY } from 'sdi/components/elements';
import {
    getKeywordList,
    getKeywords,
    getKeywordSearch,
    // getKeywordList,
    isSelectedKeyword,
} from '../../queries/metadata';
import {
    removeKeyword,
    addKeyword,
    setKeywordSearch,
} from '../../events/metadata';
import tr from 'sdi/locale';
import { inputText } from 'sdi/components/input';
import { fromRecord } from 'sdi/locale';
import { makeIcon } from 'sdi/components/button';
import { isSelectOpen } from 'sdi/app';

// const onRowSelect: SelectRowHandler = row => {
//     const id = row.from as string;
//     const isSelected = isSelectedKeyword(id);
//     if (isSelected) {
//         removeKeyword(id);
//     } else {
//         addKeyword(id);
//     }
//     setLayout('Single');
// };

// const tq = queryK('component/table/keywords');
// const te = dispatchK('component/table/keywords');

// const base = baseTable(tableQueries(tq, getKeywordsSource), tableEvents(te));

// const renderTable = base({
//     className: 'keyword-select-wrapper',
//     onRowSelect,
// });

const filter = () =>
    inputText({
        key: 'keyword-search-input',
        get: getKeywordSearch,
        set: k => setKeywordSearch(k.toLocaleLowerCase()),
        attrs: {
            id: 'keyword-search',
            placeholder: tr.core('filter'),
        },
        monitor: e => setKeywordSearch(e.toLocaleLowerCase()),
    });

const removeBtn = makeIcon('cancel', 3, 'times', {
    text: () => tr.core('clear'),
    position: 'bottom-right',
});

export const renderSelectedKeywords = () =>
    DIV(
        'tag__list',
        ...getKeywords().map(kw =>
            DIV(
                { className: 'tag' },
                SPAN({ className: 'tag__value' }, fromRecord(kw.name)),
                removeBtn(() => removeKeyword(kw.id))
            )
        )
    );

export const renderSelectKeyword = () => {
    // const choice =
    //     getKeywordList()
    //         .filter(kw => !isSelectedKeyword(kw.id))
    //         .map(k => DIV({
    //             key: k.id,
    //             onClick: () => isSelectedKeyword(k.id) ? removeKeyword(k.id) : addKeyword(k.id),
    //         }, fromRecord(k.name)));

    // return DIV({ className: 'keywords-wrapper' }, renderTable());

    return DIV(
        {},
        DETAILS(
            {
                className: `select-filter__wrapper`,
                open: isSelectOpen('select-metadata-keyword'),
            },
            SUMMARY('', tr.meta('add')),
            DIV(
                'select-tail__wrapper',
                filter(),
                DIV(
                    'tail',
                    ...getKeywordList()
                        .filter(kw => !isSelectedKeyword(kw.id))
                        .filter(kw =>
                            fromRecord(kw.name)
                                .toLocaleLowerCase()
                                .includes(getKeywordSearch())
                        )
                        .sort((a, b) =>
                            fromRecord(a.name).localeCompare(fromRecord(b.name))
                        )
                        .map(k =>
                            BUTTON(
                                {
                                    className: 'tail-item',
                                    key: k.id,
                                    onClick: () => addKeyword(k.id),
                                    // isSelectedKeyword(k.id)
                                    //     ? removeKeyword(k.id)
                                    //     : addKeyword(k.id),
                                },
                                SPAN('item__control'),
                                DIV('item__label', fromRecord(k.name))
                            )
                        )
                )
            )
        )
    );
};
