import { DIV, H1 } from 'sdi/components/elements';
import tr from 'sdi/locale';

const render = () =>
    DIV({}, H1({}, tr.meta('metadata')), DIV({}, tr.meta('loadingData')));

export default render;
