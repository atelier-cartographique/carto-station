
import { makeRecord } from 'sdi/source';
import { fromRecord } from 'sdi/locale';

export const attribution = () => 'CartoStation';

export const credits = () => fromRecord(makeRecord(
    'Fond de plan: Brussels UrbIS ®© - CIRB - CIBG',
    'Achtergrond: Brussels UrbIS ®© - CIRB - CIBG'))