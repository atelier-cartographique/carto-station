import { Nullable } from 'sdi/util';
import { PartialForm, Form } from '../components/edit';

// State Augmentation
declare module 'sdi/shape' {
    export interface IShape {
        'universe/domain-id': Nullable<number>;
        'universe/term-id': Nullable<number>;
        'universe/form': Nullable<Form>;
        'universe/form-new': Nullable<PartialForm>;
    }
}
