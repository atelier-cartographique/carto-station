// imports from sdi
import { Collection } from 'sdi/util';

export type AppLayout =
    | 'splash'
    | 'domain-list'
    | 'domain-select'
    | 'domain-form'
    | 'term-select'
    | 'term-form';

// State Augmentation
declare module 'sdi/shape' {
    export interface IShape {
        'app/layout': AppLayout;
        'app/route': string[];

        'component/foldable': Collection<boolean>;
    }
}
