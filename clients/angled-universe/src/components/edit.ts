import { getForm } from '../queries/universe';
import { some, none, fromNullable } from 'fp-ts/lib/Option';
import { MessageRecordLang } from 'sdi/source';
import { optRec } from 'sdi/locale';
import { Term, Domain } from 'angled-core/ref';

export type Editor = 'term' | 'domain';

// const saveButton = makeLabelAndIcon('save', 1, 'floppy-o', 'save');
// const closeButton = makeLabelAndIcon('close', 2, 'times', 'close');

export interface BaseForm {
    kind: Editor;
    isDirty: boolean;
    isNew: boolean;
}

export interface TermForm extends BaseForm {
    kind: 'term';
    isNew: false;
    data: Term;
}

export interface PartialTermForm extends BaseForm {
    kind: 'term';
    isNew: true;
    data: Partial<Term>;
}

export interface DomainForm extends BaseForm {
    kind: 'domain';
    isNew: false;
    data: Domain;
}

export interface PartialDomainForm extends BaseForm {
    kind: 'domain';
    isNew: true;
    data: Partial<Domain>;
}

export type Form = TermForm | DomainForm;
export type PartialForm = PartialTermForm | PartialDomainForm;

export const asDomainForm = (f: Form) => (f.kind === 'domain' ? some(f) : none);

export const asTermForm = (f: Form) => (f.kind === 'term' ? some(f) : none);

export const asPartialDomainForm = (f: PartialForm) =>
    f.kind === 'domain' ? some(f) : none;

export const asPartialTermForm = (f: PartialForm) =>
    f.kind === 'term' ? some(f) : none;

export const nameGetter = (e: Editor) => (l: MessageRecordLang) =>
    getForm(e).chain(f => optRec(f.data.name)(l));

export const descriptionGetter = (e: Editor) => (l: MessageRecordLang) =>
    getForm(e)
        .chain(f => fromNullable(f.data.description))
        .chain(d => optRec(d)(l));

// const domainName =
//     (ln: MessageRecordLang) =>
//         inputText(
//             () => nameGetter('domain')(ln).fold('', s => s),
//             (s: string) => updateDomainForm(lens => lens(['name', ln]).set(s)),
//         );

// const termName =
//     (ln: MessageRecordLang) =>
//         inputText(
//             () => nameGetter('term')(ln).fold('', s => s),
//             (s: string) => updateTermForm(lens => lens(['name', ln]).set(s)),
//         );

// const renderFormTerm =
//     () =>
//         DIV({ className: 'inner-form' },
//             termName('fr'),
//             termName('nl'),
//             saveButton(() => {
//                 saveTerm();
//                 setLayout('term-list');
//             }),
//             closeButton(() => setLayout('term-list')),
//         );

// const renderTerm =
//     () =>
//         DIV({ className: 'term-form' },
//             getForm('term')
//                 .fold(
//                     DIV({}, 'ERROR'),
//                     renderFormTerm,
//                 ));
