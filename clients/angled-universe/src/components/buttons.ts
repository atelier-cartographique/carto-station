// import { makeIcon, makeLabel, makeLabelAndIcon } from 'sdi/components/button';

import { isUserLogged, getRoot } from 'sdi/app/queries';
import { getPathElements } from 'sdi/util';

export const isActiveButton = () => (isUserLogged() ? '' : 'inactive');

export const goToLoginPage = () => {
    const path = getPathElements(document.location.pathname);
    const root = getPathElements(getRoot());
    const next = path
        .filter((p, i) => (i < root.length ? p !== root[i] : true))
        .join('/');
    const loginUrl = `${getRoot()}login/${next}`;
    window.location.assign(loginUrl);
};
