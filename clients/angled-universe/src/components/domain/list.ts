import { fromNullable } from 'fp-ts/lib/Option';

import { DIV, H2 } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { isUserLogged } from 'sdi/app/queries';
import { makeLabelAndIcon } from 'sdi/components/button';

import { Domain } from 'angled-core/ref';
import { getCurrentDomain } from 'angled-universe/src/queries/universe';
import {
    navigateDomain,
    navigateDomainNew,
} from 'angled-universe/src/events/route';
import { clearForm } from 'angled-universe/src/events/universe';
import { isActiveButton, goToLoginPage } from '../buttons';
import { DomainHelp } from './help';
import { getDomainList } from 'angled-core/queries/universe';

const createButton = makeLabelAndIcon('add', 2, 'plus', () =>
    tr.angled('addDomain')
);

const onPushButton = () => {
    if (isUserLogged()) {
        clearForm();
        navigateDomainNew();
    } else {
        goToLoginPage();
    }
};

const renderCreateButton = () =>
    createButton(() => onPushButton(), isActiveButton());

const isActive = (d: Domain) =>
    getCurrentDomain().fold(false, c => d.id === c.id);

const domainClass = (d: Domain) =>
    isActive(d) ? 'list-item domain active' : 'list-item domain';

const renderDomain = (d: Domain) =>
    DIV(
        {
            className: domainClass(d),
            onClick: () => navigateDomain(d.id),
            title: `id : ${d.id}`,
        },
        DIV('list-item-label interactive', fromRecord(d.name)),
        fromNullable(d.description).map(desc =>
            DIV('description', fromRecord(desc))
        )
    );

// tslint:disable-next-line: variable-name
export const DomainList = () =>
    DIV(
        'panel',
        DomainHelp(),
        H2({}, tr.angled('domains'), renderCreateButton()),
        DIV('list domain-list', ...getDomainList().map(renderDomain))
    );
