import { fromNullable } from 'fp-ts/lib/Option';

import { DIV, H2, NODISPLAY } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { isUserLogged } from 'sdi/app/queries';
import { makeIcon, makeLabelAndIcon } from 'sdi/components/button';

import { renderMovable } from 'sdi/components/movable';
import {
    getCurrentDomain,
    getCurrentTerm,
} from 'angled-universe/src/queries/universe';
import {
    navigateTerm,
    navigateTermForm,
    navigateTermNew,
} from 'angled-universe/src/events/route';
import {
    clearForm,
    swapTermsDown,
    swapTermsUp,
} from 'angled-universe/src/events/universe';
import { termsInDomain } from 'angled-core/queries/universe';
import { Term, Domain } from 'angled-core/ref';

import { isActiveButton, goToLoginPage } from '../buttons';
import { DomainDetail } from '../domain';

const createButton = makeLabelAndIcon('add', 2, 'plus', () =>
    tr.angled('addTerm')
);

const onPushButton = () => {
    if (isUserLogged()) {
        clearForm();
        navigateTermNew();
    } else {
        goToLoginPage();
    }
};

const renderCreateButton = () =>
    createButton(() => onPushButton(), isActiveButton());

const isActive = (t: Term) => getCurrentTerm().fold(false, c => t.id === c.id);

const termClass = (t: Term) =>
    isActive(t) ? 'list-item term active' : 'list-item  term';

const renderMoveTerms = renderMovable(swapTermsUp, swapTermsDown);

const editTerm = makeIcon('edit', 3, 'pencil-alt', {
    position: 'left',
    text: () => tr.core('edit'),
});

const onPushEditButton = (id: number) => {
    if (isUserLogged()) {
        navigateTermForm(id);
    } else {
        goToLoginPage();
    }
};

const renderTerm = (t: Term, i: number) =>
    DIV(
        {
            className: termClass(t),
            onClick: () => navigateTerm(t.id),
            title: `id : ${t.id}`,
        },

        DIV(
            'term-header',
            DIV('list-item-label interactive', fromRecord(t.name)),
            DIV(
                'actions__wrapper',
                renderMoveTerms(i, termsInDomain(t.domain).length),
                editTerm(() => onPushEditButton(t.id), isActiveButton())
            )
        ),
        DIV(
            'term__wrapper',
            fromNullable(t.description).map(desc =>
                DIV('description', fromRecord(desc))
            )
        )
    );

const renderTermList = (domain: Domain) =>
    DIV('list term-list', ...termsInDomain(domain.id).map(renderTerm));

// tslint:disable-next-line: variable-name
export const TermList = () =>
    getCurrentDomain().fold(NODISPLAY(), domain =>
        DIV(
            'panel',
            DomainDetail(),
            H2({}, tr.angled('terms'), renderCreateButton()),
            renderTermList(domain)
        )
    );
