import { fromNullable } from 'fp-ts/lib/Option';

import { DIV, H1, NODISPLAY, SPAN } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { stringToParagraphs } from 'sdi/util';
import { isUserLogged } from 'sdi/app/queries';
import { makeIcon } from 'sdi/components/button';

import {
    getCurrentTerm,
    getCurrentDomain,
} from 'angled-universe/src/queries/universe';
import {
    navigateTermForm,
    navigateDomain,
} from 'angled-universe/src/events/route';
import { Term } from 'angled-core/ref';

import { isActiveButton, goToLoginPage } from '../buttons';

// const editButton = makeLabelAndIcon('edit', 1, 'pencil-alt', () =>
//     tr.angled('editTerm')
// );

const editButton = makeIcon('edit', 3, 'pencil-alt', {
    position: 'left',
    text: () => tr.angled('editTerm'),
});

const closeButton = makeIcon('close', 3, 'times', {
    position: 'left',
    text: () => tr.core('close'),
});

const onPushButton = (id: number) => {
    if (isUserLogged()) {
        navigateTermForm(id);
    } else {
        goToLoginPage();
    }
};

const renderEditButton = (t: Term) =>
    editButton(() => onPushButton(t.id), isActiveButton());

const renderName = (term: Term) => DIV('name', fromRecord(term.name));

const renderDescription = (term: Term) =>
    DIV(
        'description',
        fromNullable(term.description).fold(
            DIV('description', tr.angled('noDescription')),
            desc =>
                DIV(
                    'description',
                    stringToParagraphs(
                        tr.angled('detailDescriptionLabel') + fromRecord(desc)
                    )
                )
        )
    );

const termHeader = (term: Term) =>
    DIV(
        'term-header',
        SPAN('term-id', `${tr.angled('term')} id : ${term.id}`),
        renderEditButton(term),
        closeButton(() => getCurrentDomain().map(d => navigateDomain(d.id)))
    );

// tslint:disable-next-line: variable-name
export const TermDetail = () =>
    getCurrentTerm().fold(NODISPLAY(), term =>
        DIV(
            'panel',
            DIV(
                'detail term',
                termHeader(term),
                H1({}, renderName(term)),
                renderDescription(term)
            )
        )
    );
