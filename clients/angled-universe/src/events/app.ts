import { assign, dispatch } from 'sdi/shape';

import { AppLayout } from '../shape';

export const setLayout = (l: AppLayout) => assign('app/layout', l);

export const unfold = (name: string) =>
    dispatch('component/foldable', folds => ({ ...folds, [name]: false }));

export const fold = (name: string) =>
    dispatch('component/foldable', folds => ({ ...folds, [name]: true }));
