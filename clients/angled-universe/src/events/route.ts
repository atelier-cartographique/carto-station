import * as debug from 'debug';

import { index } from 'fp-ts/lib/Array';
import { Option } from 'fp-ts/lib/Option';
import { literal, union, TypeOf } from 'io-ts';

import { Router, Path } from 'sdi/router';
import { getNumber } from 'sdi/util';

import { setLayout } from './app';
import {
    setDomain,
    setTerm,
    createDomainForm,
    setForm,
    createTermForm,
} from './universe';
import { getCurrentDomain } from '../queries/universe';

const logger = debug('sdi:route');

const RouteIO = union([
    literal('index'),
    literal('domain'),
    literal('term'),
    literal('domain-form'),
    literal('term-form'),
]);
type Route = TypeOf<typeof RouteIO>;

const { home, route, navigate } = Router<Route>('angled-universe');

const domainParser = (p: Path) => index(0, p).chain(getNumber);

const termParser = (p: Path) => index(0, p).chain(getNumber);

type TermFormParserResult = [number, Option<number>];
const termFormParser = (p: Path): Option<TermFormParserResult> =>
    index(0, p)
        .chain(getNumber)
        .map<TermFormParserResult>(formId => [
            formId,
            index(1, p).chain(getNumber),
        ]);

// {
//     const base = prefixParser('term-form')(p)
//         .let('did', index(1, p).chain(getNumber));
//     const termId = index(2, p).chain(getNumber);

//     return base.fold(
//         left(null),
//         (scope) => {
//             const result: TermFormParserResult = [scope.did, termId];
//             return right(result);
//         });

// };

home('index', _p => {
    setLayout('domain-list');
});

route(
    'domain',
    r =>
        r.foldL(
            () => logger('failed on parser for domain', r),
            id => {
                setDomain(id);
                setLayout('domain-select');
            }
        ),
    domainParser
);

route(
    'term',
    r =>
        r.map(id => {
            setTerm(id);
            setLayout('term-select');
        }),
    termParser
);

route(
    'domain-form',
    r =>
        r.foldL(
            () => {
                createDomainForm();
                setLayout('domain-form');
            },
            id => {
                setDomain(id);
                setForm('domain')(id);
                setLayout('domain-form');
            }
        ),
    domainParser
);

route(
    'term-form',
    e =>
        e.map(([did, optTid]) =>
            optTid.foldL(
                () => {
                    setDomain(did);
                    createTermForm(did);
                    setLayout('term-form');
                },
                tid => {
                    setDomain(did);
                    setTerm(tid);
                    setForm('term')(tid);
                    setLayout('term-form');
                }
            )
        ),
    termFormParser
);

export const loadRoute = (initial: string[]) =>
    index(0, initial).map(prefix =>
        RouteIO.decode(prefix).map(c => navigate(c, initial.slice(1)))
    );

export const navigateHome = () => navigate('index', []);

export const navigateDomain = (id: number) => navigate('domain', [id]);

export const navigateTerm = (id: number) => navigate('term', [id]);

export const navigateDomainForm = (id: number) => navigate('domain-form', [id]);

export const navigateTermForm = (id: number) =>
    getCurrentDomain().map(d => navigate('term-form', [d.id, id]));

export const navigateDomainNew = () => navigate('domain-form', []);

export const navigateTermNew = () =>
    getCurrentDomain().map(d => navigate('term-form', [d.id]));

logger('loaded');
