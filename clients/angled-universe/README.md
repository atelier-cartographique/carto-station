![illustration](../assets/anim/angled-term.gif)

This application allows you to create and define Terms and Domains, which are used among the information units to be encoded in the application _angled-project_.

A domain is the equivalent of a category, while the terms of a domain are the possible qualifiers within that particular category.

This application, which is essential for the _angled-project_ application, is very textual and technically very simple: it is a matter of describing and defining a thesaurus for qualifying urban plans and projects.

As the notions at work are moving and evolving, this application gives the ability to the operators to build and update the whole semantics underlying their information system.

This freedom comes with the responsibility of structuring and defining terms collectively, in order to progressively consolidate a common language.

## User guide

The complete user guide is available here : https://cartostation.com/documentation
