/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { Option, none, some } from 'fp-ts/lib/Option';

import { makeTable2Manip } from 'sdi/components/table2/layer';

import {
    FeatureCollection,
    Inspire,
    StreamingRequestSort,
    StreamingRequestFilter,
    hashRequestFilter,
    StreamingState,
} from 'sdi/source';
import { queryK, query, dispatchK } from 'sdi/shape';
import { isNotNullNorUndefined } from 'sdi/util';
import { TableWindow } from 'sdi/components/table2';

import {
    getCurrentLayerInfo,
    getLayerData,
    getSyntheticLayerInfoOption,
} from './app';
import { updateLayer } from '../events/table';

const logger = debug('sdi:queries/table');

// Layer / FeatureCollection

export const getLayer = (): Option<FeatureCollection> =>
    getCurrentLayerInfo().chain(({ metadata }) =>
        getLayerData(metadata.uniqueResourceIdentifier).getOrElse(none)
    );

export const getLayerOption = () => getLayer();

export const hashWindow = (
    w: TableWindow,
    s: Option<StreamingRequestSort>,
    f: StreamingRequestFilter[]
) => {
    let hash = `${w.offset}-${w.size}`;

    s.map(s => {
        hash += `-${s.column}/${s.direction}`;
    });
    f.forEach(filter => (hash += `-${hashRequestFilter(filter)}`));

    return hash;
};

export const getStreamingUrl = (metadata: Inspire) =>
    isNotNullNorUndefined(metadata.dataStreamUrl)
        ? some(metadata.dataStreamUrl)
        : none;

export const getStream: () => StreamingState = () =>
    query('data/features/stream');

export const tableQuery = queryK('component/table');

export type StreamLoadingStatus = 'no-stream' | 'loading' | 'loaded' | 'error';

export const { getFeatureData, getLayerSource, streamLoadingStatus } =
    makeTable2Manip(
        getCurrentLayerInfo,
        getSyntheticLayerInfoOption,
        getLayerData,
        () => none,
        queryK('component/table'),
        queryK('data/features/stream'),
        () => none,
        dispatchK('data/features/stream'),
        updateLayer,
        false
    );

logger('loaded');
