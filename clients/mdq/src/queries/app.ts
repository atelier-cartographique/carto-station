/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import {
    fromNullable,
    fromPredicate,
    Option,
    fromEither,
    some,
    none,
} from 'fp-ts/lib/Option';
// import { left, right, Either } from 'fp-ts/lib/Either';

import { query, queryK } from 'sdi/shape';
import {
    Feature,
    FeatureCollection,
    ILayerInfo,
    IMapInfo,
    Inspire,
    PointInterval,
    PolygonInterval,
    RemoteResource,
    remoteToOption,
    StyleConfig,
} from 'sdi/source';
// import { SyntheticLayerInfo } from 'sdi/app';
import { scopeOption } from 'sdi/lib';
// import { fromRecord } from 'sdi/locale';
import { DataEntry, DataLegend, DataSet, ExtraLayerInfo } from '../remote';
import { getLang, SyntheticLayerInfo } from 'sdi/app';
import {
    Adjust,
    barAdjustValues,
    BarchartEntry,
    plotHeight,
    PlotTableSort,
    TableEntry,
} from '../components/plot';
import { array, catOptions, index } from 'fp-ts/lib/Array';
import { getFeaturePropOption, makeNumberList, max, minAndMax } from 'sdi/util';
import { MessageRecordIO } from 'sdi/source/io/message';
import { concat, formatNumber, fromRecord } from 'sdi/locale';
import { nameToCode } from 'sdi/components/button/names';
import { isLayerSelected } from './map';
import { identity } from 'io-ts';

export const mapReady = () => {
    return query('app/map-ready');
};

export const getLayout = () => {
    const ll = query('app/layout');
    if (ll.length === 0) {
        throw new Error('PoppingEmptyLayoutList');
    }
    return ll[ll.length - 1];
};

// export const getLayerData =
//     (ressourceId: string): Either<string, Option<FeatureCollection>> => {
//         const layers = query('data/layers');
//         const errors = query('remote/errors');
//         if (ressourceId in layers) {
//             return right(some<FeatureCollection>(layers[ressourceId]));
//         }
//         else if (ressourceId in errors) {
//             return left(errors[ressourceId]);
//         }
//         return right(none);
//     };

// export const getLayerDataFromInfo =
//     (layerId: string): Option<FeatureCollection> =>
//         scopeOption()
//             .let('mid', fromNullable(query('app/current-map')))
//             .let('mapInfo', ({ mid }) => fromNullable(query('data/maps').find(m => m.id === mid)))
//             .let('layerInfo', ({ mapInfo }) => fromNullable(mapInfo.layers.find(l => l.id === layerId)))
//             .let('metadata', ({ layerInfo }) => getDatasetMetadataOption(layerInfo.metadataId))
//             .let('data', ({ metadata }) => fromNullable(query('data/layers')[metadata.uniqueResourceIdentifier]))
//             .pick('data');

// export const getMap =
//     (mid: string) => {
//         const maps = query('data/maps');
//         return maps.find(m => m.id === mid);
//     };

// export const getDatasetMetadata =
//     (id: string) => {
//         const collection = query('data/datasetMetadata');
//         if (id in collection) {
//             return collection[id];
//         }
//         return null;
//     };

// export const getSyntheticLayerInfoOption =
//     (layerId: string): Option<SyntheticLayerInfo> =>
//         scopeOption()
//             .let('mid', fromNullable(query('app/current-map')))
//             .let('info', ({ mid }) => fromNullable(query('data/maps').find(m => m.id === mid)))
//             .let('layerInfo', ({ info }) => fromNullable(info.layers.find(l => l.id === layerId)))
//             .let('metadata', ({ layerInfo }) => getDatasetMetadataOption(layerInfo.metadataId))
//             .map(({ layerInfo, metadata }) => ({
//                 name: getMessageRecord(metadata.resourceTitle),
//                 info: layerInfo,
//                 metadata
//             }));

// export const getSyntheticLayerInfo = getSyntheticLayerInfoOption;

export const getCurrentMap = () => query('app/current-map');

export const getCurrentLayer = () => getCurrentLayerOpt().toNullable();

export const getCurrentLayerOpt = () =>
    getDataSet('main').map(({ id }) => id.toString());

// export const getCurrentLayerInfoOption =
//     (): Option<SyntheticLayerInfo> =>
//         fromNullable(query('app/current-layer'))
//             .chain<SyntheticLayerInfo>(getSyntheticLayerInfoOption);

// export const getCurrentLayerInfo = getCurrentLayerInfoOption;

// export const getCurrentMetadata =
//     () => getCurrentLayerInfo().map(i => i.metadata);
// export const getCurrentInfo =
//     () => getCurrentLayerInfo().map<ILayerInfo>(i => i.info);
// export const getCurrentName =
//     () => getCurrentLayerInfo().map<string>(i => fromRecord(i.name));

export const getSelectedFeatures = () => query('app/current-feature');

export const getSelectedFeaturesAsFeatures = () => {
    const ids = getSelectedFeatures();
    const filter = (f: Feature) => ids.indexOf(f.id as number) >= 0;
    return getLayerDataOption('main', 'visible')
        .map(({ features }) => features.filter(filter))
        .getOrElse([]);
};

export const getCurrentFeature = () =>
    index(0, getSelectedFeatures() as number[]).toNullable();

export const getCurrentBaseLayerName = () => query('app/current-baselayer');

export const gteBaseLayer = (id: string | null) => {
    const [serviceId, layerName] = fromNullable(id)
        .map(i => i.split('/'))
        .getOrElse(['', '']);
    const service = query('data/baselayers').find(s => s.id === serviceId);
    if (service) {
        return service.layers.find(l => l.codename === layerName) ?? null;
    }
    return null;
};

export const getCurrentBaseLayer = () => {
    const name = getCurrentBaseLayerName();
    return fromNullable(gteBaseLayer(name)).map(array.of).getOrElse([]);
};

export const getBaseLayerServices = () =>
    query('data/baselayers').map(s => s.id);

export const getBaseLayersForService = (serviceId: string) =>
    fromNullable(query('data/baselayers').find(s => s.id === serviceId))
        .map(s => s.layers)
        .getOrElse([]);

const makeinterval = (legend: DataLegend): PolygonInterval => ({
    low: legend.low,
    high: legend.high,
    fillColor: legend.color,
    strokeWidth: 0.5,
    strokeColor: 'grey',
    label: legend.label,
});

// const makeExrainterval = (legend: DataLegend, idx: number): PointInterval => ({
//     low: legend.low,
//     high: legend.high,
//     label: legend.label,
//     marker: {
//         codePoint: nameToCode('circle'),
//         color: 'red',
//         size: (idx + 1) * 7,
//     },
// });

const primaryStyle = (data: DataSet): StyleConfig => ({
    kind: 'polygon-continuous',
    propName: 'value',
    intervals: data.legend.map(makeinterval),
});

// const SECONDARY_LEGEND_MIN = 0;
const SECONDARY_LEGEND_MAX = 32;

const SCALES = [
    10, 100, 1000, 5000, 10000, 15000, 20000, 30000, 50000, 100000, 200000,
    500000, 1000000, 2000000,
];

const findScale = (max: number) => SCALES.find(scale => max < scale) || 2000000;

const makeSteps = (max: number) => {
    const scale = findScale(max);
    const step = scale / 5;
    const steps: [number, number, number][] = [];
    for (let i = 1; i <= 5; i += 1) {
        steps.push([(i - 1) * step, i * step, i * 6]);
    }
    return { scale, steps };
};

const areaSizeMax = (maxValue: number, n: number) =>
    2 * ((SECONDARY_LEGEND_MAX / 2) * Math.sqrt(n / maxValue));

const maxDataSet = (data: DataSet) =>
    max(
        data.entries
            .filter(e => e.flag === 'NN' && e.value > 0)
            .map(e => e.value)
    );

const secondaryStyle = (data: DataSet): StyleConfig =>
    maxDataSet(data)
        .map<StyleConfig>(max => {
            const scale = findScale(max);
            // const step = scale / SECONDARY_LEGEND_MAX;
            const step = scale / data.entries.length;
            const intervals = makeNumberList(
                0,
                data.entries.length
            ).map<PointInterval>((_, idx) => {
                const low = Math.max(idx * step, 2);
                const high = (idx + 1) * step;
                const mid = low + (high - low) / 2;
                const size = areaSizeMax(scale, mid);
                if (size < 1) {
                    console.log('TRANSPARENT', low, high);
                }

                const color = size >= 1 ? 'red' : 'transparent';
                return {
                    low,
                    high,
                    label: {},
                    marker: {
                        codePoint: nameToCode('circle'),
                        color,
                        size,
                    },
                };
            });
            return {
                kind: 'point-continuous',
                propName: 'value',
                intervals,
            };
        })
        .getOrElse({
            kind: 'point-simple',
            marker: {
                codePoint: nameToCode('circle'),
                color: 'red',
                size: 7,
            },
        });

const findTableEntry = (id: number) =>
    fromNullable(getUnsortedTableEntries().find(entry => entry.id === id));

export const secondaryLegendLabel = (ds: DataSet) => {
    const selected = getSelectedFeatures();
    if (selected.length === 1) {
        const selectedId = selected[0];
        return fromNullable(ds.entries.find(entry => entry.id === selectedId))
            .chain(entry =>
                findTableEntry(entry.id).map(({ name }) => ({
                    name,
                    value: entry.value,
                }))
            )
            .map(({ name, value }) => concat(name, ' — ', formatNumber(value)));
    }
    return none;
};

const secondaryStyleForLegend = (data: DataSet): StyleConfig => ({
    kind: 'point-continuous',
    propName: 'value',
    intervals: max(data.entries.filter(e => e.flag === 'NN').map(e => e.value))
        .map(maxValue => {
            const { scale, steps } = makeSteps(maxValue);
            return steps.map(([low, high]) => ({
                low,
                high,
                label: {
                    fr: formatNumber(high),
                    nl: formatNumber(high),
                },
                marker: {
                    codePoint: nameToCode('circle'),
                    color: 'red',
                    size: areaSizeMax(scale, high),
                },
            }));
        })
        .getOrElse([]),
});

const extraStyleLine = (info: ExtraLayerInfo): StyleConfig => ({
    kind: 'line-simple',
    strokeColor: info.color,
    strokeWidth: 1,
    dash: [],
});

const extraStylePoly = (info: ExtraLayerInfo): StyleConfig => ({
    kind: 'polygon-simple',
    strokeColor: 'grey',
    fillColor: info.color,
    strokeWidth: 0.5,
});

const extraStyle = (info: ExtraLayerInfo) =>
    info.geometry_type === 'line' ? extraStyleLine(info) : extraStylePoly(info);

const primaryLayerTemplate = (data: DataSet): ILayerInfo => ({
    id: data.id.toString(),
    legend: null,
    // legend: data.name,
    group: null,
    metadataId: data.id.toString(),
    featureViewOptions: { type: 'default' },
    visible: getShowPrimary(),
    style: primaryStyle(data),
    maxZoom: 30,
    minZoom: 0,
    visibleLegend: true,
    opacitySelector: false,
});

const noDisplayLayerTemplate = (data: DataSet): ILayerInfo => ({
    id: 'no-display',
    legend: {
        en: 'Not available',
        nl: 'Niet beschikbaar',
        fr: 'Non disponible',
    },
    group: null,
    metadataId: data.id.toString(),
    featureViewOptions: { type: 'default' },
    visible: getShowPrimary(),
    style: {
        kind: 'polygon-simple',
        fillColor: '#CCCCCC',
        strokeColor: '#CCCCCC',
        strokeWidth: 0,
    },
    maxZoom: 30,
    minZoom: 0,
    visibleLegend: false,
    opacitySelector: false,
});

export const secondaryLayerTemplate = (data: DataSet): ILayerInfo => ({
    id: data.id.toString(),
    // legend: null,
    legend: makeTitle(data),
    group: null,
    metadataId: data.id.toString(),
    featureViewOptions: { type: 'default' },
    visible: getShowSecondary(),
    style: secondaryStyle(data),
    maxZoom: 30,
    minZoom: 0,
    visibleLegend: true,
    opacitySelector: false,
});

export const secondaryLayerTemplateForLegend = (data: DataSet): ILayerInfo => ({
    id: data.id.toString(),
    // legend: null,
    legend: makeTitle(data),
    group: null,
    metadataId: data.id.toString(),
    featureViewOptions: { type: 'default' },
    visible: getShowSecondary(),
    style: secondaryStyleForLegend(data),
    maxZoom: 30,
    minZoom: 0,
    visibleLegend: true,
    opacitySelector: false,
});

const extra_id = (id: number) => `extra_${id}`;

const extraLayerTemplate = (info: ExtraLayerInfo): ILayerInfo => ({
    id: extra_id(info.id),
    legend: info.name,
    // legend: data.name,
    group: null,
    metadataId: extra_id(info.id),
    featureViewOptions: { type: 'default' },
    visible: isLayerSelected(info.id),
    style: extraStyle(info),
    maxZoom: 30,
    minZoom: 0,
    visibleLegend: true,
    opacitySelector: false,
});

const NOW = new Date().toISOString();
const metadataTemplate = (data: DataSet): Inspire => ({
    id: data.level,
    geometryType: 'Polygon',
    resourceTitle: {},
    resourceAbstract: {},
    uniqueResourceIdentifier: data.level,
    topicCategory: [],
    keywords: [],
    geographicBoundingBox: { west: 0.0, north: 0.0, east: 0.0, south: 0.0 },
    temporalReference: { creation: NOW, revision: NOW },
    responsibleOrganisation: [1],
    metadataPointOfContact: [1],
    metadataDate: NOW,
    published: false,
    dataStreamUrl: null,
    maintenanceFrequency: 'unknown',
});

const extraMetadataTemplate = (info: ExtraLayerInfo): Inspire => ({
    id: extra_id(info.id),
    geometryType: info.geometry_type === 'poly' ? 'Polygon' : 'LineString',
    resourceTitle: {},
    resourceAbstract: {},
    uniqueResourceIdentifier: extra_id(info.id),
    topicCategory: [],
    keywords: [],
    geographicBoundingBox: { west: 0.0, north: 0.0, east: 0.0, south: 0.0 },
    temporalReference: { creation: NOW, revision: NOW },
    responsibleOrganisation: [1],
    metadataPointOfContact: [1],
    metadataDate: NOW,
    published: false,
    dataStreamUrl: null,
    maintenanceFrequency: 'unknown',
});

const notEmptyString = fromPredicate<string>(s => s.trim().length > 0);

export const makeTitle = (data: DataSet) => ({
    fr: fromNullable(data.unit.fr)
        .chain(notEmptyString)
        .map(unit => `${data.name.fr} (${unit}) — ${data.year}`)
        .getOrElse(`${data.name.fr} — ${data.year}`),
    nl: fromNullable(data.unit.nl)
        .chain(notEmptyString)
        .map(unit => `${data.name.nl} (${unit}) — ${data.year}`)
        .getOrElse(`${data.name.nl} — ${data.year}`),
});

const getLayers = (data: DataSet, forPrint = false): ILayerInfo[] =>
    catOptions([
        forPrint ? none : some(noDisplayLayerTemplate(data)),
        some(primaryLayerTemplate(data)),
        forPrint ? some(noDisplayLayerTemplate(data)) : none,
        forPrint
            ? getDataSet('extra').map(secondaryLayerTemplateForLegend)
            : getDataSet('extra').map(secondaryLayerTemplate),
    ]).concat(getExtraLayerInfo().map(extraLayerTemplate));

export const getMapLayerInfo = (): ILayerInfo[] =>
    getDataSet('main').map(getLayers).getOrElse([]);

const makeMapInfo = (data: DataSet, forPrint = false): IMapInfo => ({
    baseLayer: 'urbis.irisnet.be/urbis_gray',
    id: data.id.toString(),
    id_origin: data.id.toString(),
    url: `/none`,
    lastModified: Date.now(),
    status: 'published',
    title: makeTitle(data),
    description: data.description,
    categories: [],
    attachments: [],
    layers: getLayers(data, forPrint).map(layer => layer.id),
});

export const getMapInfo = () => getMapInfoOption().toNullable();

export const getMapInfoOption = () =>
    getDataSet('main').map(ds => makeMapInfo(ds, false));

export const getMapInfoPrintOption = () =>
    getDataSet('main').map(ds => makeMapInfo(ds, true));

export const getPrimaryLayerSyntInfo = (): Option<SyntheticLayerInfo> =>
    getDataSet('main').map(data => ({
        name: data.name,
        info: primaryLayerTemplate(data),
        metadata: metadataTemplate(data),
    }));

export const noDisplayLayerSyntInfo = (): Option<SyntheticLayerInfo> =>
    getDataSet('main').map(data => ({
        name: { fr: 'no-display', nl: 'no-display' },
        info: noDisplayLayerTemplate(data),
        metadata: metadataTemplate(data),
    }));

export const getSecondaryLayerSyntInfo = (): Option<SyntheticLayerInfo> =>
    getDataSet('extra').map(data => ({
        name: data.name,
        info: secondaryLayerTemplate(data),
        metadata: metadataTemplate(data),
    }));

export const getExtraLayerSyntInfo =
    (info: ExtraLayerInfo) => (): Option<SyntheticLayerInfo> =>
        some({
            name: info.name,
            info: extraLayerTemplate(info),
            metadata: extraMetadataTemplate(info),
        });

export const getDataSet = (which: 'main' | 'extra') =>
    scopeOption()
        .let('mid', fromNullable(getCurrentMap()))
        .let('remote', remoteToOption(query('data/dataset')[which]))
        .chain(({ mid, remote }) =>
            fromPredicate<DataSet>(r =>
                which === 'main' ? r.id === mid : true
            )(remote)
        );

const makeFeature = (georef: FeatureCollection) => {
    const indexed: { [k: string]: Feature } = {};
    georef.features.forEach(f => {
        indexed[f.id.toString()] = f;
    });

    return (entry: DataEntry): Option<Feature> =>
        fromNullable(indexed[entry.id]).map(feature => ({
            type: 'Feature',
            id: entry.id,
            properties: { ...entry, ...feature.properties },
            geometry: feature.geometry,
        }));
};

type DataDisplay = 'both' | 'visible' | 'unvisible';
const filterDisplay = (display: DataDisplay) => (entry: DataEntry) => {
    switch (display) {
        case 'both':
            return true;
        case 'visible':
            return entry.flag === 'NN';
        case 'unvisible':
            return entry.flag !== 'NN';
    }
};

const makeMapLayerData = (
    data: DataSet,
    georef: FeatureCollection,
    display: DataDisplay
): FeatureCollection => ({
    type: 'FeatureCollection',
    features: catOptions(
        data.entries.filter(filterDisplay(display)).map(makeFeature(georef))
    ),
});

export const getLayerDataOption = (
    which: 'main' | 'extra',
    display: DataDisplay
) =>
    scopeOption()
        .let('data', getDataSet(which))
        .let('georef', ({ data }) =>
            fromNullable(query('data/georef')[data.level]).chain(remoteToOption)
        )
        .map(({ data, georef }) => makeMapLayerData(data, georef, display));

// export const getDatasetMetadataOption = (id: string) => fromNullable(getDatasetMetadata(id));

export const hasPrintTitle = () =>
    null === query('component/print').customTitle;

export const getPrintTitle = (info: IMapInfo) =>
    fromNullable(query('component/print').customTitle).fold(info.title, s => s);

const adjuster = (values: number[]) =>
    minAndMax(values).chain(([min, max]) => {
        const range = Math.ceil(max) - Math.floor(min);
        return fromNullable(
            barAdjustValues.find(([r]) => range < r)
        ).map<Adjust>(([_, divider, decimals]) => ({
            divider,
            min: Math.floor(min / divider) * divider,
            max: Math.ceil(max / divider) * divider,
            scale:
                plotHeight / (Math.ceil(range / divider) * divider + divider),
            format: n => n.toFixed(decimals),
        }));
    });

export const getBarchartEntries = () => {
    const selection = getSelectedFeatures();
    const mapper = (legend: DataLegend[]) => (entry: DataEntry) =>
        fromNullable(
            legend.find(
                ({ low, high }) => entry.value >= low && entry.value < high
            )
        ).map<BarchartEntry>(({ color }) => ({
            id: entry.id,
            value: entry.value,
            selected: selection.indexOf(entry.id) >= 0,
            color,
        }));

    return getDataSet('main')
        .map(ds =>
            catOptions(
                ds.entries
                    .filter(filterDisplay('visible'))
                    .sort((a, b) => (a.value < b.value ? -1 : 1))
                    .map(mapper(ds.legend))
            )
        )
        .chain(entries =>
            adjuster(entries.map(e => e.value)).map(adjust => ({
                entries,
                adjust,
            }))
        );
};

const entryNameSorter =
    (sort: PlotTableSort) => (a: TableEntry, b: TableEntry) =>
        sort === 'asc'
            ? a.name.localeCompare(b.name, getLang())
            : b.name.localeCompare(a.name, getLang());

const sortAsc = (a: TableEntry, b: TableEntry) => {
    if (a.flagged && b.flagged) {
        return 0;
    } else if (a.flagged) {
        return -1;
    } else if (b.flagged) {
        return 1;
    }
    return a.value - b.value;
};

const sortDesc = (a: TableEntry, b: TableEntry) => {
    if (a.flagged && b.flagged) {
        return 0;
    } else if (a.flagged) {
        return 1;
    } else if (b.flagged) {
        return -1;
    }
    return b.value - a.value;
};

const entryValueSorter =
    (sort: PlotTableSort) => (a: TableEntry, b: TableEntry) =>
        sort === 'asc' ? sortAsc(a, b) : sortDesc(a, b);

const getUnsortedTableEntries = (): TableEntry[] => {
    const selection = getSelectedFeatures();
    const layerData = getLayerDataOption('main', 'both');
    const getName = (id: number) =>
        layerData
            .chain(({ features }) =>
                fromNullable(features.find(f => f.id === id))
            )
            .chain(f => getFeaturePropOption(f, 'name'))
            .chain(name => fromEither(MessageRecordIO.decode(name)))
            .map(fromRecord);

    const mapper = (entry: DataEntry) =>
        getName(entry.id).map<TableEntry>(name => ({
            id: entry.id,
            value: entry.value,
            selected: selection.indexOf(entry.id) >= 0,
            flagged: entry.flag !== 'NN',
            name,
        }));

    return getDataSet('main')
        .map(ds => catOptions(ds.entries.map(mapper)))
        .getOrElse([]);
};

export const getTableEntries = (): TableEntry[] => {
    const base = getUnsortedTableEntries();
    return getPlotTableSort()
        .map(([col, sort]) =>
            base.sort(
                col === 'name' ? entryNameSorter(sort) : entryValueSorter(sort)
            )
        )
        .getOrElse(base);
};

export const getPlotTableSort = () =>
    fromNullable(query('component/plot/table/sort'));

export const getShowPrimary = queryK('component/legend/show-primary');
export const getShowSecondary = queryK('component/legend/show-secondary');
export const withSecondary = () =>
    fromPredicate<boolean>(identity)(getShowSecondary());

export const getExtraLayerInfo = () => query('component/layers/info');

export const getExtraLayerData = (id: number) => {
    const idx = getExtraLayerInfo().findIndex(info => info.id === id);
    return index(
        idx,
        query('data/extra/layers') as RemoteResource<FeatureCollection>[]
    ).chain(remoteToOption);
};

export const getSources = () => getDataSet('main').map(ds => ds.sources);

export const getLegendExtra = () =>
    getDataSet('main').map(ds =>
        ds.legendExtra.filter(
            l =>
                l.lang === getLang() &&
                (!getShowSecondary() ? l.tag !== 'S' : true)
        )
    );
