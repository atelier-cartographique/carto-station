import { DIV } from 'sdi/components/elements';

import { renderChart } from './barchart';
import { renderTable } from './table';
import { renderSources } from './sources';

export type MDQLevel = 'city' | 'neighbourhood' | 'sector';

export type PlotTableCol = 'name' | 'value';
export type PlotTableSort = 'asc' | 'desc';

export interface BarchartEntry {
    id: number;
    value: number;
    color: string;
    selected: boolean;
}

export interface TableEntry {
    id: number;
    value: number;
    name: string;
    selected: boolean;
    flagged: boolean;
}

export const plotWidth = 100;
export const plotHeight = 100;
export const plotPadding = 5;
export const axisWidth = 20;

export const barAdjustValues = [
    [0.00001, 1, 1],
    [0.1, 0.005, 3],
    [1, 0.05, 2],
    [2, 0.1, 1],
    [20, 1, 0],
    [200, 10, 0],
    [2000, 100, 0],
    [20000, 1000, 0],
    [50000, 5000, 0],
    [100000, 10000, 0],
    [1000000, 100000, 0],
    [10000000, 1000000, 0],
];

export type Adjust = {
    scale: number;
    divider: number;
    min: number;
    max: number;
    format: (n: number) => string;
};

export const renderPlot = () =>
    DIV('plot', renderChart(), renderTable(), renderSources());
export default renderPlot;
