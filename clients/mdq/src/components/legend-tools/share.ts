/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { getLang, getRootUrl } from 'sdi/app';
import tr from 'sdi/locale';
import { DIV, H2, SPAN } from 'sdi/components/elements';
import { helpText } from 'sdi/components/helptext';

import { getCurrentMap } from '../../queries/app';

const logger = debug('sdi:tool-share');
const hasClipboard = !!document.execCommand;

const copyToClipBoard = (s: string) => {
    if (!hasClipboard) {
        return SPAN({});
    }

    return SPAN(
        {
            className: 'copy',
            onClick: () => {
                const ta = document.createElement('textarea');
                ta.style.position = 'absolute';
                ta.style.width = '10px';
                ta.style.left = '-10px';
                document.body.appendChild(ta);
                ta.value = s;
                ta.setAttribute('readonly', '');
                ta.select();
                ta.setSelectionRange(0, s.length);
                const copied = document.execCommand('Copy');
                document.body.removeChild(ta);
                logger(`text copied: ${copied}`);
            },
        },
        tr.mdq('copy'),
        SPAN({ className: 'copied' }, tr.mdq('copied'))
    );
};

const makeCopyable = (value: string) => {
    return DIV(
        { className: 'text-copy' },
        copyToClipBoard(value),
        SPAN({ className: 'text' }, value)
    );
};

const render = () => {
    const embedUrl = getRootUrl(`mdq/${getLang()}/${getCurrentMap()}`);

    const iframeExampleWithoutView = `<!-- ${tr.mdq(
        'embedCommentSnippet1'
    )}  -->
        <!-- ${tr.mdq('embedCommentSnippet2')}  -->
    <div id="iFrameAtlas">
        <style scoped>
            #iFrameAtlas {
                position: relative;
                width: 100%;
                padding-bottom: 55%;
            }

            @media (orientation:portrait) {
                #iFrameAtlas {
                    padding-bottom: initial;
                    height: 200vw;
                    max-height: 85vh;
                }
            }
        </style>
        <iframe src="${embedUrl}" width="100%" height="100%"
            style="position:absolute;" frameborder="0"></iframe>
    </div>`;

    return DIV(
        { className: 'sidebar-main tool-group share-embed' },
        DIV(
            { className: 'tool embed' },
            H2({}, tr.mdq('embed')),
            helpText(tr.mdq('helptext:embed')),
            DIV(
                { className: 'tool-body' },
                // H3({}, tr.mdq('mapEmbed')),
                makeCopyable(iframeExampleWithoutView)
            )
        )
    );
};

export default render;

logger('loaded');
