/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { ReactNode } from 'react';

import { DIV, H1, H2, H3, INPUT, SPAN } from 'sdi/components/elements';
import { LayerGroup, ILayerInfo, IMapInfo } from 'sdi/source';
import tr, { fromRecord } from 'sdi/locale';
import { divTooltipLeft } from 'sdi/components/tooltip';

import queries from '../../queries/legend';
import { setPage } from '../../events/legend';
import {
    setLayout,
    toggleOrtho,
    toggleShowPrimary,
    toggleShowSecondary,
    unsetCurrentFeature,
} from '../../events/app';
import {
    getCurrentBaseLayerName,
    getDataSet,
    getLegendExtra,
    getMapInfoOption,
    getShowPrimary,
    getShowSecondary,
    secondaryLayerTemplateForLegend,
    makeTitle,
    secondaryLegendLabel,
    withSecondary,
    getMapLayerInfo,
} from '../../queries/app';
import legendItem from './legend-item';
import { AppLayout, LegendPage } from '../../shape/types';
import { MDQMessageKey } from '../../locale';
import print from '../legend-tools/print';
import share from '../legend-tools/share';
import measure from '../legend-tools/measure';
import {
    zoomIn,
    zoomOut,
    enterFullScreen,
    toggleSumInteraction,
} from 'mdq/src/events/map';
import renderLayers from '../layers';

import plot from '../plot';
import { getInteractionMode } from 'mdq/src/queries/map';
import { none, some } from 'fp-ts/lib/Option';

const logger = debug('sdi:legend');

// const harvestPointButton = makeLabel('info', 2, () =>
//     tr.mdq('harvestInitiatePoint')
// );
// const harvestLineButton = makeLabel('info', 2, () =>
//     tr.mdq('harvestInitiateLine')
// );
// const harvestPolygonButton = makeLabel('info', 2, () =>
//     tr.mdq('harvestInitiatePolygon')
// );

interface Group {
    g: LayerGroup | null;
    layers: ILayerInfo[];
}

const groupItems = (layers: ILayerInfo[]) =>
    layers
        .slice()
        .reverse()
        .reduce<Group[]>((acc, info) => {
            const ln = acc.length;
            if (ln === 0) {
                return [
                    {
                        g: info.group,
                        layers: [info],
                    },
                ];
            }
            const prevGroup = acc[ln - 1];
            const cg = info.group;
            const pg = prevGroup.g;
            // Cases:
            // info.group == null && prevGroup.g == null => append
            // info.group != null && prevGroup.g != null && info.group.id == prevGroup.id => append
            if (
                (cg === null && pg === null) ||
                (cg !== null && pg !== null && cg.id === pg.id)
            ) {
                prevGroup.layers.push(info);
                return acc;
            }
            // info.group == null && prevGroup.g != null => new
            // info.group != null && prevGroup.g == null => new
            // info.group != null && prevGroup.g != null && info.group.id != prevGroup.id => new

            return acc.concat({
                g: cg,
                layers: [info],
            });
        }, []);

const renderLegendGroups = (groups: Group[]) =>
    groups.map((group, key) => {
        const layers = group.layers.filter(l => l.visible === true);

        if (layers.length === 0) {
            return DIV({ key }); // FIXME - we can do better than that
        }
        const items = layers.map(legendItem);
        if (group.g !== null) {
            return DIV(
                { key, className: 'legend-group named' },
                DIV(
                    { className: 'legend-group-title' },
                    fromRecord(group.g.name)
                ),
                DIV({ className: 'legend-group-items' }, ...items)
            );
        }
        return DIV({ key, className: 'legend-group anonymous' }, ...items);
    });

const switchItem = (
    p: LegendPage,
    tk: MDQMessageKey,
    currentPage: LegendPage
) => {
    return divTooltipLeft(
        tr.mdq(tk),
        {
            className: `switch-item switch-${p} ${
                p === currentPage ? 'active' : ''
            }`,
            onClick: () => {
                setLayout(AppLayout.MapAndInfo);
                unsetCurrentFeature();
                setPage(p);
            },
        },
        DIV({ className: 'picto' })
    );
};

const buttonSumToggle = () =>
    divTooltipLeft(
        tr.mdq('toggleSum'),
        {
            className: `switch-item switch-sum ${
                getInteractionMode() === 'select' ? 'active' : ''
            }`,
            onClick: toggleSumInteraction,
        },
        DIV({ className: 'picto' })
    );

const buttonBaseMap = () =>
    divTooltipLeft(
        tr.mdq('ortho'),
        {
            className: `switch-item switch-basemap ${
                getCurrentBaseLayerName() === 'urbis.irisnet.be/urbis_ortho'
                    ? 'active'
                    : ''
            }`,
            onClick: toggleOrtho,
        },
        DIV({ className: 'picto' })
    );

const buttonZoomIn = () =>
    divTooltipLeft(
        tr.core('zoomIn'),
        {
            className: 'switch-item switch-zoom-in',
            onClick: zoomIn,
        },
        DIV({ className: 'picto' })
    );

const buttonZoomOut = () =>
    divTooltipLeft(
        tr.core('zoomOut'),
        {
            className: 'switch-item switch-zoom-out',
            onClick: zoomOut,
        },
        DIV({ className: 'picto' })
    );

const buttonFullscreen = () =>
    divTooltipLeft(
        tr.core('fullscreen'),
        {
            className: 'switch-item switch-fullscreen',
            onClick: enterFullScreen,
        },
        DIV({ className: 'picto' })
    );

export const switcher = () => {
    const currentPage = queries.currentPage();
    return DIV(
        'switcher-wrapper',
        DIV(
            'switcher',
            switchItem('info', 'tooltip:info', currentPage),
            // switchItem('data', 'tooltip:dataAndSearch', currentPage),
            // switchItem('base-map', 'tooltip:base-map', currentPage),
            switchItem('print', 'tooltip:print', currentPage),
            switchItem('share', 'tooltip:ishare', currentPage),
            switchItem('measure', 'tooltip:measure', currentPage),
            // switchItem('locate', 'tooltip:locate', currentPage)
            buttonSumToggle(),
            buttonBaseMap()
        ),
        DIV(
            'map-tools',
            // buttonNorth(),
            buttonZoomIn(),
            buttonZoomOut(),
            buttonFullscreen()
        )
    );
};

const wrapTheWholeThing = (...es: ReactNode[]) =>
    DIV({ className: 'sidebar-right map-legend' }, ...es);

const renderMapInfoHeader = (mapInfo: IMapInfo, p: LegendPage) =>
    DIV(
        {
            className: `sidebar-header legend-header-${p}`,
        },
        H1({}, fromRecord(mapInfo.title))
    );

const renderPrimaryLegend = (mapInfo: IMapInfo) =>
    DIV(
        'legend-top-box primary',
        DIV(
            'legend-control',
            INPUT({
                type: 'checkbox',
                checked: getShowPrimary(),
                onChange: toggleShowPrimary,
            }),
            DIV('extra-title', fromRecord(mapInfo.title))
        ),
        ...renderLegendGroups(groupItems(getMapLayerInfo().slice(1, 1 + 1)))
    );

const renderNoDisplayLegend = () =>
    getDataSet('main')
        .chain(ds =>
            ds.entries.filter(e => e.flag !== 'NN').length > 0 ? some(0) : none
        )
        .map(() =>
            DIV(
                'legend-item no-display',
                DIV('item-style'),
                DIV('item-label', SPAN('', tr.mdq('noDisplay')))
            )
        );

const renderExtraLegend = () =>
    getLegendExtra().map(extras =>
        DIV(
            'legend-item extra',
            ...extras.map(extra =>
                DIV('extra-legend', H3('', extra.label), DIV('', extra.value))
            )
        )
    );

const renderSecondaryLegend = () =>
    getDataSet('extra').map(ds =>
        DIV(
            'legend-top-box secondary',
            DIV(
                'legend-control',
                INPUT({
                    type: 'checkbox',
                    checked: getShowSecondary(),
                    onChange: toggleShowSecondary,
                }),
                DIV('extra-title', fromRecord(makeTitle(ds)))
            ),
            ...renderLegendGroups(
                groupItems([secondaryLayerTemplateForLegend(ds)])
            ),
            withSecondary().map(() =>
                DIV('extra-selected', secondaryLegendLabel(ds))
            )
        )
    );

const renderLegend = (mapInfo: IMapInfo) =>
    DIV(
        { className: 'sidebar-main styles-block' },
        H2({}, tr.mdq('mapLegend')),
        renderPrimaryLegend(mapInfo),
        renderNoDisplayLegend(),
        renderSecondaryLegend(),
        renderExtraLegend()
    );

// const renderLayers = (_mapInfo: IMapInfo) => DIV('layers-block', '~LAYERS')
const renderPlot = () => DIV('plot-block', plot());

const renderMapInfo = (mapInfo: IMapInfo) =>
    wrapTheWholeThing(
        renderMapInfoHeader(mapInfo, 'info'),
        // DIV({ className: 'sidebar-main legend-main' }, info()),
        renderLegend(mapInfo),
        renderLayers(mapInfo),
        renderPlot()
    );

const legend = () => {
    const currentPage = queries.currentPage();
    return getMapInfoOption().map(mapInfo => {
        switch (currentPage) {
            case 'info':
                return renderMapInfo(mapInfo);

            case 'measure':
                return wrapTheWholeThing(
                    renderMapInfoHeader(mapInfo, 'measure'),
                    measure()
                );
            case 'print':
                return wrapTheWholeThing(
                    renderMapInfoHeader(mapInfo, 'print'),
                    print()
                );
            case 'share':
                return wrapTheWholeThing(
                    renderMapInfoHeader(mapInfo, 'share'),
                    share()
                );
        }
    });
};

export default legend;

logger('loaded');
