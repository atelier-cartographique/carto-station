/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import * as debug from 'debug';
import { Coordinate } from 'ol/coordinate';
import Map from 'ol/Map';
import { none, Option, some } from 'fp-ts/lib/Option';

import { assignK, dispatch, dispatchK } from 'sdi/shape';
import {
    viewEventsFactory,
    scaleEventsFactory,
    trackerEventsFactory,
    measureEventsFactory,
    ExtractFeature,
    Interaction,
    PrintRequest,
    PrintResponse,
} from 'sdi/map';
import { tableEvents } from 'sdi/components/table';
import { MessageRecord } from 'sdi/source';

import {
    getInteraction,
    getInteractionMode,
    withExtract,
} from '../queries/map';
import { PrintProps } from '../components/print';
import { updatePositionerLongitude, updatePositionerLatitude } from './legend';
import { fromRecord } from 'sdi/locale';
import { getExtraLayerInfo } from '../queries/app';

const logger = debug('events/app');

const setInteraction = dispatchK('port/map/interaction');

export const defaultInteraction = (): Interaction => ({
    label: 'enter',
    state: { selected: null },
});

export const resetInteraction = () => setInteraction(defaultInteraction);

export const toggleSumInteraction = () =>
    getInteractionMode() === 'select'
        ? resetInteraction()
        : setInteraction(() => ({
              label: 'select',
              state: { selected: null },
          }));

export const scalelineEvents = scaleEventsFactory(dispatchK('port/map/scale'));
export const viewEvents = viewEventsFactory(dispatchK('port/map/view'));
export const trackerEvents = trackerEventsFactory(
    setInteraction,
    getInteraction
);
export const measureEvents = measureEventsFactory(
    setInteraction,
    getInteraction
);

export const startExtract = () =>
    setInteraction(() => ({
        label: 'extract',
        state: [],
    }));

export const stopExtract = () => setInteraction(defaultInteraction);

const eq = (a: ExtractFeature[], b: ExtractFeature[]) =>
    a.length === b.length &&
    a.length ===
        a.filter((e, i) => b[i] && e.featureId === b[i].featureId).length;

const withNewExtracted = (a: ExtractFeature[]) =>
    withExtract().map(b => eq(a, b.state));

export const setExtractCollection = (es: ExtractFeature[]) =>
    withNewExtracted(es).fold(
        setInteraction(() => ({
            label: 'extract',
            state: es,
        })),
        isEq => {
            if (!isEq) {
                setInteraction(() => ({
                    label: 'extract',
                    state: es,
                }));
            }
        }
    );

export const extractTableEvents = tableEvents(
    dispatchK('component/table/extract')
);

export const startMark = () =>
    setInteraction(s => {
        if (s.label === 'mark') {
            return { ...s, state: { ...s.state, started: true } };
        }
        return s;
    });

export const endMark = () => setInteraction(() => defaultInteraction());

export const putMark = (coordinates: Coordinate) =>
    setInteraction(() => ({
        label: 'mark',
        state: {
            started: false,
            endTime: Date.now() + 12000,
            coordinates,
        },
    }));

export const startPointerPosition = (after: (c: Coordinate) => void) =>
    setInteraction(() => ({
        label: 'position',
        state: { coordinates: [0, 0], after },
    }));

export const setPointerPosition = (coordinates: Coordinate) =>
    setInteraction(it => {
        if ('position' === it.label) {
            return {
                ...it,
                state: { ...it.state, coordinates },
            };
        }
        return it;
    });

export const stopPointerPosition = (coordinate: Option<Coordinate>) => {
    const it = getInteraction();
    if (it.label === 'position') {
        coordinate.map(c => {
            const after = it.state.after;
            const coordinates = it.state.coordinates;
            updatePositionerLongitude(Math.floor(c[0]));
            updatePositionerLatitude(Math.floor(c[1]));
            setTimeout(() => after(coordinates), 1);
        });
        setInteraction(() => defaultInteraction());
    }
};

export const updateLoading = (ms: MessageRecord[]) =>
    dispatch('port/map/loading', () => ms);

export const updateLoadingAddOne = (m: MessageRecord) =>
    dispatch('port/map/loading', ms =>
        ms.filter(mm => fromRecord(mm) !== fromRecord(m)).concat(m)
    );

export const updateLoadingRemoveOne = (m: MessageRecord) =>
    dispatch('port/map/loading', ms =>
        ms.filter(mm => fromRecord(mm) !== fromRecord(m))
    );

export const setPrintRequest = (r: PrintRequest<PrintProps>) => {
    dispatchK('port/map/printRequest')(() => r);
    dispatchK('port/map/interaction')(() => ({
        label: 'print',
        state: null,
    }));
};
export const setPrintResponse = (r: PrintResponse<PrintProps>) =>
    dispatchK('port/map/printResponse')(() => r);

export const stopPrint = () => setInteraction(defaultInteraction);

export const setPrintToImage = assignK('port/map/printToImage');

// const queueZoom = (() => {
//     const queue: number[] = []
//     let it: number | null = null;
//     const queueZoom = (n: number) => {
//         const { dirty } = query('port/map/view')
//         if (dirty === 'none') {
//             logger('zoomin direct')
//             dispatch('port/map/view', (v) => ({
//                 ...v,
//                 dirty: 'geo',
//                 zoom: v.zoom + n,
//             }));
//         }
//         else {
//             queue.push(n)
//             logger(`queue ${queue.length}`)
//             if (it === null) {
//                 it = window.setInterval(() => {
//                     index(0, queue)
//                         .map(n => {
//                             const { dirty } = query('port/map/view')
//                             if (dirty === 'none') {
//                                 logger('zoomin')
//                                 queue.shift()
//                                 dispatch('port/map/view', (v) => ({
//                                     ...v,
//                                     dirty: 'geo',
//                                     zoom: v.zoom + n,
//                                 }));
//                             }
//                             return n
//                         })
//                         .alt(fromNullable(it))
//                         .map((i) => {
//                             it = null;
//                             window.clearInterval(i)
//                         })
//                 }, 64);
//             }
//         }
//     }
//     return queueZoom
// })()

const zoom = (n: number) =>
    dispatch('port/map/view', v => ({
        ...v,
        dirty: 'geo',
        zoom: v.zoom + n,
    }));

export const zoomIn = () => zoom(1);

export const zoomOut = () => zoom(-1);

export const resetRotate = () => {
    dispatch('port/map/view', v => ({
        ...v,
        dirty: 'geo',
        rotation: 0,
    }));
    // we know it's a bit dirty, but it has to be quick here
    window.setTimeout(
        () =>
            dispatch('port/map/view', v => ({
                ...v,
                dirty: 'geo',
                rotation: 0,
            })),
        1500
    );
};

const requestFullScreen = (element: HTMLElement) => {
    if (element.requestFullscreen) {
        element.requestFullscreen();
    } else if ((element as any)['msRequestFullscreen']) {
        (element as any)['msRequestFullscreen']();
    } else if ((element as any)['webkitRequestFullscreen']) {
        (element as any)['webkitRequestFullscreen']();
    }
};

export const enterFullScreen = () =>
    getOlMap().map(map => requestFullScreen(map.getTargetElement()));

/**
 * This thing is really a hack, but should work smoothly - pm
 */
export const { storeOlMap, getOlMap } = (() => {
    let stored: Option<Map> = none;

    const storeOlMap = (map: Map) => (stored = some(map));

    const getOlMap = () => stored;

    return {
        storeOlMap,
        getOlMap,
    };
})();

// const addExtraLayer = (
//     info: ExtraLayerInfo,
// ) =>

export const toggleSelectLayer = (id: number) => {
    const idx = getExtraLayerInfo().findIndex(info => info.id === id);
    dispatch('component/layers/select', state =>
        state.map((s, i) => (i === idx ? !s : s))
    );
    // removeLayerAll(mapName)
    // zip(getExtraLayerInfo(), query('component/layers/select')).map(([info, selected]) => {
    //     if (selected) {
    //         addExtraLayer(info)
    //     }
    // })
};

logger('loaded');
