#!/bin/sh

export NODE_OPTIONS=--openssl-legacy-provider
echo "Watching $1"

if test -n ${2} ;
then
    echo "CS>  ${2}"
    export CS_PATH=$(realpath "${PWD}/../platforms/${2}")
    export WEBPACK_OPTIONS="--env PLATFORM=${CS_PATH}"
fi


webpack --config $1/webpack.config.js --watch  --mode development  ${WEBPACK_OPTIONS}