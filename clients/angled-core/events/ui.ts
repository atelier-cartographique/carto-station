import {
    InformationUnitName,
    FieldValueType,
    updateFieldData,
    UnitData,
    lookupNamedField,
    mapFieldType,
    mapFieldName,
} from 'angled-core/ui';
import { dispatch, assign, query } from 'sdi/shape';
import { Nullable } from 'sdi/util';
import {
    FundingOrg,
    Site,
    Contact,
    Team,
    NovaLoader,
    TeamMemberRole,
    Audience,
} from 'angled-core/ref';
import { getWriteDomain } from '../queries/ui';
import * as debug from 'debug';
import { fromNullable } from 'fp-ts/lib/Option';
import {
    DirectGeometryObject,
    MultiPoint,
    MultiLineString,
    MultiPolygon,
    Feature,
    GeometryObject,
} from 'sdi/source';
import { domainsForField } from 'angled-core/queries/universe';

const logger = debug('sdi:angled-core/events/ui');

export const setFormInput =
    (unitName: InformationUnitName, fieldName: string) => (n: FieldValueType) =>
        dispatch('component/ui/form/manip', u => {
            // https://gitlab.com/atelier-cartographique/perspective/shared/plate-forme-veille-projets-urbains/-/issues/604
            if (typeof n === 'string' && n.length === 0) {
                if (u !== null) {
                    type Tu = typeof u;
                    type Ku = keyof Tu;
                    const updated = Object.keys(u)
                        .map<Ku>(k => k as Ku)
                        .reduce((acc, key) => {
                            if (key !== fieldName) {
                                acc[key] = u[key];
                            }
                            return acc;
                        }, {} as Tu);
                    return updated;
                }
                return null;
            }
            const updated = updateFieldData(unitName, fieldName, u)(n);
            logger(`setFormInput(${unitName}, ${fieldName})(${n})`, u, updated);
            return updated;
        });

export const ensureMultiGeometry = (
    g: DirectGeometryObject
): MultiPoint | MultiLineString | MultiPolygon => {
    switch (g.type) {
        case 'Point':
            return { ...g, type: 'MultiPoint', coordinates: [g.coordinates] };
        case 'LineString':
            return {
                ...g,
                type: 'MultiLineString',
                coordinates: [g.coordinates],
            };
        case 'Polygon':
            return { ...g, type: 'MultiPolygon', coordinates: [g.coordinates] };
        case 'MultiPoint':
        case 'MultiLineString':
        case 'MultiPolygon':
            return g;
    }
};

const noop = () => {};

const crsLambert72 = {
    type: 'name',
    properties: {
        name: 'epsg:31370',
    },
};

/**
 * Add CRS to geojson geometry
 *
 * As of https://tools.ietf.org/html/rfc7946#section-4
 * GeoJSON does not support CRS annotation anymore, nevertheless GEOS still
 * supports it so it's the easiest way to have 31370 all the way.
 *
 * @param geom
 */
const withCRS = (geom: GeometryObject) =>
    Object.assign({}, geom, { crs: crsLambert72 });

export const selectFeatureForUnit =
    (unitName: InformationUnitName, fieldName: string, then = noop) =>
    (lid: string, id: string | number) =>
        fromNullable(query('data/layers')[lid])
            .chain(fc => fromNullable(fc.features.find(f => f.id === id)))
            .map(f =>
                setFormInput(
                    unitName,
                    fieldName
                )(ensureMultiGeometry(withCRS(f.geometry)))
            )
            .map(() =>
                assign('port/map/select', { layerId: lid, featureId: id })
            )
            .map(then);

export const clearSelectedGeometry = () => assign('port/map/select', null);

export const addFeatureForUnit =
    (unit: InformationUnitName, fieldName: string, then = noop) =>
    (f: Feature) => {
        const geom = ensureMultiGeometry(withCRS(f.geometry));
        // geom.coordinates = mapCoordinates(geom, forwardT);
        switch (geom.type) {
            case 'MultiLineString':
            case 'MultiPoint':
            case 'MultiPolygon':
                setFormInput(unit, fieldName)(geom);
        }
        then();
    };

export const setFormInputTeamMembers = (a: [Contact, TeamMemberRole][]) =>
    assign('component/ui/form/manip-team-member', a);

export const resetFormInput = () => {
    assign('component/ui/form/manip', null);
    resetNewFkFormInput();
};

export const setUnitInEdition = (ud: Nullable<Partial<UnitData>>) =>
    assign('component/ui/form/manip', ud);

export const setWriteDomain = (fieldName: string, id: number) => {
    if (getWriteDomain().filter(wd => wd.fieldName === fieldName).length > 0) {
        assign(
            'component/ui/form/write/domain',
            getWriteDomain()
                .filter(wd => wd.fieldName !== fieldName)
                .concat({ fieldName, id })
        );
    } else {
        dispatch('component/ui/form/write/domain', wd =>
            wd.concat({ fieldName, id })
        );
    }
};

export const resetWriteDomain = () =>
    assign('component/ui/form/write/domain', []);

/**
 * prepare some default values for input form
 * @param unit
 */
export const prepareWriteDomainForUnit = (unit: InformationUnitName) => {
    lookupNamedField(unit).map(({ fields }) => {
        fields.forEach(field => {
            const ft = mapFieldType(field);
            const fn = mapFieldName(field);
            // Preselects domain when there's only one option
            if (ft === 'term') {
                const domains = domainsForField(unit, fn);
                if (domains.length === 1) {
                    const domain = domains[0];
                    setWriteDomain(fn, domain.id);
                }
            }
            // preset boolean fields to false to make units valid without input
            else if (ft === 'boolean') {
                setFormInput(unit, fn)(false);
            }
        });
    });
};

export const setNovaRefFormInput = (data: Nullable<string>) =>
    assign('component/ui/form/nova-ref', data);

export const setNovaDataFormInput = (data: NovaLoader) =>
    assign('component/ui/form/nova-data', data);

export const setNewFundingOrgFormInput = (data: Partial<FundingOrg>) =>
    assign('component/ui/form/new-funding', data);

export const setNewSiteFormInput = (data: Partial<Site>) =>
    assign('component/ui/form/new-site', data);

export const setNewTeamFormInput = (data: Partial<Team>) =>
    assign('component/ui/form/new-team', data);

export const setNewContactFormInput = (data: Partial<Contact>) =>
    assign('component/ui/form/new-contact', data);

export const resetNewFkFormInput = () => {
    setNovaRefFormInput(null);
    setNewFundingOrgFormInput({});
    setNewSiteFormInput({});
    setNewContactFormInput({});
    setFormInputTeamMembers([]);
};

export const setImageFile = (f: File) => assign('component/ui/image-file', f);

export const addDocumentFile = (f: File) =>
    dispatch('component/ui/document-file', fs => fs.concat(f));

export const resetDocumentFile = () => assign('component/ui/document-file', []);

export const setImage = (i: string) => assign('component/ui/image', i);

export const setWriteUnit = (unit: InformationUnitName, fieldName: string) =>
    assign('component/ui/write/modal/unit', [unit, fieldName]);

export const clearWriteUnit = () =>
    assign('component/ui/write/modal/unit', null);

export const setTeamInputFilter = (pattern: string) =>
    assign('component/ui/team/contact-pattern', pattern);

export const clearTeamInputFilter = () =>
    assign('component/ui/team/contact-pattern', '');

export const setDisplayAudience = (a: Audience) =>
    assign('component/ui/display-audience', a);

export const resetDisplayAudience = () =>
    assign('component/ui/display-audience', null);

logger('Loaded!');
