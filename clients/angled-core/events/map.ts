import {
    dispatchK,
    dispatch,
    assign,
    query,
    assignK,
    observe,
} from 'sdi/shape';
import {
    viewEventsFactory,
    scaleEventsFactory,
    removeLayerAll,
    addLayer,
    Interaction,
    FetchData,
    removeLayer,
} from 'sdi/map';
import { getLoader } from 'sdi/map/loader';
import { clearStreams } from 'sdi/geodata-stream';
import {
    fetchBaseLayerAll,
    fetchAllDatasetMetadata,
} from 'angled-core/remote/map';
import {
    GeometryType,
    Inspire,
    getMessageRecord,
    ILayerInfo,
    StyleConfig,
    FeatureCollection,
     IMapInfo,
} from 'sdi/source';
import { MiniStep } from 'sdi/map/mini';
import { getApiUrl } from 'sdi/app';
import { uniqIdentified } from 'sdi/util';
import { Either, right } from 'fp-ts/lib/Either';
import { some, Option, fromNullable } from 'fp-ts/lib/Option';
import { nameToCode } from 'sdi/components/button/names';
import { Coordinate } from 'ol/coordinate';
import { IUgWsResponse } from 'sdi/ports/geocoder';
import { InformationUnitName } from 'angled-core/ui';
import {
    getFeatureFromInput,
    getWriteLayerId,
    layerInfoOption,
    getView,
} from 'angled-core/queries/map';
import {LAYERINFO_IDS, MAP_SELECT_ID} from "angled-core/map";

export const loadAllBaseLayers = (url: string) =>
    fetchBaseLayerAll(url).then(assignK('data/baselayers'));

export const loadAllDatasetMetadata = (done?: () => void) =>
    fetchAllDatasetMetadata(getApiUrl('metadatas'))(
        frame => {
            dispatch('data/datasetMetadata', state =>
                uniqIdentified(state.concat(frame.results))
            );
        },
        () => {
            if (done) {
                done();
            }
        }
    );

const templateLayerStyle = (geometryType: GeometryType): StyleConfig => {
    switch (geometryType) {
        case 'Point':
        case 'MultiPoint':
            return {
                kind: 'point-simple',
                marker: {
                    codePoint: nameToCode('circle'),
                    size: 12,
                    color: 'rgb(223,88,68)',
                },
            };
        case 'LineString':
        case 'MultiLineString':
            return {
                kind: 'line-simple',
                strokeColor: 'rgb(223,88,68)',
                strokeWidth: 1,
                dash: [],
            };
        case 'Polygon':
        case 'MultiPolygon':
            return {
                kind: 'polygon-simple',
                fillColor: 'rgba(223,88,68,0.7)',
                strokeColor: 'rgba(255,255,255,0.7)',
                strokeWidth: 1,
            };
    }
};

const templateLayerInfo = (md: Inspire): ILayerInfo => ({
    id: md.uniqueResourceIdentifier,
    visible: true,
    group: null,
    legend: null,
    metadataId: md.id,
    featureViewOptions: { type: 'default' },
    style: templateLayerStyle(md.geometryType),
    visibleLegend: true,
    opacitySelector: false,
});

const getLayerData = (
    ressourceId: string
): Either<string, Option<FeatureCollection>> => {
    // const errors = query('remote/errors');
    // if (ressourceId in errors) {
    //     return left(errors[ressourceId]);
    // }
    return right(fromNullable(query('data/layers')[ressourceId]));
};

export const loadLayer = (mapName: string) => (md: Inspire) => {
    removeLayerAll(mapName);
    clearStreams();
    const getSyntheticLayerInfo = () =>
        some({
            metadata: md,
            name: getMessageRecord(md.resourceTitle),
            info: templateLayerInfo(md),
        });

    const { loadLayerWithMetadata, observeView } = getLoader({
        dispatchLayer: dispatchK('data/layers'),
        getLayerData,
        getSyntheticLayerInfo,
        getView,
        mapName,
    });

    try {
        observe('port/map/view', observeView);
    } catch {
        // nothing
    }

    loadLayerWithMetadata(templateLayerInfo(md), md);
}

type GeometryTypeStyle = (gt: GeometryType) => StyleConfig
type LayerTemplate = (geometryTypeStyle: GeometryTypeStyle)
    => (geometryType: GeometryType)
    => ILayerInfo
// Creates a template layer for given geometry. The layer is used by
// the template maps to display geometries and projetcs
export const layerTemplate: LayerTemplate = geometryTypeStyle => geometryType => ({
    id: geometryType,
    legend: null,
    group: null,
    metadataId: geometryType,
    visible: true,
    featureViewOptions: { type: 'default' },
    style: geometryTypeStyle(geometryType),
    visibleLegend: true,
    opacitySelector: false,
});

export const loadLayerInfos = (geometryTypeStyle: GeometryTypeStyle) => {
    const geometryTypes = LAYERINFO_IDS as GeometryType[]
    const layers = geometryTypes.map(layerTemplate(geometryTypeStyle))
    dispatch('data/layerinfos', state => state.concat(layers))
};

// creates a unique template map to display geometries and projects
export const mapTemplate = (): IMapInfo => ({
    baseLayer: 'urbis.irisnet.be/urbis_gray',
    id: MAP_SELECT_ID,
    id_origin: MAP_SELECT_ID,
    url: `/dev/null/`,
    lastModified: 1523599299611,
    status: 'published',
    title: { fr: '', nl: '', en: '' },
    description: { fr: '', nl: '', en: '' },
    categories: [],
    attachments: [],
    layers: LAYERINFO_IDS,
});

export const selectMetadataForGeometryWrite =
    (mapName: string) => (md: Inspire) => {
        assign('component/ui/form/write/geometry/md', md.id);
        loadLayer(mapName)(md);
    };

export const clearMetadataForGeometryWrite = (mapName: string) => {
    assign('component/ui/form/write/geometry/md', null);
    removeLayerAll(mapName);
};

const setInteraction = dispatchK('port/map/interaction');

export const scalelineEvents = scaleEventsFactory(dispatchK('port/map/scale'));
export const viewEvents = viewEventsFactory(dispatchK('port/map/view'));

export const updateMapView = viewEvents.updateMapView;

export const startMark = () =>
    setInteraction(s => {
        if (s.label === 'mark') {
            return { ...s, state: { ...s.state, started: true } };
        }
        return s;
    });

export const endMark = () => restoreInteraction();

const saveInteraction = () =>
    assign('port/component/geocoder/interaction/save', {
        ...query('port/map/interaction'),
    });

const restoreInteraction = () =>
    assign('port/map/interaction', {
        ...query('port/component/geocoder/interaction/save'),
    } as Interaction);

export const putMark = (coordinates: Coordinate) => {
    saveInteraction();
    setInteraction(() => ({
        label: 'mark',
        state: {
            started: false,
            endTime: Date.now() + 2000,
            coordinates,
        },
    }));
};

export const startEditGeometry = (geometryType: GeometryType) =>
    assign('port/map/interaction', {
        label: 'create',
        state: { geometryType },
    });
export const startModifyGeometry = (geometryType: GeometryType) =>
    assign('port/map/interaction', {
        label: 'modify',
        state: { geometryType, selected: 1, centerOnSelected: true },
    });

export const startSelectGeometry = () =>
    assign('port/map/interaction', {
        label: 'select',
        state: { selected: null },
    });

export const setMiniMap = (k: string, m: MiniStep) =>
    dispatch('port/map/minimap', c => ({ ...c, [k]: m }));

export const clearMiniMap = () => assign('port/map/minimap', {});

export const updateGeocoderTerm = (address: string) => {
    dispatch('port/component/geocoder', state => ({ ...state, address }));
};

export const updateGeocoderResponse = (
    serviceResponse: IUgWsResponse | null
) => {
    dispatch('port/component/geocoder', state => {
        state.serviceResponse = serviceResponse;
        return state;
    });
};

export const unfoldGeocoder = () => {
    dispatch('port/component/geocoder', state => {
        state.folded = false;
        return state;
    });
};

export const foldGeocoder = () => {
    dispatch('port/component/geocoder', state => {
        state.folded = true;
        return state;
    });
};

export const addWriteLayer = (unit: InformationUnitName, fieldName: string) =>
    getFeatureFromInput(unit, fieldName).map(feature => {
        const fetchData: FetchData = () =>
            right(some({ type: 'FeatureCollection', features: [feature] }));
        const layerInfo = layerInfoOption(unit, feature.geometry.type);
        addLayer(unit, layerInfo, fetchData);
    });

export const removeWriteLayer = (unit: InformationUnitName) =>
    removeLayer(unit, getWriteLayerId(unit));
