import { getApiUrl } from 'sdi/app';
import { assign } from 'sdi/shape';
import {
    fetchTermList,
    fetchDomainList,
    fetchDomainMappingList,
} from 'angled-core/remote/universe';


export const loadTerms =
    () => fetchTermList(getApiUrl('geodata/angled/r/term'))
        .then(terms => assign('data/terms', terms));

export const loadDomains =
    () => fetchDomainList(getApiUrl('geodata/angled/r/domain'))
        .then(domains => assign('data/domains', domains));

export const loadDomainMappings =
    () => fetchDomainMappingList(getApiUrl('geodata/angled/r/domain_mapping'))
        .then(ms => assign('data/domain-mappings', ms));


export const loadUniverse =
    () =>
        loadDomains()
            .then(loadTerms)
            .then(loadDomainMappings);
            