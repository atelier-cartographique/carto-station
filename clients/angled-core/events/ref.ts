import * as debug from 'debug';

import { getApiUrl, getUserId } from 'sdi/app';
import { assign, assignK, dispatchAsync } from 'sdi/shape';
import { MessageRecord } from 'sdi/source';

import {
    fetchContactList,
    fetchFundingOrgList,
    fetchLocalityList,
    fetchSiteList,
    fetchTeamList,
    fetchTeamMemberList,
    fetchUserList,
    fetchNova,
    postFundingOrg,
    postSite,
    postContact,
    postTeam,
    fetchTagList,
    fetchAudienceList,
    fetchSchoolSite,
} from 'angled-core/remote/ref';
import {
    FundingOrg,
    Site,
    Contact,
    TeamMember,
    Nova,
    NovaLoader,
    TeamMemberRole,
} from 'angled-core/ref';
import {
    getFundingOrgList,
    getSiteList,
    getContactList,
    getNovaList,
} from 'angled-core/queries/ref';
import { InformationUnitName } from 'angled-core/ui';
import { setFormInput } from 'angled-core/events/ui';
import { fromNullable } from 'fp-ts/lib/Option';

const logger = debug('sdi:app/angled-core/events/ref');

export const loadLocalityAll = () =>
    fetchLocalityList(getApiUrl('geodata/angled/ref/locality/')).then(items =>
        assign('data/locality', items)
    );

export const loadContactAll = () =>
    fetchContactList(getApiUrl('geodata/angled/ref/contact/')).then(items =>
        assign('data/contact', items)
    );

export const loadFundingOrgAll = () =>
    fetchFundingOrgList(getApiUrl('geodata/angled/ref/funding_org/')).then(
        items => assign('data/funding-org', items)
    );

export const loadSiteAll = () =>
    fetchSiteList(getApiUrl('geodata/angled/ref/site/')).then(items =>
        assign('data/site', items)
    );

export const loadTeamMemberAll = () =>
    fetchTeamMemberList(getApiUrl('geodata/angled/ref/teammember/')).then(
        items => assign('data/team-member', items)
    );

export const loadTeamAll = () =>
    fetchTeamList(getApiUrl('geodata/angled/ref/team/')).then(items =>
        assign('data/team', items)
    );

export const loadTagAll = () =>
    fetchTagList(getApiUrl('geodata/angled/ref/tag/')).then(items =>
        assign('data/tags', items)
    );

export const loadAudienceAll = () =>
    fetchAudienceList(getApiUrl('geodata/angled/r/audience')).then(items =>
        assign('data/audiences', items)
    );

export const loadUserAll = () =>
    fetchUserList(getApiUrl('users')).then(items => {
        assign('data/users', items);
        getUserId()
            .chain(userId =>
                fromNullable(items.find(({ id }) => userId === id))
            )
            .map(assignK('data/user'));
    });

export const loadNovaAndExecuteCallback = (
    ref: string,
    callback: (novaData: Nova) => void,
    callbackError: () => void
) => {
    const refWOSlash = ref.replace('/', '_').replace('/', '_'); // c'est la merde todo fixme réparer
    fetchNova(getApiUrl('geodata/angled/ref/nova/' + refWOSlash))
        .then(novaData => callback(novaData))
        .catch(_ => callbackError());
};

const novaEquals = (a: NovaLoader, b: NovaLoader) => {
    // todo fixme a t on vraiment besoin de cette fonct ? à tester
    if (a != null && b != null) {
        if (a.status == b.status) {
            if (a.status != 'LOADED') {
                return true;
            } else {
                return a.ref == b.ref;
            }
        } else {
            return false;
        }
    } else {
        return a == null && b == null; // return true if both are null
    }
};

export const updateOrAddNova =
    // if refNova in data/Site -> update ; otherwise data to the list
    (refNova: string, data: NovaLoader) => {
        const novaList = getNovaList();
        fromNullable(novaList.find(n => n.ref == refNova)).foldL(
            () => {
                assign('data/nova', novaList.concat([data]));
            },
            novaForRefSome => {
                if (
                    novaForRefSome.ref != null &&
                    !novaEquals(novaForRefSome, data)
                ) {
                    assign(
                        'data/nova',
                        novaList.map(dn => {
                            if (dn.ref == refNova) {
                                return data;
                            } else {
                                return dn;
                            }
                        })
                    );
                }
            }
        );
    };

export const setNovaAsLoading = (
    refNova: string // always works !
) => updateOrAddNova(refNova, { ref: refNova, data: null, status: 'LOADING' });

export const setNovaAsLoaded = (
    refNova: string,
    data: Nova // only work if data/nova has the same ref (means that setNovaAsLoading must be used before)
) => updateOrAddNova(refNova, { ref: refNova, data, status: 'LOADED' });

export const setNovaAsLoadingError = (
    refNova: string // only work if data/nova has the same ref (means that setNovaAsLoading must be used before)
) => updateOrAddNova(refNova, { ref: refNova, data: null, status: 'ERROR' });

export const addFundingOrgToList = (newItem: FundingOrg) =>
    assign('data/funding-org', getFundingOrgList().concat([newItem]));

export const addSiteToList = (newItem: Site) =>
    assign('data/site', getSiteList().concat([newItem]));

export const addContactToList = (newItem: Contact) =>
    assign('data/contact', getContactList().concat([newItem]));

export const createFundingOrg = (name: string) => {
    const data: Partial<FundingOrg> = {
        name,
    };
    postFundingOrg(getApiUrl('geodata/angled/ref/funding_org/'), data).then(
        newItem => addFundingOrgToList(newItem)
    ); // todo fixme selectionner l'élément
};

export const createSite = (name: MessageRecord) => {
    const data: Partial<Site> = {
        name,
    };
    postSite(getApiUrl('geodata/angled/ref/site/'), data).then(newItem =>
        addSiteToList(newItem)
    ); // todo fixme selectionner l'élément
};

export const createTeam = (
    teamMembers: [number, TeamMemberRole][],
    unit: InformationUnitName,
    fieldName: string
) => {
    const members: Omit<TeamMember, 'id'>[] = teamMembers.map(
        ([member, role]) => ({ member, role })
    );
    const data = { members };
    postTeam(getApiUrl('geodata/angled/ref/team/'), data).then(newItem => {
        loadTeamAll();
        setFormInput(unit, fieldName)(newItem.id);
    });
};

export const createContact = (
    name: string,
    unit: InformationUnitName,
    fieldName: string
) => {
    const data: Partial<Contact> = {
        name,
        info: '',
    };
    return postContact(getApiUrl('geodata/angled/ref/contact/'), data).then(
        newItem => {
            addContactToList(newItem);
            console.log(`test contact: ${unit}, ${fieldName} ${newItem.id}`);
            setFormInput(unit, fieldName)(newItem.id);
        }
    );
};

export const loadAllRefs = () =>
    Promise.resolve()
        .then(loadLocalityAll)
        .then(loadContactAll)
        .then(loadFundingOrgAll)
        .then(loadSiteAll)
        // .then(loadTeamMemberAll)
        // .then(loadTeamAll) I dont know where we use that, but it's really a drain on the server side
        .then(loadTagAll)
        .then(loadUserAll);

const loadDebug = (p: () => Promise<void>) => (): Promise<void> =>
    p().catch(err => logger(`load error ${err}`));

export const loadAllRefsDebug = () =>
    Promise.resolve()
        .then(loadDebug(loadLocalityAll))
        .then(loadDebug(loadContactAll))
        .then(loadDebug(loadFundingOrgAll))
        .then(loadDebug(loadSiteAll))
        .then(loadDebug(loadTeamAll))
        .then(loadDebug(loadTagAll))
        .then(loadDebug(loadUserAll))
        .then(loadDebug(loadAudienceAll));

export const loadSchoolSite = (id: number) =>
    dispatchAsync('data/school-sites', ss =>
        fromNullable(ss.find(s => s.gid === id)).foldL(
            () =>
                fetchSchoolSite(
                    getApiUrl(`geodata/angled/ref/school_site/${id}/`)
                ).then(s => ss.concat(s)),
            () => Promise.resolve(ss)
        )
    );

logger('loaded');
