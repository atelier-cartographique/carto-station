import { query, queryK } from 'sdi/shape';
import {LAYERINFO_IDS, MAP_SELECT_ID} from 'angled-core/map';
import { none, some, fromNullable, Option } from 'fp-ts/lib/Option';
import { scopeOption, fnOpt1 } from 'sdi/lib';
import { getWriteUnit, getFormInput } from './ui';
import { InformationUnitName, makeValueMapper } from 'angled-core/ui';
import {
    GeometryType,
    Inspire,
    StyleConfig,
    ILayerInfo,
    IMapInfo,
    Feature,
} from 'sdi/source';
import { nameToCode } from 'sdi/components/button/names';
import { ensureArray } from 'sdi/util';
import { index } from 'fp-ts/lib/Array';

export const unitToGeometryType = (
    u: InformationUnitName | Readonly<InformationUnitName>
): Option<GeometryType> => {
    switch (u) {
        case 'point':
            return some('MultiPoint');
        case 'line':
            return some('MultiLineString');
        case 'polygon':
            return some('MultiPolygon');
        default:
            return none;
    }
};

export const getMapInfo = () => null;
export const getMapLayerInfo = () => query('data/layerinfos').concat()

export const getCurrentBaseLayerName = () => {
    const mid = MAP_SELECT_ID;
    const map = query('data/maps').find(m => m.id === mid);
    if (map) {
        return index(0, ensureArray(map.baseLayer));
    }
    return none;
};

export const getCurrentBaseLayerOpt = () =>
    scopeOption()
        .let('name', getCurrentBaseLayerName())
        .let(
            'baseLayer',
            fnOpt1(({ name }) => {
                const parts = name.split('/');
                if (parts.length === 2) {
                    const serviceName = parts[0];
                    const layerName = parts[1];
                    return fromNullable(
                        query('data/baselayers').find(
                            service => service.id === serviceName
                        )
                    )
                        .chain(service =>
                            fromNullable(
                                service.layers.find(
                                    layer => layer.codename === layerName
                                )
                            )
                        )
                        .toNullable();
                }
                return null;
            })
        )
        .pick('baseLayer');

export const getCurrentBaseLayer = () => getCurrentBaseLayerOpt().toNullable();

export const getMetadataList = () => query('data/datasetMetadata');

export const getSelectedMetadataId = () =>
    fromNullable(query('component/ui/form/write/geometry/md'));

export const getSelectedMetadata = () =>
    getSelectedMetadataId().chain(id =>
        fromNullable(getMetadataList().find(md => md.id === id))
    );

export const getInteraction = queryK('port/map/interaction');

export const getView = queryK('port/map/view');

export const getMiniMap = (k: string) =>
    fromNullable(query('port/map/minimap')[k]);

export const toolsGeocoder = () => query('port/component/geocoder');

// WRITE MAP

export const getWriteLayerId = (unit: InformationUnitName) =>
    `angled-ui-write-${unit}`;

const metadataTemplate = (
    unit: InformationUnitName,
    geometryType: GeometryType
): Inspire => ({
    id: getWriteLayerId(unit),
    geometryType,
    resourceTitle: { fr: geometryType, nl: geometryType, en: geometryType },
    resourceAbstract: { fr: geometryType, nl: geometryType, en: geometryType },
    uniqueResourceIdentifier: geometryType,
    topicCategory: [],
    keywords: [],
    geographicBoundingBox: { west: 0.0, north: 0.0, east: 0.0, south: 0.0 },
    temporalReference: { creation: Date(), revision: Date() },
    responsibleOrganisation: [1],
    metadataPointOfContact: [1],
    metadataDate: Date(),
    published: false,
    dataStreamUrl: null,
    maintenanceFrequency: 'unknown',
});

const geometryTypeStyle = (geometryType: GeometryType): StyleConfig => {
    switch (geometryType) {
        case 'Point':
        case 'MultiPoint':
            return {
                kind: 'point-simple',
                marker: {
                    codePoint: nameToCode('circle'),
                    size: 12,
                    color: 'rgb(223,88,68)',
                },
            };
        case 'LineString':
        case 'MultiLineString':
            return {
                kind: 'line-simple',
                strokeColor: 'rgb(223,88,68)',
                strokeWidth: 1,
                dash: [],
            };
        case 'Polygon':
        case 'MultiPolygon':
            return {
                kind: 'polygon-simple',
                fillColor: 'transparent',
                pattern: {
                    angle: 45,
                    color: 'rgb(223,88,68)',
                    width: 0.5,
                },
                strokeColor: 'rgb(223,88,68)',
                strokeWidth: 0,
            };
    }
};

// todo rename to generic name if used in angled-* or move to angled-project
const layerTemplateWrite = (
    unit: InformationUnitName,
    geometryType: GeometryType
): ILayerInfo => ({
    id: getWriteLayerId(unit),
    legend: null,
    group: null,
    metadataId: getWriteLayerId(unit),
    visible: true,
    featureViewOptions: { type: 'default' },
    style: geometryTypeStyle(geometryType),
    visibleLegend: true,
    opacitySelector: false,
});

const mapTemplateWrite = (): IMapInfo => ({
    baseLayer: 'urbis.irisnet.be/urbis_gray',
    id: MAP_SELECT_ID,
    id_origin: MAP_SELECT_ID,
    url: `/dev/null/write/`,
    lastModified: 1523599299611,
    status: 'published',
    title: { fr: '', nl: '', en: '' },
    description: { fr: '', nl: '', en: '' },
    categories: [],
    attachments: [],
    layers: LAYERINFO_IDS,
});

const mapTemplateSelect = (md: Inspire): IMapInfo => ({
    baseLayer: 'urbis.irisnet.be/urbis_gray',
    id: MAP_SELECT_ID,
    id_origin: MAP_SELECT_ID,
    url: `/dev/null/write/`,
    lastModified: 1523599299611,
    status: 'published',
    title: { fr: '', nl: '', en: '' },
    description: { fr: '', nl: '', en: '' },
    categories: [],
    attachments: [],
    layers: [md.uniqueResourceIdentifier],
});

export const layerInfoOption =
    (unit: InformationUnitName, geometryType: GeometryType) => () =>
        some({
            name: { fr: geometryType, nl: geometryType, en: geometryType },
            info: layerTemplateWrite(unit, geometryType),
            metadata: metadataTemplate(unit, geometryType),
        });

// const fetchData =
//     (geometryType: GeometryType): FetchData =>
//         () => {
//             let filter = (_p: Project) => false;
//             switch (geometryType) {
//                 case 'Point':
//                 case 'MultiPoint': filter = isPoint; break;
//                 case 'LineString':
//                 case 'MultiLineString': filter = isLine; break;
//                 case 'Polygon':
//                 case 'MultiPolygon': filter = isPolygon; break;
//             }
//             const projects = toGeoJSON(query('data/projects').filter(filter));
//             return right(some(projects));
//         };

export const getWriteMapInfo = () => {
    const interaction = getInteraction();
    if (interaction.label === 'select') {
        return getSelectedMetadata()
            .map(md => mapTemplateSelect(md))
            .toNullable();
    }
    return mapTemplateWrite()
};

// todo get geometry type from unit in layerTemplateWrite
export const getWriteMapLayers = (): ILayerInfo[] => getWriteUnit()
        .chain(([unit]) =>
            unitToGeometryType(unit).map(geometryType => ({
                unit,
                geometryType,
            }))
        ).map(({unit, geometryType}) => [layerTemplateWrite(unit, geometryType)])
        .getOrElse([])

const geometryMapper = makeValueMapper<Feature | null, 'geometry'>(
    'geometry',
    geometry => ({
        type: 'Feature',
        id: 1,
        properties: {},
        geometry,
    }),
    null
);

export const getFeatureFromInput = (
    unit: InformationUnitName,
    fieldName: string
) =>
    getFormInput(unit, fieldName).chain(field =>
        fromNullable(geometryMapper(field))
    );

/// END OF WRITE LAYER
