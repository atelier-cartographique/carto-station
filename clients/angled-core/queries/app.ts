import { InformationUnitName } from 'angled-core/ui';
import { getAliasOption } from 'sdi/app';
import { Translated } from 'sdi/locale';


export const unitDisplayName =
    (unit: InformationUnitName) =>
        getAliasOption(`unit/${unit}`).getOrElse(unit as Translated);

export const fieldDisplayName =
    (unit: InformationUnitName, fieldname: string) =>
        getAliasOption(`field/${unit}/${fieldname}`).getOrElse(fieldname as Translated);