import { query, queryK } from 'sdi/shape';
import { Subscription, Notif, ProjectNotification } from 'angled-core/sub';
import { some, none, fromNullable } from 'fp-ts/lib/Option';
import { catOptions, sort } from 'fp-ts/lib/Array';
import { fromCompare, ordNumber } from 'fp-ts/lib/Ord';
import { remoteToOption } from 'angled-core/../sdi/source';

const findProjectSub = (
    pid: number,
    xs: Subscription[] | Readonly<Subscription[]>
) => {
    for (const s of xs) {
        if (s.tag === 'sub/project' && s.project === pid) {
            return some(s);
        }
    }
    return none;
};

export const getSubscriptionForProject = (pid: number) =>
    findProjectSub(pid, getSubscriptions().getOrElse([]));

export const getSubscriptionsRemote = queryK('data/subscriptions');
export const getSubscriptions = () => remoteToOption(getSubscriptionsRemote());

export const findSubscription = (sid: number) =>
    getSubscriptions().chain(xs =>
        fromNullable(xs.find(s => s.id === sid))
    );

export const findProjectSubscription = (sid: number) => {
    const subs = remoteToOption(getSubscriptionsRemote()).getOrElse([]);
    for (const s of subs) {
        if (s.tag === 'sub/project' && s.id === sid) {
            return some(s);
        }
    }
    return none;
};

const notifOrd = fromCompare<Notif>((a, b) => {
    return ordNumber.compare(
        Date.parse(b.created_at),
        Date.parse(a.created_at)
    );
});

const notifSort = sort(notifOrd);

export const getNotificationsRemote = () => query('data/notifications');

export const getNotifsOpt = () => remoteToOption(getNotificationsRemote());

export const getUnreadNotifications = () =>
    notifications().filter(n => n.mark_view === false);

export const notifications = () => notifSort(getNotifsOpt().getOrElse([]));

export type Action = {
    tag: string;
    notifications: ProjectNotification[];
};

export type DisplayProjectNotificationStatus =
    | 'read-none'
    | 'read-full'
    | 'read-partial';
export type DisplayProjectNotification = {
    project: number;
    read: DisplayProjectNotificationStatus;
    actions: Action[];
};

const getProjectNotifications = () =>
    notifications().filter(
        n => n.tag === 'notif/project'
    ) as ProjectNotification[];

const addNotifToDPN = (
    dpn: DisplayProjectNotification,
    notif: ProjectNotification
) => {
    fromNullable(dpn.actions.find(({ tag }) => tag === notif.action)).foldL(
        () =>
            dpn.actions.push({
                tag: notif.action,
                notifications: [notif],
            }),
        action => action.notifications.push(notif)
    );
    if (
        (notif.mark_view && dpn.read === 'read-none') ||
        (!notif.mark_view && dpn.read === 'read-full')
    ) {
        dpn.read = 'read-partial';
    }

    return dpn;
};

export const getDisplayProjectNotif = (): DisplayProjectNotification[] => {
    const notifs = getProjectNotifications();
    const subToProject = catOptions(
        notifs.map(({ subscription }) =>
            findProjectSubscription(subscription).map(({ project }) => ({
                subscription,
                project,
            }))
        )
    );

    const getProject = (sub: number) =>
        fromNullable(subToProject.find(sp => sp.subscription === sub)).map(
            ({ project }) => project
        );

    return notifs.reduce<DisplayProjectNotification[]>(
        (acc, notif) =>
            getProject(notif.subscription)
                .map(project =>
                    fromNullable(
                        acc.find(dpn => dpn.project === project)
                    ).foldL(
                        () =>
                            acc.concat({
                                project,
                                read: notif.mark_view
                                    ? 'read-full'
                                    : 'read-none',
                                actions: [
                                    {
                                        tag: notif.action,
                                        notifications: [notif],
                                    },
                                ],
                            }),
                        dpn =>
                            acc
                                .filter(i => i.project !== project)
                                .concat(addNotifToDPN(dpn, notif))
                    )
                )
                .getOrElse(acc),
        []
    );
};

export const getDisplayUnreadnotification = () =>
    getDisplayProjectNotif().filter(n => n.read !== 'read-full');

export const getDisplayReadnotification = () =>
    getDisplayProjectNotif().filter(n => n.read === 'read-full');

export const getTaggedTag = (taggedId: number) =>
    fromNullable(query('data/tagged').find(t => t.id === taggedId));

export const getAudienceFromUnit = (audienceId: number) =>
    fromNullable(query('data/audiences').find(a => a.id === audienceId));
