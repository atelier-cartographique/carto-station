

import { Project } from '../ui';
import { Feature, FeatureCollection } from 'sdi/source';


export type DetailProject<K extends keyof Project> = {
    readonly [k in K]: Project[K]
};

export type ProjectWithPoint = DetailProject<'point'> & Project;
export type ProjectWithLine = DetailProject<'line'> & Project;
export type ProjectWithPolygon = DetailProject<'polygon'> & Project;

export type ProjectWithGeometry =
    | ProjectWithPoint
    | ProjectWithLine
    | ProjectWithPolygon;



export function isPoint(p: Project): p is ProjectWithPoint {
    return (p.point !== undefined);
}
export function isLine(p: Project): p is ProjectWithLine {
    return (p.line !== undefined);
}
export function isPolygon(p: Project): p is ProjectWithPolygon {
    return (p.polygon !== undefined);
}

export function isGeometric(p: Project): p is ProjectWithGeometry {
    return (p.point !== undefined) || (p.line !== undefined) || (p.polygon !== undefined);
}

// type MultiGeometryType = 'MultiPoint' | 'MultiLineString' | 'MultiPolygon';

// const getGeometryType =
//     (p: ProjectWithGeometry): MultiGeometryType => {
//         if (p.point) {
//             return 'MultiPoint'
//         }
//         else if (p.line) {
//             return 'MultiLineString'
//         }
//         return 'MultiPolygon'
//     }


export const projectToGeoJSON =
    (p: ProjectWithGeometry): Feature => {
        const point = p.point;
        const line = p.line;
        const polygon = p.polygon;
        if (point) {
            return {
                id: p.id,
                type: 'Feature',
                geometry: {
                    type: 'MultiPoint',
                    coordinates: point.geom.coordinates,
                },
                properties: null,
            };
        }
        else if (line) {
            return {
                id: p.id,
                type: 'Feature',
                geometry: {
                    type: 'MultiLineString',
                    coordinates: line.geom.coordinates,
                },
                properties: null,
            };
        }
        else if (polygon) {
            return {
                id: p.id,
                type: 'Feature',
                geometry: {
                    type: 'MultiPolygon',
                    coordinates: polygon.geom.coordinates,
                },
                properties: null,
            };
        }
        else {
            throw ('err'); // FIXME
        }
    };


export const toGeoJSON =
    (ps: Project[] | Readonly<Project[]>): FeatureCollection => ({
        type: 'FeatureCollection',
        features: ps.filter(isGeometric).map(projectToGeoJSON),
    });

