import { Subscription, Notif } from 'angled-core/sub';
import { Tagged } from 'angled-core/ref';
import { remoteNone, RemoteResource } from 'angled-core/../sdi/source/remote';


declare module 'sdi/shape' {
    export interface IShape {
        'data/subscriptions': RemoteResource<Subscription[]>;
        'data/notifications': RemoteResource<Notif[]>;
        'data/tagged': Tagged[];
    }
}

export const defaultSub = () => ({
    'data/subscriptions': remoteNone,
    'data/notifications': remoteNone,
    'data/tagged': [],
});
