
import { TermList, DomainList, DomainMappingList } from 'angled-core/ref';

// State Augmentation
declare module 'sdi/shape' {
    export interface IShape {
        'data/terms': TermList;
        'data/domains': DomainList;
        'data/domain-mappings': DomainMappingList;
    }
}


export const defaultUniverse =
    () => ({
        'data/terms': [],
        'data/domains': [],
        'data/domain-mappings': [],
    });
