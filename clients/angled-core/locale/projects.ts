import * as texts from './marked-texts';

export default {
    appName: {
        fr: 'Projets',
        nl: 'Projecten', // nldone
        en: 'Projects',
    },

    appDescriptionProjectsList: {
        fr: texts.projectHomeTextFR,
        nl: texts.projectHomeTextNL,
        en: `The Plans & Projects application allows you to encode and visualize information relating to project plans and real estate projects.
All information history is stored and available for consultation.
A system of permissions allows you to define to whom the encoded information is addressed.`,
    },

    newProjectTitle: {
        fr: 'Ajouter un projet',
        nl: 'Een project toevoegen', // nldone
        en: 'Add a project',
    },
    saveProject: {
        fr: 'Enregistrer le projet',
        nl: 'Bewaar het project', // nldone
        en: 'Save the project',
    },

    insertNameFR: {
        fr: 'Entrez un nom en français',
        nl: 'Voer een naam in het Frans in', // nldone
        en: 'Enter a name in French',
    },
    insertNameNL: {
        fr: 'Entrez un nom en néerlandais',
        nl: 'Voer een naam in het Nederlands in', // nldone
        en: 'Enter a name in Dutch',
    },

    searchProjectPlaceholder: {
        fr: 'Recherche de projets',
        nl: 'Projecten zoeken', // nlTODO
        en: 'Search for projects',
    },
    labelUnit_name: {
        fr: 'Nom de projet',
        nl: 'Naam van het project', // nldone
        en: 'Name',
    },
    labelUnit_capakey: {
        fr: 'Parcelle cadastrale',
        nl: 'Kadastraal perceel', // nldone
        en: 'Cadastral parcel',
    },
    labelUnit_nova: {
        fr: 'Référence NOVA',
        nl: 'NOVA nummer', // nldone
        en: 'NOVA number',
    },
    labelUnit_description: {
        fr: 'Description',
        nl: 'Beschrijving',
        en: 'Description',
    },

    chooseResult: {
        fr: 'Choisissez un projet',
        nl: 'Kies een project', // nldone
        en: 'Choose a project',
    },

    appDescriptionForm: {
        fr: `Les informations par projet étant nombreuses, le choix d'un profil de visualisation et d'encodage doit être fait, et peut être modifié à tout moment dans l'application.
Chaque profil de visualisation donne accès à une sélection des informations disponibles.
Ces profils peuvent être définis au gré des besoins par les personnes en charge de l'administration de la plateforme.`,
        nl: `Omdat er per project veel informatie is, moet er een visualisatie- en coderingsprofiel worden gekozen, dat op elk moment in de toepassing kan worden aangepast.
Elk visualisatieprofiel geeft toegang tot een selectie van beschikbare informatie.
Deze profielen kunnen op verzoek van de verantwoordelijken van de platformadministratie worden gedefinieerd.`, // nlTODO
        en: `Since there is a lot of information per project, the choice of a visualization and encoding profile must be made, and can be modified at any time in the application.
Each visualization profile gives access to a selection of available information.
These profiles can be defined as required by the people in charge of the platform administration.`,
    },

    forSearchingProjects: {
        fr: 'Pour chercher un projet, entrez un nom, un numéro de cadastre ou une référence NOVA ci-dessous',
        nl: 'Om naar een project te zoeken, voert u hieronder een naam, een kadastraal nummer of een NOVA nummer in.', // nldone
        en: 'For searching a project, enter a project name or a NOVA or cadastre number herebelow',
    },
    foundProjects: {
        fr: '{value, plural, =1 {projet identifié} other {projets identifiés}}',
        nl: '{value, plural, =1 {gevonden project} other {gevonden projecten}}', // nldone
        en: '{value, plural, =1 {project found} other {projects found}}',
        parameters: {
            value: 1,
        },
    },
    selectProfil: {
        fr: 'Visualisation',
        nl: 'Visualisatie', // nldone
        en: 'Visualisation',
    },

    selectedProject: {
        fr: 'Projet sélectionné',
        nl: 'Geselecteerd project', // nlTODO
        en: 'Selected project',
    },

    backToSearch: {
        fr: 'retour à la recherche',
        nl: 'terug naar zoeken', // nldone
        en: 'back to search',
    },

    addedOn: {
        fr: 'Ajouté le',
        nl: 'Toegevoegd op', // nldone
        en: 'Added on',
    },
    addedOnMulti: {
        fr: 'Ajoutés',
        nl: 'Toegevoegd', // nldone
        en: 'Added',
    },
    by: {
        fr: 'par',
        nl: 'door', // nldone
        en: 'by',
    },
    fromDate: {
        fr: 'du',
        nl: 'van', // nldone
        en: 'from',
    },
    toDate: {
        fr: 'au',
        nl: 'tot', // nldone
        en: 'to',
    },

    selectUnit: {
        fr: 'Sélectionnez un élément à mettre à jour',
        nl: 'Selecteer een item om te updaten', // nldone
        en: 'Select an item to update',
    },

    updateUnit: {
        fr: 'Mettre à jour',
        nl: 'Update', // nldone
        en: 'Update',
    },

    updateData: {
        fr: 'Mettre à jour les données',
        nl: 'De gegevens updaten', // nldone
        en: 'Update the data',
    },

    selectData: {
        fr: 'Sélection des données',
        nl: 'Selectie van de gegevens', // nldone
        en: 'Data selection',
    },

    errorDisplayProject: {
        fr: "Désolé, une erreur empêche l'affichage de ce projet",
        nl: 'Sorry, een foutmelding verhindert de weergave van dit project', // nldone
        en: 'Sorry, an error prevents the display of this project',
    },

    widget: {
        fr: 'widgets',
        nl: 'widgets', // nldone
        en: 'widgets',
    },

    commitTitle: {
        fr: 'Mettre à jour',
        nl: 'Updaten', // nldone
        en: 'Update',
    },

    commitPreview: {
        fr: 'Validation des mises à jour',
        nl: 'Bevestiging van de updates', // nldone
        en: 'Validation of the update',
    },

    currentInfo: {
        fr: 'Version en cours',
        nl: 'Huidige versie', // nldone
        en: 'Current version',
    },

    pastInfo: {
        fr: 'Version en date du ',
        nl: 'Versie gedateerd op', // nldone
        en: 'Version as of ',
    },

    validateUpdate: {
        fr: 'Pré-valider',
        nl: 'Prevalideren', // nldone
        en: 'Pre-validate',
    },

    clearUpdate: {
        fr: 'Annuler et quitter',
        nl: 'Annuleren en sluiten', // nldone
        en: 'Cancel and close',
    },

    confirmClearUpdate: {
        fr: "Voulez-vous quitter l'éditeur sans sauvegarder les éventuelles modifications ?",
        nl: 'Wilt u de editor verlaten zonder wijzigingen op te slaan?', // nldone
        en: 'Do you want to leave the editor without saving any changes?',
    },

    reviewToPublish: {
        fr: 'Réviser et publier',
        nl: 'Controleren en publiceren', // nldone
        en: 'Review and publish',
    },

    backToEditor: {
        fr: "Retour à l'éditeur",
        nl: 'Terug naar de editor', // nldone
        en: 'Back to editor',
    },

    saveAndPublish: {
        fr: 'Enregistrer et publier',
        nl: 'Opslaan en publiceren', // nldone
        en: 'Save and publish',
    },

    updateReview: {
        fr: 'Révision des nouvelles informations',
        nl: 'Controle van nieuwe informatie', // nldone
        en: 'Review of new information',
    },

    triggerTimeline: {
        fr: 'Voir la ligne du temps',
        nl: 'Zie tijdlijn', // nldone
        en: 'Show the timeline',
    },

    panel: {
        fr: 'Panneau',
        nl: 'Paneel', // nldone
        en: 'Panel',
    },

    selectDomain: {
        fr: 'Sélectionnez un domaine',
        nl: 'Selecteer een domeinnaam', // nldone
        en: 'Select a domain',
    },

    adjustCurrentInfo: {
        fr: 'Supprimer des éléments de la version en cours',
        nl: 'Items van de huidige versie verwijderen', // nldone
        en: 'Delete items from the current version',
    },

    addNewInfoToCurrentInfo: {
        fr: 'Ajouter des éléments à la version en cours',
        nl: 'Items toevoegen aan de huidige versie', // nldone
        en: 'Add items to the current verion',
    },

    selectUnitToEdit: {
        fr: '',
        nl: '',
        en: '',
    },

    emptyUnit: {
        fr: "Cette information n'est pas encore encodée",
        nl: 'Deze informatie is nog niet gecodeerd.', // nldone
        en: 'This information is not yet encoded',
    },

    address: {
        fr: 'Adresse',
        nl: 'Adres', // nldone
        en: 'Address',
    },

    street: {
        fr: 'Rue',
        nl: 'Straat', // nldone
        en: 'Street',
    },

    streetNumber: {
        fr: 'Numéro',
        nl: 'Nummer', // nldone
        en: 'Number',
    },

    locality: {
        fr: 'Localité',
        nl: 'Plaatsnaam', // nldone
        en: 'Locality',
    },

    NovaID: {
        fr: 'Identifiant NOVA',
        nl: 'NOVA-ID', // nldone
        en: 'NOVA-ID',
    },

    fundingOrganisation: {
        fr: 'Organisme subsidiant',
        nl: 'Financieringsinstantie', // nldone
        en: 'Funding body',
    },

    site: {
        fr: 'Site',
        nl: 'Site', // nldone
        en: 'Site',
    },

    contact: {
        fr: 'Nom du contact',
        nl: 'Naam contactpersoon', // nldone
        en: 'Contact name',
    },

    team: {
        fr: 'Équipe',
        nl: 'Groep', // nldone
        en: 'Team ',
    },

    teamName: {
        fr: 'Nom du groupement',
        nl: 'Naam van de groep', // nldone
        en: 'Team name',
    },

    'helptext:newFkEntry': {
        fr: 'Encoder une nouvelle entrée',
        nl: 'Codering van een nieuw item', // nldone
        en: 'Encoding a new entry',
    },

    'helptext:newContactEntry': {
        fr: 'Encoder un nouveau contact',
        nl: 'Codering van een nieuw contact', // nldone
        en: 'Encoding a new contact',
    },

    addNewTeam: {
        fr: 'Nouvelle équipe',
        nl: 'Nieuw team', // nldone
        en: 'New team',
    },

    selectTeamMembers: {
        fr: `Sélection des membres de l'équipe`,
        nl: 'Selectie van teamleden', // nltocheck
        en: 'Selection of team members',
    },

    reconfigureTeam: {
        fr: `Modifier l'équipe`,
        nl: 'Wijzig het team', // nltocheck
        en: 'Modify team',
    },

    'helptext:addNewContactForTeam': {
        fr: 'Au besoin, créer un nouveau contact',
        nl: 'Indien nodig, maak een nieuw contact', // nltocheck
        en: 'If needed, create a new contact',
    },

    'helptext:addNewTeam': {
        fr: 'Créer une équipe avec cette liste de contacts',
        nl: 'Maak een team aan met deze lijst van contacten', // nldone
        en: 'Create a team with this list of contacts',
    },

    'helptext:newTeamCreated': {
        fr: 'Nouvelle équipe créée: ',
        nl: 'Nieuw team opgericht: ', // nltocheck
        en: 'New team created: ',
    },

    'helptext:closeGeom': {
        fr: "Dans le cas d'une ligne ou d'un polygone, double cliquer pour finaliser le dessin de la géométrie.",
        nl: 'In het geval van een lijn of veelhoek, dubbelklik om de tekening van de geometrie te voltooien.', // nltocheck
        en: 'In the case of a line or polygon, double click to finalize the drawing of the geometry. ',
    },

    'helptext:selectGeom': {
        fr: "Pour sélectionner une géométrie existante, commencer par sélectionner la couche de référence concernée, puis cliquer sur l'élément souhaité.",
        nl: 'Om een bestaande geometrie te selecteren, selecteert u eerst de betreffende referentielaag en klikt u vervolgens op het gewenste element.', // nltocheck
        en: 'To select an existing geometry, first select the reference layer concerned, then click on the desired element.',
    },
    'helptext:selectTerm': {
        fr: "Les catégories (Termes & Domaines) sont ajustables dans l'application Termes.",
        nl: 'De categorieën (Termen & Domeinen) zijn aanpasbaar in de Termen toepassing.', // nltocheck
        en: 'The categories (Terms & Domains) are adjustable in the Terms application.',
    },

    startGeomDrawing: {
        fr: 'Créer une géometrie',
        nl: 'Creëer een geometrische vorm', // nldone
        en: 'Create a geometry',
    },

    searchBy: {
        fr: 'Chercher par...',
        nl: 'Zoek via...', // nldone
        en: 'Search on...',
    },

    projectNoName: {
        fr: "Ce projet n'a pas encore de nom.",
        nl: 'Dit project heeft nog geen naam.', // nldone
        en: "This project doesn't have a name yet.",
    },

    projectNoDescription: {
        fr: "Ce projet n'a pas encore de description.",
        nl: 'Dit project heeft nog geen beschrijving.', // nldone
        en: 'This project has no description yet.',
    },

    projectNoImage: {
        fr: "Ce projet n'a pas encore d'image.",
        nl: 'Dit project heeft nog geen beeld.', // nldone
        en: 'This project has no illustration yet.',
    },

    addFkEntry: {
        fr: 'Nouvelle entrée',
        nl: 'Nieuw item', // nldone
        en: 'New entry',
    },

    housingOverview: {
        fr: 'Logement : Synthèse',
        nl: 'Huisvesting: Overzicht : ', // nltocheck
        en: 'Housing: Overview : ',
    },

    noHousingEntry: {
        fr: "Il n'y a pas de logement encodé pour ce projet",
        nl: 'Er is geen gecodeerde behuizing voor dit project.', // nltocheck
        en: 'No housing encoded for this project',
    },

    housingLevels: {
        fr: 'Type de logement public',
        nl: 'Type publieke huisvesting', // nldone
        en: '',
    },

    housingDisposals: {
        fr: 'Typologie des logements',
        nl: 'Type van de woningen', // nldone
        en: '',
    },

    housingCompositions: {
        fr: 'Composition des logements',
        nl: 'Samenstelling van de woningen', // nltocheck
        en: '',
    },

    housingLegislature: {
        fr: 'Législature',
        nl: 'Legislatuur', // nltocheck
        en: 'Legislature',
    },

    housingPriceSquareMeter: {
        fr: 'Prix au m²',
        nl: 'Prijs per m²', // nltocheck
        en: 'Price per m²',
    },

    housingPrice: {
        fr: 'Prix moyen par logement',
        nl: 'Prijs per eenheid', // nltocheck
        en: 'Price per housing',
    },

    manageTags: {
        fr: 'Étiquettes ',
        nl: 'Tags', // nldone
        en: 'Tags',
    },

    audiences: {
        fr: 'Audiences ',
        nl: 'Publieken', // nldone
        en: 'Audiences',
    },
    manageAudiences: {
        fr: 'Gestion des audiences ',
        nl: 'Beheer publieken', // nldone
        en: 'Manage audiences',
    },

    removedUnits: {
        fr: 'Un élément enlevé',
        nl: 'Een item verwijderd', // nldone
        en: 'One removed item',
    },

    computedInfosProjectData: {
        fr: 'Généré automatiquement à partir des données encodées pour ce projet.',
        nl: 'Automatisch gegenereerd op basis van de gegevens die voor dit project zijn gecodeerd.', // nltocheck
        en: 'Automatically generated from the data encoded for this project.',
    },

    computedInfosExternalData: {
        fr: 'Généré automatiquement à partir de données externes.',
        nl: 'Automatisch gegenereerd uit externe gegevens.', // nldone
        en: 'Automatically generated from external data.',
    },

    viewSchoolSite: {
        fr: 'Information du dictionnaire des écoles',
        nl: 'Informatie van de databanken van scholen', // nldone
        en: 'School Dictionary Information',
    },

    viewSSOrganizer: {
        fr: 'Pouvoir organisateur',
        nl: 'Organiserende macht', // nldone
        en: 'Organizing power',
    },

    viewSchoolDispatch: {
        fr: 'Ventilation des ouvertures de places',
        nl: 'Verdeling van de vrije plaatsen', // nldone
        en: 'Ventilation of seat openings',
    },

    schoolSeatOpeningTotal: {
        fr: 'Total des places',
        nl: 'Totaal aantal plaatsen', // nldone
        en: 'Total number of places',
    },

    schoolSeatOpeningStart: {
        fr: 'Début des ouvertures',
        nl: 'Start van de plaatsverdeling', // nldone
        en: 'Start of openings',
    },

    schoolSeatOpeningEnd: {
        fr: 'Fin des ouvertures',
        nl: 'Einde van de plaastverdeling', // nldone
        en: 'End of openings',
    },

    schoolSeatDispatch: {
        fr: 'Répartition',
        nl: 'Verdeling', // nldone
        en: 'Distribution',
    },

    schoolCreation: {
        fr: 'Création',
        nl: 'Oprichting', // nltocheck
        en: 'Creation',
    },

    schoolExtention: {
        fr: 'Extension',
        nl: 'Uitbreiding', // nltocheck
        en: 'Extention',
    },

    filterContact: {
        fr: 'Filtrer les contacts',
        nl: 'Contacten filteren', // nltocheck
        en: 'Filter contacts',
    },

    createTeam: {
        fr: 'Encoder une équipe',
        nl: 'Codering van een team', // nltocheck
        en: 'Encode a team',
    },

    createTeamCreateContact: {
        fr: `S'il ne figure pas dans la liste de résultats, souhaitez-vous créer le contact   `,
        nl: 'Als het niet in de resultatenlijst staat, wilt u dan de contactpersoon', // nltocheck
        en: 'If it is not in the results list, do you want to create the contact',
    },

    filterResult: {
        fr: `Résultat pour`,
        nl: 'Resultaten voor', // nltocheck
        en: 'Result for',
    },

    createNewContact: {
        fr: `Créer le contact`,
        nl: 'Maak de contactpersoon', // nltocheck
        en: 'Create contact',
    },

    'helptext:createTeam': {
        fr: `Pour constituer une équipe, sélectionner les contacts impliqués.`,
        nl: `Om een team te vormen, selecteert u de betrokken contactpersonen.`, // nltocheck
        en: `To form a team, select the contacts involved.`,
    },

    audienceNotAllowed: {
        fr: "Vous n'avez pas les autorisations pour visualiser cette information.",
        nl: 'U heeft niet de bevoegdheid om deze informatie te bekijken.', // nltodo
        en: "You don't have the authorization to view this information.",
    },

    setAudience: {
        fr: "Visibilité de l'information",
        nl: 'Zichtbaarheid van de informatie', // nldone
        en: 'Visibility of the information',
    },

    setAudienceInfo: {
        fr: `Veuillez choisir les groupes d'utilisateurs à qui s'adresse cette information.
Si aucun groupe n'est référencé, vous serez la seule personne à pouvoir voir cette information.
Une personne qui n'aurait pas accès à une information pourra cependant voir sa date de création et qui en est l'auteur.
La liste des groupes existants est éditable par les personnes en charge de l'administration de la plateforme. `,
        nl: `Selecteer de gebruikersgroepen waarvoor deze informatie bedoeld is.
Als er geen enkele groep wordt weergegeven, bent u de enige persoon die deze informatie kan zien.
Een persoon die geen toegang heeft tot de informatie kan echter de datum en de auteur van creatie ervan zien.
De lijst van bestaande groepen kan door de verantwoordelijken van de administratie van het platform gewijzigd worden. `,
        en: `Please select the user groups for which this information is intended.
If no group is referenced, you will be the only person who can see this information.
A person who does not have access to information may, however, see its creation date and who is the author.
The list of existing groups can be edited by the people in charge of the platform administration. `,
    }, // nldone

    lastDay: {
        fr: 'hier',
        nl: 'gisteren', // nldone
        en: 'yesterday',
    },

    lastWeek: {
        fr: 'semaine dernière',
        nl: 'laatste week', // nldone
        en: 'last week',
    },

    resetTimelineWindow: {
        fr: 'tout',
        nl: 'alle', // nldone
        en: 'all time',
    },

    geocode: {
        fr: 'Chercher une adresse',
        nl: 'Een adres zoeken', // nldone
        en: 'Search an adress',
    },

    getUiInfo: {
        fr: `Pour obtenir des informations sur cette unité d'information, consultez la `,
        nl: 'Informatie over deze informatie-eenheid vindt u in de ', // nltocheck
        en: 'For information on this information unit, see the ',
    },

    getUiInfoLink: {
        fr: `documentation.`,
        nl: 'documentatie.', // nltocheck
        en: 'documentation.',
    },

    projectID: {
        fr: `Identifiant du projet`,
        nl: 'Project ID', // nltocheck
        en: 'Project ID',
    },

    notifications: {
        fr: `Notifications `,
        nl: 'Meldingen', // nltocheck
        en: 'Notifications',
    },

    notificationOn: {
        fr: `Notifications (ON)`,
        nl: 'Meldingen (ON)', // nltocheck
        en: 'Notifications (ON)',
    },

    notificationOff: {
        fr: `Notifications (OFF)`,
        nl: 'Meldingen (OFF)', // nltocheck
        en: 'Notifications (OFF)',
    },

    notifHelptext: {
        fr: texts.notifDocFR,
        nl: texts.notifDocNL,
        en: '',
    },

    audienceHelptext: {
        fr: `Une audience a été ajoutée ou modifiée.`,
        nl: `Een publiek werd toegevoegd of gewijzigd.`,
        en: `An audience has been added or modified.`,
    },

    tagHelptext: {
        fr: `Une étiquette a été ajoutée ou modifiée.`,
        nl: `Een tag werd toegevoegd of gewijzigd.`,
        en: `A tag has been added or modified.`,
    },

    editHelptext: {
        fr: `Une information a été ajoutée.`,
        nl: `Een informatie-eenheid werd toegevoegd.`,
        en: `An information has been added.`,
    },

    subscribed: {
        fr: `Vous recevrez les notifications concernant ce projet.`,
        nl: 'U zult meldingen ontvangen over dit project.',
        en: 'You will receive notifications about this project.',
    },

    unsubscribed: {
        fr: `Vous ne recevez pas les notifications de ce projet.`,
        nl: 'U ontvangt geen meldingen van dit project.',
        en: 'You are not receiving notifications from this project.',
    },

    hasBeenCreated: {
        fr: `a été ajouté.`,
        nl: 'werd toegevoegd.',
        en: 'has been created.',
    },

    hasBeenDeleted: {
        fr: `a été éffacé.`,
        nl: 'is verwijderd.', // nltocheck
        en: 'has been deleted.',
    },

    hasBeenTagged: {
        fr: `a reçu l'étiquette `,
        nl: 'is gelabeld met ', // nltocheck
        en: 'has been tagged ',
    },

    hasAnAudience: {
        fr: `a recu  une audience `,
        nl: '', //nltodo
        en: 'received an audience ',
    },

    viewProject: {
        fr: `Voir le projet`,
        nl: 'Bekijk het project', // nltocheck
        en: 'View the project',
    },

    writeMode: {
        fr: `Mode édition`,
        nl: 'schrijfmodus', // nltocheck
        en: 'Write mode',
    },
    readMode: {
        fr: `Mode lecture`,
        nl: 'leesmodus', // nltocheck
        en: 'Read mode',
    },

    backToProject: {
        fr: `retour au projet`,
        nl: 'projectpagina', // nltocheck
        en: 'back to project',
    },

    savingError: {
        fr: `Une **erreur de serveur** s'est produite, nous ne pouvons pas faire grand chose maintenant, veuillez réessayer.`,
        nl: 'Er is een **serverfout** gebeurd, we kunnen nu niet veel meer doen, probeer het nog eens.', // nltocheck
        en: 'A **server error** happened, we can not do much now, please try again.',
    },

    'nova/desc/refnova': {
        fr: 'référence NOVA',
        nl: 'NOVA referentie',
    },

    'nova/desc/typedossier': {
        fr: `Type de dossier`,
        nl: `Type dossier`,
    },

    'nova/desc/novaseq': {
        fr: `séquence NOVA`,
        nl: `NOVA sequentier`,
    },

    'nova/desc/referencespecifique': {
        fr: `référence spécifique (communal ou région)`,
        nl: `specifieke referentie (gemeentelijk of gewestelijk)`,
    },

    'nova/desc/realobject': {
        fr: `Sujet de la demande`,
        nl: `Detail van de aanvraag`,
    },

    'nova/desc/statutpermis': {
        fr: `Statut du permis`,
        nl: `Status van de vergunning`,
    },

    'nova/desc/statutenquete': {
        fr: `Statut de l'enquête`,
        nl: `Status van de aanvraag`,
    },

    'nova/desc/numberpartfrom': {
        fr: `numéro de maison`,
        nl: `huisnummer`,
    },

    'nova/desc/streetname': {
        fr: `nom de la rue`,
        nl: `straatnaam`,
    },

    'nova/desc/zipcode': {
        fr: `code postal`,
        nl: `postcode`,
    },

    'nova/desc/municipality': {
        fr: `commune`,
        nl: `gemeente`,
    },

    'nova/desc/ri': {
        fr: `Rapport d'incidence`,
        nl: `milieueffectenrapport`,
    },

    'nova/desc/ei': {
        fr: `Etudé d'incidence`,
        nl: `milieueffectenstudie`,
    },

    'nova/desc/datedepot': {
        fr: `Dépôt`,
        nl: `Indiening`,
    },

    'nova/desc/dateardosscomplet': {
        fr: `Accusé de réception dossier complet`,
        nl: `Ontvangstbewijs volledig dossier`,
    },

    'nova/desc/datearifirst': {
        fr: `Accusé de réception dossier incomplet`,
        nl: `Ontvangstbewijs onvolledig dossier`,
    },

    'nova/desc/datedebutmpp': {
        fr: `Début d'enquête publique`,
        nl: `Begin van openbaar onderzoek`,
    },

    'nova/desc/datefinmpp': {
        fr: `Fin d'enquête public`,
        nl: `Einde van het openbaar onderzoek`,
    },

    'nova/desc/datecc': {
        fr: `Commission de concertation`,
        nl: `Overlegcommissie`,
    },

    'nova/desc/datenotifdecision': {
        fr: `Notification de la décision`,
        nl: `Kennisgeving van besluit`,
    },

    'nova/desc/avissiamu': {
        fr: `Avis du Service d'Incendie et d'Aide Médicale Urgente`,
        nl: `Besluit van de Dienst voor Brandbestrijding en Dringende Medische Hulp`,
    },

    'nova/desc/enquetepublique': {
        fr: `enquête public`,
        nl: `openbaar onderzoek`,
    },

    memberRoleMain: {
        fr: `Mandataire`,
        nl: `Mandataris`,
    },

    memberRoleRegular: {
        fr: `Membre`,
        nl: `Lid`,
    },

    'warning/last-audience': {
        fr: `Ceci est la dernière audience pour cette unité d‘information`,
        nl: `Dit is de laatste hoorzitting voor deze informatie-eenheid`, //todonl
    },
    signLegend: {
        fr: `Signification des symboles`,
        nl: `Betekenis van de symbolen`, //todonl
    },
};
