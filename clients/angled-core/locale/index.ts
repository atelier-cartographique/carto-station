import { MessageStore, fromRecord, Parameters } from 'sdi/locale';
import baseMessages from './base';
import universeMessages from './universe';
import projectsMessages from './projects';
import queryMessages from './query';
import formMessages from './form';


const messages = {
    ...baseMessages,
    ...universeMessages,
    ...projectsMessages,
    ...queryMessages,
    ...formMessages,
};

type MDB = typeof messages;
export type AngledMessageKey = keyof MDB;


declare module 'sdi/locale' {
    export interface MessageStore {
        angled(k: AngledMessageKey, params?: Parameters): Translated;
    }
}


MessageStore.prototype.angled =
    function (k: AngledMessageKey, params?: Parameters) {
        return this.getEdited('angled', k, () => fromRecord(messages[k], params));
    };

