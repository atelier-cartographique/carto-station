import * as texts from './marked-texts';

export default {
    appNameQuery: {
        fr: 'Requête',
        nl: 'Zoekopdracht', // nldone
    },

    aliasLabelFR: {
        fr: 'Alias en français',
        nl: 'Franse aliassen', // nltocheck
    },

    aliasLabelNL: {
        fr: 'Alias en néerlandais',
        nl: 'Nederlandse alias', // nltocheck
    },

    generatedName: {
        fr: 'Nom généré',
        nl: 'Berekende naam', // nltocheck
    },
    codeName: {
        fr: 'Nom de code',
        nl: 'Codenaam', // nltocheck
    },

    query: {
        fr: 'Requête',
        nl: 'Zoekopdracht', // nldone
    },

    'info-builder': {
        fr: 'Le constructeur de requete ...',
        nl: 'De zoekopdrachtgever ...', // nldone
    },

    runQuery: {
        fr: 'Exécuter',
        nl: 'Lanceren', // nldone
        // fr: 'Exécuter la requête',
        // nl: 'De zoekopdracht lanceren', // nldone
    },

    backToBuilder: {
        fr: 'Retour',
        nl: 'Terug', // nldone
    },

    true: {
        fr: 'oui',
        nl: 'ja', // nldone
    },

    false: {
        fr: 'non',
        nl: 'nee', // nldone
    },

    addStatement: {
        fr: 'Valider',
        nl: 'Valideren', // nldone
    },

    updateStatement: {
        fr: 'Mettre à jour',
        nl: 'Update',
    },

    showMap: {
        fr: 'Voir la carte',
        nl: 'De kaart bekijken', // nldone
    },

    queryList: {
        fr: 'Requêtes disponibles',
        nl: 'Beschikbare zoekopdrachten', // nldone
    },

    queryBuilder: {
        fr: 'Constructeur de requête',
        nl: 'Een zoekopdracht starten', // nldone
    },

    queryTester: {
        fr: 'Requête actuelle',
        nl: 'Zoekopdracht is bezig', // nldone
    },

    selectField: {
        fr: 'Sélectionner le champs',
        nl: 'Een veld selecteren', // nlremark on peut enlever een veld
    },
    selectOperator: {
        fr: "Sélectionner l'opérateur",
        nl: 'De operator selecteren', // nldone
    },
    selectAggregator: {
        fr: "Sélectionner l'aggrégateur",
        nl: 'De samensteller selecteren', // nldone
    },
    aggregatorTitle: {
        fr: 'Agrégateur',
        nl: 'Aggregator',
    },
    selectValue: {
        fr: 'Sélectionner la valeur',
        nl: 'De waarde selecteren', // nldone
    },
    queryResult: {
        fr: 'Résultat de la requête',
        nl: 'Resultaat van de zoekopdracht', // nldone
    },
    fieldSelected: {
        fr: 'Champs sélectionnés',
        nl: 'Geselecteerd veld', // nlremark on peut enlever een veld
    },
    fieldUnselected: {
        fr: 'Champs disponibles',
        nl: 'Beschikbaar veld', // nlremark on peut enlever een veld
    },
    addToCurrentQuery: {
        fr: 'Ajouter à la requête courante',
        nl: 'Toevoegen aan huidige zoekopdracht', // nldone
    },

    newQuery: {
        fr: 'Nouvelle requête',
        nl: 'Nieuwe zoekopdracht', // nldone
    },

    addNewQuery: {
        fr: 'Créer une nouvelle requête',
        nl: 'Maak een nieuwe zoekopdracht aan', // nldone
    },

    createStatement: {
        fr: 'Création du filtre',
        nl: 'Aanmaken van een filter', // nldone
    },

    queryResultsAndSelectInfo: {
        fr: 'Résultats de la requête et sélection des informations',
        nl: 'Resultaten van de zoekopdracht en selectie van informatie', // nldone
    },

    specifyAgregation: {
        fr: 'Agrégation',
        nl: 'Samenvoeging', // nldone
    },

    agregationHelptext: {
        fr: `Ce champ peut contenir de multiples informations.
        Veuillez préciser ici celles que vous souhaitez afficher et la façon de les aggréger.`,
        nl: `Dit veld kan verschillende soorten informatie bevatten.
Geef hier aan welke u wilt weergeven en hoe u ze wilt samenvoegen.`, // nldone
    },

    queryAppInfos: {
        fr: `Cette application permet de créer et exécuter des requêtes afin d'explorer les données sur les projets de la plateforme Geodata. 
Le résultat des requêtes est visualisable et exportable sous forme de tableau.
Les requêtes peuvent également être publiées comme nouvelles couches de données spatiales disponibles pour créer des cartes via l'application Studio. 
Le résultat des requêtes et les couches de données résultantes sont toujours issus des dernières informations disponibles sur la plateforme.`,
        nl: `Deze applicatie laat toe zoekopdrachten aan te maken en uit te voeren om projectgegevens op het Geodata-platform te verkennen.
De resultaten van de zoekopdrachten kunnen in tabelvorm worden bekeken en geëxporteerd.
Zoekopdrachten kunnen ook worden gepubliceerd als nieuwe ruimtelijke gegevens-lagen die beschikbaar zijn om kaarten te maken via de Studio applicatie.
De resultaten van de zoekopdrachten en de data die daaruit voortkomen, zijn altijd gebaseerd op de meest recente informatie die op het platform beschikbaar is.`, // nldone
    },

    queryAppInfosLight: {
        fr: `Créer et exécuter des requêtes afin d'explorer la base de données Geodata. 
Visualiser et exporter les résultats sous forme tabulaire. 
Publier des requêtes sous forme de couches de données spatiales.
Créer et publier des cartes à partir de ces nouvelles couches via l'application Studio.`,
        nl: `Creëren en uitvoeren van zoekopdrachten  om de Geodata-databank te verkennen. 
Visualiseren en exporteren van de resultaten in tabelvorm. 
Publiceren van de zoekopdrachten als ruimtelijke gegevenslagen.
Creëren en publiceren van kaarten van deze nieuwe lagen via de Studio applicatie.`, // nldone (j'ai traduit ce bloque comme ça donne de l'information comment l'application marche, ok? )
    },

    addStatementInfos: {
        fr: `Vous pouvez completer la requête en y ajoutant une instruction (ET/OU). 
        "ET" :  les résultats remplissent les deux conditions
        "OU" : les résulats remplissent l'une ou l'autre des conditions`,
        nl: `U kunt deze zoekopdracht aanvullen met een instructie (EN/OF). 
        "EN": de resultaten voldoen aan beide voorwaarden...
        "OF": de resultaten voldoen aan één of andere voorwaarden`, // nldone
    },

    viewLayerInfos: {
        fr: `Les projets qui n'ont pas de références géographique (point, ligne ou polygone) ne seront pas visibles sur la vue spatialisée.`,
        nl: `Projecten die geen geografische referenties hebben (punt, lijn of veelhoek) zullen niet zichtbaar zijn in de ruimtelijk beeld.`, // nldone
    },

    publishLayerInfos: {
        fr: `Pour créer et publier des cartes à partir des données issues de cette requête, vous devez auparavant publier l'une ou l'autre de ces couches.
        Les cartes issues de ces données seront automatiquement mises à jour avec les dernières informations issues de l'application Project.`,
        nl: `Om kaarten te maken en te publiceren op basis van de gegevens van deze zoekopdracht, moet u eerst één van deze lagen publiceren.
        De kaarten die voortvloeien uit deze gegevens worden automatisch bijgewerkt met de meest recente informatie uit de Project app.`, // nldone
    },

    displayStatement: {
        fr: 'Afficher la requête',
        nl: 'Geef de zoekopdracht weer', // nldone
    },

    hideStatement: {
        fr: 'Masquer la requête',
        nl: 'Verberg de zoekopdracht', // nldone
    },

    toQueryList: {
        fr: 'Liste des requêtes',
        nl: 'Lijst van zoekopdrachten', // nldone
    },

    modifyQuery: {
        fr: 'Modifier la requête',
        nl: 'De zoekopdracht wijzigen', // nldone
    },

    editQuery: {
        fr: 'Editer la requête',
        nl: 'De zoekopdracht bewerken', // nldone
    },
    editName: {
        fr: 'Modifier le nom',
        nl: 'Naam Wijzigen', // nldone
        en: 'Edit name',
    },

    clearQuery: {
        fr: 'Supprimer les instructions',
        nl: 'Instructies verwijderen', // nldone
    },

    saveAndContinue: {
        fr: 'Enregistrer et poursuivre',
        nl: 'Registreren en verder gaan', // nldone
    },

    publishQuery: {
        fr: 'Enregistrer et publier',
        nl: 'Opslaan en publiceren', // nldone
    },

    publishPoint: {
        fr: 'Publier la couche de points',
        nl: 'De laag punten publiceren', // nldone
    },

    publishLine: {
        fr: 'Publier la couche de lignes',
        nl: 'De laag lijnen publiceren', // nldone
    },

    publishPolygon: {
        fr: 'Publier la couche de polygones',
        nl: 'De laag veelhoeken publiceren', // nl done
    },

    publishData: {
        fr: 'Publications des données spatiales',
        nl: 'Publicatie van de ruimtelijke gegevens', // nl done
    },

    backToWhere: {
        fr: 'Retour aux filtres',
        nl: 'Terugkeren naar de filters', // nl done
    },

    myQueries: {
        fr: 'Requêtes personnelles',
        nl: 'Mijn zoekopdrachten', // nl to check
    },

    mySharedQueries: {
        fr: 'Mes requềtes partagées',
        nl: 'Mijn gedeelde zoekopdrachten',
    },

    commonQueries: {
        fr: "Requêtes partagées par d'autres",
        nl: 'Gedeelde zoekopdrachten', // nl to check
    },

    'op/exact': {
        fr: 'est égal à',
        nl: 'is gelijk aan', // nl done
    },

    'op/intersects': {
        fr: "rencontre l'élément géographique",
        nl: 'treffen van het geografisch element', // nl done
    },
    'op/contains': {
        fr: "contient l'élément géographique",
        nl: 'bevat het geografische element', // nl done
    },
    'op/icontains': {
        fr: 'contient',
        nl: 'bevat', // nl done
    },
    'op/istartswith': {
        fr: 'commence par',
        nl: 'begint met', // nl done
    },
    'op/iendswith': {
        fr: 'se termine par',
        nl: 'eindigt met', // nl done
    },
    'op/gt': {
        fr: 'est supérieur à',
        nl: 'is groter dan', // nl done
    },
    'op/gte': {
        fr: 'est supérieur ou égal à',
        nl: 'is groter dan of is gelijk aan', // nl done
    },
    'op/lt': {
        fr: 'est inférieur à',
        nl: 'is minder dan', // nl done
    },
    'op/lte': {
        fr: 'est inférieur ou égal à',
        nl: 'is minder dan of gelijk aan', // nl done
    },

    'aggregate/sum': {
        fr: 'Somme des valeurs',
        nl: 'Som van de waarden', // nl done
    },
    'aggregate/mean': {
        fr: 'Moyenne des valeurs',
        nl: 'Gemiddelde waarden', // nl done
    },
    'aggregate/concat': {
        fr: 'Assemblage des valeurs',
        nl: 'Samenvoegen van de waarden', // nl done
    },
    'aggregate/min': {
        fr: 'Valeur minimale',
        nl: 'Minimale waarde', // nl done
    },
    'aggregate/max': {
        fr: 'Valeur maximale',
        nl: 'Maximale waarde', // nl done
    },
    'conj/AND': {
        fr: 'ET',
        nl: 'EN', // nl done
    },
    'conj/OR': {
        fr: 'OU',
        nl: 'OF', // nl done
    },

    statementTagline: {
        fr: 'Cette requête extrait les projets pour lesquels :',
        nl: 'Deze zoekopdracht toont projecten waarvoor:', // nldone
    },

    addWithAND: {
        fr: `Ajouter l'instruction avec "ET"`,
        nl: 'Instructie toeveogen met "EN"', // nldone
    },

    addWithOR: {
        fr: `Ajouter l'instruction avec "OU"`,
        nl: 'Instructie toevoegen met "OF"', // nldone
    },

    selectInfoToFilter: {
        fr: `Selection de l'information à filtrer`,
        nl: 'Selectie van de informatie om te filteren', // nl done
    },

    displayMap: {
        fr: `Affichage de la carte`,
        nl: 'Tonen van de kaart', // nl done
    },

    shareQuery: {
        fr: 'Partager',
        nl: '', // nl todo
    },

    unShareQuery: {
        fr: 'Dé-Partager',
        nl: '', // nl todo
    },

    publishMetadata: {
        fr: 'Creer une couche',
        nl: '', // nl todo
    },

    insertDescriptionFR: {
        fr: 'Insérer une description en français',
        nl: '', // nl todo
    },

    insertDescriptionNL: {
        fr: 'Insérer une description en Néerlandais',
        nl: '', // nl todo
    },

    publishQueryTitle: {
        fr: 'Enregistrer et publier cette requête',
        nl: '', // nl todo
    },

    shareQueryTitle: {
        fr: `Partager la requête`,
        nl: '', // nl todo
    },

    publishQueryGeomTitle: {
        fr: `Publier les données spatiales`,
        nl: '', // nl todo
    },

    saveQueryTitle: {
        fr: `Enregistrer la requête.`,
        nl: '', // nl todo
    },

    saveQueryHelptxt: {
        fr: `Si vous vous arrêtez ici, elle ne sera disponible que pour vous.`,
        nl: '', // nl todo
    },

    shareQueryHelptxt: {
        fr: `Pour partager cette requête, donnez-lui une description. 
        Après validation, cette requête sera disponible auprès des utilisateurs de la plateforme. `,
        nl: '', // nl todo
    },

    publishQueryGeomHelptxt: {
        fr: `Vous pouvez publier les données géographiques issues de cette requête, afin d'en produire des cartes dans l'application Studio.
        Les éléments repris dans ces couches spatiales seront ceux pour lesquels ont été encodé une géométrie (point, ligne ou polygone). 
        Les cartes produites présenteront toujours les dernières versions des données encodées dans l'application Plans & Projets.`,
        nl: '', // nl todo
    },

    lastUpdate: {
        fr: `Mise à jour du `,
        nl: 'Update van de ', // nltocheck
    },

    noGeom: {
        fr: `Ce projet n'a pas de géométrie attachée, il n'est donc pas disponible sur la carte.`,
        nl: 'Dit project heeft geen bijgevoegde geometrie, dus het is niet beschikbaar op de kaart.', // nltocheck
    },

    projectIdLight: {
        fr: `ID du projet`,
        nl: 'Project ID', // nltocheck
    },

    goToProject: {
        fr: `Aller à la fiche du projet`,
        nl: 'Ga naar de projectfiche', // nltocheck
    },

    renameColumn: {
        fr: 'Créer un alias pour le champ sélectionné',
        nl: 'Maak een alias voor het geselecteerde veld', // nltocheck
    },

    renameColumnHelptext: {
        fr: `Note : Les alias FR et NL apparaissent à la place du nom de code partout où ce dernier est utilisé.`,
        nl: 'De aliassen FR en NL komen in de plaats van de codenaam wanneer deze wordt gebruikt.', // nltocheck
    },

    queryPreviewBlockTitle: {
        fr: 'Requête',
        nl: 'Zoekopdracht', // nldone
    },

    tooltipFieldLangSwitch: {
        fr: 'Champs multilingue - choix de la langue à prendre en compte',
        nl: 'Meertalige velden - keuze van de taal waarmee rekening moet worden gehouden', // nltocheck
    },

    tooltipFieldRename: {
        fr: 'Renommer le champs',
        nl: 'Hernoem het veld', // nltocheck
    },

    tooltipFieldAgregate: {
        fr: `Paramètres de l’agrégation`,
        nl: 'Aggregatie parameters', // nltocheck
    },

    tooltipFieldRemove: {
        fr: 'Supprimer',
        nl: 'Delete', // nltocheck
    },

    aggregatorPreview: {
        fr: `Prévisualisation de l'agrégation`,
        nl: 'Voorbeeld aggregatie', // nltocheck
    },

    aggregatorSelectType: {
        fr: `Choix du type d'agrégation`,
        nl: 'Type samenvoeging van gegevens', // nltocheck
    },

    aggregatorChooseFilter: {
        fr: `Filtres disponibles`,
        nl: 'Beschikbare filters', // nltocheck
    },

    aggregatorNoFilter: {
        fr: `Pas de filtres disponibles, vous pouvez continuer.`,
        nl: 'Geen filters beschikbaar, u kunt doorgaan.', // nltocheck
    },

    aggregatorFinalMsg: {
        fr: `Construction de l'agrégation terminée, vous pouvez valider.`,
        nl: 'Aggregatie compleet, je kunt vrijgeven.', // nltocheck
    },

    operatorListTitle: {
        fr: `Opérateur`,
        nl: 'Operator', // nltocheck
    },

    wfsBlockInfoTitle: {
        fr: `Informations WFS `,
        nl: 'Informatie WFS', // nltocheck
    },
    wfsBlockInfoContent: {
        fr: texts.wfsInfosFR,
        nl: texts.wfsInfosNL, // nltocheck
    },
};
