export const projectHomeTextFR = `
L'application Plans & Projets permet d'encoder et de visualiser les informations relatives à des plans en projet et des projets immobiliers.

L'ensemble de l'historique des informations est conservé et consultable.

Un système de permissions permet de définir à qui s'adresse l'information encodée.

**Précautions d’usage**

Les données sur les projets  sont sujettes à des modifications constantes. Par conséquent, l'exactitude et l'exhaustivité des informations sur les projets ne sont pas garanties dans cette application.

Il est également possible que certaines informations vous aient été masquées par souci de confidentialité ou encore qu’un projet n'ait pas été géoréférencé (dans ce cas il n'est donc pas visible sur la carte !).  

Pour toutes ces raisons, ces cartes doivent être prises avec grandes précautions.
`;

export const projectHomeTextNL = `
De Projecten applicatie stelt u in staat om informatie met betrekking tot projectplannen en vastgoedprojecten te coderen en te visualiseren.

Alle informatie wordt opgeslagen en is beschikbaar voor raadpleging.

Een systeem van machtigingen stelt u in staat om te bepalen aan wie de gecodeerde informatie gericht is.

**Let op**

De projectinformatie is onderhevig aan constante wijzigingen, en daarom is de correctheid en de volledigheid van de informatie betreffende de projecten niet gegarandeerd in deze applicatie.

Het is ook mogelijk dat sommige informatie voor uw gebruikersprofiel werd verborgen omwille van vertrouwelijkheid, alsook dat een project geen geolocatie heeft. (In dit geval is het project dus niet zichtbaar op de kaart !). 

Omwille van deze redenen, dient u deze al de gegevens aanwezig op deze applicatie met een kritische blik te interpreteren en de beheerder van het project contacteren om de gegevens te verifiëren.
`;

export const wfsInfosFR = `
Les couches de données spatiales de Geodata sont disponibles sous forme de WFS. 

Vous pouvez y retrouver les requêtes partagées et sauvées sous forme de couches spatiales (dont les noms commencent par \`angled:\` dans le WFS), ainsi que les couches de références  (dont les noms commencent par \`postgis:\` dans le WFS).

**Adresse du WFS FR**  
https://geodata.perspective.brussels/basic-wfs/fr  


**Adresse du WFS NL**  
https://geodata.perspective.brussels/basic-wfs/nl  

**Version du WFS**  
2.0

**Autentification**  
Votre login/mot de passe Geodata
`;

export const wfsInfosNL = `
De ruimtelijke gegevenslagen van Geodata zijn beschikbaar als WFS.  

U kunt in de WFS de gedeelde zoekopdrachten vinden die zijn opgeslagen als ruimtelijke lagen (waarvan de naam begint met \`angled:\` in de WFS), evenals de referentielagen (waarvan de naam begint met \`postgis:\` in de WFS).

**WFS adres FR**  
https://geodata.perspective.brussels/basic-wfs/fr 


**WFS adres NL**  
https://geodata.perspective.brussels/basic-wfs/nl  

**WFS versie  
2.0

**Authenticatie**  
Uw Geodata login/wachtwoord

`;

export const notifDocFR = `
Les notifications permettent de suivre les mises à jour des projets auxquels vous êtes abonné·e.

- lors de la création d’un projet, l’auteur·ice initial·e est automatiquement abonné·e à ce projet.
- lors de l’ajout d’information à un projet, l’auteur·ice est automatiquement abonné·e a ce projet.
- Il est possible de s’abonner ou se désabonner d'un projet en tout temps (*Notification ON/OFF* sur la fiche projet).
`;

export const notifDocNL = `
Met de meldingen kunt u de updates volgen van de projecten waarop u geabonneerd bent.

- bij het aanmaken van een project wordt de oorspronkelijke auteur automatisch op dat project geabonneerd.
- bij het toevoegen van informatie aan een project, wordt de auteur automatisch ingeschreven op dat project.
- U kunt zich op elk moment op een project abonneren of uitschrijven (*Melding AAN/UIT* in de projectweergave).
`;
