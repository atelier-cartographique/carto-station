import { fetchIO, postIO, putIO } from 'sdi/source';
import { getApiUrl } from 'sdi/app';

import {
    SubscriptionListIO,
    ProjectSubscription,
    ProjectSubscriptionIO,
    CircleSubscription,
    CircleSubscriptionIO,
    makePojectSub,
    NotifListIO,
    Notif,
    NotifIO,
} from 'angled-core/sub';
import { InformationUnitName } from 'angled-core/ui';
import { TaggedIO } from 'angled-core/ref';

export const fetchProjectSubscriptionList = (url: string) =>
    fetchIO(SubscriptionListIO, url);

export const fetchCircleSubscriptionList = (url: string) =>
    fetchIO(SubscriptionListIO, url);

export const fetchSubscriptionList = () =>
    Promise.all([
        fetchProjectSubscriptionList(
            getApiUrl('geodata/angled/s/project-sub/'),
        ),
        fetchCircleSubscriptionList(getApiUrl('geodata/angled/s/circle-sub/')),
    ]).then(([ps, cs]) => ps.concat(cs));

export const postProjectSubscription = (sub: Partial<ProjectSubscription>) =>
    postIO(
        ProjectSubscriptionIO,
        getApiUrl('geodata/angled/s/project-sub/'),
        sub,
    );

export const postCircleSubscription = (sub: Partial<CircleSubscription>) =>
    postIO(
        CircleSubscriptionIO,
        getApiUrl('geodata/angled/s/project-sub/'),
        sub,
    );

export const postSubscribeProject = (pid: number) =>
    postIO(
        ProjectSubscriptionIO,
        getApiUrl('geodata/angled/s/project-sub/'),
        makePojectSub(pid),
    );

export const postReSubscribeProject = (s: ProjectSubscription) =>
    putIO(
        ProjectSubscriptionIO,
        getApiUrl(`geodata/angled/s/project-sub/${s.id}/`),
        { ...s, active: true },
    );

export const postUnsubscribeProject = (s: ProjectSubscription) =>
    putIO(
        ProjectSubscriptionIO,
        getApiUrl(`geodata/angled/s/project-sub/${s.id}/`),
        { ...s, active: false },
    );

export const fetchProjectNotificationList = (url: string) =>
    fetchIO(NotifListIO, url);

export const fetchCircleNotificationList = (url: string) =>
    fetchIO(NotifListIO, url);

export const fetchNotificationList = () =>
    Promise.all([
        fetchProjectNotificationList(
            getApiUrl('geodata/angled/s/project-notif/'),
        ),
        fetchCircleNotificationList(
            getApiUrl('geodata/angled/s/circle-notif/'),
        ),
    ]).then(([pn, cn]) => pn.concat(cn));



const putNotification =
    (url: string, n: Notif) => putIO(NotifIO, url, n);

export const markNotificationRead =
    (n: Notif) =>
        putNotification(
            getApiUrl(`geodata/angled/s/project-notif/${n.id}/`),
            { ...n, mark_view: true });



export const fetchTaggedUnit = (unit: InformationUnitName, taggedId: number) =>
    fetchIO(TaggedIO, getApiUrl(`geodata/angled/t/${unit}/${taggedId}`));
