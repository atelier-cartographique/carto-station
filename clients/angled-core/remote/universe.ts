import { putIO, fetchIO, postIO, postUnrelatedIO } from 'sdi/source';
import { getApiUrl } from 'sdi/app';

import {
    DomainListIO,
    TermListIO,
    Domain,
    DomainIO,
    Term,
    TermIO,
    DomainMappingListIO,
} from 'angled-core/ref';

export const fetchDomainList = (url: string) => fetchIO(DomainListIO, url);

export const fetchDomainMappingList = (url: string) =>
    fetchIO(DomainMappingListIO, url);

export const fetchTermList = (url: string) => fetchIO(TermListIO, url);

export const postDomain = (
    url: string,
    data: Partial<Domain>
): Promise<Domain> => postIO(DomainIO, url, data);

export const putDomain = (url: string, data: Domain): Promise<Domain> =>
    putIO(DomainIO, url, data);

export const postTerm = (url: string, data: Partial<Term>): Promise<Term> =>
    postIO(TermIO, url, data);

export const putTerm = (url: string, data: Term): Promise<Term> =>
    putIO(TermIO, url, data);

export const postSwapTerms = (
    domainId: number,
    source: number,
    target: number
) =>
    postUnrelatedIO(
        TermListIO,
        getApiUrl(`geodata/angled/r/domain/${domainId}/swap`),
        { source, target }
    );
