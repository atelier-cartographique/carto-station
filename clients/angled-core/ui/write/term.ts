import { InformationUnitName, makeValueMapper } from 'angled-core/ui';
import { DIV, NODISPLAY } from 'sdi/components/elements';
import { termsInDomain, findTermsByDomain, findDomain, domainsForField, getDescriptionFromDomain, getDescriptionFromTerm } from 'angled-core/queries/universe';
import { Domain, Term, DomainList } from 'angled-core/ref';
import tr, { fromRecord } from 'sdi/locale';
import { getWriteDomain, getFormInput } from 'angled-core/queries/ui';
import { setWriteDomain, setFormInput } from 'angled-core/events/ui';
import { Setoid } from 'fp-ts/lib/Setoid';
import { renderSelect } from 'angled-core/components/select-term';
import { none } from 'fp-ts/lib/Option';
import { fieldDisplayName } from 'angled-core/queries/app';
import { makeIcon } from 'angled-core/components/buttons';
import { Tooltip } from 'angled-core/../sdi/components/tooltip';


const setoidDomain: Setoid<Domain> = {
    equals: (a: Domain, b: Domain) => a.id === b.id,
};

const setoidTerm: Setoid<Term> = {
    equals: (a: Term, b: Term) => a.id === b.id,
};

const fieldMapper = makeValueMapper('term', (a: number) => a, -1);
const getCurrentTerm =
    (unit: InformationUnitName, fieldName: string) =>
        getFormInput(unit, fieldName)
            .map(fieldMapper);



const getCurrentDomain = getWriteDomain;


const renderName =
    (unitName: InformationUnitName, fieldName: string) =>
        DIV({ className: 'field__key field--write' }, fieldDisplayName(unitName, fieldName));


const renderDomain =
    (d: Domain) =>
        DIV({
            className: 'domain__item',
            key: `domain-input-${d.id}`,
        }, 
            DIV({ className: 'item-label' }, fromRecord(d.name)),
            DIV({ className: 'item-description' }, getDescriptionFromDomain(d.id)),
        );


const selectDomainRenderer =
    (fieldName: string) =>
        renderSelect(
            setoidDomain,
            renderDomain,
            d => setWriteDomain(fieldName, d.id),
        );

const getSelectedDomain =
    (fieldName: string) => {
        if (getWriteDomain().filter(wd => wd.fieldName === fieldName).length > 0) {
            const d = getWriteDomain().filter(wd => wd.fieldName === fieldName)[0];
            return findDomain(d.id);
        }
        else {
            return none;
        }
    };

const renderDomainListMulti =
    (domains: DomainList, fieldName: string) =>
        DIV({ className: 'select--domain' },
            selectDomainRenderer(fieldName)(
                domains,
                getSelectedDomain(fieldName),
            ),
        );

// const renderDomainListSingle =
//     (domain: Domain) =>
//         DIV({ className: 'select--single' },
//             fromRecord(domain.name));


const renderDomainList =
    (unit: InformationUnitName, fieldName: string) => {
        const domains = domainsForField(unit, fieldName);
        if (domains.length === 1) {
            return NODISPLAY()
            // const d = domains[0];
            // return renderDomainListSingle(d);
        }
        return renderDomainListMulti(domains, fieldName);
    };


const renderTerm =
    (t: Term) =>
        DIV({
            className: `term__item`,
            key: `term-input-${t.id}`,
        },
            DIV({ className: 'item-label' }, fromRecord(t.name)),
            DIV({ className: 'item-description' }, getDescriptionFromTerm(t.id)),
        );


const selectTermRenderer =
    (unit: InformationUnitName, fieldName: string) =>
        renderSelect(
            setoidTerm,
            renderTerm,
            t => setFormInput(unit, fieldName)(t.id),
        );

const renderSelectedTerm =
    (unit: InformationUnitName, fieldName: string, domainId: number) =>
        getCurrentTerm(unit, fieldName).chain(t => findTermsByDomain(t, domainId));


const renderTermlist =
    (unit: InformationUnitName, fieldName: string, id: number) =>
        selectTermRenderer(unit, fieldName)(
            termsInDomain(id),
            renderSelectedTerm(unit, fieldName, id));

const infoTooltip: Tooltip = { text: () => tr.angled('helptext:selectTerm'), position: 'top' } // TODO: check position
const infoBubble = makeIcon('info', 3, 'info-circle', infoTooltip);

// const helptextTerm =
//     () =>
//         spanTooltipTop(
//             tr.angled('helptext:selectTerm'),
//             {},
//             infoBubble(() => ' '),
//         );

const renderTermListOpt =
    (unit: InformationUnitName, fieldName: string) =>
        getCurrentDomain().length === 0 ?
            [DIV({ className: 'select--term' },
                tr.angled('selectDomain'),
                infoBubble(() => ' '))] :
            getCurrentDomain()
                .map(wd =>
                    wd.fieldName === fieldName ?
                        DIV({
                            className: 'select--term',
                            key: `select--term--${unit}-${fieldName}-${wd.id}`,
                        }, renderTermlist(unit, fieldName, wd.id)) :
                        getCurrentDomain().length === 1 ?
                            DIV({
                                className: 'select--term',
                                key: `select--domain--${unit}-${fieldName}-${wd.id}`,
                            },
                                tr.angled('selectDomain'),
                                infoBubble(() => ' ')) :
                            NODISPLAY(),
                );

// const helptextTerm =
//     () => DIV({ className: 'helptext' }, tr.angled('helptext:selectTerm'));



export const render =
    (unit: InformationUnitName, fieldName: string) =>
        DIV({ className: `field field--${fieldName} field--write` },
            renderName(unit, fieldName),
            renderDomainList(unit, fieldName),
            ...(renderTermListOpt(unit, fieldName)),
        );

export default render;
