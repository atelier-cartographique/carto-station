import { InformationUnitName, makeValueMapper } from 'angled-core/ui';
import { DIV } from 'sdi/components/elements';
import { inputText } from 'sdi/components/input';
import { getFormInput } from 'angled-core/queries/ui';
import { setFormInput } from 'angled-core/events/ui';

type Self = string;

const fieldMapper = makeValueMapper('raw_text', (a: Self) => a, '');

const renderName =
    (fieldName: string) =>
        DIV({ className: 'field__key field--write' }, fieldName);

const renderInput =
    (unit: InformationUnitName, fieldName: string) =>
        DIV({ className: 'field__value field--write' },
            inputText(
                () => getFormInput(unit, fieldName)
                    .fold('', fieldMapper),
                setFormInput(unit, fieldName),
            ));

export const render =
    (unit: InformationUnitName, fieldName: string) =>
        DIV({ className: `field field--${fieldName} field--write` },
            renderName(fieldName),
            renderInput(unit, fieldName),
        );

export default render;
