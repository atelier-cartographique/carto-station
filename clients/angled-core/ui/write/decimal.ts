import {
    InformationUnitName,
    makeValueMapper,
    getFieldUnit,
} from 'angled-core/ui';
import { DIV, SPAN } from 'sdi/components/elements';
import { inputNumberAsStr, options } from 'sdi/components/input';
import { getFormInput } from 'angled-core/queries/ui';
import { setFormInput } from 'angled-core/events/ui';
import { fieldDisplayName } from 'angled-core/queries/app';

type Self = number;

const fieldMapper = makeValueMapper('decimal', (a: Self) => a, NaN);

const renderName = (unit: InformationUnitName, fieldName: string) =>
    DIV(
        { className: 'field__key field--write' },
        fieldDisplayName(unit, fieldName),
        getFieldUnit(unit, fieldName).map(u =>
            SPAN({ className: '~unit' }, ` (${u}) `)
        )
    );

const renderInput = (unit: InformationUnitName, fieldName: string) =>
    DIV(
        { className: 'field__value field--write test' },
        inputNumberAsStr(
            options(
                `input-decimal-${unit}-${fieldName}`,
                () =>
                    getFormInput(unit, fieldName).fold('', d => {
                        const fm_d = fieldMapper(d);
                        if (isNaN(fm_d)) {
                            return '';
                        } else {
                            return fm_d.toString();
                        }
                    }),
                d => {
                    if (!isNaN(parseFloat(d))) {
                        setFormInput(unit, fieldName)(parseFloat(d));
                    }
                }
            )
        )
    );

export const render = (unit: InformationUnitName, fieldName: string) =>
    DIV(
        { className: `field field--${fieldName} field--write` },
        renderName(unit, fieldName),
        renderInput(unit, fieldName)
    );

export default render;
