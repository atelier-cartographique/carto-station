import { FormEvent } from 'react';
import { DIV, INPUT } from 'sdi/components/elements';

import { InformationUnitName } from 'angled-core/ui';
import { setFormInput, addDocumentFile } from 'angled-core/events/ui';
import { fieldDisplayName } from 'angled-core/queries/app';


const renderName =
    (unitName: InformationUnitName, fieldName: string) =>
        DIV({ className: 'field__key field--write' }, fieldDisplayName(unitName, fieldName));


const inputDocument =
    (unit: InformationUnitName, fieldName: string) =>
        INPUT({
            type: 'file',
            name: 'field_input_document',
            className: 'field--document__input',
            onChange: (e: FormEvent<HTMLInputElement>) => {
                if (e && e.currentTarget.files && e.currentTarget.files.length > 0) {
                    const url = '/documents/documents/';
                    setFormInput(unit, fieldName)(url);
                    addDocumentFile(e.currentTarget.files[0]);
                }
                else {
                    setFormInput(unit, fieldName)('');
                }
            },
        });


const renderInput =
    (unit: InformationUnitName, fieldName: string) =>
        DIV({ className: 'field__value field--write' },
            inputDocument(unit, fieldName),
        );

export const render =
    (unit: InformationUnitName, fieldName: string) =>
        DIV({ className: `field field--${fieldName} field--write` },
            renderName(unit, fieldName),
            renderInput(unit, fieldName),
        );

export default render;
