import * as debug from 'debug';
import { Option } from 'fp-ts/lib/Option';

import renderBoolean from './boolean';
import renderDate from './date';
import renderDecimal from './decimal';
import renderFk from './fk';
import renderGeometry from './geometry';
import renderLabel from './label';
import renderMonth from './month';
import renderNumber from './number';
import renderRaw_text from './raw_text';
import renderTerm from './term';
import renderText from './text';
import renderVarchar from './varchar';
import renderYear from './year';
import { InformationUnitName, FieldType, Project, lookupNamedField } from 'angled-core/ui';
import { UnitWidget } from 'angled-core/profile';
import { DIV } from 'sdi/components/elements';

const logger = debug('sdi:angled/components/write');


export const renderField =
    (unitName: InformationUnitName, kind: FieldType, fieldName: string, projOpt: Option<Project>) => {
        switch (kind) {
            case 'boolean': return renderBoolean(unitName, fieldName);
            case 'date': return renderDate(unitName, fieldName);
            case 'decimal': return renderDecimal(unitName, fieldName);
            case 'fk': return renderFk(unitName, fieldName, projOpt);
            case 'geometry': return renderGeometry(unitName, fieldName);
            case 'label': return renderLabel(unitName, fieldName);
            case 'month': return renderMonth(unitName, fieldName);
            case 'number': return renderNumber(unitName, fieldName);
            case 'raw_text': return renderRaw_text(unitName, fieldName);
            case 'term': return renderTerm(unitName, fieldName);
            case 'text': return renderText(unitName, fieldName);
            case 'varchar': return renderVarchar(unitName, fieldName);
            case 'year': return renderYear(unitName, fieldName);
        }
    };



export const renderUnitWidget =
    (widget: UnitWidget, projOpt: Option<Project>) =>
        lookupNamedField(widget.name)
            .map(
                ({ fields }) =>
                    DIV({},
                        ...(fields.map(([ft, fn]) => renderField(widget.name, ft, fn, projOpt)))));


logger('Loaded');
