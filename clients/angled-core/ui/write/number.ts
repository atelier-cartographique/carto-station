import * as debug from 'debug';

import {
    InformationUnitName,
    makeValueMapper,
    getFieldUnit,
} from 'angled-core/ui';
import { DIV, SPAN } from 'sdi/components/elements';
import { inputNumberAsStr, options } from 'sdi/components/input';
import { getFormInput } from 'angled-core/queries/ui';
import { setFormInput } from 'angled-core/events/ui';
import { fieldDisplayName } from 'angled-core/queries/app';

type Self = number;

const logger = debug('sdi:angled/components/write');

const fieldMapper = makeValueMapper('number', (a: Self) => a, NaN); // todo fixme à quoi sert dflt ?

const renderName = (unit: InformationUnitName, fieldName: string) =>
    DIV(
        { className: 'field__key field--write' },
        fieldDisplayName(unit, fieldName),
        getFieldUnit(unit, fieldName).map(u =>
            SPAN({ className: '~unit' }, ` (${u}) `)
        )
    );

const renderInput = (unit: InformationUnitName, fieldName: string) =>
    DIV(
        {
            className: 'field__value field--write ',
            key: `input-number-${unit}-${fieldName}`,
        },
        inputNumberAsStr(
            options(
                `input-number-${unit}-${fieldName}`,
                () =>
                    getFormInput(unit, fieldName).fold('', d => {
                        const fmd = fieldMapper(d);
                        if (isNaN(fmd)) {
                            return '';
                        } else {
                            return fmd.toString();
                        }
                    }),
                i => {
                    if (!isNaN(parseInt(i, 10))) {
                        setFormInput(unit, fieldName)(parseInt(i, 10));
                    }
                }
            )
        )
    );

export const render = (unit: InformationUnitName, fieldName: string) =>
    DIV(
        { className: `field field--${fieldName} field--write` },
        renderName(unit, fieldName),
        renderInput(unit, fieldName)
    );

export default render;

logger('Loaded');
