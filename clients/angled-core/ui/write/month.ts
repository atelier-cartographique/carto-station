import { InformationUnitName, makeValueMapper } from 'angled-core/ui';
import { DIV } from 'sdi/components/elements';
import { attrOptions, inputText } from 'sdi/components/input';
import { getFormInput } from 'angled-core/queries/ui';
import { setFormInput } from 'angled-core/events/ui';
import { fieldDisplayName } from 'angled-core/queries/app';

type Self = string;

const nowInYYYYMM = () => {
    const d = new Date();
    const m = d.getMonth();
    if (m <= 9) {
        return d.getFullYear().toString() + '-0' + m;
    } else {
        return d.getFullYear().toString() + '-' + m;
    }
};

const fieldMapper = makeValueMapper('month', (a: Self) => a, nowInYYYYMM());

const renderName = (unitName: InformationUnitName, fieldName: string) =>
    DIV(
        { className: 'field__key field--write' },
        fieldDisplayName(unitName, fieldName)
    );

const renderInput = (unit: InformationUnitName, fieldName: string) =>
    DIV(
        { className: 'field__value field--write' },
        inputText(
            attrOptions(
                `input-month-${unit}-${fieldName}`,
                () =>
                    getFormInput(unit, fieldName).fold(
                        nowInYYYYMM(),
                        fieldMapper
                    ),
                d => setFormInput(unit, fieldName)(d),
                { type: 'month' }
            )
        )
    );

export const render = (unit: InformationUnitName, fieldName: string) =>
    DIV(
        { className: `field field--${fieldName} field--write` },
        renderName(unit, fieldName),
        renderInput(unit, fieldName)
    );

export default render;
