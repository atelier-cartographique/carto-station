import * as debug from 'debug';

import { InformationUnitName, makeValueMapper } from 'angled-core/ui';
import { DIV } from 'sdi/components/elements';
import { renderRadioOut } from 'sdi/components/input';
import { getFormInput } from 'angled-core/queries/ui';
import { setFormInput } from 'angled-core/events/ui';
import tr from 'sdi/locale';
import { fieldDisplayName } from 'angled-core/queries/app';

type Self = boolean;

const logger = debug('sdi:angled/components/write/boolean');

const fieldMapper = makeValueMapper('boolean', (a: Self) => a, false);


const getBooleanFromInput =
    (unit: InformationUnitName, fieldName: string): boolean =>
        getFormInput(unit, fieldName).fold(
            false,
            d => fieldMapper(d),
        );

const renderName =
    (unitName: InformationUnitName, fieldName: string) =>
        DIV({ className: 'field__key field--write' }, fieldDisplayName(unitName, fieldName));


const boolToText =
    (unit: InformationUnitName, b: boolean) => {
        if (unit === 'school_creation') {
            return b ? tr.angled('schoolCreation') : tr.angled('schoolExtention')
        }

        return b ? tr.angled('false') : tr.angled('true')
    };


const renderBoolean =
    (unit: InformationUnitName) => (b: boolean) => boolToText(unit, b);

const selectBoolean =
    (unit: InformationUnitName, fieldName: string) =>
        renderRadioOut(
            'switch-boolean',
            renderBoolean(unit),
            setFormInput(unit, fieldName)
        )

const renderInput =
    (unit: InformationUnitName, fieldName: string) =>
        selectBoolean(unit, fieldName)([true, false], getBooleanFromInput(unit, fieldName));


export const render =
    (unit: InformationUnitName, fieldName: string) =>
        DIV({ className: `field field--${fieldName} field--write` },
            renderName(unit, fieldName),
            renderInput(unit, fieldName),
        );

export default render;

logger('Loaded!');
