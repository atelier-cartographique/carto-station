import { UnitData, typeNameToName } from 'angled-core/ui';
import { DIV, NODISPLAY } from 'sdi/components/elements';
import { MessageRecord } from 'sdi/source';
import { fromRecord } from 'sdi/locale';
import { fieldDisplayName } from 'angled-core/queries/app';
import { WidgetFilterOpt } from 'angled-core/profile';

type Self = MessageRecord;


const renderValue =
    (value: Self, isImageCaption: string) =>
        DIV({ className: `field__value field--read ${isImageCaption}` }, fromRecord(value));

export const render = (
    u: UnitData,
    fieldName: string,
    value: Self,
    _filter: WidgetFilterOpt
) => {
    switch (u.unit) {
        case 'UnitDocument':
            return NODISPLAY();
        default:
            const isImageCaption = u.unit === 'UnitImage' ? 'field--read--image-caption' : '';
            return DIV({
                className: `field field--${fieldName} field--read`,
                title: fieldDisplayName(typeNameToName[u.unit], fieldName)
            },
                renderValue(value, isImageCaption),
            );
    }
};

export default render;
