import { UnitDocumentData, typeNameToName } from 'angled-core/ui';
import { DIV, NODISPLAY, A } from 'sdi/components/elements';
import { fromRecord } from 'sdi/locale';
import { fieldDisplayName } from 'angled-core/queries/app';
import { WidgetFilterOpt } from 'angled-core/profile';

type Self = string;


const renderValue =
    (value: Self, u: UnitDocumentData) => {
        if (value === '/documents/documents/') {  // FIXME: could be better for handling where there is no image uploaded (yet)
            return NODISPLAY();
        }
        else {
            return DIV({ className: 'field__value field--read' },
                A({ href: value }, fromRecord(u.caption)),
            );
        }
    };

export const render = (
    u: UnitDocumentData,
    fieldName: string,
    value: Self,
    _filter: WidgetFilterOpt
) =>
    DIV({
        className: `field field--${fieldName} field--read`,
        title: fieldDisplayName(typeNameToName[u.unit], fieldName),
    },
        renderValue(value, u),
    );

export default render;
