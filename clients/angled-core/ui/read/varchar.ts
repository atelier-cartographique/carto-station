import { UnitData, typeNameToName } from 'angled-core/ui';
import { DIV } from 'sdi/components/elements';
import renderImage from './image';
import renderDocument from './document';
import { fieldDisplayName } from 'angled-core/queries/app';
import { WidgetFilterOpt } from 'angled-core/profile';

type Self = string;


const renderValue =
    (value: Self) =>
        DIV({ className: 'field__value field--read' }, value);

export const render = (
    u: UnitData,
    fieldName: string,
    value: Self,
    filter: WidgetFilterOpt
) => {
    switch (u.unit) {
        case 'UnitImage': return renderImage(u, fieldName, value, filter);
        case 'UnitDocument': return renderDocument(u, fieldName, value, filter);
        // case 'UnitNova': return renderNova(u, fieldName, value);
        default: return DIV({
            className: `field field--${fieldName} field--read`,
            title: fieldDisplayName(typeNameToName[u.unit], fieldName),
        },
            renderValue(value),
        );
    }
};

export default render;
