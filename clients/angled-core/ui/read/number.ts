import { InformationUnitName, getFieldUnit, typeNameToName, UnitData } from 'angled-core/ui';
import { DIV, SPAN } from 'sdi/components/elements';
import { formatNumber } from 'sdi/locale';
import { fieldDisplayName } from 'angled-core/queries/app';
import { WidgetFilterOpt } from 'angled-core/profile';
import { fromPredicate } from 'fp-ts/lib/Option';

type Self = number;


const interestingNumber = fromPredicate((v: number) => v > 0 || v < 0);


const renderValue =
    (unit: InformationUnitName, fieldName: string, value: Self) =>
        interestingNumber(value)
            .map(value =>
                DIV({ className: 'field__value field--read' },
                    formatNumber(value),
                    getFieldUnit(unit, fieldName).map(u => SPAN({ className: '~unit' }, u)),
                ));


export const render = (
    u: UnitData,
    fieldName: string,
    value: Self,
    _filter: WidgetFilterOpt
) =>
    DIV({
        className: `field field--${fieldName} field--read`,
        title: fieldDisplayName(typeNameToName[u.unit], fieldName),
    },
        renderValue(typeNameToName[u.unit], fieldName, value),
    );

export default render;
