import * as debug from 'debug';

import renderBoolean from './boolean';
import renderDate from './date';
import renderDecimal from './decimal';
import renderFk from './fk';
import renderGeometry from './geometry';
import renderLabel from './label';
import renderMonth from './month';
import renderNumber from './number';
import renderRaw_text from './raw_text';
import renderTerm from './term';
import renderText from './text';
import renderVarchar from './varchar';
import renderYear from './year';
import {
    InformationUnitName,
    Project,
    getFieldValue,
    getUnitbyName,
    UnitField,
    lookupNamedField,
    UnitData,
    Unit
} from 'angled-core/ui';
import { UnitWidget, WidgetFilterOpt, WidgetFilter } from 'angled-core/profile';
import { fromNullable, none, some, } from 'fp-ts/lib/Option';
import { scopeOption } from 'sdi/lib';
import { DIV } from 'sdi/components/elements';
import { ReactNode } from 'react';
import { mapOption } from 'fp-ts/lib/Array';
import { findUser } from 'angled-core/queries/ref';
import { tr } from 'sdi/locale';
import { ensureArray } from 'sdi/util';

const logger = debug('sdi:angled/components/read');



const renderField = (
    u: UnitData,
    field: UnitField,
    filter: WidgetFilterOpt,
) => {
    switch (field.kind) {
        case 'boolean':
            return renderBoolean(u, field.name, field.value, filter);
        case 'date':
            return renderDate(u, field.name, field.value, filter);
        case 'decimal':
            return renderDecimal(u, field.name, field.value, filter);
        case 'fk':
            return renderFk(u, field.name, field.value, filter);
        case 'geometry':
            return renderGeometry(u, field.name, field.value, filter);
        case 'label':
            return renderLabel(u, field.name, field.value, filter);
        case 'month':
            return renderMonth(u, field.name, field.value, filter);
        case 'number':
            return renderNumber(u, field.name, field.value, filter);
        case 'raw_text':
            return renderRaw_text(u, field.name, field.value, filter);
        case 'term':
            return renderTerm(u, field.name, field.value, filter);
        case 'text':
            return renderText(u, field.name, field.value, filter);
        case 'varchar':
            return renderVarchar(u, field.name, field.value, filter);
        case 'year':
            return renderYear(u, field.name, field.value, filter);
    }
};

const userName = (id: number) => findUser(id).fold('', u => u.name);

const renderUnauthorizedUnit = (unitName: InformationUnitName) => (u: Unit) =>
    DIV(
        { className: `unit unit--${unitName} unit--read--unauthorized` },
        `${tr.angled('audienceNotAllowed')} --- ${userName(u.user)}`
    );

export const renderUnitData = (
    unitName: InformationUnitName,
    fields: string[],
    ...units: UnitData[]
): ReactNode => {
    const elems = units.map(u =>
        mapOption(fields, fieldName =>
            fromNullable(getFieldValue(unitName, fieldName, u))
        ).map(f => renderField(u, f, none))
    );

    return DIV(
        {},
        ...elems.map(e =>
            DIV({ className: `unit unit--${unitName} unit--read` }, ...e)
        )
    );
};



const filterUnit = (
    { fieldName, term }: WidgetFilter,
) => (u: Unit) => fieldName in u && (u as any)[fieldName] === term.id;


const excludeUnit = (
    { fieldName, term }: WidgetFilter,
) => (u: Unit) => !(fieldName in u && (u as any)[fieldName] === term.id);


const renderUnitDataWithFilter = (
    unitName: InformationUnitName,
    fields: string[],
    filter: WidgetFilterOpt,
    ...units: UnitData[]
): ReactNode => {
    const elems = units.map(u =>
        mapOption(fields, fieldName =>
            fromNullable(getFieldValue(unitName, fieldName, u))
        ).map(f => renderField(u, f, filter))
    );

    return DIV(
        {},
        ...elems.map(e =>
            DIV({ className: `unit unit--${unitName} unit--read` }, ...e)
        )
    );
};

export const renderUnitWidget = (
    project: Project,
    widget: UnitWidget,
    filter: WidgetFilterOpt,
    exclude: WidgetFilterOpt,
) =>
    scopeOption()
        .let('info', lookupNamedField(widget.name))
        .let('unit', getUnitbyName(widget.name, project))
        .chain(({ info, unit }) => {
            const fieldNames = info.fields.map(f => f[1]);
            const units = (() => {
                const units = ensureArray(unit);
                if (filter.isSome()) {
                    return units.filter(filterUnit(filter.value));
                }
                if (exclude.isSome()) {
                    return units.filter(excludeUnit(exclude.value));
                }
                return units;
            })();
            const visibleUnits = units.filter(u => u.visible);
            const unauthorizedUnits = units.filter(u => u.visible === false);

            if (units.length > 0) {
                return some(DIV(
                    {
                        className: `unit__wrapper--${unit instanceof Array ? 'multi' : 'single'}`
                    },
                    renderUnitDataWithFilter(widget.name, fieldNames, filter, ...visibleUnits),
                    ...unauthorizedUnits.map(renderUnauthorizedUnit(widget.name))
                ));
            }
            return none;
        });

logger('Loaded');
