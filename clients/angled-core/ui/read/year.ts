import { UnitData, typeNameToName } from 'angled-core/ui';
import { DIV } from 'sdi/components/elements';
import { fieldDisplayName } from 'angled-core/queries/app';
import { WidgetFilterOpt } from 'angled-core/profile';
import { fromPredicate } from 'fp-ts/lib/Option';

type Self = number;

const interestingYear = fromPredicate((v: number) => v > 1900);

const renderValue =
    (value: Self) => interestingYear(value).map(year =>
        DIV({ className: 'field__value field--read' }, year));

export const render = (
    u: UnitData,
    fieldName: string,
    value: Self,
    _filter: WidgetFilterOpt
) =>
    DIV({
        className: `field field--${fieldName} field--read`,
        title: fieldDisplayName(typeNameToName[u.unit], fieldName),
    },
        renderValue(value),
    );

export default render;
