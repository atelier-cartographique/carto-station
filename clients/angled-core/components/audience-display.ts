import { radioRenderer } from 'sdi/components/input';
import { NodeOrOptional } from 'sdi/components/elements';
import { DIV } from 'sdi/components/elements';
import { tr, fromRecord } from 'sdi/locale';
import { nameToString } from 'sdi/components/button/names';
import { divTooltipBottomLeft } from 'sdi/components/tooltip';
import {
    resetDisplayAudience,
    setDisplayAudience,
} from 'angled-core/events/ui';
import { getAudienceList } from 'angled-core/queries/ref';
import {
    audienceIsForDisplay,
    getDisplayAudience,
} from 'angled-core/queries/ui';
import { Audience } from 'angled-core/ref';
import { makeLabelAndIcon } from './buttons';

export const renderAudience = (audience: Audience) =>
    DIV('audience--item', fromRecord(audience.name));

const resetButton = makeLabelAndIcon('cancel', 1, 'times', () =>
    tr.angled('quitAudienceManager')
);

const selectAudience = radioRenderer<Audience>(
    'audience-button',
    renderAudience,
    setDisplayAudience,
    audienceIsForDisplay
);

const audienceClassName = () =>
    getDisplayAudience()
        .map(() => 'active')
        .getOrElse('');

export const renderAudienceDisplayWidget = (
    ...extras: ((a: Audience) => NodeOrOptional)[]
) =>
    DIV(
        `audience-display ${audienceClassName()}`,
        DIV(
            'widget__wrapper',
            DIV('widget-title', tr.angled('audiences')),
            divTooltipBottomLeft(
                tr.angled('helptextAudienceManager'),
                { className: 'edit-audience__help' },
                DIV('icon fa', nameToString('info-circle'))
            ),
            selectAudience(getAudienceList()),
            getDisplayAudience().map(a =>
                DIV(
                    'actions',
                    resetButton(resetDisplayAudience),
                    ...extras.map(f => f(a))
                )
            )
        )
    );
