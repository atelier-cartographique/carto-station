import { DIV, NODISPLAY, H2, SPAN } from 'sdi/components/elements';
import { ModalRender, ModalClose } from 'sdi/components/modal';
import { makeLabel, makeIcon } from '../buttons';
import tr from 'sdi/locale';
import { inputText } from 'sdi/components/input';
import {
    getTeamInputFilter,
    getTeamFilteredContact,
    getFormInputTeamMembers,
    getTeamFilteredContactExists,
} from 'angled-core/queries/ui';
import {
    setTeamInputFilter,
    setFormInputTeamMembers,
    clearTeamInputFilter,
} from 'angled-core/events/ui';
import { Contact, TeamMemberRole } from 'angled-core/ref';
import { createTeam, createContact } from 'angled-core/events/ref';
import { getContactList } from 'angled-core/queries/ref';
import { fromNullable } from 'fp-ts/lib/Option';
import { stringToParagraphs } from 'angled-core/../sdi/util';
import { Tooltip } from 'angled-core/../sdi/components/tooltip';

const addTeamMembers = (c: Contact, r: TeamMemberRole) => {
    setFormInputTeamMembers(getFormInputTeamMembers().concat([[c, r]]));
};

const removeTeamMembers = (c: Contact) => {
    const currentTeamMembers = getFormInputTeamMembers().filter(
        ([m, _r]) => m.id !== c.id
    );
    setFormInputTeamMembers(currentTeamMembers);
};

const setTeamMemberRole = (c: Contact, role: TeamMemberRole) => {
    setFormInputTeamMembers(
        getFormInputTeamMembers().map(([m, r]) => {
            if (m.id === c.id) {
                return [m, role];
            }
            return [m, r];
        })
    );
};

const createTeamButton = makeLabel('save', 1, () =>
    tr.angled('saveAndContinue')
);
const cancelButton = makeLabel('cancel', 2, () => tr.angled('cancel'));

const header = () => H2({}, tr.angled('createTeam'));
const footer = (close: ModalClose) =>
    DIV(
        { className: 'modal__footer__inner' },
        cancelButton(() => {
            clearTeamInputFilter();
            setFormInputTeamMembers([]);
            close();
        }),
        createTeamButton(() => {
            createTeam(
                getFormInputTeamMembers().map(([c, r]) => [c.id, r]),
                'bidders',
                'bidders_team'
            );
            clearTeamInputFilter();
            close();
        })
    );

const renderFilterInput = () =>
    inputText({
        key: `filter-contact-input`,
        get: getTeamInputFilter,
        set: setTeamInputFilter,
        attrs: {
            placeholder: tr.angled('filterContact'),
        },
        monitor: setTeamInputFilter,
    });

const renderContact = (c: Contact) =>
    DIV(
        {
            key: `contact-filtered-${c.id}`,
            className: 'contact',
            onClick: () => addTeamMembers(c, 'REGU'),
        },
        c.name
    );

const renderCreateContact = () => {
    const filter = getTeamInputFilter().trim();
    if (getTeamFilteredContactExists() || filter.length === 0) {
        return NODISPLAY();
    }

    const createButton = makeLabel('add', 2, () =>
        tr.angled('createNewContact')
    );

    return DIV(
        {},
        DIV(
            { className: 'helptext' },
            stringToParagraphs(
                `${tr.angled('createTeamCreateContact')} "${filter}" ?`
            )
        ),
        createButton(() => {
            createContact(filter, 'bidders', 'contact').then(() =>
                // TODO: check unit and fieldName (nw)
                fromNullable(getContactList().find(c => c.name === filter)).map(
                    c => {
                        clearTeamInputFilter();
                        addTeamMembers(c, 'REGU');
                    }
                )
            );
        })
    );
};

const renderFilteredTitle = () => {
    const filter = getTeamInputFilter().trim();
    if (filter.length > 0) {
        return H2({}, tr.angled('filterResult') + ` "${filter}"`);
    }
    return NODISPLAY();
};

const renderFilteredContacts = () =>
    DIV(
        { className: 'filtered-result' },
        getTeamFilteredContact().map(renderContact),
        renderCreateContact()
    );

const removeToolTip: Tooltip = {
    text: () => tr.core('remove'),
    position: 'left',
}; // TODO: check position
const buttonRemoveContact = makeIcon('reset', 3, 'times', removeToolTip);

const renderRoleSelect = (contact: Contact, role: TeamMemberRole) =>
    DIV(
        {
            className: 'switch-team-role',
            onClick: () => {
                setTeamMemberRole(contact, role === 'MAIN' ? 'REGU' : 'MAIN');
            },
        },
        DIV(
            `select-team-role ${role === 'MAIN' ? 'active' : ''}`,
            tr.angled('memberRoleMain')
        ),
        DIV(`select-team-separator`, ' | '),
        DIV(
            `select-team-role ${role === 'REGU' ? 'active' : ''}`,
            tr.angled('memberRoleRegular')
        )
    );

const renderSelectedContact = ([c, role]: [Contact, TeamMemberRole]) =>
    DIV(
        {
            key: `contact-filtered-${c.id}`,
            className: 'contact-selected',
        },
        SPAN(
            {},
            buttonRemoveContact(() => removeTeamMembers(c))
        ),
        SPAN({}, c.name),
        renderRoleSelect(c, role)
    );

const renderSelectedContacts = () =>
    DIV({}, getFormInputTeamMembers().map(renderSelectedContact));

const body = () =>
    DIV(
        { className: 'modal__content--body' },
        DIV(
            { className: 'helptext' },
            stringToParagraphs(tr.angled('helptext:createTeam'))
        ),
        DIV(
            { className: 'modal-column__wrapper modal--team' },
            DIV(
                { className: 'column select-contact' },
                H2({ className: 'head-title' }, tr.angled('selectTeamMembers')),
                renderFilterInput(),
                renderFilteredTitle(),
                renderFilteredContacts()
            ),
            DIV(
                { className: 'column' },
                H2({ className: 'head-title' }, tr.angled('team')),
                renderSelectedContacts()
            )
        )
    );

export const render: ModalRender = { header, footer, body };
