
import { Nullable } from 'sdi/util';

// State Augmentation
declare module 'sdi/shape' {
    export interface IShape {
        'component/sub/selected': Nullable<number>;
    }
}


export const defaultSub =
    () => ({
        'component/sub/selected': null,
    });