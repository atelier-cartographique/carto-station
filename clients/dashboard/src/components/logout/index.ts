import { DIV, BUTTON, A, H2 } from 'sdi/components/elements';
import { logoutButton, username } from 'sdi/components/header';
import { IUser } from 'sdi/source';
import tr from 'sdi/locale';

import { markdown } from 'sdi/ports/marked';
import { getUserData, tryLogout } from 'sdi/app';

const disclaimer = () =>
    DIV(
        { className: 'user__disclaimer' },
        markdown(tr.dashboard('dashboard.disclaimer'), 'md')
    );

const renderLoggedIn = (user: IUser) =>
    DIV(
        { className: 'login-widget' },
        H2({}, tr.dashboard('yourAccount')),
        DIV({ className: 'logout-username' }, username(user)),
        logoutButton(tryLogout),
        disclaimer()
    );

const renderAnonymous = () =>
    DIV(
        {},
        BUTTON(
            { className: 'login-widget btn btn-1 btn-navigate' },

            A({ className: '', href: '/client/login/' }, tr.dashboard('login')),
            disclaimer()
        )
    );

const render = () => getUserData().foldL(renderAnonymous, renderLoggedIn);

export default render;
