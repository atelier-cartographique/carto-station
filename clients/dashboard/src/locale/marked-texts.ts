export const homeTextFR = `
Bienvenue sur le tableau de bord de *Geodata*.

Chacune des applications propose un set de fonctionnalités spécifiques permettant d'interagir avec les données de la plateforme.

Cet ensemble applicatif est orienté vers la publication, la visualisation et l'encodage de données spatiales.

*Geodata* est basé sur le logiciel *cartostation*.  Ainsi, la [documentation](https://cartostation.com/documentation/fr/index) de *Geodata* correspond à celle de *cartostation*.
`;

export const homeTextNL = `
Welkom op het *Geodata* dashboard.

Elke applicatie biedt een specifieke set van functies om te communiceren met de data van het platform.

Deze set applicaties is gericht op het publiceren, visualiseren en coderen van ruimtelijke gegevens.

*Geodata* is gebasered op the software *cartostation*. De [documentatie](https://cartostation.com/documentation/nl/index) van *Geodata* is geintegreerd in die van *cartostation*.
`;

export const homeDisclaimerFR = ``; // to be defined in admin/lingua
export const homeDisclaimerNL = ``; // to be defined in admin/lingua
