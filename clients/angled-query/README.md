![query example](../assets/image/query-example.png)
_Query result example_

The application _angled-query_ allows to filter and extract projects encoded via the application [_angled-project_](angled-project), in order to be able to consult them in the form of a listing, to make exports exploitable in a spreadsheet, or to create dynamic maps.

To do this, the application offers a dedicated query creation wizard, which allows users who are not familiar with SQL to extract data on a more semantic basis.

In a second step, once the query is built, it is possible to choose which information will be part of the result of this query.

A query can be saved for personal use, or shared with other users of the application.

The query result is dynamic, which means that it always presents up-to-date results, depending on what is encoded in the [_angled-project_](angled-project) application, either in the form of a table or a map.

## User guide

The complete user guide is available here : https://cartostation.com/documentation
