import { queryK, query, subscribe } from 'sdi/shape';
import {
    TableDataRow,
    TableDataType,
    tableQueries,
    TableDataCell,
    TableSource,
} from 'sdi/components/table';
import {
    nameToTypeName,
    InformationUnitName,
    lookupNamedFieldType,
    isMulti,
    Unit,
    getUnitbyName,
    lookupNamedField,
    mapFieldName,
    MappedField,
} from 'angled-core/ui';
import { getResults, getSelectQuery } from './builder';
import {
    Aggregate,
    AggregateFilter,
    Field,
    QuerySelect,
    ResultRow,
} from '../remote/query';
import tr, { formatNumber, formatDate, fromRecord } from 'sdi/locale';
import {
    MessageRecord,
    remoteToOption,
    getFeatureProp,
    RemoteResource,
    FeatureCollection,
    Feature,
} from 'sdi/source';
import { uniq, isNotNullNorUndefined, ensureArray, zip3 } from 'sdi/util';
import { Setoid } from 'fp-ts/lib/Setoid';
import { formatTermValue } from 'angled-core/ui/read/term';
import { formatFkValue } from 'angled-core/ui/read/fk';
import { fromNullable, Option, none, some } from 'fp-ts/lib/Option';
import { FeaturePath } from 'sdi/map';
import { scopeOption, Pair, pair, iife } from 'sdi/lib';
import { fieldDisplayName, unitDisplayName } from 'angled-core/queries/app';
import { findTerm } from 'angled-core/queries/universe';
import { catOptions } from 'fp-ts/lib/Array';
import { getAlias, getUserIdAstNumber } from 'sdi/app';
import { getDisplayAudience } from 'angled-core/queries/ui';

const fieldValueToStringSingle = (
    r: unknown,
    u: InformationUnitName,
    fieldName: string
): string => {
    // const fieldName = k.split('.')[1];
    const fieldType = lookupNamedFieldType(u, fieldName).getOrElse('number');
    if (r === null || r === undefined) {
        return '-';
    } else {
        switch (typeof r) {
            case 'string':
                return r;
            case 'number': {
                switch (fieldType) {
                    case 'date':
                        return formatDate(new Date(r));
                    case 'term':
                        return formatTermValue(r);
                    case 'fk':
                        return formatFkValue(fieldName, r).getOrElse(''); // TODO get fieldName from unit
                    default:
                        return formatNumber(r);
                }
            }
            case 'boolean':
                return r ? tr.angled('true') : tr.angled('false');
            case 'object': {
                if (Array.isArray(r)) {
                    return r
                        .map(r => fieldValueToString(r, u, fieldName))
                        .join(', ');
                }
                try {
                    return fromRecord(r as MessageRecord);
                } catch (_err) {
                    return (r as any).toString();
                }
            }
            default: {
                try {
                    return (r as any).toString();
                } catch (_err) {
                    return '-';
                }
            }
        }
    }
};

const fieldValueToStringMulti = (
    r: unknown,
    u: InformationUnitName,
    fieldName: string
): string => {
    if (r === null || r === undefined) {
        return '-';
    }
    return ensureArray(r)
        .map(i => fieldValueToStringSingle(i, u, fieldName))
        .join(', ');
};

const fieldValueToString = (
    r: unknown,
    u: InformationUnitName,
    fieldName: string
): string => {
    if (isMulti(u)) {
        return fieldValueToStringMulti(r, u, fieldName);
    }
    return fieldValueToStringSingle(r, u, fieldName);
};

const setoidSelectUnit: Setoid<QuerySelect> = {
    equals: (a, b) => a.unit === b.unit,
};

export const uniqSelectUnit = uniq(setoidSelectUnit);

const findMappedField = (unitName: InformationUnitName, field: string) =>
    lookupNamedField(unitName).chain(unit =>
        fromNullable(unit.fields.find(f => mapFieldName(f) === field))
    );

export const getSelectedFields = () => query('component/builder/select');

export const getSelectedFieldsMapped = () =>
    getSelectedFields().map<[QuerySelect, Option<MappedField>]>(s => [
        s,
        findMappedField(s.unit, s.field[0]),
    ]);

// type SelectableCase = 'not-selected' | 'selected-single' | 'selected-multi';

export const isSelectedField = (unit: InformationUnitName, field: string) =>
    getSelectedFields().findIndex(
        s => s.unit === unit && s.field[0] === field
    ) >= 0;

export const getSelectableUnits = () => {
    // const sks = getSelectedFields().map(select => select.unit);
    const allUnits = (
        Object.keys(nameToTypeName) as InformationUnitName[]
    ).sort((a, b) => unitDisplayName(a).localeCompare(unitDisplayName(b)));

    return allUnits.map(lookupNamedField);
    // return allUnits.filter((u) => {
    //     const sc: SelectableCase =
    //         sks.indexOf(u) < 0
    //             ? 'not-selected'
    //             : isMulti(u)
    //                 ? 'selected-multi'
    //                 : 'selected-single';
    //     switch (sc) {
    //         case 'not-selected':
    //             return true;
    //         case 'selected-single':
    //             return false;
    //         case 'selected-multi':
    //             return true;
    //     }
    // });
};

// const isNotNone = isNotNullNorUndefined;
// /**
//  * This function shall mirror the one found in angled.query module: `make_field`
//  * If it feels a bit pythonic to you, good, it's intended
//  *
//  * @param select
//  */
// const keyFromSelect = (select: QuerySelect): string => {
//     const comps: string[] = [select.unit, select.field.join('__')];
//     const aggregate = select.aggregate;
//     if (isNotNone(aggregate)) {
//         comps.push(aggregate.name);
//         const filter = aggregate.filter;
//         if (isNotNone(filter)) {
//             comps.push(filter.termId.toString());
//         }
//     }

//     return comps.join('.');
// };

const filterDisplayName =
    (unit: InformationUnitName) => (filter: AggregateFilter) => {
        switch (filter.tag) {
            case 'term':
                return findTerm(filter.termId)
                    .map<string>(term => fromRecord(term.name))
                    .getOrElse(filter.termId.toString());
            case 'number': {
                const op = iife(() => {
                    switch (filter.op) {
                        case 'eq':
                            return '=';
                        case 'gt':
                            return '>';
                        case 'gte':
                            return '≥';
                        case 'lt':
                            return '<';
                        case 'lte':
                            return '≤';
                    }
                });
                return `${fieldDisplayName(unit, filter.field)} ${op} ${
                    filter.value
                }`;
            }
        }
    };

export const getFilter = (agg: Aggregate) =>
    agg.filter.length > 0 ? some(agg.filter) : none;

export const aggregateDisplayName =
    (unit: InformationUnitName) => (agg: Aggregate) => {
        const op = (() => {
            switch (agg.name) {
                case 'concat':
                    return tr.angled('aggregate/concat');
                case 'mean':
                    return tr.angled('aggregate/mean');
                case 'sum':
                    return tr.angled('aggregate/sum');
                case 'min':
                    return tr.angled('aggregate/min');
                case 'max':
                    return tr.angled('aggregate/max');
            }
        })();
        const filter = getFilter(agg)
            .map(
                filter => `/ ${filter.map(filterDisplayName(unit)).join(', ')}`
            )
            .getOrElse('');

        return `* ${op} ${filter} `;
    };

export const defaultFieldNameParts = (
    unit: InformationUnitName,
    field: string
) => {
    const justOne = lookupNamedField(unit)
        .map(fs => fs.fields.length === 1)
        .getOrElse(false);
    const unitName = unitDisplayName(unit);
    if (justOne) {
        return { unitName, fieldName: none };
    }
    const fieldName = fieldDisplayName(unit, field);
    return { fieldName: some(fieldName), unitName };
};

export const defaultFieldName = (
    unit: InformationUnitName,
    field: string
): string => {
    const { fieldName, unitName } = defaultFieldNameParts(unit, field);
    return fieldName.fold(
        `${unitName}`,
        fieldName => `${unitName} → ${fieldName}`
    );
};

export const defaultFieldNameFromSelect = (
    select: Omit<QuerySelect, 'name'>
): string => {
    const baseName = defaultFieldName(select.unit, select.field[0]);
    const aggregate = fromNullable(select.aggregate).map(
        aggregateDisplayName(select.unit)
    );

    return aggregate.fold(baseName, agg => `${baseName} ${agg}`);
};

const filterCodeName = (filter: AggregateFilter) => {
    switch (filter.tag) {
        // case 'term': return findTerm(filter.termId)
        //     .map<string>(term => fromRecord(term.name))
        //     .getOrElse(filter.termId.toString());
        case 'term':
            return filter.termId.toString();
        case 'number': {
            const op = filter.op;
            return `${filter.field}.${op}(${filter.value})`;
        }
    }
};

export const aggregateCodeName = (agg: Aggregate) => {
    const op = agg.name;
    const filter = getFilter(agg)
        .map(filter => `/ ${filter.map(filterCodeName).join('+')}`)
        .getOrElse('');
    return `*${op}${filter}`;
};

export const defaultCodeNameParts = (
    unit: InformationUnitName,
    field: string
) => {
    const justOne = lookupNamedField(unit)
        .map(fs => fs.fields.length === 1)
        .getOrElse(false);
    if (justOne) {
        return { unitName: unit, fieldName: none };
    }
    return { fieldName: some(field), unitName: unit };
};

export const defaultCodeName = (
    unit: InformationUnitName,
    field: Field
): string => `${unit}.${field.join('__')}`;

export const defaultCodeNameFromSelect = (
    select: Omit<QuerySelect, 'name'>
): string => {
    const baseName = defaultCodeName(select.unit, select.field);
    const aggregate = fromNullable(select.aggregate).map(aggregateCodeName);

    return aggregate.fold(baseName, agg => `${baseName}${agg}`);
};

export const getSelectQueryName = (name: string) =>
    fromNullable(getSelectedFields().find(select => select.name === name)).map(
        defaultFieldNameFromSelect
    );

export const getSelectUiIndex = () => {
    const ui = query('component/builder/select/ui');
    if (isNotNullNorUndefined(ui) && isNotNullNorUndefined(ui.index)) {
        return some(ui.index);
    }
    return none;
};

export const getUnitsFromSelect = () => getSelectQuery().map(s => s.unit);

export const getFieldFromSelect = () => getSelectQuery().map(s => s.field[0]);

export const getRawData = () => remoteToOption(getResults()).getOrElse([]);

// export const getKeys = () => getSelectQuery().map(keyFromSelect);

export const getResultsData = subscribe(
    'data/results',
    (): TableDataRow[] => {
        // const units = getUnitsFromSelect();
        // const keys = getResultsKeys();
        // const fields = getFieldFromSelect();
        const pack = zip3(
            getResultsKeys(),
            getUnitsFromSelect(),
            getFieldFromSelect()
        );
        const getCells = (row: ResultRow): TableDataCell[] =>
            pack.map(([key, unit, field]) =>
                fieldValueToString(row[key], unit, field)
            );

        const data = getRawData().map<TableDataRow>(r => ({
            from: r.id,
            cells: [r.id.toString()].concat(getCells(r)),
        }));

        return data;
    },
    'component/builder/select'
);

export const getResultsKeys = () => getSelectQuery().map(s => s.name);

const getResultsTypes = (): TableDataType[] =>
    getResultsKeys().map<TableDataType>(_k => 'string'); // TODO real types

export const getSource = (): TableSource => ({
    kind: 'local',
    data: getResultsData(),
    keys: ['id'].concat(getResultsKeys()).map(getAlias),
    types: ['number' as TableDataType].concat(getResultsTypes()),
});

export const resultTableQueries = tableQueries(
    queryK('component/preview/table'),
    getSource
);

export const audienceHighlighter = (from: number | string, column: number) => {
    if (column === 0) {
        return false;
    }
    const current = getDisplayAudience().toNullable();
    if (current === null) {
        return false;
    }
    const row = getRawData().find(r => r.id === from);
    if (row === undefined) {
        return false;
    }
    const key = getResultsKeys()[column - 1];
    const audiences = row.__audiences__[key];
    return audiences.indexOf(current.id) >= 0;
};

const findFeature = (pid: number, rdata: RemoteResource<FeatureCollection>) =>
    rdata.tag === 'success'
        ? fromNullable(
              rdata.data.features.find(
                  f => getFeatureProp(f, 'id', pid + 1) === pid
              )
          )
        : none;

export const getFeatureForSelected = (pid: number): Option<Feature> => {
    if (!query('component/preview/map')) {
        return none;
    }
    const polygon = findFeature(
        pid,
        query('component/preview/geojson/polygon')
    );

    const point = findFeature(pid, query('component/preview/geojson/point'));
    const line = findFeature(pid, query('component/preview/geojson/line'));

    if (polygon.isSome()) {
        return polygon;
    }
    if (point.isSome()) {
        return point;
    }
    if (line.isSome()) {
        return line;
    }
    return none;
};

const getFeaturePathForSelected = (pid: number): Option<FeaturePath> => {
    if (!query('component/preview/map')) {
        return none;
    }
    const polygon = findFeature(
        pid,
        query('component/preview/geojson/polygon')
    ).map<FeaturePath>(() => ({ layerId: 'MultiPolygon', featureId: pid }));

    const point = findFeature(
        pid,
        query('component/preview/geojson/point')
    ).map<FeaturePath>(() => ({ layerId: 'MultiPoint', featureId: pid }));
    const line = findFeature(
        pid,
        query('component/preview/geojson/line')
    ).map<FeaturePath>(() => ({ layerId: 'MultiLineString', featureId: pid }));

    if (polygon.isSome()) {
        return polygon;
    }
    if (point.isSome()) {
        return point;
    }
    if (line.isSome()) {
        return line;
    }
    return none;
};

export const getSelectedRow = () =>
    fromNullable(query('component/preview/table/selected'));

export const getSelected = () =>
    getSelectedRow()
        .chain<FeaturePath>(getFeaturePathForSelected)
        .getOrElse({ featureId: null, layerId: null });

export const getSelectedData = (): Option<
    [InformationUnitName, string, string][]
> =>
    fromNullable(query('component/preview/table/selected'))
        .chain(pid =>
            fromNullable(
                getRawData().find(raw => 'id' in raw && raw.id === pid)
            )
        )
        .map(raw =>
            zip3(
                getResultsKeys(),
                getUnitsFromSelect(),
                getFieldFromSelect()
            ).map<[InformationUnitName, string, string]>(([k, u, f]) => [
                u,
                k,
                fieldValueToString(raw[k], u, f),
            ])
        );

export type ProjectedData = Pair<Pair<string, string>, Unit | Unit[]>[];

export const getProjectedSelectedData = () =>
    scopeOption()
        .let('pid', fromNullable(query('component/preview/table/selected')))
        .let('data', getSelectedData())
        .let('resource', ({ pid }) => getProjectResource(pid))
        .let('project', ({ resource }) => remoteToOption(resource))
        .map(({ data, project }) =>
            catOptions(
                data.map(([unitName, key, value]) =>
                    getUnitbyName(unitName, project).map(unit =>
                        pair(pair(key, value), unit)
                    )
                )
            )
        );

export const previewMapName = 'preview-map';

export const mapIsVisible = queryK('component/preview/map');

export const getLoadingLayers = queryK('component/preview/map/loading');

export const getProjectResource = (pid: number) =>
    fromNullable(query('data/projects')[pid.toString()]);

export const getTags = () => query('data/tags');

export const findTag = (id: number) =>
    fromNullable(getTags().find(t => id === t.id));

export const getCurrentQueryData = () =>
    getUserIdAstNumber().map(user => {
        const where = query('component/builder/where');
        const select = query('component/builder/select');
        return { user, statements: { where, select } };
    });
