import {
    fetchIO,
    IUser,
    IUserIO,
    IAlias,
    IAliasIO,
    postIO,
    putIO,
} from 'sdi/source';
import { ProjectIO } from 'angled-core/ui';
import { getApiUrl } from 'sdi/app';

export const fetchUser = (url: string): Promise<IUser> => fetchIO(IUserIO, url);

export const postAlias = (
    url: string,
    data: Omit<IAlias, 'id'>
): Promise<IAlias> => postIO(IAliasIO, url, data);

export const putAlias = (url: string, data: IAlias): Promise<IAlias> =>
    putIO(IAliasIO, url, data);

export const fetchProject = (pid: number | string) =>
    fetchIO(
        ProjectIO,
        getApiUrl(`geodata/angled/p/project/${pid}/?at=${Date.now()}`)
    );
