import * as io from 'io-ts';
import {
    fetchIO,
    postIO,
    putIO,
    postUnrelatedIO,
    deleteIO,
    Inspire,
    InspireIO,
} from 'sdi/source';
import {
    InformationUnitNameIO,
    FieldValueTypeIO,
    InformationUnitName,
} from 'angled-core/ui';
import {
    MessageRecordIO,
    MessageRecordLangIO,
    nullable,
} from 'sdi/source/io/io';
import { Nullable } from 'sdi/util';

// tslint:disable-next-line: variable-name
const OpIO = io.union(
    [
        io.literal('exact'),
        io.literal('contains'),
        io.literal('icontains'),
        io.literal('istartswith'),
        io.literal('iendswith'),
        io.literal('gt'),
        io.literal('gte'),
        io.literal('lt'),
        io.literal('lte'),
        io.literal('intersects'),
    ],
    'OpIO'
);

// tslint:disable-next-line: variable-name
const AggregateNameIO = io.union(
    [
        io.literal('sum'),
        io.literal('mean'),
        io.literal('concat'),
        io.literal('min'),
        io.literal('max'),
    ],
    'AggregateNameIO'
);

// Here we've got an array of the form [fieldName<, accessor>]
// think something like ['name', 'fr'] in case of a LinguaField
const FieldIO = io.union([
    io.tuple([io.string]),
    io.tuple([io.string, MessageRecordLangIO]),
]);
export type Field = io.TypeOf<typeof FieldIO>;

// tslint:disable-next-line: variable-name
const QueryFilterIO = io.interface(
    {
        tag: io.literal('filter'),
        field: FieldIO,
        op: OpIO,
        value: FieldValueTypeIO,
        negate: io.boolean,
    },
    'QueryFilterIO'
);

// tslint:disable-next-line: variable-name
const QueryStatementIO = io.interface(
    {
        tag: io.literal('statement'),
        unit: InformationUnitNameIO,
        filters: io.array(QueryFilterIO),
    },
    'QueryStatementIO'
);

// tslint:disable-next-line: variable-name
const ConjIO = io.union([io.literal('AND'), io.literal('OR')], 'ConjIO');

// tslint:disable-next-line: variable-name
const QueryConjIO = io.interface(
    {
        tag: io.literal('conj'),
        conj: ConjIO,
    },
    'QueryConjIO'
);

const AggregateFilterTermIO = io.interface(
    {
        tag: io.literal('term'),
        field: io.string,
        termId: io.Integer,
    },
    'AggregateFilterTermIO'
);

const AggregateFilterNumberOpIO = io.union(
    [
        io.literal('gt'),
        io.literal('gte'),
        io.literal('lt'),
        io.literal('lte'),
        io.literal('eq'),
    ],
    'AggregateFilterNumberOpIO'
);

const AggregateFilterNumberIO = io.interface(
    {
        tag: io.literal('number'),
        field: io.string,
        value: io.number,
        op: AggregateFilterNumberOpIO,
    },
    'AggregateFilterNumberIO'
);

const AggregateFilterIO = io.union(
    [AggregateFilterTermIO, AggregateFilterNumberIO],
    'AggregateFilterIO'
);

// tslint:disable-next-line: variable-name
const AggregateIO = io.interface(
    {
        tag: io.literal('aggregate'),
        name: AggregateNameIO,
        filter: io.array(AggregateFilterIO),
    },
    'AggregateIO'
);

// tslint:disable-next-line: variable-name
const QuerySelectIO = io.interface(
    {
        tag: io.literal('select'),
        name: io.string,
        unit: InformationUnitNameIO,
        field: FieldIO,
        aggregate: nullable(AggregateIO),
        filters: io.array(QueryFilterIO),
    },
    'QuerySelectIO'
);

// tslint:disable-next-line: variable-name
const QueryWhereIO = io.union([QueryStatementIO, QueryConjIO], 'QueryWhereIO');

// tslint:disable-next-line: variable-name
const QueryDataIO = io.interface(
    {
        where: io.array(QueryWhereIO),
        select: io.array(QuerySelectIO),
    },
    'QueryDataIO'
);

// tslint:disable-next-line: variable-name
const QueryIO = io.interface(
    {
        id: io.Integer,
        user: io.Integer,
        statements: QueryDataIO,
        name: MessageRecordIO,
    },
    'QueryIO'
);

// tslint:disable-next-line: variable-name
const QueryShareIO = io.interface(
    {
        id: io.Integer,
        query: io.Integer,
        description: MessageRecordIO,
    },
    'QueryIO'
);

// tslint:disable-next-line: variable-name
const ResultRowIO = io.intersection(
    [
        io.interface({
            id: io.Integer,
            __audiences__: io.dictionary(io.string, io.array(io.Integer)),
        }),
        io.dictionary(io.string, io.any),
    ],
    'ResultRowIO'
);

export type ResultRow = io.TypeOf<typeof ResultRowIO>;

export type Op = io.TypeOf<typeof OpIO>;
export type AggregateName = io.TypeOf<typeof AggregateNameIO>;
export type AggregateFilterTerm = io.TypeOf<typeof AggregateFilterTermIO>;
export type AggregateFilterNumberOp = io.TypeOf<
    typeof AggregateFilterNumberOpIO
>;
export type AggregateFilterNumber = io.TypeOf<typeof AggregateFilterNumberIO>;
export type AggregateFilter = io.TypeOf<typeof AggregateFilterIO>;
export type Aggregate = io.TypeOf<typeof AggregateIO>;
export type Conj = io.TypeOf<typeof ConjIO>;
export type QueryFilter = io.TypeOf<typeof QueryFilterIO>;
export type QueryConj = io.TypeOf<typeof QueryConjIO>;
export type QueryStatement = io.TypeOf<typeof QueryStatementIO>;
export type QueryWhere = io.TypeOf<typeof QueryWhereIO>;
export type QuerySelect = io.TypeOf<typeof QuerySelectIO>;
export type QueryData = io.TypeOf<typeof QueryDataIO>;
export type Query = io.TypeOf<typeof QueryIO>;
export type QueryShare = io.TypeOf<typeof QueryShareIO>;

// utilities

export const querySelect = (
    name: string,
    unit: InformationUnitName,
    field: Field,
    aggregate: Nullable<Aggregate>,
    filters: QueryFilter[]
): QuerySelect => ({ tag: 'select', name, unit, field, aggregate, filters });

export const aggFilterTerm = (
    field: string,
    termId: number
): AggregateFilterTerm => ({
    tag: 'term',
    field,
    termId,
});

export const aggFilterNumber = (
    field: string,
    value: number,
    op: AggregateFilterNumberOp
): AggregateFilterNumber => ({
    tag: 'number',
    field,
    value,
    op,
});

export const aggregate = (
    name: AggregateName,
    filter = [] as AggregateFilter[]
): Aggregate => ({
    tag: 'aggregate',
    name,
    filter,
});

export const fetchQueries = (url: string) => fetchIO(io.array(QueryIO), url);

export const postQuery = (url: string, data: Partial<Query>) =>
    postIO(QueryIO, url, data);

export const putQuery = (url: string, data: Partial<Query>) =>
    putIO(QueryIO, url, data);

export const postExecQuery = (url: string, data: Partial<Query>) =>
    postUnrelatedIO(io.array(ResultRowIO), url, data);

export const fetchShares = (url: string) =>
    fetchIO(io.array(QueryShareIO), url);

export const postShare = (url: string, data: Partial<QueryShare>) =>
    postIO(QueryShareIO, url, data);

export const deleteShare = (url: string) => deleteIO(url);

export const postMetadata = (url: string, data: Partial<Inspire>) =>
    postIO(InspireIO, url, data);

export const lookupMetadata = (url: string) => fetchIO(InspireIO, url);
