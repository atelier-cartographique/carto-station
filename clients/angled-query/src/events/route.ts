import * as debug from 'debug';

import { index } from 'fp-ts/lib/Array';
import { literal, union, TypeOf } from 'io-ts';

import { getNumber } from 'sdi/util';
import { Router, Path } from 'sdi/router';

import { activityLogger, setLayout } from './app';
import {
    clearAll,
    resetCurrentQuery,
    setWhereStep,
    selectQuery,
    execQuery,
} from './builder';
import { findQuery, getCurrentQuery } from '../queries/builder';
import { navigateQueryAction } from 'sdi/activity';

const logger = debug('sdi:route');

// tslint:disable-next-line: variable-name
const RouteIO = union([
    literal('index'),
    literal('builder'),
    literal('preview'),
    // literal('map'),
]);
type Route = TypeOf<typeof RouteIO>;

const { home, route, navigate } = Router<Route>('angled-query');

const queryIdParser = (p: Path) => index(0, p).chain(getNumber);

home('index', _p => {
    clearAll();
    setLayout('landing');
});

route(
    'builder',
    qidOpt => {
        qidOpt.foldL(
            () => {
                resetCurrentQuery();
            },
            qid =>
                getCurrentQuery().foldL(
                    () => {
                        selectQuery(qid);
                    },
                    selected => {
                        if (selected.id !== qid) {
                            selectQuery(qid);
                        }
                    }
                )
        );
        setWhereStep('Initial');
        setLayout('builder');
    },
    queryIdParser
);

route(
    'preview',
    qidOpt => {
        qidOpt.map(qid =>
            getCurrentQuery().foldL(
                () => {
                    selectQuery(qid);
                    findQuery(qid).map(q => activityLogger(navigateQueryAction(q.id.toString(), q.name)))
                },
                selected => {
                    if (selected.id !== qid) {
                        selectQuery(qid);
                    }
                    findQuery(qid).map(q => activityLogger(navigateQueryAction(q.id.toString(), q.name)))
                }
                )
                );
        execQuery();
        setLayout('preview');
    },
    queryIdParser
);

// route('map', (qidOpt) => {
//     qidOpt.foldL(
//         () => { execQuery(); },
//         (qid) => {
//             selectQuery(qid);
//             execSavedQuery();
//         }
//     );
//     setLayout('preview-map');
// }, queryIdParser);

export const navigateIndex = () => navigate('index', []);

export const navigateNewQuery = () => navigate('builder', []);

export const navigateEdit = (id: number) => navigate('builder', [id]);

export const navigateExecSaved = (id: number) => navigate('preview', [id]);

export const navigateExec = () => navigate('preview', []);

// export const navigateMap =
//     (id: number) => navigate('map', [id]);

export const loadRoute = (initial: string[]) =>
    index(0, initial).map(prefix =>
        RouteIO.decode(prefix).map(c => navigate(c, initial.slice(1)))
    );

logger('loaded');
