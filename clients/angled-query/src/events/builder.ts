import * as debug from 'debug';
import { assign, query, dispatch, assignK } from 'sdi/shape';
import {
    FieldValueType,
    geometryTypeForUnit,
    GeometryUnit,
    InformationUnitName,
    lookupNamedField,
    MappedField,
    mapFieldType,
    mapFieldName,
} from 'angled-core/ui';
import { WhereBuilderStep } from 'angled-query/src/components/builder/where';
import { SelectBuilderStep } from 'angled-query/src/components/preview/select-multi';
import { fromPredicate, fromNullable, some, none } from 'fp-ts/lib/Option';
import {
    findQuery,
    findShare,
    getAggregate,
    getBuilderStatement,
    getCurrentQuery,
    getSelectField,
    getSelectQuery,
    getSelectUnit,
    getShareDescription,
    getWhereField,
    isWhereStatementListEmpty,
} from '../queries/builder';
import {
    Conj,
    deleteShare,
    fetchQueries,
    fetchShares,
    lookupMetadata,
    Op,
    postExecQuery,
    postMetadata,
    postQuery,
    postShare,
    putQuery,
    Query,
    QuerySelect,
    QueryWhere,
    querySelect,
    Aggregate,
    AggregateFilter,
    aggregate,
    Field,
    AggregateFilterNumberOp,
} from 'angled-query/src/remote/query';
import {
    getApiUrl,
    getUserId,
    getUserIdAstNumber,
    getLang,
    getAliasRaw,
} from 'sdi/app';
import { scopeOption } from 'sdi/lib';
import {
    MessageRecordLang,
    remoteLoading,
    remoteSuccess,
    remoteError,
    IAlias,
} from 'sdi/source';
import {
    resetFormInput,
    setFormInput,
    ensureMultiGeometry,
    prepareWriteDomainForUnit,
} from 'angled-core/events/ui';
import { FieldDescription } from 'angled-query/src/components/statement';
import { identity } from 'fp-ts/lib/function';
import {
    Nullable,
    isNotNullNorUndefined,
    tryStringRange,
    range,
} from 'sdi/util';
import {
    defaultCodeName,
    defaultCodeNameFromSelect,
    getSelectedFields,
} from '../queries/preview';
import { postAlias, putAlias } from '../remote';

const logger = debug('sdi:events/builder');

const withWhereStep = (step: WhereBuilderStep) =>
    fromPredicate<WhereBuilderStep>(s => s === step)(
        query('component/builder/where/step')
    );

const withSelectStep = (step: SelectBuilderStep) =>
    fromPredicate<SelectBuilderStep>(s => s === step)(
        query('component/builder/select/step')
    );

// const procField =
//     ([ft, name, extra]: MappedField): MappedField => {
//         switch (ft) {
//             case 'label':
//             case 'text': return [ft, `${name}__${getLang()}`, extra];
//             default: return [ft, name, extra];
//         }
//     };

// observe('component/builder/where/ui', (ui) => {
//     logger(`observe(builder/ui) ${ui}`);
// });

// observe('component/builder/where', (where) => {
//     logger(`observe(builder/where) ${where}`);
//     // if (where.length === 0) {
//     //     throw 'where is 0-length'
//     // }
// });

// observe('component/ui/form/manip', (ui) => {
//     logger(`observe(manip) ${fromNullable(ui).fold('NULL', u => u.unit as string)}`);
// });

export const setWhereStep = (s: WhereBuilderStep) =>
    assign('component/builder/where/step', s);

export const setWhereField = (ui: InformationUnitName, f: FieldDescription) => {
    logger(`setWhereField ${ui} [${f}]`);
    const o = withWhereStep('Initial').map(() => {
        assign('component/builder/where/ui', ui);
        assign('component/builder/where/field', f);
        setWhereStep('Fielded');
        prepareWriteDomainForUnit(ui);
    });
    if (o.isNone()) {
        logger('setWhereField FAILED');
    }
    return o;
};

export const setSelectField = (fd: FieldDescription) => {
    const o = withSelectStep('Initial').map(() => {
        dispatch('component/builder/select/ui', state =>
            fromNullable(state).fold(
                { unit: fd.unit, index: null },
                ({ index }) => ({ unit: fd.unit, index })
            )
        );
        assign('component/builder/select/field', fd);
        assign('component/builder/select/step', 'Fielded');
        prepareWriteDomainForUnit(fd.unit);
    });
    if (o.isNone()) {
        logger('setSelectField FAILED');
    }
    return o;
};

export const setOp = (op: Op) =>
    withWhereStep('Fielded').map(() => {
        assign('component/builder/where/op', op);
        setWhereStep('Oped');
    });

export const isPossiblyFilterField = (name: string) => (field: MappedField) => {
    if (mapFieldName(field) === name) {
        return false;
    }
    switch (mapFieldType(field)) {
        case 'term':
        case 'year':
        case 'number':
            return true;
        default:
            return false;
    }
};

const aggregateStep = () =>
    scopeOption()
        .let('field', getSelectField())
        .let('lookup', ({ field }) => lookupNamedField(field.unit))
        .map(({ lookup, field }) => {
            if (lookup.multi) {
                const fields = lookup.fields.filter(
                    isPossiblyFilterField(field.name[0])
                );
                return fields.length > 0 ? 'Aggregated' : 'Filtered';
            }
            return 'Filtered';
        })
        .getOrElse('Filtered');

export const setAggregate = (agg: Aggregate) =>
    withSelectStep('Fielded').map(() => {
        assign('component/builder/select/agg', agg);
        assign('component/builder/select/step', aggregateStep());
    });

export const addAggregateFilter = (filter: AggregateFilter) =>
    withSelectStep('Aggregated').map(() => {
        dispatch('component/builder/select/agg', agg => {
            if (agg !== null) {
                return aggregate(agg.name, agg.filter.concat(filter));
            }
            return agg;
        });
        clearAggregateFilterNumberOp();
    });

export const clearAggregateFilter = () =>
    withSelectStep('Aggregated').map(() => {
        dispatch('component/builder/select/agg', agg => {
            if (agg !== null) {
                return aggregate(agg.name, []);
            }
            return agg;
        });
        clearAggregateFilterNumberOp();
    });

export const clearAggregateFilterNumberOp = () =>
    assign('component/builder/select/agg/number/op', null);
export const setAggregateFilterNumberOp = (op: AggregateFilterNumberOp) =>
    assign('component/builder/select/agg/number/op', op);

export const markFiltered = () =>
    assign('component/builder/select/step', 'Filtered');

export const setValue = (v: FieldValueType) =>
    withWhereStep('Oped').map(() => {
        assign('component/builder/where/value', v);
        setWhereStep('Valued');
        if (isWhereStatementListEmpty()) {
            addInitialStatement();
            clearWhereBuilder();
        }
    });

export const setInFlightValue = (v: FieldValueType) =>
    withWhereStep('Oped').map(() =>
        assign('component/builder/where/in-flight-value', v)
    );

export const valueManip = <T extends FieldValueType>(dflt: T) => {
    const getter = (): T =>
        fromNullable(query('component/builder/where/in-flight-value')).fold(
            dflt,
            v => {
                if (typeof v !== typeof dflt) {
                    return dflt as T;
                }
                return v as T;
            }
        );
    const setter = (v: T) =>
        assign('component/builder/where/in-flight-value', v);

    return { getter, setter };
};

export const clearWhereBuilder = () => {
    resetFormInput();
    assign('component/builder/where/ui', null);
    assign('component/builder/where/field', null);
    assign('component/builder/where/op', null);
    assign('component/builder/where/value', null);
    assign('component/builder/where/in-flight-value', null);
    setWhereStep('Initial');
    // resetCurrentQuery(); TODO check if tis current-query should not be sometimes nulled.
};

export const clearWhereBuilderAndStatements = () => {
    clearWhereBuilder();
    assign('component/builder/where', []);
};

export const resetCurrentQuery = () => {
    clearWhereBuilderAndStatements();
    resetSelectBuilder();
    assign('component/builder/current-query', null);
    assign('component/builder/query-name', null);
};

export const addStatement = (conj: Conj) =>
    getBuilderStatement().map(stmt =>
        dispatch('component/builder/where', stmts =>
            stmts.concat([{ tag: 'conj', conj }, stmt])
        )
    );

export const addInitialStatement = () =>
    getBuilderStatement().map(stmt =>
        assign('component/builder/where', [stmt])
    );

export const addSelectQuery = () =>
    scopeOption()
        .let('unit', getSelectUnit())
        .let('fd', getSelectField())
        .let('field', ({ unit, fd }) =>
            lookupNamedField(unit.unit).chain(unit =>
                fromNullable(
                    unit.fields.find(f => mapFieldName(f) === fd.name[0])
                )
            )
        )
        .let('aggregate', getAggregate())
        .let('filters', some([]))
        .map(({ unit, field, aggregate, filters }) => {
            const field2: Field = isRecordField(field)
                ? [mapFieldName(field), getLang()]
                : [mapFieldName(field)];
            const q = querySelect(
                defaultCodeName(unit.unit, field2),
                unit.unit,
                field2,
                aggregate,
                filters
            );
            q.name = defaultCodeNameFromSelect(q);
            dispatch('component/builder/select', qs => qs.concat(q));
            clearSelectBuilder();
        });

const reasonableColumnName = tryStringRange(range(2, 32));

export const initSelectNameForm = (select: string) =>
    assign(
        'component/builder/select/name/form',
        getAliasRaw(select)
            .map<Omit<IAlias, 'id'>>(a => a)
            .getOrElse({
                select,
                replace: {},
            })
    );

export const setSelectNameForm = (lang: MessageRecordLang, name: string) =>
    dispatch('component/builder/select/name/form', state => ({
        ...state,
        select: state.select,
        replace: {
            ...state.replace,
            [lang]: name,
        },
    }));

export const saveSelectAliasName = () => {
    const alias: IAlias | Omit<IAlias, 'id'> = query(
        'component/builder/select/name/form'
    );
    const select = alias.select;
    const prom =
        'id' in alias
            ? putAlias(getApiUrl(`alias/${alias.id}`), alias)
            : postAlias(getApiUrl('alias'), alias);

    prom.then(alias =>
        dispatch('data/alias', state =>
            state.filter(a => a.select !== select).concat(alias)
        )
    ).catch(err => logger(`error updating alias`, err));
};

export const setSelectName = (name: string) =>
    reasonableColumnName(name).map(assignK('component/builder/select/name'));

export const updateSelectQuery = (index: number) =>
    scopeOption()
        .let('oldSelect', fromNullable(getSelectedFields()[index]))
        .let('unit', getSelectUnit())
        .let(
            'field',
            getSelectField().map(f => f.name)
        )
        .let('aggregate', getAggregate())
        .let('filters', some([]))
        .map(({ oldSelect, unit, field, aggregate, filters }) => {
            const q = querySelect(
                oldSelect.name,
                unit.unit,
                field,
                aggregate,
                filters
            );
            dispatch('component/builder/select', qs => {
                qs[index] = q;
                return qs;
            });
            clearSelectBuilder();
        });

// export const updateSelectQueryName = (
//     index: number
// ) => fromNullable(getSelectedFields()[index])
//     .chain(old => getSelectName().map(name => ({ old, name })))
//     .map(({ old, name }) => {
//         const q = querySelect(name, old.unit, old.field, old.aggregate, old.filters);
//         dispatch('component/builder/select', qs => {
//             qs[index] = q;
//             return qs;
//         });
//         clearSelectBuilder();
//     })

export const updateSelectQueryLang = (index: number, lang: MessageRecordLang) =>
    fromNullable(getSelectedFields()[index]).map(old => {
        const newField: Field = [old.field[0], lang];
        const newName = defaultCodeName(old.unit, newField);
        const q = querySelect(
            newName,
            old.unit,
            newField,
            old.aggregate,
            old.filters
        );
        q.name = defaultCodeNameFromSelect(q);
        dispatch('component/builder/select', qs => {
            qs[index] = q;
            return qs;
        });
        clearSelectBuilder();
    });

export const isRecordField = (f: MappedField) =>
    mapFieldType(f) === 'label' || mapFieldType(f) === 'text';

export const withRecordField = fromPredicate<MappedField>(isRecordField);

// export const addSelectQueryForSingle =
//     (u: InformationUnitName) => {
//         lookupNamedField(u).map((unit) => {
//             const select = unit.fields.map<QuerySelect>((f) => {
//                 const select = querySelect(
//                     '',
//                     u,
//                     isRecordField(f) ? [f[1], getLang()] : [f[1]],
//                     null,
//                     [],
//                 )
//                 select.name = defaultFieldName(select);
//                 return select

//             });
//             assign('component/builder/select',
//                 getSelectQuery().concat(select),
//             );
//         });
//     };
export const addSelectQueryForSingle = (
    unitName: InformationUnitName,
    fieldName: string
) =>
    lookupNamedField(unitName)
        .chain(unit =>
            fromNullable(unit.fields.find(f => mapFieldName(f) === fieldName))
        )
        .map(field => {
            const field2: Field = isRecordField(field)
                ? [mapFieldName(field), getLang()]
                : [mapFieldName(field)];
            const select = querySelect(
                defaultCodeName(unitName, field2),
                unitName,
                field2,
                null,
                []
            );

            assign('component/builder/select', getSelectQuery().concat(select));
        });

export const clearSelectBuilder = () => {
    resetFormInput();
    assign('component/builder/select/name', null);
    assign('component/builder/select/ui', null);
    assign('component/builder/select/field', null);
    assign('component/builder/select/agg', null);
    assign('component/builder/select/step', 'Initial');
};

export const resetSelectBuilder = () => {
    // resetFormInput();
    // assign('component/builder/select/field', null);
    // assign('component/builder/select/agg', null);
    // assign('component/builder/select/step', 'Initial');
    clearSelectBuilder();
    assign('component/builder/select', []);
};

// const addNameToSelectQuery =
//     (q: QuerySelect[]) => q.concat(
//         ({
//             tag: 'select',
//             unit: 'name',
//             field: ['name', getLang()],
//             aggregate: 'concat', // TODO should be empty
//             filters: [],
//         }),
//     );

const checkWhereStatementList = (s: QueryWhere[]) =>
    s.length > 0 ? some(s) : none;

export const saveNewQuery = (andThen?: (q: Query) => void) =>
    scopeOption()
        .let(
            'user',
            getUserId().map(uid => parseInt(uid, 10))
        )
        .let(
            'where',
            some(query('component/builder/where')).chain(
                checkWhereStatementList
            )
        )
        .let('name', fromNullable(query('component/builder/query-name')))
        .let('select', some(query('component/builder/select').map(identity)))
        .map(({ user, where, name, select }) =>
            postQuery(getApiUrl('geodata/angled/q/query/'), {
                user,
                name,
                statements: { where, select },
            })
        )
        .map(prm =>
            prm.then(newQuery => {
                dispatch('data/queries', qs =>
                    qs.filter(q => q.id !== newQuery.id).concat(newQuery)
                );
                assign('component/builder/current-query', newQuery.id);
                selectQuery(newQuery.id);
                if (andThen) andThen(newQuery);
            })
        );

export const updateQuery = () =>
    getCurrentQuery()
        .map(q =>
            putQuery(getApiUrl(`geodata/angled/q/query/${q.id}/`), {
                ...q,
                statements: {
                    where: query('component/builder/where') as QueryWhere[],
                    select: query('component/builder/select') as QuerySelect[],
                },
                name: fromNullable(
                    query('component/builder/query-name')
                ).getOrElse(q.name),
            })
        )
        .map(prm =>
            prm.then(query =>
                dispatch('data/queries', qs =>
                    qs.filter(q => q.id !== query.id).concat(query)
                )
            )
        )
        .map(clearSelectBuilder);

// export const saveQuery =
//     () => fromNullable(query('component/builder/current-query'))
//         .foldL(saveNewQuery, qid => updateWhereQuery(qid))

export const loadQueries = () =>
    fetchQueries(getApiUrl('geodata/angled/q/query/')).then(queries =>
        assign('data/queries', queries)
    );

export const loadShares = () =>
    fetchShares(getApiUrl('geodata/angled/q/share/')).then(shares =>
        assign('data/shares', shares)
    );

export const selectQuery = (id: number) =>
    findQuery(id).map(q => {
        clearWhereBuilder();
        assign('component/builder/where', q.statements.where);
        assign('component/builder/select', q.statements.select);
        assign('component/builder/current-query', id);
        assign('component/builder/query-name', q.name);
        findShare(id).map(s =>
            assign('component/builder/query-share-desciption', s.description)
        );
        checkMetadataPublished(id);
    });

export const setQueryName = (l: MessageRecordLang) => (s: string) =>
    dispatch('component/builder/query-name', r =>
        r === null ? { [l]: s } : { ...r, [l]: s }
    );

export const setShareDescription = (l: MessageRecordLang) => (s: string) =>
    dispatch('component/builder/query-share-desciption', r =>
        r === null ? { [l]: s } : { ...r, [l]: s }
    );

export const execQuery = () =>
    scopeOption()
        .let('user', getUserIdAstNumber())
        .let('where', some(query('component/builder/where') as QueryWhere[])) // we cast to drop readonly
        .let('select', some(query('component/builder/select') as QuerySelect[]))
        .map(({ user, where, select }) => {
            logger('exec ', where);
            assign('data/results', remoteLoading);
            return postExecQuery(getApiUrl('geodata/angled/q/query/exec/'), {
                user,
                statements: { where, select },
            })
                .then(ps => assign('data/results', remoteSuccess(ps)))
                .catch(err => assign('data/results', remoteError(`${err}`)));
        });

export const selectFeature = (layerId: string, featureId: string | number) => {
    assign('component/builder/geometry/select', { layerId, featureId });
    const layer = query('data/layer')[layerId];
    if (layer) {
        const f = layer.features.find(f => f.id === featureId);
        if (f) {
            logger(`geom ${f.geometry}`);
            getWhereField().map(({ unit, field }) =>
                setFormInput(
                    unit,
                    field.name[0]
                )(ensureMultiGeometry(f.geometry))
            );
        }
    }
};

export const clearSelectedFeature = () =>
    assign('component/builder/geometry/select', null);

export const setSelectUnit = (
    unit: Nullable<InformationUnitName>,
    index: Nullable<number>
) =>
    assign(
        'component/builder/select/ui',
        isNotNullNorUndefined(unit) ? { unit, index } : null
    );

export const clearAll = () => {
    assign('component/builder/current-query', null);
    clearSelectBuilder();
    clearSelectedFeature();
    clearWhereBuilderAndStatements();
};

export const shareQuery = (qid: number) =>
    getShareDescription()
        .map(description =>
            postShare(getApiUrl('geodata/angled/q/share/'), {
                query: qid,
                description,
            })
        )
        .map(prm =>
            prm.then(share => {
                dispatch('data/shares', shares => shares.concat(share));
            })
        );

export const unShareQuery = (qid: number) =>
    findShare(qid).map(share =>
        deleteShare(getApiUrl(`geodata/angled/q/share/${share.id}/`)).then(
            () => {
                dispatch('data/shares', xs =>
                    xs.filter(s => s.id !== share.id)
                );
                assign('component/builder/query-share-desciption', null);
            }
        )
    );

const checkMetadataPublished = (qid: number) => {
    assign('component/builder/metadata/published', []);

    const checkGeom = (gu: GeometryUnit) => {
        const uri = encodeURIComponent(`angled://${gu}/${qid}`);
        const lookupURL = getApiUrl(`metadatas/lookup?exact=1&q=${uri}`);
        lookupMetadata(lookupURL)
            .then(() =>
                dispatch('component/builder/metadata/published', ps =>
                    ps.filter(p => p !== gu).concat(gu)
                )
            )
            .catch(() => void 0);
    };

    const gus: GeometryUnit[] = ['point', 'line', 'polygon'];
    gus.forEach(gu => checkGeom(gu));
};

export const saveMetadata = (qid: number, gu: GeometryUnit) =>
    scopeOption()
        .let('q', findQuery(qid))
        .let('share', findShare(qid))
        .let('description', getShareDescription())
        .map(({ q, description }) =>
            postMetadata(getApiUrl('metadatas'), {
                uniqueResourceIdentifier: `angled://${gu}/${qid}`,
                resourceIdentifier: `angled://${gu}/${qid}`,
                resourceTitle: q.name,
                resourceAbstract: description,
                geometryType: geometryTypeForUnit(gu),
                maintenanceFrequency: 'unknown',
            })
        )
        .map(prm =>
            prm.then(() => {
                dispatch('component/builder/metadata/published', ps =>
                    ps.filter(p => p !== gu).concat(gu)
                );
            })
        );

export const setInfoFilterPattern = (pattern: string) =>
    assign('component/builder/info/filter-pattern', pattern);

logger('loaded');
