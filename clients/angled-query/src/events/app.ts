import { assign, dispatch } from 'sdi/shape';
import { loadAllRefs } from 'angled-core/events/ref';
import { loadDomains, loadTerms } from 'angled-core/events/universe';

import { AppLayout } from '../shape';
import { loadQueries, loadShares } from './builder';
import { getApiUrl } from 'sdi/app';
import { Setoid } from 'fp-ts/lib/Setoid';
import {
    Inspire,
    getMessageRecord,
    ILayerInfo,
} from 'sdi/source';
import { uniq } from 'sdi/util';
import { addLayer, removeLayerAll } from 'sdi/map';
import { right } from 'fp-ts/lib/Either';
import { some } from 'fp-ts/lib/Option';
import { querySelectMap } from '../components/builder/value/geometry';
import { fetchAllDatasetMetadata, fetchLayer } from 'angled-core/remote/map';
import { activity } from 'sdi/activity';
import { mapTemplate } from "angled-core/events/map";

export const activityLogger = activity('angled-query');

export const setLayout = (l: AppLayout) => assign('app/layout', l);

export const unfold = (name: string) =>
    dispatch('component/foldable', folds => ({ ...folds, [name]: false }));

export const fold = (name: string) =>
    dispatch('component/foldable', folds => ({ ...folds, [name]: true }));

const inspireS: Setoid<Inspire> = {
    equals(a, b) {
        return a.id === b.id;
    },
};

const uniqInspire = uniq(inspireS);

export const loadAllDatasetMetadata = (done?: () => void) =>
    fetchAllDatasetMetadata(getApiUrl('metadatas'))(
        frame => {
            dispatch('data/datasetMetadata', state =>
                uniqInspire(state.concat(frame.results))
            );
            // dispatch('component/table/layers',
            //     ts => ({ ...ts, loaded: 'loading' as LoadingStatus }));
            // dispatch('component/splash', () => Math.floor(frame.page * 100 / frame.total));
        },
        () => {
            // dispatch('component/table/layers',
            //     ts => ({ ...ts, loaded: 'done' as LoadingStatus }));
            if (done) {
                done();
            }
        }
    );

export const loadApplicationData = () =>
    loadQueries()
        .then(loadShares)
        .then(loadDomains)
        .then(loadTerms)
        .then(loadAllRefs)
        .then(() => loadAllDatasetMetadata());

// map select things

const selectLayerInfo = (md: Inspire): ILayerInfo => ({
    id: md.uniqueResourceIdentifier,
    visible: true,
    group: null,
    legend: null,
    metadataId: md.id,
    featureViewOptions: { type: 'default' },
    style: {
        kind: 'polygon-simple',
        fillColor: 'blue',
        strokeColor: 'white',
        strokeWidth: 1,
    },
    visibleLegend: true,
    opacitySelector: false,
});

export const loadLayer = (md: Inspire) => {
    removeLayerAll(querySelectMap);
    fetchLayer(md.uniqueResourceIdentifier).then(layer => {
        dispatch('data/layer', state => ({
            ...state,
            [md.uniqueResourceIdentifier]: layer,
        }));
        addLayer(
            querySelectMap,
            () =>
                some({
                    metadata: md,
                    name: getMessageRecord(md.resourceTitle),
                    info: selectLayerInfo(md),
                }),
            () => right(some(layer))
        );
    });
};

export const loadMaps = () =>
    dispatch('data/maps', state => state.concat([mapTemplate()]));
