import { DIV, H2 } from 'sdi/components/elements';
import { makeLabel } from 'sdi/components/button';
import tr from 'sdi/locale';
import {
    setSelectUnit,
    clearSelectBuilder,
    addSelectQuery,
    execQuery,
    updateSelectQuery,
} from '../../events/builder';
import renderSelectForm from '../preview/select-multi';
import { stringToParagraphs } from 'sdi/util';
import { ModalRender } from 'sdi/components/modal';
import { makeLabelAndIcon } from 'sdi/components/button';
import { getSelectUiIndex } from 'angled-query/src/queries/preview';
import { withSelectBuilderFiltered } from 'angled-query/src/queries/builder';

const closeModalButton = makeLabel('cancel', 2, () => tr.core('cancel'));
const addStatementBtn = makeLabelAndIcon('add', 1, 'chevron-right', () =>
    tr.angled('addStatement')
);
const updateStatementBtn = makeLabelAndIcon('add', 1, 'chevron-right', () =>
    tr.angled('updateStatement')
);

const renderAddStatement = (closeSelect: () => void) =>
    withSelectBuilderFiltered().map(() =>
        getSelectUiIndex().fold(
            addStatementBtn(() => {
                addSelectQuery();
                closeSelect();
                execQuery();
            }),
            index =>
                updateStatementBtn(() => {
                    updateSelectQuery(index);
                    closeSelect();
                    execQuery();
                })
        )
    );

export const render: ModalRender = {
    sizeHint: 'medium',
    header: () => H2({}, tr.angled('aggregatorTitle')),
    footer: close =>
        DIV(
            { className: 'modal__footer__inner' },
            closeModalButton(() => {
                close();
                setSelectUnit(null, null);
                clearSelectBuilder();
            }),
            renderAddStatement(close)
        ),
    body: () => [
        DIV(
            { className: 'helptext' },
            stringToParagraphs(tr.angled('agregationHelptext'))
        ),
        renderSelectForm(),
    ],
};
