import { DIV, H2, SPAN } from 'sdi/components/elements';
import { makeLabel } from 'sdi/components/button';
import tr, { optRec } from 'sdi/locale';
import { getSelectNameForm } from '../../queries/builder';
import { saveSelectAliasName, setSelectNameForm } from '../../events/builder';
import { ModalRender } from 'sdi/components/modal';
import { inputText, options } from 'sdi/components/input';
import { MessageRecordLang } from 'sdi/source';
import { getSelectQueryName } from 'angled-query/src/queries/preview';

const closeModalButton = makeLabel('cancel', 2, () => tr.core('cancel'));
const saveButton = makeLabel('save', 1, () => tr.core('save'));

const getName = () => getSelectNameForm().replace;
const getIndex = () => getSelectNameForm().select;
const setName = (lang: MessageRecordLang, name: string) =>
    setSelectNameForm(lang, name);

const renderForm = (lang: MessageRecordLang) =>
    DIV(
        'form-wrapper',
        inputText(
            options(
                `input-column-name-${lang}`,
                () => optRec(getName())(lang).getOrElse(''),
                name => setName(lang, name)
            )
        )
    );

const body = () => [
    DIV(
        'content-modal_column-name',
        DIV(
            'code-name',
            SPAN('label', `${tr.angled('codeName')} : `),
            SPAN('', getIndex())
        ),
        DIV(
            'generated-name',
            SPAN('label', `${tr.angled('generatedName')} : `),
            SPAN('', getSelectQueryName(getIndex()))
        ),
        DIV(
            'alias-wrapper',
            DIV(
                '',
                DIV('label-lang', tr.angled('aliasLabelFR')),
                renderForm('fr')
            ),
            DIV(
                '',
                DIV('label-lang', tr.angled('aliasLabelNL')),
                renderForm('nl')
            )
        ),
        DIV({ className: 'helptext' }, tr.angled('renameColumnHelptext'))
    ),
];

export const render: ModalRender = {
    sizeHint: 'medium',
    header: () => H2({}, tr.angled('renameColumn')),
    footer: close =>
        DIV(
            { className: 'modal__footer__inner' },
            closeModalButton(close),
            saveButton(() => {
                saveSelectAliasName();
                close();
            })
        ),
    body,
};
