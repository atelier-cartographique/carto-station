import { DIV, NODISPLAY, A, SPAN, H2 } from 'sdi/components/elements';
import {
    mapIsVisible,
    getSelectedRow,
    getFeatureForSelected,
    getProjectedSelectedData,
    ProjectedData,
    findTag,
} from 'angled-query/src/queries/preview';
import { pmap, fst, snd } from 'sdi/lib';
import { getRootUrl } from 'sdi/app';
import { ensureArray, uniqNumber } from 'sdi/util';
import { Unit } from 'angled-core/ui';
import { flatten } from 'fp-ts/lib/Array';
import tr, { fromRecord, formatDate } from 'sdi/locale';

const renderProjectId = () =>
    getSelectedRow().map(pid =>
        H2(
            { className: 'project-id' },
            `${tr.angled('projectIdLight')} : ${pid}`
        )
    );

const renderProjectLink = () =>
    getSelectedRow().map(pid =>
        A(
            {
                className: 'link',
                target: '_blank',
                href: getRootUrl(`angled-project/form/${pid}`),
            },
            tr.angled('goToProject')
        )
    );

const renderTags = (u: Unit | Unit[]) =>
    uniqNumber(flatten(ensureArray(u).map(u => u.tags.map(t => t.tag)))).map(
        tagId =>
            DIV(
                { className: 'unit__tag', key: `tag-${tagId}` },
                findTag(tagId).map(tag => fromRecord(tag.label))
            )
    );

const renderDate = (u: Unit | Unit[]) => {
    const ts = ensureArray(u).reduce(
        (acc, u) => Math.max(acc, u.created_at),
        0
    );
    const d = new Date(ts);
    return SPAN({ className: '_date-inner' }, formatDate(d));
};

const renderData = (data: ProjectedData) =>
    data.map(
        pmap((a, unit) => {
            const key = fst(a);
            const value = snd(a);
            return DIV(
                { key, className: 'unit' },
                DIV({ className: 'unit__key' }, key),
                DIV({ className: 'unit__value' }, value),
                DIV(
                    { className: 'unit__date' },
                    tr.angled('lastUpdate'),
                    renderDate(unit)
                ),
                DIV({ className: 'unit__tags' }, renderTags(unit))
            );
        })
    );

const renderGeometryIndicator = () =>
    mapIsVisible()
        ? getSelectedRow()
              .chain(getFeatureForSelected)
              .fold(DIV({ className: 'helptext' }, tr.angled('noGeom')), () =>
                  NODISPLAY()
              )
        : NODISPLAY();

export const renderRowView = () =>
    getSelectedRow().map(() =>
        DIV(
            { className: 'row--view' },
            renderProjectId(),
            renderProjectLink(),
            renderGeometryIndicator(),
            getProjectedSelectedData().map(renderData)
        )
    );

export default renderRowView;
