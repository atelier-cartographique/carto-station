import { DIV, H2, NodeOrOptional, NODISPLAY } from 'sdi/components/elements';
import {
    FieldType,
    lookupNamedField,
    mapFieldType,
    MappedField,
    InformationUnitName,
    mapFieldName,
    mapValueField,
} from 'angled-core/ui';
import {
    isPossiblyFilterField,
    markFiltered,
    setAggregate,
    addAggregateFilter,
    setAggregateFilterNumberOp,
    clearAggregateFilter,
} from 'angled-query/src/events/builder';
import {
    getAggregate,
    getAggregateNumberOp,
    getSelectField,
    getSelectUnit,
} from 'angled-query/src/queries/builder';
import {
    AggregateName,
    Aggregate,
    aggregate,
    aggFilterTerm,
    aggFilterNumber,
    AggregateFilterNumberOp,
} from 'angled-query/src/remote/query';
import tr from 'sdi/locale';
import { scopeOption } from 'sdi/lib';
import { getFormInput } from 'angled-core/queries/ui';
import renderTermWrite from 'angled-core/ui/write/term';
import renderYearWrite from 'angled-core/ui/write/year';
import renderNumberWrite from 'angled-core/ui/write/number';
import { makeLabelAndIcon } from 'sdi/components/button';
import { Option, none, some } from 'fp-ts/lib/Option';
import {
    aggregateCodeName,
    defaultCodeName,
} from 'angled-query/src/queries/preview';
import { FieldDescription } from '../statement';

const addFilterButton = makeLabelAndIcon('confirm', 1, 'plus', () =>
    tr.core('add')
);
// const skipFilterButton = makeLabelAndIcon('next', 1, 'chevron-right', () => tr.core('skipStep'));
const confirmFilterButton = makeLabelAndIcon('confirm', 1, 'check', () =>
    tr.core('confirm')
);
const clearFilterButton = makeLabelAndIcon('clear', 2, 'times', () =>
    tr.core('reset')
);
const selectAggregateButton = makeLabelAndIcon('select', 1, 'check', () =>
    tr.core('add')
);

// TODO applicableAgg could be in statement/index

const aggregators = {
    sum: () => tr.angled('aggregate/sum'),
    mean: () => tr.angled('aggregate/mean'),
    concat: () => tr.angled('aggregate/concat'),
    min: () => tr.angled('aggregate/min'),
    max: () => tr.angled('aggregate/max'),
};

const applicableAgg = (ft: FieldType): AggregateName[] => {
    switch (ft) {
        case 'boolean':
            return ['concat'];
        case 'date':
            return ['concat'];
        case 'decimal':
            return ['sum', 'mean', 'min', 'max'];
        case 'fk':
            return ['concat'];
        case 'geometry':
            return ['concat'];
        case 'label':
            return ['concat'];
        case 'month':
            return ['concat', 'min', 'max'];
        case 'number':
            return ['sum', 'mean', 'min', 'max'];
        case 'raw_text':
            return ['concat'];
        case 'term':
            return ['concat'];
        case 'text':
            return ['concat'];
        case 'varchar':
            return ['concat'];
        case 'year':
            return ['concat', 'min', 'max'];
    }
};

const renderAgg = (name: AggregateName) =>
    DIV(
        {
            className: 'aggregator',
            key: `aggregator-${name}-key`,
        },
        DIV('aggregator__label', aggregators[name]()),
        selectAggregateButton(() => setAggregate(aggregate(name)))
    );

const makeNumberValue = mapValueField<Option<number>>({
    year: n => some(n),
    term: n => some(n),
    number: n => some(n),
    text: () => none,
    label: () => none,
    raw_text: () => none,
    boolean: () => none,
    decimal: () => none,
    date: () => none,
    geometry: () => none,
    month: () => none,
    fk: () => none,
    varchar: () => none,
});

const getCurrentValue = (unit: InformationUnitName, fieldName: string) =>
    getFormInput(unit, fieldName).chain(makeNumberValue);

const renderFilterTermItem = (
    unit: InformationUnitName,
    _agg: Aggregate,
    field: MappedField
) =>
    DIV(
        {
            className: 'render-filter__item render-filter__item--term',
            key: `renderFilterItem-${unit}-${mapFieldName(field)}`,
        },
        renderTermWrite(unit, mapFieldName(field)),
        getCurrentValue(unit, mapFieldName(field)).map(termId =>
            addFilterButton(() =>
                addAggregateFilter(aggFilterTerm(mapFieldName(field), termId))
            )
        )
    );

const renderOp = (
    op: AggregateFilterNumberOp,
    label: string,
    selected: AggregateFilterNumberOp
) => {
    if (selected === op) {
        return DIV(
            {
                className: 'btn btn-2 number-op selected',
            },
            label
        );
    }
    return DIV(
        {
            className: 'btn btn-2 number-op',
            onClick: () => setAggregateFilterNumberOp(op),
        },
        label
    );
};

const numberOps = () => {
    const selectedOp = getAggregateNumberOp().getOrElse('eq');
    return DIV(
        'number-ops-list__wrapper',
        DIV('field__key ops-list__title', tr.angled('operatorListTitle')),
        DIV(
            'ops-list',
            renderOp('eq', '=', selectedOp),
            renderOp('lt', '<', selectedOp),
            renderOp('lte', '≤', selectedOp),
            renderOp('gte', '≥', selectedOp),
            renderOp('gt', '>', selectedOp)
        )
    );
};

const renderFilterNumberItem = (
    unit: InformationUnitName,
    _agg: Aggregate,
    field: MappedField
) =>
    DIV(
        {
            className: 'render-filter__item  render-filter__item--number',
            key: `renderFilterItem-${unit}-${mapFieldName(field)}`,
        },
        renderNumberWrite(unit, mapFieldName(field)),
        numberOps(),
        getCurrentValue(unit, mapFieldName(field)).map(value =>
            addFilterButton(() =>
                addAggregateFilter(
                    aggFilterNumber(
                        mapFieldName(field),
                        value,
                        getAggregateNumberOp().getOrElse('eq')
                    )
                )
            )
        )
    );

const renderFilterYearItem = (
    unit: InformationUnitName,
    _agg: Aggregate,
    field: MappedField
) =>
    DIV(
        {
            className: 'render-filter__item  render-filter__item--number',
            key: `renderFilterItem-${unit}-${mapFieldName(field)}`,
        },
        renderYearWrite(unit, mapFieldName(field)),
        numberOps(),
        getCurrentValue(unit, mapFieldName(field)).map(value =>
            addFilterButton(() =>
                addAggregateFilter(
                    aggFilterNumber(
                        mapFieldName(field),
                        value,
                        getAggregateNumberOp().getOrElse('eq')
                    )
                )
            )
        )
    );

const renderFilterItem = (
    unit: InformationUnitName,
    agg: Aggregate,
    field: MappedField
) => {
    switch (mapFieldType(field)) {
        case 'term':
            return renderFilterTermItem(unit, agg, field);
        case 'year':
            return renderFilterYearItem(unit, agg, field);
        case 'number':
            return renderFilterNumberItem(unit, agg, field);
        default:
            return NODISPLAY();
    }
};

const renderAggFilterButtons = () =>
    DIV(
        'button-wrapper',
        // skipFilterButton(() => {
        //     markFiltered();
        //     clearAggregateFilter();
        // }),
        clearFilterButton(clearAggregateFilter),
        confirmFilterButton(markFiltered)
    );

export const renderAggFilter = () =>
    scopeOption()
        .let('agg', getAggregate())
        .let('field', getSelectField())
        .let('lookup', ({ field }) => lookupNamedField(field.unit))
        .map(({ lookup, agg, field }) => {
            if (lookup.multi) {
                return DIV(
                    'render-filter__wrapper',
                    H2('', tr.angled('aggregatorChooseFilter')),
                    DIV(
                        'render-filter__items',
                        lookup.fields
                            .filter(isPossiblyFilterField(field.name[0]))
                            .map(f => renderFilterItem(field.unit, agg, f))
                    ),
                    renderAggFilterButtons()
                );
            }
            return DIV('helptext', tr.angled('aggregatorNoFilter'));
        });

export const renderAggSelect = () =>
    DIV(
        { className: 'query-tool' },
        DIV(
            { className: 'query-tool--aggregator-list' },
            getSelectField().map(field =>
                DIV(
                    {},
                    H2('', tr.angled('aggregatorSelectType')),
                    DIV(
                        'aggregator__wrapper',
                        ...applicableAgg(field.type).map(renderAgg)
                    )
                )
            )
        )
    );

const renderPreviewNone = (
    unit: InformationUnitName,
    field: FieldDescription
) =>
    DIV(
        { className: 'query-preview--none' },
        DIV('query-preview--field', defaultCodeName(unit, field.name))
    );

const renderPreviewSome = (
    unit: InformationUnitName,
    field: FieldDescription,
    agg: Aggregate
) =>
    DIV(
        { className: 'query-preview--aggregator' },
        DIV('query-preview--field', defaultCodeName(unit, field.name)),
        DIV('query-preview--aggregate', aggregateCodeName(agg))
        // defaultFieldName(unit, field.name[0]),
        // aggregateDisplayName(unit)(agg)
    );

export const renderAggPreview = () =>
    scopeOption()
        .let('unit', getSelectUnit())
        .let('field', getSelectField())
        .map(({ unit, field }) =>
            getAggregate().fold<NodeOrOptional>(
                renderPreviewNone(unit.unit, field),
                agg => renderPreviewSome(unit.unit, field, agg)
            )
        );
