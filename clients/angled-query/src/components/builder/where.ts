import { DIV } from 'sdi/components/elements';
import { renderUnitSelect, renderUnitPreview } from './ui';
import { renderValueSelect, renderValuePreview } from './value';
import { renderOpSelect, renderOpPreview } from './op';
import { getWhereBuilderStep } from 'angled-query/src/queries/builder';
import { ReactNode } from 'react';
import {
    addStatement,
    clearWhereBuilder,
} from 'angled-query/src/events/builder';
import { Conj } from 'angled-query/src/remote/query';
import { tr } from 'sdi/locale';
import statement from '../statement';
import { condOn } from 'sdi/lib';

// see https://docs.djangoproject.com/en/2.1/ref/models/querysets/#id4
// At this point, it might be useful to develop, but it's
// way too low level, the request backend should offer simpler
// APIs, adjusted to use cases.

export type WhereBuilderStep = 'Initial' | 'Fielded' | 'Oped' | 'Valued';

const wrapBuilder = (...nodes: ReactNode[]) =>
    DIV(
        { className: 'query-builder' },
        DIV({ className: 'query-builder__wrapper' }, ...nodes)
    );

const renderSteps = () =>
    DIV(
        { className: 'query-step' }
        // renderUnitStep(),
        // renderOpStep(),
        // renderValueStep(),
    );

const renderPreview = () =>
    DIV(
        { className: 'query-preview' },
        renderUnitPreview(),
        renderOpPreview(),
        renderValuePreview()
    );

const handleAdd = (conj: Conj) => () => {
    addStatement(conj);
    clearWhereBuilder();
};

const renderAddStatement = () =>
    DIV(
        { className: 'query-tool' },
        DIV(
            {
                className: 'query-tool--add-statement',
                onClick: handleAdd('AND'),
            },
            tr.angled('addWithAND')
        ),
        DIV(
            {
                className: 'query-tool--add-statement',
                onClick: handleAdd('OR'),
            },
            tr.angled('addWithOR')
        )
    );

const renderInitial = () =>
    wrapBuilder(renderSteps(), renderPreview(), renderUnitSelect());

const renderFielded = () =>
    wrapBuilder(renderSteps(), renderPreview(), renderOpSelect());

const renderOped = () =>
    wrapBuilder(renderSteps(), renderPreview(), renderValueSelect());

const renderValued = () =>
    wrapBuilder(renderSteps(), renderPreview(), renderAddStatement());

const withStatements = (node: ReactNode) =>
    DIV(
        { className: 'statement-builder' },

        DIV({ className: 'statement-builder__content' }, node, statement())
    );

export default () =>
    condOn(
        getWhereBuilderStep(),
        [step => step === 'Initial', withStatements(renderInitial())],
        [step => step === 'Fielded', withStatements(renderFielded())],
        [step => step === 'Oped', withStatements(renderOped())],
        [step => step === 'Valued', withStatements(renderValued())]
    );

// Because of TS 3.7 complaining about the switch on `getWhereBuilderStep()`
// I took the occasion to get fancy and try out the new `cond` toy
// Feel free to get back to more conventional style at some point - pm
// {
//     switch (getWhereBuilderStep()) {
//         case 'Initial': return withStatements(renderInitial());
//         case 'Fielded': return withStatements(renderFielded());
//         case 'Oped': return withStatements(renderOped());
//         case 'Valued': return withStatements(renderValued());
//     }
// };
