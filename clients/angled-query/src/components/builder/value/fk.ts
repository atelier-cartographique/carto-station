import * as debug from 'debug';
import { Setoid } from 'fp-ts/lib/Setoid';

import { DIV, NODISPLAY } from 'sdi/components/elements';
import { renderSelect } from 'sdi/components/input';
import { fromRecord } from 'sdi/locale';
import { Contact, Site, FundingOrg, Team } from 'angled-core/ref';
import { IUser } from 'sdi/source';
import {
    InformationUnitName,
    FieldType,
    makeValueMapper,
} from 'angled-core/ui';
import {
    getContactList,
    getSiteList,
    getFundingOrgList,
    getUserList,
    getTeamList,
    findContact,
    findSite,
    findFundingOrg,
    findUser,
    findTeam,
} from 'angled-core/queries/ref';
import { getFormInput } from 'angled-core/queries/ui';
import { setFormInput } from 'angled-core/events/ui';

const logger = debug('app/sdi:angled-core/ui/write/fk');

const renderName = (fieldName: string) =>
    DIV({ className: 'field__key field--write' }, fieldName);

const fieldMapper = makeValueMapper('fk', (a: number) => a, -1);

const getCurrentFk = (unit: InformationUnitName, fieldName: string) =>
    getFormInput(unit, fieldName).map(fieldMapper);

const setoidContact: Setoid<Contact> = {
    equals: (a: Contact, b: Contact) => a.id === b.id,
};

const setoidSite: Setoid<Site> = {
    equals: (a: Site, b: Site) => a.id === b.id,
};

const setoidFunding: Setoid<FundingOrg> = {
    equals: (a: FundingOrg, b: FundingOrg) => a.id === b.id,
};

const setoidUser: Setoid<IUser> = {
    equals: (a: IUser, b: IUser) => a.id === b.id,
};

const setoidTeam: Setoid<Team> = {
    equals: (a: Team, b: Team) => a.id === b.id,
};

// const renderContact =
//     (c: Contact) =>
//         DIV({
//             className: '_fk__item',
//             key: `contact-input-${c.id}`,
//         }, c.name,
//         );

// const renderSite =
//     (s: Site) =>
//         DIV({
//             className: '_fk__item',
//             key: `site-input-${s.id}`,
//         }, fromRecord(s.name),
//         );

// const renderFunding =
//     (f: FundingOrg) =>
//         DIV({
//             className: '_fk__item',
//             key: `funding-input-${f.id}`,
//         }, f.name,
//         );

// const renderUser =
//     (u: IUser) =>
//         DIV({
//             className: '_fk__item',
//             key: `user-input-${u.id}`,
//         }, u.name,
//         );

const renderTeam = (t: Team) =>
    t.members.map(m => findContact(m.member).fold('', c => c.name)).join(', ');

const selectContactRenderer = (unit: InformationUnitName, fieldName: string) =>
    renderSelect(
        'select-contact',
        c => c.name,
        d => setFormInput(unit, fieldName)(d.id),
        setoidContact
    );

const selectSiteRenderer = (unit: InformationUnitName, fieldName: string) =>
    renderSelect(
        'select-site',
        s => fromRecord(s.name),
        d => setFormInput(unit, fieldName)(d.id),
        setoidSite
    );

const selectFundingRenderer = (unit: InformationUnitName, fieldName: string) =>
    renderSelect(
        'select-funding',
        t => t.name,
        d => setFormInput(unit, fieldName)(d.id),
        setoidFunding
    );

const selectTeamRenderer = (unit: InformationUnitName, fieldName: string) =>
    renderSelect(
        'select-team',
        renderTeam,
        d => setFormInput(unit, fieldName)(d.id),
        setoidTeam
    );

const selectUserRenderer = (unit: InformationUnitName, fieldName: string) =>
    renderSelect(
        'select-user',
        u => u.name,
        d => setFormInput(unit, fieldName)(parseInt(d.id, 10)),
        setoidUser
    );

const renderFkList = (unit: InformationUnitName, fieldName: string) => {
    switch (unit) {
        case 'funding':
            return selectFundingRenderer(unit, fieldName)(
                getFundingOrgList(),
                getCurrentFk(unit, fieldName).chain(findFundingOrg)
            );
        case 'site':
            return selectSiteRenderer(unit, fieldName)(
                getSiteList(),
                getCurrentFk(unit, fieldName).chain(findSite)
            );
        case 'bidders':
        case 'tender_winner':
            return selectTeamRenderer(unit, fieldName)(
                getTeamList(),
                getCurrentFk(unit, fieldName).chain(findTeam)
            );
        case 'actor':
            return selectContactRenderer(unit, fieldName)(
                getContactList(),
                getCurrentFk(unit, fieldName).chain(findContact)
            );
        case 'manager':
            return selectUserRenderer(unit, fieldName)(
                getUserList(),
                getCurrentFk(unit, fieldName).chain(findUser)
            );
        default:
            return NODISPLAY();
    }
};

export const render = (
    unit: InformationUnitName,
    _ft: FieldType,
    fieldName: string
) => {
    return DIV(
        { className: `field field--${fieldName} field--write` },
        renderName(fieldName),
        renderFkList(unit, fieldName)
    );
};

export default render;

logger('Loaded !');
