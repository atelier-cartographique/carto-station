import * as debug from 'debug';

import { DIV } from 'sdi/components/elements';

import {
    viewEvents,
    startEditGeometry,
    clearMetadataForGeometryWrite,
} from 'angled-core/events/map';

import tr from 'sdi/locale';

import { InformationUnitName, FieldType } from 'angled-core/ui';
import {
    clearSelectedGeometry,
    ensureMultiGeometry,
    setWriteUnit,
    resetFormInput,
} from 'angled-core/events/ui';
import { getSelectedFeature } from 'angled-core/queries/ui';
import { makeLabel } from 'angled-core/components/buttons';
import { defaultMapEvent } from 'angled-core/shape/map';
import { fieldDisplayName } from 'angled-core/queries/app';
import { renderValue } from 'angled-core/ui/read/geometry';
import { openModalGeometry } from 'angled-query/src/events/modal';

const logger = debug('sdi:write/geometry');

export const querySelectMap = 'project-select-map';

const openModalButton = makeLabel('add', 2, () =>
    tr.angled('startGeomDrawing')
);

const renderName = (unitName: InformationUnitName, fieldName: string) =>
    DIV(
        { className: 'field__key field--write' },
        fieldDisplayName(unitName, fieldName)
    );

export const render = (
    unit: InformationUnitName,
    _kind: FieldType,
    fieldName: string
) => {
    const button = openModalButton(() => {
        setWriteUnit('polygon', fieldName);
        resetFormInput();
        openModalGeometry();
        startEditGeometry('MultiPolygon');
        clearSelectedGeometry();
        clearMetadataForGeometryWrite(unit);
        viewEvents.updateMapView(defaultMapEvent());
    });

    const modalTrigger = getSelectedFeature().fold(button, feature =>
        DIV(
            {},
            renderValue(fieldName, ensureMultiGeometry(feature.geometry)),
            button
        )
    );

    return DIV(
        { className: `field field--${fieldName} field--write` },
        renderName(unit, fieldName),
        DIV({ className: 'new-data new-data--geom' }, modalTrigger)
    );
};

export default render;

logger('loaded');
