import { DIV, SPAN } from 'sdi/components/elements';
import { setOp } from 'angled-query/src/events/builder';
import { getOp, getWhereField } from 'angled-query/src/queries/builder';
import { Operations, applicableOps } from '../statement';
import { Op } from 'angled-query/src/remote/query';
import tr from 'sdi/locale';

const renderOp = (op: Op) =>
    DIV(
        {
            className: 'operator',
            key: `operation-${op}-key`,
            onClick: () => setOp(op),
        },
        Operations[op]()
    );

export const renderOpSelect = () => {
    const ops = getWhereField()
        .map(({ field }) => applicableOps(field.type))
        .getOrElse([]);
    if (ops.length === 1) {
        const op = ops[0];
        window.setTimeout(() => setOp(op), 1);
    }

    return DIV(
        { className: 'query-tool' },
        DIV({ className: 'query-tool--operator-list' }, ...ops.map(renderOp))
    );
};

const renderPreviewNone = () =>
    DIV({ className: 'query-preview--none' }, tr.angled('selectOperator'));

const renderPreviewSelected = () =>
    DIV(
        { className: 'query-preview--none selected' },
        tr.angled('selectOperator')
    );

const renderPreviewSome = (op: Op) =>
    SPAN({ className: 'query-preview--operator' }, Operations[op]());

export const renderOpStep = () =>
    getOp().fold(renderPreviewNone(), renderPreviewSelected);

export const renderOpPreview = () => getOp().map(renderPreviewSome);
