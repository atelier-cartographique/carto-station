import { some, none } from 'fp-ts/lib/Option';
import { DIV, H2 } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { makeLabelAndIcon } from 'sdi/components/button';
// import { renderAudienceDisplayWidget } from 'angled-core/components/audience-display';
import { navigateNewQuery, navigateEdit } from 'angled-query/src/events/route';
import { openPublish } from 'angled-query/src/events/modal';
import {
    getCurrentQuery,
    getLocalizedQueryName,
} from 'angled-query/src/queries/builder';
import { isQueryEditable } from 'angled-query/src/queries/app';

const publishBtn = makeLabelAndIcon('save', 1, 'cog', () =>
    tr.angled('publishQuery')
);

const editButton = makeLabelAndIcon('navigate', 2, 'chevron-left', () =>
    tr.angled('backToWhere')
);

const renderPublish = () =>
    getCurrentQuery().fold(some(publishBtn(openPublish)), query =>
        isQueryEditable(query.user) ? some(publishBtn(openPublish)) : none
    );

// const headerContent =
//     () =>
//         DIV({ className: 'header__content' },
//             DIV({ className: 'app-info app-info--query' },
//                 H2({}, tr.angled('queryBuilder')),
//             ),
//         );

// const headerContent =
//     () =>
//         DIV({ className: 'header__content' },
//             DIV({ className: 'app-info app-info--query' },
//                 getCurrentQuery().fold(
//                     H2({}, tr.angled('addNewQuery')),
//                     q => H2({}, fromRecord(q.name)),
//                 ),
//             ),
//         );

export const queryHeaderWhere = () =>
    DIV(
        'query-header',
        DIV(
            'header__content',
            DIV(
                'app-info app-info--query',
                H2(
                    '',
                    tr.angled('queryBuilder'),
                    ' : ',
                    tr.angled('createStatement')
                )
            )
        )
    );

export const queryHeaderSelect = () =>
    DIV(
        'query-header',
        DIV(
            'header__content',
            DIV(
                'app-info app-info--query',
                H2('', getLocalizedQueryName()),
                editButton(() =>
                    getCurrentQuery().foldL(navigateNewQuery, q =>
                        navigateEdit(q.id)
                    )
                ),
                renderPublish()
            )
            // renderAudienceDisplayWidget()
        )
    );

export const queryHeaderView = () =>
    DIV(
        'query-header',
        DIV(
            'header__content',
            DIV(
                'app-info app-info--query',
                getCurrentQuery().map(q => H2('', fromRecord(q.name)))
            )
        )
    );
