import { DETAILS, DIV, H2, SPAN, SUMMARY } from 'sdi/components/elements';
import { Query } from 'angled-query/src/remote/query';
import tr, { fromRecord } from 'sdi/locale';
import {
    getSharedQueries,
    isQueryShared,
    getUserOnlyQuery,
    getUserSharedQuery,
} from 'angled-query/src/queries/builder';
import { makeLabelAndIcon } from 'sdi/components/button';
import { navigateExecSaved } from 'angled-query/src/events/route';
import { findUser } from 'angled-core/queries/ref';
import { renderWhereStatement } from '../statement';
import { nameToString } from 'sdi/components/button/names';

const runButton = makeLabelAndIcon('view', 3, 'play', () =>
    tr.angled('runQuery')
);

const queryActions = (q: Query) =>
    DIV(
        'query__actions',
        runButton(() => navigateExecSaved(q.id))
    );

const renderQuery = (q: Query) => {
    const where = q.statements.where.map(renderWhereStatement);
    return DIV({}, 'WHERE', where);
};

const queryName = (q: Query, withUser: boolean) =>
    DETAILS(
        'query__name--wrapper',

        SUMMARY(
            'query__name',
            SPAN('icon', nameToString('info')),
            fromRecord(q.name)
        ),
        DIV(
            'details-body',

            withUser
                ? DIV(
                      'query-user__wrapper',
                      DIV('query-label', tr.core('contact')),
                      DIV(
                          'query-user',
                          findUser(q.user).map(u => u.name)
                      )
                  )
                : '',
            DIV('query-label', tr.angled('query')),
            DIV('query__statement--view', renderQuery(q))
        )
    );

const userQueryClass = (q: Query) =>
    isQueryShared(q.id) ? 'query__item query__shared' : 'query__item';

const renderUserQuery = (q: Query) =>
    DIV({ className: userQueryClass(q) }, queryName(q, false), queryActions(q));

const renderCommonQuery = (q: Query) =>
    DIV('query__item', queryName(q, true), queryActions(q));

const userQuery = () =>
    DIV(
        'query__list',
        H2('head-title', tr.angled('myQueries')),
        ...getUserOnlyQuery().map(renderUserQuery),
        H2('head-title', tr.angled('mySharedQueries')),
        ...getUserSharedQuery().map(renderUserQuery)
    );

const commonQuery = () =>
    DIV(
        'query__list query__list--common',
        H2('head-title', tr.angled('commonQueries')),
        ...getSharedQueries().map(renderCommonQuery)
    );

const render = () =>
    DIV(
        'query__list--main',
        H2('head-title', tr.angled('queryList')),
        DIV('query__list--wrapper', userQuery(), commonQuery())
    );

export default render;
