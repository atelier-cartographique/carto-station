import { Nullable } from 'sdi/util';
import {
    InformationUnitName,
    FieldValueType,
    GeometryUnit,
} from 'angled-core/ui';
import { WhereBuilderStep } from 'angled-query/src/components/builder/where';
import { SelectBuilderStep } from 'angled-query/src/components/preview/select-multi';
import {
    Aggregate,
    Op,
    QueryWhere,
    QuerySelect,
    AggregateFilterNumberOp,
} from 'angled-query/src/remote/query';
import { IAlias, MessageRecord } from 'sdi/source';
import { FieldDescription } from 'angled-query/src/components/statement';
import { FeaturePath } from 'sdi/map';

// State Augmentation
declare module 'sdi/shape' {
    export interface IShape {
        'component/builder/where/step': WhereBuilderStep;
        'component/builder/where/ui': Nullable<InformationUnitName>;
        'component/builder/where/field': Nullable<FieldDescription>;
        'component/builder/where/op': Nullable<Op>;
        'component/builder/where/value': Nullable<FieldValueType>;
        'component/builder/where/in-flight-value': Nullable<FieldValueType>;
        'component/builder/where': QueryWhere[];

        'component/builder/select/step': SelectBuilderStep;
        'component/builder/select/name': Nullable<string>;
        'component/builder/select/ui': Nullable<{
            unit: InformationUnitName;
            index: Nullable<number>;
        }>;
        'component/builder/select/field': Nullable<FieldDescription>;
        'component/builder/select/agg': Nullable<Aggregate>;
        'component/builder/select/agg/number/op': Nullable<AggregateFilterNumberOp>;
        'component/builder/select': QuerySelect[];
        'component/builder/select/name/form': IAlias | Omit<IAlias, 'id'>;

        'component/builder/current-query': Nullable<number>;
        'component/builder/query-name': Nullable<MessageRecord>;
        'component/builder/query-share-desciption': Nullable<MessageRecord>;
        'component/builder/geometry/select': Nullable<FeaturePath>;
        'component/builder/metadata/published': GeometryUnit[];

        'component/builder/info/filter-pattern': string;
    }
}

export const defaultBuilderState = () => ({
    'component/builder/where/step': 'None' as WhereBuilderStep,
    'component/builder/where/ui': null,
    'component/builder/where/field': null,
    'component/builder/where/op': null,
    'component/builder/where/value': null,
    'component/builder/where/in-flight-value': null,
    'component/builder/where': [],

    'component/builder/select/step': 'Initial' as SelectBuilderStep,
    'component/builder/select/name': null,
    'component/builder/select/agg': null,
    'component/builder/select/agg/number/op': null,
    'component/builder/select/ui': null,
    'component/builder/select/field': null,
    'component/builder/select': [],
    'component/builder/select/name/form': { select: '', replace: {} },

    'component/builder/current-query': null,
    'component/builder/query-name': null,
    'component/builder/query-share-desciption': null,
    'component/builder/geometry/select': null,
    'component/builder/metadata/published': [],

    'component/builder/info/filter-pattern': '',
});
