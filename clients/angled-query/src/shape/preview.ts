import { IDataTable, initialTableState } from 'sdi/components/table';
import {
    FeatureCollection,
    RemoteResource,
    remoteNone,
    MessageRecord,
} from 'sdi/source';
import { Nullable } from 'sdi/util';

// State Augmentation
declare module 'sdi/shape' {
    export interface IShape {
        'component/preview/table': IDataTable;
        'component/preview/map': boolean;
        'component/preview/geojson/point': RemoteResource<FeatureCollection>;
        'component/preview/geojson/line': RemoteResource<FeatureCollection>;
        'component/preview/geojson/polygon': RemoteResource<FeatureCollection>;
        'component/preview/map/loading': MessageRecord[];
        'component/preview/table/selected': Nullable<number>;
    }
}

export const defaultPreviewState = () => ({
    'component/preview/table': initialTableState(),
    'component/preview/map': false,
    'component/preview/geojson/point': remoteNone,
    'component/preview/geojson/line': remoteNone,
    'component/preview/geojson/polygon': remoteNone,
    'component/preview/map/loading': [],
    'component/preview/table/selected': null,
});
