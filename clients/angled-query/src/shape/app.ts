// imports from sdi
import { ButtonComponent } from 'sdi/components/button';
import { Collection } from 'sdi/util';

export type AppLayout =
    | 'splash'
    | 'landing'
    | 'builder'
    | 'confirm'
    | 'preview';

// State Augmentation
declare module 'sdi/shape' {
    export interface IShape {
        'app/layout': AppLayout;
        'app/route': string[];

        'component/button': ButtonComponent;
        'component/foldable': Collection<boolean>;
    }
}
