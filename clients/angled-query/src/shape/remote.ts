import {
    Inspire,
    FeatureCollection,
    RemoteResource,
    remoteNone,
} from 'sdi/source';
import { Collection } from 'sdi/util';
import { Query, QueryShare, ResultRow } from '../remote/query';
import { Project } from 'angled-core/ui';

// State Augmentation
declare module 'sdi/shape' {
    export interface IShape {
        'data/datasetMetadata': Inspire[];
        'data/layer': Collection<FeatureCollection>;
        'data/queries': Query[];
        'data/shares': QueryShare[];
        'data/results': RemoteResource<ResultRow[]>;
        'data/projects': Collection<RemoteResource<Project>>;
    }
}

export const initialRemoteState = () => ({
    'data/datasetMetadata': [],
    'data/layer': {},
    'data/queries': [],
    'data/shares': [],
    'data/results': remoteNone,
    'data/projects': {},
});
