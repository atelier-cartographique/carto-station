import { DIV, H1 } from 'sdi/components/elements';
import tr from 'sdi/locale';

import { makeLabelAndIcon } from '../button';
import { buildForm } from '../../events/alias';

import form from './form';
import table from './table';

const addButton = makeLabelAndIcon('add', 1, 'plus', () =>
    tr.alias('createAlias')
);

const render = () =>
    DIV(
        { className: 'alias-wrapper' },
        H1({}, tr.alias('aliasDictonary')),
        DIV(
            { className: 'alias-widget' },
            DIV(
                { className: 'alias-table' },
                addButton(() => buildForm('')),
                table()
            ),
            form()
        )
    );

export default render;
