// import * as debug from 'debug';

// import { index } from 'fp-ts/lib/Array';
// import { literal, union, TypeOf } from 'io-ts';

// import { Router, Path } from 'sdi/router';

// import { setLayout } from './app';
// import { buildForm } from './alias';



// const logger = debug('sdi:route');

// const RouteIO = union([
//     literal('index'),
//     literal('record')
// ]);
// type Route = TypeOf<typeof RouteIO>;

// const { home, route, navigate } = Router<Route>('metadata');



// const recordParser =
//     (p: Path) => index(0, p);

// home('index', (_p) => {
//     setLayout('Alias');
// });

// route('record',
//     r => r.foldL(
//         () => logger('failed on parser for table', r),
//         (id: string) => {
//             buildForm(id);
//             setLayout('Selected');
//         }),
//     recordParser);


// export const loadRoute =
//     (initial: string[]) =>
//         index(0, initial)
//             .map(prefix =>
//                 RouteIO
//                     .decode(prefix)
//                     .map(c => navigate(c, initial.slice(1))));

// export const navigateIndex =
//     () => navigate('index', []);

// export const navigateRecord =
//     (id: number) => navigate('record', [id]);

// logger('loaded');