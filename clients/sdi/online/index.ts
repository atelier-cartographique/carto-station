import { OnlineIO } from '../source';
import { fetchIO } from '../source/remote';

const fetchOnline = (source: number) =>
    fetchIO(OnlineIO, `/online/meter/${source}`);

// There's still some experiment to be made here
const timeout = 12000;
const maxTime = 3000;
let interval: null | number = null;

const handler = (onChange: (quality: number) => void) => () => {
    const source = Date.now();
    fetchOnline(source)
        .then(({ serverTime }) => {
            const upLink = serverTime - source;
            const downLink = Date.now() - serverTime;
            const total = upLink + downLink;
            const score = Math.max(
                0,
                Math.min(100, 100 - Math.round((total * 100) / maxTime))
            );
            onChange(score);
        })
        .catch(() => onChange(0));
};

export const startMonitor = (onChange: (quality: number) => void) => {
    if (interval === null) {
        interval = window.setInterval(handler(onChange), timeout);
        window.addEventListener('offline', () => onChange(0));
    }
};

export default startMonitor;
