/**
 * @license
 *
 * Copyright (c) 2011-2014, Christopher Jeffrey. (MIT Licensed)
 * https://github.com/chjj/marked
 *
 * Copyright (c) 2018, Костя Третяк. (MIT Licensed)
 * https://github.com/KostyaTretyak/marked-ts
 */

import { InlineLexer } from './inline-lexer';
import { Links, MarkedOptions, SimpleRenderer, Token, TokenType } from './interfaces';
import { Marked } from './marked';
import { Renderer } from './renderer';
import { NodeOrOptional } from '../../components/elements';
import { none } from 'fp-ts/lib/Option';
import { filterNotNull } from '../../util';


type ParseResult = [TokenType, NodeOrOptional, Token[]];

const fst = (r: ParseResult) => r[0];
const snd = (r: ParseResult) => r[1];
const tail = (r: ParseResult) => r[2];

/**
 * Parsing & Compiling.
 */
export class Parser {
    simpleRenderers: SimpleRenderer[] = [];

    protected inlineLexer: InlineLexer;
    protected options: MarkedOptions;
    protected renderer: Renderer;
    protected line: number = 0;
    protected accumulator: NodeOrOptional[];

    constructor(options?: MarkedOptions) {
        // tokens = [];
        // token = null;
        this.options = options || Marked.options;
        this.renderer = new Renderer(this.options);
        this.accumulator = [];
    }

    clearAccumulator() {
        this.accumulator = [];
    }

    static parse(tokens: Token[], links: Links, options?: MarkedOptions): NodeOrOptional[] {
        const parser = new this(options);
        return parser.parse(links, tokens);
    }

    parse(links: Links, tokens: Token[]) {
        this.inlineLexer = new InlineLexer(InlineLexer, links, this.options, this.renderer);
        let toks = tokens;
        const nodes: NodeOrOptional[] = [];
        while (toks.length > 0) {
            const result = this.tok(toks[0], toks.slice(1));
            nodes.push(snd(result));
            toks = tail(result);
        }
        return nodes;
    }


    protected tok(token: Token, rest: Token[]): ParseResult {
        const pair = (tt: TokenType, nn: NodeOrOptional, input = rest): ParseResult => [tt, nn, input];
        // console.log('Tok', TokenType[token.type], token);
        switch (token.type) {

            case TokenType.space: {
                return pair(TokenType.space, '');
            }

            case TokenType.paragraph: {
                return pair(
                    TokenType.paragraph,
                    this.renderer.paragraph(this.inlineLexer.output(token.text ?? '')));
            }

            case TokenType.text: {
                if (this.options.isNoP) {
                    return pair(TokenType.text, this.inlineLexer.output(token.text ?? ''));
                }
                else {
                    return pair(
                        TokenType.text,
                        this.renderer.paragraph(this.inlineLexer.output(token.text ?? '')));
                }
            }

            case TokenType.heading: {
                return pair(TokenType.heading,
                    this.renderer.heading(
                        this.inlineLexer.output(token.text ?? ''),
                        token.depth ?? 0,
                        token.text ?? '')
                );
            }

            case TokenType.listStart: {
                // this.clearAccumulator()
                const ordered = token.ordered;
                return (() => {
                    // while (this.next().type != TokenType.listEnd) {
                    //   body += this.tok();
                    // }
                    const body: NodeOrOptional[] = [];
                    let depth = 0;
                    for (let i = 0; i < rest.length; i += 1) {
                        const p = this.tok(rest[i], rest.slice(i + 1));
                        const t = fst(p);
                        // console.log('TokenType.listStart', i, TokenType[t], snd(p));
                        if (t === TokenType.listItemStart && depth === 0) {
                            body.push(snd(p));
                        }
                        else if (t === TokenType.listStart) {
                            depth += 1;
                        }
                        else if (t === TokenType.listEnd && depth > 0) {
                            depth -= 1;
                        }
                        else if (t === TokenType.listEnd && depth === 0) {
                            return pair(TokenType.listStart, this.renderer.list(body, ordered), rest.slice(i + 1));
                        }
                    }
                    return pair(TokenType.listStart, this.renderer.list(body, ordered), []);
                })();
            }

            case TokenType.listItemStart: {
                // let body = '';

                // while (this.next().type != TokenType.listItemEnd) {
                //   body += token.type == (TokenType.text as any) ? this.parseText() : this.tok();
                // }
                return (() => {
                    const body: NodeOrOptional[] = [];
                    let depth = 0;
                    for (let i = 0; i < rest.length; i += 1) {
                        const p = this.tok(rest[i], rest.slice(i + 1));
                        const t = fst(p);
                        // console.log('TokenType.listItemStart', i, TokenType[t], snd(p));
                        if (t !== TokenType.listItemEnd && depth === 0) {
                            body.push(snd(p));
                        }
                        else if (t === TokenType.listItemStart) {
                            depth += 1;
                        }
                        else if (t === TokenType.listItemEnd && depth > 0) {
                            depth -= 1;
                        }
                        else if (t === TokenType.listItemEnd && depth === 0) {
                            return pair(TokenType.listItemStart, this.renderer.listitem(filterNotNull(body)), rest.slice(i + 1));
                        }
                    }
                    return pair(TokenType.listItemStart, this.renderer.listitem(filterNotNull(body)), []);
                })();
            }

            case TokenType.looseItemStart: {
                // let body = '';

                // while (this.next().type != TokenType.listItemEnd) {
                //   body += this.tok();
                // }

                // return this.renderer.listitem(body);


                const body: NodeOrOptional[] = [];
                let depth = 0;
                for (let i = 0; i < rest.length; i += 1) {
                    const p = this.tok(rest[i], rest.slice(i + 1));
                    const t = fst(p);
                    if (t === TokenType.listItemStart && depth === 0) {
                        body.push(snd(p));
                    }
                    else if (t === TokenType.listItemStart) {
                        depth += 1;
                    }
                    else if ((t === TokenType.listItemEnd || t === TokenType.looseItemEnd) && depth > 0) {
                        depth -= 1;
                    }
                    else if ((t === TokenType.listItemEnd || t === TokenType.looseItemEnd) && depth === 0) {
                        return pair(TokenType.listItemStart, this.renderer.listitem(filterNotNull(body)), rest.slice(i + 1));
                    }
                }

                return pair(TokenType.listItemStart, this.renderer.listitem(filterNotNull(body)), []);
            }

            case TokenType.code: {
                return pair(
                    TokenType.code,
                    this.renderer.code(token.text ?? '', token.lang, token.escaped));
            }

            case TokenType.table: {
                let header: NodeOrOptional = none;
                let body: NodeOrOptional = none;
                let cell: NodeOrOptional[] = [];

                // header
                if (token.header) {
                    for (let i = 0; i < token.header?.length; i += 1) {
                        const flags = { header: true, align: token.align![i] }; // FIXME !
                        const out = this.inlineLexer.output(token.header[i]);

                        cell.push(this.renderer.tablecell(out, flags));
                    }

                    header = this.renderer.tablerow(cell.slice(0));
                }

                if (token.cells) {
                    for (const row of token.cells) {
                        cell = [];

                        for (let j = 0; j < row.length; j += 1) {
                            cell.push(this.renderer.tablecell(this.inlineLexer.output(row[j]), {
                                header: false,
                                align: token.align![j]
                            }));
                        }

                        body = this.renderer.tablerow(cell);
                    }
                }

                return pair(TokenType.table, this.renderer.table(header, body));
            }

            case TokenType.blockquoteStart: {
                const body: NodeOrOptional[] = [];

                // while (this.next().type != TokenType.blockquoteEnd) {
                //   body += this.tok();
                // }
                for (let i = 0; i < rest.length; i += 1) {
                    const p = this.tok(rest[i], rest.slice(i + 1));
                    const t = fst(p);
                    // console.log(`TokenType.blockquoteStart ${i}`, rest[i]);
                    if (t === TokenType.blockquoteEnd) {
                        return pair(TokenType.blockquoteStart, this.renderer.blockquote(filterNotNull(body)), rest.slice(i + 1));
                    }
                    body.push(snd(p));
                }

                return pair(TokenType.blockquoteStart, this.renderer.blockquote(filterNotNull(body)), []);
            }

            case TokenType.hr: {
                return pair(TokenType.hr, this.renderer.hr());
            }

            case TokenType.html: {
                const html =
                    !token.pre && !this.options.pedantic ?
                        this.inlineLexer.output(token.text!) :
                        token.text!;
                return pair(TokenType.html, this.renderer.html(html));
            }

            default: {
                return pair(token.type, none);
                // if (this.simpleRenderers.length) {
                //   for (let i = 0; i < this.simpleRenderers.length; i++) {
                //     if (token.type == 'simpleRule' + (i + 1)) {
                //       return this.simpleRenderers[i].call(this.renderer, token.execArr);
                //     }
                //   }
                // }

                // const errMsg = `Token with "${token.type}" type was not found.`;

                // if (this.options.silent) {
                //   // console.log(errMsg);
                // } else {
                //   throw new Error(errMsg);
                // }
            }
        }
    }
}
