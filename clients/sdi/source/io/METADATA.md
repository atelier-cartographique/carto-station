# Metadata


Metadata records are the documents that reference data sources. They are modelled after the [INSPIRE Directive](https://inspire.ec.europa.eu/).


**TODO**

```typescript
export interface Inspire {
    id: uuid,
    geometryType: GeometryType,
    dataStreamUrl: string | null,
    published: boolean,

    resourceTitle: ResourceTitle,
    resourceAbstract: ResourceAbstract,
    uniqueResourceIdentifier: DatasetUID, 
    topicCategory: string[],
    keywords: string[],
    geographicBoundingBox: BoundingBox,
    temporalReference: TemporalReference,
    responsibleOrganisation: number[], 
    metadataPointOfContact: number[],
    metadataDate: MdDate, 
    resourceLanguage?: MetadataLanguageCode,
}
```

## `id`

A `uuid` identifier for this record.

## `geometryType`


```typescript
type GeometryType = 
    | "Point" 
    | "Polygon" 
    | "LineString" 
    | "MultiPoint" 
    | "MultiPolygon" 
    | "MultiLineString"
```

## `dataStreamUrl`



## `published`


## `resourceTitle`

## `resourceAbstract`

## `uniqueResourceIdentifier`

## `topicCategory`

## `keywords`
## `geographicBoundingBox`
## `temporalReference`
## `responsibleOrganisation`
## `metadataPointOfContact`
## `metadataDate`
## `resourceLanguage`