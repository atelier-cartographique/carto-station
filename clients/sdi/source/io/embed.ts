import * as io from 'io-ts';

export const EmbedProfileIO = io.union([
    io.literal('simple'),
    io.literal('all-tools'),
    io.literal('fancy'),
]);
export type EmbedProfile = io.TypeOf<typeof EmbedProfileIO>;
