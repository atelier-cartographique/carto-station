import * as io from 'io-ts';
import { i } from '../io';
import { BaseActivityIO } from './base';

// print-report action
export const PrintReportDataIO = i(
    {
        action: io.literal('print-report'),
        parameter: i({
            address: io.string,
        }),
    },
    'PrintReportDataIO'
);
export type PrintReportData = io.TypeOf<typeof PrintReportDataIO>;

export const PrintReportIO = io.intersection(
    [BaseActivityIO, PrintReportDataIO],
    'PrintReportIO'
);
export type PrintReport = io.TypeOf<typeof PrintReportIO>;

// navigate-detail action
export const NavigateDetailDataIO = i(
    {
        action: io.literal('navigate-detail'),
        parameter: i({
            system: io.string,
        }),
    },
    'NavigateDetailDataIO'
);
export type NavigateDetailData = io.TypeOf<typeof NavigateDetailDataIO>;

export const NavigateDetailIO = io.intersection(
    [BaseActivityIO, NavigateDetailDataIO],
    'NavigateDetailIO'
);
export type NavigateDetail = io.TypeOf<typeof NavigateDetailIO>;

// navigate-solar-contact action
export const NavigateSolarContactDataIO = i(
    {
        action: io.literal('navigate-solar-contact'),
        parameter: i({
            system: io.string,
        }),
    },
    'NavigateSolarContactDataIO'
);
export type NavigateSolarContactData = io.TypeOf<typeof NavigateSolarContactDataIO>;

export const NavigateSolarContactIO = io.intersection(
    [BaseActivityIO, NavigateSolarContactDataIO],
    'NavigateSolarContactIO'
);
export type NavigateSolarContact = io.TypeOf<typeof NavigateSolarContactIO>;

// navigate-capakey action
export const NavigateCapakeyDataIO = i(
    {
        action: io.literal('navigate-capakey'),
        parameter: i({
            capakey: io.string,
        }),
    },
    'NavigateCapakeyDataIO'
);
export type NavigateCapakeyData = io.TypeOf<typeof NavigateCapakeyDataIO>;

export const NavigateCapakeyIO = io.intersection(
    [BaseActivityIO, NavigateCapakeyDataIO],
    'NavigateCapakeyIO'
);
export type NavigateCapakey = io.TypeOf<typeof NavigateCapakeyIO>;
