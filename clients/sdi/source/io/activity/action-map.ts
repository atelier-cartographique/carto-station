import * as io from 'io-ts';
import { i, MessageRecordIO } from '../io';
import { BaseActivityIO } from './base';


// navigate-map action
export const NavigateMapDataIO = i({
    action: io.literal('navigate-map'),
    parameter: i({
        id: io.string,
        title: MessageRecordIO,
    })
})
export type NavigateMapData = io.TypeOf<typeof NavigateMapDataIO>

export const NavigateMapIO = io.intersection([
    BaseActivityIO,
    NavigateMapDataIO,
],
    'NavigateMapIO')

export type NavigateMap = io.TypeOf<typeof NavigateMapIO>

// print-map action
export const PrintMapDataIO = i({
    action: io.literal('print-map'),
    parameter: i({
        id: io.string,
        title: MessageRecordIO,
    })
}, 'PrintMapDataIO')
export type PrintMapData = io.TypeOf<typeof PrintMapDataIO>

export const PrintMapIO = io.intersection([
    BaseActivityIO,
    PrintMapDataIO
], 'PrintMapIO')
export type PrintMap = io.TypeOf<typeof PrintMapIO>;

