import * as io from 'io-ts';
import { i } from '../io';
import { BaseActivityIO } from './base';

// select-water-level action
export const SelectWaterLevelDataIO = i(
    {
        action: io.literal('select-water-level'),
        parameter: i({
            type: io.union([io.literal('ground'), io.literal('surface')]),
        }),
    },
    'SelectWaterLevelDataIO'
);
export type SelectWaterLevelData = io.TypeOf<typeof SelectWaterLevelDataIO>;

export const SelectWaterLevelIO = io.intersection(
    [BaseActivityIO, SelectWaterLevelDataIO],
    'SelectWaterLevelIO'
);
export type SelectWaterLevel = io.TypeOf<typeof SelectWaterLevelIO>;
