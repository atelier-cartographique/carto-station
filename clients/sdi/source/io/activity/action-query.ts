import * as io from 'io-ts';
import { i, MessageRecordIO } from '../io';
import { BaseActivityIO } from './base';


// navigate-query action
export const NavigateQueryDataIO = i({
    action: io.literal('navigate-query'),
    parameter: i({
        id: io.string,
        title: MessageRecordIO,
    })
})
export type NavigateQueryData = io.TypeOf<typeof NavigateQueryDataIO>

export const NavigateQueryIO = io.intersection([
    BaseActivityIO,
    NavigateQueryDataIO,
],
    'NavigateQueryIO')

export type NavigateQuery = io.TypeOf<typeof NavigateQueryIO>


