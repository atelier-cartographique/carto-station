import * as io from 'io-ts';

export const OnlineIO = io.interface(
    {
        source: io.number,
        serverTime: io.number,
    },
    'OnlineIO'
);
