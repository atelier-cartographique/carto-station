# An overview of a map

This overview uses *Typescript* interfaces and types to describe the data structures involved. These interfaces "transpile" to plain *Javascript* objects, consequently subject to *Javascript* Object Notation (JSON). Thus it should be trivial to infere the correct notation in languages supporting JSON.
Note that optional keys are indicated with a `?` between the identifier and the colon, other keys are required to be present and well typed.



## `IMapInfo`

The root interface.

```typescript
export interface IMapInfo{
        id: uuid;
        title: MessageRecord;
        description: MessageRecord;
        imageUrl?: string,
        url: string;
        status: MapStatus;
        lastModified: number;
        attachments: uuid[];
        categories: uuid[];
        baseLayer: string;
        layers: ILayerInfo[];
}
```

### `IMapInfo.id`

It's a UUIDv4 `string` representation and serves as a uniq identifier for a map resource. It's conveniently produced in *Python* with the `uuid` module coming with the standard library.

```python
from uuid import uuid4
id = uuid4()
str(id)
# -> '689f4d6f-7f9c-4e04-8bed-04cd1db2a83a'
```

Or, in the context of a *Django* model, one might leave it to the ORM alltogether.

```python
from uuid import uuid4
from django.db import models

class MyModel(models.Model):
    id = models.UUIDField(
            primary_key=True, 
            default=uuid4, 
            editable=False,
        )
```


### `IMapInfo.title`


The title key holds a `MessageRecord` which has the (simplified) following interface.


```typescript
export interface MessageRecord {
    fr?: string;
    nl?: string;
    en?: string;
}
```

It's used as a display name for the map.

### `IMapInfo.description`

A description for the map, encoded as a `MessageRecord`.


### `IMapInfo.imageUrl`

An optional URL `string` pointing to an image that illustrates this map.



### `IMapInfo.url`

The URL `string` from which this resource comes from.


### `IMapInfo.status`

Indicates if the map is to be listed on the Atlas index page. The `MapStatus` type has the signature

```typescript
export type MapStatus = 'draft' | 'published';
```


### `IMapInfo.lastModified`

An integer `number` representing a POSIX timestamp (seconds) of the last modification.


### `IMapInfo.attachments`

A `uuid array` referencing documents identifiers uploaded by a user.

### `IMapInfo.categories`

A `uuid array` referencing categories identifiers available on the API.


### `IMapInfo.baseLayer`

A `string` identifier for a WMS service registered on the API.


### `IMapInfo.layers`

A `ILayerInfo array`. The `ILayerInfo` object describes a layer to be displayed on the map. Layers shall be sorted upward (from bottom to top). Because of the complexity of this data structure, we'll describe it in a dedicated section.


## `ILayerInfo`


```typescript
export interface  ILayerInfo{
        id: uuid,
        metadataId: uuid,
        visible: boolean,
        minZoom?: number,
        maxZoom?: number,
        legend: MessageRecord | null,
        group: LayerGroup | null,
        layerInfoExtra: LayerInfoExtra | null,
        featureViewOptions: FeatureViewOptions,
        style: StyleConfig,
    }
```

### `ILayerInfo.id`

A `uuid` identifier for this layer.


### `ILayerInfo.metadataId`

A `uuid` identifier of a metadata record available on the API. [Metadata records](./METADATA.md) inform a data source from which geographic features can be obtained to display on a map.

### `ILayerInfo.visible`

A `boolean` controlling if the layer should be made visible by default.


### `ILayerInfo.minZoom` and `ILayerInfo.minZoom`

Optional integer `number`s controlling the zoom interval at which this layer is visible. Zoom levels are expressed roughly as in [openstreetmap zoom levels](https://wiki.openstreetmap.org/wiki/Zoom_levels).


### `ILayerInfo.legend`

A `MessageRecord` or `null` indicating, if not null, a label for the layer in the legend. If null, the label will be the title of the metadata record associated with this layer.


### `ILayerInfo.group`

Almost obsolete, should be null. 


### `ILayerInfo.layerInfoExtra`

Almost obsolete, should be null. 


### `ILayerInfo.featureViewOptions`

Holds a `FeatureViewOptions` object that acts as a *template* for the display of a feature's attributes. It's the union of a `FeatureViewDefault` which marks for a generic display of all key/value pairs, and  a `FeatureViewConfig` which offers ways to design such view in more details. Such details are encoded as `RowConfig` objects that are described in a [dedicated section](./ROWCONFIG.md).

```typescript
export type FeatureViewOptions = FeatureViewDefault | FeatureViewConfig

export interface FeatureViewDefault { type: 'default' }

export interface FeatureViewConfig {
    type: 'config';
    rows: RowConfig[];
}
```


### `ILayerInfo.style`


The `StyleConfig` here drives the styling of this layer on map. It's a quite complex data structure which again is worth its [own section](./STYLE.md).



## Examples

### A test map

```json
{
    "id": "9c0a6a1e-36b5-4729-bbb5-1cf404d3a265",
    "url": "/api/maps/9c0a6a1e-36b5-4729-bbb5-1cf404d3a265",
    "lastModified": 1610362570253,
    "user": 1,
    "status": "published",
    "title": {
        "en": "test",
        "fr": "testc"
    },
    "description": {
        "en": "",
        "fr": ""
    },
    "baseLayer": "urbis.irisnet.be/urbis_gray",
    "imageUrl": "/documents/images/fb4cbef5-77f5-4e85-bc4b-084f3a17b34d",
    "categories": [],
    "attachments": [],
    "layers": [
        {
            "id": "8027fb2f-a7b0-4473-9bad-bbf2e5965791",
            "metadataId": "dbea182c-2604-49f5-b7a3-6eeff5d1fbc3",
            "visible": true,
            "style": {
                "kind": "polygon-continuous",
                "label": {
                    "size": 12,
                    "align": "center",
                    "color": "hsla(234.10000000000002, 82%, 35.4%, 0.987)",
                    "baseline": "alphabetic",
                    "propName": {
                        "en": "",
                        "fr": "capakey",
                        "nl": ""
                    },
                    "resLimit": 35
                },
                "propName": "gid",
                "intervals": [
                    {
                        "low": 1,
                        "high": 230,
                        "label": {
                            "en": "1 - 230",
                            "fr": "1 - 230",
                            "nl": "1 - 230"
                        },
                        "pattern": false,
                        "fillColor": "hsl(195.60000000000002, 100%, 56.3%)",
                        "strokeColor": "hsl(16.100000000000023, 100%, 30.2%)",
                        "strokeWidth": 1,
                        "patternAngle": 0
                    },
                    {
                        "low": 230,
                        "high": 459,
                        "label": {
                            "en": "230 - 459",
                            "fr": "230 - 459",
                            "nl": "230 - 459"
                        },
                        "pattern": false,
                        "fillColor": "#00729A",
                        "strokeColor": "#00729A",
                        "strokeWidth": 1,
                        "patternAngle": 0
                    }
                ]
            },
            "featureViewOptions": {
                "rows": [
                    {
                        "lang": "fr",
                        "type": "string",
                        "options": {
                            "level": "subtitle",
                            "style": "normal",
                            "withLabel": true
                        },
                        "propName": "capakey"
                    }
                ],
                "type": "config"
            },
            "group": null,
            "legend": {
                "en": "",
                "fr": "bleu"
            },
            "minZoom": 0,
            "maxZoom": 30,
            "layerInfoExtra": null
        },
        {
            "id": "0ec712b3-e8b5-4d1d-9b1a-0aa1b91bdbad",
            "metadataId": "9bb19349-8e1f-45d9-891f-d1023d348c69",
            "visible": true,
            "style": {
                "kind": "point-continuous",
                "label": {
                    "size": 12,
                    "align": "start",
                    "color": "hsl(328.1, 96.7%, 46.5%)",
                    "xOffset": 0,
                    "yOffset": 12,
                    "baseline": "middle",
                    "propName": {
                        "en": "",
                        "fr": "nom",
                        "nl": ""
                    },
                    "resLimit": 39
                },
                "propName": "id",
                "intervals": [
                    {
                        "low": 1,
                        "high": 7.5,
                        "label": {
                            "en": "1 - 7.5",
                            "fr": "1 - 7.5",
                            "nl": "1 - 7.5"
                        },
                        "marker": {
                            "size": 10,
                            "color": "hsl(339.9, 100%, 30.2%)",
                            "codePoint": 61842
                        }
                    },
                    {
                        "low": 7.5,
                        "high": 14,
                        "label": {
                            "en": "7.5 - 14",
                            "fr": "7.5 - 14",
                            "nl": "7.5 - 14"
                        },
                        "marker": {
                            "size": 10,
                            "color": "#00729A",
                            "codePoint": 61713
                        }
                    }
                ]
            },
            "featureViewOptions": {
                "type": "default"
            },
            "group": null,
            "legend": {
                "en": "",
                "fr": "Un peu de Texte"
            },
            "minZoom": 0,
            "maxZoom": 30,
            "layerInfoExtra": null
        },
        {
            "id": "7b6641c7-6dc9-4063-a8eb-442713a6cbb6",
            "metadataId": "206cd428-ba7c-4a14-b6eb-f4d543091266",
            "visible": true,
            "style": {
                "kind": "polygon-simple",
                "pattern": true,
                "fillColor": "hsl(340, 100%, 30.2%)",
                "strokeColor": "hsl(144.39999999999998, 100%, 12.4%)",
                "strokeWidth": 2,
                "patternAngle": 90
            },
            "featureViewOptions": {
                "rows": [
                    {
                        "lang": "fr",
                        "type": "string",
                        "options": {
                            "level": "normal",
                            "style": "bold",
                            "withLabel": false
                        },
                        "propName": "official_name"
                    },
                    {
                        "lang": "fr",
                        "type": "string",
                        "options": {
                            "level": "normal",
                            "style": "normal",
                            "withLabel": true
                        },
                        "propName": "gestion"
                    },
                    {
                        "lang": "fr",
                        "type": "string",
                        "options": {
                            "level": "normal",
                            "style": "normal",
                            "withLabel": true
                        },
                        "propName": "commune"
                    }
                ],
                "type": "config"
            },
            "group": {
                "id": "ea9c7e9d-f6fb-472d-aa67-7c0ed1a90150",
                "name": {
                    "en": "group1",
                    "fr": "group1"
                }
            },
            "legend": {
                "en": "",
                "fr": "pub green"
            },
            "minZoom": 0,
            "maxZoom": 30,
            "layerInfoExtra": null
        },
        {
            "id": "e3532a93-46b6-4776-968e-174dd567b143",
            "metadataId": "ee2ec864-b67e-4f13-a848-584aaa365786",
            "visible": true,
            "style": {
                "dash": [],
                "kind": "line-simple",
                "label": {
                    "size": 13,
                    "align": "center",
                    "color": "hsl(328.4, 89.2%, 19.4%)",
                    "yOffset": 0,
                    "baseline": "alphabetic",
                    "propName": {
                        "en": "",
                        "fr": "nomfr",
                        "nl": ""
                    },
                    "resLimit": 23
                },
                "strokeColor": "hsl(331, 90.2%, 30.2%)",
                "strokeWidth": 3
            },
            "featureViewOptions": {
                "rows": [
                    {
                        "lang": "fr",
                        "type": "html",
                        "options": {
                            "level": "normal",
                            "style": "normal",
                            "withLabel": true
                        },
                        "propName": "nomfr"
                    }
                ],
                "type": "config"
            },
            "group": {
                "id": "ea9c7e9d-f6fb-472d-aa67-7c0ed1a90150",
                "name": {
                    "en": "group1",
                    "fr": "group1"
                }
            },
            "legend": {
                "en": "",
                "fr": "Gros titre"
            },
            "minZoom": 0,
            "maxZoom": 30,
            "layerInfoExtra": null
        }
    ]
}
```