# Feature's attributes display configuration


The display of attributes is controlled by a list of `RowConfig` objects, one per desired bloc and language. For most configuration objects, it is a matter of styling an input data found at `propName` key in the feature's properties. However, some might ignore the `propName` value and get their data based on their configuration options, but because of their shared base interface they still have to provide a `propName` value, for which an empty string will do.


## `RowConfig`

It's the union of specialized data structures for different types of input data and formating options. All based on the same design:

```typescript
export type PropType = 
    | "string" 
    | "number" 
    | "boolean" 
    | "text" 
    | "url" 
    | "image" 
    | "html" 
    | "piechart" 
    | "timeserie"

export interface BaseConfig {
    lang: MessageRecordLang; // a supported language code, such as 'fr', 'nl', etc.
    propName: string;
    type: PropType;
    options: ...
}


export type RowConfig = 
    | StringConfig
    | NumberConfig
    | BooleanConfig
    | URLConfig
    | ImageConfig
    | PiechartConfig
    | TimeserieConfig
    | HTMLConfig
    | TextConfig
```


### `StringConfig`

The most basic configuration that treats the input data as a string. Note that the `StringOptions` are generic quite generic and will be re-used, even though aliased, for   `NumberConfig`, `BooleanConfig` and `URLConfig`

```typescript
type StringOptions = {
    // font size
    level: "title" | "subtitle" | "normal";
    // font style
    style: "normal" | "bold" | "italic" | "bold-italic";
    // control the display of the column name
    withLabel: boolean;
}

export interface StringConfig {
    type: 'string';
    lang: MessageRecordLang;
    propName: string;
    options: StringOptions;
}
```


### `NumberConfig`

Offers the same format options that of a `StringConfig` but will run the value through `sdi/locale/formatNumber` first to get a string to format.

```typescript
type URLOptions = StringOptions;
export interface NumberConfig {
    type: 'number';
    lang: MessageRecordLang;
    propName: string;
    options: URLOptions;
}
```


### `BooleanConfig`

Offers the same format options that of a `StringConfig`. Does not transform the input value.

```typescript
type BooleanOptions = StringOptions;
export interface BooleanConfig {
    type: 'boolean';
    lang: MessageRecordLang;
    propName: string;
    options: BooleanOptions;
}
```

### `URLConfig`

Offers the same format options that of a `StringConfig`. We build an `HTMLAnchorElement` off of the input value, which is used for the `href` attribute as well as the textual content. 

```typescript
type URLOptions = StringOptions;
export interface BooleanConfig {
    type: 'url';
    lang: MessageRecordLang;
    propName: string;
    options: URLOptions;
}
```


### `ImageConfig`

We build an `HTMLImageElement` from the input value, used as content for the `src` attribute.

```typescript
type ImageOptions = {
    withLabel: boolean;
};
export interface ImageConfig {
    type: 'image';
    lang: MessageRecordLang;
    propName: string;
    options: ImageOptions;
}
```

### `PiechartConfig`

Configure an SVG pie chart with a legend. Each slice is configured by a `PiechartPiece` objects referencing a properties key (`propName`) from which to get the input value and a color (`color`) in the form of a CSS color string, with an optional `label` to replace the column name in the legend.

The `PiechartOptions` object let you configure the `scale`, that is if the input value will be first ran through `Math.log` before being used. And finally with the `radius` key if tou want the radius of each slice to be a function of the input value (`'dynamic'`) or fixed (`'normal'`).


```typescript
type PiechartPiece = {
    propName: string;
    color: string;
    label?: string;
}

type PiechartOptions = {
    columns: PiechartPiece[];
    scale: "normal" | "log";
    radius: "normal" | "dynamic";
}
export interface PiechartConfig {
    type: 'piechart';
    lang: MessageRecordLang;
    propName: string; // shall be an empty string
    options: PiechartOptions;
}
```


### `TimeserieConfig`

This type is obsolete, should not be used anymore.


### `HTMLConfig`

This type of config leads to the input value being inserted as raw HTML. Allowing for arbitrary content to be displayed. Note that it uses the  *React* `dangerouslySetInnerHTML` special key and no attempt is made to sanitize this content.


```typescript
export interface HTMLConfig {
    type: 'html';
    lang: MessageRecordLang;
    propName: string;
    options: {};
}
```

### `TextConfig`

Insert a static text.

```typescript
type TextOptions = {
    text: string;
    level: "title" | "subtitle" | "normal";
    style: "normal" | "bold" | "italic" | "bold-italic";
}

export interface TextConfig {
    type: 'text';
    lang: MessageRecordLang;
    propName: string; // shall be an empty string
    options: TextOptions;
}
```