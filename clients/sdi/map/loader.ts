/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { Option, fromNullable, fromPredicate } from 'fp-ts/lib/Option';
import { Either } from 'fp-ts/lib/Either';
import { Extent } from 'ol/extent';

import {
    ILayerInfo,
    fetchIO,
    StreamingMetaIO,
    Inspire,
    FieldsDescriptorList,
    FeatureCollection,
} from '../source';
import { addLayer, addFeaturesToLayer } from './map';
import { getApiUrl, SyntheticLayerInfo } from '../app';
import { addStream, mapStream, pushStreamExtent } from '../geodata-stream';

import {
    fetchLayer,
    fetchLayerWithExtent,
    fetchDatasetMetadata,
} from '../source';

import { Collection, isNotNullNorUndefined } from '../util';
import { IMapViewData } from './index';

const logger = debug('sdi:map/loader');

type LayerUpdate = (
    state: Collection<FeatureCollection>
) => Collection<FeatureCollection>;
type DispatchLayer = (u: LayerUpdate) => void;

// type MetadataUpdate = (state: Collection<Inspire>) => Collection<Inspire>;
// type DispatchMetadata = (u: MetadataUpdate) => void;

type LoaderOptions = {
    mapName: string;
    getLayerData: (
        ressourceId: string
    ) => Either<string, Option<FeatureCollection>>;
    getSyntheticLayerInfo: (layerId: string) => Option<SyntheticLayerInfo>;
    dispatchLayer: DispatchLayer;
    getView: () => IMapViewData;
    markVisibilityPending?: (lid: string) => Set<string>;
};

export const getLoader = ({
    mapName,
    getLayerData,
    getSyntheticLayerInfo,
    dispatchLayer,
    getView,
    markVisibilityPending,
}: LoaderOptions) => {
    const observeView = (view: IMapViewData) =>
        fromNullable(view.extent).map(e => {
            mapStream(e, ({ uri, lid }, extent) => {
                loadLayerDataExtent(lid, uri, extent);
            });
        });

    const layerInRange = (info: ILayerInfo) => {
        const low = fromNullable(info.minZoom).getOrElse(0);
        const high = fromNullable(info.maxZoom).getOrElse(30);
        const zoom = getView().zoom;
        return info.visible && zoom > low && zoom < high;
    };

    const whenInRange = fromPredicate(layerInRange);

    const prepareStreamedLayer = (
        layerInfo: ILayerInfo,
        md: Inspire,
        fields: FieldsDescriptorList
    ) => {
        dispatchLayer(state => {
            if (!(md.uniqueResourceIdentifier in state)) {
                state[md.uniqueResourceIdentifier] = {
                    type: 'FeatureCollection',
                    fields,
                    features: [],
                };
            }
            return state;
        });
        if (layerInfo.visible && layerInRange(layerInfo)) {
            fromNullable(getView().extent).map(e => {
                loadLayerDataExtent(
                    layerInfo.id,
                    md.uniqueResourceIdentifier,
                    e
                );
            });
        } else if (markVisibilityPending !== undefined) {
            markVisibilityPending(layerInfo.id);
        }
    };

    const loadLayerWithMetadata = (layerInfo: ILayerInfo, md: Inspire) => {
        addLayer(
            mapName,
            () => getSyntheticLayerInfo(layerInfo.id),
            () => getLayerData(md.uniqueResourceIdentifier)
        );

        if (isNotNullNorUndefined(md.dataStreamUrl)) {
            addStream({
                uri: md.uniqueResourceIdentifier,
                lid: layerInfo.id,
            });
            return fetchIO(StreamingMetaIO, md.dataStreamUrl)
                .then(({ fields }) => {
                    prepareStreamedLayer(layerInfo, md, fields);
                })
                .catch(() => {
                    logger(
                        `[ERROR] failed to get fields for ${md.uniqueResourceIdentifier}`
                    );
                    prepareStreamedLayer(layerInfo, md, []);
                });
        } else {
            return loadLayerData(md.uniqueResourceIdentifier);
        }
    };

    const loadLayer = (layerInfo: ILayerInfo) =>
        fetchDatasetMetadata(getApiUrl(layerInfo.metadataId))
            .then(md => {
                // dispatchMetadata(state => {
                //     state[md.id] = md;
                //     return state;
                // });
                return loadLayerWithMetadata(layerInfo, md);
            })
            .catch(err =>
                logger(`Failed to load MD ${layerInfo.metadataId}: ${err}`)
            );

    const loadLayerDataExtent = (layerId: string, url: string, bbox: Extent) =>
        getSyntheticLayerInfo(layerId).map(({ info }) => {
            whenInRange(info).map(info => {
                pushStreamExtent(bbox, { lid: layerId, uri: url });
                fetchLayerWithExtent(url, bbox)
                    .then(layer => {
                        if (layer.features !== null) {
                            addFeaturesToLayer(mapName, info, layer.features);
                            dispatchLayer(state => {
                                if (url in state) {
                                    state[url].features = state[
                                        url
                                    ].features.concat(layer.features);
                                } else {
                                    state[url] = layer;
                                }
                                return state;
                            });
                        }
                    })
                    .catch(err => {
                        logger(
                            `Failed to load features at ${url} due to ${err}`
                        );
                        // dispatch('remote/errors', state => ({
                        //     ...state,
                        //     [url]: `${err}`,
                        // }));
                    });
            });
        });

    const loadLayerData = (url: string) => {
        logger(`loadLayerData(${url})`);
        return fetchLayer(url)
            .then(layer => {
                dispatchLayer(state => {
                    logger(`Put layer ${url} on state`);
                    state[url] = layer;
                    return state;
                });
            })
            .catch(_err => {
                // logger(`Failed to load layer at ${url} due to ${err}`);
                // dispatch('remote/errors', state => ({
                //     ...state,
                //     [url]: `${err}`,
                // }));
            });
    };

    return { loadLayerWithMetadata, loadLayer, observeView };
};

logger('loaded');
