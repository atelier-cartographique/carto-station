A tutorial on how to set up a CartoStation application
=======================================================

This is a tutorial about setting up a new application in CartoStation.

# Structure of an application

We want to create an application adequately named `tutorial`. Typically one starts by creating a `tutorial/src` folder that contains several subfolders as follows.


```bash
clients$ mkdir -p tutorial/src/shape tutorial/src/queries tutorial/src/events tutorial/src/components
```
You may also have a look at the README of the `clients` folder of CartoStation for more insights about the structure of the application.


At the root of the application, we add two files to allow npm and webpack to point to the application folder, respectively, `tsconfig.json` and `webpack.config.js`:

```
//tsconfig.json
{
    "extends": "../tsconfig-base.json",
    "compilerOptions": {
        "outDir": "js",
        "rootDirs": [
            "./src",
            "../"
        ]
    },
    "files": [
        "src/index.ts"
    ]
}
```

```js
//webpack.config.js
const { resolve } = require('path');
const configure = require('../webpack.base');
module.exports = configure(resolve(__dirname));
```


Finally, we add a style folder.

```bash
mkdir -p tutorial/style
touch tutorial/style/index.js
```

# Main function
We start by creating a `main` function in a `index.ts` file located at the root of the application.

```ts
// index.ts
export const main =
    () => {
        console.log('Hello World');
    };
```

But we need to add a route to let Django (NB: Django manages the backend of CartoStation) know that we've just added a new application to CartoStation. Let's add this route:

```py
#settings.py
CLIENTS =  [
    {'route':'tutorial'},
]
```

Now browse to http://localhost:8000/client/tutorial/ and you will see your first 'hello world' (in the web console)!

We will add the argument `SDI` to the `main` function in order to parse any argument that will be entered in the url. `SDI` is actually a dictionary that is built by Django and that is sent to any application (cfr `L24 of sdi/clients/templates/clients/app_index.html`).


```ts
// index.ts
export const main =
    (SDI: any) => {
        console.log(`Hello ${SDI.args}`);
    };
```

I assume your name is Alice. Now, just try http://localhost:8000/client/tutorial/Alice (look at the console).


# Add a shape to our application

The application is always characterized by its *internal state*, that is designed by the *shape*. Again, have a look at the README of the `clients` folder of CartoStation for more background about these notions.

In the main function, we will now create a initial state using the CartoStation shape by default, i.e., `defaultShape`.


```ts
// index.ts
import { source } from 'sdi/source';
import { IShape, configure, defaultShape } from 'sdi/shape';

export const main =
    (SDI: any) => {
        const initialState: IShape = {
            ...defaultShape(),
            'app/user': SDI.user,
        };

        const stateActions = source<IShape>([]);
        const store = stateActions(initialState);
        configure(store); // too much boilerplate?

    };
```

Now we have a small application that works and has a internal state!


# Application component

Now we will *really* display 'Hello world' in our page http://localhost:8000/client/tutorial/ !

To do this, we will add a render function that display something in our application.

Since we are in a event-based application, we also need to loop on the internal state. So we add a `loop` function that consistently looks at the internal state at the rate of 16 ms and that triggers the `render` function whenever the initial state is altered. Conventionally, we put this in a new `app.ts` file.

```ts
// app.ts
import { DIV } from 'sdi/components/elements';
import { loop } from 'sdi/app';

const render = () => DIV({ className: 'hello' }, 'Hello World!');

const effects = () => { };

export const app = loop('tutorial', render, effects);

```

```ts
// index.ts
        ...

        const start = app(store);
        start();
```

The web page at http://localhost:8000/client/tutorial/ should now display a nice Hello World message.


# Adding a item to the application

We will add *a list of things* to the application, namely a list of `item`.

First, we'll define a typescript interface for the item in a new file `components/item/index.ts`

```ts
// components/item/index.ts
export interface Item {
    name: string;
    content: string;
}
```

Secondly, we add a list of items in the shape of the application by creating a new file `shape/item.ts`. We also add a file `shape/index.ts` that simply re-export all the components that we will add in the future in the folder `shape`. So we can conveniently import the `shape/index` wherever needed in the application.

We add this new list of items as a key to the `initialState`, with an empty list as the value :

```ts
// index.ts

const initialState: IShape = {
    ...defaultShape(),
    'app/user': SDI.user,
    'component/item': []
};

```

Then, we add a query to, well, *query* the items from the internal state of the application.

```ts
// queries/item.ts
import { query } from 'sdi/shape';


export const getItems = () => query('component/items');

```


Similarly we add an event to *dispatch* the items to the internal state of the application. In this `addItem` function, we may add some logic, like here where duplicata are filtered out.

```ts
// events/item.ts
import { dispatch } from 'sdi/shape';
import { Item } from 'tutorial/src/components/item';


export const addItem =
    (i: Item) =>
        dispatch('component/items',
            xs => xs.filter(x => x.name !== i.name).concat([i]));

```

Finally we can add a render function in `components/item/index.ts` that will render the item list. We add a default item for initializing the item list.

```ts
// components/item/index.ts
export const defaultItem: Item = {
    name: 'Default',
    content: 'I am a default item!',
};


const renderItem =
    (i: Item) =>
        DIV({ className: 'ietm-item', key: i.name },
            H2({}, i.name),
            DIV({}, i.content));


export const render =
    () => DIV({ className: 'item-list' },
        getItems().map(renderItem));

export default render;
```



Some tricks:

* we use a default export for the render function of components/items (`export default render;`) so we can conveniently use the name of the component in the main function (`import { defaultItem } from './components/item/index';`).

* In `addItem`, we introduce some logic about our item list by avoiding some duplicata, the logic being written in the "filter".

Note that in the code extracts here, we did not always write down the import statements that are necessary. If there are no errors in the typescript compiler (import some modules where needed), the application should show a list with a default item:  http://localhost:8000/client/tutorial/


Now, let's add a simple form for adding a new item to our application. We will create some setter and getter functions for setting and getting a item from the application state. We start by adding a function for rendering the form and we use some utilities that were written for rendering forms in CartoStation (`sdi/components/input`).



```ts
// components/item/index.ts

const renderNewItem =
    () => DIV({ className: 'new-item' },
        inputText(getNewName, setNewName, { key: 'new-name' }),
        inputLongText(getNewContent, setNewContent, { key: 'new-content' }),
        DIV({
            className: 'submit-new',
            onClick: addNewItem,
        }, 'ADD'),
    );


```

Before writing the setter and getter functions, we need to add new application internal variables for the form. The rationale is that as soon as the user starts typing something in the input fields, these values must be stored. So we need to store the input values from the fields in some application variables.

So we add new variables in the shape and in the `initialState`. These variables are kind of intermediate state variables for forms.

```ts
// index.ts
    ...
    'component/items/new/name': null,
    'component/items/new/content': null,
```

```ts
// shape/item.ts
    ...
    'component/items/new/name': Nullable<string>;
    'component/items/new/content': Nullable<string>;
```

We can now write our setter and getter functions that we call in `renderNewItem`.


```ts
// events/item.ts
export const addNewItem =
    () =>
        scopeOption()
            .let('name', fromNullable(query('component/items/new/name')))
            .let('content', fromNullable(query('component/items/new/content')))
            .map(({ name, content }) => addItem({ name, content }))
            .map(() => {
                assign('component/items/new/name', null);
                assign('component/items/new/content', null);
            });


export const setNewName =
    (name: string) => assign('component/items/new/name', name);

export const setNewContent =
    (content: string) => assign('component/items/new/content', content);

```

```ts
// queries/item.ts
export const getNewName =
    () => fromNullable(query('component/items/new/name')).getOrElse('');

export const getNewContent =
    () => fromNullable(query('component/items/new/content')).getOrElse('');
```

It works! Add some items in your page and try to add an item whose the key already exist in the list (a duplicata): http://localhost:8000/client/tutorial/.



In case we need a placeholder in the form, we can make something a little bit more sophisticated:


```ts
// components/item/index.ts

const renderNewItem =
    () => DIV({ className: 'new-item' },
        getNewName().foldL(
            () => inputText(() => '', setNewName, { key: 'new-name', placeholder: 'A name' }),
            newName => inputText(() => newName, setNewName, { key: 'new-name' }),
        ),
        inputLongText(getNewContent, setNewContent, { key: 'new-content' }),
        DIV({
            className: 'submit-new',
            onClick: addNewItem,
        }, 'ADD'),
    );
```


# Internationalization

How to manage multi-languages in the application?

We add a new file `locale/index.ts` and we define a namespace "tuto" for our specific application.

```ts
// locale/index.ts
import { MessageStore, fromRecord } from 'sdi/locale';

const messages = {
    addItem: {
        fr: 'ajouter un article',
        en: 'Add an item',
    },
};

type MDB = typeof messages;
export type TutoMessageKey = keyof MDB;


declare module 'sdi/locale' {
    export interface MessageStore {
        tuto(k: TutoMessageKey): Translated;
    }
}

MessageStore.prototype.tuto = (k: TutoMessageKey) => fromRecord(messages[k]);

```

When we need to internationalize a message in our application, we use the function `tr` from `sdi/locale`. For a message that is available at the core of the SDI, we can use `tr.core`, otherwise, we'll use `tr.tuto`, e.g., `tr.tuto('addItem')`.


# Routing

We will make two pages in our application. We define a new application key conventionally named `app/layout`.

Let's do it in the shape of the application and in the `initialState`.

```ts
//shape/app.ts
    ...
    'app/layout': Layout;
    'app/selectd-item': Nullable<string>;
```

```ts
// index.ts
    ...
    'app/layout': 'list',
    'app/': null,
```

We create two new files in the components/item folder instead of `components/item/index.ts`: `single.ts` and `list.ts`.

```ts
// components/item/single.ts
import { DIV, H2 } from 'sdi/components/elements';
import { Item } from './index';
import tr from 'sdi/locale';
import { getCurrentItem } from 'tutorial/src/queries/app';



const renderItem =
    (i: Item) =>
        DIV({ className: 'ietm-item', key: i.name },
            H2({}, i.name),
            DIV({}, i.content));

const renderNoItem =
    () => DIV({ className: 'error' }, tr.tuto('noItemError'));


export const render =
    () => DIV({ className: 'item-list' },
        getCurrentItem()
            .foldL(
                renderNoItem,
                renderItem),
    );

export default render;

```


```ts
// components/item/list.ts
import { DIV, H2 } from 'sdi/components/elements';
import { inputLongText, inputText } from 'sdi/components/input';
import { getItems, getNewName, getNewContent } from 'tutorial/src/queries/item';
import { setNewName, setNewContent, addNewItem, selectItem } from 'tutorial/src/events/item';

import { tr } from 'sdi/locale';
import { Item } from './index';



const renderNewItem =
    () => DIV({ className: 'new-item' },
        getNewName().foldL(
            () => inputText(() => '', setNewName, { key: 'new-name', placeholder: 'A name' }),
            newName => inputText(() => newName, setNewName, { key: 'new-name' }),
        ),
        inputLongText(getNewContent, setNewContent, { key: 'new-content' }),
        DIV({
            className: 'submit-new',
            onClick: addNewItem,
        }, tr.tuto('addItem')),
    );


const renderItem =
    (i: Item) =>
        DIV({ className: 'ietm-item', key: i.name },
            H2({
                onClick: () => selectItem(i.name),
            }, i.name));


export const render =
    () => DIV({ className: 'item-list' },
        getItems().map(renderItem),
        renderNewItem(),
    );

export default render;

```

We define a new query for getting the layout.

```ts
// queries/app.ts
import { query } from 'sdi/shape';
import { fromNullable } from 'fp-ts/lib/Option';
import { Item } from 'tutorial/src/components/item';


export const getLayout =
    () => query('app/layout');

```

We need to define a state variable `app/selected-item` that will be used when we select an item. The page `single` will display the selected item with the function `getCurrentItem`. This function queries the internal state variable `selected-item`.


```ts
// queries/app.ts

const findItem =
    (name: string, xs: Item[]) => xs.find(x => x.name === name);


export const getCurrentItem =
    () =>
        fromNullable(
            query('app/selectd-item'))
            .chain(name => fromNullable(findItem(name, query('component/items'))));

```

And to select the item, we write this function that dispatch the value of the selected item to the application state variable `app/selected-item` and change the application layout at the same time.

```ts
// events/item.ts

export const selectItem =
    (name: string) => {
        assign('app/selectd-item', name);
        assign('app/layout', 'single');
    };

```

Test the application at http://localhost:8000/client/tutorial/ to see the list (by default). Add some item and click on one item to see the single application layout. Note that we have now some pages but without any navigation: the url does not change when moving from one layout to another.


Now we will really add some navigation in the app.

We add a file `events/route.ts` that uses `sdi/router`. In this file, we define some callbacks function for each route (`list` and `single`).


```ts
// events/route.ts
import * as debug from 'debug';

import { Router } from 'sdi/router';

import { setLayout } from './app';
import { assign } from 'sdi/shape';
import { selectItem } from './item';

const logger = debug('sdi:route');

type Route = 'list' | 'single';

const { route, navigate } = Router<Route>('tuto');


route('list', () => {
    assign('app/selectd-item', null);
    setLayout('list');
});

route('single',
    (r) => {
        if (r.length > 0) {
            const name = r[0];
            selectItem(name);
        }
    });

export const navigateList =
    () => navigate('list', []);


export const navigateItem =
    (name: string) => navigate('single', [name]);


logger('loaded');
```

We also need to add a internal state variable `app/route` in the `initialState` to define the root of the application.


# Communicate with the server

Here we will fetch some user data from the server. Conventionally, we set up a `remote/index.ts` file for everything to do with communication with the server. We will create a function to get some user data based on some user id. Then, we will also create a internal state variable of user data (`data/user`) in the shape and in the `initialState`.

```ts
// remote/index.ts
import {
    fetchIO,
    IUser,
    IUserIO,
} from 'sdi/source';


export const fetchUser =
    (url: string): Promise<IUser> =>
        fetchIO(IUserIO, url);
```

Typically, we will use an `effects` function to load some user data


```ts
// app.ts
const effects = () => {
    getUserId()
        .map(userId => fetchUser(getApiUrl(`users/${userId}`))
            .then(setUserData));
};
```
with

```ts
// events/app.ts
export const setUserData =
    (u: IUser) => assign('data/user', u);
```


Finally, we will use this to render some user data in the application.

```ts
// components/list.ts

DIV({ className: 'greetings' }, getUsername().fold(
    NODISPLAY(),
    username => SPAN({}, `Hello ${username}`),
)),
```

```ts
// queries/app.ts

export const getUsername =
    () => fromNullable(query('data/user')).map(u => u.name);
```
