import { baseTable, Config } from 'sdi/components/table';
import { DIV, SPAN } from 'sdi/components/elements';

import { tableUserQueries } from '../queries/table';
import { tableUserEvents } from '../events/table';
import { fromRecord } from 'sdi/locale';
import { MessageRecord } from 'sdi/source';
import { Route, navigate } from '../events/route';
import { getLinks } from '../queries/navigate';

const tableConfig: Config = {
    className: 'table--',
};

const table = baseTable(tableUserQueries, tableUserEvents)(tableConfig);

const renderLink = (name: MessageRecord, onClick: () => void) =>
    DIV({ onClick, className: 'tuto-links' }, SPAN({}, fromRecord(name)));

const goTo = (r: Route) => () => navigate(r, []);

const render = () =>
    DIV(
        { className: 'main-app interface' },
        ...getLinks().map(([n, r]) => renderLink(n, goTo(r))),
        DIV({}, table())
    );

export default render;
