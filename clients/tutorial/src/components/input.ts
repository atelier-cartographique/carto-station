import { none } from 'fp-ts/lib/Option';
import { Setoid } from 'fp-ts/lib/Setoid';
import { A, DIV, H2, SPAN } from 'sdi/components/elements';
import {
    attrOptions,
    inputLongText,
    inputText,
    renderSelect,
} from 'sdi/components/input';
import { fromRecord } from 'sdi/locale';
import { IUser, MessageRecord } from 'sdi/source';
import { setInputText } from '../events/input';
import { Route, navigate } from '../events/route';
import { getInputText } from '../queries/input';
import { getLinks } from '../queries/navigate';
import { getUsers } from '../queries/table';

const renderText = () =>
    inputText(
        attrOptions(`input-text-example`, getInputText, setInputText, {
            placeholder: 'put your text here',
        })
    );

const renderTextLong = () =>
    inputLongText(getInputText, setInputText, {
        placeholder: 'put your text here',
    });

const text = () =>
    DIV({}, H2({}, 'Text Inputs'), renderText(), renderTextLong());

const renderUser = (u: IUser) => u.name;

const selectUser = (u: IUser) => setInputText(`selected user => ${u.name}`);

export const setoidUser: Setoid<IUser> = {
    equals: (a, b) => a.id === b.id,
};

const renderInputUsers = renderSelect(
    'input-users',
    renderUser,
    selectUser,
    setoidUser
);

const renderLink = (name: MessageRecord, onClick: () => void) =>
    DIV({ onClick, className: 'tuto-links' }, SPAN({}, fromRecord(name)));

const goTo = (r: Route) => () => navigate(r, []);

const render = () =>
    DIV(
        { className: 'main-app interface' },
        ...getLinks().map(([n, r]) => renderLink(n, goTo(r))),
        DIV(
            {},
            text(),
            A({ href: getInputText() }, getInputText()),
            renderInputUsers(getUsers(), none)
        )
    );

export default render;
