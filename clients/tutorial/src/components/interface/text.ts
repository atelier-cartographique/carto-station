import { DIV, H1, H2, H3, H4, H5, H6 } from 'sdi/components/elements';
import { markdown } from 'sdi/ports/marked';

const componentLabel = (label: string) => DIV({}, label);

const title1 = DIV(
    {},
    componentLabel('headline 1'),
    H1({}, 'The quick brown fox jumps over the lazy dog')
);
const title2 = DIV(
    {},
    componentLabel('headline 2'),
    H2({}, 'The quick brown fox jumps over the lazy dog')
);
const title3 = DIV(
    {},
    componentLabel('headline 3'),
    H3({}, 'The quick brown fox jumps over the lazy dog')
);
const title4 = DIV(
    {},
    componentLabel('headline 4'),
    H4({}, 'The quick brown fox jumps over the lazy dog')
);
const title5 = DIV(
    {},
    componentLabel('headline 5'),
    H5({}, 'The quick brown fox jumps over the lazy dog')
);
const title6 = DIV(
    {},
    componentLabel('headline 6'),
    H6({}, 'The quick brown fox jumps over the lazy dog')
);

const mdText = DIV(
    {},
    componentLabel('markdown text'),
    markdown(`Similique [https://atelier-cartographique.be](dignissimos) nisi **earum aut** distinctio. Veniam commodi vel adipisci ducimus natus. Nulla ut impedit animi eligendi qui eos. Perferendis vitae inventore dolor exercitationem.

Asperiores illo *similique* ipsam architecto ut facere. Dicta et voluptatum aliquam porro voluptatem. Dolorum ut officiis temporibus totam eum alias eum. Quo in atque qui. Enim autem aperiam adipisci enim doloribus qui nihil provident.`)
);

const helpText = DIV(
    {},
    componentLabel('Helptext'),
    DIV(
        { className: 'helptext' },
        `Asperiores illo *similique* ipsam architecto ut facere. Dicta et voluptatum aliquam porro voluptatem. Dolorum ut officiis temporibus totam eum alias eum. Quo in atque qui. Enim autem aperiam adipisci enim doloribus qui nihil provident.`
    )
);

const render = () =>
    DIV(
        { className: 'interface-text' },
        title1,
        title2,
        title3,
        title4,
        title5,
        title6,
        mdText,
        helpText
    );

export default render;
