import { makeIcon, makeLabel, makeLabelAndIcon } from 'sdi/components/button';
import { DIV, H2, NodeOrOptional, SECTION } from 'sdi/components/elements';
import tr, { Translated } from 'sdi/locale';

const noop = () => {};

const wrapper = (name: string, ...nodes: NodeOrOptional[]) =>
    SECTION(
        // name.toLowerCase().replace(/\s+/g, '-'),
        'component__wrapper',
        H2('component__title', name),
        DIV('component', ...nodes)
    );

export const btn1 = () =>
    wrapper(
        'Buttons level 1',
        makeLabelAndIcon('edit', 1, 'pencil-alt', () => tr.core('edit'))(
            noop,
            'inactive'
        ),
        makeLabelAndIcon('edit', 1, 'pencil-alt', () => tr.core('edit'))(noop),
        makeLabel('edit', 1, () => tr.core('edit'))(noop),
        makeIcon('edit', 1, 'pencil-alt', {
            position: 'bottom',
            text: 'Do nothing' as Translated,
        })(noop)
    );

export const btn2 = () =>
    wrapper(
        'Buttons level 2',
        makeLabelAndIcon('edit', 2, 'pencil-alt', () => tr.core('edit'))(
            noop,
            'inactive'
        ),
        makeLabelAndIcon('edit', 2, 'pencil-alt', () => tr.core('edit'))(noop),
        makeLabel('edit', 2, () => tr.core('edit'))(noop),
        makeIcon('edit', 2, 'pencil-alt', {
            position: 'right',
            text: 'Do nothing' as Translated,
        })(noop)
    );

export const btn3 = () =>
    wrapper(
        'Buttons level 3',
        makeLabelAndIcon('edit', 3, 'pencil-alt', () => tr.core('edit'))(
            noop,
            'inactive'
        ),
        makeLabelAndIcon('edit', 3, 'pencil-alt', () => tr.core('edit'))(noop),
        makeLabel('edit', 3, () => tr.core('edit'))(noop),
        makeIcon('edit', 3, 'pencil-alt', {
            position: 'top-left',
            text: 'Do nothing' as Translated,
        })(noop)
    );
