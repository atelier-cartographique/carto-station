declare module 'sdi/shape' {
    export interface IShape {
        'component/input/text': string;
        'component/input/date': Date;
        'component/input/number': number;
    }
}

export const defaultInputShape = () => ({
    'component/input/text': '',
    'component/input/number': 232,
    'component/input/date': new Date(),
});
