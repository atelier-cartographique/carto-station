import { queryK } from 'sdi/shape';

export const getInputText = queryK('component/input/text');
export const getInputDate = queryK('component/input/number');
export const getInputNumber = queryK('component/input/number');
