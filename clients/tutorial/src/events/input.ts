import { assignK } from 'sdi/shape';

export const setInputText = assignK('component/input/text');
export const setInputDate = assignK('component/input/date');
export const setInputNumber = assignK('component/input/number');
