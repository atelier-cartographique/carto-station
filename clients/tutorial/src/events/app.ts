import { assign } from 'sdi/shape';
import { Layout } from './route';

export const setLayout = (l: Layout) => assign('app/layout', l);

export const setAppName = (n: string) => assign('app/name', n);
