import * as debug from 'debug';

import { literal, TypeOf, union } from 'io-ts';
import { fromNullable } from 'fp-ts/lib/Option';
import { index } from 'fp-ts/lib/Array';

import { Router, Path } from 'sdi/router';
import { queryK, dispatchK, dispatch } from 'sdi/shape';
import { scopeOption } from 'sdi/lib';
import { getRootUrl } from 'sdi/app';

import { AppLayout } from '../shape/types';
import { clearMap, loadMap } from './app';
import { viewEvents } from './map';
import { prepareGeometryForHarvesting } from './harvest';
import { getCurrentMap } from '../queries/app';

const logger = debug('sdi:route');

//new elem to place in other files
const setLayout = (l: Layout) => {
    //todo
};

// New Route system
const RouteIO = union([literal('home'), literal('map')]);
export type Route = TypeOf<typeof RouteIO>;
export type Layout = Route;
export const { home, route, navigate } = Router<Route>('view');

const baseRouteParser = (p: Path) => index(0, p).map(mapId => ({ mapId }));
const featureRouteParser = (p: Path) => scopeOption();

home('index', () => setLayout('home'));
route(
    'map',
    route => {
        route.map(({ mapId }) => {
            clearMap();
            dispatch('app/current-map', () => mapId);
            loadMap();
            prepareGeometryForHarvesting();
            setLayout('map');
        });
    },
    baseRouteParser
);
route('feature', route => {
    route.map();
});

const defaultRoute = () => navigate('index', []);

export const loadRoute = (initial: string[]) =>
    index(0, initial).foldL(defaultRoute, prefix =>
        RouteIO.decode(prefix).fold(defaultRoute, c =>
            navigate(c, initial.slice(1))
        )
    );
export const navigateIndex = () => {
    navigate('index', []);
};

export const navigateMap = (mapId: string) => {
    navigate('map', [mapId]);
};

export const navigateFeature = (lid: string, fid: number) => {
    getCurrentMap().map(mid => navigate('feature', [mid, lid, fid]));
};

// Old system:

// const hasHistory =
//     typeof window !== 'undefined' &&
//     window.history &&
//     window.history.pushState;
// type historyStateKind = 'home' | 'map';
// interface HistoryState {
//     kind: historyStateKind;
//     route: string[];
// }

// const getRoute = queryK('app/route');
// const setRoute = dispatchK('app/route');

// const cleanRoute = () =>
//     getRoute().reduce((acc, s) => {
//         if (s.length > 0) {
//             return acc.concat([s]);
//         }
//         return acc;
//     }, [] as string[]);

// const getNumber = (s?: string) => {
//     if (s) {
//         const n = parseFloat(s);
//         if (!Number.isNaN(n)) {
//             return n;
//         }
//     }
//     return null;
// };

// const setMapView = () => {
//     const r = cleanRoute();
//     scopeOption()
//         .let('lat', fromNullable(getNumber(r[1])))
//         .let('lon', fromNullable(getNumber(r[2])))
//         .let('zoom', fromNullable(getNumber(r[3])))
//         .map(({ lat, lon, zoom }) => {
//             viewEvents.updateMapView({
//                 dirty: 'geo',
//                 center: [lat, lon],
//                 zoom,
//             });
//         });
// };

// export const navigate = () => {
// const r = cleanRoute();
// if (r.length > 0) {
//     clearMap();
//     const mapId = r[0];
//     dispatch('app/current-map', () => mapId);
//     loadMap();
//     prepareGeometryForHarvesting();
//     setLayout(AppLayout.MapAndInfo);
//     setMapView();
// }
// else {
//     setLayout(AppLayout.MapNavigatorFS);
// }
// };

// const pushMap = (mid: string) => {
//     if (hasHistory) {
//         const s: HistoryState = {
//             kind: 'map',
//             route: [mid],
//         };

//         window.history.pushState(s, `View - ${mid}`, getRootUrl(`view/${mid}`));
//     }
// };

// const pushHome = () => {
//     if (hasHistory) {
//         const s: HistoryState = {
//             kind: 'home',
//             route: [],
//         };

//         window.history.pushState(s, `View - Atlas`, getRootUrl('view/'));
//     }
// };

// export const navigateHome = () => {
//     setRoute(() => []);
//     navigate();
//     pushHome();
// };

// export const navigateMap = (mid: string) => {
//     setRoute(() => [mid]);
//     navigate();
//     pushMap(mid);
// };

// (function () {
//     if (hasHistory) {
//         window.onpopstate = (event: PopStateEvent) => {
//             const s = event.state as HistoryState;
//             switch (s.kind) {
//                 case 'home':
//                     setRoute(() => s.route);
//                     break;
//                 case 'map':
//                     setRoute(() => s.route);
//                     break;
//             }
//             navigate();
//         };
//     }
// })();

logger('loaded');
