// /*
//  *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
//  *
//  *  This program is free software: you can redistribute it and/or modify
//  *  it under the terms of the GNU General Public License as published by
//  *  the Free Software Foundation, version 3 of the License.
//  *
//  *  This program is distributed in the hope that it will be useful,
//  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  *  GNU General Public License for more details.
//  *
//  *  You should have received a copy of the GNU General Public License
//  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//  */

// import * as debug from 'debug';
// import { setMapBaseLayer } from '../../events/app';
// import { getCurrentBaseLayerName } from '../../queries/app';
// import { NodeOrOptional } from '../../../../sdi/components/elements';
// import { getHighlightedBaseLayers } from '../../queries/map';
// import baseLayerSwitch from 'sdi/map/base-layer-switch';

// const logger = debug('sdi:map-info/base-layer-switch');

// const render = (): NodeOrOptional =>
//     baseLayerSwitch(
//         getHighlightedBaseLayers(),
//         getCurrentBaseLayerName(),
//         setMapBaseLayer
//     );

// // {
// //     const baseLayers = getHighlightedBaseLayers();
// //     const len = baseLayers.length;
// //     const current = getCurrentBaseLayerName();
// //     if (current) {
// //         if (len === 2) {
// //             const bl1 = baseLayers[0];
// //             const bl2 = baseLayers[1];

// //             if (bl1.codename === current) {
// //                 return buttonTooltipLeft(
// //                     tr.core('changeBackgroundMap'),
// //                     {
// //                         className: 'switch-background',
// //                         onClick: () => setMapBaseLayer(bl2.codename),
// //                     },
// //                     IMG({
// //                         title: tr.core('changeBackgroundMap'),
// //                         src: bl2.img,
// //                     })
// //                 ) as NodeOrOptional;
// //             }

// //             return buttonTooltipLeft(
// //                 tr.core('changeBackgroundMap'),
// //                 {
// //                     className: 'switch-background',
// //                     onClick: () => setMapBaseLayer(bl1.codename),
// //                 },
// //                 IMG({
// //                     title: tr.core('changeBackgroundMap'),
// //                     src: bl1.img,
// //                 })
// //             ) as NodeOrOptional;
// //         }
// //     }
// //     return DIV({}, 'baseless');
// // };

// export default render;

// logger('loaded');
