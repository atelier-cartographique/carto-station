import { DIV } from 'sdi/components/elements';
import sidebar from './sidebar';
import navigator from './navigator';

export const renderHome = () => DIV('map-navigator', sidebar(), navigator());

export default renderHome;
