Geothermie
----------


# Documentation

## Constants by geological layers

There are a lot of numeric or text constants defined for each geological layers. There are mainly used in the tables, graph and the carrot.  Here we describe how they are handled in the application.

By geological layers, we mean:

* 19 geological layers, aka "Unité Stratigraphique (US)", such as 'US/RBC_1', 'US/RBC_21', ...
* 4 geological layers for the Quaternaire layer, namely 'US/RBC_11', 'US/RBC_12', 'US/RBC_13', 'US/RBC_14'.
* 14 hydrogeological layers, aka "Unité hydrogéologique (UH)", such as 'UH/RBC_1a', 'UH/RBC_1b', ...
* 6 piezometric layers, which are a subset of hydrogeologic layers where there is a piezometric data

The mapping between geological and hydrogeological layers can be found in `getHydroLayersThicknesses`.

### Numeric constants

Numeric constants are defined as array of numbers in `components/settings/index.ts`, for each geological layer.

### Textual constants

Textual constants needs to be translated. As a result, they are defined in the `messages` object of `locale/index.ts`. The keys of these constants are dynamically built with the `addSuffixToMessageKey` function. The suffix needs to be defined as a key of the `messages` object.
