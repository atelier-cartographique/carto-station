import { Nullable, Collection } from 'sdi/util';

import {
    System,
    DisplayMapsSeparator,
    SearchType,
    InfoPanel,
} from '../components/geothermie';
import { XY, MultilingualAddress } from '../components/geocoder';
import { GraphXY } from '../components/table/graph';
import { DepthTile, DepthPoint, ConstraintPoint } from '../remote/io';
import { CakeAlpha, CakeMode, CakeRes } from '../components/cake';
import { MoreTextHidden, Depth } from '../components/table/format';
import { ConnectData, ConnectName } from '../components/table/connect';
import {
    RemoteResource,
    FeatureCollection,
    remoteNone,
    Feature,
    LineString,
} from 'sdi/source';
import { GeoMessageKey } from '../locale';
import {
    InfoStrati,
    InfoOpen,
    InfoHydroGeol,
    InfoClosed,
    InfoBulle,
} from '../remote/info';
import { FinanialDataRow } from '../components/table/table-financial';
import { Coordinate } from 'ol/coordinate';
import { BottomRender } from '../queries/geothermie';
import { SliceData } from '../components/slice';
import { DisplayPiezo } from '../components/slice/legend';

declare module 'sdi/shape' {
    export interface IShape {
        'geo/name': string;
        'geo/xy': Nullable<XY>;
        'geo/address': Nullable<MultilingualAddress>;
        'geo/system': Nullable<System>;

        'geo/bottom-render': Nullable<BottomRender>;
        'geo/depth-scaling': number;

        'geo/cake/mode': CakeMode;
        'geo/cake/res': CakeRes;
        'geo/cake/geometry': Nullable<Feature>;
        'geo/cake/quat-alpha': CakeAlpha;

        'geo/slice/res': number;
        'geo/slice/geometry': Nullable<LineString>;
        'geo/slice': RemoteResource<DepthPoint[]>;
        'geo/slice/data': SliceData;
        'geo/slice/piezo': DisplayPiezo;
        'geo/slice/phreatic': boolean;

        'geo/data/point': RemoteResource<DepthPoint>;
        'geo/data/constraint': RemoteResource<ConstraintPoint>;

        'geo/map/top': Nullable<string>;
        'geo/map/bottom': Nullable<string>;

        'geo/display/maps/separator': DisplayMapsSeparator;
        'geo/display/dataswitch': InfoPanel[];
        'geo/tiles': Collection<DepthTile>;

        'geo/graph/xy': Nullable<GraphXY>;
        'geo/graph/snap-xy': Nullable<GraphXY>;

        'geo/diagram/height': number;
        'geo/diagram/graph-width': number;
        'geo/diagram/carrot-width': number;

        'geo/table/show/geol': boolean;
        'geo/table/show/hydro': boolean;
        'geo/table/text-hidden': MoreTextHidden;

        'geo/depth/mode': Depth;

        'geo/finance/show/thermic/needs': boolean;
        'geo/finance/table/data': FinanialDataRow[];

        'geo/legend/group/visibility': Collection<boolean>;
        'geo/legend/group/folding': Collection<boolean>;
        'geo/legend/group/bulle': Collection<boolean>;
        'geo/legend/layer/info': Collection<boolean>;
        'geo/legend/wms-visible': boolean;

        'geo/connect': Collection<ConnectData, ConnectName>;

        'geo/search/type': SearchType;
        'geo/search/capakey': Nullable<string>;
        'geo/search/capakey/result': RemoteResource<FeatureCollection>;
        'geo/search/coordinates': Nullable<string>;

        'geo/map/interaction/pin': boolean;
        'geo/map/marker': Nullable<Coordinate>;
        'geo/map/measure/box': boolean;
        'geo/map/button/helptext': boolean;

        'geo/capakey/current': RemoteResource<FeatureCollection>;

        'geo/form/depth': Nullable<number>;
        'geo/form/conductivity': Nullable<number>;
        'geo/form/uh': Nullable<GeoMessageKey>;

        'geo/info/strati': InfoStrati[];
        'geo/info/open': InfoOpen[];
        'geo/info/hgeol': InfoHydroGeol[];
        'geo/info/closed': InfoClosed[];

        'geo/bulles': InfoBulle[];

        'geo/geological-group-id': string;
    }
}

export const defaultGeothermieShape = () => ({
    'geo/name': 'Brugeotool',
    'geo/xy': null,
    'geo/address': null,
    'geo/system': 'close' as System,

    'geo/bottom-render': null,
    'geo/depth-scaling': 0.5,

    'geo/cake/mode': 'double-cut' as CakeMode,
    'geo/cake/res': 64 as CakeRes,
    'geo/cake/geometry': null,
    'geo/cake/quat-alpha': 'FC' as CakeAlpha,

    'geo/data/point': remoteNone,
    'geo/data/constraint': remoteNone,

    'geo/slice/res': 128,
    'geo/slice/geometry': null,
    'geo/slice': remoteNone,
    'geo/slice/data': 'geol' as SliceData,
    'geo/slice/piezo': {
        'UH/RBC_1b': false,
        'UH/RBC_2': false,
        'UH/RBC_4': false,
        'UH/RBC_6': false,
        'UH/RBC_7b': false,
        'UH/RBC_8a': false,
    } as DisplayPiezo,
    'geo/slice/phreatic': true,

    'geo/map/top': null,
    'geo/map/bottom': null,

    'geo/display/maps/separator': 'bottom-hidden' as DisplayMapsSeparator,
    'geo/display/dataswitch': ['presentation' as InfoPanel],
    'geo/tiles': {},

    'geo/graph/xy': null,
    'geo/graph/snap-xy': null,

    'geo/diagram/height': 10, // px
    'geo/diagram/graph-width': 200, // px
    'geo/diagram/carrot-width': 260, // px

    'geo/table/show/geol': false,
    'geo/table/show/hydro': false,
    'geo/table/text-hidden': {
        detailledLithology: Array(100).fill(true),
        remarks: Array(100).fill(true),
    } as MoreTextHidden,

    'geo/depth/mode': 'relative' as Depth,

    'geo/finance/show/thermic/needs': false,
    'geo/finance/table/data': [],

    'geo/legend/group/visibility': {},
    'geo/legend/group/folding': {},
    'geo/legend/group/bulle': {},
    'geo/legend/layer/info': {},
    'geo/legend/wms-visible': false,

    'geo/connect': {},

    'geo/search/type': 'geocoder' as SearchType,
    'geo/search/capakey': null,
    'geo/search/capakey/result': remoteNone,
    'geo/search/coordinates': null,

    'geo/map/interaction/pin': false,
    'geo/map/marker': null,
    'geo/map/measure/box': false,
    'geo/map/button/helptext': false,

    'geo/capakey/current': remoteNone,

    'geo/form/depth': null,
    'geo/form/conductivity': null,
    'geo/form/uh': null,

    'geo/info/strati': [],
    'geo/info/open': [],
    'geo/info/hgeol': [],
    'geo/info/closed': [],

    'geo/bulles': [],

    // at some point it shall come from the api, but tbh it's very stable, should work for a long time. -pm
    'geo/geological-group-id': '7a2b53d2-770b-46a4-b2f6-79bbbc68d783',
});
