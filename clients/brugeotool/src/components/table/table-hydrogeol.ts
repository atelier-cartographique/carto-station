import {
    DIV,
    TABLE,
    TBODY,
    THEAD,
    TH,
    TR,
    TD,
    NODISPLAY,
} from 'sdi/components/elements';
import tr from 'sdi/locale';

import {
    getLayersThicknesses,
    getQuaternaireThicknesses,
    getShowPartTableGeol,
    getShowPartTableHydro,
    getHydroLayerId,
    getKMin,
    getKMean,
    getKMax,
    // getHydroSystemLayerId,
    getGeologicalEtageLayerId,
    getGeologicalSerieLayerId,
    getGeologicalSystemLayerId,
    getGeologicalEraLayerId,
    quaternaireLayerCount,
    isUS13,
    countLayersUntilLayerName,
    getLayerId,
} from '../../queries/geothermie';
import {
    addSuffixToMessageKey,
    STRATIGRAPHY_UNITS_NAME,
    Q_STRATIGRAPHY_UNITS_NAME,
    STRATIGRAPHY_COLORS,
    HYDROGEOLOGY_UNITS_NAME,
    UH_GREY_SHADE,
    UH_FOR_US_ID,
    // HYDROLOGICAL_SYSTEM_NAME_FOR_US_ID,
    HYDROLOGICAL_SYSTEM_NAME,
    GEOLOGICAL_ETAGE_NAME_FOR_US_ID,
    GEOLOGICAL_SERIE_NAME_FOR_US_ID,
    GEOLOGICAL_SYSTEM_NAME_FOR_US_ID,
    GEOLOGICAL_ERA_NAME_FOR_US_ID,
    GEOLOGICAL_ETAGE_LAYERS_NAME,
    GEOLOGICAL_SERIE_LAYERS_NAME,
    GEOLOGICAL_SYSTEM_LAYERS_NAME,
    GEOLOGICAL_ERA_LAYERS_NAME,
    QUATERNARY_COLORS,
} from '../settings';

import {
    renderNoTableLine,
    // computeLineHeight,
    // computeQLineHeight,
    getLayersDepths,
    keyAndTrad,
    renderMoreText,
    formatDepth,
    computeSmartGeolLineHeight,
    MIN_LINE_HEIGHT,
} from './format';
import { renderDepthTitle, renderDepthCol } from './table-depth';
import {
    changeShowPartTableGeol,
    changeShowPartTableHydro,
    updateConnect,
} from 'brugeotool/src/events/geothermie';
import { makeLabelAndIcon } from 'sdi/components/button';
import { GeoMessageKey } from '../../locale';
import { rect } from 'sdi/app';
import { markdown } from 'sdi/ports/marked';
import { fromNullable } from 'fp-ts/lib/Option';

const getUS = () => STRATIGRAPHY_UNITS_NAME;
const getUH = () => HYDROGEOLOGY_UNITS_NAME;

const getGeologicalEra = () => GEOLOGICAL_ERA_LAYERS_NAME.map(u => tr.geo(u));

const getGeologicalSystem = () =>
    GEOLOGICAL_SYSTEM_LAYERS_NAME.map(u => tr.geo(u));

const getGeologicalEtage = () =>
    GEOLOGICAL_ETAGE_LAYERS_NAME.map(u => tr.geo(u));

const getGeologicalSerie = () =>
    GEOLOGICAL_SERIE_LAYERS_NAME.map(u => tr.geo(u));

const getUHClassName = () => UH_GREY_SHADE;

const getHydroState = () =>
    addSuffixToMessageKey(HYDROGEOLOGY_UNITS_NAME, '-hydro_state').map(u =>
        tr.geo(u)
    );

const getHydroSystem = () =>
    HYDROLOGICAL_SYSTEM_NAME.map(u => DIV({}, tr.geo(u)));

const getQuaternaireName = () => Q_STRATIGRAPHY_UNITS_NAME.map(u => tr.geo(u)); // todo fix me utiliser plutôt : keyAndTrad(u));

const getDetailedLithology = () =>
    addSuffixToMessageKey(STRATIGRAPHY_UNITS_NAME, '-litho').map(u =>
        tr.geo(u)
    );

const getDetailedLithologyQuaternaire = () =>
    addSuffixToMessageKey(Q_STRATIGRAPHY_UNITS_NAME, '-litho').map(u =>
        tr.geo(u)
    );

// FIXTURES
// const getLayersThicknesses = () => [9.331809997558594, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.7692070007324219, 3.0461788177490234, 27.77418327331543, 45.48012161254883, 13.581069946289062, 19.563568115234375, 11.043914794921875] // for debug when no connection
// const getLayersSat = () => [0.3, 0, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.8, 1, 1, 1, 1, 1, 1, 1]; // (fixture) get real data from x,y
// const getQuaternaireThicknesses = () => [0.2, 0.3, 0.4, 0.1]; // (fixture)

// Render of tables

const renderShowGeolButton = () =>
    DIV(
        { className: 'toggle-btn--wrapper' },
        getShowPartTableGeol()
            ? makeLabelAndIcon('toggle-on', 3, 'minus', () => tr.geo('info'))(
                  changeShowPartTableGeol
              )
            : makeLabelAndIcon('toggle-off', 3, 'plus', () => tr.geo('info'))(
                  changeShowPartTableGeol
              )
    );

const renderShowHydroButton = () =>
    DIV(
        { className: 'toggle-btn--wrapper' },
        getShowPartTableHydro()
            ? makeLabelAndIcon('toggle-on', 3, 'minus', () => tr.geo('info'))(
                  changeShowPartTableHydro
              )
            : makeLabelAndIcon('toggle-off', 3, 'plus', () => tr.geo('info'))(
                  changeShowPartTableHydro
              )
    );

const prevLayerInMapping = (index: number, mapping: GeoMessageKey[]) => {
    let i = index;
    const thicknesses = getLayersThicknesses().fold([], arr => arr);
    while (thicknesses[index - 1] === 0) {
        i = index - 1;
        index = index - 1;
    }
    return mapping[i - 1];
};

const renderTableDataSpan = (
    index: number,
    mapping: GeoMessageKey[],
    variable: string,
    className: string = ''
) => {
    const curLayer = mapping[index];
    const prevLayer = prevLayerInMapping(index, mapping);
    let rowspan = 1;
    if (prevLayer !== curLayer) {
        while (mapping[index] === mapping[index + 1]) {
            if (getLayersThicknesses().fold(0, arr => arr[index + 1]) !== 0) {
                rowspan = rowspan + 1;
            }
            index++;
        }
        return TD(
            {
                className: `${curLayer} ${className}`,
                rowSpan: rowspan,
                key: `${curLayer}-${className}`,
            },
            variable
        );
    } else {
        return NODISPLAY();
    }
};

const renderQTableDataSpan = (
    qindex: number,
    variables: string[],
    className: string = ''
) =>
    qindex === 0
        ? TD(
              {
                  className,
                  rowSpan: isUS13() ? 3 : 2,
              },
              variables[0]
          )
        : qindex === 3
        ? TD({}, variables[1])
        : NODISPLAY();

const renderQuaternaireDepth = (qindex: number, thicknesses: number[]) =>
    qindex === thicknesses.length - 1
        ? TD({ className: 'depth depth--4' }, formatDepth(getLayersDepths()[0]))
        : qindex === thicknesses.length - 2
        ? TD({ className: 'depth depth--3', style: { border: 0 } }, '?')
        : qindex === thicknesses.length - 3
        ? TD(
              { className: 'depth depth--2', style: { border: 0 } },
              thicknesses[3] === 0
                  ? formatDepth(getLayersDepths()[0])
                  : thicknesses[2] === 0
                  ? '?'
                  : ''
          )
        : TD({ className: 'depth depth--1', style: { borderBottom: 0 } }, '');

const renderQuaternaire = () =>
    getQuaternaireThicknesses().map((ql, qindex, thicknesses) =>
        ql === 0
            ? renderNoTableLine()
            : TR(
                  {
                      className: '',
                      style: {
                          // height: `${(computeLineHeight(l) *
                          //     computeQLineHeight(ql)) /
                          //     100}%`
                          height: `${MIN_LINE_HEIGHT}px`,
                      },
                      key: `quaternaire-${qindex}`,
                  },
                  renderQuaternaireDepth(qindex, thicknesses),
                  TD(
                      {
                          className: 'large us',
                          style: {
                              background: QUATERNARY_COLORS[qindex],
                          },
                      },
                      getQuaternaireName()[qindex]
                  ),
                  getShowPartTableGeol()
                      ? TD(
                            { className: 'litho' },
                            renderMoreText(
                                getDetailedLithologyQuaternaire()[qindex],
                                'detailledLithology',
                                qindex,
                                30
                            )
                        )
                      : NODISPLAY(),
                  getShowPartTableGeol()
                      ? TD({ className: 'etage' }, getGeologicalEtage()[qindex])
                      : NODISPLAY(),
                  getShowPartTableGeol()
                      ? TD({ className: 'serie' }, getGeologicalSerie()[qindex])
                      : NODISPLAY(),

                  getShowPartTableGeol()
                      ? qindex === 0
                          ? TD(
                                {
                                    className: 'system',
                                    rowSpan: quaternaireLayerCount(),
                                },
                                getGeologicalSystem()[0]
                            )
                          : NODISPLAY()
                      : NODISPLAY(),
                  getShowPartTableGeol()
                      ? qindex === 0
                          ? TD(
                                {
                                    className: 'era',
                                    rowSpan: quaternaireLayerCount(),
                                },
                                getGeologicalEra()[0]
                            )
                          : NODISPLAY()
                      : NODISPLAY(),

                  qindex === 0
                      ? TD(
                            {
                                className: `uh ${getUHClassName()[0]}`,
                                rowSpan: isUS13() ? 3 : 2,
                            },
                            keyAndTrad(getUH()[0])
                        )
                      : qindex === 3
                      ? TD(
                            { className: `uh ${getUHClassName()[1]}` },
                            keyAndTrad(getUH()[1])
                        )
                      : NODISPLAY(),
                  getShowPartTableHydro()
                      ? renderQTableDataSpan(qindex, getKMin(), 'kmin')
                      : NODISPLAY(),
                  getShowPartTableHydro()
                      ? renderQTableDataSpan(qindex, getKMean(), 'kmean')
                      : NODISPLAY(),
                  getShowPartTableHydro()
                      ? renderQTableDataSpan(qindex, getKMax(), 'kmax')
                      : NODISPLAY(),
                  getShowPartTableHydro()
                      ? renderQTableDataSpan(qindex, getHydroState(), 'state')
                      : NODISPLAY(),
                  getShowPartTableHydro()
                      ? qindex === 0
                          ? TD(
                                {
                                    className: 'hydro-system',
                                    rowSpan:
                                        quaternaireLayerCount() +
                                        countLayersUntilLayerName('US/RBC_81') -
                                        1,
                                }, // minus 1 because quaternaire is already counted
                                getHydroSystem()[0]
                            )
                          : NODISPLAY()
                      : NODISPLAY()
              )
    );

const renderHydroGeolTableLine = (l: number, index: number) => {
    const uh = UH_FOR_US_ID[index];
    const uhId = getHydroLayerId(uh);
    if (l === 0) {
        return renderNoTableLine();
    } else {
        if (index === 0) {
            return renderQuaternaire();
        } else {
            return TR(
                {
                    className: `row-hydrogeo row-hydrogeo-index-${index}`,
                    //   style: { height: computeLineHeight(l) + '%' },
                    style: {
                        height: `${computeSmartGeolLineHeight(index)}px`,
                    },
                    key: index,
                },
                renderDepthCol(getLayersDepths(), index),
                TD(
                    {
                        className: 'large us',
                        style: { background: STRATIGRAPHY_COLORS[index] },
                    },
                    tr.geo(getUS()[index]) // todo fixme utiliser plutôt : keyAndTrad(getUS()[index])
                ),
                getShowPartTableGeol()
                    ? TD(
                          { className: 'litho' },
                          renderMoreText(
                              getDetailedLithology()[index],
                              'detailledLithology',
                              index + getQuaternaireThicknesses().length,
                              30
                          )
                      )
                    : NODISPLAY(),
                // to collapse - geol
                getShowPartTableGeol()
                    ? renderTableDataSpan(
                          index,
                          GEOLOGICAL_ETAGE_NAME_FOR_US_ID,
                          getGeologicalEtage()[
                              getGeologicalEtageLayerId(
                                  GEOLOGICAL_ETAGE_NAME_FOR_US_ID[index]
                              )
                          ],
                          'etage'
                      )
                    : NODISPLAY(),
                getShowPartTableGeol()
                    ? renderTableDataSpan(
                          index,
                          GEOLOGICAL_SERIE_NAME_FOR_US_ID,
                          getGeologicalSerie()[
                              getGeologicalSerieLayerId(
                                  GEOLOGICAL_SERIE_NAME_FOR_US_ID[index]
                              )
                          ],
                          'serie'
                      )
                    : NODISPLAY(),
                getShowPartTableGeol()
                    ? renderTableDataSpan(
                          index,
                          GEOLOGICAL_SYSTEM_NAME_FOR_US_ID,
                          getGeologicalSystem()[
                              getGeologicalSystemLayerId(
                                  GEOLOGICAL_SYSTEM_NAME_FOR_US_ID[index]
                              )
                          ],
                          'system'
                      )
                    : NODISPLAY(),
                getShowPartTableGeol()
                    ? renderTableDataSpan(
                          index,
                          GEOLOGICAL_ERA_NAME_FOR_US_ID,
                          getGeologicalEra()[
                              getGeologicalEraLayerId(
                                  GEOLOGICAL_ERA_NAME_FOR_US_ID[index]
                              )
                          ],
                          'era'
                      )
                    : NODISPLAY(),

                // hydro
                renderTableDataSpan(
                    index,
                    UH_FOR_US_ID,
                    keyAndTrad(getUH()[uhId]),
                    `uh ${getUHClassName()[uhId]}`
                ),
                // to collapse - hydro
                getShowPartTableHydro()
                    ? renderTableDataSpan(
                          index,
                          UH_FOR_US_ID,
                          getKMin()[uhId],
                          'kmin'
                      )
                    : NODISPLAY(),
                getShowPartTableHydro()
                    ? renderTableDataSpan(
                          index,
                          UH_FOR_US_ID,
                          getKMean()[uhId],
                          'kmean'
                      )
                    : NODISPLAY(),
                getShowPartTableHydro()
                    ? renderTableDataSpan(
                          index,
                          UH_FOR_US_ID,
                          getKMax()[uhId],
                          'kmax'
                      )
                    : NODISPLAY(),
                getShowPartTableHydro()
                    ? renderTableDataSpan(
                          index,
                          UH_FOR_US_ID,
                          getHydroState()[uhId],
                          'state'
                      )
                    : NODISPLAY(),
                // renderTableDataSpan(index, HYDROLOGICAL_SYSTEM_NAME_FOR_US_ID, getHydroSystem()[getHydroSystemLayerId(HYDROLOGICAL_SYSTEM_NAME_FOR_US_ID[index])], 'hydro-system'),
                getShowPartTableHydro()
                    ? index === getLayerId('US/RBC_81')
                        ? TD(
                              {
                                  className: 'hydro-system',
                                  rowSpan:
                                      countLayersUntilLayerName('US/RBC_92') +
                                      1 -
                                      countLayersUntilLayerName('US/RBC_81'),
                              },
                              getHydroSystem()[1]
                          )
                        : NODISPLAY()
                    : NODISPLAY()
            );
        }
    }
};

const onGeometryChange = (_r: unknown, el: Element) => {
    const offset = fromNullable(el.querySelector('thead'))
        .map(h => h.clientHeight)
        .getOrElse(0);
    fromNullable(el.querySelector('tbody')).map(tbody => {
        let accumulateQuat = 0;
        const heights = [...tbody.children]
            .map(c => {
                const notShow = fromNullable(c.children.item(0))
                    .map(
                        firstCell =>
                            firstCell.classList.contains('depth--2') ||
                            firstCell.classList.contains('depth--1')
                    )
                    .getOrElse(false);
                if (notShow) {
                    accumulateQuat += c.clientHeight;
                    return 0;
                } else if (accumulateQuat > 0) {
                    const ret = accumulateQuat + c.clientHeight;
                    accumulateQuat = 0;
                    return ret;
                } else {
                    return c.clientHeight;
                }
            })
            .filter(h => h > 0);
        updateConnect('depth', () => ({ offset, heights }));
    });
};

export const renderHydroGeolTable = () =>
    getLayersThicknesses().fold(NODISPLAY(), thicknesses =>
        DIV(
            { className: 'table--hydrogeol guide' },
            TABLE(
                {
                    ref: rect(onGeometryChange),
                    // style: { height: getDiagramHeight() }
                },
                THEAD(
                    {},
                    TR(
                        {},
                        // geol
                        renderDepthTitle(),
                        TH(
                            { className: 'us' },
                            markdown(tr.geo('closeTableColUS')),
                            renderShowGeolButton()
                        ),
                        getShowPartTableGeol()
                            ? TH({ className: 'litho' }, tr.geo('lithoCol'))
                            : NODISPLAY(),
                        getShowPartTableGeol()
                            ? TH({ className: 'etage' }, tr.geo('etageCol'))
                            : NODISPLAY(),
                        getShowPartTableGeol()
                            ? TH({ className: 'serie' }, tr.geo('serieCol'))
                            : NODISPLAY(),
                        getShowPartTableGeol()
                            ? TH({ className: 'system' }, tr.geo('systemCol'))
                            : NODISPLAY(),
                        getShowPartTableGeol()
                            ? TH({ className: 'era' }, tr.geo('eraCol'))
                            : NODISPLAY(),

                        // hydro
                        TH(
                            {},
                            markdown(tr.geo('openTableColUH')),
                            renderShowHydroButton()
                        ),
                        getShowPartTableHydro()
                            ? TH(
                                  { className: 'kmin' },
                                  markdown(tr.geo('openTableColKmin'))
                              )
                            : NODISPLAY(),
                        getShowPartTableHydro()
                            ? TH(
                                  { className: 'kmean' },
                                  markdown(tr.geo('openTableColKmean'))
                              )
                            : NODISPLAY(),
                        getShowPartTableHydro()
                            ? TH(
                                  { className: 'kmax' },
                                  markdown(tr.geo('openTableColKmax'))
                              )
                            : NODISPLAY(),
                        getShowPartTableHydro()
                            ? TH(
                                  { className: 'state' },
                                  tr.geo('openTableColState')
                              )
                            : NODISPLAY(),
                        getShowPartTableHydro()
                            ? TH(
                                  { className: 'hydro-system' },
                                  tr.geo('systemCol')
                              )
                            : NODISPLAY()
                    )
                ),
                TBODY(
                    {},
                    ...thicknesses.map((l, i) => renderHydroGeolTableLine(l, i))
                )
            )
        )
    );
