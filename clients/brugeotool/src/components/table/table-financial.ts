import {
    DIV,
    TABLE,
    TBODY,
    THEAD,
    TH,
    TR,
    TD,
    NODISPLAY,
    H2,
    SPAN,
    BUTTON,
} from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { toggleShowPartTableFinanceThermicNeeds } from 'brugeotool/src/events/geothermie';
import {
    getShowPartTableFinanceThermicNeeds,
    getSystem,
    getFinanceTableData,
} from 'brugeotool/src/queries/geothermie';
import * as io from 'io-ts';
import { markdown } from 'sdi/ports/marked';
import { finPageText } from 'brugeotool/src/locale/marked-texts';
import { nameToString } from 'sdi/components/button/names';

export type FinanicalTableColumnType =
    | 'class'
    | 'buildingType'
    | 'isolationType'
    | 'surface'
    | 'hotNeeds'
    | 'coldNeeds'
    | 'hotWaterNeeds'
    | 'closeSysTubeLength'
    | 'closeSysTubesNumber'
    | 'closeSysDrillingDepth'
    | 'openSysFlow'
    | 'openSysDoubletNumber';

export type BuildingType =
    | 'uniResidential'
    | 'multiResidential'
    | 'smallOffice'
    | 'mediumOffice'
    | 'hugeOffice';

export type IsolationType = 'new' | 'renew';

export const FinanialDataRowIO = io.interface(
    {
        class: io.Integer,
        hotNeeds: io.string,
        coldNeeds: io.string,
        hotWaterNeeds: io.string,
        closeSysTubeLength: io.string,
        closeSysTubesNumber: io.string,
        closeSysDrillingDepth: io.string,
        openSysFlow: io.string,
        openSysDoubletNumber: io.string,
    },
    'FinanialDataRowIO'
);

export type FinanialDataRow = io.TypeOf<typeof FinanialDataRowIO>;

const display = '';
const hide = 'none';

const getRowDisplay = (rowClass: string) => {
    switch (rowClass) {
        case 'class':
            return display;
        case 'needs':
            return getShowPartTableFinanceThermicNeeds() ? display : hide;
        case 'open':
            return getSystem().getOrElse('close') === 'open' ? display : hide;
        case 'close':
            return getSystem().getOrElse('close') === 'close' ? display : hide;
        default:
            return display;
    }
};

const buttonThermicNeeds = () =>
    BUTTON(
        {
            className: 'geo-btn geo-btn--2 geo-finance-show-thermic-needs',
            onClick: toggleShowPartTableFinanceThermicNeeds,
        },
        SPAN(
            { className: 'btn__icon' },
            getShowPartTableFinanceThermicNeeds()
                ? nameToString('minus')
                : nameToString('plus')
        ),
        SPAN({ className: 'btn__label' }, tr.geo('thermicNeedsSwitch'))
    );

const columnIds: FinanicalTableColumnType[] = [
    'class',
    'buildingType',
    'isolationType',
    'surface',
    'hotNeeds',
    'coldNeeds',
    'hotWaterNeeds',
    'closeSysTubeLength',
    'closeSysTubesNumber',
    'closeSysDrillingDepth',
    'openSysFlow',
    'openSysDoubletNumber',
];

const buildingTypes: BuildingType[] = [
    'uniResidential',
    'multiResidential',
    'smallOffice',
    'mediumOffice',
    'hugeOffice',
];

const getColumnCategory = (columId: FinanicalTableColumnType) => {
    switch (columId) {
        case 'class':
        case 'buildingType':
        case 'isolationType':
        case 'surface':
            return 'class';
        case 'hotNeeds':
        case 'coldNeeds':
        case 'hotWaterNeeds':
            return 'needs';
        case 'closeSysTubeLength':
        case 'closeSysTubesNumber':
        case 'closeSysDrillingDepth':
            return 'close';
        default:
            // openSysFlow or openSysDoubletNumber
            return 'open';
    }
};

const rowIds = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

const translationKey = (k: FinanicalTableColumnType) => {
    switch (k) {
        case 'class':
            return 'FinTable_class';
        case 'buildingType':
            return 'FinTable_buildingType';
        case 'isolationType':
            return 'FinTable_isolationType';
        case 'surface':
            return 'FinTable_surface';
        case 'hotNeeds':
            return 'FinTable_hotNeeds';
        case 'coldNeeds':
            return 'FinTable_coldNeeds';
        case 'hotWaterNeeds':
            return 'FinTable_hotWaterNeeds';
        case 'closeSysTubeLength':
            return 'FinTable_closeSysTubeLength';
        case 'closeSysTubesNumber':
            return 'FinTable_closeSysTubesNumber';
        case 'closeSysDrillingDepth':
            return 'FinTable_closeSysDrillingDepth';
        case 'openSysFlow':
            return 'FinTable_openSysFlow';
        case 'openSysDoubletNumber':
            return 'FinTable_openSysDoubletNumber';
    }
};

const getCellContent = (rowId: number, columId: FinanicalTableColumnType) => {
    if (columId === 'buildingType') {
        return tr.geo(buildingTypes[Math.floor((rowId - 1) / 2)]);
    } else if (columId === 'isolationType') {
        return tr.geo(rowId % 2 === 0 ? 'renewIso' : 'newIso');
    } else if (columId === 'surface') {
        return [150, 700, 5500, 15000, 75000][Math.floor((rowId - 1) / 2)];
    }

    const data = getFinanceTableData();
    if (data.length >= rowId) {
        return getFinanceTableData()[rowId - 1][columId];
    } else {
        return 'Loading';
    }
};

const getTdStyle = (rowId: number, columnId: FinanicalTableColumnType) =>
    rowId % 2 === 1
        ? {
              display: getRowDisplay(getColumnCategory(columnId)),
              borderBottom: 'none',
          }
        : {
              display: getRowDisplay(getColumnCategory(columnId)),
              borderTop: 'none',
          };

const getCssClass = (category: 'class' | 'close' | 'open' | 'needs') => {
    switch (category) {
        case 'class':
            return 'building-class';
        case 'close':
            return 'system-closed';
        case 'open':
            return 'system-open';
        case 'needs':
            return 'thermic-need';
    }
};

const getTdOption = (rowId: number, columnId: FinanicalTableColumnType) =>
    rowId % 2 === 1 && columnId === 'buildingType'
        ? {
              style: getTdStyle(rowId, columnId),
              rowSpan: 2,
              className: 'building-type',
          }
        : {
              style: getTdStyle(rowId, columnId),
              className: getCssClass(getColumnCategory(columnId)),
          };

const getTd = (rowId: number, columnId: FinanicalTableColumnType) =>
    rowId % 2 === 0 && columnId === 'buildingType'
        ? NODISPLAY
        : TD(getTdOption(rowId, columnId), getCellContent(rowId, columnId));

const getRow = (rowId: number) =>
    TR(
        {},
        columnIds.map(columnId => getTd(rowId, columnId))
    );

const getThLevel2 = (columnId: FinanicalTableColumnType) =>
    TH(
        {
            style: { display: getRowDisplay(getColumnCategory(columnId)) },
            className: getCssClass(getColumnCategory(columnId)),
        },
        tr.geo(translationKey(columnId))
    );

const financialTable = () =>
    DIV(
        { className: 'table--financial' },
        DIV(
            {},
            TABLE(
                {
                    style: {},
                },
                THEAD(
                    {},
                    TR(
                        {},
                        TH(
                            {
                                colSpan: 4,
                                style: { display: getRowDisplay('class') },
                                className: getCssClass('class'),
                            },
                            tr.geo('buildingClass')
                        ),
                        TH(
                            {
                                colSpan: 3,
                                style: { display: getRowDisplay('needs') },
                                className: getCssClass('needs'),
                            },
                            tr.geo('thermicNeeds')
                        ),
                        TH(
                            {
                                colSpan: 3,
                                style: { display: getRowDisplay('close') },
                                className: getCssClass('close'),
                            },
                            tr.geo('closeSystem')
                        ),
                        TH(
                            {
                                colSpan: 2,
                                style: { display: getRowDisplay('open') },
                                className: getCssClass('open'),
                            },
                            tr.geo('openSystem')
                        )
                    ),
                    TR(
                        {},
                        columnIds.map(colId => getThLevel2(colId))
                    )
                ),
                TBODY(
                    {},
                    rowIds.map(i => getRow(i))
                )
            )
        )
    );

export const pageTitle = () =>
    H2(
        {
            style: {
                marginBottom: '15px',
            },
        },
        tr.geo('finPageTitle')
    );

export const tableRemark = () =>
    DIV(
        {
            className: 'table-remark',
        },
        getSystem().getOrElse('close') === 'open'
            ? tr.geo('finPageRemarkOpen')
            : tr.geo('finPageRemarkClose')
    );

export const pageText = () =>
    DIV(
        { className: 'text--finance' },
        markdown(
            fromRecord(finPageText(getSystem().getOrElse('close') === 'open'))
        )
    );

export const renderFinancialTable = () =>
    DIV(
        {},
        pageTitle(),
        buttonThermicNeeds(),
        financialTable(),
        tableRemark(),
        pageText()
    );
