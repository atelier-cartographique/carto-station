import * as debug from 'debug';
import { STRATIGRAPHY_COLORS, STRATIGRAPHY_UNITS_NAME } from '../settings';
import { drawYAxisTick, writeYAxisLabels } from '../carrot/axes';
import { getGraphSnapXy, getLayersTotalDepth } from '../../queries/geothermie';
// import { depthToY } from './graph';
import { noNanButOne } from './format';
import { color } from '../carrot/engine';

const logger = debug('sdi:grah/engine');

export const GRAPH_LEFT_MARGIN = 30;
export const GRAPH_TOP_MARGIN = 99;
const AXIS_ARROW_WIDTH = 3;
export const TICK_LENGTH = 4;
export const HEIGHT_STEP = 10;

export const MAX_CONDUCTIVITY_VALUE = 3.5; // W/(m.K)

const withContext = (ctx: CanvasRenderingContext2D) => {
    const drawXAxis = (width: number) => {
        ctx.beginPath();
        ctx.moveTo(0, 0);
        ctx.lineTo(width, 0);
        ctx.moveTo(width - 2 * AXIS_ARROW_WIDTH, -AXIS_ARROW_WIDTH);
        ctx.lineTo(width, 0);
        ctx.lineTo(width - 2 * AXIS_ARROW_WIDTH, AXIS_ARROW_WIDTH);
        ctx.lineTo(width - 2 * AXIS_ARROW_WIDTH, 0);
        ctx.closePath();
        ctx.fillStyle = 'black';
        ctx.fill();
        ctx.strokeStyle = 'black';
        ctx.stroke();
    };

    const drawYAxis = (height: number) => {
        ctx.beginPath();
        ctx.moveTo(0, 0);
        ctx.lineTo(0, height);
        ctx.moveTo(AXIS_ARROW_WIDTH, height - 2 * AXIS_ARROW_WIDTH);
        ctx.lineTo(0, height);
        ctx.lineTo(-AXIS_ARROW_WIDTH, height - 2 * AXIS_ARROW_WIDTH);
        ctx.lineTo(0, height - 2 * AXIS_ARROW_WIDTH);
        ctx.closePath();
        ctx.fillStyle = 'black';
        ctx.fill();
        ctx.closePath();
        ctx.strokeStyle = 'black';
        ctx.stroke();
    };

    const drawGraticuleX = (xTick: number) => {
        ctx.beginPath();
        ctx.moveTo(xTick, 0);
        ctx.lineTo(xTick, -TICK_LENGTH);
        ctx.closePath();
        ctx.strokeStyle = 'black';
        ctx.stroke();
    };

    const writeXAxisTickLabel = (label: number, xTick: number) => {
        ctx.fillStyle = 'black';
        ctx.font = '12px Arial';
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        ctx.fillText(label.toString(), xTick, -3 * TICK_LENGTH);
    };

    // const _writeAxisLabels = (width: number, height: number) => {
    //     ctx.fillStyle = "black"
    //     ctx.font = "12px Arial";
    //     ctx.textAlign = "center";
    //     ctx.textBaseline = "middle";
    //     ctx.fillText(tr.geo('condGraphTitle'), width / 2, -16);
    //     ctx.translate(-8, height/2);
    //     ctx.rotate(-Math.PI / 2);
    //     ctx.fillText('z (m)', 0, 0);
    // }

    const drawAxis = (
        widthPx: number,
        heightPx: number,
        maxX: number,
        maxZ: number
    ) => {
        /*
         * widthPx: length of the X axis in pixels
         * heightPx: length of the Z axis in pixels
         * maxX: maximum value for the X axis, in value unit
         * maxZ: maximum value for the Z axis, in meter
         */
        drawXAxis(widthPx);
        const valueStep = 0.5; // in value unit
        const valueLabelStep = valueStep * 2;
        const scaleFactorX = widthPx / maxX;
        [...Array(noNanButOne(Math.ceil(maxX / valueStep))).keys()].map(i =>
            drawGraticuleX(i * valueStep * scaleFactorX)
        );
        [...Array(noNanButOne(Math.ceil(maxX / valueLabelStep))).keys()].map(
            i =>
                writeXAxisTickLabel(
                    i * valueLabelStep,
                    i * valueLabelStep * scaleFactorX
                )
        );

        drawYAxis(heightPx);
        drawYAxisTick(ctx, maxZ, heightPx, -TICK_LENGTH, 0);
        writeYAxisLabels(ctx, maxZ, heightPx, 0, 0);

        // writeAxisLabels(widthPx, heightPx);
    };

    const drawHorizontalLine = (
        startX: number,
        endX: number,
        startY: number,
        endY: number,
        hasConnector: boolean
    ) => {
        hasConnector ? ctx.setLineDash([]) : ctx.setLineDash([3, 12]);

        ctx.globalCompositeOperation = 'multiply';
        ctx.beginPath();
        ctx.moveTo(startX, startY);
        ctx.lineTo(endX, endY);
        ctx.closePath();
        ctx.strokeStyle = hasConnector ? '#555555' : color.saturated_wo_transp;
        ctx.lineWidth = hasConnector ? 1 : 2;
        ctx.stroke();
    };

    const drawObliqueLine = (
        startX: number,
        endX: number,
        startY: number,
        endY: number
    ) => {
        ctx.setLineDash([]);
        ctx.globalCompositeOperation = 'source-over';
        ctx.lineCap = 'round';

        ctx.beginPath();
        ctx.moveTo(startX, startY);
        ctx.lineTo(endX, endY);
        ctx.closePath();
        ctx.strokeStyle = '#ffffff';
        ctx.lineWidth = 8;
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(startX, startY);
        ctx.lineTo(endX, endY);
        ctx.closePath();
        ctx.strokeStyle = '#fc0d05';
        ctx.lineWidth = 6;
        ctx.stroke();
    };

    const drawColouredRectangle = (
        x: number,
        y: number,
        width: number,
        height: number,
        colour: string
    ) => {
        ctx.globalCompositeOperation = 'multiply';
        ctx.beginPath();
        ctx.rect(x, y, width, height);
        ctx.fillStyle = colour;
        ctx.fill();
    };

    const drawGraph = (
        width: number,
        height: number,
        depths: number[],
        values1: number[],
        values2: number[]
    ) => {
        /*
         * width: graph width in pixels
         * height: graph height in pixels
         * depths: vector of depths values, in data unit (e.g., m)
         * values1: vector of values for drawing the rectangle
         * values2: vector of values for drawnig the oblique line
         */

        // 1) Remove null values
        const notNullDepths: number[] = [];
        const notNullValues1: number[] = [];
        const notNullValues2: number[] = [];
        const notNullColours: string[] = [];
        const doubleColours: string[] = [];
        STRATIGRAPHY_COLORS.map(c => {
            doubleColours.push(...[c, c]);
        });
        const isSaturated: boolean[] = [];

        depths.map((d, i) => {
            if (depths[i - 1] !== depths[i]) {
                notNullDepths.push(d);
                notNullValues1.push(values1[i]);
                notNullValues2.push(values2[i]);
                notNullColours.push(doubleColours[i]);
                isSaturated.push(i % 2 !== 0);
            }
        });
        // 2) Scale the values to the dimensions of the graph
        const maxDepth = notNullDepths[notNullDepths.length - 1];
        const scale = height / maxDepth;
        const scaledDepths = notNullDepths.map(d => d * scale);
        logger(`height: ${height}, maxdepth: ${maxDepth}, scale: ${scale}`);

        const scaledValues1 = notNullValues1.map(
            v => (v * width) / MAX_CONDUCTIVITY_VALUE
        );
        const scaledValues2 = notNullValues2.map(
            v => (v * width) / MAX_CONDUCTIVITY_VALUE
        );

        scaledDepths.map((d, i) => {
            const startDepth =
                scaledDepths[i - 1] !== undefined ? scaledDepths[i - 1] : 0;
            drawColouredRectangle(
                0,
                startDepth,
                scaledValues1[i],
                d - startDepth,
                notNullColours[i]
            );
            drawObliqueLine(
                scaledValues2[i - 1] !== undefined
                    ? scaledValues2[i - 1]
                    : scaledValues2[i],
                scaledValues2[i],
                startDepth,
                d
            );

            const isSaturatedInterface: boolean = isSaturated[i] !== isSaturated[i+1];
            drawHorizontalLine(0, width, d, d, !isSaturatedInterface);
            // if (isSaturated[i]) { // for saturated layers only, for uneven indices
            //     drawColouredRectangle(
            //         0,
            //         startDepth,
            //         scaledValues1[i],
            //         d - startDepth,
            //         SATURATED_COLOR
            //     );
            // }
        });
    };

    const drawPointOnGraph = () => {
        const xSnap = getGraphSnapXy().fold(-9999, xy => xy.x);
        const ySnap = getGraphSnapXy().fold(-9999, xy => xy.y);

        ctx.globalCompositeOperation = 'source-over';
        ctx.setLineDash([]);
        ctx.beginPath();
        ctx.arc(xSnap, ySnap, 6, 0, 2 * Math.PI);
        ctx.strokeStyle = 'black';
        ctx.lineWidth = 1;
        ctx.stroke();
    };

    const drawAbscisOnGraph = (
        widthPx: number,
        heightPx: number,
    ) => {
        ctx.fillStyle = 'black';
        ctx.font = '12px arial';
        ctx.fillText('λ(W/m.K)', widthPx, heightPx);
    }

    const writeUS = (depths: number[]) => {
        depths.map((d, i) => {
            if (depths[i - 1] !== depths[i]) {
                ctx.fillStyle = 'black';
                ctx.font = 'bold 12px arial';
                ctx.textAlign = 'right';
                ctx.textBaseline = 'middle';
                const us =
                    STRATIGRAPHY_UNITS_NAME[Math.ceil(i / 2) - 1] !== undefined
                        ? STRATIGRAPHY_UNITS_NAME[Math.ceil(i / 2) - 1]
                        : '';
                ctx.fillText(
                    us,
                    90,
                    (d / getLayersTotalDepth()) * (800 - GRAPH_TOP_MARGIN) +
                        GRAPH_TOP_MARGIN -
                        10
                );
            }
        });
    };

    return { drawGraph, drawAxis, drawPointOnGraph, drawAbscisOnGraph, writeUS };
};

export const graph = () => {
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d')!;

    const getGraph = (
        width: number,
        height: number,
        depths: number[],
        values1: number[],
        values2: number[],
        print = false
    ) => {
        canvas.width = width;
        canvas.height = height;

        ctx.translate(GRAPH_LEFT_MARGIN, GRAPH_TOP_MARGIN); // add some margins to the graph
        withContext(ctx).drawAxis(
            width - GRAPH_LEFT_MARGIN,
            height - GRAPH_TOP_MARGIN,
            MAX_CONDUCTIVITY_VALUE,
            depths[depths.length - 1]
        );

        ctx.setTransform(1, 0, 0, 1, 0, 0); // reset the translation to zero
        ctx.translate(GRAPH_LEFT_MARGIN, GRAPH_TOP_MARGIN); // add some margins to the graph
        withContext(ctx).drawGraph(
            width - GRAPH_LEFT_MARGIN,
            height - GRAPH_TOP_MARGIN,
            depths,
            values1,
            values2
        );

        ctx.setTransform(1, 0, 0, 1, 0, 0); // reset the translation to zero
        withContext(ctx).drawPointOnGraph();

        withContext(ctx).drawAbscisOnGraph(100, 70);

        if (print) {
            ctx.setTransform(1, 0, 0, 1, 0, 0); // reset the translation to zero
            withContext(ctx).writeUS(depths);
        }

        return canvas.toDataURL();
    };

    const getScale = (height: number, thickness: number[]) => {
        const maxDepth = thickness
            .filter(t => t > 0)
            .reduce((acc, v) => acc + v, 0);
        return height / maxDepth;
    };

    return { getGraph, getScale };
};

logger('loaded');
