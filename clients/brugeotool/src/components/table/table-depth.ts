import {
    DIV,
    TABLE,
    TBODY,
    THEAD,
    TH,
    TR,
    TD,
    NODISPLAY,
    SPAN,
} from 'sdi/components/elements';
import tr from 'sdi/locale';

import {
    getLayersThicknesses,
    isUH1b,
    getDiagramHeight,
    getDepthMode,
    getElevation,
} from '../../queries/geothermie';
import { getLayout } from '../../queries/app';
import {
    renderNoTableLine,
    computeSmartDepthLineHeight,
    getLayersDepths,
    formatDepth,
} from './format';
import { markdown } from 'sdi/ports/marked';
import { fromNullable } from 'fp-ts/lib/Option';
import { updateConnect } from 'brugeotool/src/events/geothermie';
import { rect } from 'sdi/app';

export const renderDepthTitle = () =>
    TH(
        { className: 'depth' },
        getDepthMode() === 'relative'
            ? SPAN({}, '0')
            : SPAN({}, getElevation().toFixed(1)),
        getDepthMode() === 'relative'
            ? markdown(tr.geo('closeTableColDepth'))
            : markdown(tr.geo('closeTableColDepthAbsolute'))
    );

export const renderDepthCol = (depths: number[], index: number) =>
    TD(
        { className: 'depth' },
        isUH1b() && index === 0 && getLayout() !== 'general'
            ? '?'
            : index <= depths.length - 2
            ? formatDepth(depths[index])
            : ''
    );

const renderDepthTableLine = (l: number, index: number) =>
    l === 0
        ? renderNoTableLine()
        : TR(
              {
                  className: '',
                  //style: { height: computeLineHeight(l) + '%' },
                  style: { height: `${computeSmartDepthLineHeight(index)}px` },
                  key: index,
              },
              renderDepthCol(getLayersDepths(), index)
          );

const rectifyConnect = (_r: any, el: Element) => {
    const offset = fromNullable(el.querySelector('thead'))
        .map(h => h.clientHeight)
        .getOrElse(0);
    fromNullable(el.querySelector('tbody')).map(tbody => {
        const heights = [...tbody.children]
            .map(c => c.clientHeight)
            .filter(h => h > 0);
        updateConnect('depth', () => ({ offset, heights }));
    });
};

export const renderDepthTable = (key: string) =>
    getLayersThicknesses().fold(NODISPLAY(), thicknesses =>
        DIV(
            { className: 'table--depth' },
            TABLE(
                {
                    key,
                    ref: rect(rectifyConnect),
                    style: { height: getDiagramHeight() },
                },
                THEAD({}, TR({}, renderDepthTitle())),
                TBODY(
                    {},
                    ...thicknesses.map((l, i) => renderDepthTableLine(l, i))
                )
            )
        )
    );
