import * as debug from 'debug';

import { DIV, SPAN, NODISPLAY, TR, BUTTON } from 'sdi/components/elements';
import tr from 'sdi/locale';

import { toggleMoreTextHidden } from '../../events/geothermie';
import {
    getLayersThicknesses,
    getHydroLayersThicknesses,
    getLayersTotalDepth,
    getQLayersTotalDepth,
    getMoreTextHiddenItem,
    getDiagramHeight,
    quaternaireLayerCount,
    getGeologicalEtageThicknesses,
    getGeologicalSerieThicknesses,
    getGeologicalEraThicknesses,
    getGeologicalSystemThicknesses,
    getElevation,
    getDepthMode,
} from '../../queries/geothermie';
import { GeoMessageKey } from '../../locale';

const logger = debug('sdi:table/format');

export const keyAndTrad = (key: GeoMessageKey) => tr.geo(key);

export const formatDepth = (d: number) =>
    getDepthMode() === 'relative'
        ? `-${d.toFixed(1)}`
        : (getElevation() - d).toFixed(1);

export const formatPiezo = (d: number) => {
    const piezo = getDepthMode() === 'relative' ? d : getElevation() + d;
    return d > 0 ? `+${piezo.toFixed(1)}` : piezo.toFixed(1); // The 'plus' indicates the artesianism
};

export type MoreTextHidden = {
    detailledLithology: boolean[];
    remarks: boolean[];
};

export type Depth = 'relative' | 'absolute';

const toggleMoreText = (key: keyof MoreTextHidden, index: number) =>
    toggleMoreTextHidden(key, index);

export const renderMoreText = (
    t: string,
    key: keyof MoreTextHidden,
    index: number,
    maxChar: number
) =>
    DIV(
        { key: `moreText-${key}-${index}` },
        SPAN({}, t.slice(0, maxChar)),
        t.length > maxChar
            ? BUTTON(
                  {
                      className: 'interactive',
                      alt: tr.core('expand'),
                      hidden: !getMoreTextHiddenItem(key, index),
                      onClick: () => toggleMoreText(key, index),
                  },
                  '... [+]'
              )
            : NODISPLAY(),
        t.length > maxChar
            ? SPAN(
                  {
                      hidden: getMoreTextHiddenItem(key, index),
                  },
                  t.slice(maxChar)
              )
            : NODISPLAY(),
        t.length > maxChar
            ? BUTTON(
                  {
                      className: 'interactive',
                      alt: tr.core('collapse'),
                      hidden: getMoreTextHiddenItem(key, index),
                      onClick: () => toggleMoreText(key, index),
                  },
                  '[-]'
              )
            : NODISPLAY()
    );

export const renderNoTableLine = () => TR({ style: { display: 'none' } });

export const computeLineHeight = (l: number) =>
    (100 * l) / getLayersTotalDepth();

const add = (a: number, b: number) => a + b;

const getSmartLineHeights = (
    thicknesses: number[],
    minLineHeight: number,
    quaternaire = false,
    geology = false
) => {
    let lineHeight: number[] = [];
    const layersTotalDepth = getLayersTotalDepth();
    const diagramHeight = getDiagramHeight();
    const qCount = quaternaireLayerCount();
    const correctedDiagramHeight = diagramHeight - 99; // 99px is the table header height
    if (layersTotalDepth <= 0 || correctedDiagramHeight <= 0) {
        return lineHeight;
    }
    const scale = correctedDiagramHeight / layersTotalDepth;

    let comp: number = 0; // we start at 1px for fine-tuning the level wrt to the graph
    if (quaternaire) {
        comp = comp + (qCount - 1) * minLineHeight;
    }
    lineHeight = thicknesses.map((l, i) => {
        // return 1;
        let h: number;
        const lpx = l * scale;
        if (geology && i === 0) {
            return qCount * minLineHeight;
        } else {
            if (lpx !== 0) {
                if (lpx < minLineHeight) {
                    h = minLineHeight;
                    comp = comp + minLineHeight - lpx + 0; // 2px must be added for accounting for the table line width
                    // lineHeight.push(h);
                    return h;
                } else {
                    h = lpx - comp;
                    if (h < minLineHeight) {
                        comp = comp - (lpx - minLineHeight);
                        return minLineHeight;
                    } else {
                        comp = 0;
                        return h;
                    }
                }
            } else {
                return 0;
            }
        }
    });
    const sumT = thicknesses.reduce(add) * scale;
    const sumL = lineHeight.reduce(add);
    if (sumT !== sumL) {
        // throw new Error('doesnt compute');
        logger(`ERROR:   sumT !== sumL    [${Math.abs(sumT - sumL)}] `);
    }
    // logger('=================================')
    // logger(`thicknesses: ${ thicknesses } `)
    // logger(`minLineHeight: ${ minLineHeight } `)
    // logger(`quaternaire: ${ quaternaire } `)
    // logger(`geology: ${ geology } `)
    // logger(`layersTotalDepth: ${ layersTotalDepth } `)
    // logger(`diagramHeight: ${ diagramHeight } `)
    // logger(`qCount: ${ qCount } `)
    // logger(`lineHeight: ${ lineHeight } `)
    return lineHeight;
};

export const MIN_LINE_HEIGHT = 50; // px
export const computeSmartClosedLineHeight = (index: number) =>
    getSmartLineHeights(getLayersThicknesses().getOrElse([]), MIN_LINE_HEIGHT)[
        index
    ];

export const computeSmartDepthLineHeight = (index: number) =>
    getSmartLineHeights(getLayersThicknesses().getOrElse([]), 36)[index];

export const computeSmartGeolLineHeight = (index: number) =>
    getSmartLineHeights(
        getLayersThicknesses().getOrElse([]),
        MIN_LINE_HEIGHT,
        true
    )[index];

export const computeSmartEtageLineHeight = (index: number) =>
    getSmartLineHeights(getGeologicalEtageThicknesses(), MIN_LINE_HEIGHT, true)[
        index
    ];

export const computeSmartSerieLineHeight = (index: number) =>
    getSmartLineHeights(getGeologicalSerieThicknesses(), MIN_LINE_HEIGHT, true)[
        index
    ];

export const computeSmartEraLineHeight = (index: number) =>
    getSmartLineHeights(
        getGeologicalEraThicknesses(),
        MIN_LINE_HEIGHT,
        false,
        true
    )[index];

export const computeSmartSystemLineHeight = (index: number) =>
    getSmartLineHeights(
        getGeologicalSystemThicknesses(),
        MIN_LINE_HEIGHT,
        false,
        true
    )[index];

export const computeSmartOpenLineHeight = (index: number) =>
    getSmartLineHeights(getHydroLayersThicknesses(), MIN_LINE_HEIGHT)[index];

export const computeQLineHeight = (ql: number) =>
    (100 * ql) / getQLayersTotalDepth();

export const getLayersDepths = () =>
    getLayersThicknesses().fold([], arr =>
        arr.reduce((a, x, i) => [...a, x + (a[i - 1] || 0)], [])
    );
// getDepthMode() === 'relative'
//     ? getLayersThicknesses().fold([], arr =>
//         arr.reduce((a, x, i) => [...a, x + (a[i - 1] || 0)], [])
//     )
//     : getLayersThicknesses().fold([], arr =>
//         arr.reduce((a, x, i) => [...a, x + (a[i - 1] || 0)], [])
//     ).map(d => getElevation() - d);

export const getHydroLayersDepths = () =>
    getHydroLayersThicknesses().reduce(
        (a, x, i) => [...a, x + (a[i - 1] || 0)],
        []
    );

export const noNanButOne = (value: number) => (isNaN(value) ? 1 : value);

logger('loaded');
