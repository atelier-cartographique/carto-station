import tr, { fromRecord, EmptyString } from 'sdi/locale';
//import { Marked } from 'sdi/ports/marked/marked';
import { nameToString } from 'sdi/components/button/names';

import {
    isExcluded,
    isConstraintSoil,
    isConstraintNatura,
    getConstraintSoil,
    isConstraints,
    isStrictlyConstraintSoil,
    getSystem,
    getBedrockDepth,
    getLayerDepth,
    getPiezoValue,
    getLayersThicknesses,
    isConstraintMetroNord,
    isConstraintWater,
} from '../../queries/geothermie';
import {
    generalInfoClosedOk,
    generalInfoOpenOkBefore,
    generalInfoOpenOkAfter,
    licenseOpenFrCommonHeader,
    licenseOpenFrCommonBody,
    licenseOpenSoilFrCheck0or01or02,
    licenseOpenSoilFrCheck03or04,
    licenseOpenSoilFrCheck3or4,
    aquifere1,
    aquifere2,
    aquifere3,
} from 'brugeotool/src/locale/marked-texts';
import { numberAquifere } from '../text/general';

const markdownParser = (t: string): string => t; //TODO

export const textGeneralInfo = () =>
    isExcluded()
        ? getSystem().getOrElse('close') === 'close'
            ? markdownParser(tr.geo('generalInfoClosedNotOk'))
            : markdownParser(tr.geo('generalInfoOpenNotOk'))
        : getSystem().getOrElse('close') === 'close'
        ? markdownParser(
              fromRecord(
                  generalInfoClosedOk(
                      getLayerDepth('US/RBC_92'),
                      getBedrockDepth()
                  )
              )
          )
        : markdownParser(
              fromRecord(generalInfoOpenOkBefore(numberAquifere()))
          ).concat(
              getPiezoValue('UH/RBC_4')
                  .fold('', v =>
                      v < -10000
                          ? ''
                          : markdownParser(fromRecord(aquifere1(Math.abs(v))))
                  )
                  .concat(
                      getLayersThicknesses()
                          .fold('', thicknesses =>
                              thicknesses[15] > 0
                                  ? markdownParser(
                                        fromRecord(
                                            aquifere2(
                                                getLayerDepth('US/RBC_81')
                                            )
                                        )
                                    )
                                  : ''
                          )
                          .concat(
                              getLayersThicknesses()
                                  .fold('', thicknesses =>
                                      thicknesses[18] > 0
                                          ? markdownParser(
                                                fromRecord(
                                                    aquifere3(
                                                        getLayerDepth(
                                                            'US/RBC_92'
                                                        )
                                                    )
                                                )
                                            )
                                          : ''
                                  )
                                  .concat(
                                      markdownParser(
                                          fromRecord(generalInfoOpenOkAfter())
                                      )
                                  )
                          )
                  )
          );

const renderSoilCategory = () =>
    !isStrictlyConstraintSoil()
        ? ''
        : `${tr.geo('polutionCategory')} ${getConstraintSoil()}`;

const renderSoilConstraint = () => {
    switch (getConstraintSoil()) {
        case '0':
            return markdownParser(tr.geo('constraintHeaderSoilCase0'))
                .concat(renderSoilCategory())
                .concat(
                    getSystem().fold(EmptyString, s =>
                        s === 'close'
                            ? tr.geo('constraintHeaderSoilcondUrbaClose')
                            : tr.geo('constraintHeaderSoilcondUrbaOpen')
                    )
                )
                .concat(
                    getSystem().fold(EmptyString, s =>
                        s === 'close'
                            ? tr.geo('constraintHeaderSoilcondEnviClose')
                            : ''
                    )
                );
        case '1':
            return markdownParser(tr.geo('constraintHeaderSoilCase1')).concat(
                renderSoilCategory()
            );
        case '2':
            return markdownParser(tr.geo('constraintHeaderSoilCase2')).concat(
                renderSoilCategory()
            );
        case '3':
            return markdownParser(tr.geo('constraintHeaderSoilCase2'))
                .concat(renderSoilCategory())
                .concat(
                    getSystem().fold(EmptyString, s =>
                        s === 'close'
                            ? tr.geo('constraintHeaderSoilcondEnviClose')
                            : tr.geo('constraintHeaderSoilcondEnviOpen')
                    )
                );
        case '4':
            return markdownParser(tr.geo('constraintHeaderSoilCase4'))
                .concat(renderSoilCategory())
                .concat(
                    getSystem().fold(EmptyString, s =>
                        s === 'close'
                            ? tr.geo('constraintHeaderSoilcondEnviClose')
                            : tr.geo('constraintHeaderSoilcondEnviOpen')
                    )
                );
        case '0+1':
        case '0+2':
            return markdownParser(tr.geo('constraintHeaderSoilCase4'))
                .concat(renderSoilCategory())
                .concat(
                    getSystem().fold(EmptyString, s =>
                        s === 'close'
                            ? tr.geo('constraintHeaderSoilcondUrbaClose')
                            : tr.geo('constraintHeaderSoilcondUrbaOpen')
                    )
                )
                .concat(
                    getSystem().fold(EmptyString, s =>
                        s === 'close'
                            ? tr.geo('constraintHeaderSoilcondEnviClose')
                            : ''
                    )
                );
        case '0+3':
        case '0+4':
            return markdownParser(tr.geo('constraintHeaderSoilCase4'))
                .concat(renderSoilCategory())
                .concat(
                    getSystem().fold(EmptyString, s =>
                        s === 'close'
                            ? tr.geo('constraintHeaderSoilcondUrbaClose')
                            : tr.geo('constraintHeaderSoilcondUrbaOpen')
                    )
                )
                .concat(
                    getSystem().fold(EmptyString, s =>
                        s === 'close'
                            ? tr.geo('constraintHeaderSoilcondEnviClose')
                            : tr.geo('constraintHeaderSoilcondEnviOpen')
                    )
                );
        case 'none':
        default:
            return markdownParser(tr.geo('constraintHeaderSoilCaseNone'));
    }
};

const renderOpenSoilConstraintSoilInfo = () => {
    switch (getConstraintSoil()) {
        case '0':
        case '0+1':
        case '0+2':
            return markdownParser(licenseOpenSoilFrCheck0or01or02);
        case '3':
        case '4':
            return markdownParser(licenseOpenSoilFrCheck3or4);
        case '0+3':
        case '0+4':
            return markdownParser(licenseOpenSoilFrCheck03or04);
        default:
            return '';
    }
};
const renderNoConstraints = () =>
    getSystem().getOrElse('close') === 'close'
        ? tr.geo('projectSteps').concat(markdownParser(tr.geo('licenseClose')))
        : tr
              .geo('projectSteps')
              .concat(markdownParser(licenseOpenFrCommonHeader))
              .concat(markdownParser(licenseOpenFrCommonBody));

export const textConstraintHeader = () =>
    isExcluded()
        ? tr
              .geo('projectSituation')
              .concat(
                  // title
                  nameToString('times')
              )
              .concat(
                  isConstraintWater()
                      ? markdownParser(tr.geo('constraintHeaderWaterTrue'))
                      : markdownParser(tr.geo('constraintHeaderMetroTrue'))
              )
              .concat(markdownParser(tr.geo('constraintHeaderWaterTrueResult')))
        : tr
              .geo('projectSituation')
              .concat(
                  // title
                  nameToString('check')
              )
              .concat(markdownParser(tr.geo('constraintHeaderWaterFalse')))
              .concat(
                  isConstraintSoil()
                      ? nameToString('exclamation-triangle')
                      : nameToString('check')
              )
              .concat(renderSoilConstraint())
              .concat(
                  isConstraintNatura()
                      ? nameToString('exclamation-triangle').concat(
                            markdownParser(tr.geo('constraintHeaderNaturaTrue'))
                        )
                      : nameToString('check').concat(
                            markdownParser(
                                tr.geo('constraintHeaderNaturaFalse')
                            )
                        )
              )
              .concat(
                  isConstraintMetroNord()
                      ? nameToString('exclamation-triangle').concat(
                            markdownParser(tr.geo('constraintHeaderMetroNordTrue'))
                        )
                      : nameToString('check').concat(
                            markdownParser(
                                tr.geo('constraintHeaderMetroNordFalse')
                            )
                        )
              )
              ;

export const textConstraintBody = () =>
    isExcluded()
        ? ''
        : isConstraints() // any constraints ?
        ? getSystem().getOrElse('close') === 'close'
            ? tr
                  .geo('projectSteps')
                  .concat(markdownParser(tr.geo('licenseClose')))
            : isConstraintNatura()
            ? tr
                  .geo('projectSteps')
                  .concat(
                      markdownParser(licenseOpenFrCommonHeader).concat(
                          markdownParser(licenseOpenFrCommonBody)
                      )
                  )
            : tr
                  .geo('projectSteps')
                  .concat(markdownParser(licenseOpenFrCommonHeader))
                  .concat(renderOpenSoilConstraintSoilInfo())
                  .concat(markdownParser(licenseOpenFrCommonBody))
        : renderNoConstraints();
