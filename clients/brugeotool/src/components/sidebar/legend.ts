import { Nullable, isNotNullNorUndefined } from 'sdi/util';

export const {
    clear,
    moveTo,
    lineTo,
    fill,
    // stroke,
    begin,
    close,
    fillStyle,
    // strokeStyle,
    // strokeWidth,
    image,
} = (() => {
    let cachedCtx: Nullable<CanvasRenderingContext2D> = null;

    const inner = <T>(op: (ctx: CanvasRenderingContext2D) => T): T => {
        if (isNotNullNorUndefined(cachedCtx)) {
            return op(cachedCtx);
        } else {
            const canvas = document.createElement('canvas');
            canvas.width = 1000;
            canvas.height = 1000;
            while (!isNotNullNorUndefined(cachedCtx)) {
                cachedCtx = canvas.getContext('2d');
            }
            cachedCtx.lineJoin = 'round';
            cachedCtx.lineCap = 'round';
            return inner(op);
        }
    };

    const clear = (x: number, y: number, w: number, h: number) =>
        inner(ctx => ctx.clearRect(x, y, w, h));

    const moveTo = (x: number, y: number) => inner(ctx => ctx.moveTo(x, y));

    const lineTo = (x: number, y: number) => inner(ctx => ctx.lineTo(x, y));

    const fill = (color?: string) => {
        if (color === undefined) {
            inner(ctx => ctx.fill());
        } else {
            inner(ctx => {
                ctx.save();
                ctx.fillStyle = color;
                ctx.fill();
                ctx.restore();
            });
        }
    };

    const begin = () => inner(ctx => ctx.beginPath());

    const close = () => inner(ctx => ctx.closePath());

    const fillStyle = (color: string) =>
        inner(ctx => {
            ctx.fillStyle = color;
        });

    const image = (x: number, y: number, w: number, h: number) =>
        inner(ctx => {
            const src = ctx.getImageData(x, y, w, h);
            const target = document.createElement('canvas');
            target.width = w;
            target.height = h;
            const tctx = (() => {
                let tctx: CanvasRenderingContext2D | null = null;
                while (tctx === null) {
                    tctx = target.getContext('2d');
                }
                return tctx;
            })();
            tctx.putImageData(src, 0, 0);
            return target.toDataURL();
        });

    return {
        clear,
        moveTo,
        lineTo,
        fill,
        // stroke,
        begin,
        close,
        fillStyle,
        // strokeStyle,
        // strokeWidth,
        image,
    };
})();

export const prect = (
    [ax, ay]: [number, number],
    [bx, by]: [number, number],
    [cx, cy]: [number, number],
    [dx, dy]: [number, number]
) => {
    begin();
    moveTo(ax, ay);
    lineTo(bx, by);
    lineTo(cx, cy);
    lineTo(dx, dy);
    close();
};