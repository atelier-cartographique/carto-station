import * as debug from 'debug';

import {
    DIV,
    SPAN,
    A,
    NodeOrOptional,
    BUTTON,
    DETAILS,
    SUMMARY,
    H3,
} from 'sdi/components/elements';
import tr, { formatNumber } from 'sdi/locale';
import { nameToString } from 'sdi/components/button/names';
import { markdown } from 'sdi/ports/marked';

import { renderToggleSystem } from '../button';

import {
    navigateHome,
    navigateTechnical,
    navigatePrint,
    navigateGeneral,
    navigateLicense,
    navigateGeology,
    navigateFinance,
    Layout,
} from '../../events/route';
import {
    getX,
    getY,
    getXY,
    getFormatAddress,
    getCurrentCapakey,
    getSystem,
} from '../../queries/geothermie';
import { getLayout, getOrientation } from '../../queries/app';
import { renderTopMap } from '../map';
import { zoomIn, zoomOut } from 'brugeotool/src/events/map';
import { divTooltipLeft } from 'sdi/components/tooltip';
import { openSmartGeotherm } from 'brugeotool/src/events/modal';
import { getTopView } from 'brugeotool/src/queries/map';
import { activityLogger } from 'brugeotool/src/events/app';
import { linkAction } from 'sdi/activity';
import {
    renderLithology,
    renderHydroLegend,
} from '../legend/lithostratigraphy';
import { fromPredicate } from 'fp-ts/lib/Option';

const logger = debug('sdi:geothermie');

const renderText = () =>
    DIV({ className: 'sidebar__text' }, markdown(tr.geo('sidebarText'), 'md'));

const renderBothSystemsButton = () =>
    BUTTON(
        {
            className: 'geo-btn geo-btn--both-system geo-btn--2',
            onClick: () => navigateTechnical(getX(), getY()),
        },
        DIV({ className: 'geo-btn__inner' }, tr.geo('bothSystem'))
    );

const renderBackToMap = () =>
    BUTTON(
        {
            className: 'geo-btn geo-btn--back-to-map geo-btn--3',
            onClick: () => navigateHome(),
        },
        DIV(
            { className: 'geo-btn__inner' },
            SPAN({ className: 'btn__icon' }, nameToString('chevron-left')),
            SPAN({ className: 'btn__label' }, tr.geo('backToMap'))
        )
    );

const goToSmartGeoTherm = () =>
    BUTTON(
        {
            className: 'geo-btn geo-btn--1 geo-btn--link',
            onClick: openSmartGeotherm,
        },
        SPAN({ className: 'btn__icon' }, nameToString('cogs')),
        SPAN({ className: 'btn__label' }, tr.geo('expertEstim'))
    );

const goToFAQ = () =>
    A(
        {
            className: 'geo-btn geo-btn--1 geo-btn--link  btn-link',
            href: tr.geo('faqLink'),
            target: '_blank',
        },
        SPAN({ className: 'btn__label' }, tr.geo('faq'))
    );

const goToSource = () =>
    A(
        {
            className: 'geo-btn geo-btn--1 geo-btn--link btn-link',
            href: tr.geo('dataSourceLink'),
            target: '_blank',
        },
        SPAN({ className: 'btn__label' }, tr.geo('dataSource'))
    );

const btnWrapper = () =>
    DIV({ className: 'btn-wrapper' }, printButton(), goToFAQ(), goToSource());

const renderCoordinates = () =>
    getXY().map(pos =>
        DIV(
            { className: 'mini-map--info-item' },
            SPAN({ className: 'label' }, `${tr.geo('coordinatesLambert')} : `),
            SPAN(
                { className: 'value' },
                `${formatNumber(pos.x)}, ${formatNumber(pos.y)}`
            )
        )
    );

const renderCapakey = () =>
    getCurrentCapakey().map(capakey =>
        DIV(
            { className: 'mini-map--info-item' },
            SPAN({ className: 'label' }, `${tr.geo('capakey')} : `),
            SPAN({ className: 'value' }, capakey)
        )
    );

const renderAddress = () =>
    getFormatAddress().fold(
        DIV({
            className: 'mini-map--info-item mini-map--info-item--placeholder',
        }),
        addr =>
            DIV(
                {
                    className:
                        'mini-map--info-item mini-map--info-item--adress',
                },
                addr
            )
    );

const renderMiniMapInfos = () =>
    DIV(
        { className: 'mini-map--infos' },
        renderAddress(),
        renderCoordinates(),
        renderCapakey()
    );

const buttonZoomIn = () =>
    divTooltipLeft(
        tr.core('zoomIn'),
        {},
        BUTTON(
            {
                className: 'geo-btn geo-btn--3 geo-btn--icon geo-btn--zoom-in',
                onClick: zoomIn,
            },
            DIV({ className: 'btn-icon' }, nameToString('plus'))
        )
    );

const buttonZoomOut = () =>
    divTooltipLeft(
        tr.core('zoomOut'),
        {},
        BUTTON(
            {
                className: 'geo-btn geo-btn--3 geo-btn--icon geo-btn--zoom-out',
                onClick: zoomOut,
            },
            DIV({ className: 'btn-icon' }, nameToString('minus'))
        )
    );

const zoomButtons = () =>
    DIV(
        {
            className: 'mini-map--zoom unfocused',
            'aria-hidden': true,
        },
        buttonZoomIn(),
        buttonZoomOut()
    );

export const renderMinimapWrapper = () =>
    DIV(
        {
            className: 'mini-map',
            onClick: () => activityLogger(linkAction('click-mini-map')),
        },
        renderMiniMapInfos(),
        DIV(
            { className: 'centroid-wrapper' },
            DIV({
                className: 'centroid',
                onClick: () => {
                    const [x, y] = getTopView().center;
                    switch (getLayout()) {
                        case 'general':
                            return navigateGeneral(x, y);
                        case 'license':
                            return navigateLicense(x, y);
                        case 'geology':
                            return navigateGeology(x, y);
                        case 'finance':
                            return navigateFinance(x, y);
                        case 'technical':
                            return navigateTechnical(x, y);
                    }
                },
            }),
            renderBackToMap(),
            zoomButtons(),
            renderTopMap()
        )
    );

const renderStratiLegend = () =>
    fromPredicate<Layout>(
        l => l === 'geology' || l === 'general' || l === 'technical'
    )(getLayout()).map(() =>
        DIV(
            'legend-log',

            DETAILS(
                {
                    className: 'strati-legend-details',
                    open: true,
                },
                SUMMARY('strati-legend-summary', H3('', tr.geo('lithoLegend'))),
                renderLithology()
            ),
            DETAILS(
                {
                    className: 'hydro-legend-details',
                    open: true,
                },
                SUMMARY('hydro-legend-summary', H3('', tr.geo('hydroLegend'))),
                renderHydroLegend()
            )
        )
    );

const printButton = () =>
    BUTTON(
        {
            className: 'geo-btn geo-btn--2 geo-btn--cssprint',
            onClick: navigatePrint,
        },
        SPAN('btn__icon', nameToString('print')),
        SPAN('btn__label', tr.geo('printAnalysis'))
    );

const renderSidebarHeader = () =>
    DIV(
        'sidebar-header',
        getSystem().map(s => renderToggleSystem(['close', 'open'], s))
    );

const wrapTop = (...nodes: NodeOrOptional[]) => DIV('sidebar--top', ...nodes);

export const renderSidebarTopContent = () =>
    getOrientation() === 'horizontal'
        ? wrapTop(
              renderSidebarHeader(),
              renderText(),
              renderBothSystemsButton(),
              goToSmartGeoTherm(),
              renderStratiLegend(),
              renderMinimapWrapper()
          )
        : wrapTop(
              renderSidebarHeader(),
              renderText(),
              renderMinimapWrapper(),
              renderBothSystemsButton(),
              goToSmartGeoTherm()
          );

const wrapBottom = (...nodes: NodeOrOptional[]) =>
    DIV('sidebar--bottom', ...nodes);

export const renderSidebarBottomContent = () =>
    getOrientation() === 'horizontal'
        ? wrapBottom(
              btnWrapper(),
              markdown(tr.geo('disclaimerSidebar'), 'disclaimer md md--small')
          )
        : wrapBottom(
              renderStratiLegend(),
              btnWrapper(),
              markdown(tr.geo('disclaimerSidebar'), 'disclaimer md md--small')
          );

export const renderGeoSidebar = () =>
    DIV(
        'sidebar sidebar-general',
        renderSidebarTopContent(),
        renderSidebarBottomContent()
    );

logger('loaded');
