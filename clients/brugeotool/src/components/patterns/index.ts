import { __forceRefreshState } from 'sdi/app';
import { Option, some, none, fromNullable } from 'fp-ts/lib/Option';
import { Collection } from 'sdi/util';

import pat602 from './pat-602';
import pat607 from './pat-607';
import pat616 from './pat-616';
import pat619 from './pat-619';
import pat620 from './pat-620';
import pat626 from './pat-626';
import pat705 from './pat-705';
// import pat706 from './pat-706';
// import pat706000 from './pat-706000';
// import pat706001 from './pat-706001';
import patDefault from './default';

const getPatternSVG = (id: number) => {
    switch (id) {
        case 602:
            return pat602;
        case 607:
            return pat607;
        case 616:
            return pat616;
        case 619:
            return pat619;
        case 620:
            return pat620;
        case 626:
            return pat626;
        case 705:
            return pat705;
        // case 706:
        //     return pat706;
        // case 706000:
        //     return pat706000;
        // case 706001:
        //     return pat706001;
        default:
            return patDefault;
    }
};

const patterns: Collection<CanvasPattern> = {};
// let inFlight: number[] = [];

export const getPatternUrl = (patternId: number) => {
    const blob = new Blob([getPatternSVG(patternId)], {
        type: 'image/svg+xml',
    });
    return URL.createObjectURL(blob);
};

const makePattern = (ctx: CanvasRenderingContext2D, patternId: number) => {
    // if (inFlight.indexOf(patternId) < 0) {
    //     inFlight.push(patternId);
    const img = new Image();
    const url = getPatternUrl(patternId);
    img.src = url;
    img.addEventListener(
        'load',
        () => {
            URL.revokeObjectURL(url);
            fromNullable(ctx.createPattern(img, 'repeat'))
                .map(pattern => {
                    patterns[patternId] = pattern;
                    __forceRefreshState();
                })
                .getOrElseL(() =>
                    window.setTimeout(() => makePattern(ctx, patternId), 16)
                );
        },
        { once: true }
    );
    // }
};

export const getPattern = (
    ctx: CanvasRenderingContext2D,
    patternId: number
): Option<CanvasPattern> => {
    const key = patternId.toString();
    if (key in patterns) {
        return some(patterns[key]).map(pat => {
            // some have had fun with them behind our back -pm
            pat.setTransform(new DOMMatrix([1, 0, 0, 1, 0, 0]));
            return pat;
        });
    }
    makePattern(ctx, patternId);
    return none;
};

// /**
//  * We used to have a rather bad problem with patterns being often null.
//  * This might-seem-complicated setup tries to solve this issue, and as
//  * far as I can tell manages to fix it. If you find better, you're my guess
//  * - pm
//  */
// const __getPattern = iife(() => {
//     const patterns: Collection<CanvasPattern> = {};
//     let inFlight: number[] = [];

//     const makePattern = (ctx: CanvasRenderingContext2D, patternId: number) => {
//         const img = new Image();
//         const blob = new Blob([getPatternSVG(patternId)], {
//             type: 'image/svg+xml',
//         });
//         const url = URL.createObjectURL(blob);
//         img.src = url;
//         return ctx.createPattern(img, 'repeat');
//     };

//     const backgroundLoad = (
//         ctx: CanvasRenderingContext2D,
//         patternId: number
//     ) => {
//         if (inFlight.indexOf(patternId) < 0) {
//             inFlight.push(patternId);
//             const key = patternId.toString();
//             let tries = 0;
//             const i = window.setInterval(() => {
//                 const pattern = makePattern(ctx, patternId);
//                 if (pattern !== null) {
//                     patterns[key] = pattern;
//                     window.clearInterval(i);
//                     inFlight = inFlight.filter(id => id !== patternId);
//                     hackyReset();
//                 } else if (tries > 12) {
//                     window.clearInterval(i);
//                     inFlight = inFlight.filter(id => id !== patternId);
//                 } else {
//                     tries += 1;
//                 }
//             }, 120);
//         }
//     };

//     return (
//         ctx: CanvasRenderingContext2D,
//         patternId: number
//     ): Option<CanvasPattern> => {
//         const key = patternId.toString();
//         if (key in patterns) {
//             return some(patterns[key]);
//         }
//         const pattern = makePattern(ctx, patternId);

//         if (pattern !== null) {
//             patterns[key] = pattern;
//             return some(pattern);
//         } else {
//             backgroundLoad(ctx, patternId);
//         }

//         return none;
//     };
// });
