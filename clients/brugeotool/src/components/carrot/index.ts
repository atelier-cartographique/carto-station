import { engine } from './engine';
import { DIV, IMG } from 'sdi/components/elements';

import { getCarrotWidth, getDiagramHeight } from '../../queries/geothermie';

const { getImage } = engine();

export const renderCarrot = () =>
    DIV(
        { className: 'carrot ' },
        IMG({
            src: getImage(getCarrotWidth(), getDiagramHeight()),
            alt: 'geologic carrot',
        })
    );

export default renderCarrot;
