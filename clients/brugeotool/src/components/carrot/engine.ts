import * as debug from 'debug';

import {
    getLayersThicknesses,
    getLayersTotalDepth,
    getHydroLayerDepth,
    getSystem,
    getLayerDepth,
    getLayerId,
    getHydroLayerId,
    getHydroLayersThicknesses,
    getElevation,
    getPhreaticHeadDepth,
    getLayersSat,
    getHydroLayersSat,
    getQuaternaireUS11_12Thickness,
    getQuaternaireUS14Thickness,
    getUH_RBC1aThicknessFactor,
    getUH_RBC1bThicknessFactor,
    getGreyShades,
    getQuaternaireThicknesses,
    getGreyShadeQuaternaire,
    getPatternIdQuaternaire,
} from 'brugeotool/src/queries/geothermie';
import {
    CANONICAL_PATTERN_UH_BASEROCK,
    CANONICAL_PATTERN_US,
    US_GREY_SHADE,
} from '../settings';
import { getPattern } from '../patterns';
import { GraphXY, newGraphXY } from '../table/graph';
import { formatDepth } from '../table/format';
import { GRAPH_TOP_MARGIN, TICK_LENGTH } from '../table/engine';
import { getPersistentLayout, getLayout } from '../../queries/app';
import { Layout } from '../../events/route';

import { Option } from 'fp-ts/lib/Option';

import { GeoMessageKey } from 'brugeotool/src/locale';

import { updateConnect } from 'brugeotool/src/events/geothermie';

import { drawYAxisTick, writeYAxisLabels } from './axes';

const logger = debug('sdi:carrot/engine');

export const color = {
    white: '#FFF',
    black: '#000000',
    red: '#C93814',
    blue: '#006B93',
    light_blue: '#0099e6',
    grass: '#8DB63C',
    darkgray: '#777',
    building: '#9B9B9B',
    groundL1: '#DEB887',
    groundR1: '#D2B48C',
    groundL2: '#CD853F',
    groundR2: '#C7833E',
    transparent: '#FFFFFF00',
    roof: '#FF0000',
    saturated: 'rgba(125, 174, 237, 0.4)',
    saturated_wo_transp: '#CBDFF7',
};

/**
 * Get the ratio between the undergrond depth and the canvas height (unit : m / px, ie meter by pixel)
 *
 * @param canvasHeight the height of the canvas
 * @param buildingHeight the heigth taken by the building
 */
export function getDepthPixelRatio(
    canvasHeight: number,
    buildingHeight: number
) {
    const sumHeight = getLayersTotalDepth();
    const carrotHeight = canvasHeight - buildingHeight;

    return sumHeight / carrotHeight;
}

const withContext = (ctx: CanvasRenderingContext2D) => {
    /**
     * Compute the position of a new vertex from
     *   - a vertex, ({x0}, {y0})
     *   - a distance, {distance}
     *   - an angle, {angle}
     * such that :
     *
     * (x0, y0) ----------
     *         \ ) angle
     *          \
     *           \
     *            newVertex
     * ({distance} is the distance between ({x0}, {y0}) and the new vertex ({newVertex}))
     */
    function newVertex(
        x0: number,
        y0: number,
        distance: number,
        angle: number
    ): GraphXY {
        return {
            x: x0 + distance * Math.cos(angle),
            y: y0 + distance * Math.sin(angle),
        };
    }

    // function drawPoint(pt: GraphXY, color: string) {
    //     ctx.fillStyle = color;
    //     ctx.fillRect(pt.x, pt.y, 10, 10);
    // }

    /**
     * Draw a polygon with a pattern
     *
     * @param vertices
     * @param patternUrl
     * @param angle
     */
    function drawPolyPattern(
        vertices: GraphXY[],
        patternId: number,
        angle: number,
        strokeColor: string | null = null
    ) {
        // if (patternId === 706) {
        //     if (angle < 0) {
        //         // right
        //         patternId = 706001;
        //         angle = 0;
        //     }
        //     if (angle > 0) {
        //         // left
        //         patternId = 706000;
        //         angle = 0;
        //     }
        // }

        getPattern(ctx, patternId).map(pattern => {
            ctx.save();
            ctx.beginPath();
            ctx.moveTo(vertices[0].x, vertices[0].y);

            for (let i = 1; i < vertices.length; i += 1) {
                ctx.lineTo(vertices[i].x, vertices[i].y);
            }

            if (patternId !== CANONICAL_PATTERN_UH_BASEROCK) {
                ctx.rotate(angle);
            }

            ctx.closePath();
            ctx.fillStyle = pattern;
            ctx.fill();

            if (strokeColor) {
                ctx.strokeStyle = strokeColor;
                ctx.stroke();
            }

            ctx.restore();
        });
    }

    /**
     * Draw a polygon
     *
     * @param vertices The array of the vertices of the polygon
     * @param color The color of the polygon
     */
    function drawPolygon(
        vertices: GraphXY[],
        fillColor: string | null,
        strokeColor: string | null = 'black'
    ) {
        ctx.save();

        ctx.beginPath();
        ctx.moveTo(vertices[0].x, vertices[0].y);

        for (let i = 1; i < vertices.length; i++) {
            ctx.lineTo(vertices[i].x, vertices[i].y);
        }

        ctx.closePath();
        if (fillColor !== null) {
            ctx.fillStyle = fillColor;
            ctx.fill();
        }

        if (strokeColor !== null) {
            ctx.strokeStyle = strokeColor;
            ctx.stroke();
        }

        ctx.restore();
    }

    /**
     * Return the array [V0, v1, V2, V3]
     *
     * see drawLosange for the definition of the vi
     */
    function getLosangeVertices(
        x0: number,
        y0: number,
        distanceV0V1: number,
        angle: number
    ) {
        const v0 = newGraphXY(x0, y0);
        const v1 = newVertex(x0, y0, distanceV0V1, angle);
        const v2 = newGraphXY(x0 + 2 * distanceV0V1 * Math.cos(angle), y0);
        const v3 = newVertex(x0, y0, distanceV0V1, -angle);

        return [v0, v1, v2, v3];
    }

    /**
     *  Draw a losange such that (if {angle} is > 0, with {v0} = ({x0}, {y0})
     *
     *        v3
     *        /
     *      /
     *    /  ) angle
     * v0 ---------- v2
     *    \ ) angle
     *     \
     *      \
     *       V1
     *
     * with distance({v0}, {v1}) = dist({v0}, {v3}) = {distanceV0V1}
     */
    function drawLosange(
        x0: number,
        y0: number,
        distanceV0V1: number,
        angle: number,
        color: string
    ) {
        const v = getLosangeVertices(x0, y0, distanceV0V1, angle);
        drawPolygon(v, color);

        // if (false) {
        //     // for debug
        //     drawPoint(v[0], 'fuchsia');
        //     drawPoint(v[1], 'aqua');
        //     drawPoint(v[2], 'orangered');
        //     drawPoint(v[3], 'greenyellow');
        // }
    }

    function _drawPipeVertex(
        v0: GraphXY,
        v1: GraphXY,
        angle: number,
        externalBound: number,
        internalBound: number
    ) {
        /**
         * v0: vertex on the building edge
         * v1: vertex on the carrot edge
         */
        return {
            v0LL: newVertex(v0.x, v0.y, -externalBound, -angle),
            v0LR: newVertex(v0.x, v0.y, -internalBound, -angle),
            v0RL: newVertex(v0.x, v0.y, internalBound, -angle),
            v0RR: newVertex(v0.x, v0.y, externalBound, -angle),
            v1LL: newVertex(v1.x, v1.y, -externalBound, -angle),
            v1LR: newVertex(v1.x, v1.y, -internalBound, -angle),
            v1RL: newVertex(v1.x, v1.y, internalBound, -angle),
            v1RR: newVertex(v1.x, v1.y, externalBound, -angle),
        };
    }

    /**
     *
     * @param vBL Left vertex (black color) at the bottum of the crepine
     * @param vBR Right vertex (black color) at the bottum of the crepine
     * @param crepineProf Deph (profondeur) of the crepine
     * @param crepineStep Height of each step of the crepine
     */
    function _drawCrepine(
        vBL: GraphXY,
        vBR: GraphXY,
        crepineProf = 0,
        crepineStep = 5
    ) {
        //  | b |   c   | b |    ^
        //  | l |   o   | l |    |
        //  | a |   l   | a |    | crepineProf
        //  | c |   o   | c |    |
        //  | k |   r   | k |    v
        // vBL             vBR

        if (crepineProf > 0) {
            // draw crepine
            const numberSteps: number = crepineProf / crepineStep;

            // draw crepine as horizontal line
            for (let i = 0; i < numberSteps; i++) {
                ctx.beginPath();
                ctx.moveTo(vBL.x, vBL.y - crepineProf + i * crepineStep);
                ctx.lineTo(vBR.x, vBR.y - crepineProf + i * crepineStep);
                ctx.strokeStyle = color.black;
                ctx.stroke();
            }
        }
    }

    /**
     * Draw the pipes system as open - outflux (pipe on the left : ^)
     */
    function drawOpenPipeOut(
        v0: GraphXY,
        v1: GraphXY,
        prof: number,
        angle: number,
        pipeWidth: number,
        layerThickness: number
    ) {
        //  |       |      |       |
        //  |   ^   |      |   v   |
        //  |       |      |       |
        //  viLL viLR  vi  viRL ViRR
        //

        const vb = _drawPipeVertex(
            v0,
            v1,
            angle,
            pipeWidth * 6 + 1,
            pipeWidth * 3 - 1
        ); // black
        const vc = _drawPipeVertex(v0, v1, angle, pipeWidth * 6, pipeWidth * 3); // color

        drawPolygon(
            [
                vb.v0LL,
                vb.v1LL,
                newGraphXY(vb.v1LL.x, vb.v1LL.y + prof),
                newGraphXY(vb.v1LR.x, vb.v1LR.y + prof),
                vb.v1LR,
                vb.v0LR,
            ],
            color.black,
            color.transparent
        );

        drawPolygon(
            [
                vc.v0LL,
                vc.v1LL,
                newGraphXY(vc.v1LL.x, vc.v1LL.y + prof),
                newGraphXY(vc.v1LR.x, vc.v1LR.y + prof),
                vc.v1LR,
                vc.v0LR,
            ],
            color.red,
            color.transparent
        ); // TODO: changer ceci pour deplacer la pipe sur le cote

        _drawCrepine(
            newGraphXY(vc.v1LL.x, vc.v1LL.y + prof),
            newGraphXY(vc.v1LR.x, vc.v1LR.y + prof),
            layerThickness
        );
    }

    /**
     * Draw the pipes system as open - influx (pipe on the right : v)
     */
    function drawOpenPipeIn(
        v0: GraphXY,
        v1: GraphXY,
        prof: number,
        angle: number,
        pipeWidth: number,
        layerThickness: number
    ) {
        //  |       |      |       |
        //  |   ^   |      |   v   |
        //  |       |      |       |
        //  viLL viLR  vi  viRL ViRR
        //
        const vb = _drawPipeVertex(
            v0,
            v1,
            angle,
            pipeWidth * 6 + 1,
            pipeWidth * 3 - 1
        ); // black
        const vc = _drawPipeVertex(v0, v1, angle, pipeWidth * 6, pipeWidth * 3); // color

        drawPolygon(
            [
                vb.v0RL,
                vb.v1RL,
                newGraphXY(vb.v1RL.x, vb.v1RL.y + prof),
                newGraphXY(vb.v1RR.x, vb.v1RR.y + prof),
                vb.v1RR,
                vb.v0RR,
            ],
            color.black,
            color.transparent
        );

        drawPolygon(
            [
                vc.v0RL,
                vc.v1RL,
                newGraphXY(vc.v1RL.x, vc.v1RL.y + prof),
                newGraphXY(vc.v1RR.x, vc.v1RR.y + prof),
                vc.v1RR,
                vc.v0RR,
            ],
            color.blue,
            color.transparent
        );

        _drawCrepine(
            newGraphXY(vc.v1RL.x, vc.v1RL.y + prof),
            newGraphXY(vc.v1RR.x, vc.v1RR.y + prof),
            layerThickness
        );
    }

    function drawOpenPipeSystem(
        v0: GraphXY,
        v1: GraphXY,
        depth: number,
        angle: number,
        distanceBuilding: number,
        distanceV0V1: number,
        layerThickness: number
    ) {
        /**
         * Draw the pipes system as open
         *
         * @param {string} v0 - Origin of the building (building bottom)
         * @param {string} v1 - Origin of the carrot (carrot bottom)
         * @param {string} depth - Depth of the pipe (in pixels)
         * @param {string} angle
         * @param {string} distanceBuilding - diagonal of the building (in pixels)
         * @param {string} distanceV0V1 - diagonal of the carrot (in pixels)
         * @param {string} layerThickness - thickness of the last layer (in pixels)
         */
        const pLB = newVertex(v0.x, v0.y, -distanceBuilding * 0.8, angle); // origin of the pipe on the left border of the building
        const pLC = newVertex(v1.x, v1.y, -distanceV0V1 * 0.6, angle); // inflexion point of the pipe on the left border of the carrot
        const pRB = newVertex(v0.x, v0.y, distanceBuilding * 0.8, -angle); // origin of the pipe on the right border of the building
        const pRC = newVertex(v1.x, v1.y, distanceV0V1 * 0.6, -angle); // inflexion point of the pipe on the right border of the carrot

        const pipeWidth = 2;

        drawOpenPipeIn(pLB, pLC, depth, -angle, pipeWidth, layerThickness);
        drawOpenPipeOut(pRB, pRC, depth, angle, pipeWidth, layerThickness);
    }

    /**
     * Draw a closed pipe (linked at bottum)
     */
    function drawClosedPipe(
        v0: GraphXY,
        v1: GraphXY,
        prof: number,
        angle: number
    ) {
        function drawInt(
            externalBound: number,
            internalBound: number,
            color: string,
            onlyRightPart: boolean
        ) {
            //  |       |      |       |
            //  |   ^   |      |   v   |
            //  |       |      |       |
            //  viLL viLR  vi  viRL ViRR
            //  |       |______|       |
            //  |______________________|

            const v0LL = newVertex(v0.x, v0.y, -externalBound, -angle);
            const v0LR = newVertex(v0.x, v0.y, -internalBound, -angle);
            const v0RL = newVertex(v0.x, v0.y, internalBound, -angle);
            const v0RR = newVertex(v0.x, v0.y, externalBound, -angle);

            const v1LL = newVertex(v1.x, v1.y, -externalBound, -angle);
            const v1LR = newVertex(v1.x, v1.y, -internalBound, -angle);
            const v1RL = newVertex(v1.x, v1.y, internalBound, -angle);
            const v1RR = newVertex(v1.x, v1.y, externalBound, -angle);

            ctx.beginPath(); // todo : simplify using the function drawPolygon

            // exterieur right
            ctx.moveTo(v0RR.x, v0RR.y);
            ctx.lineTo(v1RR.x, v1RR.y);
            ctx.lineTo(v1RR.x, v1RR.y + prof);

            // boucle exterieur
            ctx.lineTo((v1.x + v1RR.x) / 2, v1.y + prof + externalBound * 0.8);
            ctx.lineTo(v1.x, v1.y + prof + externalBound * 0.9);

            if (!onlyRightPart) {
                // fin boucle exterieur
                ctx.lineTo((v1.x + v1LL.x) / 2, v1.y + prof + externalBound);

                // exterieur left
                ctx.lineTo(v1LL.x, v1LL.y + prof);
                ctx.lineTo(v1LL.x, v1LL.y);
                ctx.lineTo(v0LL.x, v0LL.y);

                // interieur left
                ctx.lineTo(v0LR.x, v0LR.y);
                ctx.lineTo(v1LR.x, v1LR.y);
                ctx.lineTo(v1LR.x, v1LR.y + prof);

                // boucle interieur
                ctx.lineTo(
                    (v1.x + v1LR.x) / 2,
                    v1.y + prof + internalBound / 1.3
                );
            }

            // fin boucle interieur
            ctx.lineTo(v1.x, v1.y + prof + internalBound / 1.6);
            ctx.lineTo((v1.x + v1RL.x) / 2, v1.y + prof + internalBound / 2);

            // interieur right
            ctx.lineTo(v1RL.x, v1RL.y + prof);
            ctx.lineTo(v1RL.x, v1RL.y);
            ctx.lineTo(v0RL.x, v0RL.y);

            // close
            ctx.closePath();

            ctx.fillStyle = color;
            ctx.fill();
        }

        drawInt(7, 2, color.black, false);
        drawInt(6, 3, color.red, false);
        drawInt(6, 3, color.blue, true);
    }

    /**
     * Draw a side of a level of the carrot
     */
    function drawLevelSide(
        x0: number,
        y0: number,
        height: number,
        distanceV0V1: number,
        angle: number,
        fColor: string | null,
        patternId: number | null = null,
        strokeColor: string | null = null
    ) {
        const v0 = newGraphXY(x0, y0);
        const v1 = newVertex(x0, y0, distanceV0V1, angle);
        const v1_height = newGraphXY(v1.x, v1.y + height);
        const v0_height = newGraphXY(x0, y0 + height);

        if (fColor !== null) {
            /*
            let colorStroke: string;
            if (fColor.startsWith('rgba')) {
                // always saturate the stroke color if color code looks like rgba(125, 174, 237, 0.4)'
                colorStroke = [
                    fColor.split(',')[0],
                    fColor.split(',')[1],
                    fColor.split(',')[2],
                    '1)'
                ].join(', ');
            } else {
                colorStroke = fColor;
            }
            */
            drawPolygon([v0, v1, v1_height, v0_height], fColor, strokeColor);
        }

        if (patternId !== null) {
            drawPolyPattern(
                [v0, v1, v1_height, v0_height],
                patternId,
                angle,
                color.darkgray
            );
        }
    }

    /**
     * Draw a level of the carrot as follows
     *
     * (v0)
     * |  \       /\    --
     * |   \    /  \    height
     * |    \ /    \    --
     *  \   (v1)  /
     *    \  \  /
     *      \\/
     */
    function drawLevel(
        x0: number,
        y0: number,
        height: number,
        distanceV0V1: number,
        angle: number,
        colorLeft: string | null,
        colorRight: string | null,
        patternId: number | null = null,
        strokeColor: string | null = null
    ) {
        drawLevelSide(
            x0,
            y0,
            height,
            distanceV0V1,
            angle,
            colorLeft,
            patternId,
            strokeColor
        );
        drawLevelSide(
            x0 + distanceV0V1 * Math.cos(angle),
            y0 + distanceV0V1 * Math.sin(angle),
            height,
            distanceV0V1,
            -angle,
            colorRight,
            patternId,
            strokeColor
        );
    }

    /**
     * Draw a curved line form v0 to v1
     */
    function drawCurvedLine(v0: GraphXY, v1: GraphXY) {
        const strength = 50;
        ctx.beginPath();
        ctx.moveTo(v0.x, v0.y);
        ctx.bezierCurveTo(
            v0.x + strength,
            v0.y,
            v1.x - strength,
            v1.y - strength / 2,
            v1.x,
            v1.y
        );
        ctx.strokeStyle = 'black';
        ctx.stroke();
    }

    /**
     * Draw the underground levels
     *
     * @param refPoint the point where to draw the first level
     * @param l a table with the heigth of each level (from top to bottom)
     * @param canvasHeight the height of the canvas
     * @param buildingHeight the heigth taken by the building // where to start to draw the underground (ordonnees)
     * @param withLineToArray if true draws the lines to array
     * @param distanceV0V1 The distance for V0 to V1 of front face of the building
     */
    function drawUndergroundLevels(
        refPoint: GraphXY,
        l: Option<number[]>,
        canvasHeight: number,
        buildingHeight: number,
        withLineToArray: boolean,
        distanceV0V1: number
    ) {
        const minHeight = 25; // choix de hauteur min nimale (pour tableau) en sortie // display -- option
        const ratio = getDepthPixelRatio(canvasHeight, buildingHeight); // ratioMeterPixel

        l.map(levels => {
            // draw line to array
            if (withLineToArray) {
                drawCurvedLine(
                    newGraphXY(0, buildingHeight),
                    newGraphXY(refPoint.x, buildingHeight)
                );
            }

            let totalHeight = buildingHeight;
            let totalHeightWithMin = buildingHeight;
            levels.forEach((lev, i) => {
                // const colorLeft = STRATIGRAPHY_COLORS[i];
                // const colorRight = STRATIGRAPHY_COLORS[i];

                let height = lev / ratio;
                if (i == levels.length - 1) {
                    // we draw the base of the carrot
                    height = 500;
                }

                const heightWithMin =
                    height < minHeight && lev != 0 ? minHeight : lev / ratio; // todo dans une fonction

                // if (getLayout() === 'technical' || getLayout() === 'general' || getLayout() === 'license' || getLayout() === 'geology') {
                const patternId = CANONICAL_PATTERN_US[i];

                const grey = getGreyShades()[i];

                // draw level with pattern and shade of grey
                if (i === 0) {
                    // display quaternaire sublayer
                    const firstQuaternaireLayerThickness =
                        height * getUH_RBC1aThicknessFactor();
                    if (
                        getQuaternaireUS11_12Thickness().fold(0, t => t) !== 0
                    ) {
                        // usrbc11_12
                        drawLevel(
                            refPoint.x,
                            totalHeight,
                            firstQuaternaireLayerThickness,
                            distanceV0V1,
                            Math.PI / 6,
                            getGreyShadeQuaternaire(0).left,
                            getGreyShadeQuaternaire(0).right,
                            getPatternIdQuaternaire(0),
                            color.darkgray
                        );
                    } else {
                        // usrbc11_13
                        drawLevel(
                            refPoint.x,
                            totalHeight,
                            firstQuaternaireLayerThickness,
                            distanceV0V1,
                            Math.PI / 6,
                            getGreyShadeQuaternaire(1).left,
                            getGreyShadeQuaternaire(1).right,
                            getPatternIdQuaternaire(1),
                            color.darkgray
                        );
                    }
                    if (getQuaternaireUS14Thickness().fold(0, t => t) !== 0) {
                        // usrbc11_14
                        drawLevel(
                            refPoint.x,
                            totalHeight + firstQuaternaireLayerThickness,
                            height * getUH_RBC1bThicknessFactor(),
                            distanceV0V1,
                            Math.PI / 6,
                            getGreyShadeQuaternaire(2).left,
                            getGreyShadeQuaternaire(2).right,
                            getPatternIdQuaternaire(2),
                            color.darkgray
                        );
                    }
                } else {
                    drawLevel(
                        refPoint.x,
                        totalHeight,
                        height,
                        distanceV0V1,
                        Math.PI / 6,
                        // patternId !== CANONICAL_PATTERN_UH_BASEROCK
                        //     ? grey.left
                        //     : null,
                        // patternId !== CANONICAL_PATTERN_UH_BASEROCK
                        //     ? grey.right
                        //     : null,
                        grey.left,
                        grey.right,
                        patternId,
                        color.darkgray
                    );
                }

                // draw saturated level
                const satHeight = height * getLayersSat()[i];

                if (
                    US_GREY_SHADE[i] !== 'dark'
                    // && CANONICAL_PATTERN_US[i] !== CANONICAL_PATTERN_UH_BASEROCK
                ) {
                    drawLevel(
                        refPoint.x,
                        totalHeight + height - satHeight,
                        satHeight,
                        distanceV0V1,
                        Math.PI / 6,
                        color.saturated,
                        color.saturated,
                        null,
                        null
                    );
                }

                // } else {
                //     drawLevel(refPoint.x, totalHeight, height, distanceV0V1, Math.PI / 6, null, null, null);
                // }

                totalHeight = totalHeight + height;
                totalHeightWithMin = totalHeightWithMin + heightWithMin;

                if (i != levels.length - 1) {
                    // we do draw line for the base of the carrot
                    if (withLineToArray && height != 0) {
                        drawCurvedLine(
                            newGraphXY(0, totalHeightWithMin),
                            newGraphXY(refPoint.x, totalHeight)
                        );
                    }
                }
            });
        });
    }

    function drawPhreaticHead(vRef: GraphXY) {
        /*
         * @param vRef Reference point where we start writing
         */
        const x0 = vRef.x;
        const y0 = vRef.y;

        // draw the phreatic symbol
        ctx.beginPath();
        ctx.moveTo(x0 - 14, y0 - 12);
        ctx.lineTo(x0 - 2, y0 - 12);
        ctx.lineTo(x0 - 8, y0);
        ctx.lineTo(x0 - 14, y0 - 12);
        ctx.closePath();
        ctx.fillStyle = color.blue;
        ctx.fill();

        ctx.beginPath();
        ctx.moveTo(x0 - 16, y0);
        ctx.lineTo(x0, y0);
        ctx.moveTo(x0 - 13, y0 + 4);
        ctx.lineTo(x0 - 3, y0 + 4);
        ctx.moveTo(x0 - 10, y0 + 8);
        ctx.lineTo(x0 - 6, y0 + 8);
        ctx.closePath();
        ctx.strokeStyle = color.blue;
        ctx.stroke();

        // draw the phreatic depth value
        ctx.fillStyle = color.blue;
        ctx.font = 'Bold 12px';
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        ctx.fillText(formatDepth(-getPhreaticHeadDepth()), x0 - 10, y0 - 30);
        ctx.fillText(` m`, x0 - 10, y0 - 20);
    }

    /**
     * write elevation (altitude) on top of the carrot
     *
     */
    function writeElevation(vRef: GraphXY, distanceV0V1: number) {
        /*
         * @param vRef Reference point where we start writing
         */
        ctx.fillStyle = 'black';
        ctx.font = '12px';
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        ctx.fillText(`z = ${getElevation().toFixed(1)} m-DNG`, vRef.x, vRef.y);

        // write a decorative line
        ctx.beginPath();
        ctx.moveTo(vRef.x + distanceV0V1 / 3, vRef.y);
        ctx.lineTo(vRef.x + distanceV0V1 - 5, vRef.y);
        ctx.closePath();
        ctx.strokeStyle = 'black';
        ctx.lineWidth = 0.5;
        ctx.stroke();
    }

    function getDepthForPipes(
        layerNames: GeoMessageKey[],
        system: string
    ): number {
        /**
         * Get the depth (profondeur) to draw the pipes
         * For the close system, the depth is at the middle of the
         * first layer which has not its thickness = 0.
         * For the layer 'US/RBC92' (bedrock), the depth is computed to
         * arrive at the 1/4 of the bedrock thickness
         *
         * For the open system, the depth is at the bottom of the
         * first layer which has not its saturated thickness > 5.
         * @param layerNames The names of the layers on which the search is done (from left to right).
         */
        const layerName = layerNames[0];

        if (system === 'close') {
            const layerId = getLayerId(layerName);
            const layerThickness = getLayersThicknesses().fold(
                0,
                h => h[layerId]
            );
            if (layerThickness > 0 || layerNames.length === 1) {
                const depth = getLayerDepth(layerName);
                if (layerName === 'US/RBC_92') {
                    // BEDROCK
                    return depth + layerThickness / 4;
                } else {
                    return depth + layerThickness / 2;
                }
            } else {
                return getDepthForPipes(layerNames.slice(1), system);
            }
        } else {
            const layerId = getHydroLayerId(layerName);
            const layerThickness = getHydroLayersThicknesses()[layerId];
            const layerSatThickness =
                layerThickness * getHydroLayersSat()[layerId];
            if (layerSatThickness > 5 || layerNames.length === 1) {
                const depth = getHydroLayerDepth(layerName);
                return depth + layerThickness;
            } else {
                return getDepthForPipes(layerNames.slice(1), system);
            }
        }
    }

    function getLayerThicknessForPipes(
        layerNames: GeoMessageKey[],
        system: string
    ): number {
        /**
         * Get the layer thickness of the first layer thicker than 5 meter
         *
         * @param layerNames The names of the layers on which the search is done (from left to right).
         */
        const layerName = layerNames[0];
        if (system === 'close') {
            const layerId = getLayerId(layerName);
            const layerThickness = getLayersThicknesses().fold(
                0,
                h => h[layerId]
            );
            if (layerThickness > 0 || layerNames.length === 1) {
                if (layerName === 'US/RBC_92') {
                    // BEDROCK
                    return layerThickness / 4;
                } else {
                    return layerThickness;
                }
            } else {
                return getLayerThicknessForPipes(layerNames.slice(1), system);
            }
        } else {
            const layerId = getHydroLayerId(layerName);
            const layerThickness = getHydroLayersThicknesses()[layerId];
            const layerSatThickness =
                layerThickness * getHydroLayersSat()[layerId];
            if (layerSatThickness > 5 || layerNames.length === 1) {
                return layerThickness;
            } else {
                return getLayerThicknessForPipes(layerNames.slice(1), system);
            }
        }
    }

    /**
     * Draw the carrot
     */
    function draw(
        width: number,
        height: number,
        layout: Layout,
        system: string
    ) {
        const ratioX = width / 400;
        const distanceV0V1 = 190 * ratioX;
        const angle = Math.PI / 6;
        const x0 = 0.15 * width; // 100 * ratioX; // absice (or x) of the reference point (left corner of the grass)
        const y0 = 99; // coordinate (or y) of the reference point (left corner of the grass)
        const refPoint = newGraphXY(x0, y0);

        const v1 = newVertex(x0, y0, distanceV0V1, angle);
        const buildingX = x0 + ((v1.x - x0) / 3) * 2;
        const buildingY = y0 - 30;

        const buildingWallHeight = 30;

        ctx.clearRect(0, 0, width, height);
        // draw the buidling
        const distanceB = distanceV0V1 / 3;

        // underground levels
        const canvasHeight = height;
        drawUndergroundLevels(
            refPoint,
            getLayersThicknesses(),
            canvasHeight,
            refPoint.y,
            false /*layout == 'geology'*/,
            distanceV0V1
        );

        // Draw the ground level
        if (layout == 'general' || layout == 'license' || layout == 'finance') {
            // Compute building position
            const buildingTopV = getLosangeVertices(
                buildingX,
                buildingY,
                distanceB,
                Math.PI / 6
            );
            const betweenBuildingV0V1 = newVertex(
                buildingX,
                buildingY,
                distanceB / 2,
                angle
            );
            const houseRoofTopL = newVertex(
                betweenBuildingV0V1.x,
                betweenBuildingV0V1.y,
                20,
                -(Math.PI / 2)
            );
            const houseRoofTopR = newGraphXY(
                buildingTopV[3].x - buildingTopV[0].x + houseRoofTopL.x,
                buildingTopV[3].y - buildingTopV[0].y + houseRoofTopL.y
            );

            const buildingBottomV0 = newGraphXY(
                buildingTopV[0].x,
                buildingTopV[0].y + buildingWallHeight
            );
            const buildingBottomV1 = newGraphXY(
                buildingTopV[1].x,
                buildingTopV[1].y + buildingWallHeight
            );
            const buildingBottonV2 = newGraphXY(
                buildingTopV[2].x,
                buildingTopV[2].y + buildingWallHeight
            );

            const vPipe0 = newVertex(
                buildingBottomV1.x,
                buildingBottomV1.y,
                distanceB * 0.2,
                -angle
            );
            const vPipe1 = newVertex(v1.x, v1.y, distanceV0V1 * 0.4, -angle);

            const vPipe2 = newVertex(
                buildingBottomV1.x,
                buildingBottomV1.y,
                distanceB * 0.8,
                -angle
            );
            const vPipe3 = newVertex(v1.x, v1.y, distanceV0V1 * 0.6, -angle);

            const ratioMeterPixel = getDepthPixelRatio(canvasHeight, y0);

            // draw the grass
            drawLosange(x0, y0, distanceV0V1, Math.PI / 6, color.grass);

            if (system === 'close') {
                const prof = getDepthForPipes(['US/RBC_92'], system); // SOCLE
                drawClosedPipe(vPipe2, vPipe3, prof / ratioMeterPixel, angle);
                drawClosedPipe(vPipe0, vPipe1, prof / ratioMeterPixel, angle);
            } else {
                const prof = getDepthForPipes(
                    ['UH/RBC_4', 'UH/RBC_8a', 'UH/RBC_9b'],
                    system
                );
                const layerThickness = getLayerThicknessForPipes(
                    ['UH/RBC_4', 'UH/RBC_8a', 'UH/RBC_9b'],
                    system
                );
                drawOpenPipeSystem(
                    buildingBottomV1,
                    v1,
                    prof / ratioMeterPixel,
                    angle,
                    distanceB,
                    distanceV0V1,
                    layerThickness / ratioMeterPixel
                );
            }

            // Draw the roof
            drawPolygon(
                [
                    houseRoofTopL,
                    houseRoofTopR,
                    buildingTopV[3],
                    buildingTopV[0],
                ],
                color.roof
            );
            drawPolygon(
                [
                    buildingTopV[1],
                    buildingTopV[2],
                    houseRoofTopR,
                    houseRoofTopL,
                ],
                color.roof
            );

            // Draw the wall
            drawPolygon(
                [
                    buildingBottomV1,
                    buildingTopV[1],
                    buildingTopV[2],
                    buildingBottonV2,
                ],
                color.building
            );
            drawPolygon(
                [
                    buildingBottomV0,
                    buildingBottomV1,
                    buildingTopV[1],
                    houseRoofTopL,
                    buildingTopV[0],
                ],
                color.building
            ); // left part 5 colums

            drawYAxisTick(
                ctx,
                getLayersTotalDepth(),
                height - GRAPH_TOP_MARGIN,
                refPoint.x - TICK_LENGTH,
                refPoint.y
            );
            writeYAxisLabels(
                ctx,
                getLayersTotalDepth(),
                height - GRAPH_TOP_MARGIN,
                refPoint.x - TICK_LENGTH,
                refPoint.y
            );
            // draw m label
            ctx.fillStyle = 'black';
            ctx.font = '12px Arial';
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            ctx.fillText('m', refPoint.x - 3 * TICK_LENGTH, refPoint.y);
        } else {
            drawLosange(x0, y0, distanceV0V1, Math.PI / 6, color.transparent);

            // Write altitude and add phreatic head
            writeElevation(
                newGraphXY(x0 + distanceV0V1, refPoint.y),
                distanceV0V1
            );
            const depthPixelRatio = getDepthPixelRatio(
                canvasHeight,
                refPoint.y
            );
            // drawPhreaticHead(newGraphXY(width * 0.89, refPoint.y - getPhreaticHeadDepth() / depthPixelRatio));
            drawPhreaticHead(
                newGraphXY(
                    x0 - 4,
                    refPoint.y - getPhreaticHeadDepth() / depthPixelRatio
                )
            );

            drawYAxisTick(
                ctx,
                getLayersTotalDepth(),
                height - GRAPH_TOP_MARGIN,
                width - 2 * TICK_LENGTH,
                refPoint.y
            );
        }
    }
    return { draw };
};

export const adjustCarrotConnect = (height: number) => {
    const ratio = getDepthPixelRatio(height, 99);
    window.setTimeout(() => {
        const isPrint = getLayout() === 'print';
        // const isClosed = getSystem().fold(false, s => s === 'close')
        const isOpen = getSystem().fold(false, s => s === 'open');
        const persistentLayout = getPersistentLayout();
        if (persistentLayout === 'general' && !isPrint) {
            updateConnect('carrot', () => {
                const heights = getLayersThicknesses()
                    .getOrElse([])
                    .filter(t => t > 0)
                    .map(t => t / ratio);
                const offset = 99;
                return { offset, heights };
            });
        }
        if (persistentLayout === 'general' && isPrint && isOpen) {
            updateConnect('carrot', () => {
                const heights = getHydroLayersThicknesses()
                    .filter(t => t > 0)
                    .map(t => t / ratio);
                const offset = 99;
                return { offset, heights };
            });
        } else if (persistentLayout === 'geology') {
            updateConnect('carrot', () => {
                const heights = getHydroLayersThicknesses()
                    .filter(t => t > 0)
                    .map(t => t / ratio);
                const offset = 99;
                return { offset, heights };
            });
        } else if (persistentLayout === 'technical') {
            updateConnect('carrot', currentDepth =>
                getLayersThicknesses()
                    .map(ts => {
                        if (ts.length < 1) {
                            return currentDepth;
                        }
                        const [us11, us12, us13, us14] =
                            getQuaternaireThicknesses().map(v => v / ratio);
                        const heights = ts
                            .filter(t => t > 0)
                            .map(t => t / ratio);
                        const offset = 99;
                        const us1_123 = us11 + us12 + us13;
                        if (us1_123 > 0 && us14 > 0) {
                            return {
                                offset,
                                heights: [us1_123, us14, ...heights.slice(1)],
                            };
                        }

                        return { offset, heights };
                    })
                    .getOrElse(currentDepth)
            );
        }
    }, 124);
};

export const engine = () => {
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d')!;

    let ratio = 1;

    const getImage = (width: number, height: number) => {
        canvas.width = width;
        canvas.height = height;

        const newRatio = getDepthPixelRatio(height, 99);
        if (newRatio !== ratio) {
            ratio = newRatio;
        }
        adjustCarrotConnect(height);

        const system: string = getSystem().fold('', s => s);

        const layout = getLayout();
        withContext(ctx).draw(
            width,
            height,
            layout === 'print' ? getPersistentLayout() : layout,
            system
        );

        return canvas.toDataURL();
    };

    return { getImage };
};

logger('loaded');
