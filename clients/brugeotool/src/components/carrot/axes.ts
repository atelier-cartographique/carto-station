import { getDepthMode, getElevation } from 'brugeotool/src/queries/geothermie';
import { noNanButOne } from '../table/format';
import { TICK_LENGTH, HEIGHT_STEP } from '../table/engine';

const drawGraticuleY = (
    ctx: CanvasRenderingContext2D,
    yTick: number,
    x0: number,
    y0: number
) => {
    ctx.beginPath();
    ctx.moveTo(x0, y0 + yTick);
    ctx.lineTo(x0 + TICK_LENGTH, y0 + yTick);
    ctx.closePath();
    ctx.strokeStyle = 'black';
    ctx.stroke();
};

const writeGraticuleLabel = (
    ctx: CanvasRenderingContext2D,
    label: number,
    yTick: number,
    x0: number,
    y0: number
) => {
    ctx.fillStyle = 'black';
    ctx.font = '12px Arial';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.fillText(label.toString(), x0 - 18, y0 + yTick);
};

export const drawYAxisTick =
    /*
     * maxDepth : The depth in meter up to draw the axis to.
     * height: height in pixels of the axis
     * x0: x origin in pixels of the Y axis
     * y0: y origin in pixels of the Y axis
     */
    (
        ctx: CanvasRenderingContext2D,
        maxDepth: number,
        height: number,
        x0: number,
        y0: number
    ) => {
        const elevationPerHeightStep = getElevation() / HEIGHT_STEP;
        const diffDepthMode = () =>
            (elevationPerHeightStep - Math.floor(elevationPerHeightStep)) *
            HEIGHT_STEP;

        const scaleFactor = height / maxDepth;
        getDepthMode() === 'relative'
            ? [
                  ...Array(
                      noNanButOne(Math.ceil(maxDepth / HEIGHT_STEP))
                  ).keys(),
              ].map(i =>
                  drawGraticuleY(ctx, i * HEIGHT_STEP * scaleFactor, x0, y0)
              )
            : [
                  ...Array(
                      noNanButOne(Math.ceil(maxDepth / HEIGHT_STEP))
                  ).keys(),
              ].map(i =>
                  drawGraticuleY(
                      ctx,
                      (i * HEIGHT_STEP + diffDepthMode()) * scaleFactor,
                      x0,
                      y0
                  )
              );
    };

export const writeYAxisLabels = (
    ctx: CanvasRenderingContext2D,
    maxDepth: number,
    height: number,
    x0: number,
    y0: number
) => {
    const elevationPerHeightStep = getElevation() / HEIGHT_STEP;
    const diffDepthMode = () =>
        (elevationPerHeightStep - Math.floor(elevationPerHeightStep)) *
        HEIGHT_STEP;

    const scaleFactor = height / maxDepth;
    getDepthMode() === 'relative'
        ? [...Array(noNanButOne(Math.ceil(maxDepth / HEIGHT_STEP))).keys()].map(
              i =>
                  writeGraticuleLabel(
                      ctx,
                      i * HEIGHT_STEP,
                      i * HEIGHT_STEP * scaleFactor,
                      x0,
                      y0
                  )
          )
        : [...Array(noNanButOne(Math.ceil(maxDepth / HEIGHT_STEP))).keys()].map(
              i =>
                  writeGraticuleLabel(
                      ctx,
                      getElevation() - diffDepthMode() - i * HEIGHT_STEP,
                      (i * HEIGHT_STEP + diffDepthMode()) * scaleFactor,
                      x0,
                      y0
                  )
          );
};
