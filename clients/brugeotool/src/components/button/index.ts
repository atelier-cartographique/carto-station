import { getLayout } from '../../queries/app';

import {
    mapsSeparatorMoveDown,
    mapsSeparatorMoveUp,
    setDepthMode,
    setDepthScaling,
} from '../../events/geothermie';
import { navigateSystem } from '../../events/route';
import tr from 'sdi/locale';
import {
    InputAttributes,
    renderRadioOut,
    withChecked,
} from 'sdi/components/input';
import { Depth } from '../table/format';
import {
    getDisplayMapsSeparator,
    getDepthScaling,
} from 'brugeotool/src/queries/geothermie';
import { divTooltipBottom, divTooltipBottomLeft } from 'sdi/components/tooltip';
import {
    BUTTON,
    DIV,
    FIELDSET,
    INPUT,
    LABEL,
    SPAN,
} from 'sdi/components/elements';
import { nameToString } from 'sdi/components/button/names';

const renderSystem = (sys: 'open' | 'close') => {
    switch (sys) {
        case 'close':
            return tr.geo('closeSystem') + ' ';
        case 'open':
            return tr.geo('openSystem') + ' ';
    }
};
const selectSystem = (sys: 'open' | 'close') =>
    navigateSystem(sys, getLayout());

export const renderToggleSystem = renderRadioOut(
    'geo-system-toggle',
    renderSystem,
    selectSystem
);

const renderDepth = (depth: Depth) => {
    switch (depth) {
        case 'absolute':
            return tr.geo('absolute');
        case 'relative':
            return tr.geo('relative');
    }
};

const selectDepth = (depth: Depth) => setDepthMode(depth);
export const renderToggleDepthMode = renderRadioOut(
    'geo-depth-toggle',
    renderDepth,
    selectDepth
);

const buttonOpenFullscreen = () =>
    divTooltipBottom(
        tr.geo('fullscreenOn'),
        {},
        BUTTON(
            {
                className:
                    'geo-btn geo-btn--2 geo-btn--icon geo-btn--open-fullscreen',
                onClick: mapsSeparatorMoveUp,
            },
            DIV({ className: 'btn__icon' }, nameToString('expand'))
        )
    );

const buttonCloseFullscreen = () =>
    divTooltipBottom(
        tr.geo('fullscreenOff'),
        {},
        BUTTON(
            {
                className:
                    'geo-btn geo-btn--2 geo-btn--icon geo-btn--close-fullscreen',
                onClick: mapsSeparatorMoveDown,
            },
            DIV({ className: 'btn__icon' }, nameToString('compress'))
        )
    );

export const toggleFullScreenPanelButton = () =>
    getDisplayMapsSeparator() === 'middle'
        ? buttonOpenFullscreen()
        : buttonCloseFullscreen();

export const closeBottomRenderButton = (onClick: () => void) =>
    divTooltipBottomLeft(
        tr.core('close'),
        {},
        BUTTON(
            {
                onClick,
            },
            DIV(
                {
                    className:
                        'geo-btn geo-btn--2 geo-btn--icon geo-btn--close',
                },
                DIV({ className: 'btn__icon' }, nameToString('times'))
            )
        )
    );

export const stepWidget = (altName: string, inputAttributes: InputAttributes) =>
    DIV(
        'radio__item',
        LABEL(
            { alt: altName },
            // nameToString('circle'),
            INPUT(inputAttributes),
            SPAN('radio__control', SPAN('radio__control-inner'))
        )
    );

const depthScalingInputAttributes = (s: number) =>
    withChecked(getDepthScaling() === s, {
        onClick: () => setDepthScaling(s),
        type: 'radio',
        id: `depth-scaling-${s}`,
        name: 'depth-scaling',
        value: s,
    });

export const depthScalingWidget = () =>
    DIV(
        'depth-scaling__widget',

        DIV('depth-scaling__title', tr.geo('scalingZlabel')),
        DIV('depth-scaling__label icon fa ', nameToString('minus')),
        FIELDSET(
            'radio__fieldset fieldset--horizontal depth-scaling__body',
            stepWidget('', depthScalingInputAttributes(0.25)),
            stepWidget('', depthScalingInputAttributes(0.375)),
            stepWidget('', depthScalingInputAttributes(0.5)),
            stepWidget('', depthScalingInputAttributes(0.75)),
            stepWidget('', depthScalingInputAttributes(1))
        ),
        DIV('depth-scaling__label icon fa ', nameToString('plus'))
    );

// All the following toggle system should be linked to the main toggle system...

// const activeClass = (a: boolean) => (a ? 'unselected' : 'selected');
// const isBothSystem = () => getLayout() === 'technical';

// const toggleSystem = (get: () => boolean, set: (a: boolean) => void) => (
//     value1: GeoMessageKey,
//     value2: GeoMessageKey
// ) =>
//     DIV(
//         {
//             className: `geo-toggle ${activeClass(!isBothSystem())}`,
//             onClick: () => set(!get()),
//         },
//         DIV(
//             {
//                 className: `value first-value ${activeClass(
//                     isBothSystem() ? true : !get()
//                 )}`,
//             },
//             tr.geo(value1) + ' '
//         ),
//         DIV(
//             {
//                 className: `toggle  ${activeClass(
//                     isBothSystem() ? true : !get()
//                 )} `,
//             },
//             DIV({ className: 'toggle__button' })
//         ),
//         DIV(
//             {
//                 className: `value second-value ${activeClass(
//                     isBothSystem() ? true : get()
//                 )}`,
//             },
//             ' ' + tr.geo(value2)
//         )
//     );

// export const renderToggleSystem = toggleSystem(
//     () => getSystem().getOrElse('close') === 'close',
//     (v) =>
//         v
//             ? navigateSystem('close', getLayout())
//             : navigateSystem('open', getLayout())
// );

// const toggleDepth = (get: () => boolean, set: (a: boolean) => void) => (
//     value1: GeoMessageKey,
//     value2: GeoMessageKey
// ) =>
//     DIV(
//         {
//             className: `geo-toggle selected`,
//             onClick: () => set(!get()),
//         },
//         DIV(
//             {
//                 className: `value first-value ${activeClass(!get())}`,
//             },
//             tr.geo(value1) + ' '
//         ),
//         DIV(
//             {
//                 className: `toggle  ${activeClass(!get())} `,
//             },
//             DIV({ className: 'toggle__button' })
//         ),
//         DIV(
//             {
//                 className: `value second-value ${activeClass(get())}`,
//             },
//             ' ' + tr.geo(value2)
//         )
//     );

// export const renderToggleDepthMode = toggleDepth(
//     () => getDepthMode() === 'absolute',
//     (v) => (v ? setDepthMode('absolute') : setDepthMode('relative'))
// );
