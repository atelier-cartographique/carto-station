/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { Option, fromNullable } from 'fp-ts/lib/Option';

import { ILayerInfo, LayerGroup, Inspire, getMessageRecord } from 'sdi/source';
import { fromRecord } from 'sdi/locale';
import { DIV, SPAN } from 'sdi/components/elements';
// import { makeIcon } from 'sdi/components/button';

import legendPoint from './legend-point';
import legendLinestring from './legend-linestring';
import legendPolygon from './legend-polygon';
import { groupVisibility } from 'brugeotool/src/queries/map';
import // setGroupVisibility,
// setLayerVisibility,
// setGroupFolding
'brugeotool/src/events/map';
// import { makeIcon } from 'sdi/components/button';
import { withoutToolingLayers } from 'brugeotool/src/queries/geothermie';

const logger = debug('sdi:legend-item');

const withLabel =
    (layerInfo: ILayerInfo, md: Option<Inspire>) =>
    (nodes: React.ReactNode[]) => {
        const labelOpt = fromNullable(layerInfo.legend).map(fromRecord);

        return [
            DIV(
                { className: `legend-group__item` },
                DIV(
                    { className: 'layer-title' },
                    SPAN(
                        { className: 'layer-label' },
                        md.map(md =>
                            fromRecord(getMessageRecord(md.resourceTitle))
                        )
                    )
                    // vButton(() => setLayerVisibility(layerInfo.id, !isVisible))
                    // labelButton(() => '')
                ),
                labelOpt.map(label =>
                    DIV({ className: 'legend-label hidden' }, label)
                ),

                ...nodes
            ),
        ];
    };

const withoutLabel = (nodes: React.ReactNode[]) => {
    return [DIV({ className: `legend-group__item` }, ...nodes)];
};

export const renderLegendItem = (
    layerInfo: ILayerInfo,
    md: Option<Inspire>
) => {
    const label = withLabel(layerInfo, md);
    switch (layerInfo.style.kind) {
        case 'polygon-continuous':
        case 'polygon-discrete':
            return label(legendPolygon(layerInfo.style, layerInfo, md));
        case 'polygon-simple':
            return withoutLabel(legendPolygon(layerInfo.style, layerInfo, md));
        case 'point-discrete':
        case 'point-continuous':
            return label(legendPoint(layerInfo.style, layerInfo, md));
        case 'point-simple':
            return withoutLabel(legendPoint(layerInfo.style, layerInfo, md));
        case 'line-discrete':
        case 'line-continuous':
            return label(legendLinestring(layerInfo.style, layerInfo, md));
        case 'line-simple':
            return withoutLabel(
                legendLinestring(layerInfo.style, layerInfo, md)
            );
        default:
            throw new Error('UnknownStyleKind');
    }
};

export interface Group {
    g: LayerGroup | null;
    layers: ILayerInfo[];
}

export const groupItems = (layers: ILayerInfo[]) =>
    withoutToolingLayers(layers)
        .reverse()
        .reduce<Group[]>((acc, info) => {
            const ln = acc.length;
            if (ln === 0) {
                return [
                    {
                        g: info.group,
                        layers: [info],
                    },
                ];
            }
            const prevGroup = acc[ln - 1];
            const cg = info.group;
            const pg = prevGroup.g;
            // Cases:
            // info.group == null && prevGroup.g == null => append
            // info.group != null && prevGroup.g != null && info.group.id == prevGroup.id => append
            if (
                (cg === null && pg === null) ||
                (cg !== null && pg !== null && cg.id === pg.id)
            ) {
                prevGroup.layers.push(info);
                return acc;
            }
            // info.group == null && prevGroup.g != null => new
            // info.group != null && prevGroup.g == null => new
            // info.group != null && prevGroup.g != null && info.group.id != prevGroup.id => new

            return acc.concat({
                g: cg,
                layers: [info],
            });
        }, []);

export type MetadataGetter = (id: string) => Option<Inspire>;
export const renderGroups = (
    groups: Group[],
    getDatasetMetadata: MetadataGetter
) =>
    groups.map(group => {
        const isVisible = fromNullable(group.g?.id).fold(true, groupVisibility);

        const visibleLayers = group.layers.filter(l => l.visible === true);

        const items =
            visibleLayers.length > 0
                ? visibleLayers.map(layer =>
                      renderLegendItem(
                          layer,
                          getDatasetMetadata(layer.metadataId)
                      )
                  )
                : [];

        const groupGroup = group.g;
        if (groupGroup !== null && isVisible && visibleLayers.length > 0) {
            const vizClass = isVisible ? ' open ' : ' close ';

            return DIV(
                { className: `legend-group named ${vizClass}` },
                DIV(
                    {
                        className: 'legend-group-title',
                    },
                    SPAN(
                        { className: 'group-name' },
                        fromRecord(groupGroup.name)
                    )
                ),
                DIV({ className: 'legend-group-items' }, items)
            );
        }
        return DIV({ className: 'legend-group anonymous' }, items);
    });

export const legendRenderer =
    (getDatasetMetadata: MetadataGetter) => (layers: ILayerInfo[]) =>
        renderGroups(groupItems(layers), getDatasetMetadata);

export const hasZoomConstraint = (layerInfo: ILayerInfo): boolean => {
    return layerInfo.minZoom !== 0 || layerInfo.maxZoom !== 30;
};

export const getItemZoom = (zoomConstraint: boolean) => {
    return zoomConstraint ? SPAN({ className: 'fa fa-search-plus' }) : null;
};

logger('loaded');
