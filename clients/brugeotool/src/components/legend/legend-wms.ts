import { some } from 'fp-ts/lib/Option';

import { DIV, NODISPLAY, IMG } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';

import {
    getCurrentBaseLayer,
    findBaseLayer,
    topMapName,
} from 'brugeotool/src/queries/map';
import { getWMSLegendVisible } from 'brugeotool/src/queries/geothermie';
import { setWMSLegendVisible } from 'brugeotool/src/events/geothermie';

const renderImages = (url: string, version: string, layers: string) => {
    const images = layers.split(',').map(l =>
        IMG({
            src: `${url}?SERVICE=WMS&REQUEST=GetLegendGraphic&VERSION=${version}&FORMAT=image/png&WIDTH=20&HEIGHT=20&LAYER=${l}`,
        })
    );

    return DIV({}, ...images);
};

const renderWmsLegendImg = () =>
    findBaseLayer(getCurrentBaseLayer(topMapName).getOrElse('')).fold(
        NODISPLAY(),
        bl =>
            some(fromRecord(bl.params.LAYERS)).fold(NODISPLAY(), layers =>
                renderImages(bl.url, bl.params.VERSION, layers)
            )
    );

export const renderWmsLegend = () =>
    DIV(
        { className: 'legend-group named' },
        DIV(
            {
                className: `legend-group-title ${
                    getWMSLegendVisible() ? 'opened' : 'closed'
                }`,
                onClick: () => setWMSLegendVisible(!getWMSLegendVisible()),
            },
            getWMSLegendVisible()
                ? tr.geo('wmsLegendHide')
                : tr.geo('wmsLegendDisplay')
        ),
        DIV(
            { className: 'legend-group-items' },
            getWMSLegendVisible() ? renderWmsLegendImg() : NODISPLAY()
        )
    );
