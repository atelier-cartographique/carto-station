import { DIV, H2 } from 'sdi/components/elements';
import { makeLabel } from 'sdi/components/button';
import tr from 'sdi/locale';

import { getSystem } from '../../queries/geothermie';
import { setInputDepth } from '../../events/geothermie';

import { renderFormClose, renderLinkClose } from './closed';
import { renderFormOpen, renderLinkOpen } from './open';

const closeModalButton = makeLabel('cancel', 2, () => tr.core('cancel'));

const header = () => H2({}, tr.geo('modalSmartTitle'));

const footer = (close: () => void) =>
    DIV(
        { className: 'modal__footer__inner' },
        closeModalButton(() => {
            setInputDepth(null);
            close();
        }),
        renderLink()
    );

const renderHelpText = () =>
    DIV({ className: 'helptext' }, tr.geo('helptext:modalSmart'));

const renderForm = () =>
    getSystem().fold(DIV({}, 'ERROR: Missing System'), sys => {
        switch (sys) {
            case 'close':
                return renderFormClose();
            case 'open':
                return renderFormOpen();
        }
    });

const renderLink = () =>
    getSystem().fold(DIV({}, 'ERROR: Missing System'), sys => {
        switch (sys) {
            case 'close':
                return renderLinkClose();
            case 'open':
                return renderLinkOpen();
        }
    });

const body = () => DIV({}, renderHelpText(), renderForm());

export const render = { header, footer, body };
