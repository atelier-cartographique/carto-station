import * as debug from 'debug';
import { init } from 'sdi/components/modal';

const logger = debug('sdi:geo/modal');

export const { render, register } = init();
export default render;

export { render as renderPrintMap } from '../print-map';

export { render as renderSmartGeotherm } from './smart-geotherm';

logger('loaded');
