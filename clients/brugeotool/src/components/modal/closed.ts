import { DIV, A } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { attrOptions, inputNullableNumber } from 'sdi/components/input';
import { getLang } from 'sdi/app';

import { getInputDepth, getConductivity } from '../../queries/geothermie';
import { setInputDepth } from '../../events/geothermie';
import { getSmartGOThermBaseUrl } from '../settings';

const getLinkClose = () =>
    getInputDepth().fold(
        `${getSmartGOThermBaseUrl('closed')}?v=3&lang=${getLang()}`,
        d =>
            `${getSmartGOThermBaseUrl(
                'closed'
            )}?D=${d}&TC=${getConductivity().toFixed(2)}&v=3&lang=${getLang()}`
    );

export const renderLinkClose = () =>
    DIV(
        { className: 'geo-btn geo-btn--1' },
        A(
            { href: getLinkClose(), target: '_blank' },
            tr.geo('goToSmartGeotherm')
        )
    );

const renderHelpTextFormClose = () =>
    DIV({}, tr.geo('helptext:modalSmartClose'));

export const renderFormClose = () =>
    DIV(
        {},
        renderHelpTextFormClose(),
        DIV(
            {},
            inputNullableNumber(
                attrOptions(
                    `input-depth`,
                    () => getInputDepth().toNullable(),
                    i => setInputDepth(i),
                    { min: 1 }
                )
            ),
            ' m'
        ),
        DIV(
            {},
            tr.geo('conductivity'),
            ': ',
            getConductivity().toFixed(2),
            ' W/(m.K)'
        )
    );
