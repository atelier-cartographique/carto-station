import { getDepthScaling } from 'brugeotool/src/queries/geothermie';
import {
    GRAPH_LEFT_MARGIN,
    GRAPH_TOP_MARGIN,
    TICK_LENGTH,
    Y_TICK_LABEL,
} from './constants';

const drawXAxis = (ctx: CanvasRenderingContext2D, width: number) => {
    ctx.beginPath();
    ctx.moveTo(0, 0);
    ctx.lineTo(width, 0);
    /* draw arrow head */
    // ctx.lineTo(width - 2 * GRAPH_LEFT_MARGIN * xScaling - AXIS_ARROW_WIDTH, -AXIS_ARROW_WIDTH);
    // ctx.lineTo(width - 2 * GRAPH_LEFT_MARGIN * xScaling - AXIS_ARROW_WIDTH, AXIS_ARROW_WIDTH);
    // ctx.lineTo(width - 2 * GRAPH_LEFT_MARGIN * xScaling, 0);
    ctx.closePath();
    ctx.fillStyle = 'black';
    ctx.fill();
    ctx.strokeStyle = 'black';
    ctx.stroke();
};

const drawYAxis = (ctx: CanvasRenderingContext2D, height: number) => {
    ctx.beginPath();
    ctx.moveTo(0, 0);
    ctx.lineTo(0, height);
    ctx.closePath();
    ctx.strokeStyle = 'black';
    ctx.stroke();
};

const drawXTick = (ctx: CanvasRenderingContext2D, x: number, y: number) => {
    ctx.beginPath();
    ctx.moveTo(x, y);
    ctx.lineTo(x, y - TICK_LENGTH);
    ctx.closePath();
    ctx.strokeStyle = 'black';
    ctx.stroke();
};

const drawXLine = (
    ctx: CanvasRenderingContext2D,
    x: number,
    y: number,
    height: number
) => {
    ctx.beginPath();
    ctx.moveTo(x, y);
    ctx.lineTo(x, y + height);
    ctx.closePath();
    ctx.strokeStyle = 'rgba(75, 75, 75, 0.5)';
    ctx.stroke();
};

const drawXTickLabel = (
    ctx: CanvasRenderingContext2D,
    x: number,
    y: number,
    label: number
) => {
    ctx.fillStyle = 'black';
    ctx.font = '18px Arial';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.fillText(`${label} m`, x, y);
};

const drawYTick = (ctx: CanvasRenderingContext2D, x: number, y: number) => {
    ctx.beginPath();
    ctx.moveTo(x, y);
    ctx.lineTo(x + TICK_LENGTH, y);
    ctx.closePath();
    ctx.strokeStyle = 'black';
    ctx.stroke();
};

const drawYLine = (
    ctx: CanvasRenderingContext2D,
    x: number,
    y: number,
    width: number,
    lineWidth: number,
    color: string
) => {
    ctx.beginPath();
    ctx.moveTo(x, y);
    ctx.lineTo(x + width, y);
    ctx.closePath();
    ctx.strokeStyle = color;
    ctx.lineWidth = lineWidth;
    ctx.stroke();
};

const drawYTickLabel = (
    ctx: CanvasRenderingContext2D,
    x: number,
    y: number,
    label: number
) => {
    ctx.fillStyle = 'black';
    ctx.font = '18px Arial';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.fillText(`${label} m`, x + Y_TICK_LABEL, y);
};

const axisStep = (rawStep: number) =>
    rawStep > 3000
        ? 10000
        : rawStep > 300
        ? 1000
        : rawStep > 30
        ? 100
        : rawStep > 3
        ? 10
        : 1;

const drawYAxisTitle = (ctx: CanvasRenderingContext2D, heightPx: number) => {
    ctx.fillStyle = 'black';
    ctx.font = '22px Arial';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.rotate(-Math.PI / 2);
    ctx.fillText('z (m-DNG)', -0.4 * heightPx, -14);
};

export const drawAxis = (
    ctx: CanvasRenderingContext2D,
    widthPx: number,
    heightPx: number,
    xScaling: number,
    maxElevation: number
) => {
    /*
     * widthPx: length of the X axis in pixels
     * heightPx: length of the Z axis in pixels
     */
    ctx.translate(GRAPH_LEFT_MARGIN, GRAPH_TOP_MARGIN);
    drawXAxis(ctx, widthPx);
    drawYAxis(ctx, heightPx);

    const rawDepthStep = heightPx / getDepthScaling() / 10;
    /* depthStep in meter */
    const depthStep = axisStep(rawDepthStep);
    const nYTicks = Math.ceil(heightPx / getDepthScaling() / depthStep);
    [...Array(nYTicks).keys()].map(i => {
        drawYTick(ctx, 0, i * depthStep * getDepthScaling());
        if (i === 0) {
            drawYLine(
                ctx,
                0,
                i * depthStep * getDepthScaling(),
                widthPx,
                3,
                '#096077'
            );
        } else {
            drawYLine(
                ctx,
                0,
                i * depthStep * getDepthScaling(),
                widthPx,
                0.6,
                'rgba(75, 75, 75, 0.5)'
            );
        }
        if (i !== 0) {
            drawYTickLabel(
                ctx,
                0,
                i * depthStep * getDepthScaling(),
                maxElevation - i * depthStep / 5
            );
        }
    });

    const rawDistStep = widthPx / xScaling / 10;
    /* distStep in meter */
    const distStep = axisStep(rawDistStep);
    const nXTicks = Math.ceil(widthPx / xScaling);
    [...Array(nXTicks).keys()].map(i => {
        if (i !== 0) {
            drawXTick(ctx, i * distStep * xScaling, 0);
            drawXLine(ctx, i * distStep * xScaling, 0, heightPx);
            drawXTickLabel(ctx, i * distStep * xScaling, -20, i * distStep);
        }
    });

    ctx.save();
    drawYAxisTitle(ctx, heightPx);
    ctx.restore();
};
