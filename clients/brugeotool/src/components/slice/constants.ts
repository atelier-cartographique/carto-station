export const GRAPH_LEFT_MARGIN = 40; /* in px */
export const GRAPH_TOP_MARGIN = 56; /* in px */
export const MAX_ELEV_MARGIN = 20; /* in m */
export const PATTERN_SCALING = 2;
// const AXIS_ARROW_WIDTH = 12;
export const TICK_LENGTH = 12;
export const Y_TICK_LABEL = 50;
export const PIEZO_NO_DATA = 1000;
