import * as debug from 'debug';

import { GeoMessageKey } from '../../locale';
import {
    InfoStrati,
    InfoHydroGeol,
    InfoClosed,
    InfoOpen,
} from 'brugeotool/src/remote/info';
import { Pair, fst, pair, snd } from 'sdi/lib';
import { fromNullable } from 'fp-ts/lib/Option';
import { formatK } from 'brugeotool/src/queries/geothermie';

const logger = debug('sdi:geo/settings');

//staging
//const SMARTGOTHERM_BASE_URL = 'http://staging.anglegaming.com/wtcb/';
// production
const SMARTGOTHERM_BASE_URL = 'https://tool.smartgeotherm.be/';

export const getSmartGOThermBaseUrl = (c: 'open' | 'closed' | 'extern') => {
    switch (c) {
        case 'open':
            return SMARTGOTHERM_BASE_URL + 'screenkwo/';
        case 'closed':
            return SMARTGOTHERM_BASE_URL + 'vbww/';
        default:
            // extern (externe à bxl)
            return SMARTGOTHERM_BASE_URL + 'geo/alg';
    }
};

export const addSuffixToMessageKey = (m_arr: GeoMessageKey[], suffix: string) =>
    m_arr.map(m => `${m}${suffix}` as GeoMessageKey);

// Constants for defining the stratigraphic, hydrogeologic and piezo layer keys
export const STRATIGRAPHY_UNITS_NAME: GeoMessageKey[] = [
    'US/RBC_1',
    'US/RBC_21',
    'US/RBC_22',
    'US/RBC_23',
    'US/RBC_25',
    'US/RBC_31',
    'US/RBC_41',
    'US/RBC_42',
    'US/RBC_43',
    'US/RBC_44',
    'US/RBC_51',
    'US/RBC_61',
    'US/RBC_71',
    'US/RBC_72',
    'US/RBC_73',
    'US/RBC_81',
    'US/RBC_82',
    'US/RBC_91',
    'US/RBC_92',
];

export const Q_STRATIGRAPHY_UNITS_NAME: GeoMessageKey[] = [
    'US/RBC_11',
    'US/RBC_12',
    'US/RBC_13',
    'US/RBC_14',
];

export const HYDROGEOLOGY_UNITS_NAME: GeoMessageKey[] = [
    'UH/RBC_1a',
    'UH/RBC_1b',
    'UH/RBC_2',
    'UH/RBC_3',
    'UH/RBC_4',
    'UH/RBC_5',
    'UH/RBC_6',
    'UH/RBC_7a',
    'UH/RBC_7b',
    'UH/RBC_7c',
    'UH/RBC_8a',
    'UH/RBC_8b',
    'UH/RBC_9a',
    'UH/RBC_9b',
];

export const UH_FOR_US_ID: GeoMessageKey[] = [
    'UH/RBC_1a',
    'UH/RBC_2',
    'UH/RBC_2',
    'UH/RBC_2',
    'UH/RBC_2',
    'UH/RBC_3',
    'UH/RBC_4',
    'UH/RBC_4',
    'UH/RBC_4',
    'UH/RBC_4',
    'UH/RBC_5',
    'UH/RBC_6',
    'UH/RBC_7a',
    'UH/RBC_7b',
    'UH/RBC_7c',
    'UH/RBC_8a',
    'UH/RBC_8b',
    'UH/RBC_9a',
    'UH/RBC_9b',
];

export const PIEZO_LAYERS_NAME: GeoMessageKey[] = [
    'UH/RBC_1b',
    'UH/RBC_2',
    'UH/RBC_4',
    'UH/RBC_6',
    'UH/RBC_7b',
    'UH/RBC_8a',
];

export const PIEZO_LAYERS_NAME_GEOLOGY_OPEN: GeoMessageKey[] = [
    'UH/RBC_4',
    'UH/RBC_8a',
];

export const GEOLOGICAL_ETAGE_LAYERS_NAME: GeoMessageKey[] = [
    'geoEtage1',
    'geoEtage2',
    'geoEtage3',
    'geoEtage4',
    'geoEtage5',
    'geoEtage6',
    'geoEtage7',
    'geoEtage8',
    'geoEtage9',
    'geoEtage10',
    'geoEtage11',
    'geoEtage12',
    'geoEtage13',
    'geoEtage14',
    'geoEtage15',
];

export const GEOLOGICAL_SERIE_LAYERS_NAME: GeoMessageKey[] = [
    'geoSerie1',
    'geoSerie2',
    'geoSerie3',
    'geoSerie4',
    'geoSerie5',
    'geoSerie6',
    'geoSerie7',
    'geoSerie8',
    'geoSerie9',
    'geoSerie10',
    'geoSerie11',
    'geoSerie12',
];

export const GEOLOGICAL_ERA_LAYERS_NAME: GeoMessageKey[] = [
    'IV',
    'III',
    'II',
    'I',
];

export const GEOLOGICAL_SYSTEM_LAYERS_NAME: GeoMessageKey[] = [
    'Quaternary',
    'Neogene',
    'Paleogene',
    'Cretacea',
    'Cambria',
];

export const HYDROLOGICAL_SYSTEM_NAME: GeoMessageKey[] = [
    'phreatic',
    'confined',
];

export const HYDROLOGICAL_SYSTEM_NAME_FOR_US_ID: GeoMessageKey[] = [
    'phreatic',
    'phreatic',
    'phreatic',
    'phreatic',
    'phreatic',
    'phreatic',
    'phreatic',
    'phreatic',
    'phreatic',
    'phreatic',
    'phreatic',
    'phreatic',
    'phreatic',
    'phreatic',
    'phreatic',
    'confined',
    'confined',
    'confined',
    'confined',
];

export const GEOLOGICAL_ETAGE_NAME_FOR_US_ID: GeoMessageKey[] = [
    'geoEtage1',
    'geoEtage5',
    'geoEtage6',
    'geoEtage7',
    'geoEtage8',
    'geoEtage8',
    'geoEtage8',
    'geoEtage9',
    'geoEtage10',
    'geoEtage11',
    'geoEtage11',
    'geoEtage12',
    'geoEtage12',
    'geoEtage12',
    'geoEtage12',
    'geoEtage13',
    'geoEtage13',
    'geoEtage14',
    'geoEtage15',
];

export const GEOLOGICAL_SERIE_NAME_FOR_US_ID: GeoMessageKey[] = [
    'geoSerie1',
    'geoSerie5',
    'geoSerie6',
    'geoSerie7',
    'geoSerie8',
    'geoSerie8',
    'geoSerie8',
    'geoSerie8',
    'geoSerie8',
    'geoSerie9',
    'geoSerie9',
    'geoSerie9',
    'geoSerie9',
    'geoSerie9',
    'geoSerie9',
    'geoSerie10',
    'geoSerie10',
    'geoSerie11',
    'geoSerie12',
];
export const GEOLOGICAL_SYSTEM_NAME_FOR_US_ID: GeoMessageKey[] = [
    'Quaternary',
    'Neogene',
    'Neogene',
    'Paleogene',
    'Paleogene',
    'Paleogene',
    'Paleogene',
    'Paleogene',
    'Paleogene',
    'Paleogene',
    'Paleogene',
    'Paleogene',
    'Paleogene',
    'Paleogene',
    'Paleogene',
    'Paleogene',
    'Paleogene',
    'Cretacea',
    'Cambria',
];
export const GEOLOGICAL_ERA_NAME_FOR_US_ID: GeoMessageKey[] = [
    'IV',
    'III',
    'III',
    'III',
    'III',
    'III',
    'III',
    'III',
    'III',
    'III',
    'III',
    'III',
    'III',
    'III',
    'III',
    'III',
    'III',
    'II',
    'I',
];

// Constants for each layer key
export const STRATIGRAPHY_COLORS: string[] = [
    '#cccccc',
    '#ff7350',
    '#dfffa0',
    '#debdff',
    '#aa9c5e',
    '#917c53',
    '#bc9953',
    '#ffd452',
    '#ffff51',
    '#ffbbbb',
    '#ff9595',
    '#ffbbff',
    '#92a3b3',
    '#c6d0e8',
    '#abb8dc',
    '#ff9650',
    '#de864d',
    '#9fcd70',
    '#CCC', // socle color
];

export const UH_COLORS: string[] = [
    '', //UH/RBC-1
    '#debdff', //UH/RBC-2
    '#ffff51', //UH/RBC-4
    '#ffbbff', //UH/RBC-6
    '#abb8dc', //UH/RBC-7b
    '#de864d', //UH/RBC-8
];


export const UH_COLORS_DARKER: string[] = [
    '', //UH/RBC-1
    '#bf80ff', //UH/RBC-2
    '#ffff00', //UH/RBC-4
    '#ff80ff', //UH/RBC-6
    '#6f85c3', //UH/RBC-7b
    '#d76e28', //UH/RBC-8
];

export const QUATERNARY_COLORS: string[] = [
    '#cccccc66',
    '#cccccc66',
    '#cccccc66',
    '#cccccc66',
];

export const SATURATED_COLOR = '#3b7aa655';

export const K_MIN: string[] = [
    '-',
    '5.0e-07',
    '-',
    '-',
    '5.0e-06',
    '-',
    '2.0e-07',
    '-',
    '2.07E-07',
    '-',
    '5.0e-06',
    '-',
    '1.5e-06',
    '-',
];
export const K_MIN_IF_USRBC43: string[] = [
    '-',
    '5.0e-07',
    '-',
    '-',
    '1.0e-05',
    '-',
    '2.0e-07',
    '-',
    '2.07E-07',
    '-',
    '5.0e-06',
    '-',
    '1.5e-06',
    '-',
];

export const K_MEAN: string[] = [
    '-',
    '5.0e-04',
    '-',
    '-',
    '2.0e-05',
    '-',
    '2.0e-06',
    '-',
    '1.0e-06',
    '-',
    '3.0e-05',
    '-',
    '5.5e-05',
    '?',
];
export const K_MEAN_IF_USRBC43: string[] = [
    '-',
    '5.0e-04',
    '-',
    '-',
    '2.0e-04',
    '-',
    '2.0e-06',
    '-',
    '1.0e-06',
    '-',
    '3.0e-05',
    '-',
    '5.5e-05',
    '?',
];

export const K_MAX: string[] = [
    '-',
    '1.0e-02',
    '-',
    '-',
    '1.0e-04',
    '-',
    '5.0e-05',
    '-',
    '5.0e-05',
    '-',
    '1.0e-03',
    '-',
    '1.5e-03',
    '?',
];
export const K_MAX_IF_USRBC43: string[] = [
    '-',
    '1.0e-02',
    '-',
    '-',
    '1.0e-03',
    '-',
    '5.0e-05',
    '-',
    '5.0e-05',
    '-',
    '1.0e-03',
    '-',
    '1.5e-03',
    '?',
];

export const DRY_CONDUCTIVITY: number[] = [
    1.9, 1.8, 1.8, 1.6, 1.8, 1.2, 1.6, 1.6, 1.6, 1.8, 1.2, 1.6, 0, 0, 0, 0, 0,
    0, 0,
];
export const SAT_CONDUCTIVITY: number[] = [
    2, 2.6, 2.6, 2.2, 2.6, 1.4, 2.6, 2.6, 2.6, 2.6, 1.4, 2.2, 1.4, 2.1, 1.4, 2,
    1.9, 2.2, 3,
];

export const CANONICAL_PATTERN_UH_BASEROCK = 705;

// export const CANONICAL_PATTERN_UH: number[] = [0, 602, 607, 616, 607, 620, 616, 620, 616, 620, 607, 620, 626, 706];
export const CANONICAL_PATTERN_US: number[] = [
    602,
    607,
    607,
    616,
    607,
    620,
    607,
    607,
    607,
    607,
    620,
    616,
    620,
    616,
    620,
    607,
    620,
    626,
    CANONICAL_PATTERN_UH_BASEROCK,
];

export const POTENTIAL_CLASS: string[] = [
    'bad',
    'medium',
    'bad',
    'bad',
    'good',
    'bad',
    'medium',
    'bad',
    'medium',
    'bad',
    'good',
    'bad',
    'good',
    'good',
];

export const US_GREY_SHADE: string[] = [
    'medium',
    'light',
    'light',
    'light',
    'light',
    'dark',
    'light',
    'light',
    'light',
    'light',
    'dark',
    'medium',
    'dark',
    'medium',
    'dark',
    'light',
    'dark',
    'light',
    'light',
];
export const UH_GREY_SHADE: string[] = [
    'medium',
    'light',
    'light',
    'dark',
    'light',
    'dark',
    'medium',
    'dark',
    'medium',
    'dark',
    'light',
    'dark',
    'light',
    '',
];
export const UH_SYSTEM: string[] = [
    'freat',
    'freat',
    'freat',
    'freat',
    'freat',
    'freat',
    'freat',
    'freat',
    'freat',
    'freat',
    'conf',
    'conf',
    'conf',
    'conf',
];

// Not used?
export const STRATIGRAPHY_UNITS = STRATIGRAPHY_UNITS_NAME.map((id, it) => ({
    id,
    color: STRATIGRAPHY_COLORS[it],
}));

export const FORMATION_NAME: string[] = [
    '-',
    'Sables',
    'Sables',
    'Sables et argiles',
    'Sables',
    'Argiles',
    'Sables',
    'Sables',
    'Sables',
    'Sables',
    'Argiles',
    'Sables et argiles',
    'Argiles',
    'Sables et argiles',
    'Argiles',
    'Sables',
    'Argiles',
    'Craies',
    'Argilites / quartzites',
]; // TODO go to message key if used

const codeUsSorted = [
    '11', // "US/RBC_11 Remblais"
    '12', // "US/RBC_12 Couverture limoneuse"
    '13', // "US/RBC_13 Argiles alluviales"
    '14', // "US/RBC_14 Limons, sables et graviers alluviaux"
    '21', // "US/RBC_21 Sables de Diest"
    '22', // "US/RBC_22 Sables de Bolderberg"
    '23', // "US/RBC_23 Sables et argiles de Sint-Huibrechts-Hern"
    '25', // "US/RBC_25 Sables de Maldegem (membre de Onderdale)"
    '31', // "US/RBC_31 Argiles de Maldegem (membre de Ursel et Asse)"
    '41', // "US/RBC_41 Sables de Maldegem (membre de Wemmel)"
    '42', // "US/RBC_42 Sables de Lede"
    '43', // "US/RBC_43 Sables de Bruxelles"
    '44', // "US/RBC_44 Sables de Gent (membre de Vlierzele)"
    '51', // "US/RBC_51 Argiles de Gent (membre de Merelbeke)"
    '61', // "US/RBC_61 Sables et argiles de Tielt"
    '71', // "US/RBC_71 Argiles de Kortrijk (membre d'Aalbeke)"
    '72', // "US/RBC_72 Sables et argiles de Kortrijk (membre de Moen)"
    '73', // "US/RBC_73 Argiles de Kortrijk (membre de Saint Maur)"
    '81', // "US/RBC_81 Sables de Hannut (Membre de Grandglise)"
    '82', // "US/RBC_82 Argiles de Hannut (Membre de Lincent)"
    '91', // "US/RBC_91 Craies de Gulpen"
    '92', // "US/RBC_92 Socle Paleozoique"
];

const codeUHSorted = [
    '1a', // "UH/RBC_01a Système aquitard quaternaire superficiel"
    '1b', // "UH/RBC_01b Aquifère des limons, sables et graviers alluviaux"
    '2', // "UH/RBC_02 Systeme aquifere sableux perche"
    '3', // "UH/RBC_03 Aquiclude des argiles de Ursel et Asse"
    '4', // "UH/RBC_04 Systeme aquifere des sables de Wemmel, Lede, Bruxelles et Vlierzele"
    '5', // "UH/RBC_05 Aquiclude des argiles de Merelbeke"
    '6', // "UH/RBC_06 Aquitard des sables et argiles de Tielt"
    '7a', // "UH/RBC_07a Aquiclude des argiles de Aalbeke"
    '7b', // "UH/RBC_07b Aquitard des sables et argiles de Moen"
    '7c', // "UH/RBC_07c Aquiclude des argiles de Saint-Maur"
    '8a', // "UH/RBC_08a Aquifère des sables du Landénien"
    '8b', // "UH/RBC_08b Aquiclude des argiles du Landenien"
    '9a', // "UH/RBC_09a Aquifère des craies du Crétacé"
    '9b', // "UH/RBC_09b Système aquifère du socle Paléozoïque"
];

export const getUHCode = (index: number) => codeUHSorted[index];

const sortByRef =
    (ref: string[]) =>
    <T>(a: Pair<string, T>, b: Pair<string, T>) => {
        const ia = ref.indexOf(fst(a));
        const ib = ref.indexOf(fst(b));
        if (ia < ib) {
            return -1;
        }
        if (ia > ib) {
            return 1;
        }
        return 0;
    };

const updateList = <S, T>(source: S[], target: T[], map: (s: S) => T) => {
    if (source.length !== target.length) {
        throw new Error('source and target have different length');
    }
    for (let i = 0; i < source.length; i += 1) {
        target[i] = map(source[i]);
    }
};

export const updateValues = (
    strati: InfoStrati[],
    closed: InfoClosed[],
    hgeol: InfoHydroGeol[],
    opened: InfoOpen[]
) => {
    const sortedStrati = strati
        .map(s => pair(s.code_us, s))
        .sort(sortByRef(codeUsSorted))
        .map(snd);
    const sortedClosed = closed
        .map(s => pair(s.code_us, s))
        .sort(sortByRef(codeUsSorted))
        .map(snd);

    const sortedHgeol = hgeol
        .map(s => pair(s.code_uh, s))
        .sort(sortByRef(codeUHSorted))
        .map(snd);
    const sortedOpened = opened
        .map(s => pair(s.code_uh, s))
        .sort(sortByRef(codeUHSorted))
        .map(snd);

    const sortedHgeolEast = sortedHgeol.filter(s => s.code_geogr !== 'W');
    const sortedHgeolWest = sortedHgeol.filter(s => s.code_geogr !== 'E');

    updateList(sortedHgeolWest, K_MIN, s =>
        fromNullable(s.kmin).map(formatK).getOrElse('-')
    );
    updateList(sortedHgeolEast, K_MIN_IF_USRBC43, s =>
        fromNullable(s.kmin).map(formatK).getOrElse('-')
    );

    updateList(sortedHgeolWest, K_MAX, s =>
        fromNullable(s.kmax).map(formatK).getOrElse('-')
    );
    updateList(sortedHgeolEast, K_MAX_IF_USRBC43, s =>
        fromNullable(s.kmax).map(formatK).getOrElse('-')
    );

    updateList(sortedHgeolWest, K_MEAN, s =>
        fromNullable(s.kavg).map(formatK).getOrElse('-')
    );
    updateList(sortedHgeolEast, K_MEAN_IF_USRBC43, s =>
        fromNullable(s.kavg).map(formatK).getOrElse('-')
    );

    // const sortedStratiCompressed = [sortedStrati[0], ...sortedStrati.slice(4)];

    updateList(sortedClosed, STRATIGRAPHY_COLORS, s => `rgb(${s.color})`);
    //  la couleur de l’US quaternaire (« US11-14 etc. ») et gérée via la ligne de l’US 11. Serait-il possible que cette couleur soit gérée via la ligne de l’US11-14.
    // - the architect
    fromNullable(closed.find(c => c.code_us === '11-14')).map(
        c => STRATIGRAPHY_COLORS[0] === `rgb(${c.color})`
    );

    // Synthèse: les couleurs des US quaternaires sont toutes gérées pas la couleur
    // définie pour l’US 11. Serait - il possible qu’elles soient définies chacun par l’unité à laquelle elles correspondent ?
    // - the architect
    updateList(
        sortedStrati.slice(0, 4),
        QUATERNARY_COLORS,
        s => `rgb(${s.color})`
    );

    updateList(sortedClosed, DRY_CONDUCTIVITY, c =>
        fromNullable(c.lambda_dry).getOrElse(0)
    );
    updateList(sortedClosed, SAT_CONDUCTIVITY, c =>
        fromNullable(c.lambda_sat).getOrElse(0)
    );

    // just to shut up TS complaining about unused varibles
    logger(sortedOpened);
};

logger('loaded');
