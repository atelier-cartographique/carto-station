import { engine, VERTICAL_RATIO } from './engine';
import { DIV, CANVAS, FIELDSET } from 'sdi/components/elements';
import { getTile, getTopZoom } from 'brugeotool/src/queries/map';
import { Nullable, isNotNullNorUndefined } from 'sdi/util';
import { none, some, Option } from 'fp-ts/lib/Option';
import {
    setCakeRes,
    setCakeQuatAlpha,
    clearBottomRender,
    endCake,
} from 'brugeotool/src/events/geothermie';
import {
    getCakeRes,
    getCakeQuatAlpha,
} from 'brugeotool/src/queries/geothermie';
import { rect } from 'sdi/app';

import './legend';
import tr from 'sdi/locale';
import { renderRadioOut, withChecked } from 'sdi/components/input';
import {
    closeBottomRenderButton,
    depthScalingWidget,
    stepWidget,
    toggleFullScreenPanelButton,
} from '../button';

export type CakeMode = 'left-cut' | 'right-cut' | 'uncut' | 'double-cut';

export type CakeRes = 16 | 32 | 64 | 128 | 256;

export type CakeAlpha = '00' | '20' | '70' | 'FC';

export const dec2hex = (d: number): string =>
    Math.round(d).toString(16).padStart(2, '0');

export const hex2dec = (h: string): number => parseInt(h, 16);

const { onCanvas } = engine();

let updater: Option<() => void> = none;

const rectify = rect(() => updater.map(u => u()));

const attachEngine = (node: Nullable<HTMLCanvasElement>) => {
    if (isNotNullNorUndefined(node)) {
        const { update, load } = onCanvas(node);
        updater = some(() => {
            getTile().foldL(load, update);
        });
        rectify(node);
    } else {
        updater = none;
    }
};

const VERTICAL_SCALE_HEIGHT = 100;

const verticalScale = () =>
    DIV(
        {
            className: 'map-scale--vertical',
        },
        DIV(
            {
                className: 'map-scale-line',
                style: {
                    height: `${Math.round(
                        VERTICAL_SCALE_HEIGHT * VERTICAL_RATIO
                    )}px`,
                },
            },
            DIV({ className: 'quarter' }),
            DIV({ className: 'quarter' }),
            DIV({ className: 'half' })
        ),
        DIV({ className: 'map-scale-label' }, `${VERTICAL_SCALE_HEIGHT} m`)
    );

// const buttonClass = (m: CakeMode) =>
//     `geo-btn geo-btn--2 geo-btn--icon geo-btn--${m} ${
//         getCakeMode() === m ? 'active' : ''
//     }`;

// const buttonLeftSlice = () =>
//     divTooltipTop(
//         tr.geo('leftSlice'),
//         {},
//         BUTTON(
//             {
//                 onClick: () => setCakeMode('left-cut'),
//             },
//             DIV({ className: buttonClass('left-cut') })
//         )
//     );

// const buttonDoubleSlice = () =>
//     divTooltipTop(
//         tr.geo('doubleSlice'),
//         {},
//         BUTTON(
//             {
//                 onClick: () => setCakeMode('double-cut'),
//             },
//             DIV({ className: buttonClass('double-cut') })
//         )
//     );

// const buttonRightSlice = () =>
//     divTooltipTopLeft(
//         tr.geo('rightSlice'),
//         {},
//         BUTTON(
//             {
//                 onClick: () => setCakeMode('right-cut'),
//             },
//             DIV({ className: buttonClass('right-cut') })
//         )
//     );

// const buttonNoSlice = () =>
//     divTooltipTop(
//         tr.geo('noSlice'),
//         {},
//         BUTTON(
//             {
//                 onClick: () => setCakeMode('uncut'),
//             },
//             DIV({ className: buttonClass('uncut') })
//         )
//     );

// const alphaInputAttributes = (a: CakeAlpha) =>
//     withChecked(getCakeQuatAlpha() === a, {
//         onClick: () => setCakeQuatAlpha(a),
//         type: 'radio',
//         id: `alpha-${a}`,
//         name: 'alpha',
//         value: a,
//     })

// const alphaWidget = () =>
//     DIV(
//         { className: 'alpha__widget' },
//         DIV({ className: 'alpha__label' }, tr.geo('displayTransparency')),
//         FIELDSET(
//             {
//                 className:
//                     'radio__fieldset fieldset--horizontal alpha__body',
//             },
//             stepWidget(tr.geo('lowAlpha'), alphaInputAttributes('00')),
//             stepWidget('', alphaInputAttributes('20')),
//             stepWidget('', alphaInputAttributes('70')),
//             stepWidget(tr.geo('highAlpha'), alphaInputAttributes('FC')),
//         ),
//         DIV({ className: 'alpha__label' }, tr.geo('displayOpacity'))
//     );

const renderQuaternaireSwitch = (a: CakeAlpha) => {
    switch (a) {
        default:
            return '';
        case 'FC':
            return tr.geo('Quaternary');
    }
};

const selectQuaternaireSwitch = (a: CakeAlpha) => setCakeQuatAlpha(a);

const quaternaireSwitch = renderRadioOut(
    'show-quaternaire-toggle',
    renderQuaternaireSwitch,
    selectQuaternaireSwitch
);

const resolutionInputAttributes = (res: CakeRes) =>
    withChecked(getCakeRes() === res, {
        onClick: () => setCakeRes(res),
        type: 'radio',
        id: `resolution-${res}`,
        name: 'resolution',
        value: res,
    });

const resolutionWidget = () =>
    DIV(
        'resolution__widget',
        DIV('resolution__label', tr.geo('displayPerformance')),
        FIELDSET(
            'radio__fieldset fieldset--horizontal resolution__body',
            stepWidget(
                tr.geo('highPerformance'),
                resolutionInputAttributes(32)
            ),
            stepWidget(
                tr.geo('goodPerformance'),
                resolutionInputAttributes(64)
            ),
            stepWidget(tr.geo('goodQuality'), resolutionInputAttributes(128)),
            stepWidget(tr.geo('highQuality'), resolutionInputAttributes(256))
        ),
        DIV('resolution__label', tr.geo('displayQuality'))
    );

// const toolbar3D = () =>
//     DIV(
//         'toolbar--3D',
//         buttonNoSlice(),
//         buttonLeftSlice(),
//         buttonDoubleSlice(),
//         buttonRightSlice()
//     );

const display3DButtons = () =>
    DIV(
        'display--3D',
        toggleFullScreenPanelButton(),
        closeBottomRenderButton(() => {
            clearBottomRender();
            endCake();
        })
    );

const display3DNotVisible = () =>
    DIV(
        'widget__wrapper',
        closeBottomRenderButton(() => {
            clearBottomRender();
            endCake();
        })
    );

export type CakeInZoomRange = 'before' | 'in' | 'after';
export const cakeInZoomRange = (): CakeInZoomRange => {
    const z = getTopZoom();
    if (z < 3) {
        return 'before';
    } else if (z > 11) {
        return 'after';
    }
    return 'in';
};

const alphaWidget = () =>
    DIV('alpha__widget', quaternaireSwitch(['00', 'FC'], getCakeQuatAlpha()));

const reallyRenderCake = () => {
    // avoid blocking rendering of the main interface
    updater.map(u => window.setTimeout(u, 16));
    return DIV(
        {
            className: 'map-wrapper geo-tile ',
            style: {
                position: 'absolute',
                top: 0,
                right: 0,
                bottom: 0,
                left: 0,
            },
        },

        DIV(
            'widget__wrapper',
            alphaWidget(),
            resolutionWidget(),
            display3DButtons()
        ),

        verticalScale(),
        //alphaWidget(),
        // toolbar3D(),
        depthScalingWidget(),
        CANVAS({
            ref: attachEngine,
            style: {
                width: '100%',
                height: '100%',
            },
        })
    );
};

const renderBefore = () =>
    DIV('cake-message', display3DNotVisible(), tr.geo('cakeMessageBefore'));

const renderAfter = () =>
    DIV('cake-message', display3DNotVisible(), tr.geo('cakeMessageAfter'));

export const renderCake = () => {
    const r = cakeInZoomRange();
    switch (r) {
        case 'in':
            return reallyRenderCake();
        case 'before':
            return renderBefore();
        case 'after':
            return renderAfter();
    }
};
