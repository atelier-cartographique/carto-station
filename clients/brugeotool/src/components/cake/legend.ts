import { GeoMessageKey } from 'brugeotool/src/locale';
import { getUSName } from 'brugeotool/src/queries/geothermie';
import { DIV, IMG } from 'sdi/components/elements';
import { STRATIGRAPHY_UNITS } from '../settings';
import { clear, fillStyle, prect, fill, image } from '../sidebar/legend';

const MARGIN = 0.15;

const renderBlock = (width: number, height: number, color: string) => {
    const [
        w,
        // h
    ] = [width * (1 - 2 * MARGIN), height * (1 - 2 * MARGIN)];
    const t = w * 0.4;
    const dw = w / 2;

    clear(0, 0, width, height);
    fillStyle(color);

    // top
    prect(
        [MARGIN, MARGIN + t],
        [MARGIN + dw, MARGIN],
        [MARGIN + w, MARGIN + t],
        [MARGIN + dw, MARGIN + 2 * t]
    );
    fill();

    // left
    prect(
        [MARGIN, height - (MARGIN + t)],
        [MARGIN, MARGIN + t],
        [MARGIN + dw, MARGIN + 2 * t],
        [MARGIN + dw, height - MARGIN]
    );
    fill();
    prect(
        [MARGIN, height - (MARGIN + t)],
        [MARGIN, MARGIN + t],
        [MARGIN + dw, MARGIN + 2 * t],
        [MARGIN + dw, height - MARGIN]
    );
    fill('#00000015');

    // right
    prect(
        [MARGIN + dw, MARGIN + 2 * t],
        [MARGIN + w, MARGIN + t],
        [MARGIN + w, height - (MARGIN + t)],
        [MARGIN + dw, height - MARGIN]
    );
    fill();
    prect(
        [MARGIN + dw, MARGIN + 2 * t],
        [MARGIN + w, MARGIN + t],
        [MARGIN + w, height - (MARGIN + t)],
        [MARGIN + dw, height - MARGIN]
    );
    fill('#00000033');

    return IMG({ src: image(0, 0, width, height), alt: '' });
};

const renderItem = (nameKey: GeoMessageKey, color: string) =>
    DIV(
        {
            className: 'legend-item legend-item-3d',
            key: `3d-legend-${nameKey}`,
        },
        DIV(
            {
                className: 'item-style',
            },
            renderBlock(132, 100, color)
        ),
        getUSName(nameKey)
    );

export const renderLegend = () =>
    DIV(
        { className: 'legend-3d' },
        STRATIGRAPHY_UNITS.map(({ id, color }) => renderItem(id, color))
    );

export default renderLegend;
