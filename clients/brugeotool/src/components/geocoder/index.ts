// import * as debug from 'debug';
import * as io from 'io-ts';
// import { Option } from 'fp-ts/lib/Option';

// import { BUTTON, DIV } from 'sdi/components/elements';
// import { inputText, CLEAR_INPUT_TEXT } from 'sdi/components/input';
import { IUgWsAddress } from 'sdi/ports/geocoder';
import tr, { Translated } from 'sdi/locale';
import { ServiceResult } from 'sdi/geocoder/io';
import { Interaction, viewEventsFactory } from 'sdi/map';
import { Setter } from 'sdi/shape';
import { IMapViewData } from 'sdi/map';
import { NodeOrOptional } from 'sdi/components/elements';
import { Tooltip } from 'sdi/components/tooltip';
import { makeIcon } from 'sdi/components/button';
import { setPinnable } from 'brugeotool/src/events/geothermie';
import { Coordinate } from 'ol/coordinate';
import { clearServiceResponse } from 'sdi/geocoder/events';
import { addPositionMarker, setInteractionClick } from 'brugeotool/src/events/map';


// import { IUgWsAddress, IUgWsResult, queryGeocoder } from 'sdi/ports/geocoder';
// import { isENTER } from 'sdi/components/keycodes';
// import tr from 'sdi/locale';
// import { getLang, setFocusId } from 'sdi/app';

// import { getGeocoderInput, getGeocoderResponse } from '../../queries/map';
// import {
//     setGeocoderInput,
//     setGeocoderResponse,
//     addPositionMarker,
//     centerMapOn,
//     removePositionMarker,
// } from '../../events/map';
// import {
//     setAddress,
//     setSearchType,
//     searchCapakey,
//     setSearchCapakey,
//     setSearchCoordinates,
//     searchCoordinates
// } from '../../events/geothermie';
// import { snap } from '../../events/route';
// import { nameToString } from 'sdi/components/button/names';
// import { divTooltipBottom } from 'sdi/components/tooltip';
// import { SearchType } from '../geothermie';
// import {
//     getSearchType,
//     getSearchCapakey,
//     getSearchCoordinates
// } from 'brugeotool/src/queries/geothermie';

// const logger = debug('sdi:geothermie');

// tslint:disable-next-line: variable-name
export const XyIO = io.interface(
    {
        x: io.Integer,
        y: io.Integer,
    },
    'XyIO'
);

export type XY = io.TypeOf<typeof XyIO>;

export type MultilingualAddress = {
    fr?: IUgWsAddress;
    nl?: IUgWsAddress;
};

// const searchAddress = () =>
//     getGeocoderInput().foldL(
//         () => setGeocoderResponse(null),
//         input => queryGeocoder(input, getLang()).then(setGeocoderResponse)
//     );

// const addressToString = (a: IUgWsAddress) =>
//     `${a.street.name} ${a.number}, ${a.street.postCode} ${a.street.municipality}`;

// const search = (st: SearchType) => () => {
//     switch (st) {
//         case 'geocoder':
//             return searchAddress();
//         case 'capakey':
//             return searchCapakey();
//         case 'coordinates':
//             return searchCoordinates();
//     }
// };

// const searchOnEnter = (st: SearchType) => (
//     e: React.KeyboardEvent<HTMLInputElement>
// ) => {
//     if (isENTER(e)) {
//         search(st)();
//     }
// };

// const reset = (st: SearchType) => () => {
//     removePositionMarker();
//     switch (st) {
//         case 'geocoder':
//             return setGeocoderInput(CLEAR_INPUT_TEXT);
//         case 'capakey':
//             return setSearchCapakey(CLEAR_INPUT_TEXT);
//         case 'coordinates':
//             return setSearchCoordinates(CLEAR_INPUT_TEXT);
//     }
// };

// const inputClass = (
//     base: string,
//     optContent: Option<string>,
// ) => optContent
//     .map(content => content === CLEAR_INPUT_TEXT ? `${base} clear` : base)
//     .getOrElse(base);

// const renderGeocoderInput = () =>
//     inputText(() => getGeocoderInput().getOrElse(''), setGeocoderInput, {
//         className: inputClass('locate-input geocoder', getGeocoderInput()),
//         id: 'locate-input geocoder',
//         placeholder: tr.geo('placeholderGeocode'),
//         key: 'geocoder-input',
//         onKeyPress: searchOnEnter('geocoder'),
//     });

// const renderCapakeyInput = () =>
//     inputText(() => getSearchCapakey().getOrElse(''), setSearchCapakey, {
//         className: inputClass('locate-input capakey', getSearchCapakey()),
//         id: 'locate-input capakey',
//         placeholder: tr.geo('placeholderGeocodeCapakey'),
//         key: 'capakey-input',
//         onKeyPress: searchOnEnter('capakey'),
//     });

// const renderCoordinatesInput = () =>
//     inputText(
//         () => getSearchCoordinates().getOrElse(''),
//         setSearchCoordinates,
//         {
//             className: inputClass('locate-input coordinates', getSearchCoordinates()),
//             id: 'locate-input coordinates',
//             placeholder: tr.geo('placeholderGeocodeCoordinates'),
//             key: 'coordinates-input',
//             onKeyPress: searchOnEnter('coordinates'),
//         }
//     );

// const selectSearchClass = (st: SearchType, c: string) =>
//     st === getSearchType() ? `${c} active` : `${c} interactive`;

// const selectSearchClick = (st: SearchType) => () => {
//     setSearchType(st);
//     setFocusId(`locate-input ${st}`)
// }

// const renderAdressSwitch = () =>
//     divTooltipBottom(
//         tr.geo('searchByAdress'),
//         {},
//         BUTTON({
//             className: selectSearchClass(
//                 'geocoder',
//                 'geo-btn geo-btn--3 input-switch tag'
//             ),
//             onClick: selectSearchClick('geocoder')
//         },
//             tr.geo('address')
//         )
//     );

// const renderCapakeySwitch = () =>
//     divTooltipBottom(
//         tr.geo('searchByCapakey'),
//         {},
//         BUTTON({
//             className: selectSearchClass(
//                 'capakey',
//                 'geo-btn geo-btn--3 input-switch tag'
//             ),
//             onClick: selectSearchClick('capakey')
//         },
//             tr.geo('parcelle')
//         )
//     );

// const renderCoordinatesSwitch = () =>
//     divTooltipBottom(
//         tr.geo('searchByCoordinates'),
//         {},
//         BUTTON({
//             className: selectSearchClass(
//                 'coordinates',
//                 'geo-btn geo-btn--3 input-switch tag'
//             ),
//             onClick: selectSearchClick('coordinates')
//         },
//             tr.geo('coordinates')
//         )
//     );

// const renderSearchButton = (st: SearchType) =>
//     divTooltipBottom(
//         tr.geo('launchSearch'),
//         {},
//         BUTTON({
//             className: 'btn__icon btn-analyse',
//             onClick: search(st),
//             key: 'geocoder-button'
//         },
//             nameToString('search')
//         )
//     );

// const renderResetButton = (st: SearchType) =>
//     BUTTON({
//         className: 'btn-reset btn__icon',
//         onClick: reset(st),
//     }, nameToString('times'));

// const renderSearchInputWrapper = (...n: React.ReactNode[]) =>
//     DIV({ className: 'input-wrapper' }, ...n);

// const renderGeocoderResults = (results: IUgWsResult[]) => {
//     return results.map(({ point, address }, key) => {
//         // const coords: [number, number] = [result.point.x, result.point.y];
//         const x = snap(point.x);
//         const y = snap(point.y);
//         if ('' === address.number) {
//             return DIV(
//                 {
//                     key,
//                     className: 'adress-result',
//                     onClick: () => {
//                         setGeocoderResponse(null);
//                         setAddress(getLang(), address);
//                         addPositionMarker([x, y]);
//                         centerMapOn(x, y);
//                     }
//                 },
//                 BUTTON({ className: 'select-icon' }),
//                 DIV(
//                     {},
//                     addressToString(address),
//                 )
//             );
//         }
//         return DIV(
//             {
//                 className: 'adress-result',
//                 key,
//                 onClick: () => {
//                     setGeocoderResponse(null);
//                     setAddress(getLang(), address);
//                     // navigateGeneral(x, y);
//                     addPositionMarker([x, y]);
//                     centerMapOn(x, y);
//                 }
//             },
//             BUTTON({ className: 'select-icon' }),
//             DIV({}, addressToString(address))
//         );
//     });
// };

// const renderGeocoderResultsWrapper = (...n: React.ReactNode[]) =>
//     DIV({ className: 'geocoder-wrapper' }, ...n);

// const renderGeocoder = (): React.ReactNode[] =>
//     getGeocoderResponse().fold(
//         [
//             renderSearchInputWrapper(
//                 renderAdressSwitch(),
//                 renderCapakeySwitch(),
//                 renderCoordinatesSwitch(),
//                 renderGeocoderInput(),
//                 renderResetButton('geocoder'),
//                 renderSearchButton('geocoder')
//             )
//         ],
//         ({ result }) => [
//             renderSearchInputWrapper(
//                 renderAdressSwitch(),
//                 renderCapakeySwitch(),
//                 renderCoordinatesSwitch(),
//                 renderGeocoderInput(),
//                 renderResetButton('geocoder'),
//                 renderSearchButton('geocoder')
//             ),
//             renderGeocoderResultsWrapper(renderGeocoderResults(result))
//         ]
//     );

// const renderCapakey = () =>
//     renderSearchInputWrapper(
//         renderAdressSwitch(),
//         renderCapakeySwitch(),
//         renderCoordinatesSwitch(),
//         renderCapakeyInput(),
//         renderResetButton('capakey'),
//         renderSearchButton('capakey')
//     );

// const renderCoordinates = () =>
//     renderSearchInputWrapper(
//         renderAdressSwitch(),
//         renderCapakeySwitch(),
//         renderCoordinatesSwitch(),
//         renderCoordinatesInput(),
//         renderResetButton('coordinates'),
//         renderSearchButton('coordinates')
//     );

// const renderCoordinatesHelp = () =>
//     DIV(
//         {
//             className: 'geocoder-wrapper'
//         },
//         tr.geo('helptext:searchByCoordinates')
//     );

// const render = () => {
//     const st = getSearchType();
//     switch (st) {
//         case 'geocoder':
//             return DIV({ className: 'locate-geocode' }, ...renderGeocoder());
//         case 'capakey':
//             return DIV({ className: 'locate-geocode' }, renderCapakey());
//         case 'coordinates':
//             return DIV(
//                 { className: 'locate-geocode' },
//                 renderCoordinates(),
//                 renderCoordinatesHelp()
//             );
//     }
// };
// export default render;

// logger('loaded');


type ResultAction = (
    result: ServiceResult,
    setView: Setter<IMapViewData>,
    setInteraction: Setter<Interaction>
) => NodeOrOptional;

const zoomTo = (
    coords: Coordinate,
    _setInteraction: Setter<Interaction>,
    setView: Setter<IMapViewData>
) => {
    const viewEvents = viewEventsFactory(setView);
    clearServiceResponse();
    viewEvents.updateMapView({
        dirty: 'geo',
        center: coords,
        zoom: 12,
    });
    //putMark(coords, setInteraction);
    setInteractionClick();
    //TODO put the pin mark, instead of setInteractionClick
    setPinnable(true);
    addPositionMarker(coords);
};

export const actionCenter: ResultAction = (
    result: ServiceResult,
    setView: Setter<IMapViewData>,
    setInteraction: Setter<Interaction>
) => {
    const coords: [number, number] = [result.coord.x, result.coord.y];
    const tooltip: Tooltip = {
        text: `${tr.core('zoomToAddress')}\n${result.message}` as Translated,
        position: 'bottom-right',
    };
    const button = makeIcon('select', 3, 'map-marker-alt', tooltip); // change here the icon, or add the pin icon in addition
    const action = () => zoomTo(coords, setInteraction, setView);
    return button(action);
};
