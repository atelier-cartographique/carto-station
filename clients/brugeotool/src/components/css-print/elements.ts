import { DIV } from 'sdi/components/elements';
import { navigateSystem } from 'brugeotool/src/events/route';
import {
    setMoreTextHiddenColumWise,
    setShowPartTableGeol,
    setShowPartTableHydro,
} from 'brugeotool/src/events/geothermie';
import { getX, getY, getSystem } from 'brugeotool/src/queries/geothermie';
import { getPersistentLayout } from 'brugeotool/src/queries/app';
import tr, { formatDate } from 'sdi/locale';
import { markdown } from 'sdi/ports/marked';

const buttonBack = () =>
    DIV(
        {
            className: 'geo-btn geo-btn--2',
            onClick: () => {
                setMoreTextHiddenColumWise('detailledLithology', true);
                setShowPartTableGeol(false);
                setShowPartTableHydro(false);
                navigateSystem(
                    getSystem().fold('close', s => s),
                    getPersistentLayout() === 'print'
                        ? 'general'
                        : getPersistentLayout()
                );
            },
        },
        tr.geo('backToAnalysis')
    );

const buttonPrint = () =>
    DIV(
        {
            className: 'geo-btn geo-btn--1',
            onClick: () => window.print(),
        },
        tr.core('print')
    );

export const printHelp = () =>
    DIV(
        { className: 'helptext' },
        markdown(tr.geo('helptext:printToPdf')),
        buttonBack(),
        buttonPrint()
    );

export const printDate = () =>
    DIV({}, `${tr.geo('date')}: ${formatDate(new Date())}`);

const BASE_URL =
    'https://geodata.environnement.brussels/client/brugeotool/general/';
// const system: string = getSystem()
//     .fold(
//         'technical',
//         s => s
//     );
export const projectUrl = () =>
    DIV({}, `${tr.geo('url')}: ${BASE_URL}${getX()}/${getY()}`);
