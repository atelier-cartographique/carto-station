import { getSystem } from 'brugeotool/src/queries/geothermie';
import close from './close';
import open from './open';
import technical from './technical';

export const render = () =>
    getSystem().foldL(technical, s => (s === 'close' ? close() : open()));

export default render;
