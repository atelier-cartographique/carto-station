import { DIV, IMG, H1, BR } from 'sdi/components/elements';
import tr from 'sdi/locale';

import textGeneral from '../text/general';
import textLicense from '../text/license';
import { engine } from '../carrot/engine';
import { renderOpenTable } from '../table';
import { renderSidebarTopContent } from '../sidebar/geothermie';

import { logoBE } from './logo';
import { printHelp, printDate, projectUrl } from './elements';
import { markdown } from 'sdi/ports/marked';
import { renderConnect } from '../table/connect';
import {
    renderLithology,
    renderHydroLegend,
} from '../legend/lithostratigraphy';

const { getImage } = engine();

const header = () =>
    DIV(
        'print-header',
        H1({}, tr.geo('printTitle')),
        printDate(),
        projectUrl(),
        IMG({ className: 'logo', src: logoBE }),
        BR({}),
        markdown(tr.geo('disclaimerPrint'))
    );

const page1 = () =>
    DIV('page', header(), renderSidebarTopContent(), textGeneral());

const page2 = () => DIV('page', H1({}, tr.geo('projectSteps')), textLicense());

const page3 = () =>
    DIV(
        'page landscape',
        DIV(
            'page__inner',
            H1({}, tr.geo('projectGeology')),
            DIV(
                { style: { display: 'flex' } },
                DIV('carrot ', IMG({ src: getImage(300, 800) })),
                renderConnect('carrot', 'depth'),
                renderOpenTable()
            )
        )
    );

const page4 = () =>
    DIV(
        'page',
        H1({}, tr.geo('lithoLegend')),
        renderLithology(),
        H1({}, tr.geo('hydroLegend')),
        renderHydroLegend()
    );

export const render = () =>
    DIV('css-print', printHelp(), page1(), page2(), page3(), page4());

export default render;
