import { DIV, H2 } from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { markdown } from 'sdi/ports/marked';

import {
    isExcluded,
    getSystem,
    getBedrockDepth,
    getLayerDepth,
    getPiezoValue,
    getLayersThicknesses,
    getConstraintRegion,
    isUS91,
} from '../../queries/geothermie';
import {
    generalInfoClosedOk,
    generalInfoClosedOkUS91,
    generalInfoOpenOkBefore,
    generalInfoOpenOkAfter,
    aquifere1,
    aquifere2,
    aquifere3,
    aquifere3US91,
    outOfRegionGeneralButInBufferFR,
    outOfRegionGeneralButInBufferNL,
} from 'brugeotool/src/locale/marked-texts';
import { getLang } from 'sdi/app';
import { getCenterAsLambert2008 } from 'brugeotool/src/queries/map';

export const numberAquifere = () =>
    (getPiezoValue('UH/RBC_4').fold(0, v => v) > -10000 ? 1 : 0) +
    // (getLayerDepth('US/RBC_81') > 0 ? 1 : 0) +
    // (getLayerDepth('US/RBC_92') > 0 ? 1 : 0);
    getLayersThicknesses().fold(0, thicknesses =>
        thicknesses[15] > 0 ? 1 : 0
    ) + // US/RBC_81
    getLayersThicknesses().fold(0, thicknesses =>
        thicknesses[18] > 0 ? 1 : 0
    ); // US/RBC_92

const outOfRegionGeneralButInBuffer = () => {
    const lang = getLang();
    const [x, y] = getCenterAsLambert2008().map(Math.round);
    if (lang === 'fr') {
        return outOfRegionGeneralButInBufferFR(x, y);
    } else if (lang === 'nl') {
        return outOfRegionGeneralButInBufferNL(x, y);
    }

    return 'missing translation';
};

const render = () =>
    getConstraintRegion() !== 'in'
        ? markdown(outOfRegionGeneralButInBuffer())
        : isExcluded()
        ? getSystem().getOrElse('close') === 'close'
            ? H2(
                  { className: 'paginator-text is-not-possible' },
                  markdown(tr.geo('generalInfoClosedNotOk'))
              )
            : H2(
                  { className: 'paginator-text is-not-possible' },
                  markdown(tr.geo('generalInfoOpenNotOk'))
              )
        : getSystem().getOrElse('close') === 'close'
        ? DIV(
              { className: 'paginator-text guide' },
              H2({ className: 'is-possible' }, tr.geo('closeIsPossible')),
              isUS91()
                  ? markdown(
                        fromRecord(
                            generalInfoClosedOkUS91(getLayerDepth('US/RBC_91'))
                        )
                    )
                  : markdown(fromRecord(generalInfoClosedOk(getBedrockDepth())))
          )
        : DIV(
              { className: 'paginator-text guide' },
              H2({ className: 'is-possible' }, tr.geo('openIsPossible')),
              markdown(fromRecord(generalInfoOpenOkBefore(numberAquifere()))),
              getPiezoValue('UH/RBC_4').fold('', v =>
                  v < -10000 ? '' : markdown(fromRecord(aquifere1(Math.abs(v))))
              ),
              getLayersThicknesses().fold('', thicknesses =>
                  thicknesses[15] > 0
                      ? markdown(
                            fromRecord(aquifere2(getLayerDepth('US/RBC_81')))
                        )
                      : ''
              ),
              getLayersThicknesses().fold('', thicknesses =>
                  thicknesses[18] > 0
                      ? isUS91()
                          ? markdown(
                                fromRecord(
                                    aquifere3US91(getLayerDepth('US/RBC_91'))
                                )
                            )
                          : markdown(fromRecord(aquifere3(getBedrockDepth())))
                      : ''
              ),
              markdown(fromRecord(generalInfoOpenOkAfter()))
          );

export default render;
