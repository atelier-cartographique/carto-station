import { NODISPLAY } from 'sdi/components/elements';

import { getLayout } from '../../queries/app';
import renderLicense from './license';
import renderGeneral from './general';

// const getTranslatedSystem = () => tr.geo(getSystem().getOrElse('close'));
//
// const getTranslatedGeothermalSystem = () =>
//     getSystem().getOrElse('close') === 'close'
//         ? tr.geo('closeGeothermal')
//         : tr.geo('openGeothermal');

const render = () => {
    switch (getLayout()) {
        case 'general':
            return renderGeneral();
        case 'license':
            return renderLicense();
        case 'finance':
        case 'geology':
        case 'technical':
        default:
            return NODISPLAY();
    }
};

export default render;
