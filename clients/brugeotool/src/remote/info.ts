import * as io from 'io-ts';
import { fetchIO } from 'sdi/source';
import { getApiUrl } from 'sdi/app';
import { FinanialDataRow } from '../components/table/table-financial';
import { MessageRecordIO } from 'sdi/source/io/io';

export const InfoStratiIO = io.interface(
    {
        descrnl: io.string,
        se_bhg: io.string,
        us_rbc: io.string,
        ere_fr: io.string,
        code_us: io.string,
        descrfr: io.string,
        syst_nl: io.string,
        ref_etage: io.number,
        serie_fr: io.string,
        ref_form_field: io.number,
        color: io.string,
        ere_nl: io.string,
        ref_ere: io.number,
        syst_fr: io.string,
        serie_nl: io.string,
        ref_syst: io.number,
        ref_serie: io.number,
        etage_fr: io.string,
        etage_nl: io.string,
    },
    'InfoStratiIO'
);

export type InfoStrati = io.TypeOf<typeof InfoStratiIO>;

export const InfoOpenIO = io.interface(
    {
        color_pot: io.string,
        he_bhg: io.string,
        ref_h_type: io.number,
        code_geogr: io.union([io.null, io.string]),
        poten_fr: io.string,
        h_staat: io.string,
        selectable: io.boolean,
        rmk_pot_fr: io.union([io.null, io.string]),
        kmax: io.union([io.null, io.number]),
        kmin: io.union([io.null, io.number]),
        color_uh: io.string,
        uh_rbc: io.string,
        code_uh: io.string,
        kavg: io.union([io.null, io.number]),
        poten_nl: io.string,
        h_etat: io.string,
        ref_h_etat: io.number,
        rmk_pot_nl: io.union([io.null, io.string]),
    },
    'InfoOpenIO'
);

export type InfoOpen = io.TypeOf<typeof InfoOpenIO>;

export const InfoHydroGeolIO = io.interface(
    {
        color: io.string,
        he_bhg: io.string,
        ref_h_type: io.number,
        code_geogr: io.union([io.null, io.string]),
        h_staat: io.string,
        kmax: io.union([io.null, io.number]),
        kmin: io.union([io.null, io.number]),
        ref_h_etat: io.number,
        uh_rbc: io.string,
        code_uh: io.string,
        h_syst_nl: io.string,
        ref_h_syst: io.number,
        h_etat: io.string,
        h_syst_fr: io.string,
        kavg: io.union([io.null, io.number]),
    },
    'InfoHydroGeolIO'
);

export type InfoHydroGeol = io.TypeOf<typeof InfoHydroGeolIO>;

export const InfoClosedIO = io.interface(
    {
        code_us: io.string,
        color: io.string,
        lambda_dry: io.union([io.null, io.number]),
        lambda_sat: io.number,
        us_rbc: io.string,
        remark_nl: io.union([io.null, io.string]),
        remark_fr: io.union([io.null, io.string]),
        se_bhg: io.string,
    },
    'InfoClosedIO'
);

export type InfoClosed = io.TypeOf<typeof InfoClosedIO>;

export const fetchInfoStrati = () =>
    fetchIO(
        io.array(InfoStratiIO),
        getApiUrl('geodata/geothermie/info/strati')
    );
export const fetchInfoOpen = () =>
    fetchIO(io.array(InfoOpenIO), getApiUrl('geodata/geothermie/info/open'));
export const fetchInfoHydroGeol = () =>
    fetchIO(
        io.array(InfoHydroGeolIO),
        getApiUrl('geodata/geothermie/info/hgeol')
    );
export const fetchInfoClosed = () =>
    fetchIO(
        io.array(InfoClosedIO),
        getApiUrl('geodata/geothermie/info/closed')
    );

export const fetchFinanialTableData = () => {
    const ret = [
        {
            class: 1,
            hotNeeds: '15',
            coldNeeds: '-',
            hotWaterNeeds: '4100',
            closeSysTubeLength: '92',
            closeSysTubesNumber: '1',
            closeSysDrillingDepth: '92',
            openSysFlow: '0.5',
            openSysDoubletNumber: '1',
        },
        {
            class: 2,
            hotNeeds: '45',
            coldNeeds: '-',
            hotWaterNeeds: '4100',
            closeSysTubeLength: '156',
            closeSysTubesNumber: '1',
            closeSysDrillingDepth: '156',
            openSysFlow: '0.8',
            openSysDoubletNumber: '1',
        },
        {
            class: 3,
            hotNeeds: '15',
            coldNeeds: '-',
            hotWaterNeeds: '28500',
            closeSysTubeLength: '562',
            closeSysTubesNumber: '3',
            closeSysDrillingDepth: '187',
            openSysFlow: '3',
            openSysDoubletNumber: '1',
        },
        {
            class: 4,
            hotNeeds: '45',
            coldNeeds: '-',
            hotWaterNeeds: '28500',
            closeSysTubeLength: '865',
            closeSysTubesNumber: '4',
            closeSysDrillingDepth: '216',
            openSysFlow: '4',
            openSysDoubletNumber: '1',
        },
        {
            class: 5,
            hotNeeds: '15',
            coldNeeds: '15',
            hotWaterNeeds: '5500',
            closeSysTubeLength: '1784',
            closeSysTubesNumber: '9',
            closeSysDrillingDepth: '198',
            openSysFlow: '9',
            openSysDoubletNumber: '1',
        },
        {
            class: 6,
            hotNeeds: '30',
            coldNeeds: '10',
            hotWaterNeeds: '5500',
            closeSysTubeLength: '2458',
            closeSysTubesNumber: '12',
            closeSysDrillingDepth: '205',
            openSysFlow: '13',
            openSysDoubletNumber: '1 - 2',
        },
        {
            class: 7,
            hotNeeds: '15',
            coldNeeds: '15',
            hotWaterNeeds: 'étude',
            closeSysTubeLength: '4865',
            closeSysTubesNumber: '24',
            closeSysDrillingDepth: '203',
            openSysFlow: '25',
            openSysDoubletNumber: '1 - 3',
        },
        {
            class: 8,
            hotNeeds: '30',
            coldNeeds: '10',
            hotWaterNeeds: 'étude',
            closeSysTubeLength: '6486',
            closeSysTubesNumber: '32',
            closeSysDrillingDepth: '203',
            openSysFlow: '33',
            openSysDoubletNumber: '1 - 4',
        },
        {
            class: 9,
            hotNeeds: '15',
            coldNeeds: '15',
            hotWaterNeeds: 'étude',
            closeSysTubeLength: '24324',
            closeSysTubesNumber: '118',
            closeSysDrillingDepth: '206',
            openSysFlow: '126',
            openSysDoubletNumber: '3 - 13',
        },
        {
            class: 10,
            hotNeeds: '30',
            coldNeeds: '10',
            hotWaterNeeds: 'étude',
            closeSysTubeLength: '32432',
            closeSysTubesNumber: '158',
            closeSysDrillingDepth: '205',
            openSysFlow: '167',
            openSysDoubletNumber: '4 - 17',
        },
    ];

    const p = new Promise((resolve, _) => {
        resolve(ret);
    });

    return p.then(data => {
        if (data) {
            return data as Promise<FinanialDataRow[]>;
        }
        throw data;
    });
};

// tslint:disable-next-line: variable-name
const InfoBulleIO = io.interface({
    id: io.Integer,
    group: io.string,
    content: MessageRecordIO,
});

// tslint:disable-next-line: variable-name
const InfoBulleArrayIO = io.array(InfoBulleIO);

export type InfoBulle = io.TypeOf<typeof InfoBulleIO>;

export const fetchInfoBulles = () =>
    fetchIO(InfoBulleArrayIO, getApiUrl('geodata/geothermie/bulles/'));
