import {
    PIEZO_LAYERS_NAME,
    HYDROGEOLOGY_UNITS_NAME,
    STRATIGRAPHY_UNITS_NAME,
    DRY_CONDUCTIVITY,
    SAT_CONDUCTIVITY,
    K_MIN,
    K_MIN_IF_USRBC43,
    K_MEAN,
    K_MEAN_IF_USRBC43,
    K_MAX,
    K_MAX_IF_USRBC43,
    HYDROLOGICAL_SYSTEM_NAME,
    GEOLOGICAL_ETAGE_LAYERS_NAME,
    GEOLOGICAL_SERIE_LAYERS_NAME,
    GEOLOGICAL_SYSTEM_LAYERS_NAME,
    GEOLOGICAL_ERA_LAYERS_NAME,
    US_GREY_SHADE,
} from '../components/settings';
import { fromNullable, fromPredicate, none, some } from 'fp-ts/lib/Option';

import { query, queryK } from 'sdi/shape';

import { IUgWsAddress } from 'sdi/ports/geocoder';
import { getLang } from 'sdi/app';
import { GeoMessageKey } from '../locale';
import { MoreTextHidden } from '../components/table/format';
import { ConnectName } from '../components/table/connect';
import { remoteToOption, ILayerInfo } from 'sdi/source';
import { index } from 'fp-ts/lib/Array';
import { getFeaturePropOption } from 'sdi/util';
import { CAKE_ON_MAP_LAYER } from '../events/geothermie';
import tr, { fromRecord } from 'sdi/locale';
import { Extent } from 'ol/extent';
import { POSITION_MARKER_LAYER } from '../events/map';
import { DisplayPiezo } from '../components/slice/legend';
import { getTopView, SLICE_LAYER_NAME } from './map';

export type BottomRender = 'cake' | 'slice' | 'map';

const BEDROCK_THICKNESS = 30;

export const getSystem = () => fromNullable(query('geo/system'));

export const getDisplayMapsSeparator = () =>
    query('geo/display/maps/separator');

export const getXY = () => fromNullable(query('geo/xy'));

export const getX = () => getXY().fold(0, xy => xy.x); // TODO other default value than 0?
export const getY = () => getXY().fold(0, xy => xy.y);

export const getDataPoint = () => query('geo/data/point');

export const getDataPointOption = () => remoteToOption(getDataPoint());

export const getDataConstraint = () => query('geo/data/constraint');

const getDataConstraintOption = () => remoteToOption(getDataConstraint());

export const isConstraintNatura = () =>
    getDataConstraintOption()
        .map(c => c.constraints)
        .fold(false, c => c.natura);

export const isConstraintWater = () =>
    getDataConstraintOption()
        .map(c => c.constraints)
        .fold(false, c => c.water);

export const isExcluded = () => isConstraintMetroNord() || isConstraintWater();

export const isStrictlyConstraintSoil = () =>
    getDataConstraintOption()
        .map(c => c.constraints)
        .fold(false, c => c.soil !== 'none');

export const isConstraintSoil = () => {
    switch (getConstraintSoil()) {
        case '1':
        case '2':
            return false;
        case '0':
        case '0+1':
        case '0+2':
        case '3':
        case '4':
        case '0+3':
        case '0+4':
            return true;
        case 'none':
        default:
            return false;
    }
};

export const getConstraintSoil = () =>
    getDataConstraintOption()
        .map(c => c.constraints)
        .fold('none', c => c.soil);

export const isConstraintMetroNord = () =>
    getDataConstraintOption()
        .map(c => c.constraints)
        .fold(false, c => c.metro_nord);

export const isConstraints = () =>
    isConstraintSoil() ||
    isExcluded() ||
    isConstraintNatura() ||
    isConstraintMetroNord();

export const getConstraintRegion = () =>
    getDataConstraintOption()
        .map(c => c.constraints)
        .map(constraints => constraints.region)
        .getOrElse('none');

export const isOutRegion = () => getConstraintRegion() === 'none';

export const getAddress = () => fromNullable(query('geo/address'));

export const getFormatAddress = () =>
    getAddress()
        .chain(addr =>
            fromNullable(
                (() => {
                    const lc = getLang();
                    switch (lc) {
                        case 'fr':
                            return addr.fr;
                        case 'nl':
                            return addr.nl;
                        default:
                            return null;
                    }
                })()
            )
        )
        .map(formatAddress);

const formatAddress = (a: IUgWsAddress) =>
    a.number +
    ' ' +
    a.street.name +
    ', ' +
    a.street.postCode +
    ' ' +
    a.street.municipality;

export const getDisplayDataswitch = () => {
    const ps = query('geo/display/dataswitch');
    return ps[ps.length - 1];
};

export const getInfoPanel = getDisplayDataswitch;

export const getBottomRender = () => fromNullable(query('geo/bottom-render'));

export const foldBottomRender = <T>(b: BottomRender, yes: T, no: T) =>
    getBottomRender()
        .chain(fromPredicate(which => which === b))
        .map(() => yes)
        .getOrElse(no);

export const getCakeMode = () => query('geo/cake/mode');

export const getCakeRes = () => query('geo/cake/res');

export const getCakeQuatAlpha = () => query('geo/cake/quat-alpha');

export const getSliceData = () => query('geo/slice/data');

export const getGraphXy = () => fromNullable(query('geo/graph/xy'));

export const getGraphSnapXy = () => fromNullable(query('geo/graph/snap-xy'));

export const getDiagramHeight = () => query('geo/diagram/height');

export const getCarrotWidth = () => query('geo/diagram/carrot-width');

export const getGraphWidth = () => query('geo/diagram/graph-width');

/******************************/
/* QUERIES ABOUT GroundLayers */
/******************************/
// todo fixme question : create a new file for theses queries ie. queries/groundLayers.ts ?

export const getElevation = () =>
    getDataPointOption()
        .map(d => d.thickness[0])
        .fold(0, e => e);

export const getPhreaticHead = () =>
    getDataPointOption()
        .map(d => d.phreatic_head)
        .fold(0, p => p[0]);

export const getPhreaticHeadDepth = () => -getElevation() + getPhreaticHead();

export const getLayersThicknesses = () =>
    getDataPointOption().map(d =>
        d.thickness.slice(1, d.thickness.length).concat([BEDROCK_THICKNESS])
    );

export const getQuaternaireThicknesses = () => {
    const thicknesses: number[] = Array(4).fill(0);
    const raw = getDataPointOption()
        .map(d => d.quat_thickness)
        .getOrElse([0, 0, 0, 0]);
    if (raw[0] > 0) {
        // US/RBC11-12
        thicknesses[0] = raw[0] / 2;
        thicknesses[1] = raw[0] / 2;
    }
    if (raw[1] > 0) {
        // US/RBC11-13
        thicknesses[0] = raw[1] / 3;
        thicknesses[1] = raw[1] / 3;
        thicknesses[2] = raw[1] / 3;
    }
    if (raw[2] > 0) {
        // US/RBC14
        thicknesses[3] = raw[2];
    }
    return thicknesses;
};

export const quaternaireLayerCount = () =>
    getQuaternaireThicknesses().reduce((acc, t) => acc + (t > 0 ? 1 : 0), 0);

export const getQuaternaireUS11_12Thickness = () =>
    getDataPointOption()
        .map(d => d.quat_thickness)
        .fold(none, t => (t[0] !== 0 ? some(t[0]) : none));

export const getQuaternaireUS11_13Thickness = () =>
    getDataPointOption()
        .map(d => d.quat_thickness)
        .fold(none, t => (t[1] !== 0 ? some(t[1]) : none));

export const getQuaternaireUS14Thickness = () =>
    getDataPointOption()
        .map(d => d.quat_thickness)
        .fold(none, t => (t[2] !== 0 ? some(t[2]) : none));

export const isUS13 = () => getQuaternaireThicknesses()[2] > 0;

export const isUS91 = () =>
    getLayersThicknesses().fold(false, arr => arr[getLayerId('US/RBC_91')] > 0);

export const getLayersTotalDepth = () =>
    /*
     * total of the depth for all the layers (m)
     */
    getLayersThicknesses().fold(0, layersThicknesses =>
        layersThicknesses.reduce((sum, curThickness) => sum + curThickness, 0)
    );

export const getBedrockDepth = () =>
    /*
     * Depth of the bedrock (m)
     */
    getLayersTotalDepth() - BEDROCK_THICKNESS;

export const getQLayersTotalDepth = () =>
    getQuaternaireThicknesses().reduce(
        (sum, curThickness) => sum + curThickness,
        0
    );

export const countLayersUntilLayerName = (layerName: GeoMessageKey) => {
    const thicknesses = getLayersThicknesses().fold([], arr => arr);
    return thicknesses
        .slice(0, getLayerId(layerName))
        .reduce((acc, t) => acc + (t > 0 ? 1 : 0), 0);
};

export const getLayerId = (layerName: GeoMessageKey) =>
    STRATIGRAPHY_UNITS_NAME.indexOf(layerName);

export const getLayerDepth = (layerName: GeoMessageKey) =>
    getLayersThicknesses().fold(0, layersThicknesses => {
        const layerId = getLayerId(layerName);
        return layersThicknesses.reduce((ret, curThickness, currentId) =>
            currentId >= layerId ? ret : ret + curThickness
        );
    });

export const getUH_RBC1aThicknessFactor = () =>
    (getQuaternaireThicknesses()[0] +
        getQuaternaireThicknesses()[1] +
        getQuaternaireThicknesses()[2]) /
    getQLayersTotalDepth();

export const getUH_RBC1bThicknessFactor = () =>
    getQuaternaireThicknesses()[3] / getQLayersTotalDepth();

export const getUSName = (u: GeoMessageKey) =>
    u === 'US/RBC_1' ? tr.geo('US/RBC 11-14') : tr.geo(u);

export const getHydroLayersThicknesses = () =>
    getLayersThicknesses().fold([], t => [
        t[0] * getUH_RBC1aThicknessFactor(), // UH/RBC1a
        t[0] * getUH_RBC1bThicknessFactor(), // UH/RBC1b
        [t[1], t[2], t[3], t[4]].reduce((a, b) => a + b), // UH/RBC2
        t[5], // UH/RBC3
        [t[6], t[7], t[8], t[9]].reduce((a, b) => a + b), // UH/RBC4
        ...t.slice(10),
    ]);

export const getGeologicalEraThicknesses = () =>
    getLayersThicknesses().fold([], t => [
        t[0], // US/RBC1
        t.slice(1, t.length - 2).reduce((a, b) => a + b),
        t[t.length - 2], // US/RBC91
        t[t.length - 1], // US/RBC92
    ]);

export const getGeologicalSystemThicknesses = () =>
    getLayersThicknesses().fold([], t => [
        t[0], // US/RBC1
        [t[1], t[2]].reduce((a, b) => a + b), // US/RBC21 & US/RBC22
        t.slice(3, t.length - 2).reduce((a, b) => a + b),
        t[t.length - 2], // US/RBC91
        t[t.length - 1], // US/RBC92
    ]);

const getQuaternaireThicknessFactor = () =>
    getQuaternaireThicknesses().map(tt => tt / getQLayersTotalDepth());

export const getGeologicalSerieThicknesses = () =>
    getLayersThicknesses().fold([], t => [
        t[0] * getQuaternaireThicknessFactor()[0], // US/RBC11
        t[0] * getQuaternaireThicknessFactor()[1], // US/RBC12
        t[0] * getQuaternaireThicknessFactor()[2], // US/RBC13
        t[0] * getQuaternaireThicknessFactor()[3], // US/RBC14
        t[1], // US/RBC21
        t[2], // US/RBC22
        t[3], // US/RBC23
        t.slice(4, 9).reduce((a, b) => a + b),
        t.slice(9, 15).reduce((a, b) => a + b),
        t.slice(15, 17).reduce((a, b) => a + b),
        t[17], // US/RBC91
        t[18], // US/RBC92 = BEDROCK_THICKNESS
    ]);

export const getGeologicalEtageThicknesses = () =>
    getLayersThicknesses().fold([], t => [
        t[0] * getQuaternaireThicknessFactor()[0], // US/RBC11
        t[0] * getQuaternaireThicknessFactor()[1], // US/RBC12
        t[0] * getQuaternaireThicknessFactor()[2], // US/RBC13
        t[0] * getQuaternaireThicknessFactor()[3], // US/RBC14
        t[1], // US/RBC21
        t[2], // US/RBC22
        t[3], // US/RBC23
        t.slice(4, 7).reduce((a, b) => a + b),
        t[7], // US/RBC42
        t[8], // US/RBC43
        t.slice(9, 11).reduce((a, b) => a + b),
        t.slice(11, 15).reduce((a, b) => a + b),
        t.slice(15, 17).reduce((a, b) => a + b),
        t[17], // US/RBC91
        t[18], // US/RBC92 = BEDROCK_THICKNESS
    ]);

export const getHydroSystemThicknesses = () =>
    getHydroLayersThicknesses().length === 14
        ? [
              getHydroLayersThicknesses()
                  .slice(1, 10)
                  .reduce((a, b) => a + b), // UH/RBC1 -> US/RBC7c
              getHydroLayersThicknesses()
                  .slice(10, 13)
                  .reduce((a, b) => a + b), // UH/RBC1 -> US/RBC7c
          ]
        : [];

export const getHydroLayerId = (layerName: GeoMessageKey) =>
    HYDROGEOLOGY_UNITS_NAME.indexOf(layerName);

export const getHydroSystemLayerId = (hydroSystem: GeoMessageKey) =>
    HYDROLOGICAL_SYSTEM_NAME.indexOf(hydroSystem);

export const getGeologicalEtageLayerId = (name: GeoMessageKey) =>
    GEOLOGICAL_ETAGE_LAYERS_NAME.indexOf(name);

export const getGeologicalSerieLayerId = (name: GeoMessageKey) =>
    GEOLOGICAL_SERIE_LAYERS_NAME.indexOf(name);

export const getGeologicalSystemLayerId = (name: GeoMessageKey) =>
    GEOLOGICAL_SYSTEM_LAYERS_NAME.indexOf(name);

export const getGeologicalEraLayerId = (name: GeoMessageKey) =>
    GEOLOGICAL_ERA_LAYERS_NAME.indexOf(name);

export const getHydroLayerDepth = (layerName: GeoMessageKey) => {
    const layerId = getHydroLayerId(layerName);
    return getHydroLayersThicknesses().reduce(
        (ret, curThickness, currentId) =>
            currentId >= layerId ? ret : ret + curThickness,
        0
    );
};

export const isUH1b = () => getHydroLayersThicknesses()[1] !== 0;

export const getPiezo = () =>
    // order : UH/RBC1b, UH/RBC2, UH/RBC4, UH/RBC6, UH/RBC7b, UH/RBC8a
    getDataPointOption()
        .map(d => d.piezo)
        .fold([0, 0, 0, 0, 0, 0], arr => arr.map(p => p - getElevation()));

export const hasPiezo = (layerId: string) =>
    PIEZO_LAYERS_NAME.reduce(
        (has, piezoId) => has || layerId == piezoId,
        false
    );

export const getPiezoIndex = (layerName: GeoMessageKey) =>
    PIEZO_LAYERS_NAME.indexOf(layerName);

export const getHydroLayerIndex = (layerName: GeoMessageKey) =>
    HYDROGEOLOGY_UNITS_NAME.indexOf(layerName);

export const getPiezoValue = (layerName: GeoMessageKey) => {
    const piezoId = getPiezoIndex(layerName);
    return piezoId == -1 ? none : some(getPiezo()[piezoId]);
};

export const getSatLevelForUS_RBC_11_13 =
    // special computation for US/RBC11 & US/RBC12 & US/RBC13
    () => {
        const dataPoint = getDataPointOption();
        return dataPoint.fold(
            0.0,
            dp =>
                (dp.saturation_rate[0] * dp.quat_thickness[0] +
                    dp.saturation_rate[1] * dp.quat_thickness[1] +
                    dp.saturation_rate[2] * dp.quat_thickness[2]) /
                (dp.quat_thickness[0] +
                    dp.quat_thickness[1] +
                    dp.quat_thickness[2])
        );
    };

export const getLayersSat = () =>
    getDataPointOption()
        .map(d => d.saturation_rate)
        .fold(
            [
                0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
            ],
            arr =>
                [
                    getSatLevelForUS_RBC_11_13(),
                    ...arr.slice(3, 20),
                    100, // US/RBC92 (BEDROCK)
                ].map(s => s / 100)
        );

export const getHydroLayersSat = () => {
    const sat = getLayersSat();
    return [
        sat[0], // UH/RBC1a
        sat[0], // UH/RBC1b
        Math.max(...[sat[1], sat[2], sat[3], sat[4]]), // UH/RBC2
        sat[5], // UH/RBC3
        Math.max(...[sat[6], sat[7], sat[8], sat[9]]), // UH/RBC4
        ...sat.slice(10),
    ];
};

export const getDryConductivity = () => DRY_CONDUCTIVITY;
export const getSatConductivity = () => SAT_CONDUCTIVITY;

export const getDrySatConductivity = () => {
    const arr: number[] = [];
    getDryConductivity().map((l, i) => {
        arr.push(l); // dry
        arr.push(getSatConductivity()[i]); // sat
    });
    return arr;
};

export const getDrySatThickness = () => {
    const arr: number[] = [];
    const sat = getLayersSat();
    getLayersThicknesses().fold([], t =>
        t.map((l, i) => {
            arr.push(l * (1 - sat[i]));
            arr.push(l * sat[i]);
        })
    );
    return arr;
};

export const computeCumulDrySatConductivity = () =>
    getDrySatConductivity()
        .map((l, i) => l * getDrySatThickness()[i])
        .reduce((a, x, i) => [...a, x + (a[i - 1] || 0)], [])
        .map((ll, ii) => ll / getDrySatLayersDepths()[ii]);

export const getDrySatLayersDepths = () =>
    getDrySatThickness().reduce((a, x, i) => [...a, x + (a[i - 1] || 0)], []);
// getDepthMode() === 'relative'
//     ? getDrySatThickness().reduce(
//             (a, x, i) => [...a, x + (a[i - 1] || 0)], []
//         )
//     : getDrySatThickness().reduce(
//             (a, x, i) => [...a, x + (a[i - 1] || 0)], []
//         ).map(d => getElevation() - d);

export const formatK = (n: number) => n.toExponential(1);

export const getKMin = () =>
    getLayersThicknesses().fold([], arr =>
        arr[STRATIGRAPHY_UNITS_NAME.indexOf('US/RBC_43')] === 0
            ? K_MIN
            : K_MIN_IF_USRBC43
    );
export const getKMean = () =>
    getLayersThicknesses().fold([], arr =>
        arr[STRATIGRAPHY_UNITS_NAME.indexOf('US/RBC_43')] === 0
            ? K_MEAN
            : K_MEAN_IF_USRBC43
    );
export const getKMax = () =>
    getLayersThicknesses().fold([], arr =>
        arr[STRATIGRAPHY_UNITS_NAME.indexOf('US/RBC_43')] === 0
            ? K_MAX
            : K_MAX_IF_USRBC43
    );

export const getGreyShades = () =>
    US_GREY_SHADE.map(uhcl => {
        switch (uhcl) {
            default:
            case 'light':
                return { left: '#F8F8F8', right: '#C4C4C4' };
            case 'medium':
                return { left: '#C6C6C6', right: '#949494' };
            case 'dark':
                return { left: '#707070', right: '#3d3d3d' };
        }
    });

export const getGreyShadeQuaternaire = (index: number) => {
    switch (index) {
        case 0:
        case 1:
        default:
            return { left: '#C6C6C6', right: '#949494' };
        case 2:
            return { left: '#F8F8F8', right: '#C4C4C4' };
    }
};

export const getPatternIdQuaternaire = (index: number): number => {
    switch (index) {
        case 0:
            return 616;
        case 1:
            return 619;
        case 2:
        default:
            return 602;
    }
};

export const getShowPartTableGeol = () => query('geo/table/show/geol');

export const getShowPartTableHydro = () => query('geo/table/show/hydro');

export const getMoreTextHidden = () => query('geo/table/text-hidden');

export const getMoreTextHiddenArray = (key: keyof MoreTextHidden) =>
    query('geo/table/text-hidden')[key];

export const getMoreTextHiddenItem = (
    key: keyof MoreTextHidden,
    index: number
) => getMoreTextHiddenArray(key)[index];

export const getShowPartTableFinanceThermicNeeds = () =>
    query('geo/finance/show/thermic/needs');

export const getFinanceTableData = () => query('geo/finance/table/data');

export const getConnect = (name: ConnectName) =>
    fromNullable(query('geo/connect')[name]);

export const getSearchType = queryK('geo/search/type');

export const getSearchCapakey = () => fromNullable(query('geo/search/capakey'));

export const getSearchCapakeyResult = queryK('geo/search/capakey/result');

export const getSearchCoordinates = () =>
    fromNullable(query('geo/search/coordinates'));

export const getPinnable = () => query('geo/map/interaction/pin');

export const isMeasureBox = () => query('geo/map/measure/box');

export const getCurrentCapakey = () =>
    remoteToOption(query('geo/capakey/current'))
        .chain(fc => index(0, fc.features))
        .chain(feature => getFeaturePropOption<string>(feature, 'capakey'));

export const withoutToolingLayers = (ls: ILayerInfo[]) =>
    ls.filter(
        ({ id }) =>
            id !== POSITION_MARKER_LAYER &&
            id !== CAKE_ON_MAP_LAYER &&
            id !== SLICE_LAYER_NAME
    );

export const getInputDepth = () => fromNullable(query('geo/form/depth'));

export const getConductivity = () =>
    fromNullable(query('geo/form/conductivity')).getOrElse(0);

export const getInputUH = () => fromNullable(query('geo/form/uh'));

export const findUHInfo = (code: string) =>
    fromNullable(query('geo/info/open').find(info => info.code_uh === code));

export const getUHPotentialColor = (code: string) =>
    findUHInfo(code)
        .map(info => `rgb(${info.color_pot})`)
        .getOrElse('transparent');

export const getUHPotentialLabel = (code: string) =>
    findUHInfo(code)
        .map<string>(info =>
            fromRecord({
                fr: info.poten_fr,
                nl: info.poten_nl,
            })
        )
        .getOrElse('Unknown');

export const getDepthMode = () => query('geo/depth/mode');

export const getWMSLegendVisible = () => query('geo/legend/wms-visible');

const extentNotVoid = fromPredicate<Extent>(
    ([x0, y0, x1, y1]) => y1 - y0 > 1 && x1 - x0 > 1
);

export const getArchiExtent = () =>
    fromNullable(getTopView().extent).chain(extentNotVoid);

export const getSliceResolution = queryK('geo/slice/res');

export const getDepthScaling = queryK('geo/depth-scaling');

export const getSliceGeometry = () => fromNullable(query('geo/slice/geometry'));
// .chain(geom => {
//     if (geom.coordinates.length > 1) {
//         const start = geom.coordinates[0];
//         const end = geom.coordinates[geom.coordinates.length - 1];
//         if (start[0] <= end[0]) {
//             return some({
//                 start,
//                 end,
//             });
//         } else {
//             return some({ start: end, end: start });
//         }
//     }
//     return none;
// });

export const getSliceGeometryLength = () =>
    //TODO use getSliceGeometrySegments and make the sum
    fromNullable(query('geo/slice/geometry')).chain(geom => {
        const data = geom.coordinates;

        let length = 0;
        for (let i = 0; i < data.length - 1; i += 1) {
            length =
                length +
                Math.sqrt(
                    (data[i][0] - data[i + 1][0]) ** 2 +
                        (data[i][1] - data[i + 1][1]) ** 2
                );
        }
        return some(length);
    });

export const getSliceGeometrySegments = () =>
    fromNullable(query('geo/slice/geometry')).chain(geom => {
        const data = geom.coordinates;

        const segments = [];
        for (let i = 0; i < data.length - 1; i += 1) {
            segments.push(
                Math.sqrt(
                    (data[i][0] - data[i + 1][0]) ** 2 +
                        (data[i][1] - data[i + 1][1]) ** 2
                )
            );
        }
        return some(segments);
    });

export const getSlice = () => remoteToOption(query('geo/slice'));

export const getDisplayPiezo = (key: keyof DisplayPiezo) =>
    query('geo/slice/piezo')[key];

export const getDisplayPiezzoOpt = (key: keyof DisplayPiezo) =>
    fromPredicate((k: keyof DisplayPiezo) => query('geo/slice/piezo')[k])(key);

export const displayPiezo = (piezoName: string) =>
    getDisplayPiezo(piezoName as keyof DisplayPiezo);

export const getDisplayPhreatic = queryK('geo/slice/phreatic');
export const getDisplayPhreaticOpt = () =>
    getDisplayPhreatic() ? some(true) : none;

export const getGeologicalGroupId = queryK('geo/geological-group-id');

export const getMapButtonHelpText = () => query('geo/map/button/helptext');
