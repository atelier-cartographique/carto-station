/*
 *  Copyright (C) 2020 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { loop, rect, loadAlias, getApiUrl } from 'sdi/app';
import { DIV, NodeOrOptional } from 'sdi/components/elements';
import header from 'sdi/components/header';
import { maintenance } from 'sdi/components/maintenance';
import tr from 'sdi/locale';
import { loadAllServices } from 'sdi/geocoder/events';

import footer from './components/footer';
import { loadRoute } from './events/route';
import { getLayout } from './queries/app';

import home from './components/home';
import {
    general,
    license,
    geology,
    finance,
    technical,
} from './components/geothermie';
import cssPrint from './components/css-print';
import modal from './components/modal';
import {
    loadSelectionOfMaps,
    loadBaseLayers,
    setMainRect,
    loadInfoBulles,
} from './events/app';
import { loadInfoAll } from './events/geothermie';

const mainRect = rect(({ width, height }) => setMainRect(width, height));

const wrappedMain = (name: string, ...elements: NodeOrOptional[]) =>
    DIV(
        {
            className: 'app-inner geo-inner',
            ref: mainRect,
        },
        modal(),
        header('brugeotool'),
        maintenance('brugeotool'),
        DIV({ className: `main ${name}` }, ...elements),
        footer()
    );

const renderHome = () => wrappedMain('home', home());

const renderGeneral = () => wrappedMain('general', general());
const renderLicense = () => wrappedMain('license', license());
const renderGeology = () => wrappedMain('geology', geology());
const renderFinance = () => wrappedMain('finance', finance());
const renderTechnical = () => wrappedMain('technical', technical());

const render = () => {
    const layout = getLayout();
    switch (layout) {
        case 'home':
            return renderHome();
        case 'general':
            return renderGeneral();
        case 'license':
            return renderLicense();
        case 'geology':
            return renderGeology();
        case 'finance':
            return renderFinance();
        case 'technical':
            return renderTechnical();
        case 'print':
            return cssPrint();
    }
};

const effects = (initialRoute: string[]) => () => {
    loadAllServices();
    loadRoute(initialRoute);
    loadSelectionOfMaps();
    loadInfoBulles();
    loadBaseLayers();
    loadInfoAll();
    loadAlias(getApiUrl('alias'));
    tr.init_edited();
};

export const app = (initialRoute: string[]) =>
    loop('brugeotool', render, effects(initialRoute));
