/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { A, DIV } from 'sdi/components/elements';
import { create, IMapOptions, multiSelectOptions } from 'sdi/map';
import { none, some } from 'fp-ts/lib/Option';
import langSwitch from 'sdi/components/lang-switch';
import geocoder from 'sdi/components/geocoder';
import { buttonTooltipLeft } from 'sdi/components/tooltip';
import tr from 'sdi/locale';
import { getRoot } from 'sdi/app';
import { nameToString } from 'sdi/components/button/names';
import { EmbedProfile } from 'sdi/source';
import { mapRightToolsSimplified } from 'sdi/map/controls';

import {
    getCurrentBaseLayers,
    getMapInfo,
    getEmbedProfile,
    getLayerInfoList,
    getCurrentSelection,
    withoutProfileFancy,
    withProfileFancy,
} from '../queries/app';
import { getView, getInteractionMode, getInteraction } from '../queries/map';
import {
    viewEvents,
    scalelineEvents,
    dispatchView,
    setInteraction,
} from '../events/map';
import { selectFeatures } from '../events/app';
import fancy from './fancy';

const logger = debug('sdi:comp/map');

export const mapName = 'main-view';

const getInfo = () => getMapInfo().fold(null, m => m);

// const selectFeature = (lid: string, fid: string | number) => {
//     setCurrentFeatureById(lid, fid);
//     setLayout('map-and-feature');
// };

// const clearSelection = () => {
//     unsetCurrentFeature();
//     setLayout('map');
// };

const options: IMapOptions = {
    getBaseLayer: getCurrentBaseLayers,
    getView,
    element: null,
    getMapInfo: getInfo,
    getMapLayerInfo: getLayerInfoList,
    updateView: viewEvents.updateMapView,
    setScaleLine: scalelineEvents.setScaleLine,
};

let mapSetTarget: (t: HTMLElement | null) => void;
let mapUpdate: () => void;

// const getSelected = () => ({
//     featureId: getFeatureId(),
//     layerId: getCurrentLayer(),
// });

const attachMap = () => (element: HTMLElement | null) => {
    // logger(`attachMap ${typeof element}`);
    if (!mapUpdate) {
        const { update, setTarget, selectable } = create(mapName, {
            ...options,
            element,
        });
        mapSetTarget = setTarget;
        mapUpdate = update;

        selectable(
            multiSelectOptions({
                selectFeatures,
                getSelected: getCurrentSelection,
            }),
            getInteraction
        );

        // highlightable(getSelected);
    }
    if (element) {
        mapSetTarget(element);
    } else {
        mapSetTarget(null);
    }
};

const atlasButtonLink = () =>
    getMapInfo().fold(DIV({}), mapInfo =>
        buttonTooltipLeft(
            tr.embed('atlasLinkLabel'),
            {
                className: 'btn btn-1 atlas-link icon-only',
            },
            A(
                {
                    href: `${getRoot()}view/${mapInfo.id}`,
                    target: '_top',
                    className: 'fa icon',
                },
                nameToString('external-link-alt')
            )
        )
    );

const fromProfile = (profile: EmbedProfile) => {
    switch (profile) {
        case 'all-tools':
            return some(
                DIV(
                    'tools',
                    geocoder(dispatchView, setInteraction),
                    mapRightToolsSimplified(dispatchView)
                )
            );
        default:
            return none;
    }
};

const render = () => {
    if (mapUpdate) {
        mapUpdate();
    }

    return DIV(
        `map-wrapper map-interaction-${getInteractionMode()}`,
        DIV({
            className: 'map',
            ref: attachMap(),
        }),
        DIV('brand-logo-box'),
        withProfileFancy(fancy),
        langSwitch('embed'),
        withoutProfileFancy(atlasButtonLink),
        fromProfile(getEmbedProfile())
    );
};

export default render;

logger('loaded');
