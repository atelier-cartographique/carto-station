import { ILayerInfo, getFeatureTitle, getMessageRecord } from 'sdi/source';
import {
    DETAILS,
    DIV,
    // H1,
    H4,
    H5,
    NodeOrOptional,
    SPAN,
    SUMMARY,
} from 'sdi/components/elements';
import tr, { fromRecord } from 'sdi/locale';
import { renderConfig } from 'sdi/components/feature-view';
import timeserie from 'sdi/components/timeserie';

import {
    getSelectedFeatures,
    pathsSortedByLayers,
    // getMapInfo,
    getCurrentLayer,
    getDatasetMetadata,
    withProfileFancy,
    moreThanOneLayer,
    getEmbedProfile,
    withDesktop,
} from '../queries/app';
// import { setLayout } from '../events/app';
import { dispatchTimeserie, loadData } from '../events/timeserie';
import { getData, queryTimeserie } from '../queries/timeserie';
import { zoomToFeatures } from '../events/map';
import { fromNullable, fromPredicate } from 'fp-ts/lib/Option';
import { makeIcon } from 'sdi/components/button';
import { FeaturePathInstance } from 'sdi/map';
import { renderLegendItem } from './legend/feature';
import renderLegend from './legend';
import { deselectFeature } from '../events/app';
import {
    clearPosition,
    onSelectionChange,
    setListRect,
    setPosition,
} from './fancy';

const featureRef =
    (lid: string, fid: number | string) => (element: HTMLElement | null) => {
        if (element === null) {
            clearPosition(lid, fid);
        } else {
            setPosition(lid, fid, element);
        }
    };

const zoomGroupBtn = makeIcon('zoomOnFeature', 3, 'search', {
    text: () => tr.embed('zoomOnFeatureGroup'),
    position: 'left',
});

const tsPlotter = timeserie(
    queryTimeserie,
    getData,
    getCurrentLayer,
    dispatchTimeserie,
    loadData
);

const deselectBtn = makeIcon('close', 3, 'times', {
    text: () => tr.core('unselect'),
    position: 'left',
});

const selectionHeader = (info: ILayerInfo) =>
    getDatasetMetadata(info.metadataId).map(md =>
        H4(
            'kv layer-name',
            SPAN('value', fromRecord(getMessageRecord(md.resourceTitle)))
        )
    );

const selectionDetails = (
    summary: NodeOrOptional,
    details: NodeOrOptional,
    close: boolean
) =>
    DETAILS(
        {
            className: 'collapsible-wrapper',
            onClick: () => setTimeout(onSelectionChange, 128),
            open: !close,
        },
        SUMMARY('collapsible-wrapper__summary', summary),
        details
    );

const renderSummary = ({ feature, layer }: FeaturePathInstance) =>
    DIV(
        'feature-summary',
        DIV({
            className: 'feature-anchor',
            ref: featureRef(layer.id, feature.id),
        }),
        renderLegendItem(layer, feature),
        getFeatureTitle(feature, layer.featureViewOptions)
            .map(title => H5('feature-title', title))
            .getOrElse(H5('feature-title', feature.id)),
        DIV(
            'actions',
            deselectBtn(() => deselectFeature(layer.id, feature.id))
            // zoomBtn(() => zoomToFeatures([feature]))
        )
    );

const renderFeatureInfoGroup = (
    key: string,
    paths: FeaturePathInstance[],
    multi: boolean
) =>
    DIV(
        { className: 'group', key },
        DIV(
            'group-title',
            moreThanOneLayer(
                fromNullable(paths.find(({ layer }) => layer.id === key)).map(
                    ({ layer }) => selectionHeader(layer)
                )
            ),

            fromPredicate((l: FeaturePathInstance[]) => l.length > 1)(
                paths
            ).map(paths =>
                DIV(
                    'actions',

                    SPAN(
                        'features number',
                        `${paths.length} `,
                        tr.embed('element')
                    ),
                    zoomGroupBtn(() =>
                        zoomToFeatures(
                            paths.map(({ feature }) => {
                                return feature;
                            })
                        )
                    )
                )
            )
        ),
        DIV(
            'group-content',
            ...paths.map(({ feature, layer }) =>
                selectionDetails(
                    renderSummary({ feature, layer }),
                    DIV(
                        'feature-info-wrapper',
                        renderConfig(
                            layer.featureViewOptions,
                            feature,
                            tsPlotter
                        )
                    ),
                    multi &&
                        withDesktop()
                            .chain(() => withProfileFancy(() => true))
                            .getOrElse(false)
                )
            )
        )
    );

const renderFeatureInfoMulti = (paths: FeaturePathInstance[]) =>
    DIV(
        {
            className: `feature-list ${paths.length > 1 ? 'multi' : 'single'}`,
            ref: setListRect,
            onScroll: () => {
                if (getEmbedProfile() === 'fancy') {
                    onSelectionChange();
                }
            },
        },
        pathsSortedByLayers(paths).map(([key, paths]) =>
            renderFeatureInfoGroup(key, paths, paths.length > 1)
        )
    );

const renderLegendBox = () =>
    DIV(
        'legend-with-feature',
        DETAILS('', SUMMARY('', tr.embed('legendAndInfos')), renderLegend())
    );

const render = () =>
    DIV(
        'sidebar__wrapper sidebar-right feature-info-page',
        withProfileFancy(renderLegendBox),
        getSelectedFeatures().map(renderFeatureInfoMulti)
    );
// );
export default render;
