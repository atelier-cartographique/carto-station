import * as debug from 'debug';
import { fromNullable, some, none } from 'fp-ts/lib/Option';
import { CANVAS } from 'sdi/components/elements';
import { iife } from 'sdi/lib';
import { getSelectedFeatures } from '../queries/app';
import { getView } from '../queries/map';

const logger = debug('sdi:fancy');

export const { setPosition, getPosition, clearPosition, setListRect } = iife(
    () => {
        const positions: {
            lid: string;
            fid: number | string;
            element: HTMLElement;
        }[] = [];

        const refreshRate = 64;
        let listElement: Element | null = null;
        let scheduledRefresh: { tid: number; ts: number } | null = null;

        const inRect = (y: number) =>
            fromNullable(listElement)
                .map(elem => {
                    const { top, bottom } = elem.getBoundingClientRect();
                    logger('inRect', y, bottom, top, y <= bottom && y >= top);
                    return y <= bottom && y >= top;
                })
                .getOrElse(false);

        const setListRect = (elem: Element | null) => (listElement = elem);

        const update = () => {
            const now = Date.now();
            if (scheduledRefresh === null) {
                const tid = window.setTimeout(onSelectionChange, refreshRate);
                scheduledRefresh = { tid, ts: now };
            } else if (now - scheduledRefresh.ts > refreshRate / 2) {
                clearTimeout(scheduledRefresh.tid);
                const tid = window.setTimeout(onSelectionChange, refreshRate);
                scheduledRefresh = { tid, ts: now };
            }
        };

        const setPosition = (
            lid: string,
            fid: number | string,
            element: HTMLElement
        ) => {
            const pos = positions.findIndex(
                o => o.lid === lid && o.fid === fid
            );
            if (pos >= 0) {
                positions[pos].element = element;
            } else {
                positions.push({ lid, fid, element });
            }
            update();
        };

        const getPosition = (lid: string, fid: number | string) =>
            fromNullable(
                positions.find(o => o.lid === lid && o.fid === fid)
            ).chain(({ element }) => {
                const { x, y } = element.getBoundingClientRect();
                if (inRect(y)) {
                    return some({ x, y });
                }
                return none;
            });

        const clearPosition = (lid: string, fid: number | string) => {
            const pos = positions.findIndex(
                o => o.lid === lid && o.fid === fid
            );
            if (pos >= 0) {
                positions.splice(pos, 1);
            }
            update();
        };

        return { setPosition, getPosition, clearPosition, setListRect };
    }
);

let root: HTMLCanvasElement | null = null;

const renderFancy = (element: HTMLCanvasElement | null) => {
    if (element) {
        logger('Im here');
    } else {
        logger('Im out');
    }
    root = element;
};

// type point_ = [number, number];
// const muln = (n: number, [x, y]: point_): point_ => [n * x, n * y];
// const mul = ([x1, y1]: point_, [x2, y2]: point_): point_ => [x1 * x2, y1 * y2];
// const div = ([x1, y1]: point_, [x2, y2]: point_): point_ => [x1 / x2, y1 / y2];

export const onSelectionChange = () => {
    if (root === null) {
        return;
    }
    const { extent } = getView();
    if (extent === null) {
        return;
    }
    const parent = root.parentElement!;
    const { width, height, top, left } = parent.getBoundingClientRect();
    root.width = width;
    root.height = height;
    const [minx, miny, maxx, maxy] = extent;
    const sx = width / (maxx - minx);
    const sy = height / (maxy - miny);
    // const base = new DOMMatrix();
    // const vertInverter = base.scale(1, -1);
    // const matrix = base.scale(sx, sy).translate(-minx, -miny);
    // const { a, b, c, d, e, f } = vertInverter.multiply(matrix);
    const transform = (x: number, y: number) => [
        (x - minx) * sx,
        height - (y - miny) * sy,
    ];
    // ctx.translate(-minx, miny - height / sy);
    // ctx.scale(sx, -sy);
    // ctx.scale(1, -1);
    fromNullable(root.getContext('2d')).map(ctx => {
        ctx.lineWidth = 1;
        ctx.strokeStyle = 'black';
        ctx.fillStyle = 'white';
        ctx.clearRect(0, 0, width, height);
        ctx.resetTransform();
        getSelectedFeatures().map(instances =>
            instances.map(({ layer, feature }) => {
                const geom = feature.geometry;
                if (geom.type === 'Point' || geom.type === 'MultiPoint') {
                    const drawPosition = () =>
                        getPosition(layer.id, feature.id).map(ref => {
                            const rx = ref.x - left;
                            const ry = ref.y - top;

                            const coords =
                                geom.type === 'Point'
                                    ? [geom.coordinates]
                                    : geom.coordinates;
                            coords.map(coord => {
                                const [x, y] = transform(coord[0], coord[1]);
                                ctx.beginPath();
                                ctx.moveTo(rx, ry);
                                ctx.lineTo(x, y);
                                ctx.stroke();
                                ctx.save();
                                ctx.lineWidth = 3;
                                ctx.beginPath();
                                ctx.arc(x, y, 12, 0, Math.PI * 2);
                                ctx.stroke();
                                ctx.fill();
                                ctx.beginPath();
                                ctx.fillStyle = 'black';
                                ctx.arc(rx, ry, 5, 0, Math.PI * 2);
                                // ctx.stroke();
                                ctx.fill();
                                ctx.restore();
                            });
                        });
                    // .getOrElseL(() => setTimeout(drawPosition, 32));

                    drawPosition();
                }
            })
        );
        ctx.beginPath();
    });
};

export const render = () =>
    CANVAS({
        className: 'fancy',
        ref: renderFancy,
        style: {
            userSelect: 'none',
            pointerEvents: 'none',
        },
    });

export default render;

logger('loaded');
