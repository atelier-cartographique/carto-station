import * as debug from 'debug';
import host from './host';

import { DIV, NodeOrOptional } from 'sdi/components/elements';
import { loop, getApiUrl, getLang } from 'sdi/app';
import tr from 'sdi/locale';
import { visitAction, langAction } from 'sdi/activity';
// import { maintenance } from 'sdi/components/maintenance';

import {
    initMap,
    loadAlias,
    activityLogger,
    loadAllBaseLayers,
} from './events/app';

import map from './components/map';
import legend from './components/legend';
import feature from './components/feature-view';
import { getEmbedProfile, getLayout, withMobile } from './queries/app';
import { loadAllServices } from 'sdi/geocoder/events';

const logger = debug('sdi:app');

const logoBe = () => DIV('brand-logo', DIV('brand-name'));

const wrappedMain = (...elements: NodeOrOptional[]) =>
    DIV(
        `app-inner embed profile-${getEmbedProfile()} ${withMobile()
            .map(() => 'mobile')
            .getOrElse('desktop')}`,
        DIV('main', ...elements),
        logoBe()
    );

const render = () => {
    const layout = getLayout();
    switch (layout) {
        case 'map':
            return wrappedMain(map(), legend());
        case 'map-and-feature':
            return wrappedMain(map(), feature());
    }
};

const effects = () => {
    loadAllServices();
    loadAllBaseLayers(getApiUrl(`wmsconfig/`));
    loadAlias(getApiUrl('alias'));
    initMap().finally(host);
    tr.init_edited();
    activityLogger(visitAction());
    activityLogger(langAction(getLang()));
};

const app = loop('embed-app', render, effects);
export default app;

logger('loaded');
