import * as debug from 'debug';

import 'sdi/polyfill';
import { source, AppConfigIO, getMessage, EmbedProfileIO } from 'sdi/source';
import { IShape, configure, defaultShape } from 'sdi/shape';
import { defaultInteraction } from 'sdi/map';

import './shape';
import './locale';

import App from './app';
import { displayException } from 'sdi/app';

const logger = debug('sdi:index');

const getProfileFromParams = () => {
    const loc = document.location;
    const params = new URL(loc.href).searchParams;
    return EmbedProfileIO.decode(params.get('profile')).getOrElse('simple');
};

export const main = (SDI: unknown) => {
    AppConfigIO.decode(SDI).fold(
        errors => {
            const textErrors = errors.map(e => getMessage(e.value, e.context));
            displayException(textErrors.join('\n'));
        },
        config => {
            const initialState: IShape = {
                'app/codename': 'embed',
                ...defaultShape(config),

                'app/layout': 'map',
                'app/current-layer': null,
                'app/selected-features': [],
                'app/current-feature': null,
                'app/profile': getProfileFromParams(),

                'data/alias': [],
                'data/map': null,
                'data/metadata': [],
                'data/layer': [],
                'data/layerinfo-list': [],

                'port/map/scale': {
                    count: 0,
                    unit: '',
                    width: 0,
                },

                'port/map/view': {
                    dirty: 'geo',
                    srs: 'EPSG:31370',
                    center: [149546.27830713114, 169775.91753364357],
                    rotation: 0,
                    zoom: 6,
                    feature: null,
                    extent: null,
                },

                'port/map/interaction': defaultInteraction(),

                'component/timeserie': {},
                'data/timeseries': {},
                'data/baselayers': [],
            };

            try {
                const start = source<IShape>(['app/lang']);
                const store = start(initialState);
                configure(store);
                const app = App(store);
                logger('start rendering');
                app();
            } catch (err) {
                displayException(`${err}`);
            }
        }
    );
};

logger('loaded');
