/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import {assign, dispatchK, observe} from 'sdi/shape';
import { viewEventsFactory, scaleEventsFactory } from 'sdi/map';
import {Feature, ILayerInfo} from 'sdi/source';
import { onSelectionChange } from 'embed/src/components/fancy';

export const scalelineEvents = scaleEventsFactory(dispatchK('port/map/scale'));
export const viewEvents = viewEventsFactory(dispatchK('port/map/view'));

export const setZoom = (zoom: number) =>
    viewEvents.updateMapView({
        dirty: 'geo',
        zoom,
    });

export const dispatchView = dispatchK('port/map/view');
export const setInteraction = dispatchK('port/map/interaction');
export const assignLayerInfoList = (layers: ILayerInfo[]) => assign('data/layerinfo-list', layers)

export const zoomToFeatures = (features: Feature[]) =>
    viewEvents.updateMapView({
        dirty: 'geo/feature',
        features,
    });

// observe('app/selected-features', onSelectionChange);
observe('port/map/view', onSelectionChange);
