/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { fromNullable, some } from 'fp-ts/lib/Option';
import { right } from 'fp-ts/lib/Either';

import { assign, assignK, dispatch, observe } from 'sdi/shape';
import { getApiUrl, getRootUrl, getUserId } from 'sdi/app';
import { IMapInfo, getMessageRecord, Inspire, ILayerInfo } from 'sdi/source';
import {
    removeLayerAll,
    addLayer,
    addFeaturesToLayer,
    FeaturePath,
} from 'sdi/map';
import { activity } from 'sdi/activity';

import {
    fetchMap,
    fetchDatasetMetadata,
    fetchLayer,
    fetchAlias,
    fetchLayers,
    fetchBaseLayerAll,
} from '../remote';
import { AppLayout } from '../shape';
import { mapName } from '../components/map';
import { getView } from '../queries/map';
import {
    getMapInfo,
    getLayer,
    getDatasetMetadata,
    getLayerInfoList,
} from '../queries/app';
import { cleanRoute, setMapView } from './route';

export const activityLogger = activity('embed');

export const setLayout = (l: AppLayout) => dispatch('app/layout', () => l);

// export const setCurrentFeatureById = (lid: string, fid: string | number) => {
//     dispatch('app/layerId', () => lid);
//     dispatch('app/featureId', () => fid);
// };

// export const unsetCurrentFeature = () => {
//     dispatch('app/layerId', () => null);
//     dispatch('app/featureId', () => null);
// };

export const selectFeatures = (ps: FeaturePath[]) => {
    assign('app/selected-features', ps);
    if (ps.length > 0) {
        assign('app/layout', 'map-and-feature');
    } else {
        assign('app/layout', 'map');
    }
};

export const deselectFeature = (
    layerId: string,
    featureId: string | number
) => {
    dispatch('app/selected-features', paths =>
        paths.filter(p => p.layerId !== layerId || p.featureId !== featureId)
    );
};

export const loadMapLayerInfo = (mapId: string) => {
    const url = `${getApiUrl(`layerinfos?mid=`)}${mapId}`;
    return fetchLayers(url).then(newLayers => {
        dispatch('data/layerinfo-list', layerList => {
            return layerList
                .filter(
                    layer => newLayers.findIndex(nl => nl.id == layer.id) < 0
                )
                .concat(newLayers);
        });
        return getLayerInfoList();
    });
};

const loadLayer = (
    info: ILayerInfo,
    metadata: Inspire,
    bbox: number[] | null
) => {
    const bboxParams = bbox
        ? `?bbox=${bbox[0]},${bbox[1]},${bbox[2]},${bbox[3]}`
        : '';

    fetchLayer(metadata.uniqueResourceIdentifier + bboxParams).then(layer => {
        dispatch('data/layer', state => {
            const layerIndex = state.findIndex(x => x[0] === info.id);

            if (layerIndex >= 0) {
                state[layerIndex] = [info.id, layer];
            } else {
                state.push([info.id, layer]);
            }

            return state;
        });

        getLayer(info.id).fold(
            addLayer(
                mapName,
                () =>
                    some({
                        name: getMessageRecord(metadata.resourceTitle),
                        info,
                        metadata,
                    }),
                () => right(some(layer))
            ),
            () => {
                if (bbox) {
                    return addFeaturesToLayer(mapName, info, layer.features);
                }
                return null;
            }
        );
    });
};

const withMetadata = (metadataId: string, then: (md: Inspire) => void) =>
    getDatasetMetadata(metadataId)
        .map(then)
        .getOrElseL(() => {
            fetchDatasetMetadata(getApiUrl(`metadatas/${metadataId}`)).then(
                md => {
                    addMetadata(md);
                    then(md);
                }
            );
        });

observe('port/map/view', () => {
    getMapInfo().fold(null, () => {
        getLayerInfoList().forEach(layerInfo => {
            if (layerInfo.visible) {
                withMetadata(layerInfo.metadataId, md => {
                    if (md.dataStreamUrl) {
                        loadLayerDataAccordingToZoom(layerInfo, md);
                    }
                });
            }
        });
    });
});

// export const loadBaseLayer = (id: string, url: string) => {
//     fetchBaseLayer(url).then(bl => {
//         dispatch('data/baselayers', state => ({ ...state, [id]: bl }));
//     });
// };

export const loadAllBaseLayers = (url: string) => {
    fetchBaseLayerAll(url).then(assignK('data/baselayers'));
};

export const addMetadata = (md: Inspire) =>
    dispatch('data/metadata', mds =>
        mds.filter(m => m.id !== md.id).concat([md])
    );

const loadMetadata = (layers: ILayerInfo[]) => {
    removeLayerAll(mapName);
    layers.forEach(layerInfo => {
        if (layerInfo.visible) {
            withMetadata(layerInfo.metadataId, md => {
                loadLayerDataAccordingToZoom(layerInfo, md);
            });
        }
    });
};

const isZoomVisible = (
    zoom: number,
    minZoom: number,
    maxZoom: number
): boolean => {
    return zoom > minZoom && zoom < maxZoom;
};
const loadLayerDataAccordingToZoom = (layer: ILayerInfo, metadata: Inspire) => {
    const zoom = getView().zoom;
    const minZoom = fromNullable(layer.minZoom).getOrElse(0);
    const maxZoom = fromNullable(layer.maxZoom).getOrElse(30);

    if (isZoomVisible(zoom, minZoom, maxZoom)) {
        let extent = null;
        if (metadata.dataStreamUrl) {
            extent = getView().extent;
        }

        loadLayer(layer, metadata, extent);
    }
};

export const loadAlias = (url: string) =>
    fetchAlias(url).then(alias => {
        dispatch('data/alias', () => alias);
    });

export const loadMapInfo = (info: IMapInfo) => {
    dispatch('data/map', () => info);
    loadMetadata(getLayerInfoList());
};

const loadMap = (mid: string) =>
    fetchMap(getApiUrl(`maps/${mid}`))
        .then(info => {
            dispatch('data/map', () => info);
            loadMapLayerInfo(info.id).then(layers => loadMetadata(layers));
        })
        .catch(response => {
            const user = getUserId();

            if (response.status === 403 && user.isNone()) {
                window.location.assign(getRootUrl(`login/embed/${mid}`));
                return;
            }
        });

export const initMap = () => {
    const r = cleanRoute();
    if (r.length > 0) {
        const mapId = r[0];
        loadMap(mapId);
        setMapView();
        return loadMap(r[0]);
    }
    return Promise.resolve();
};
