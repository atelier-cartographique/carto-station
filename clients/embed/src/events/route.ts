/*
 *  Copyright (C) 2023 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { queryK } from 'sdi/shape';
import { scopeOption } from 'sdi/lib';
import { viewEvents } from './map';
import { tryNumber } from 'sdi/util';

// Very simple route mod

const getRoute = queryK('app/route');

export const cleanRoute = () =>
    getRoute().reduce((acc, s) => {
        if (s.length > 0) {
            return acc.concat([s]);
        }
        return acc;
    }, [] as string[]);

export const setMapView = () => {
    const r = cleanRoute();
    scopeOption()
        .let('lat', tryNumber(r[1]))
        .let('lon', tryNumber(r[2]))
        .let('zoom', tryNumber(r[3]))
        .map(({ lat, lon, zoom }) => {
            viewEvents.updateMapView({
                dirty: 'geo',
                center: [lat, lon],
                zoom,
            });
        });
};
