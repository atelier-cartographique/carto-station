import { MessageStore, formatMessage } from 'sdi/locale';

const messages = {
    mapLegend: {
        fr: 'Légende',
        nl: 'Legende',
        en: 'Legend',
    },

    atlasLinkLabel: {
        fr: 'Carte en plein écran',
        nl: 'Kaart op volledig scherm', // nltocheck
        en: 'Full screen map',
    },

    zoomOnFeature: {
        fr: 'Zoomer sur l’entité ',
        nl: 'Zoom in op de entiteit ', // nldone
        en: 'Zoom to item ',
    },
    zoomOnFeatureGroup: {
        fr: 'Zoomer sur les entités ',
        nl: 'Zoom in op de entiteiten ', // nldone
        en: 'Zoom to items ',
    },
    activateLayerAndZoomOnFeature: {
        fr: 'Activer la couche & Zoomer sur l’entité ',
        nl: 'Activeer de laag & Zoom in op de entiteit ', // nldone
        en: 'Zoom to item ',
    },

    element: {
        fr: 'éléments',
        nl: 'elementen',
        en: 'elements',
    },

    selectedTitle: {
        en: 'Selection',
        fr: 'Sélection',
        nl: 'Selectie',
    },

    selectedAmount: {
        en: ' features',
        fr: ' entités',
        nl: ' kenmerken',
    },
    multiSelectionHelptext: {
        en: 'Use `SHIFT + CLICK` to select multiple entities.',
        fr: 'Utilisez `SHIFT + CLIC` pour sélectionner plusieurs entités.',
        nl: 'Gebruik `SHIFT + CLICK` om meerdere entiteiten te selecteren.',
    },
    legendAndInfos: {
        en: 'Legend and informations',
        fr: 'Légende et informations',
        nl: 'Legende en informatie',
    },
};

type MDB = typeof messages;
export type EmbedMessageKey = keyof MDB;

declare module 'sdi/locale' {
    export interface MessageStore {
        embed(k: EmbedMessageKey): Translated;
    }
}

MessageStore.prototype.embed = function (k: EmbedMessageKey) {
    return this.getEdited('embed', k, () => formatMessage(messages[k]));
};
