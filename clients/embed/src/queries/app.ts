import bbox from '@turf/bbox';
import { catOptions } from 'fp-ts/lib/Array';
import { fromNullable, none, Option, some } from 'fp-ts/lib/Option';
import { scopeOption } from 'sdi/lib';
import { FeaturePath, FeaturePathInstance } from 'sdi/map';

import { ensureArray } from 'sdi/util';
import { ILayerInfo, IMapInfo } from 'sdi/source';
import { query, queryK } from 'sdi/shape';
import { DirectGeometryObject, EmbedProfile } from 'sdi/source';
import { Collection, notEmpty, updateCollection } from 'sdi/util';

export const getLayout = () => query('app/layout');

const mobileMediaQuery =
    'screen and (max-width: 900px) and (orientation: portrait)';

export const withMobile = () =>
    window.matchMedia(mobileMediaQuery).matches ? some(true) : none;

export const withDesktop = () =>
    window.matchMedia(mobileMediaQuery).matches ? none : some(true);

export const getMapInfo = () => fromNullable(query('data/map'));

export const getCurrentBaseLayerNames = () =>
    getMapInfo()
        .map(info => ensureArray(info.baseLayer))
        .getOrElse([]);

const baseLayerParts = (
    codename: string
): Option<{ service: string; layer: string }> => {
    const parts = codename.split('/');
    if (parts.length === 2) {
        return some({ service: parts[0], layer: parts[1] });
    }
    return none;
};

export const findBaseLayer = (codename: string) =>
    baseLayerParts(codename).chain(({ layer, service }) =>
        fromNullable(
            query('data/baselayers').find(({ id }) => id === service)
        ).chain(service =>
            fromNullable(
                service.layers.find(({ codename }) => codename === layer)
            )
        )
    );
export const getBaseLayers = (codenames: string[]) =>
    catOptions(codenames.map(findBaseLayer));

export const getCurrentBaseLayers = () =>
    getBaseLayers(getCurrentBaseLayerNames());

// export const getBaseLayer = () => {
//     const name = getCurrentBaseLayerName();
//     const [serviceId, layerName] = fromNullable(name)
//         .map(i => i.split('/'))
//         .getOrElse(['', '']);
//     const service = query('data/baselayers').find(s => s.id === serviceId);
//     if (service) {
//         return service.layers.find(l => l.codename === layerName) ?? null;
//     }
//     return null;
// };

export const getCurrentLayer = queryK('app/current-layer');

// export const getFeatureId = () => query('app/featureId');

export const getLayerInfoList = () => query('data/layerinfo-list').concat();
export const getLayerInfo = (id: string) =>
    fromNullable(getLayerInfoList().find(li => li.id === id));

export const getMapLayers = (umap: IMapInfo): ILayerInfo[] =>
    getLayerInfoList().filter(l => umap.layers.indexOf(l.id) >= 0);

export const getCurrentLayerInfo = () =>
    fromNullable(getCurrentLayer()).chain(lid =>
        fromNullable(getLayerInfoList().find(l => l.id === lid))
    );

// export const getCurrentFeature = () =>
//     scopeOption()
//         .let('lid', fromNullable(getLayerId()))
//         .let('fid', fromNullable(getFeatureId()))
//         .let('layer', ({ lid }) =>
//             fromNullable(query('data/layer').find(t => t[0] === lid))
//         )
//         .let('feature', ({ layer, fid }) =>
//             fromNullable(layer[1].features.find(f => f.id === fid))
//         )
//         .pick('feature');

export const getLayerCount = () =>
    getMapInfo()
        .map(info => info.layers.length)
        .getOrElse(0);

export const moreThanOneLayer = <T>(val: T) =>
    getLayerCount() > 1 ? val : none;

export const pathInstance = ({
    layerId,
    featureId,
}: FeaturePath): Option<FeaturePathInstance> =>
    scopeOption()
        .let(
            'layer',
            fromNullable(layerId).chain(l => getLayerInfo(l))
        )
        .let('data', ({ layer }) => getLayer(layer.id))
        .let('feature', s =>
            fromNullable(s.data[1].features.find(f => f.id === featureId))
        );

export const getCurrentSelection = queryK('app/selected-features');

const getNorth = (geom: DirectGeometryObject) => bbox(geom)[3];

const sortFeaturePathInstance = (
    a: FeaturePathInstance,
    b: FeaturePathInstance
) => {
    const ay = getNorth(a.feature.geometry);
    const by = getNorth(b.feature.geometry);
    return by - ay;
};

export const getSelectedFeatures = () =>
    notEmpty(
        catOptions(getCurrentSelection().map(pathInstance)).sort(
            sortFeaturePathInstance
        )
    );

export const getDatasetMetadata = (id: string) =>
    fromNullable(query('data/metadata').find(md => md.id === id));

export const getLayer = (id: string) =>
    fromNullable(query('data/layer').find(t => t[0] === id));

export const getEmbedProfile = queryK('app/profile');

const groupPathsByLayer = (paths: FeaturePathInstance[]) =>
    paths.reduce<Collection<FeaturePathInstance[]>>((acc, path) => {
        if (!(path.layer.id in acc)) {
            return updateCollection(acc, path.layer.id, [path]);
        } else {
            return updateCollection(
                acc,
                path.layer.id,
                acc[path.layer.id].concat(path)
            );
        }
    }, {});

export const pathsSortedByLayers = (paths: FeaturePathInstance[]) => {
    const pathsByLayer = Object.entries(groupPathsByLayer(paths));
    return getMapInfo()
        .map(info =>
            info.layers
                .reduce(
                    (acc: [string, FeaturePathInstance[]][], layer) =>
                        fromNullable(
                            pathsByLayer.find(([key, _]) => key === layer)
                        )
                            .map(paths => acc.concat([paths]))
                            .getOrElse(acc),
                    []
                )
                .reverse()
        )
        .getOrElse([]);
};

const withProfile =
    (prof: EmbedProfile) =>
    <R>(f: () => R): Option<R> =>
        getEmbedProfile() === prof ? some(f()) : none;

export const withProfileFancy = withProfile('fancy');
export const withProfileSimple = withProfile('simple');
export const withProfileAllTools = withProfile('all-tools');

const withoutProfile =
    (prof: EmbedProfile) =>
    <R>(f: () => R): Option<R> =>
        getEmbedProfile() !== prof ? some(f()) : none;

export const withoutProfileFancy = withoutProfile('fancy');
export const withoutProfileSimple = withoutProfile('simple');
export const withoutProfileAllTools = withoutProfile('all-tools');
