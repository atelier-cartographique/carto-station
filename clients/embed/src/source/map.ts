/*
 *  Copyright (C) 2024 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as io from 'io-ts';
import {MessageRecordIO} from "sdi/source/io/message";
import {ExtentIO, ILayerInfoIO, IMapInfoIO, InspireIO, MapStatusIO, uuidIO} from "sdi/source";

// legacy Map Info to keep compatibility with embedded maps
export const ILegacyMapInfoIO = io.intersection(
    [
        io.interface({
            id: io.string,
            id_origin: io.string,
            title: MessageRecordIO,
            url: io.string,
            status: MapStatusIO,
            lastModified: io.number,
            description: MessageRecordIO,
            attachments: io.array(uuidIO),
            layers: io.array(ILayerInfoIO),  // layers are passed with metadata
            baseLayer: io.union([io.string, io.array(io.string)]), // "{service-id}/{layer-name}" //
            categories: io.array(io.string),
        }),
        io.partial({
            inPublicGroup: io.boolean,
            user: io.number,
            imageUrl: io.string,
            extent: ExtentIO,
        }),
    ],
    'ILegacyMapInfoIO'
);
export type ILegacyMapInfo = io.TypeOf<typeof ILegacyMapInfoIO>;

const MessageInitIO = io.interface(
    {
        tag: io.literal('init'),
    },
    'MessageInitIO'
);

const MessageMetadataIO = io.interface(
    {
        tag: io.literal('metadata'),
        data: InspireIO,
    },
    'MessageMetadataIO'
);

const MessageMapIO = io.interface(
    {
        tag: io.literal('map'),
        data: io.union([IMapInfoIO, ILegacyMapInfoIO], 'MessageMapIOData'),
    },
    'MessageMapIO'
);

export const MessageIO = io.union(
    [MessageInitIO, MessageMetadataIO, MessageMapIO],
    'MessageIO'
);

export type Message = io.TypeOf<typeof MessageIO>;
