// imports from sdi
import { FeaturePath } from 'sdi/map';
import { Feature, EmbedProfile, IServiceBaseLayers } from 'sdi/source';

// interface BaseLayerCollection {
//     [k: string]: IMapBaseLayer;
// }

export type AppLayout = 'map' | 'map-and-feature';

// State Augmentation
declare module 'sdi/shape' {
    export interface IShape {
        'app/layout': AppLayout;
        'app/current-layer': string | null;
        'app/current-feature': Feature | null;
        'app/selected-features': FeaturePath[];
        'app/route': string[];
        'app/profile': EmbedProfile;

        'data/baselayers': IServiceBaseLayers;
    }
}
