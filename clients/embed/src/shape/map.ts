// imports from sdi
import { IMapInfo, Inspire, FeatureCollection, ILayerInfo } from 'sdi/source';
import { IMapViewData, IMapScale, Interaction } from 'sdi/map';

export type IdentifiedFeatureCollection = [string, FeatureCollection];

// State Augmentation
declare module 'sdi/shape' {
    export interface IShape {
        'port/map/view': IMapViewData;
        'port/map/scale': IMapScale;
        'port/map/interaction': Interaction;

        'data/map': IMapInfo | null;
        'data/layerinfo-list': ILayerInfo[];
        'data/metadata': Inspire[];
        'data/layer': IdentifiedFeatureCollection[];
    }
}
