import {
    fetchIO,
    IUser,
    IUserIO,
    IMapInfo,
    IMapInfoIO,
    Inspire,
    InspireIO,
    FeatureCollection,
    ITimeserieIO,
    ITimeserie,
    IMapBaseLayer,
    IMapBaseLayerIO,
    fetchWithoutValidationIO,
    IAliasCollection,
    IAliasCollectionIO,
    ILayerInfoIO,
    ILayerInfo,
    IServiceBaseLayers,
    IServiceBaseLayersIO,
} from 'sdi/source';

import * as io from 'io-ts';

export const fetchUser = (url: string): Promise<IUser> => fetchIO(IUserIO, url);

export const fetchMap = (url: string): Promise<IMapInfo> =>
    fetchIO(IMapInfoIO, url);

export const fetchLayers = (url: string): Promise<ILayerInfo[]> =>
    fetchIO(io.array(ILayerInfoIO), url);

export const fetchBaseLayerAll = (url: string): Promise<IServiceBaseLayers> =>
    fetchIO(IServiceBaseLayersIO, url);

export const fetchBaseLayer = (url: string): Promise<IMapBaseLayer> =>
    fetchIO(IMapBaseLayerIO, url);

export const fetchDatasetMetadata = (url: string): Promise<Inspire> =>
    fetchIO(InspireIO, url);

export const fetchAlias = (url: string): Promise<IAliasCollection> =>
    fetchIO(IAliasCollectionIO, url);

export const fetchLayer = (url: string): Promise<FeatureCollection> =>
    fetchWithoutValidationIO(url);

export const fetchTimeserie = (url: string): Promise<ITimeserie> =>
    fetchIO(ITimeserieIO, url);
