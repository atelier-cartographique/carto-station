import * as debug from 'debug';
import {fromNullable, fromPredicate} from 'fp-ts/lib/Option';
import {IMapInfo, onValidationErrorString} from 'sdi/source';
import {addMetadata, loadMapInfo} from './events/app';
import {assignLayerInfoList} from "./events/map";
import {ILegacyMapInfo, Message, MessageIO} from './source/map'

const logger = debug('sdi:host');

const error = (body: string) => ({
    source: 'carto-station',
    status: 'error',
    body,
});

const ok = (kind: string, body: string) => ({
    source: 'carto-station',
    status: 'success',
    kind,
    body,
});

const init = () => ({
    source: 'carto-station',
    status: 'init',
});

const isLegacyMapInfo = (map: IMapInfo | ILegacyMapInfo): map is ILegacyMapInfo => {
    if (!map.layers) return false;
    return typeof map.layers[0] !== "string"
}

const processLegacyMapInfo = (legacyMapInfo: ILegacyMapInfo): IMapInfo => {
    assignLayerInfoList(legacyMapInfo.layers);
    return {...legacyMapInfo, layers: legacyMapInfo.layers.map(l => l.id)}
}

const handleMessage = (post: (a: unknown) => void) => (message: Message) => {
    logger(`handle ${message.tag}`);
    switch (message.tag) {
        case 'init': {
            post(init());
            break;
        }
        case 'metadata': {
            addMetadata(message.data);
            post(ok(message.tag, message.data.id));
            break;
        }
        case 'map': {
            if (isLegacyMapInfo(message.data)) {
                loadMapInfo(processLegacyMapInfo(message.data))
            } else {
                loadMapInfo(message.data);
            }
            post(ok(message.tag, message.data.id));
            break;
        }
    }
};

const isInFrame = fromPredicate<Window>(w => w !== window);

export const initHost = () =>
    isInFrame(window.parent).map(() => {
        logger('listening');
        window.addEventListener('message', event => {
            logger('<message>', event);
            const source = fromNullable(event.source);
            const origin = event.origin;
            const post = (a: unknown) =>
                source.map(w => {
                    logger('posting', a, w);
                    w.postMessage(a, {targetOrigin: origin});
                });
            MessageIO.decode(event.data).fold(
                errors =>
                    post(
                        error(
                            onValidationErrorString(
                                MessageIO,
                                event.origin
                            )(errors)
                        )
                    ),
                handleMessage(post)
            );
        });
    });

export default initHost;

logger('loaded');
