/*
 *  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import {
    FeatureCollection,
    IMapInfo,
    Inspire,
    IServiceBaseLayers,
    MessageRecord,
} from 'sdi/source';
import { IUgWsResponse } from 'sdi/ports/geocoder';
import {
    IMapViewData,
    Interaction,
    IMapScale,
    defaultInteraction,
    ViewDirt,
} from 'sdi/map';
import { Collection } from 'sdi/util';

// State Augmentation

declare module 'sdi/shape' {
    export interface IShape {
        'component/geocoder/response': IUgWsResponse | null;
        'component/geocoder/input': string;

        'port/map/main/view': IMapViewData;
        'port/map/mini/view': IMapViewData;
        'port/map/interaction': Interaction;
        'port/map/loading': MessageRecord[];
        'port/map/scale': IMapScale;

        'data/maps': IMapInfo[];
        'data/layers': Collection<FeatureCollection>;
        'data/metadata': Collection<Inspire>;
        'data/baselayers': IServiceBaseLayers;
        'data/remote/errors': Collection<string>;
    }
}

export const defaultMapShape = () => ({
    'component/geocoder/response': null,
    'component/geocoder/input': '',

    'port/map/main/view': {
        dirty: 'geo' as ViewDirt,
        srs: 'EPSG:31370',
        center: [148885, 170690],
        rotation: 0,
        zoom: 8,
        feature: null,
        extent: null,
    },
    'port/map/mini/view': {
        dirty: 'geo' as ViewDirt,
        srs: 'EPSG:31370',
        center: [148885, 170690],
        rotation: 0,
        zoom: 10,
        feature: null,
        extent: null,
    },

    'port/map/interaction': defaultInteraction(),
    'port/map/loading': [],

    'port/map/scale': {
        count: 0,
        unit: '',
        width: 0,
    },

    'data/maps': [],
    'data/layers': {},
    'data/metadata': {},
    'data/baselayers': [],
    'data/remote/errors': {},
});
