/*
 *  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { fromNullable, Option } from 'fp-ts/lib/Option';
import {
    getCatNb,
    inPPAS,
    inRCU,
    inPAD,
    inNatura2000,
    nearPhreatic,
    inFloodAlea,
    getPermitSelection,
    hasEnvPermits,
    hasUrbaPermits,
    getSurfaceWaterDist,
    hasFundingLocalityOpt,
    nearWaterCapture,
    isHeritage,
} from 'infiltration/src/queries/infiltration';
import { GroundCategories, Natura2000Proximity } from 'infiltration/src/remote';
import { createElement } from 'react';
import { rect } from 'sdi/app';
import { DIV, H4, SPAN } from 'sdi/components/elements';
import { iife } from 'sdi/lib';
import tr from 'sdi/locale';
import { withMeter } from 'sdi/util';
import { getBrugeotoolLink } from '../button';
import renderPicto from '../pictos';

const logger = debug('sdi:infiltration');

const getCatDescription = (cat: Option<GroundCategories>) =>
    cat.fold(tr.infiltrMD('infNoCategory'), c => {
        switch (c) {
            case 'blanco':
                return tr.infiltrMD('infCatBlanco');
            case '0':
                return tr.infiltrMD('infCat0');
            case '1':
                return tr.infiltrMD('infCat1');
            case '2':
                return tr.infiltrMD('infCat2');
            case '3':
                return tr.infiltrMD('infCat3');
            case '4':
                return tr.infiltrMD('infCat4');
            case '0+1':
                return tr.infiltrMD('infCat01');
            case '0+2':
                return tr.infiltrMD('infCat02');
            case '0+3':
                return tr.infiltrMD('infCat03');
            case '0+4':
                return tr.infiltrMD('infCat04');
        }
    });

const waterSurfaceInWater = () => tr.infiltrMD('infInWaterSurface');

const waterSurfaceInBuffer = (distance: number) =>
    DIV(
        'water-distance',
        `${tr.infiltr('distance')}: ${withMeter(distance, 2)}`,
        tr.infiltrMD('infBufferWaterSurface')
    );

const waterSurfaceBeyondBuffer = () =>
    tr.infiltrMD('infBeyondBufferWaterSurface');

const getWaterSurfaceDescription = (distance: number | null) =>
    fromNullable(distance)
        .map(distance =>
            distance > 0
                ? waterSurfaceInBuffer(distance)
                : waterSurfaceInWater()
        )
        .getOrElseL(waterSurfaceBeyondBuffer);

const getNatura2000Description = (status: Natura2000Proximity) => {
    switch (status) {
        case 'inBuffer':
            return tr.infiltrMD('infBufferNatura2000');
        case 'outBuffer':
            return tr.infiltrMD('infOutNatura2000');
        case 'inNatura':
            return tr.infiltrMD('infInNatura2000');
    }
};

const stretchingArrow = () =>
    DIV(
        'stretching-arrow__wrapper',
        renderPicto('pic-arrow-a-body', 'none'),
        renderPicto('pic-arrow-a-tip')
    );

const category = () =>
    DIV(
        'list-elem legal cat',
        renderPicto('pic-08-cat'),
        stretchingArrow(),
        DIV('description', getCatDescription(getCatNb()))
    );

const rru = () =>
    DIV(
        'list-elem legal rru rru-indented true',
        DIV(
            'picto__wrapper',
            renderPicto('pic-03-rru'),
            renderPicto('pic-triangle')
        ),
        stretchingArrow(),
        DIV('description', tr.infiltrMD('rruPitch'))
    );

const ppas = () =>
    inPPAS().map(inPPAS =>
        DIV(
            `list-elem legal ppas ppas-indented ${inPPAS}`,
            DIV(
                'picto__wrapper',
                renderPicto('pic-05-ppas'),
                inPPAS
                    ? renderPicto('pic-triangle')
                    : renderPicto('pic-circle-yes')
            ),
            stretchingArrow(),
            DIV(
                'description',
                inPPAS ? tr.infiltrMD('infPPASyes') : tr.infiltrMD('infPPASno')
            )
        )
    );

const rcu = () =>
    inRCU().map(inRCU =>
        DIV(
            `list-elem legal rcu rcu-indented ${inRCU}`,
            DIV(
                'picto__wrapper',
                renderPicto('pic-06-rcu'),
                inRCU
                    ? renderPicto('pic-triangle')
                    : renderPicto('pic-circle-yes')
            ),
            stretchingArrow(),
            DIV(
                'description',
                inRCU ? tr.infiltrMD('infRCUyes') : tr.infiltrMD('infRCUno')
            )
        )
    );

const pad = () =>
    inPAD().map(inPAD =>
        DIV(
            `list-elem legal pad pad-indented ${inPAD}`,
            DIV(
                'picto__wrapper',
                renderPicto('pic-04-pad'),
                inPAD
                    ? renderPicto('pic-triangle')
                    : renderPicto('pic-circle-yes')
            ),
            stretchingArrow(),
            DIV(
                'description',
                inPAD ? tr.infiltrMD('infPADyes') : tr.infiltrMD('infPADno')
            )
        )
    );

const patrimoine = () =>
    isHeritage().map(h =>
        DIV(
            `list-elem legal heritage heritage-indented ${h}`,
            DIV(
                'picto__wrapper',
                renderPicto('pic-07-dpc'),
                h ? renderPicto('pic-triangle') : renderPicto('pic-circle-yes')
            ),
            stretchingArrow(),
            DIV(
                'description',
                h
                    ? tr.infiltrMD('infHeritagePitch')
                    : tr.infiltrMD('infHeritageNoPitch')
            )
        )
    );

const indentedTopUrba = () =>
    DIV(
        'indented-top',
        DIV(
            '',
            DIV(
                'description__wrapper',
                renderPicto('pic-arrow-c-big-body', 'none'),
                DIV('indented-top-text', tr.infiltrMD('legalIntroDetailsUrba'))
            ),
            renderPicto('pic-arrow-c-to-b-right')
        )
    );

const circle = (key: number, cx: number, cy: number, r: number, fill: string) =>
    createElement('circle', { key, cx, cy, r, style: { fill } });

const border = (color: string) =>
    createElement(
        'svg',
        {
            viewBox: `0 0 10 400`,
        },
        new Array(200).fill(0).map((_, i) => circle(i, 5, i * 2, 0.5, color))
    );

const indentedTopEnvUrba = () => {
    const { rx, tx } = iife(() => {
        let f: (height: number) => void = () => void 0;
        const rx = (g: (height: number) => void) => (f = g);
        const tx = (height: number) => f(height);
        return { rx, tx };
    });
    return DIV(
        'indented-top',
        DIV(
            {
                key: 'side__wrapper_urba_env_top',
                className: 'side__wrapper env-urba',
                ref: node => {
                    if (node) {
                        rx(h => {
                            node.style.height = `calc(${h}px - 3rem)`;
                        });
                    }
                },
            },
            DIV('side-top', border('#289084')),
            renderPicto('pic-01-permit-env'),
            DIV('side-bottom', border('#289084'))
        ),
        DIV(
            '',
            DIV(
                'description__wrapper',
                renderPicto('pic-00-env'),
                renderPicto('pic-arrow-c-big-body', 'none'),
                DIV(
                    {
                        className: 'indented-top-text',
                        ref: rect(t => tx(t.height)),
                    },
                    tr.infiltrMD('legalIntroDetailsEnvUrba')
                )
            ),
            renderPicto('pic-arrow-c-to-b-right')
        )
    );
};

const envLegalList = () => {
    const { rx, tx } = iife(() => {
        let f: (height: number) => void = () => void 0;
        const rx = (g: (height: number) => void) => (f = g);
        const tx = (height: number) => f(height);
        return { rx, tx };
    });
    return DIV(
        'legal-list env',
        DIV(
            {
                key: 'side__wrapper_env',
                className: 'side__wrapper env',
                ref: node => {
                    if (node) {
                        rx(h => {
                            node.style.height = `calc(${h}px - 3rem)`;
                        });
                    }
                },
            },
            DIV('side-top', border('#289084')),
            renderPicto('pic-01-permit-env'),
            DIV('side-bottom', border('#289084'))
        ),
        DIV(
            '',
            DIV(
                'list-elem legal env',
                renderPicto('pic-00-env'),
                stretchingArrow(),
                DIV(
                    {
                        className: 'description',
                        ref: rect(t => tx(t.height)),
                    },
                    tr.infiltrMD('legalIntroDetailsEnv')
                )
            ),
            category()
        )
    );
};

const urbaLegalList = () => {
    const { rx, tx } = iife(() => {
        let f: (height: number) => void = () => void 0;
        const rx = (g: (height: number) => void) => (f = g);
        const tx = (height: number) => f(height);
        return { rx, tx };
    });
    return DIV(
        'legal-list urba',
        indentedTopUrba(),
        DIV(
            'indented-list',
            DIV(
                {
                    key: 'side__wrapper_urba',
                    className: 'side__wrapper urba',
                    ref: node => {
                        if (node) {
                            rx(h => {
                                node.style.height = `${h}px`;
                            });
                        }
                    },
                },
                DIV('side-top', border('#e44435')),
                renderPicto('pic-02-permit-urb'),
                DIV('side-bottom', border('#e44435'))
            ),
            DIV(
                {
                    className: 'urba__wrapper',
                },
                DIV(
                    {
                        className: 'inner',
                        ref: rect(t => tx(t.height)),
                    },
                    rru(),
                    rcu(),
                    pad(),
                    ppas(),
                    patrimoine()
                )
            )
        ),
        DIV('indented-list-footer', renderPicto('pic-arrow-b-to-b-left')),
        category()
    );
};

const envUrbaLegalList = () => {
    const { rx, tx } = iife(() => {
        let f: (height: number) => void = () => void 0;
        const rx = (g: (height: number) => void) => (f = g);
        const tx = (height: number) => f(height);
        return { rx, tx };
    });

    return DIV(
        'legal-list urba',
        indentedTopEnvUrba(),
        DIV(
            'indented-list',
            DIV(
                {
                    key: 'side__wrapper_urba_env',
                    className: 'side__wrapper urba',
                    ref: node => {
                        if (node) {
                            rx(h => {
                                node.style.height = `${h}px`;
                            });
                        }
                    },
                },
                DIV('side-top', border('#e44435')),
                renderPicto('pic-02-permit-urb'),
                DIV('side-bottom', border('#e44435'))
            ),
            DIV(
                'urba__wrapper',
                DIV(
                    {
                        className: 'inner',
                        ref: rect(t => tx(t.height)),
                    },
                    rru(),
                    rcu(),
                    pad(),
                    ppas(),
                    patrimoine()
                )
            )
        ),
        DIV('indented-list-footer', renderPicto('pic-arrow-b-to-b-left')),
        category()
    );
};

const envAndUrbaLegalList = () =>
    DIV('legal-list env-urba', envUrbaLegalList());

const defaultLegalList = () => DIV('legal-list', category());

const waterCapture = () =>
    nearWaterCapture().map(capt => {
        const text = capt
            ? tr.infiltrMD('infWaterCapture')
            : tr.infiltrMD('infWaterCaptureNo');
        return DIV(
            `list-elem proximity capture ${capt}`,
            renderPicto('pic-09-pump'),
            stretchingArrow(),
            DIV('description', text)
        );
    });

const surfaceWater = () =>
    getSurfaceWaterDist().map(sw =>
        DIV(
            `list-elem proximity surface  ${sw}`,
            renderPicto('pic-10-water'),
            stretchingArrow(),
            DIV('description', getWaterSurfaceDescription(sw))
        )
    );
const natura2000 = () =>
    inNatura2000().map(n =>
        DIV(
            'list-elem proximity natura2000',
            renderPicto('pic-11-natura-2000'),
            stretchingArrow(),
            DIV('description', getNatura2000Description(n))
        )
    );

const phreatic = () =>
    nearPhreatic().map(p =>
        DIV(
            `list-elem hydrogeo phreatic ${p}`,
            renderPicto('pic-12-depth'),
            stretchingArrow(),
            DIV(
                'description',
                p
                    ? tr.infiltrMD('infNearPhreatic')
                    : tr.infiltrMD('infNoPhreatic')
            )
        )
    );
const carrotLink = () =>
    DIV(
        'list-elem hydrogeo carrot true',
        renderPicto('pic-13-hydrogeo'),
        stretchingArrow(),
        DIV(
            'description',
            SPAN('info', tr.infiltr('infGeology1')),
            getBrugeotoolLink(tr.infiltr('infGeology2'), 'technical'),
            DIV('info', tr.infiltrMD('infGeology3'))
        )
    );

const floodAlea = () =>
    inFloodAlea().map(f =>
        DIV(
            `list-elem hydrogeo flood-alea ${f}`,
            renderPicto('pic-14-flooding'),
            stretchingArrow(),
            DIV(
                'description',
                f
                    ? tr.infiltrMD('infFloodAlea')
                    : tr.infiltrMD('infNoFloodAlea')
            )
        )
    );

const fundingRegion = () =>
    DIV(
        `list-elem funding region`,
        renderPicto('pic-17-prime-regional'),
        stretchingArrow(),
        DIV('description', tr.infiltrMD('fundingInfoRegion'))
    );

const fundingLocality = () =>
    hasFundingLocalityOpt().map(() =>
        DIV(
            `list-elem funding locality`,
            renderPicto('pic-18-prime-communal'),
            stretchingArrow(),
            DIV('description', tr.infiltrMD('fundingInfoLocality'))
        )
    );

const legalList = () => {
    switch (getPermitSelection(hasEnvPermits(), hasUrbaPermits())) {
        case 'env':
            return envLegalList();
        case 'urba':
            return urbaLegalList();
        case 'envAndUrba':
            return envAndUrbaLegalList();
        case 'none':
            return defaultLegalList();
    }
};

const proximityList = () =>
    DIV('proximity-list', waterCapture(), surfaceWater(), natura2000());

const hydrogeoList = () =>
    DIV('hydrogeo-list', phreatic(), carrotLink(), floodAlea());

const fundingList = () =>
    DIV('funding-list', fundingRegion(), fundingLocality());

const renderLegalMeasuresFull = () =>
    DIV(
        'legal',
        H4('infos-title detail-title', tr.infiltr('legalMeasures')),
        DIV('detail-info', legalList())
    );

const renderProximityToElementsFull = () =>
    DIV(
        'proximity',
        H4('infos-title detail-title', tr.infiltr('proximityToElements')),
        DIV('detail-info', proximityList())
    );

const renderHydrogeoAndFloodRiskFull = () =>
    DIV(
        'hydrogeo',
        H4({
            className: 'infos-title detail-title',
            dangerouslySetInnerHTML: {
                __html: tr.infiltr('hydrogeoAndFloodRisk'),
            },
        }),
        DIV('detail-info', hydrogeoList())
    );

const renderFunding = () =>
    DIV(
        'funding',
        H4('infos-title detail-title', tr.infiltr('fundingTitle')),
        DIV('detail-info', fundingList())
    );

const render = () =>
    DIV(
        'detail',
        renderLegalMeasuresFull(),
        renderProximityToElementsFull(),
        renderHydrogeoAndFloodRiskFull(),
        renderFunding()
    );

export default render;

logger('loaded');
