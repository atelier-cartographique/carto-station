/*
 *  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { fromNullable } from 'fp-ts/lib/Option';
import { navigateFull } from 'infiltration/src/events/route';
import {
    getCatNb,
    getCoord,
    getPermitSelection,
    getSurfaceWaterDist,
    hasEnvPermits,
    hasUrbaPermits,
    inFloodAlea,
    isHeritage,
    inNatura2000,
    inPAD,
    inPPAS,
    inRCU,
    nearPhreatic,
    nearWaterCapture,
    hasFundingLocalityOpt,
} from 'infiltration/src/queries/infiltration';
import { Natura2000Proximity } from 'infiltration/src/remote';
import { DIV, H4, SPAN } from 'sdi/components/elements';
import tr, { concat } from 'sdi/locale';
import { withMeter } from 'sdi/util';
import { getBrugeotoolURL, trackedLink } from '../button';
import renderPicto from '../pictos';

const logger = debug('sdi:infiltration');

const natura2000Text = (n: Natura2000Proximity) => {
    switch (n) {
        case 'inBuffer':
            return tr.infiltr('nearNatura2000');
        case 'inNatura':
            return tr.infiltr('inNatura2000');
        case 'outBuffer':
            return tr.infiltr('outNatura2000');
    }
};

const goToFullView = () => getCoord().map(c => navigateFull(c));

const renderShortCat = () =>
    getCatNb().fold(DIV('label cat', tr.infiltr('infNoCatShort')), cat => {
        const short = cat === 'blanco' ? tr.infiltr('infCatBlancoShort') : cat;
        return DIV('label cat', short);
    });

const category = () =>
    DIV(
        {
            className: 'list-elem legal cat',
            onClick: goToFullView,
        },
        renderPicto('pic-08-cat'),
        SPAN(
            'label',
            SPAN({
                className: '',
                dangerouslySetInnerHTML: {
                    __html: tr.infiltr('legalCAT'),
                },
            }),
            renderShortCat()
        )
    );

const envPermit = () =>
    DIV(
        {
            className: 'list-elem legal env',
            onClick: goToFullView,
        },
        renderPicto('pic-00-env-shadow'),
        tr.infiltr('legalEnv')
    );

const rru = () =>
    DIV(
        {
            className: 'list-elem legal rru true',
            onClick: goToFullView,
        },
        renderPicto('pic-03-rru-shadow'),
        tr.infiltr('legalRRU'),
        renderPicto('pic-triangle')
    );

const ppas = () =>
    inPPAS().map(inPPAS =>
        DIV(
            {
                className: `list-elem legal ppas ${inPPAS}`,
                onClick: goToFullView,
            },
            renderPicto('pic-05-ppas-shadow'),
            tr.infiltr('legalPPAS'),
            inPPAS ? renderPicto('pic-triangle') : renderPicto('pic-circle-yes')
        )
    );

const rcu = () =>
    inRCU().map(inRCU =>
        DIV(
            {
                className: `list-elem legal rcu ${inRCU}`,
                onClick: goToFullView,
            },
            renderPicto('pic-06-rcu-shadow'),
            tr.infiltr('legalRCU'),
            inRCU ? renderPicto('pic-triangle') : renderPicto('pic-circle-yes')
        )
    );

const pad = () =>
    inPAD().map(inPAD =>
        DIV(
            {
                className: `list-elem legal pad ${inPAD}`,
                onClick: goToFullView,
            },
            renderPicto('pic-04-pad-shadow'),
            tr.infiltr('legalPAD'),
            inPAD ? renderPicto('pic-triangle') : renderPicto('pic-circle-yes')
        )
    );

const patrimoine = () =>
    isHeritage().map(inHeritage =>
        DIV(
            {
                className: `list-elem legal heritage ${inHeritage}`,
                onClick: goToFullView,
            },
            renderPicto('pic-07-dpc-shadow'),
            tr.infiltr('legalHeritage'),
            inHeritage
                ? renderPicto('pic-triangle')
                : renderPicto('pic-circle-yes')
        )
    );

const envLegalList = () => DIV('legal-list env', envPermit(), category());

const urbaLegalList = () =>
    DIV(
        'legal-list urba',
        rru(),
        rcu(),
        pad(),
        ppas(),
        patrimoine(),
        category()
    );

const envAndUrbaLegalList = () =>
    DIV('legal-list env-urba', envPermit(), urbaLegalList());

const defaultLegalList = () => DIV('legal-list', category());

const waterCapture = () =>
    nearWaterCapture().map(capt =>
        DIV(
            {
                className: `list-elem proximity capture  ${capt}`,
                onClick: goToFullView,
            },
            renderPicto('pic-09-pump'),
            DIV({
                className: 'label',
                dangerouslySetInnerHTML: {
                    __html: capt
                        ? tr.infiltr('proximityCapture')
                        : tr.infiltr('outCapture'),
                },
            })
        )
    );

const waterSurfaceInWater = () => tr.infiltr('proximitySurfaceInWater');

const waterSurfaceInBuffer = (distance: number) =>
    DIV(
        '',
        concat(
            tr.infiltr('proximitySurface'),
            ' ',
            tr.core('at'),
            ' ',
            withMeter(distance)
        )
    );

const waterSurfaceBeyondBuffer = () =>
    DIV({
        className: '',
        dangerouslySetInnerHTML: {
            __html: tr.infiltr('proximitySurfaceOutBuffer'),
        },
    });

const surfaceWater = () =>
    getSurfaceWaterDist().map(sw =>
        DIV(
            {
                className: `list-elem proximity surface`,
                onClick: goToFullView,
            },
            renderPicto('pic-10-water'),
            DIV(
                'label',
                fromNullable(sw)
                    .map(sw =>
                        sw > 0
                            ? waterSurfaceInBuffer(sw)
                            : waterSurfaceInWater()
                    )
                    .getOrElseL(waterSurfaceBeyondBuffer)
            )
        )
    );

const natura2000 = () =>
    inNatura2000().map(n =>
        DIV(
            {
                className: `list-elem proximity natura2000 ${n}`,
                onClick: goToFullView,
            },
            renderPicto('pic-11-natura-2000'),
            DIV('label', natura2000Text(n))
        )
    );

const phreatic = () =>
    nearPhreatic().map(p =>
        DIV(
            {
                className: `list-elem hydrogeo phreatic ${p}`,
                onClick: goToFullView,
            },
            renderPicto('pic-12-depth'),
            DIV({
                className: 'label',
                dangerouslySetInnerHTML: {
                    __html: p
                        ? tr.infiltr('hydrogeoPhreatic')
                        : tr.infiltr('hydrogeoNoPhreatic'),
                },
            })
        )
    );

const carrotLink = () =>
    getCoord().map(c =>
        DIV(
            {
                className: 'list-elem hydrogeo carrot true',
                onClick: goToFullView,
            },
            renderPicto('pic-13-hydrogeo'),
            DIV(
                'label',
                getBrugeotoolURL().map(url =>
                    DIV(
                        'label link-btn brugeotool',
                        trackedLink(
                            url,
                            {
                                href: `${url}general/${c[0]}/${c[1]}`,
                            },
                            tr.infiltr('seeCarrot')
                        )
                    )
                )
            )
        )
    );

const floodAlea = () =>
    inFloodAlea().map(f =>
        DIV(
            {
                className: `list-elem hydrogeo flood-alea ${f}`,
                onClick: goToFullView,
            },
            renderPicto('pic-14-flooding'),
            DIV({
                className: 'label',
                dangerouslySetInnerHTML: {
                    __html: f
                        ? tr.infiltr('hydrogeoFloodYes')
                        : tr.infiltr('hydrogeoFloodNo'),
                },
            })
        )
    );

const legalList = () => {
    switch (getPermitSelection(hasEnvPermits(), hasUrbaPermits())) {
        case 'env':
            return envLegalList();
        case 'urba':
            return urbaLegalList();
        case 'envAndUrba':
            return envAndUrbaLegalList();
        case 'none':
            return defaultLegalList();
    }
};

const fundingRegion = () =>
    DIV(
        {
            className: 'list-elem funding region true',
            onClick: goToFullView,
        },
        renderPicto('pic-17-prime-regional'),
        tr.infiltr('fundingRegion'),
        renderPicto('pic-circle-yes')
    );

const fundingLocality = () =>
    hasFundingLocalityOpt().map(() =>
        DIV(
            {
                className: `list-elem funding locality true`,
                onClick: goToFullView,
            },
            renderPicto('pic-18-prime-communal'),
            tr.infiltr('fundingLocality'),
            renderPicto('pic-circle-yes')
        )
    );

const proximityList = () =>
    DIV('proximity-list', waterCapture(), surfaceWater(), natura2000());

const hydrogeoList = () =>
    DIV('hydrogeo-list', phreatic(), carrotLink(), floodAlea());

const fundingList = () =>
    DIV('funding-list', fundingRegion(), fundingLocality());

const renderLegalMeasures = () =>
    DIV(
        'summary-section legal',
        H4('infos-title summary-title', tr.infiltr('legalMeasures')),
        DIV('summary-info', legalList())
    );

const renderProximityToElements = () =>
    DIV(
        'summary-section proximity',
        H4('infos-title summary-title', tr.infiltr('proximityToElements')),
        DIV('summary-info', proximityList())
    );

const renderHydrogeoAndFloodRisk = () =>
    DIV(
        'summary-section hydrogeo',
        H4({
            className: 'infos-title summary-title',
            dangerouslySetInnerHTML: {
                __html: tr.infiltr('hydrogeoAndFloodRisk'),
            },
        }),
        DIV('summary-info', hydrogeoList())
    );

const renderFunding = () =>
    DIV(
        'summary-section funding',
        H4('infos-title summary-title', tr.infiltr('fundingTitle')),
        DIV('summary-info', fundingList())
    );

const render = () =>
    DIV(
        'summary',
        renderLegalMeasures(),
        renderProximityToElements(),
        renderHydrogeoAndFloodRisk(),
        renderFunding()
        // renderLocalPermits() TODO
    );

export default render;

logger('loaded');
