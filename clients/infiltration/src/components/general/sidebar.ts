/*
 *  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';

import { BUTTON, DIV, H1 } from 'sdi/components/elements';
import tr from 'sdi/locale';

import { navigateHome, navigateCoord, navigatePrint } from '../../events/route';
import { zoomIn, zoomOut } from 'infiltration/src/events/map';
import { getAddressStr } from '../../queries/infiltration';
import renderMap from '../map';
import { getMiniMapView, MINI_MAP } from 'infiltration/src/queries/map';
import { tryCoord2D } from 'sdi/map';
import { makeIcon, makeLabel } from 'sdi/components/button';
import { brugeotoolGoToAnalysis, bruwaterGoToLink } from '../button';
import { nameToString } from 'sdi/components/button/names';

const logger = debug('sdi:infiltration');

const renderTitle = () =>
    H1('sidebar__text', `${tr.infiltr('sidebarText')} ${getAddressStr()}`);

const backToMapButton = makeLabel('navigate', 1, () => tr.infiltr('backToMap'));

const zoomInButton = makeIcon('navigate', 2, 'plus', {
    position: 'left',
    text: () => tr.core('zoomIn'),
});

const zoomOutButton = makeIcon('navigate', 2, 'minus', {
    position: 'left',
    text: () => tr.core('zoomOut'),
});

const zoomButtons = () =>
    DIV(
        {
            className: 'mini-map--zoom unfocused',
            'aria-hidden': true,
        },
        zoomInButton(zoomIn),
        zoomOutButton(zoomOut)
    );

export const renderMinimapWrapper = () =>
    DIV(
        'mini-map',
        DIV(
            'centroid-wrapper',
            DIV({
                className: 'centroid',
                onClick: () => {
                    const c = getMiniMapView().center;
                    tryCoord2D(c).map(coord => navigateCoord(coord));
                },
            }),
            zoomButtons(),
            renderMap(MINI_MAP)
        )
    );

const printButton = () =>
    BUTTON(
        {
            className: 'print-report',
            onClick: navigatePrint,
        },
        DIV('btn__icon', nameToString('file-pdf')),
        DIV(
            'label__wrapper',
            DIV('print__label', tr.infiltr('report')),
            DIV('print__pitch', tr.infiltr('printAnalysis'))
        )
    );

export const renderSidebarTopContent = () =>
    DIV(
        'sidebar--top',
        DIV('title__wrapper', renderTitle(), DIV('title--arrow')),
        renderMinimapWrapper()
    );

export const renderSidebarBottomContent = () =>
    DIV(
        'sidebar--bottom',
        printButton(),
        brugeotoolGoToAnalysis(),
        bruwaterGoToLink(),
        DIV('back-to-map__wrapper', backToMapButton(navigateHome))
    );

// export const renderSidebar = () =>
//     DIV(
//         'sidebar sidebar-general',
//         renderSidebarTopContent(),
//         renderSidebarBottomContent()
//     );

logger('loaded');
