import { DIV } from 'sdi/components/elements';
import {
    getX,
    getY,
    hasEnvPermits,
    hasUrbaPermits,
} from 'infiltration/src/queries/infiltration';
import tr, { formatDate } from 'sdi/locale';
import { navigatePermitSelection } from 'infiltration/src/events/route';
import { getCoord } from 'infiltration/src/queries/infiltration';
import { makeLabel } from 'sdi/components/button';

const buttonBack = makeLabel('navigate', 2, () => tr.infiltr('backToAnalysis'));

const buttonPrint = makeLabel('navigate', 1, () => tr.core('print'));

export const printHelp = () =>
    DIV(
        { className: 'helptext' },
        tr.infiltrMD('helptext:printToPdf'),
        DIV(
            'print-actions',
            buttonBack(() =>
                navigatePermitSelection(hasEnvPermits(), hasUrbaPermits())
            ),
            buttonPrint(window.print)
        )
    );

export const printDate = () =>
    DIV({}, `${tr.infiltr('date')} : ${formatDate(new Date())}`);

const BASE_URL = 'https://geodata.environnement.brussels/client/infiltrasoil/';
// const system: string = getSystem()
//     .fold(
//         'technical',
//         s => s
//     );
export const projectUrl = () =>
    getCoord().map(coord =>
        DIV(
            {},
            `${tr.infiltr('url')} : ${BASE_URL}${getX(coord)}/${getY(coord)}`
        )
    );
