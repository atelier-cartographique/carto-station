/*
 *  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { DIV, IMG, H1, H2, BR, H3 } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { renderSidebarTopContent } from '../general/sidebar';

import { printDate, printHelp, projectUrl } from './elements';
import { logoBE } from './logo';
import detail from './../general/detail';
import {
    getAddressStr,
    hasEnvPermits,
    hasUrbaPermits,
    isUserProfessional,
} from 'infiltration/src/queries/infiltration';
import renderPicto from '../pictos';

// const { getImage } = engine();

const header = () =>
    DIV(
        'print-header',
        H1({}, tr.infiltr('printTitle')),
        H2('', getAddressStr()),
        BR({}),
        printDate(),
        projectUrl(),
        DIV('print-credit', tr.infiltr('creditsPrint')),
        IMG({ className: 'logo', src: logoBE }),
        BR({})
        // tr.infiltrMD('disclaimerPrint')
    );

const renderEnvInfos = () =>
    DIV(
        {
            key: 'status-env',
            className: 'status',
        },
        renderPicto('pic-01-permit-env'),
        hasEnvPermits()
            ? renderPicto('pic-checkbox-checked')
            : renderPicto('pic-checkbox_unchecked'),
        H3('', tr.infiltr('environmentalPermits'))
        // DIV('description', tr.infiltr('infPrescription'))
    );

const renderUrbaInfos = () =>
    DIV(
        {
            key: 'status-urba',
            className: 'status',
        },
        renderPicto('pic-02-permit-urb'),
        hasUrbaPermits()
            ? renderPicto('pic-checkbox-checked')
            : renderPicto('pic-checkbox_unchecked'),
        H3('', tr.infiltr('planningPermits'))
    );

const renderProInfos = (isPro: boolean) =>
    DIV(
        'pro-wrapper',
        DIV(
            {
                key: 'status-pro',
                className: 'status',
            },
            renderPicto('pic-16-facilitator'),
            isPro
                ? renderPicto('pic-checkbox-checked')
                : renderPicto('pic-checkbox_unchecked'),
            H3('', tr.infiltr('userProQuestion'))
        ),
        DIV('description', isPro ? tr.infiltrMD('infPitchFacilitator') : '')
    );

const renderProjectStatus = () =>
    DIV(
        'project-status',
        H2(
            'title',
            `${tr.infiltr('projectAt')} ${getAddressStr()} ${tr.infiltr(
                'hasRestrictions'
            )}`
        ),
        BR({}),
        renderEnvInfos(),
        renderUrbaInfos(),
        renderProInfos(isUserProfessional())
    );

const page1 = () =>
    DIV(
        { className: 'page' },
        header(),
        renderSidebarTopContent(),
        renderProjectStatus()
    );

const page2 = () =>
    DIV(
        { className: 'page' },
        header(),
        DIV(
            { className: 'page__content', style: { display: 'flex' } },
            detail()
        )
    );

export const render = () =>
    DIV({ className: 'css-print' }, printHelp(), page1(), page2());

export default render;
