/*
 *  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { DIV, SPAN, H1 } from 'sdi/components/elements';
import tr from 'sdi/locale';

import {
    getLoading,
    streetName,
    streetNumber,
    locality,
} from '../../queries/infiltration';

const render = () => {
    const l = getLoading();
    if (l.loading) {
        return DIV(
            { className: 'loader-part' },
            H1(
                {},
                SPAN({}, `${streetName()} ${streetNumber()} `),
                SPAN({}, tr.infiltr('in')),
                SPAN({}, ` ${locality()}`)
            ),
            // context(),
            DIV(
                { className: 'loading-msg' },
                `${tr.infiltr('loadingData')} ${l.loaded}/${l.total}`
            )
        );
    }
    return DIV({});
};

export default render;
