import { createElement } from 'react';
import { DIV } from 'sdi/components/elements';

export type PreserveAspectRatio =
    | 'none'
    | 'xMinYMin'
    | 'xMidYMin'
    | 'xMaxYMin'
    | 'xMinYMid'
    | 'xMidYMid'
    | 'xMaxYMid'
    | 'xMinYMax'
    | 'xMidYMax'
    | 'xMaxYMax';

const pictos = {
    'pic-05-ppas-shadow': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 64.66 54.27', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-05-ppas-shadow-cls-1{fill:#d9d9d9;}.ppic-05-ppas-shadow-cls-2{fill:#289084;}.ppic-05-ppas-shadow-cls-2,.ppic-05-ppas-shadow-cls-3{stroke:#fff;stroke-miterlimit:10;stroke-width:1.5px;}.ppic-05-ppas-shadow-cls-3{fill:none;stroke-linecap:round;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-05-ppas-shadow-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    {
                        id: 'ppic-05-ppas-shadow-Calque_2-2',
                        'data-name': 'Calque 2',
                    },
                    createElement('ellipse', {
                        className: 'ppic-05-ppas-shadow-cls-1',
                        cx: '32.33',
                        cy: '33.48',
                        rx: '32.33',
                        ry: '20.78',
                    }),
                    createElement('path', {
                        className: 'ppic-05-ppas-shadow-cls-2',
                        d: 'M51.16,14.48V7.66H7.45V40.11H51.16c3.34,0,6.05-2,6.05-4.58v-21Z',
                    }),
                    createElement('path', {
                        className: 'ppic-05-ppas-shadow-cls-2',
                        d: 'M45.12,35.53c0-2.53,2.71-4.58,6-4.58s6.05,2.05,6.05,4.58V5.33C57.21,2.8,54.5.75,51.16.75s-6,2.05-6,4.58Z',
                    }),
                    createElement('polyline', {
                        className: 'ppic-05-ppas-shadow-cls-3',
                        points: '32.15 36.32 34.74 29.41 39.19 21.83 44.84 11.27',
                    }),
                    createElement('polyline', {
                        className: 'ppic-05-ppas-shadow-cls-3',
                        points: '42.23 36.32 34.74 29.41 20.65 25.02 14.54 29.74',
                    }),
                    createElement('polyline', {
                        className: 'ppic-05-ppas-shadow-cls-3',
                        points: '24.71 36.32 20.65 25.02 27.28 16.02 19.59 11.13',
                    }),
                    createElement('polyline', {
                        className: 'ppic-05-ppas-shadow-cls-3',
                        points: '44.84 22.36 39.19 21.83 27.28 16.02',
                    }),
                    createElement('polyline', {
                        className: 'ppic-05-ppas-shadow-cls-3',
                        points: '14.54 16.9 29.04 23.75 34.14 19.18 32.91 11.13',
                    }),
                    createElement('line', {
                        className: 'ppic-05-ppas-shadow-cls-3',
                        x1: '29.04',
                        y1: '23.75',
                        x2: '34.74',
                        y2: '29.41',
                    })
                )
            )
        ),
    'pic-14-flooding': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            {
                viewBox: '0 0 65.009993 74.518267',
                version: '1.1',
                id: 'ppic-14-flooding-svg81',
                docname: '14-flooding.svg',
                width: '65.009995',
                height: '74.518265',
                preserveAspectRatio,
            },
            createElement('namedview', {
                id: 'ppic-14-flooding-namedview83',
                pagecolor: '#ffffff',
                bordercolor: '#666666',
                borderopacity: '1.0',
                pageshadow: '2',
                pageopacity: '0.0',
                pagecheckerboard: '0',
                showgrid: 'false',
                'fit-margin-top': '0',
                'fit-margin-left': '0',
                'fit-margin-right': '0',
                'fit-margin-bottom': '0',
                zoom: '1.2179496',
                cx: '-153.1262',
                cy: '238.51561',
                'window-width': '1920',
                'window-height': '1016',
                'window-x': '798',
                'window-y': '1467',
                'window-maximized': '1',
                'current-layer': 'Calque_2-2',
            }),
            createElement(
                'defs',
                { id: 'ppic-14-flooding-defs7' },
                createElement(
                    'style',
                    { id: 'ppic-14-flooding-style2' },
                    '.ppic-14-flooding-cls-1,.ppic-14-flooding-cls-3,.ppic-14-flooding-cls-4,.ppic-14-flooding-cls-5,.ppic-14-flooding-cls-7,.ppic-14-flooding-cls-8{fill:none;}.ppic-14-flooding-cls-10,.ppic-14-flooding-cls-11,.ppic-14-flooding-cls-2{fill:#289084;}.ppic-14-flooding-cls-10,.ppic-14-flooding-cls-11,.ppic-14-flooding-cls-3,.ppic-14-flooding-cls-4,.ppic-14-flooding-cls-5{stroke:#fff;}.ppic-14-flooding-cls-3,.ppic-14-flooding-cls-4,.ppic-14-flooding-cls-5{stroke-linecap:round;stroke-linejoin:round;}.ppic-14-flooding-cls-10,.ppic-14-flooding-cls-3,.ppic-14-flooding-cls-4,.ppic-14-flooding-cls-5{stroke-width:1.5px;}.ppic-14-flooding-cls-4{stroke-dasharray:0 4.92;}.ppic-14-flooding-cls-5{stroke-dasharray:0 5.05;}.ppic-14-flooding-cls-6{clip-path:url(#clip-path);}.ppic-14-flooding-cls-7{stroke:#1d1d1b;}.ppic-14-flooding-cls-10,.ppic-14-flooding-cls-11,.ppic-14-flooding-cls-7,.ppic-14-flooding-cls-8{stroke-miterlimit:10;}.ppic-14-flooding-cls-7,.ppic-14-flooding-cls-8{stroke-width:0.5px;}.ppic-14-flooding-cls-8{stroke:#9fd1b6;}.ppic-14-flooding-cls-9{fill:#9fd1b6;}'
                ),
                createElement(
                    'clipPath',
                    {
                        id: 'ppic-14-flooding-clip-path',
                        transform: 'translate(621.76)',
                    },
                    createElement('path', {
                        className: 'ppic-14-flooding-cls-1',
                        d: 'm 63.87,29.56 a 1,1 0 0 0 -1,-1.3 l -6.17,0.15 c -7.26,0.18 -8.72,-0.81 -10.41,-2 -0.6,-0.41 -1.27,-0.87 -2.08,-1.34 -1,-0.6 -2.34,-1.42 -3.85,-2.36 -4.24,-2.63 -9.5,-5.89 -14,-8.16 C 21.59,12.21 15,10.91 9.56,10.21 v 0 C 3.66,14 0,19.2 0,25 a 14.07,14.07 0 0 0 1.51,6.28 1,1 0 0 0 0.79,0.54 c 6.2,0.65 10.74,1.79 12.49,2.66 3.72,1.86 8.76,5 12.44,7.26 1.56,1 2.94,1.83 4,2.47 0.33,0.2 0.86,0.54 1.18,0.76 l 0.93,0.62 a 1,1 0 0 0 0.59,0.16 C 48.62,45.26 60.73,38.5 63.87,29.56 Z',
                        id: 'ppic-14-flooding-path4',
                    })
                )
            ),
            createElement(
                'g',
                {
                    id: 'ppic-14-flooding-Calque_2',
                    'data-name': 'Calque 2',
                    transform: 'translate(-621.76003,4.5969843e-4)',
                },
                createElement(
                    'g',
                    {
                        id: 'ppic-14-flooding-Calque_2-2',
                        'data-name': 'Calque 2',
                    },
                    createElement('ellipse', {
                        className: 'ppic-14-flooding-cls-2',
                        cx: '654.09003',
                        cy: '24.969999',
                        rx: '32.330002',
                        ry: '20.780001',
                        id: 'ppic-14-flooding-ellipse9',
                    }),
                    createElement('line', {
                        className: 'ppic-14-flooding-cls-3',
                        x1: '686.02002',
                        y1: '28.219999',
                        x2: '686.02002',
                        y2: '28.219999',
                        id: 'ppic-14-flooding-line11',
                    }),
                    createElement('path', {
                        className: 'ppic-14-flooding-cls-4',
                        d: 'm 59.35,28.35 -2.61,0.06 c -7.26,0.18 -8.72,-0.81 -10.41,-2 -0.6,-0.41 -1.27,-0.87 -2.08,-1.34 -1,-0.6 -2.34,-1.42 -3.85,-2.36 -4.24,-2.63 -9.5,-5.89 -14,-8.16 -4.08,-2 -9.48,-3.28 -14.36,-4',
                        transform: 'translate(621.76)',
                        id: 'ppic-14-flooding-path13',
                    }),
                    createElement('line', {
                        className: 'ppic-14-flooding-cls-3',
                        x1: '631.32001',
                        y1: '10.21',
                        x2: '631.32001',
                        y2: '10.21',
                        id: 'ppic-14-flooding-line15',
                    }),
                    createElement('line', {
                        className: 'ppic-14-flooding-cls-3',
                        x1: '655.39001',
                        y1: '45.73',
                        x2: '655.39001',
                        y2: '45.73',
                        id: 'ppic-14-flooding-line17',
                    }),
                    createElement('path', {
                        className: 'ppic-14-flooding-cls-5',
                        d: 'M 29.37,43 27.23,41.67 C 23.55,39.39 18.51,36.27 14.79,34.41 13.23,33.67 9.46,32.68 4.27,32',
                        transform: 'translate(621.76)',
                        id: 'ppic-14-flooding-path19',
                    }),
                    createElement('line', {
                        className: 'ppic-14-flooding-cls-3',
                        x1: '623.52002',
                        y1: '31.73',
                        x2: '623.52002',
                        y2: '31.73',
                        id: 'ppic-14-flooding-line21',
                    }),
                    createElement('path', {
                        className: 'ppic-14-flooding-cls-9',
                        d: 'M 39.18,33.72 C 35.5,31.55 27.68,26.43 21.89,23.54 c -4.94,-2.47 -14.14,-3.65 -20,-4 a 0.94,0.94 0 0 0 -1,0.62 L 0.8,20.4 a 1,1 0 0 0 0.84,1.3 c 5.77,0.51 13.32,1.67 17.62,3.83 5.79,2.89 13.61,8 17.29,10.18 3.27,1.93 5.3,4.89 17.19,4.7 a 1,1 0 0 0 0.51,-0.14 v 0 a 1,1 0 0 0 -0.5,-1.82 C 44.15,38.16 42.19,35.5 39.18,33.72 Z',
                        transform: 'translate(621.76)',
                        id: 'ppic-14-flooding-path71',
                    }),
                    createElement('path', {
                        className: 'ppic-14-flooding-cls-10',
                        d: 'M 39.27,1.16 36.2,5.41 A 1,1 0 0 0 36,6 v 6.15 a 1,1 0 0 0 0.76,1 l 7.77,1.78 a 1,1 0 0 0 1,-0.32 l 3,-3.53 a 1,1 0 0 0 0.23,-0.63 V 3.5 A 1,1 0 0 0 48,2.5 L 40.28,0.77 a 1,1 0 0 0 -1.01,0.39 z',
                        transform: 'translate(621.76)',
                        id: 'ppic-14-flooding-path73',
                    }),
                    createElement('path', {
                        className: 'ppic-14-flooding-cls-11',
                        d: 'm 34.58,51 v 22 a 1,1 0 0 0 1,1 h 3 V 73.23 L 41,74 a 1.26,1.26 0 0 0 0.28,0 h 7.24 a 1,1 0 0 0 1,-0.94 6.52,6.52 0 0 0 -0.27,-2.15 C 48.73,69.7 44.43,69.18 43.25,68.84 42.07,68.5 43.6,57.3 43.76,50.99 a 1,1 0 0 0 -1,-1 h -7.2 A 1,1 0 0 0 34.58,51 Z',
                        transform: 'translate(621.76)',
                        id: 'ppic-14-flooding-path75',
                    }),
                    createElement('path', {
                        className: 'ppic-14-flooding-cls-11',
                        d: 'm 22,50 a 1,1 0 0 0 -1,1 c 0.16,6.31 1.65,17.53 0.51,17.85 -1.14,0.32 -5.51,0.86 -6,2.07 a 6.52,6.52 0 0 0 -0.27,2.15 1,1 0 0 0 1,0.94 h 7.24 a 1.26,1.26 0 0 0 0.28,0 l 2.45,-0.73 V 74 h 3 a 1,1 0 0 0 1,-1 V 51 a 1,1 0 0 0 -1,-1 z',
                        transform: 'translate(621.76)',
                        id: 'ppic-14-flooding-path77',
                    })
                )
            )
        ),
    'pic-03-rru': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            {
                viewBox: '0 0 64.66 54.27',
                version: '1.1',
                id: 'ppic-03-rru-svg16',
                docname: '03-rru.svg',
                preserveAspectRatio,
            },
            createElement('namedview', {
                id: 'ppic-03-rru-namedview18',
                pagecolor: '#ffffff',
                bordercolor: '#666666',
                borderopacity: '1.0',
                pageshadow: '2',
                pageopacity: '0.0',
                pagecheckerboard: '0',
                showgrid: 'false',
                zoom: '5.6873431',
                cx: '22.330286',
                cy: '39.121958',
                'window-width': '1971',
                'window-height': '1146',
                'window-x': '194',
                'window-y': '97',
                'window-maximized': '0',
                'current-layer': 'Calque_2-2',
            }),
            createElement(
                'defs',
                { id: 'ppic-03-rru-defs4' },
                createElement(
                    'style',
                    { id: 'ppic-03-rru-style2' },
                    '.ppic-03-rru-cls-1{fill:#d9d9d9;}.ppic-03-rru-cls-2{fill:#289084;stroke-miterlimit:10;}.ppic-03-rru-cls-2,.ppic-03-rru-cls-3{stroke:#fff;stroke-width:1.5px;}.ppic-03-rru-cls-3{fill:#fff;stroke-linecap:round;stroke-linejoin:round;}#ellipse6{ fill:#d9d9d9;fill-opacity:0 }'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-03-rru-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    { id: 'ppic-03-rru-Calque_2-2', 'data-name': 'Calque 2' },
                    createElement('ellipse', {
                        className: 'ppic-03-rru-cls-1',
                        cx: '32.330002',
                        cy: '33.48',
                        rx: '32.330002',
                        ry: '20.780001',
                        id: 'ellipse6',
                    }),
                    createElement('path', {
                        className: 'ppic-03-rru-cls-2',
                        d: 'M51.16,14.48V7.66H7.45V40.11H51.16c3.34,0,6.05-2,6.05-4.58v-21Z',
                        id: 'ppic-03-rru-path8',
                    }),
                    createElement('path', {
                        className: 'ppic-03-rru-cls-2',
                        d: 'M45.12,35.53c0-2.53,2.71-4.58,6-4.58s6.05,2.05,6.05,4.58V5.33C57.21,2.8,54.5.75,51.16.75s-6,2.05-6,4.58Z',
                        id: 'ppic-03-rru-path10',
                    }),
                    createElement('polygon', {
                        className: 'ppic-03-rru-cls-3',
                        points: '22.37 16.16 25.82 14.31 29.53 15.1 32.27 12.63 34.39 14.66 34.66 17.58 33.86 19.7 37.48 22 38.72 26.5 37.04 26.59 36.16 28.27 39.34 30.3 30.24 34.81 30.06 33.57 24.85 32.51 23.79 29.51 22.55 29.24 23.08 27.3 20.78 28.09 17.34 26.42 18.66 24.12 20.96 23.68 21.31 20.85 20.34 19.7 22.11 17.58 22.37 16.16',
                        id: 'ppic-03-rru-polygon12',
                    })
                )
            )
        ),
    'pic-13-hydrogeo': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 64.66 73.5', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-13-hydrogeo-cls-1,.ppic-13-hydrogeo-cls-3,.ppic-13-hydrogeo-cls-4,.ppic-13-hydrogeo-cls-5,.ppic-13-hydrogeo-cls-6,.ppic-13-hydrogeo-cls-7{fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.5px;}.ppic-13-hydrogeo-cls-1{stroke:#00789c;}.ppic-13-hydrogeo-cls-2{fill:#289084;}.ppic-13-hydrogeo-cls-3,.ppic-13-hydrogeo-cls-4,.ppic-13-hydrogeo-cls-5,.ppic-13-hydrogeo-cls-6,.ppic-13-hydrogeo-cls-7{stroke:#fff;}.ppic-13-hydrogeo-cls-4{stroke-dasharray:0 4.59;}.ppic-13-hydrogeo-cls-5{stroke-dasharray:0 4.59;}.ppic-13-hydrogeo-cls-6{stroke-dasharray:0 4.9;}.ppic-13-hydrogeo-cls-7{stroke-dasharray:0 4.71;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-13-hydrogeo-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    {
                        id: 'ppic-13-hydrogeo-Calque_2-2',
                        'data-name': 'Calque 2',
                    },
                    createElement('polygon', {
                        className: 'ppic-13-hydrogeo-cls-1',
                        points: '32.33 72.75 32.33 28 48.86 20.01 48.86 64.77 32.33 72.75',
                    }),
                    createElement('polygon', {
                        className: 'ppic-13-hydrogeo-cls-1',
                        points: '32.33 72.75 32.33 28 15.8 20.01 15.8 64.77 32.33 72.75',
                    }),
                    createElement('ellipse', {
                        className: 'ppic-13-hydrogeo-cls-2',
                        cx: '32.33',
                        cy: '20.78',
                        rx: '32.33',
                        ry: '20.78',
                    }),
                    createElement('line', {
                        className: 'ppic-13-hydrogeo-cls-3',
                        x1: '15.81',
                        y1: '20.01',
                        x2: '15.81',
                        y2: '20.01',
                    }),
                    createElement('line', {
                        className: 'ppic-13-hydrogeo-cls-4',
                        x1: '19.94',
                        y1: '18.01',
                        x2: '30.27',
                        y2: '13.02',
                    }),
                    createElement('line', {
                        className: 'ppic-13-hydrogeo-cls-3',
                        x1: '32.33',
                        y1: '12.02',
                        x2: '32.33',
                        y2: '12.02',
                    }),
                    createElement('line', {
                        className: 'ppic-13-hydrogeo-cls-5',
                        x1: '36.46',
                        y1: '14.02',
                        x2: '46.79',
                        y2: '19.01',
                    }),
                    createElement('line', {
                        className: 'ppic-13-hydrogeo-cls-3',
                        x1: '48.86',
                        y1: '20.01',
                        x2: '48.86',
                        y2: '20.01',
                    }),
                    createElement('line', {
                        className: 'ppic-13-hydrogeo-cls-3',
                        x1: '32.33',
                        y1: '37.8',
                        x2: '32.33',
                        y2: '37.8',
                    }),
                    createElement('line', {
                        className: 'ppic-13-hydrogeo-cls-6',
                        x1: '32.33',
                        y1: '32.9',
                        x2: '32.33',
                        y2: '30.45',
                    }),
                    createElement('line', {
                        className: 'ppic-13-hydrogeo-cls-3',
                        x1: '32.33',
                        y1: '28',
                        x2: '32.33',
                        y2: '28',
                    }),
                    createElement('line', {
                        className: 'ppic-13-hydrogeo-cls-4',
                        x1: '36.46',
                        y1: '26',
                        x2: '46.79',
                        y2: '21.01',
                    }),
                    createElement('line', {
                        className: 'ppic-13-hydrogeo-cls-3',
                        x1: '48.86',
                        y1: '20.01',
                        x2: '48.86',
                        y2: '20.01',
                    }),
                    createElement('line', {
                        className: 'ppic-13-hydrogeo-cls-7',
                        x1: '48.86',
                        y1: '24.72',
                        x2: '48.86',
                        y2: '31.78',
                    }),
                    createElement('line', {
                        className: 'ppic-13-hydrogeo-cls-3',
                        x1: '48.86',
                        y1: '34.13',
                        x2: '48.86',
                        y2: '34.13',
                    }),
                    createElement('line', {
                        className: 'ppic-13-hydrogeo-cls-3',
                        x1: '32.33',
                        y1: '28',
                        x2: '32.33',
                        y2: '28',
                    }),
                    createElement('line', {
                        className: 'ppic-13-hydrogeo-cls-5',
                        x1: '28.2',
                        y1: '26',
                        x2: '17.87',
                        y2: '21.01',
                    }),
                    createElement('line', {
                        className: 'ppic-13-hydrogeo-cls-3',
                        x1: '15.81',
                        y1: '20.01',
                        x2: '15.81',
                        y2: '20.01',
                    }),
                    createElement('line', {
                        className: 'ppic-13-hydrogeo-cls-7',
                        x1: '15.81',
                        y1: '24.72',
                        x2: '15.81',
                        y2: '31.78',
                    }),
                    createElement('line', {
                        className: 'ppic-13-hydrogeo-cls-3',
                        x1: '15.81',
                        y1: '34.13',
                        x2: '15.81',
                        y2: '34.13',
                    })
                )
            )
        ),
    'pic-04-pad-shadow': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 64.66 54.27', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-04-pad-shadow-cls-1{fill:#d9d9d9;}.ppic-04-pad-shadow-cls-2{fill:#289084;}.ppic-04-pad-shadow-cls-2,.ppic-04-pad-shadow-cls-4{stroke:#fff;stroke-miterlimit:10;stroke-width:1.5px;}.ppic-04-pad-shadow-cls-3{fill:#fff;}.ppic-04-pad-shadow-cls-4{fill:none;stroke-linecap:round;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-04-pad-shadow-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    {
                        id: 'ppic-04-pad-shadow-Calque_2-2',
                        'data-name': 'Calque 2',
                    },
                    createElement('ellipse', {
                        className: 'ppic-04-pad-shadow-cls-1',
                        cx: '32.33',
                        cy: '33.48',
                        rx: '32.33',
                        ry: '20.78',
                    }),
                    createElement('path', {
                        className: 'ppic-04-pad-shadow-cls-2',
                        d: 'M51.16,14.48V7.66H7.45V40.11H51.16c3.34,0,6.05-2,6.05-4.58v-21Z',
                    }),
                    createElement('path', {
                        className: 'ppic-04-pad-shadow-cls-2',
                        d: 'M45.12,35.53c0-2.53,2.71-4.58,6-4.58s6.05,2.05,6.05,4.58V5.33C57.21,2.8,54.5.75,51.16.75s-6,2.05-6,4.58Z',
                    }),
                    createElement('polygon', {
                        className: 'ppic-04-pad-shadow-cls-3',
                        points: '18.14 29.48 23.94 14.88 38.84 22.13 36.51 34.42 18.14 29.48',
                    }),
                    createElement('polyline', {
                        className: 'ppic-04-pad-shadow-cls-4',
                        points: '27.34 10.7 23.94 14.88 18.14 29.48 14.65 33.09',
                    }),
                    createElement('polyline', {
                        className: 'ppic-04-pad-shadow-cls-4',
                        points: '18.18 15.15 23.94 14.88 38.84 22.13 44.95 17.41',
                    }),
                    createElement('polyline', {
                        className: 'ppic-04-pad-shadow-cls-4',
                        points: '34.78 10.84 38.84 22.13 36.51 34.42 39.9 36.35',
                    }),
                    createElement('polyline', {
                        className: 'ppic-04-pad-shadow-cls-4',
                        points: '14.46 24.79 18.14 29.48 36.51 34.42',
                    })
                )
            )
        ),
    'pic-15-fragile': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 66 73.5', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-15-fragile-cls-1,.ppic-15-fragile-cls-4,.ppic-15-fragile-cls-5,.ppic-15-fragile-cls-6{fill:none;}.ppic-15-fragile-cls-1{stroke:#1d1d1b;stroke-miterlimit:10;stroke-dasharray:2.98 1.99;}.ppic-15-fragile-cls-1,.ppic-15-fragile-cls-4,.ppic-15-fragile-cls-5{stroke-width:1.5px;}.ppic-15-fragile-cls-2{fill:#9fd1b6;}.ppic-15-fragile-cls-3{fill:#289084;}.ppic-15-fragile-cls-4,.ppic-15-fragile-cls-5{stroke:#fff;stroke-linecap:round;stroke-linejoin:round;}.ppic-15-fragile-cls-5{stroke-dasharray:0 4.94;}.ppic-15-fragile-cls-7{fill:#00789c;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-15-fragile-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    {
                        id: 'ppic-15-fragile-Calque_2-2',
                        'data-name': 'Calque 2',
                    },
                    createElement('path', {
                        className: 'ppic-15-fragile-cls-1',
                        d: 'M.75,52c0-11.48,14.48-20.79,32.33-20.79C76,32.3,76,71.64,33.08,72.75,15.23,72.75.75,63.45.75,52Z',
                    }),
                    createElement('path', {
                        className: 'ppic-15-fragile-cls-2',
                        d: 'M39.93,60.72c-3.68-2.17-11.5-7.29-17.29-10.18C17.5,48,7.74,46.8,1.9,46.48a14.83,14.83,0,0,0-.71,2.1C7.07,49,15.4,50.2,20,52.51c5.79,2.89,13.61,8,17.29,10.18,3.29,2,5.33,4.94,17.46,4.7a30.81,30.81,0,0,0,3-2C45.29,65.72,43.25,62.68,39.93,60.72Z',
                    }),
                    createElement('ellipse', {
                        className: 'ppic-15-fragile-cls-3',
                        cx: '33.08',
                        cy: '20.78',
                        rx: '32.33',
                        ry: '20.78',
                    }),
                    createElement('line', {
                        className: 'ppic-15-fragile-cls-4',
                        x1: '11.71',
                        y1: '36.38',
                        x2: '11.71',
                        y2: '36.38',
                    }),
                    createElement('path', {
                        className: 'ppic-15-fragile-cls-5',
                        d: 'M16.17,34.26a46.18,46.18,0,0,1,16.91-3.08,45.09,45.09,0,0,1,19.18,4.06',
                    }),
                    createElement('line', {
                        className: 'ppic-15-fragile-cls-4',
                        x1: '54.45',
                        y1: '36.38',
                        x2: '54.45',
                        y2: '36.38',
                    }),
                    createElement('path', {
                        className: 'ppic-15-fragile-cls-6',
                        d: 'M11.71,36.38C5,40.19.75,45.76.75,52c0,11.48,14.48,20.78,32.33,20.78S65.41,63.45,65.41,52c0-6.21-4.24-11.78-11-15.59',
                    }),
                    createElement('polygon', {
                        className: 'ppic-15-fragile-cls-7',
                        points: '14.04 42.85 15.51 56.63 23.14 58.11 15.51 59.58 14.04 73.36 12.56 59.58 4.93 58.11 12.56 56.63 14.04 42.85',
                    }),
                    createElement('polygon', {
                        className: 'ppic-15-fragile-cls-7',
                        points: '45.67 42.22 46.7 50.1 51.97 50.94 46.7 51.79 45.67 59.66 44.65 51.79 39.38 50.94 44.65 50.1 45.67 42.22',
                    })
                )
            )
        ),
    'pic-checkbox_unchecked': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            {
                viewBox: '0 0 42.84 37.81',
                version: '1.1',
                id: 'ppic-checkbox_unchecked-svg12',
                docname: 'checkbox2.svg',
                preserveAspectRatio,
            },
            createElement('namedview', {
                id: 'ppic-checkbox_unchecked-namedview14',
                pagecolor: '#ffffff',
                bordercolor: '#666666',
                borderopacity: '1.0',
                pageshadow: '2',
                pageopacity: '0.0',
                pagecheckerboard: '0',
                showgrid: 'false',
                'snap-global': 'true',
                'snap-bbox': 'true',
                'bbox-paths': 'true',
                'snap-page': 'true',
                zoom: '11.161068',
                cx: '14.111553',
                cy: '18.412216',
                'window-width': '1920',
                'window-height': '1016',
                'window-x': '798',
                'window-y': '1467',
                'window-maximized': '1',
                'current-layer': 'Calque_2-2',
            }),
            createElement(
                'defs',
                { id: 'ppic-checkbox_unchecked-defs4' },
                createElement(
                    'style',
                    { id: 'ppic-checkbox_unchecked-style2' },
                    '.ppic-checkbox_unchecked-cls-1{fill:#fff;stroke:#000;}.ppic-checkbox_unchecked-cls-2{fill:none;stroke:#289084;stroke-miterlimit:10;stroke-width:8px;}#rect1035{ fill:#ffffff;stroke-width:1.00157;stroke-linecap:round;stroke-linejoin:round;fill-opacity:0.00688572 }'
                )
            ),
            createElement(
                'g',
                {
                    id: 'ppic-checkbox_unchecked-Calque_2',
                    'data-name': 'Calque 2',
                },
                createElement(
                    'g',
                    {
                        id: 'ppic-checkbox_unchecked-Calque_2-2',
                        'data-name': 'Calque 2',
                    },
                    createElement('rect', {
                        id: 'rect1035',
                        width: '42.722328',
                        height: '37.67741',
                        x: '0.11767283',
                        y: '0.13259111',
                    }),
                    createElement('rect', {
                        className: 'ppic-checkbox_unchecked-cls-1',
                        x: '0.5',
                        y: '5.31',
                        width: '32',
                        height: '32',
                        rx: '4.5',
                        id: 'ppic-checkbox_unchecked-rect6',
                    })
                )
            )
        ),
    'pic-17-empty': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 64.57 41.52', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-17-empty-cls-1{fill:#289084;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-17-empty-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    { id: 'ppic-17-empty-Calque_3', 'data-name': 'Calque 3' },
                    createElement('path', {
                        className: 'ppic-17-empty-cls-1',
                        d: 'M32.28,0h-.06a1,1,0,0,0,0,2h.05a1,1,0,0,0,0-2ZM27.56.22h-.15a1,1,0,0,0-.9,1.1,1,1,0,0,0,1,.9h.15a1,1,0,0,0,.89-1.09,1,1,0,0,0-1-.91ZM22.89.91l-.2,0h-.05a1,1,0,0,0-.79,1.18,1,1,0,0,0,1,.8l.2,0h0a1,1,0,0,0-.19-2ZM18.32,2.08a1,1,0,0,0-.3,0l-.05,0a1,1,0,0,0,.3,2,1,1,0,0,0,.3,0l.05,0a1,1,0,0,0-.3-1.95ZM13.94,3.76a.9.9,0,0,0-.42.1h0a1,1,0,0,0,.42,1.91,1.06,1.06,0,0,0,.42-.09l0,0a1,1,0,0,0-.41-1.91ZM9.83,6a1,1,0,0,0-.54.16l0,0h0A1,1,0,0,0,9,7.58a1,1,0,0,0,1.39.3l0,0A1,1,0,0,0,9.83,6ZM6.18,8.87a1,1,0,0,0-.68.27l0,0h0a1,1,0,0,0,.69,1.72,1,1,0,0,0,.69-.27l0,0a1,1,0,0,0-.69-1.72Zm-2.94,3.5a1,1,0,0,0-.84.46h0v0h0l0,0a1,1,0,0,0,.3,1.38,1.08,1.08,0,0,0,.54.15A1,1,0,0,0,4.06,14l0,0a1,1,0,0,0-.3-1.38,1,1,0,0,0-.54-.16Zm-1.85,4.1a1,1,0,0,0-1,.74h0v0a1,1,0,0,0,.71,1.22,1.15,1.15,0,0,0,.26,0,1,1,0,0,0,1-.74v-.05a1,1,0,0,0-.71-1.23,1.15,1.15,0,0,0-.26,0ZM1,20.9H.91A1,1,0,0,0,0,22v.06A1,1,0,0,0,1,23h.1A1,1,0,0,0,2,21.86v-.05a1,1,0,0,0-1-.91Zm1.16,4.32a1.06,1.06,0,0,0-.42.09,1,1,0,0,0-.5,1.32h0v0a1,1,0,0,0,.91.58,1,1,0,0,0,.41-.09,1,1,0,0,0,.5-1.32l0-.05a1,1,0,0,0-.91-.58ZM4.61,29a1,1,0,0,0-.65.23,1,1,0,0,0-.12,1.41l0,0h0a1,1,0,0,0,.76.36,1,1,0,0,0,.76-1.65l0,0A1,1,0,0,0,4.61,29Zm3.33,3.17a1,1,0,0,0-.79.39A1,1,0,0,0,7.33,34l0,0h0A1,1,0,0,0,8.6,32.46l0,0a1,1,0,0,0-.62-.21Zm3.91,2.55a1,1,0,0,0-.49,1.87l.06,0a1,1,0,0,0,1.35-.4,1,1,0,0,0-.4-1.36l0,0a1,1,0,0,0-.47-.12Zm4.26,2a1,1,0,0,0-.37,1.93h.06a1.15,1.15,0,0,0,.36.07,1,1,0,0,0,.35-1.94l-.05,0a1.07,1.07,0,0,0-.35-.06Zm4.48,1.42a1,1,0,0,0-.25,2h.06a1,1,0,0,0,1.21-.72,1,1,0,0,0-.72-1.22h-.05a1.07,1.07,0,0,0-.25,0Zm4.63.92a1,1,0,0,0-.15,2h.2a1,1,0,0,0,.15-2h-.2Zm4.7.45a1,1,0,0,0-.05,2H30a1,1,0,0,0,0-2h-.1Zm4.78,0h-.1a1,1,0,0,0-1,1.05,1,1,0,0,0,1,.95h.1a1,1,0,0,0,0-2Zm4.71-.46h-.2a1,1,0,0,0,.14,2h.21a1,1,0,0,0-.15-2ZM44,38.12l-.25,0-.05,0a1,1,0,0,0,.25,2,.73.73,0,0,0,.25,0h.05a1,1,0,0,0-.25-2Zm4.49-1.43a.92.92,0,0,0-.36.07l0,0a1,1,0,0,0,.36,1.93,1,1,0,0,0,.35-.06h.06a1,1,0,0,0-.36-1.94Zm4.25-2a1,1,0,0,0-.48.12l0,0a1,1,0,0,0,.48,1.87,1,1,0,0,0,.47-.12h0l0,0a1,1,0,0,0,.39-1.35,1,1,0,0,0-.88-.52Zm3.9-2.55a1,1,0,0,0-.62.21l0,0a1,1,0,0,0-.17,1.4,1,1,0,0,0,.79.39,1,1,0,0,0,.61-.21l0,0h0a1,1,0,0,0,.17-1.4,1,1,0,0,0-.79-.38ZM60,29a1,1,0,0,0-.77.36l0,0a1,1,0,1,0,1.53,1.29l0,0a1,1,0,0,0-.13-1.41A1,1,0,0,0,60,29Zm2.43-3.83a1,1,0,0,0-.91.59l0,0a1,1,0,0,0,.5,1.33,1,1,0,0,0,.41.09,1,1,0,0,0,.91-.59h0l0,0a1,1,0,0,0-.51-1.32,1,1,0,0,0-.41-.09Zm1.14-4.32a1,1,0,0,0-1,.92v0a1,1,0,0,0,.91,1.08h.09a1,1,0,0,0,1-.9v-.07a1,1,0,0,0-.92-1.07Zm-.41-4.43a.84.84,0,0,0-.27,0,1,1,0,0,0-.7,1.23v0a1,1,0,0,0,1,.74,1.15,1.15,0,0,0,.26,0,1,1,0,0,0,.71-1.22l0-.05h0a1,1,0,0,0-1-.74Zm-1.87-4.09a1,1,0,0,0-.84,1.55l0,0a1,1,0,0,0,1.68-1.08l0-.06a1,1,0,0,0-.83-.45Zm-3-3.5a1,1,0,0,0-.72.32,1,1,0,0,0,0,1.41l0,0a1,1,0,0,0,.69.27,1,1,0,0,0,.69-1.72v0h0v0a1,1,0,0,0-.69-.27ZM54.68,6a1,1,0,0,0-.55,1.84l0,0a1,1,0,0,0,.54.16,1,1,0,0,0,.55-1.83l0,0,0,0A1.08,1.08,0,0,0,54.68,6ZM50.57,3.74a1,1,0,0,0-.91.58,1,1,0,0,0,.5,1.33l0,0a1.06,1.06,0,0,0,.42.09A1,1,0,0,0,51,3.85l0,0a1.06,1.06,0,0,0-.42-.09ZM46.18,2.06A1,1,0,0,0,45.88,4l0,0a1,1,0,0,0,.3,0,1,1,0,0,0,.31-2h-.06a1,1,0,0,0-.3,0ZM41.61.9a1,1,0,0,0-1,.8,1,1,0,0,0,.79,1.18h0l.2,0a1,1,0,0,0,.19-2H41.8a.58.58,0,0,0-.19,0ZM36.94.22a1,1,0,0,0-.09,2H37a1,1,0,0,0,.09-2h-.15Z',
                    })
                )
            )
        ),
    'pic-06-rcu': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            {
                viewBox: '0 0 64.66 54.27',
                version: '1.1',
                id: 'ppic-06-rcu-svg16',
                docname: '06-rcu.svg',
                preserveAspectRatio,
            },
            createElement('namedview', {
                id: 'ppic-06-rcu-namedview18',
                pagecolor: '#ffffff',
                bordercolor: '#666666',
                borderopacity: '1.0',
                pageshadow: '2',
                pageopacity: '0.0',
                pagecheckerboard: '0',
                showgrid: 'false',
                zoom: '16.086235',
                cx: '32.325773',
                cy: '27.135',
                'window-width': '2075',
                'window-height': '1376',
                'window-x': '0',
                'window-y': '27',
                'window-maximized': '0',
                'current-layer': 'Calque_2-2',
            }),
            createElement(
                'defs',
                { id: 'ppic-06-rcu-defs4' },
                createElement(
                    'style',
                    { id: 'ppic-06-rcu-style2' },
                    '.ppic-06-rcu-cls-1{fill:#d9d9d9;}.ppic-06-rcu-cls-2{fill:#289084;stroke-miterlimit:10;}.ppic-06-rcu-cls-2,.ppic-06-rcu-cls-3{stroke:#fff;stroke-width:1.5px;}.ppic-06-rcu-cls-3{fill:none;stroke-linecap:round;stroke-linejoin:round;}#ellipse6{ fill:#d9d9d9;fill-opacity:0.03246759 }'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-06-rcu-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    { id: 'ppic-06-rcu-Calque_2-2', 'data-name': 'Calque 2' },
                    createElement('ellipse', {
                        className: 'ppic-06-rcu-cls-1',
                        cx: '32.33',
                        cy: '33.48',
                        rx: '32.33',
                        ry: '20.78',
                        id: 'ellipse6',
                    }),
                    createElement('path', {
                        className: 'ppic-06-rcu-cls-2',
                        d: 'M51.16,14.48V7.66H7.45V40.11H51.16c3.34,0,6.05-2,6.05-4.58v-21Z',
                        id: 'ppic-06-rcu-path8',
                    }),
                    createElement('path', {
                        className: 'ppic-06-rcu-cls-2',
                        d: 'M45.12,35.53c0-2.53,2.71-4.58,6-4.58s6.05,2.05,6.05,4.58V5.33C57.21,2.8,54.5.75,51.16.75s-6,2.05-6,4.58Z',
                        id: 'ppic-06-rcu-path10',
                    }),
                    createElement('polygon', {
                        className: 'ppic-06-rcu-cls-3',
                        points: '35.66 33.02 37.63 25.59 36.37 19.26 31.87 17.57 28.63 14.48 23.71 14.48 17.66 19.82 15.13 25.59 19.49 28.96 23.43 29.1 25.68 33.02 35.66 33.02',
                        id: 'ppic-06-rcu-polygon12',
                    })
                )
            )
        ),
    'pic-arrow-title': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            {
                viewBox: '0 0 10.26 60.339999',
                version: '1.1',
                id: 'ppic-arrow-title-svg10',
                width: '10.26',
                height: '60.34',
                preserveAspectRatio,
            },
            createElement(
                'defs',
                { id: 'ppic-arrow-title-defs4' },
                createElement(
                    'style',
                    { id: 'ppic-arrow-title-style2' },
                    '.ppic-arrow-title-cls-1{fill:#cee8da;}'
                )
            ),
            createElement(
                'g',
                {
                    id: 'ppic-arrow-title-Calque_2',
                    'data-name': 'Calque 2',
                    transform: 'rotate(-90,30.17,30.17)',
                },
                createElement(
                    'g',
                    {
                        id: 'ppic-arrow-title-Calque_2-2',
                        'data-name': 'Calque 2',
                    },
                    createElement('polygon', {
                        className: 'ppic-arrow-title-cls-1',
                        points: '10.66,5.05 0,5.05 30.17,10.26 60.34,5.05 49.68,5.05 49.68,0 10.66,0 ',
                        id: 'ppic-arrow-title-polygon6',
                    })
                )
            )
        ),
    'pic-01-permit-env-shadow': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 64.66 72.78', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-01-permit-env-shadow-cls-1{isolation:isolate;}.ppic-01-permit-env-shadow-cls-2{fill:#d9d9d9;}.ppic-01-permit-env-shadow-cls-3,.ppic-01-permit-env-shadow-cls-6{fill:#289084;}.ppic-01-permit-env-shadow-cls-4,.ppic-01-permit-env-shadow-cls-7{fill:none;}.ppic-01-permit-env-shadow-cls-4,.ppic-01-permit-env-shadow-cls-6,.ppic-01-permit-env-shadow-cls-7{stroke:#fff;stroke-width:1.5px;}.ppic-01-permit-env-shadow-cls-4{stroke-linecap:round;stroke-linejoin:round;}.ppic-01-permit-env-shadow-cls-5{fill:#b6b6b6;mix-blend-mode:multiply;}.ppic-01-permit-env-shadow-cls-6,.ppic-01-permit-env-shadow-cls-7{stroke-miterlimit:10;}'
                )
            ),
            createElement(
                'g',
                { className: 'ppic-01-permit-env-shadow-cls-1' },
                createElement(
                    'g',
                    {
                        id: 'ppic-01-permit-env-shadow-Calque_2',
                        'data-name': 'Calque 2',
                    },
                    createElement(
                        'g',
                        {
                            id: 'ppic-01-permit-env-shadow-Calque_3',
                            'data-name': 'Calque 3',
                        },
                        createElement('ellipse', {
                            className: 'ppic-01-permit-env-shadow-cls-2',
                            cx: '32.33',
                            cy: '52',
                            rx: '32.33',
                            ry: '20.78',
                        }),
                        createElement('rect', {
                            className: 'ppic-01-permit-env-shadow-cls-3',
                            x: '14.83',
                            y: '15.85',
                            width: '35',
                            height: '46.19',
                        }),
                        createElement('line', {
                            className: 'ppic-01-permit-env-shadow-cls-4',
                            x1: '19.64',
                            y1: '27.81',
                            x2: '45.27',
                            y2: '27.81',
                        }),
                        createElement('line', {
                            className: 'ppic-01-permit-env-shadow-cls-4',
                            x1: '19.64',
                            y1: '36.31',
                            x2: '45.27',
                            y2: '36.31',
                        }),
                        createElement('line', {
                            className: 'ppic-01-permit-env-shadow-cls-4',
                            x1: '19.64',
                            y1: '40.21',
                            x2: '45.27',
                            y2: '40.21',
                        }),
                        createElement('line', {
                            className: 'ppic-01-permit-env-shadow-cls-4',
                            x1: '19.64',
                            y1: '44.12',
                            x2: '45.27',
                            y2: '44.12',
                        }),
                        createElement('line', {
                            className: 'ppic-01-permit-env-shadow-cls-4',
                            x1: '19.64',
                            y1: '48.02',
                            x2: '45.27',
                            y2: '48.02',
                        }),
                        createElement('line', {
                            className: 'ppic-01-permit-env-shadow-cls-4',
                            x1: '19.64',
                            y1: '51.92',
                            x2: '45.27',
                            y2: '51.92',
                        }),
                        createElement('rect', {
                            className: 'ppic-01-permit-env-shadow-cls-5',
                            x: '10.65',
                            y: '16.74',
                            width: '13.18',
                            height: '3.04',
                            transform: 'translate(-7.86 17.54) rotate(-45)',
                        }),
                        createElement('rect', {
                            className: 'ppic-01-permit-env-shadow-cls-5',
                            x: '40.83',
                            y: '16.74',
                            width: '13.18',
                            height: '3.04',
                            transform: 'translate(68.04 64.71) rotate(-135)',
                        }),
                        createElement('rect', {
                            className: 'ppic-01-permit-env-shadow-cls-5',
                            x: '10.65',
                            y: '58.11',
                            width: '13.18',
                            height: '3.04',
                            transform: 'translate(47.22 5.28) rotate(45)',
                        }),
                        createElement('rect', {
                            className: 'ppic-01-permit-env-shadow-cls-5',
                            x: '40.83',
                            y: '58.11',
                            width: '13.18',
                            height: '3.04',
                            transform: 'translate(123.12 68.26) rotate(135)',
                        }),
                        createElement('path', {
                            className: 'ppic-01-permit-env-shadow-cls-6',
                            d: 'M37.91,14.05v7.43c0,.28.63.69,1.62.69s1.64-.41,1.64-.69V14.05',
                        }),
                        createElement('path', {
                            className: 'ppic-01-permit-env-shadow-cls-3',
                            d: 'M45.75,9.83A4.1,4.1,0,0,0,43.21,4a3.69,3.69,0,0,0-7.34,0,4.1,4.1,0,0,0-2.54,5.81,4.06,4.06,0,0,0-.95,2.61,4.13,4.13,0,0,0,7.16,2.81,4.12,4.12,0,0,0,6.21-5.42Z',
                        }),
                        createElement('path', {
                            className: 'ppic-01-permit-env-shadow-cls-7',
                            d: 'M45.75,9.83A4.1,4.1,0,0,0,43.21,4a3.69,3.69,0,0,0-7.34,0,4.1,4.1,0,0,0-2.54,5.81,4.06,4.06,0,0,0-.95,2.61,4.13,4.13,0,0,0,7.16,2.81,4.12,4.12,0,0,0,6.21-5.42Z',
                        })
                    )
                )
            )
        ),
    'pic-01-permit-env': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 41.65 65.36', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-01-permit-env-cls-1{isolation:isolate;}.ppic-01-permit-env-cls-2,.ppic-01-permit-env-cls-5{fill:#289084;}.ppic-01-permit-env-cls-3,.ppic-01-permit-env-cls-6{fill:none;}.ppic-01-permit-env-cls-3,.ppic-01-permit-env-cls-5,.ppic-01-permit-env-cls-6{stroke:#fff;stroke-width:1.5px;}.ppic-01-permit-env-cls-3{stroke-linecap:round;stroke-linejoin:round;}.ppic-01-permit-env-cls-4{fill:#b6b6b6;mix-blend-mode:multiply;}.ppic-01-permit-env-cls-5,.ppic-01-permit-env-cls-6{stroke-miterlimit:10;}'
                )
            ),
            createElement(
                'g',
                { className: 'ppic-01-permit-env-cls-1' },
                createElement(
                    'g',
                    {
                        id: 'ppic-01-permit-env-Calque_2',
                        'data-name': 'Calque 2',
                    },
                    createElement(
                        'g',
                        {
                            id: 'ppic-01-permit-env-Calque_3',
                            'data-name': 'Calque 3',
                        },
                        createElement('rect', {
                            className: 'ppic-01-permit-env-cls-2',
                            x: '3.33',
                            y: '15.85',
                            width: '35',
                            height: '46.19',
                        }),
                        createElement('line', {
                            className: 'ppic-01-permit-env-cls-3',
                            x1: '8.14',
                            y1: '27.81',
                            x2: '33.76',
                            y2: '27.81',
                        }),
                        createElement('line', {
                            className: 'ppic-01-permit-env-cls-3',
                            x1: '8.14',
                            y1: '36.31',
                            x2: '33.76',
                            y2: '36.31',
                        }),
                        createElement('line', {
                            className: 'ppic-01-permit-env-cls-3',
                            x1: '8.14',
                            y1: '40.21',
                            x2: '33.76',
                            y2: '40.21',
                        }),
                        createElement('line', {
                            className: 'ppic-01-permit-env-cls-3',
                            x1: '8.14',
                            y1: '44.12',
                            x2: '33.76',
                            y2: '44.12',
                        }),
                        createElement('line', {
                            className: 'ppic-01-permit-env-cls-3',
                            x1: '8.14',
                            y1: '48.02',
                            x2: '33.76',
                            y2: '48.02',
                        }),
                        createElement('line', {
                            className: 'ppic-01-permit-env-cls-3',
                            x1: '8.14',
                            y1: '51.92',
                            x2: '33.76',
                            y2: '51.92',
                        }),
                        createElement('rect', {
                            className: 'ppic-01-permit-env-cls-4',
                            x: '-0.85',
                            y: '16.74',
                            width: '13.18',
                            height: '3.04',
                            transform: 'translate(-11.23 9.4) rotate(-45)',
                        }),
                        createElement('rect', {
                            className: 'ppic-01-permit-env-cls-4',
                            x: '29.33',
                            y: '16.74',
                            width: '13.18',
                            height: '3.04',
                            transform: 'translate(48.4 56.57) rotate(-135)',
                        }),
                        createElement('rect', {
                            className: 'ppic-01-permit-env-cls-4',
                            x: '-0.85',
                            y: '58.11',
                            width: '13.18',
                            height: '3.04',
                            transform: 'translate(43.85 13.41) rotate(45)',
                        }),
                        createElement('rect', {
                            className: 'ppic-01-permit-env-cls-4',
                            x: '29.33',
                            y: '58.11',
                            width: '13.18',
                            height: '3.04',
                            transform: 'translate(103.48 76.4) rotate(135)',
                        }),
                        createElement('path', {
                            className: 'ppic-01-permit-env-cls-5',
                            d: 'M26.41,14.05v7.43c0,.28.62.69,1.61.69s1.64-.41,1.64-.69V14.05',
                        }),
                        createElement('path', {
                            className: 'ppic-01-permit-env-cls-2',
                            d: 'M34.24,9.83A4.07,4.07,0,0,0,31.7,4a3.69,3.69,0,0,0-7.34,0,4.1,4.1,0,0,0-2.54,5.81A4.12,4.12,0,0,0,28,15.25a4.13,4.13,0,0,0,7.17-2.81A4.06,4.06,0,0,0,34.24,9.83Z',
                        }),
                        createElement('path', {
                            className: 'ppic-01-permit-env-cls-6',
                            d: 'M34.24,9.83A4.07,4.07,0,0,0,31.7,4a3.69,3.69,0,0,0-7.34,0,4.1,4.1,0,0,0-2.54,5.81A4.12,4.12,0,0,0,28,15.25a4.13,4.13,0,0,0,7.17-2.81A4.06,4.06,0,0,0,34.24,9.83Z',
                        })
                    )
                )
            )
        ),
    'pic-arrow-a': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 41.62 5.96', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement('style', {}, '.ppic-arrow-a-cls-1{fill:#cee8da;}')
            ),
            createElement(
                'g',
                { id: 'ppic-arrow-a-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    { id: 'ppic-arrow-a-Calque_2-2', 'data-name': 'Calque 2' },
                    createElement('polygon', {
                        className: 'ppic-arrow-a-cls-1',
                        points: '33.7 2.36 33.7 0 7.92 0 7.92 2.36 0 2.36 20.81 5.96 41.62 2.36 33.7 2.36',
                    })
                )
            )
        ),
    'pic-triangle': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 32.3 29.26', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-triangle-cls-1{fill:none;stroke:#00799c;stroke-miterlimit:10;stroke-width:3px;}.ppic-triangle-cls-2{fill:#00799c;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-triangle-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    { id: 'ppic-triangle-Calque_3', 'data-name': 'Calque 3' },
                    createElement('path', {
                        className: 'ppic-triangle-cls-1',
                        d: 'M13.27,3.16,2,22.76a3.33,3.33,0,0,0,2.88,5H27.46a3.33,3.33,0,0,0,2.89-5L19,3.16A3.33,3.33,0,0,0,13.27,3.16Z',
                    }),
                    createElement('path', {
                        className: 'ppic-triangle-cls-2',
                        d: 'M14.24,21.49a1.9,1.9,0,1,1,1.9,2A1.9,1.9,0,0,1,14.24,21.49Zm.43-9.77-.11-2.84h3.15l-.1,2.84-.44,6.54H15.1Z',
                    })
                )
            )
        ),
    'pic-05-ppas': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            {
                viewBox: '0 0 64.66 54.27',
                version: '1.1',
                id: 'ppic-05-ppas-svg26',
                docname: '05-ppas.svg',
                preserveAspectRatio,
            },
            createElement('namedview', {
                id: 'ppic-05-ppas-namedview28',
                pagecolor: '#ffffff',
                bordercolor: '#666666',
                borderopacity: '1.0',
                pageshadow: '2',
                pageopacity: '0.0',
                pagecheckerboard: '0',
                showgrid: 'false',
                zoom: '16.086235',
                cx: '32.325773',
                cy: '27.135',
                'window-width': '2075',
                'window-height': '1376',
                'window-x': '0',
                'window-y': '27',
                'window-maximized': '0',
                'current-layer': 'Calque_2-2',
            }),
            createElement(
                'defs',
                { id: 'ppic-05-ppas-defs4' },
                createElement(
                    'style',
                    { id: 'ppic-05-ppas-style2' },
                    '.ppic-05-ppas-cls-1{fill:#d9d9d9;}.ppic-05-ppas-cls-2{fill:#289084;}.ppic-05-ppas-cls-2,.ppic-05-ppas-cls-3{stroke:#fff;stroke-miterlimit:10;stroke-width:1.5px;}.ppic-05-ppas-cls-3{fill:none;stroke-linecap:round;}#ellipse6{ fill:#d9d9d9;fill-opacity:0 }'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-05-ppas-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    { id: 'ppic-05-ppas-Calque_2-2', 'data-name': 'Calque 2' },
                    createElement('ellipse', {
                        className: 'ppic-05-ppas-cls-1',
                        cx: '32.330002',
                        cy: '33.48',
                        rx: '32.330002',
                        ry: '20.780001',
                        id: 'ellipse6',
                    }),
                    createElement('path', {
                        className: 'ppic-05-ppas-cls-2',
                        d: 'M51.16,14.48V7.66H7.45V40.11H51.16c3.34,0,6.05-2,6.05-4.58v-21Z',
                        id: 'ppic-05-ppas-path8',
                    }),
                    createElement('path', {
                        className: 'ppic-05-ppas-cls-2',
                        d: 'M45.12,35.53c0-2.53,2.71-4.58,6-4.58s6.05,2.05,6.05,4.58V5.33C57.21,2.8,54.5.75,51.16.75s-6,2.05-6,4.58Z',
                        id: 'ppic-05-ppas-path10',
                    }),
                    createElement('polyline', {
                        className: 'ppic-05-ppas-cls-3',
                        points: '32.15 36.32 34.74 29.41 39.19 21.83 44.84 11.27',
                        id: 'ppic-05-ppas-polyline12',
                    }),
                    createElement('polyline', {
                        className: 'ppic-05-ppas-cls-3',
                        points: '42.23 36.32 34.74 29.41 20.65 25.02 14.54 29.74',
                        id: 'ppic-05-ppas-polyline14',
                    }),
                    createElement('polyline', {
                        className: 'ppic-05-ppas-cls-3',
                        points: '24.71 36.32 20.65 25.02 27.28 16.02 19.59 11.13',
                        id: 'ppic-05-ppas-polyline16',
                    }),
                    createElement('polyline', {
                        className: 'ppic-05-ppas-cls-3',
                        points: '44.84 22.36 39.19 21.83 27.28 16.02',
                        id: 'ppic-05-ppas-polyline18',
                    }),
                    createElement('polyline', {
                        className: 'ppic-05-ppas-cls-3',
                        points: '14.54 16.9 29.04 23.75 34.14 19.18 32.91 11.13',
                        id: 'ppic-05-ppas-polyline20',
                    }),
                    createElement('line', {
                        className: 'ppic-05-ppas-cls-3',
                        x1: '29.04',
                        y1: '23.75',
                        x2: '34.74',
                        y2: '29.41',
                        id: 'ppic-05-ppas-line22',
                    })
                )
            )
        ),
    'pic-02-permit-urb': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 41.65 52.84', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-02-permit-urb-cls-1{isolation:isolate;}.ppic-02-permit-urb-cls-2{fill:#e30613;}.ppic-02-permit-urb-cls-3{fill:none;stroke:#fff;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.5px;}.ppic-02-permit-urb-cls-4{fill:#b6b6b6;mix-blend-mode:multiply;}'
                )
            ),
            createElement(
                'g',
                { className: 'ppic-02-permit-urb-cls-1' },
                createElement(
                    'g',
                    {
                        id: 'ppic-02-permit-urb-Calque_2',
                        'data-name': 'Calque 2',
                    },
                    createElement(
                        'g',
                        {
                            id: 'ppic-02-permit-urb-Calque_2-2',
                            'data-name': 'Calque 2',
                        },
                        createElement('rect', {
                            className: 'ppic-02-permit-urb-cls-2',
                            x: '3.33',
                            y: '3.33',
                            width: '35',
                            height: '46.19',
                        }),
                        createElement('line', {
                            className: 'ppic-02-permit-urb-cls-3',
                            x1: '8.14',
                            y1: '15.28',
                            x2: '33.76',
                            y2: '15.28',
                        }),
                        createElement('line', {
                            className: 'ppic-02-permit-urb-cls-3',
                            x1: '8.14',
                            y1: '23.78',
                            x2: '33.76',
                            y2: '23.78',
                        }),
                        createElement('line', {
                            className: 'ppic-02-permit-urb-cls-3',
                            x1: '8.14',
                            y1: '27.69',
                            x2: '33.76',
                            y2: '27.69',
                        }),
                        createElement('line', {
                            className: 'ppic-02-permit-urb-cls-3',
                            x1: '8.14',
                            y1: '31.59',
                            x2: '33.76',
                            y2: '31.59',
                        }),
                        createElement('line', {
                            className: 'ppic-02-permit-urb-cls-3',
                            x1: '8.14',
                            y1: '35.49',
                            x2: '33.76',
                            y2: '35.49',
                        }),
                        createElement('line', {
                            className: 'ppic-02-permit-urb-cls-3',
                            x1: '8.14',
                            y1: '39.4',
                            x2: '33.76',
                            y2: '39.4',
                        }),
                        createElement('rect', {
                            className: 'ppic-02-permit-urb-cls-4',
                            x: '-0.85',
                            y: '4.21',
                            width: '13.18',
                            height: '3.04',
                            transform: 'translate(-2.38 5.73) rotate(-45)',
                        }),
                        createElement('rect', {
                            className: 'ppic-02-permit-urb-cls-4',
                            x: '29.33',
                            y: '4.21',
                            width: '13.18',
                            height: '3.04',
                            transform: 'translate(57.26 35.19) rotate(-135)',
                        }),
                        createElement('rect', {
                            className: 'ppic-02-permit-urb-cls-4',
                            x: '-0.85',
                            y: '45.58',
                            width: '13.18',
                            height: '3.04',
                            transform: 'translate(34.99 9.74) rotate(45)',
                        }),
                        createElement('rect', {
                            className: 'ppic-02-permit-urb-cls-4',
                            x: '29.33',
                            y: '45.58',
                            width: '13.18',
                            height: '3.04',
                            transform: 'translate(94.62 55.01) rotate(135)',
                        })
                    )
                )
            )
        ),
    'pic-00-env': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            {
                version: '1.1',
                docname: '03-rru.svg',
                x: '0px',
                y: '0px',
                viewBox: '0 0 64.7 54.3',
                id: 'svg16',
                space: 'preserve',
                preserveAspectRatio,
            },
            createElement(
                'style',
                { type: 'text/css' },
                '	.ppic-00-env-st0{fill:#D9D9D9;fill-opacity:0;}	.ppic-00-env-st1{fill:#289084;stroke:#FFFFFF;stroke-width:1.5;stroke-miterlimit:10;}	.ppic-00-env-st2{fill:#00799C;stroke:#FFFFFF;stroke-width:4;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}	.ppic-00-env-st3{fill:#FFFFFF;stroke:#FFFFFF;stroke-width:4;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}	.ppic-00-env-st4{fill:#00799C;}	.ppic-00-env-st5{fill:#FFFFFF;}#svg16{ enable-background:new 0 0 64.7 54.3; }'
            ),
            createElement(
                'namedview',
                {
                    bordercolor: '#666666',
                    borderopacity: '1.0',
                    id: 'ppic-00-env-namedview18',
                    'current-layer': 'Calque_2-2',
                    cx: '22.330286',
                    cy: '39.121958',
                    pagecheckerboard: '0',
                    pageopacity: '0.0',
                    pageshadow: '2',
                    'window-height': '1146',
                    'window-maximized': '0',
                    'window-width': '1971',
                    'window-x': '194',
                    'window-y': '97',
                    zoom: '5.6873431',
                    pagecolor: '#ffffff',
                    showgrid: 'false',
                },
                '\n	'
            ),
            createElement(
                'g',
                {
                    id: 'ppic-00-env-Calque_2_00000104684812667270930870000017957178391073312135_',
                },
                createElement(
                    'g',
                    { id: 'ppic-00-env-Calque_2-2' },
                    createElement('ellipse', {
                        id: 'ppic-00-env-ellipse6',
                        className: 'ppic-00-env-st0',
                        cx: '32.3',
                        cy: '33.5',
                        rx: '32.3',
                        ry: '20.8',
                    }),
                    createElement('path', {
                        id: 'ppic-00-env-path8',
                        className: 'ppic-00-env-st1',
                        d: 'M51.2,14.5V7.7H7.4v32.5h43.7c3.3,0,6-2,6-4.6v-21L51.2,14.5z',
                    }),
                    createElement('path', {
                        id: 'ppic-00-env-path10',
                        className: 'ppic-00-env-st1',
                        d: 'M45.1,35.5c0-2.5,2.7-4.6,6-4.6s6,2.1,6,4.6V5.3c0-2.5-2.7-4.6-6-4.6s-6,2-6,4.6L45.1,35.5z',
                    })
                )
            ),
            createElement(
                'g',
                {},
                createElement(
                    'g',
                    {},
                    createElement(
                        'g',
                        {},
                        createElement('path', {
                            className: 'ppic-00-env-st2',
                            d: 'M27.4,2.1c-3.8,5.6-6.3,9.1-6.3,12.5c0,3.4,2.8,6.1,6.3,6.1c3.5,0,6.3-2.8,6.3-6.1     C33.6,11.2,31.2,7.7,27.4,2.1z',
                        })
                    )
                ),
                createElement('path', {
                    className: 'ppic-00-env-st3',
                    d: 'M27.3,6.1c0.4,0.6,0.8,1.5,0.8,2.6c0,3-3.1,3.5-3.1,1.7C25,9.1,26.6,7.2,27.3,6.1z',
                })
            ),
            createElement(
                'g',
                {},
                createElement(
                    'g',
                    {},
                    createElement(
                        'g',
                        {},
                        createElement('path', {
                            className: 'ppic-00-env-st4',
                            d: 'M27.4,2.1c-3.8,5.6-6.3,9.1-6.3,12.5c0,3.4,2.8,6.1,6.3,6.1c3.5,0,6.3-2.8,6.3-6.1     C33.6,11.2,31.2,7.7,27.4,2.1z',
                        })
                    )
                ),
                createElement('path', {
                    className: 'ppic-00-env-st5',
                    d: 'M27.3,6.1c0.4,0.6,0.8,1.5,0.8,2.6c0,3-3.1,3.5-3.1,1.7C25,9.1,26.6,7.2,27.3,6.1z',
                })
            )
        ),
    'pic-11-natura-2000': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 64.66 59.6', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-11-natura-2000-cls-1,.ppic-11-natura-2000-cls-2{fill:#289084;}.ppic-11-natura-2000-cls-2,.ppic-11-natura-2000-cls-3{stroke:#fff;stroke-miterlimit:10;stroke-width:1.5px;}.ppic-11-natura-2000-cls-3{fill:none;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-11-natura-2000-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    {
                        id: 'ppic-11-natura-2000-Calque_2-2',
                        'data-name': 'Calque 2',
                    },
                    createElement('ellipse', {
                        className: 'ppic-11-natura-2000-cls-1',
                        cx: '32.33',
                        cy: '38.82',
                        rx: '32.33',
                        ry: '20.78',
                    }),
                    createElement('path', {
                        className: 'ppic-11-natura-2000-cls-2',
                        d: 'M29,28.12V43.4c0,.59,1.29,1.42,3.33,1.42S35.7,44,35.7,43.4V28.12',
                    }),
                    createElement('path', {
                        className: 'ppic-11-natura-2000-cls-1',
                        d: 'M45.16,19.41a8.49,8.49,0,0,0-5.23-12,7.64,7.64,0,0,0-15.16,0,8.49,8.49,0,0,0-5.23,12A8.51,8.51,0,0,0,32.35,30.6,8.51,8.51,0,0,0,45.16,19.41Z',
                    }),
                    createElement('path', {
                        className: 'ppic-11-natura-2000-cls-3',
                        d: 'M45.16,19.41a8.49,8.49,0,0,0-5.23-12,7.64,7.64,0,0,0-15.16,0,8.49,8.49,0,0,0-5.23,12A8.51,8.51,0,0,0,32.35,30.6,8.51,8.51,0,0,0,45.16,19.41Z',
                    })
                )
            )
        ),
    'pic-09-pump': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 64.66 46.34', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-09-pump-cls-1,.ppic-09-pump-cls-2{fill:#289084;}.ppic-09-pump-cls-2{stroke:#fff;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.5px;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-09-pump-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    { id: 'ppic-09-pump-Calque_2-2', 'data-name': 'Calque 2' },
                    createElement('ellipse', {
                        className: 'ppic-09-pump-cls-1',
                        cx: '32.33',
                        cy: '25.56',
                        rx: '32.33',
                        ry: '20.78',
                    }),
                    createElement('polygon', {
                        className: 'ppic-09-pump-cls-2',
                        points: '38.6 33.73 17.71 28.94 17.71 12.3 38.6 17.09 38.6 33.73',
                    }),
                    createElement('polygon', {
                        className: 'ppic-09-pump-cls-2',
                        points: '38.6 17.09 17.71 12.3 26.06 0.75 46.95 5.54 38.6 17.09',
                    }),
                    createElement('polygon', {
                        className: 'ppic-09-pump-cls-2',
                        points: '38.6 17.09 38.6 33.73 46.95 23.97 46.95 5.54 38.6 17.09',
                    }),
                    createElement('path', {
                        className: 'ppic-09-pump-cls-2',
                        d: 'M53.22,18.87a2.05,2.05,0,0,0-1.39-.63L45.62,17a2.06,2.06,0,0,0-.84,4l4.9,1v7.46a2.06,2.06,0,1,0,4.12,0V20.76A2,2,0,0,0,53.22,18.87Z',
                    })
                )
            )
        ),
    'pic-checkbox-checked': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 42.84 37.81', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-checkbox-checked-cls-1{fill:#fff;stroke:#000;}.ppic-checkbox-checked-cls-2{fill:none;stroke:#289084;stroke-miterlimit:10;stroke-width:8px;}'
                )
            ),
            createElement(
                'g',
                {
                    id: 'ppic-checkbox-checked-Calque_2',
                    'data-name': 'Calque 2',
                },
                createElement(
                    'g',
                    {
                        id: 'ppic-checkbox-checked-Calque_2-2',
                        'data-name': 'Calque 2',
                    },
                    createElement('rect', {
                        className: 'ppic-checkbox-checked-cls-1',
                        x: '0.5',
                        y: '5.31',
                        width: '32',
                        height: '32',
                        rx: '4.5',
                    }),
                    createElement('path', {
                        className: 'ppic-checkbox-checked-cls-2',
                        d: 'M9,13.43,19.25,23.81,40,2.81',
                    })
                )
            )
        ),
    'pic-shadow': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 64.66 41.57', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement('style', {}, '.ppic-shadow-cls-1{fill:#d9d9d9;}')
            ),
            createElement(
                'g',
                { id: 'ppic-shadow-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    { id: 'ppic-shadow-Calque_2-2', 'data-name': 'Calque 2' },
                    createElement('ellipse', {
                        className: 'ppic-shadow-cls-1',
                        cx: '32.33',
                        cy: '20.78',
                        rx: '32.33',
                        ry: '20.78',
                    })
                )
            )
        ),
    'pic-drop-white': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 11.59 17.15', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-drop-white-cls-1{fill:#fff;}.ppic-drop-white-cls-2{fill:#00799c;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-drop-white-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    {
                        id: 'ppic-drop-white-Calque_2-2',
                        'data-name': 'Calque 2',
                    },
                    createElement('path', {
                        className: 'ppic-drop-white-cls-1',
                        d: 'M5.79,0C2.27,5.14,0,8.36,0,11.49a5.72,5.72,0,0,0,5.79,5.66,5.72,5.72,0,0,0,5.8-5.66C11.59,8.36,9.32,5.14,5.79,0Z',
                    }),
                    createElement('path', {
                        className: 'ppic-drop-white-cls-2',
                        d: 'M5.77,3.66A4.59,4.59,0,0,1,6.52,6c0,2.79-2.9,3.25-2.9,1.55C3.62,6.42,5.06,4.69,5.77,3.66Z',
                    })
                )
            )
        ),
    'pic-arrow-c': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 102.15 14.59', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement('style', {}, '.ppic-arrow-c-cls-1{fill:#cee8da;}')
            ),
            createElement(
                'g',
                { id: 'ppic-arrow-c-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    { id: 'ppic-arrow-c-Calque_2-2', 'data-name': 'Calque 2' },
                    createElement('polygon', {
                        className: 'ppic-arrow-c-cls-1',
                        points: '82.72 5.77 82.72 0 19.43 0 19.43 5.77 0 5.77 51.07 14.59 102.15 5.77 82.72 5.77',
                    })
                )
            )
        ),
    'pic-08-cat': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 64.66 41.57', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-08-cat-cls-1{fill:#289084;}.ppic-08-cat-cls-2,.ppic-08-cat-cls-3,.ppic-08-cat-cls-4,.ppic-08-cat-cls-5{fill:none;stroke:#fff;stroke-miterlimit:10;}.ppic-08-cat-cls-2,.ppic-08-cat-cls-3,.ppic-08-cat-cls-4{stroke-width:1.5px;}.ppic-08-cat-cls-3{stroke-dasharray:3.15 2.1;}.ppic-08-cat-cls-4{stroke-dasharray:2.91 1.94;}.ppic-08-cat-cls-5{stroke-width:0.5px;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-08-cat-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    { id: 'ppic-08-cat-Calque_2-2', 'data-name': 'Calque 2' },
                    createElement('ellipse', {
                        className: 'ppic-08-cat-cls-1',
                        cx: '32.33',
                        cy: '20.78',
                        rx: '32.33',
                        ry: '20.78',
                    }),
                    createElement('polyline', {
                        className: 'ppic-08-cat-cls-2',
                        points: '43.92 33.52 43.04 34.74 41.58 34.4',
                    }),
                    createElement('line', {
                        className: 'ppic-08-cat-cls-3',
                        x1: '39.53',
                        y1: '33.93',
                        x2: '9.84',
                        y2: '27.13',
                    }),
                    createElement('polyline', {
                        className: 'ppic-08-cat-cls-2',
                        points: '8.82 26.89 7.36 26.56 8.24 25.34',
                    }),
                    createElement('line', {
                        className: 'ppic-08-cat-cls-4',
                        x1: '9.38',
                        y1: '23.77',
                        x2: '20.18',
                        y2: '8.83',
                    }),
                    createElement('polyline', {
                        className: 'ppic-08-cat-cls-2',
                        points: '20.75 8.05 21.62 6.83 23.09 7.17',
                    }),
                    createElement('line', {
                        className: 'ppic-08-cat-cls-3',
                        x1: '25.13',
                        y1: '7.64',
                        x2: '54.82',
                        y2: '14.44',
                    }),
                    createElement('polyline', {
                        className: 'ppic-08-cat-cls-2',
                        points: '55.84 14.67 57.3 15.01 56.42 16.23',
                    }),
                    createElement('line', {
                        className: 'ppic-08-cat-cls-4',
                        x1: '55.29',
                        y1: '17.8',
                        x2: '44.49',
                        y2: '32.74',
                    }),
                    createElement('line', {
                        className: 'ppic-08-cat-cls-5',
                        x1: '18.42',
                        y1: '11.26',
                        x2: '29.47',
                        y2: '8.63',
                    }),
                    createElement('line', {
                        className: 'ppic-08-cat-cls-5',
                        x1: '15.22',
                        y1: '15.69',
                        x2: '37.32',
                        y2: '10.43',
                    }),
                    createElement('line', {
                        className: 'ppic-08-cat-cls-5',
                        x1: '12.01',
                        y1: '20.12',
                        x2: '45.17',
                        y2: '12.23',
                    }),
                    createElement('line', {
                        className: 'ppic-08-cat-cls-5',
                        x1: '8.81',
                        y1: '24.55',
                        x2: '53.01',
                        y2: '14.03',
                    }),
                    createElement('line', {
                        className: 'ppic-08-cat-cls-5',
                        x1: '11.65',
                        y1: '27.54',
                        x2: '55.85',
                        y2: '17.02',
                    }),
                    createElement('line', {
                        className: 'ppic-08-cat-cls-5',
                        x1: '19.5',
                        y1: '29.34',
                        x2: '52.65',
                        y2: '21.45',
                    }),
                    createElement('line', {
                        className: 'ppic-08-cat-cls-5',
                        x1: '27.34',
                        y1: '31.14',
                        x2: '49.44',
                        y2: '25.88',
                    }),
                    createElement('line', {
                        className: 'ppic-08-cat-cls-5',
                        x1: '35.19',
                        y1: '32.94',
                        x2: '46.24',
                        y2: '30.31',
                    })
                )
            )
        ),
    'pic-arrow-b': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 60.34 10.26', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement('style', {}, '.ppic-arrow-b-cls-1{fill:#cee8da;}')
            ),
            createElement(
                'g',
                { id: 'ppic-arrow-b-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    { id: 'ppic-arrow-b-Calque_2-2', 'data-name': 'Calque 2' },
                    createElement('polygon', {
                        className: 'ppic-arrow-b-cls-1',
                        points: '49.68 5.05 49.68 0 10.66 0 10.66 5.05 0 5.05 30.17 10.26 60.34 5.05 49.68 5.05',
                    })
                )
            )
        ),
    'pic-plouf': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            {
                version: '1.1',
                x: '0px',
                y: '0px',
                viewBox: '0 0 65.3 55.5',
                id: 'Calque_1',
                space: 'preserve',
                preserveAspectRatio,
            },
            createElement(
                'style',
                { type: 'text/css' },
                '	.ppic-plouf-st0{fill:#289084;}	.ppic-plouf-st1{fill:#00799C;}	.ppic-plouf-st2{fill:#FFFFFF;}#Calque_1{ enable-background:new 0 0 65.3 55.5; }'
            ),
            createElement(
                'g',
                {},
                createElement(
                    'g',
                    {},
                    createElement(
                        'g',
                        {},
                        createElement('path', {
                            className: 'ppic-plouf-st0',
                            d: 'M23.2,14.3c-0.1,0-0.1,0-0.2,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c-0.5,0.1-0.9,0.6-0.8,1.2     c0.1,0.5,0.5,0.8,1,0.8c0.1,0,0.1,0,0.2,0l0.1,0c0.5-0.1,0.9-0.6,0.8-1.2C24.1,14.7,23.7,14.3,23.2,14.3L23.2,14.3z M18.7,15.5     c-0.1,0-0.2,0-0.3,0c0,0,0,0,0,0c0,0,0,0,0,0c-0.5,0.2-0.8,0.7-0.7,1.3c0.1,0.4,0.5,0.7,1,0.7c0.1,0,0.2,0,0.3,0l0,0     c0.5-0.2,0.8-0.7,0.7-1.3C19.5,15.8,19.1,15.5,18.7,15.5L18.7,15.5z M14.3,17.2c-0.1,0-0.3,0-0.4,0.1c0,0,0,0,0,0c0,0,0,0,0,0     c0,0,0,0,0,0c-0.5,0.2-0.7,0.8-0.5,1.3c0.2,0.4,0.5,0.6,0.9,0.6c0.1,0,0.3,0,0.4-0.1l0,0c0.5-0.2,0.7-0.8,0.5-1.3     C15,17.4,14.6,17.2,14.3,17.2L14.3,17.2z M10.2,19.4c-0.2,0-0.4,0.1-0.5,0.2c0,0,0,0,0,0c0,0,0,0,0,0C9.1,19.9,9,20.5,9.3,21     c0.2,0.3,0.5,0.5,0.8,0.5c0.2,0,0.4-0.1,0.5-0.2l0,0c0.5-0.3,0.6-0.9,0.3-1.4C10.8,19.6,10.5,19.4,10.2,19.4L10.2,19.4z      M6.5,22.3c-0.2,0-0.5,0.1-0.7,0.3c0,0,0,0,0,0c0,0,0,0,0,0c-0.4,0.4-0.4,1,0,1.4c0.2,0.2,0.5,0.3,0.7,0.3c0.2,0,0.5-0.1,0.7-0.3     l0,0c0.4-0.4,0.4-1,0-1.4C7,22.4,6.8,22.3,6.5,22.3L6.5,22.3z M3.6,25.8c-0.3,0-0.6,0.2-0.8,0.5c0,0,0,0,0,0c0,0,0,0,0,0     c-0.3,0.5-0.2,1.1,0.3,1.4c0.2,0.1,0.4,0.2,0.5,0.2c0.3,0,0.7-0.2,0.8-0.5l0,0c0.3-0.5,0.2-1.1-0.3-1.4     C3.9,25.8,3.8,25.8,3.6,25.8L3.6,25.8z M1.7,29.9c-0.4,0-0.8,0.3-1,0.7c0,0,0,0,0,0c0,0,0,0,0,0c-0.1,0.5,0.2,1.1,0.7,1.2     c0.1,0,0.2,0,0.3,0c0.4,0,0.8-0.3,1-0.7l0,0c0.1-0.5-0.2-1.1-0.7-1.2C1.9,29.9,1.8,29.9,1.7,29.9L1.7,29.9z M1.3,34.3     C1.3,34.3,1.3,34.3,1.3,34.3c-0.6,0.1-1,0.5-1,1.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0     c0.1,0.5,0.5,0.9,1,0.9c0,0,0.1,0,0.1,0c0.5-0.1,1-0.5,0.9-1.1l0,0C2.3,34.7,1.8,34.3,1.3,34.3L1.3,34.3z M2.5,38.6     c-0.1,0-0.3,0-0.4,0.1C1.6,39,1.3,39.5,1.6,40c0,0,0,0,0,0c0,0,0,0,0,0c0.2,0.4,0.5,0.6,0.9,0.6c0.1,0,0.3,0,0.4-0.1     c0.5-0.2,0.7-0.8,0.5-1.3l0,0C3.2,38.9,2.9,38.6,2.5,38.6L2.5,38.6z M4.9,42.5c-0.2,0-0.5,0.1-0.6,0.2c-0.4,0.4-0.5,1-0.1,1.4     c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0.2,0.2,0.5,0.4,0.8,0.4c0.2,0,0.5-0.1,0.6-0.2     c0.4-0.4,0.5-1,0.1-1.4l0,0C5.5,42.6,5.2,42.5,4.9,42.5L4.9,42.5z M8.3,45.6c-0.3,0-0.6,0.1-0.8,0.4c-0.3,0.4-0.3,1.1,0.2,1.4     c0,0,0,0,0,0c0.2,0.1,0.4,0.2,0.6,0.2c0.3,0,0.6-0.1,0.8-0.4c0.3-0.4,0.3-1.1-0.2-1.4l0,0C8.7,45.7,8.5,45.6,8.3,45.6L8.3,45.6z      M12.2,48.2c-0.4,0-0.7,0.2-0.9,0.5c-0.3,0.5-0.1,1.1,0.4,1.4c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0.2,0.1,0.3,0.1,0.5,0.1     c0.4,0,0.7-0.2,0.9-0.5c0.3-0.5,0.1-1.1-0.4-1.4l0,0C12.5,48.2,12.3,48.2,12.2,48.2L12.2,48.2z M16.4,50.1     c-0.4,0-0.8,0.2-0.9,0.6c-0.2,0.5,0.1,1.1,0.6,1.3c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0     c0.1,0,0.2,0.1,0.4,0.1c0.4,0,0.8-0.2,0.9-0.6c0.2-0.5-0.1-1.1-0.6-1.3l0,0C16.7,50.2,16.6,50.1,16.4,50.1L16.4,50.1z M20.9,51.6     c-0.4,0-0.9,0.3-1,0.8c-0.1,0.5,0.2,1.1,0.7,1.2c0,0,0,0,0.1,0c0,0,0,0,0,0c0.1,0,0.2,0,0.2,0c0.4,0,0.9-0.3,1-0.8     c0.1-0.5-0.2-1.1-0.7-1.2l-0.1,0C21.1,51.6,21,51.6,20.9,51.6L20.9,51.6z M25.5,52.5c-0.5,0-0.9,0.4-1,0.9     c-0.1,0.5,0.3,1.1,0.8,1.1c0,0,0,0,0.1,0c0,0,0.1,0,0.1,0c0.5,0,0.9-0.4,1-0.9c0.1-0.5-0.3-1.1-0.8-1.1l-0.1,0     C25.6,52.5,25.6,52.5,25.5,52.5L25.5,52.5z M30.3,52.9c-0.5,0-1,0.4-1,1c0,0.6,0.4,1,1,1c0,0,0,0,0,0c0,0,0,0,0,0h0c0,0,0,0,0,0     c0,0,0,0,0,0c0.5,0,1-0.4,1-1c0-0.6-0.4-1-1-1L30.3,52.9C30.3,52.9,30.3,52.9,30.3,52.9L30.3,52.9z M35,52.9     C35,52.9,35,52.9,35,52.9l-0.1,0c-0.6,0-1,0.5-1,1c0,0.5,0.5,1,1,1c0,0,0,0,0,0c0,0,0,0,0.1,0c0.6,0,1-0.5,1-1     C36,53.4,35.6,52.9,35,52.9L35,52.9z M39.7,52.5c0,0-0.1,0-0.1,0l-0.1,0c-0.5,0.1-0.9,0.6-0.8,1.1c0.1,0.5,0.5,0.9,1,0.9     c0,0,0.1,0,0.1,0c0,0,0,0,0.1,0c0,0,0,0,0,0c0.5-0.1,0.9-0.6,0.8-1.1C40.7,52.8,40.2,52.5,39.7,52.5L39.7,52.5z M44.4,51.5     c-0.1,0-0.2,0-0.2,0l-0.1,0c-0.5,0.1-0.9,0.7-0.7,1.2c0.1,0.5,0.5,0.8,1,0.8c0.1,0,0.2,0,0.2,0c0,0,0,0,0,0c0,0,0,0,0,0     c0.5-0.1,0.9-0.7,0.7-1.2C45.2,51.8,44.8,51.5,44.4,51.5L44.4,51.5z M48.8,50.1c-0.1,0-0.2,0-0.4,0.1l0,0     c-0.5,0.2-0.8,0.8-0.6,1.3c0.2,0.4,0.5,0.6,0.9,0.6c0.1,0,0.2,0,0.4-0.1c0,0,0,0,0.1,0c0.5-0.2,0.8-0.8,0.6-1.3     C49.6,50.4,49.2,50.1,48.8,50.1L48.8,50.1z M53.1,48.2c-0.2,0-0.3,0-0.5,0.1l0,0c-0.5,0.3-0.7,0.9-0.4,1.4     c0.2,0.3,0.5,0.5,0.9,0.5c0.2,0,0.3,0,0.5-0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0     c0.5-0.3,0.7-0.9,0.4-1.4C53.8,48.3,53.5,48.2,53.1,48.2L53.1,48.2z M57,45.6c-0.2,0-0.4,0.1-0.6,0.2l0,0c-0.4,0.3-0.5,1-0.2,1.4     c0.2,0.3,0.5,0.4,0.8,0.4c0.2,0,0.4-0.1,0.6-0.2c0,0,0,0,0,0c0.4-0.3,0.5-1,0.2-1.4C57.6,45.7,57.3,45.6,57,45.6L57,45.6z      M60.3,42.4c-0.3,0-0.6,0.1-0.8,0.4l0,0c-0.4,0.4-0.3,1.1,0.1,1.4c0.2,0.2,0.4,0.2,0.6,0.2c0.3,0,0.6-0.1,0.8-0.4c0,0,0,0,0,0     c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0.4-0.4,0.3-1.1-0.1-1.4C60.8,42.5,60.6,42.4,60.3,42.4L60.3,42.4z M62.8,38.6     c-0.4,0-0.7,0.2-0.9,0.6l0,0c-0.2,0.5,0,1.1,0.5,1.3c0.1,0.1,0.3,0.1,0.4,0.1c0.4,0,0.7-0.2,0.9-0.6c0,0,0,0,0-0.1c0,0,0,0,0,0     c0,0,0,0,0,0c0.2-0.5,0-1.1-0.5-1.3C63,38.6,62.9,38.6,62.8,38.6L62.8,38.6z M63.9,34.3c-0.5,0-1,0.4-1,0.9l0,0     c0,0.6,0.4,1,0.9,1.1c0,0,0.1,0,0.1,0c0.5,0,0.9-0.4,1-0.9c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0     C64.9,34.8,64.5,34.3,63.9,34.3C64,34.3,63.9,34.3,63.9,34.3L63.9,34.3z M63.5,29.8c-0.1,0-0.2,0-0.3,0c-0.5,0.1-0.8,0.7-0.7,1.2     l0,0c0.1,0.4,0.5,0.7,1,0.7c0.1,0,0.2,0,0.3,0c0.5-0.1,0.8-0.7,0.7-1.2c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0     c0,0,0,0,0,0C64.3,30.1,63.9,29.8,63.5,29.8L63.5,29.8z M61.6,25.7c-0.2,0-0.4,0.1-0.5,0.2c-0.5,0.3-0.6,0.9-0.3,1.4l0,0     c0.2,0.3,0.5,0.5,0.8,0.5c0.2,0,0.4-0.1,0.5-0.2c0.5-0.3,0.6-0.9,0.3-1.4c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0     c0,0,0,0,0,0c0,0,0,0,0,0C62.3,25.9,61.9,25.7,61.6,25.7L61.6,25.7z M58.7,22.2c-0.3,0-0.5,0.1-0.7,0.3c-0.4,0.4-0.4,1,0,1.4l0,0     c0.2,0.2,0.4,0.3,0.7,0.3c0.3,0,0.5-0.1,0.7-0.3c0.4-0.4,0.4-1,0-1.4c0,0,0,0,0,0C59.2,22.3,58.9,22.2,58.7,22.2L58.7,22.2z      M55,19.4c-0.3,0-0.6,0.2-0.8,0.5c-0.3,0.5-0.2,1.1,0.3,1.4l0,0c0.2,0.1,0.4,0.2,0.5,0.2c0.3,0,0.6-0.2,0.8-0.5     c0.3-0.5,0.2-1.1-0.3-1.4c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0C55.4,19.5,55.2,19.4,55,19.4L55,19.4z M50.9,17.2     c-0.4,0-0.7,0.2-0.9,0.6c-0.2,0.5,0,1.1,0.5,1.3l0,0c0.1,0.1,0.3,0.1,0.4,0.1c0.4,0,0.7-0.2,0.9-0.6c0.2-0.5,0-1.1-0.5-1.3     c0,0,0,0-0.1,0C51.2,17.2,51,17.2,50.9,17.2L50.9,17.2z M46.5,15.5c-0.4,0-0.8,0.3-1,0.7c-0.2,0.5,0.1,1.1,0.7,1.3l0,0     c0.1,0,0.2,0,0.3,0c0.4,0,0.8-0.3,1-0.7c0.2-0.5-0.1-1.1-0.7-1.3c0,0,0,0,0,0c0,0,0,0,0,0C46.7,15.5,46.6,15.5,46.5,15.5     L46.5,15.5z M41.9,14.3c-0.5,0-0.9,0.3-1,0.8c-0.1,0.5,0.2,1.1,0.8,1.2l0.1,0c0.1,0,0.1,0,0.2,0c0.5,0,0.9-0.3,1-0.8     c0.1-0.5-0.2-1.1-0.8-1.2c0,0,0,0,0,0c0,0,0,0,0,0C42.1,14.3,42,14.3,41.9,14.3L41.9,14.3z',
                        })
                    )
                ),
                createElement(
                    'g',
                    {},
                    createElement('path', {
                        className: 'ppic-plouf-st0',
                        d: 'M40,21.3C40,21.3,40,21.3,40,21.3c-0.5-0.1-1,0.2-1.2,0.7c-0.1,0.5,0.2,1,0.7,1.2c0.1,0,0.1,0,0.2,0    c0.4,0,0.9-0.3,0.9-0.8C40.9,22,40.6,21.5,40,21.3z',
                    }),
                    createElement('path', {
                        className: 'ppic-plouf-st0',
                        d: 'M16,25.4c-0.4,0.3-0.5,0.9-0.2,1.4c0.2,0.3,0.5,0.4,0.8,0.4c0.2,0,0.4-0.1,0.6-0.2c0.4-0.3,0.5-0.9,0.2-1.4    C17,25.1,16.4,25,16,25.4z',
                    }),
                    createElement('path', {
                        className: 'ppic-plouf-st0',
                        d: 'M35.1,20.6c-0.5,0-1,0.4-1,0.9c0,0.5,0.4,1,0.9,1H35c0.5,0,0.9-0.4,1-0.9C36,21.1,35.6,20.7,35.1,20.6z',
                    }),
                    createElement('path', {
                        className: 'ppic-plouf-st0',
                        d: 'M20.4,22.9c-0.5,0.2-0.7,0.8-0.5,1.3c0.1,0.4,0.5,0.6,0.9,0.6c0.1,0,0.3,0,0.4-0.1c0.5-0.2,0.7-0.8,0.5-1.3    C21.5,22.9,20.9,22.7,20.4,22.9z',
                    }),
                    createElement('path', {
                        className: 'ppic-plouf-st0',
                        d: 'M30.1,20.6c-0.5,0-0.9,0.5-0.9,1c0,0.5,0.5,0.9,1,0.9h0.1c0.5,0,0.9-0.5,0.9-1C31.1,21,30.6,20.6,30.1,20.6z',
                    }),
                    createElement('path', {
                        className: 'ppic-plouf-st0',
                        d: 'M25.2,21.3c-0.5,0.1-0.9,0.6-0.7,1.2c0.1,0.5,0.5,0.8,0.9,0.8c0.1,0,0.1,0,0.2,0c0.5-0.1,0.9-0.6,0.7-1.2    C26.2,21.6,25.7,21.2,25.2,21.3z',
                    }),
                    createElement('path', {
                        className: 'ppic-plouf-st0',
                        d: 'M47.9,25.6c-0.3,0.4-0.2,1,0.2,1.4c0.2,0.1,0.4,0.2,0.6,0.2c0.3,0,0.6-0.1,0.8-0.4c0.3-0.4,0.2-1-0.2-1.4    C48.8,25,48.2,25.1,47.9,25.6z',
                    }),
                    createElement('path', {
                        className: 'ppic-plouf-st0',
                        d: 'M52,30.6c0.2,0,0.4,0,0.5-0.2c0.5-0.3,0.6-0.9,0.3-1.3c-0.3-0.5-0.9-0.6-1.3-0.3c-0.5,0.3-0.6,0.9-0.3,1.3    C51.3,30.4,51.7,30.6,52,30.6z',
                    }),
                    createElement('path', {
                        className: 'ppic-plouf-st0',
                        d: 'M44.8,22.9c-0.5-0.2-1.1,0-1.3,0.5c-0.2,0.5,0,1.1,0.5,1.3v0c0.1,0.1,0.2,0.1,0.4,0.1c0.4,0,0.8-0.2,0.9-0.6    C45.5,23.6,45.3,23.1,44.8,22.9z',
                    }),
                    createElement('path', {
                        className: 'ppic-plouf-st0',
                        d: 'M13.8,28.8c-0.5-0.3-1-0.1-1.3,0.3c0,0,0,0,0,0c-0.3,0.5-0.2,1.1,0.3,1.4c0.2,0.1,0.3,0.1,0.5,0.1    c0.3,0,0.6-0.2,0.8-0.4C14.3,29.7,14.2,29.1,13.8,28.8z',
                    }),
                    createElement('path', {
                        className: 'ppic-plouf-st0',
                        d: 'M12.8,34.6c0-0.1,0.1-0.2,0.1-0.4c0-0.2-0.1-0.5-0.3-0.7c-0.4-0.4-1-0.4-1.4,0c-0.1,0.1-0.2,0.2-0.2,0.3    s-0.1,0.2-0.1,0.4s0,0.3,0.1,0.4c0,0.1,0.1,0.2,0.2,0.3c0.1,0.1,0.2,0.2,0.3,0.2c0.1,0.1,0.2,0.1,0.4,0.1c0.3,0,0.5-0.1,0.7-0.3    C12.6,34.8,12.7,34.7,12.8,34.6z',
                    }),
                    createElement('path', {
                        className: 'ppic-plouf-st0',
                        d: 'M15.8,41.7c-0.3,0.4-0.2,1,0.2,1.4c0.2,0.1,0.4,0.2,0.6,0.2c0.3,0,0.6-0.1,0.8-0.4c0.3-0.4,0.2-1-0.2-1.4    C16.7,41.2,16.1,41.3,15.8,41.7z',
                    }),
                    createElement('path', {
                        className: 'ppic-plouf-st0',
                        d: 'M21.2,43.7C21.2,43.7,21.2,43.7,21.2,43.7c-0.5-0.2-1.1,0-1.3,0.5s0,1.1,0.5,1.3c0.1,0,0.3,0.1,0.4,0.1    c0.4,0,0.7-0.2,0.9-0.6C21.9,44.5,21.7,44,21.2,43.7z',
                    }),
                    createElement('path', {
                        className: 'ppic-plouf-st0',
                        d: 'M12.7,38c-0.5,0.3-0.6,0.9-0.3,1.3c0.2,0.3,0.5,0.5,0.8,0.5c0.2,0,0.4,0,0.5-0.2c0.5-0.3,0.6-0.9,0.3-1.3    C13.8,37.8,13.2,37.7,12.7,38z',
                    }),
                    createElement('path', {
                        className: 'ppic-plouf-st0',
                        d: 'M25.6,45.2c-0.5-0.1-1.1,0.2-1.2,0.7c-0.1,0.5,0.2,1,0.7,1.2c0.1,0,0.1,0,0.2,0c0.4,0,0.8-0.3,0.9-0.8    C26.5,45.8,26.1,45.3,25.6,45.2z',
                    }),
                    createElement('path', {
                        className: 'ppic-plouf-st0',
                        d: 'M48.1,41.5c-0.4,0.3-0.5,0.9-0.2,1.4c0.2,0.2,0.5,0.4,0.8,0.4c0.2,0,0.4-0.1,0.6-0.2c0.4-0.3,0.5-0.9,0.2-1.4    C49.1,41.3,48.5,41.2,48.1,41.5z',
                    }),
                    createElement('path', {
                        className: 'ppic-plouf-st0',
                        d: 'M52.5,38c-0.4-0.3-1-0.1-1.3,0.3c-0.3,0.5-0.1,1.1,0.3,1.4c0.2,0.1,0.3,0.1,0.5,0.1c0.3,0,0.6-0.2,0.8-0.4    C53.1,38.9,53,38.3,52.5,38z',
                    }),
                    createElement('path', {
                        className: 'ppic-plouf-st0',
                        d: 'M30.3,45.8c-0.5,0-1,0.4-1,0.9c0,0.5,0.4,1,0.9,1h0.1c0.5,0,0.9-0.4,1-0.9C31.2,46.4,30.8,45.9,30.3,45.8z',
                    }),
                    createElement('path', {
                        className: 'ppic-plouf-st0',
                        d: 'M44,43.8c-0.5,0.2-0.7,0.8-0.5,1.3c0.2,0.4,0.5,0.6,0.9,0.6c0.1,0,0.2,0,0.4-0.1v0c0.5-0.2,0.7-0.8,0.5-1.3    C45.1,43.8,44.5,43.5,44,43.8z',
                    }),
                    createElement('path', {
                        className: 'ppic-plouf-st0',
                        d: 'M35,45.9C35,45.8,35,45.8,35,45.9c-0.5,0-0.9,0.5-0.9,1c0,0.5,0.5,0.9,1,0.9h0.1c0.5,0,0.9-0.5,0.9-1    C36,46.2,35.5,45.8,35,45.9z',
                    }),
                    createElement('path', {
                        className: 'ppic-plouf-st0',
                        d: 'M39.6,45.2c-0.5,0.1-0.9,0.6-0.7,1.2c0.1,0.5,0.5,0.8,1,0.8c0.1,0,0.1,0,0.2,0c0.5-0.1,0.9-0.6,0.7-1.2    C40.7,45.4,40.1,45.1,39.6,45.2z',
                    }),
                    createElement('path', {
                        className: 'ppic-plouf-st0',
                        d: 'M54.3,33.8c-0.1-0.1-0.1-0.2-0.2-0.3c-0.4-0.4-1-0.4-1.4,0c-0.2,0.2-0.3,0.4-0.3,0.7c0,0.1,0,0.3,0.1,0.4    c0.1,0.1,0.1,0.2,0.2,0.3c0.2,0.2,0.4,0.3,0.7,0.3c0.1,0,0.3,0,0.4-0.1c0.1,0,0.2-0.1,0.3-0.2c0.2-0.2,0.3-0.4,0.3-0.7    C54.4,34.1,54.3,34,54.3,33.8z',
                    })
                ),
                createElement(
                    'g',
                    {},
                    createElement(
                        'g',
                        {},
                        createElement('path', {
                            className: 'ppic-plouf-st0',
                            d: 'M32.6,27.7C32.6,27.7,32.6,27.7,32.6,27.7C32.6,27.7,32.6,27.7,32.6,27.7C32.5,27.7,32.5,27.7,32.6,27.7     c-0.6,0-1.1,0.5-1.1,1c0,0.6,0.5,1,1,1c0,0,0,0,0,0l0,0c0.6,0,1-0.4,1-1C33.6,28.2,33.1,27.7,32.6,27.7     C32.6,27.7,32.6,27.7,32.6,27.7L32.6,27.7z M28.2,28.4c-0.1,0-0.2,0-0.3,0.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0     c0,0,0,0,0,0c-0.5,0.2-0.8,0.7-0.6,1.3c0.1,0.4,0.5,0.7,0.9,0.7c0.1,0,0.2,0,0.3-0.1l0,0c0.5-0.2,0.8-0.7,0.6-1.3     C29,28.7,28.6,28.4,28.2,28.4L28.2,28.4z M24.5,30.7c-0.3,0-0.5,0.1-0.7,0.3c0,0,0,0,0,0l0,0c0,0,0,0,0,0c0,0,0,0,0,0     c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c-0.4,0.4-0.3,1,0.1,1.4c0.2,0.2,0.4,0.2,0.7,0.2c0.3,0,0.6-0.1,0.8-0.3l0,0     c0.4-0.4,0.3-1-0.1-1.4C25,30.8,24.8,30.7,24.5,30.7L24.5,30.7z M23.6,34.2c-0.1,0-0.2,0-0.3,0c-0.5,0.2-0.8,0.7-0.7,1.2     c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0.1,0.4,0.5,0.7,0.9,0.7     c0.1,0,0.2,0,0.3,0c0.5-0.2,0.8-0.7,0.6-1.3l0,0C24.4,34.5,24.1,34.2,23.6,34.2L23.6,34.2z M26.3,37.2c-0.3,0-0.7,0.2-0.9,0.5     c-0.3,0.5-0.1,1.1,0.4,1.4c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0.2,0.1,0.3,0.1,0.5,0.1c0.3,0,0.7-0.2,0.9-0.5     c0.3-0.5,0.1-1.1-0.4-1.4l0,0C26.6,37.2,26.5,37.2,26.3,37.2L26.3,37.2z M30.5,38.6c-0.5,0-0.9,0.4-1,0.9     c-0.1,0.5,0.3,1.1,0.8,1.1c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0.1,0,0.1,0c0.5,0,0.9-0.4,1-0.9c0.1-0.5-0.3-1.1-0.9-1.1l0,0     C30.6,38.6,30.5,38.6,30.5,38.6L30.5,38.6z M35,38.5c-0.1,0-0.1,0-0.2,0l0,0c-0.5,0.1-0.9,0.6-0.8,1.2c0.1,0.5,0.5,0.8,1,0.8     c0.1,0,0.1,0,0.2,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0     c0,0,0,0,0,0c0.5-0.1,0.9-0.6,0.8-1.2C35.9,38.8,35.5,38.5,35,38.5L35,38.5z M39.2,37c-0.2,0-0.4,0.1-0.5,0.2l0,0     c-0.5,0.3-0.6,0.9-0.3,1.4c0.2,0.3,0.5,0.5,0.8,0.5c0.2,0,0.4,0,0.5-0.2l0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0     c0.5-0.3,0.6-0.9,0.3-1.4C39.8,37.1,39.5,37,39.2,37L39.2,37z M41.6,33.9c-0.5,0-0.9,0.3-1,0.8l0,0c-0.1,0.5,0.2,1.1,0.8,1.2     c0.1,0,0.1,0,0.2,0c0.5,0,0.9-0.3,1-0.8c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0     c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0.1-0.5-0.2-1.1-0.8-1.2C41.7,33.9,41.7,33.9,41.6,33.9L41.6,33.9z M40.4,30.4     c-0.3,0-0.5,0.1-0.7,0.3c-0.4,0.4-0.4,1,0,1.4l0,0c0.2,0.2,0.5,0.3,0.7,0.3c0.3,0,0.5-0.1,0.7-0.3c0.4-0.4,0.4-1,0-1.4     c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0c0,0,0,0,0,0c0,0,0,0,0,0C40.9,30.5,40.6,30.4,40.4,30.4L40.4,30.4z      M36.6,28.3c-0.4,0-0.8,0.3-1,0.7c-0.2,0.5,0.1,1.1,0.7,1.3l0,0c0.1,0,0.2,0,0.3,0c0.4,0,0.8-0.3,1-0.7c0.2-0.5-0.1-1.1-0.7-1.2     c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0C36.8,28.3,36.7,28.3,36.6,28.3L36.6,28.3     z',
                        })
                    )
                ),
                createElement(
                    'g',
                    {},
                    createElement(
                        'g',
                        {},
                        createElement(
                            'g',
                            {},
                            createElement('path', {
                                className: 'ppic-plouf-st1',
                                d: 'M32.6,0.3c-3.8,5.6-6.3,9.1-6.3,12.5c0,3.4,2.8,6.1,6.3,6.1c3.5,0,6.3-2.8,6.3-6.1      C38.9,9.4,36.4,5.9,32.6,0.3z',
                            })
                        )
                    ),
                    createElement('path', {
                        className: 'ppic-plouf-st2',
                        d: 'M32.6,4.3c0.4,0.6,0.8,1.5,0.8,2.6c0,3-3.1,3.5-3.1,1.7C30.3,7.3,31.8,5.4,32.6,4.3z',
                    })
                )
            )
        ),
    'pic-07-dpc': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            {
                viewBox: '0 0 64.66 54.27',
                version: '1.1',
                id: 'ppic-07-dpc-svg46',
                docname: '07-dpc.svg',
                preserveAspectRatio,
            },
            createElement('namedview', {
                id: 'ppic-07-dpc-namedview48',
                pagecolor: '#ffffff',
                bordercolor: '#666666',
                borderopacity: '1.0',
                pageshadow: '2',
                pageopacity: '0.0',
                pagecheckerboard: '0',
                showgrid: 'false',
                zoom: '16.086235',
                cx: '32.356856',
                cy: '27.166083',
                'window-width': '1920',
                'window-height': '1043',
                'window-x': '798',
                'window-y': '1440',
                'window-maximized': '1',
                'current-layer': 'Calque_2-2',
            }),
            createElement(
                'defs',
                { id: 'ppic-07-dpc-defs4' },
                createElement(
                    'style',
                    { id: 'ppic-07-dpc-style2' },
                    '.ppic-07-dpc-cls-1{fill:#d9d9d9;}.ppic-07-dpc-cls-2{fill:#289084;stroke-miterlimit:10;}.ppic-07-dpc-cls-2,.ppic-07-dpc-cls-3{stroke:#fff;stroke-width:1.5px;}.ppic-07-dpc-cls-3{fill:none;stroke-linecap:round;stroke-linejoin:round;}.ppic-07-dpc-cls-4{fill:#fff;}.ppic-07-dpc-cls-5{fill:#75b626;}#ellipse6{ fill:#d9d9d9;fill-opacity:0.00799704 }'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-07-dpc-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    { id: 'ppic-07-dpc-Calque_2-2', 'data-name': 'Calque 2' },
                    createElement('ellipse', {
                        className: 'ppic-07-dpc-cls-1',
                        cx: '32.33',
                        cy: '33.48',
                        rx: '32.33',
                        ry: '20.78',
                        id: 'ellipse6',
                    }),
                    createElement('path', {
                        className: 'ppic-07-dpc-cls-2',
                        d: 'M51.16,14.48V7.66H7.45V40.11H51.16c3.34,0,6.05-2,6.05-4.58v-21Z',
                        id: 'ppic-07-dpc-path8',
                    }),
                    createElement('path', {
                        className: 'ppic-07-dpc-cls-2',
                        d: 'M45.12,35.53c0-2.53,2.71-4.58,6-4.58s6.05,2.05,6.05,4.58V5.33C57.21,2.8,54.5.75,51.16.75s-6,2.05-6,4.58Z',
                        id: 'ppic-07-dpc-path10',
                    }),
                    createElement('polygon', {
                        className: 'ppic-07-dpc-cls-3',
                        points: '22.37 17.13 25.82 15.28 29.53 16.07 32.27 13.6 34.39 15.63 34.66 18.55 33.86 20.67 37.48 22.96 38.72 27.47 37.04 27.56 36.16 29.24 39.34 31.27 30.24 35.78 30.06 34.54 24.85 33.48 23.79 30.48 22.55 30.21 23.08 28.27 20.78 29.06 17.34 27.38 18.66 25.09 20.96 24.64 21.31 21.82 20.34 20.67 22.11 18.55 22.37 17.13',
                        id: 'ppic-07-dpc-polygon12',
                    }),
                    createElement('path', {
                        className: 'ppic-07-dpc-cls-4',
                        d: 'M23.45,17.28a1,1,0,1,0,0-2V13.43a2.71,2.71,0,0,1,2.72,2.71c0,2.93-2.72,4.42-2.72,5.94',
                        id: 'ppic-07-dpc-path14',
                    }),
                    createElement('path', {
                        className: 'ppic-07-dpc-cls-4',
                        d: 'M23.45,22.08c0-1.52-2.72-3-2.72-5.94a2.71,2.71,0,0,1,2.72-2.71v1.86a1,1,0,1,0,0,2',
                        id: 'ppic-07-dpc-path16',
                    }),
                    createElement('circle', {
                        className: 'ppic-07-dpc-cls-5',
                        cx: '23.45',
                        cy: '16.31',
                        r: '1.19',
                        id: 'ppic-07-dpc-circle18',
                    }),
                    createElement('path', {
                        className: 'ppic-07-dpc-cls-4',
                        d: 'M30.16,14.69a1,1,0,0,0,0-2V10.83a2.71,2.71,0,0,1,2.71,2.72c0,2.93-2.71,4.41-2.71,5.93',
                        id: 'ppic-07-dpc-path20',
                    }),
                    createElement('path', {
                        className: 'ppic-07-dpc-cls-4',
                        d: 'M30.16,19.48c0-1.52-2.72-3-2.72-5.93a2.72,2.72,0,0,1,2.72-2.72V12.7a1,1,0,1,0,0,2',
                        id: 'ppic-07-dpc-path22',
                    }),
                    createElement('circle', {
                        className: 'ppic-07-dpc-cls-5',
                        cx: '30.16',
                        cy: '13.72',
                        r: '1.19',
                        id: 'ppic-07-dpc-circle24',
                    }),
                    createElement('path', {
                        className: 'ppic-07-dpc-cls-4',
                        d: 'M28.64,23.61a1,1,0,0,0,0-2V19.75a2.71,2.71,0,0,1,2.71,2.72c0,2.92-2.71,4.41-2.71,5.93',
                        id: 'ppic-07-dpc-path26',
                    }),
                    createElement('path', {
                        className: 'ppic-07-dpc-cls-4',
                        d: 'M28.64,28.4c0-1.52-2.72-3-2.72-5.93a2.72,2.72,0,0,1,2.72-2.72v1.87a1,1,0,1,0,0,2',
                        id: 'ppic-07-dpc-path28',
                    }),
                    createElement('circle', {
                        className: 'ppic-07-dpc-cls-5',
                        cx: '28.64',
                        cy: '22.64',
                        r: '1.19',
                        id: 'ppic-07-dpc-circle30',
                    }),
                    createElement('path', {
                        className: 'ppic-07-dpc-cls-4',
                        d: 'M34.26,27.38a1,1,0,1,0,0-2V23.52A2.72,2.72,0,0,1,37,26.24c0,2.92-2.72,4.41-2.72,5.93',
                        id: 'ppic-07-dpc-path32',
                    }),
                    createElement('path', {
                        className: 'ppic-07-dpc-cls-4',
                        d: 'M34.26,32.17c0-1.52-2.71-3-2.71-5.93a2.71,2.71,0,0,1,2.71-2.72v1.87a1,1,0,0,0,0,2',
                        id: 'ppic-07-dpc-path34',
                    }),
                    createElement('circle', {
                        className: 'ppic-07-dpc-cls-5',
                        cx: '34.26',
                        cy: '26.41',
                        r: '1.19',
                        id: 'ppic-07-dpc-circle36',
                    }),
                    createElement('path', {
                        className: 'ppic-07-dpc-cls-4',
                        d: 'M26.32,25.82a1,1,0,1,0,0-2V22A2.71,2.71,0,0,1,29,24.68c0,2.93-2.72,4.41-2.72,5.94',
                        id: 'ppic-07-dpc-path38',
                    }),
                    createElement('path', {
                        className: 'ppic-07-dpc-cls-4',
                        d: 'M26.32,30.62c0-1.53-2.71-3-2.71-5.94A2.71,2.71,0,0,1,26.32,22v1.86a1,1,0,0,0,0,2',
                        id: 'ppic-07-dpc-path40',
                    }),
                    createElement('circle', {
                        className: 'ppic-07-dpc-cls-5',
                        cx: '26.32',
                        cy: '24.85',
                        r: '1.19',
                        id: 'ppic-07-dpc-circle42',
                    })
                )
            )
        ),
    'pic-17-prime-regional': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            {
                id: 'ppic-17-prime-regional-Calque_2',
                viewBox: '0 0 64.66 52.82',
                preserveAspectRatio,
            },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-17-prime-regional-cls-1,.ppic-17-prime-regional-cls-2,.ppic-17-prime-regional-cls-3{stroke:#fff;}.ppic-17-prime-regional-cls-1,.ppic-17-prime-regional-cls-4{stroke-miterlimit:10;}.ppic-17-prime-regional-cls-1,.ppic-17-prime-regional-cls-3{stroke-width:1.5px;}.ppic-17-prime-regional-cls-1,.ppic-17-prime-regional-cls-3,.ppic-17-prime-regional-cls-5{fill:#289084;}.ppic-17-prime-regional-cls-2{stroke-width:.87px;}.ppic-17-prime-regional-cls-2,.ppic-17-prime-regional-cls-4{fill:#fff;}.ppic-17-prime-regional-cls-2,.ppic-17-prime-regional-cls-3{stroke-linecap:round;stroke-linejoin:round;}.ppic-17-prime-regional-cls-6{fill:#d9d9d9;}.ppic-17-prime-regional-cls-4{stroke:#289084;stroke-width:1.7px;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-17-prime-regional-Calque_2-2' },
                createElement(
                    'g',
                    {},
                    createElement(
                        'g',
                        {},
                        createElement('ellipse', {
                            className: 'ppic-17-prime-regional-cls-6',
                            cx: '32.33',
                            cy: '32.04',
                            rx: '32.33',
                            ry: '20.78',
                        }),
                        createElement(
                            'g',
                            {},
                            createElement('polygon', {
                                className: 'ppic-17-prime-regional-cls-1',
                                points: '39.8 11.91 39.8 3.42 17.75 3.42 17.75 43.88 47.34 43.88 47.34 11.91 39.8 11.91',
                            }),
                            createElement('polygon', {
                                className: 'ppic-17-prime-regional-cls-2',
                                points: '27.89 22.82 30.4 21.47 33.1 22.05 35.09 20.25 36.63 21.73 36.83 23.85 36.25 25.39 38.88 27.06 39.78 30.34 38.56 30.4 37.92 31.62 40.23 33.1 33.61 36.38 33.48 35.48 29.69 34.71 28.92 32.52 28.02 32.33 28.41 30.92 26.74 31.49 24.23 30.27 25.2 28.6 26.87 28.28 27.12 26.23 26.42 25.39 27.7 23.85 27.89 22.82',
                            })
                        )
                    ),
                    createElement('polygon', {
                        className: 'ppic-17-prime-regional-cls-3',
                        points: '39.8 3.42 47.34 11.91 39.8 11.91 39.8 3.42',
                    }),
                    createElement('circle', {
                        className: 'ppic-17-prime-regional-cls-4',
                        cx: '17.16',
                        cy: '10.48',
                        r: '9.63',
                    }),
                    createElement('path', {
                        className: 'ppic-17-prime-regional-cls-5',
                        d: 'M15.07,12.21c-.09,0-.12,.04-.12,.1,0,.04,.01,.09,.03,.12,.43,.93,1.15,1.41,2.44,1.41,2.41,0,2.19-1.75,3.39-1.75,.56,0,.9,.22,.9,.87,0,1.11-1.42,2.81-4.41,2.81-2.71,0-4.31-1.38-4.81-3.36-.03-.12-.06-.16-.19-.16h-.59c-.36,0-.43-.15-.43-.53,0-.47,.16-.53,.46-.53h.38c.12,0,.15-.03,.15-.19s-.02-.46-.02-.62c0-.12-.03-.18-.13-.18h-.25c-.35,0-.43-.15-.43-.53,0-.47,.16-.53,.46-.53h.34c.1,0,.18-.06,.21-.19,.09-.42,.28-.89,.5-1.29,.84-1.57,2.38-2.37,4.22-2.37,2.59,0,4.57,1.27,4.57,3.27,0,.64-.34,1.11-1.1,1.11-.67,0-.95-.43-1.26-1.04-.38-.75-.9-1.38-2.12-1.38-.99,0-1.58,.36-2.09,1.1-.13,.21-.28,.5-.28,.67,0,.07,.03,.1,.12,.1,.83-.01,1.76-.03,2.8-.03,.34,0,.53,.1,.53,.5s-.1,.58-.58,.58c-.13,0-.42-.01-2.28-.01h-.64c-.19,0-.22,.06-.22,.24,0,.19,0,.46,.01,.64,.01,.12,.06,.15,.19,.15,.77,0,1.67-.03,2.65-.03,.46,0,.53,.1,.53,.5s-.1,.56-.58,.56h-2.38Z',
                    })
                )
            )
        ),
    'pic-arrow-a-body': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 25.78 25.78', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-arrow-a-body-cls-1{fill:#cee8da;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-arrow-a-body-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    {
                        id: 'ppic-arrow-a-body-Calque_2-2',
                        'data-name': 'Calque 2',
                    },
                    createElement('rect', {
                        className: 'ppic-arrow-a-body-cls-1',
                        width: '25.78',
                        height: '25.78',
                    })
                )
            )
        ),
    'pic-arrow-b-to-b-left': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 129.65 69.08', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-arrow-b-to-b-left-cls-1{fill:#cee8da;}'
                )
            ),
            createElement(
                'g',
                {
                    id: 'ppic-arrow-b-to-b-left-Calque_2',
                    'data-name': 'Calque 2',
                },
                createElement(
                    'g',
                    {
                        id: 'ppic-arrow-b-to-b-left-Calque_2-2',
                        'data-name': 'Calque 2',
                    },
                    createElement('path', {
                        className: 'ppic-arrow-b-to-b-left-cls-1',
                        d: 'M129.65,0H90.81V30A10.64,10.64,0,0,1,80.17,40.6h-30c-30.11,0-43.61,8.92-43.61,23v1.78H0l21.52,3.72L43,65.36H36.27V63.53A10.65,10.65,0,0,1,46.92,52.88H76.84c36.46,0,52.81-8.92,52.81-23Z',
                    })
                )
            )
        ),
    'pic-test': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            {
                viewBox: '0 0 64.66 72.78',
                version: '1.1',
                id: 'ppic-test-svg40',
                preserveAspectRatio,
            },
            createElement(
                'defs',
                { id: 'ppic-test-defs4' },
                createElement(
                    'style',
                    { id: 'ppic-test-style2' },
                    '.ppic-test-cls-1{isolation:isolate;}.ppic-test-cls-2{fill:#d9d9d9;}.ppic-test-cls-3,.ppic-test-cls-6{fill:#289084;}.ppic-test-cls-4,.ppic-test-cls-7{fill:none;}.ppic-test-cls-4,.ppic-test-cls-6,.ppic-test-cls-7{stroke:#fff;stroke-width:1.5px;}.ppic-test-cls-4{stroke-linecap:round;stroke-linejoin:round;}.ppic-test-cls-5{fill:#b6b6b6;mix-blend-mode:multiply;}.ppic-test-cls-6,.ppic-test-cls-7{stroke-miterlimit:10;}'
                )
            ),
            createElement(
                'g',
                { className: 'ppic-test-cls-1', id: 'ppic-test-g38' },
                createElement(
                    'g',
                    { id: 'ppic-test-Calque_2', 'data-name': 'Calque 2' },
                    createElement(
                        'g',
                        { id: 'ppic-test-Calque_3', 'data-name': 'Calque 3' },
                        createElement('ellipse', {
                            className: 'ppic-test-cls-2',
                            cx: '32.33',
                            cy: '52',
                            rx: '32.33',
                            ry: '20.78',
                            id: 'ppic-test-ellipse6',
                        }),
                        createElement('rect', {
                            className: 'ppic-test-cls-3',
                            x: '14.83',
                            y: '15.85',
                            width: '35',
                            height: '46.19',
                            id: 'ppic-test-rect8',
                        }),
                        createElement('line', {
                            className: 'ppic-test-cls-4',
                            x1: '19.64',
                            y1: '27.81',
                            x2: '45.27',
                            y2: '27.81',
                            id: 'ppic-test-line10',
                        }),
                        createElement('line', {
                            className: 'ppic-test-cls-4',
                            x1: '19.64',
                            y1: '36.31',
                            x2: '45.27',
                            y2: '36.31',
                            id: 'ppic-test-line12',
                        }),
                        createElement('line', {
                            className: 'ppic-test-cls-4',
                            x1: '19.64',
                            y1: '40.21',
                            x2: '45.27',
                            y2: '40.21',
                            id: 'ppic-test-line14',
                        }),
                        createElement('line', {
                            className: 'ppic-test-cls-4',
                            x1: '19.64',
                            y1: '44.12',
                            x2: '45.27',
                            y2: '44.12',
                            id: 'ppic-test-line16',
                        }),
                        createElement('line', {
                            className: 'ppic-test-cls-4',
                            x1: '19.64',
                            y1: '48.02',
                            x2: '45.27',
                            y2: '48.02',
                            id: 'ppic-test-line18',
                        }),
                        createElement('line', {
                            className: 'ppic-test-cls-4',
                            x1: '19.64',
                            y1: '51.92',
                            x2: '45.27',
                            y2: '51.92',
                            id: 'ppic-test-line20',
                        }),
                        createElement('rect', {
                            className: 'ppic-test-cls-5',
                            x: '10.65',
                            y: '16.74',
                            width: '13.18',
                            height: '3.04',
                            transform: 'translate(-7.86 17.54) rotate(-45)',
                            id: 'ppic-test-rect22',
                        }),
                        createElement('rect', {
                            className: 'ppic-test-cls-5',
                            x: '40.83',
                            y: '16.74',
                            width: '13.18',
                            height: '3.04',
                            transform: 'translate(68.04 64.71) rotate(-135)',
                            id: 'ppic-test-rect24',
                        }),
                        createElement('rect', {
                            className: 'ppic-test-cls-5',
                            x: '10.65',
                            y: '58.11',
                            width: '13.18',
                            height: '3.04',
                            transform: 'translate(47.22 5.28) rotate(45)',
                            id: 'ppic-test-rect26',
                        }),
                        createElement('rect', {
                            className: 'ppic-test-cls-5',
                            x: '40.83',
                            y: '58.11',
                            width: '13.18',
                            height: '3.04',
                            transform: 'translate(123.12 68.26) rotate(135)',
                            id: 'ppic-test-rect28',
                        }),
                        createElement('path', {
                            className: 'ppic-test-cls-6',
                            d: 'M37.91,14.05v7.43c0,.28.63.69,1.62.69s1.64-.41,1.64-.69V14.05',
                            id: 'ppic-test-path30',
                        }),
                        createElement('path', {
                            className: 'ppic-test-cls-3',
                            d: 'M45.75,9.83A4.1,4.1,0,0,0,43.21,4a3.69,3.69,0,0,0-7.34,0,4.1,4.1,0,0,0-2.54,5.81,4.06,4.06,0,0,0-.95,2.61,4.13,4.13,0,0,0,7.16,2.81,4.12,4.12,0,0,0,6.21-5.42Z',
                            id: 'ppic-test-path32',
                        }),
                        createElement('path', {
                            className: 'ppic-test-cls-7',
                            d: 'M45.75,9.83A4.1,4.1,0,0,0,43.21,4a3.69,3.69,0,0,0-7.34,0,4.1,4.1,0,0,0-2.54,5.81,4.06,4.06,0,0,0-.95,2.61,4.13,4.13,0,0,0,7.16,2.81,4.12,4.12,0,0,0,6.21-5.42Z',
                            id: 'ppic-test-path34',
                        })
                    )
                )
            )
        ),
    'pic-16-facilitator': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            {
                viewBox: '0 0 64.66 72.779999',
                version: '1.1',
                id: 'ppic-16-facilitator-svg34',
                docname: '16-facilitator.svg',
                width: '64.660004',
                height: '72.779999',
                preserveAspectRatio,
            },
            createElement('namedview', {
                id: 'ppic-16-facilitator-namedview36',
                pagecolor: '#ffffff',
                bordercolor: '#666666',
                borderopacity: '1.0',
                pageshadow: '2',
                pageopacity: '0.0',
                pagecheckerboard: '0',
                showgrid: 'false',
                zoom: '3.6637569',
                cx: '-28.522634',
                cy: '74.377205',
                'window-width': '1720',
                'window-height': '1376',
                'window-x': '0',
                'window-y': '27',
                'window-maximized': '0',
                'current-layer': 'svg34',
            }),
            createElement(
                'defs',
                { id: 'ppic-16-facilitator-defs4' },
                createElement(
                    'style',
                    { id: 'ppic-16-facilitator-style2' },
                    '.ppic-16-facilitator-cls-1{fill:#d9d9d9;}.ppic-16-facilitator-cls-2,.ppic-16-facilitator-cls-4{fill:#289084;}.ppic-16-facilitator-cls-3{fill:#fff;}.ppic-16-facilitator-cls-4{stroke:#fff;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.2px;}'
                )
            ),
            createElement(
                'g',
                {
                    id: 'ppic-16-facilitator-Calque_2',
                    'data-name': 'Calque 2',
                    transform: 'translate(0,12.972797)',
                },
                createElement(
                    'g',
                    {
                        id: 'ppic-16-facilitator-Calque_3',
                        'data-name': 'Calque 3',
                    },
                    createElement('ellipse', {
                        className: 'ppic-16-facilitator-cls-1',
                        cx: '32.330002',
                        cy: '38.790001',
                        rx: '32.330002',
                        ry: '20.780001',
                        id: 'ppic-16-facilitator-ellipse6',
                    }),
                    createElement('path', {
                        className: 'ppic-16-facilitator-cls-2',
                        d: 'M 47.44,35.07 A 10.67,10.67 0 0 0 38.85,24.67 3.55,3.55 0 0 1 36,21.87 c 1.66,-1.95 2.79,-4.84 3.36,-8.61 a 0.82,0.82 0 0 0 0.26,-0.1 l 0.07,-0.05 0.16,-0.1 a 0.47,0.47 0 0 0 0.12,-0.09 l 0.05,-0.05 v 0 l 0.11,-0.1 a 1.54,1.54 0 0 0 0.38,-1.2 l -0.09,-1.19 v 0 a 1.09,1.09 0 0 0 0,-0.18 1.7,1.7 0 0 0 -0.86,-1.53 7.76,7.76 0 0 0 -3.58,-6 C 35.84,2.58 35.72,2.23 35.61,2.09 A 4,4 0 0 0 35.1,1.51 3.17,3.17 0 0 0 33.53,0.68 1.09,1.09 0 0 0 33.45,0.53 c 0,-0.06 -0.06,-0.1 -0.09,-0.15 A 1,1 0 0 0 32.59,0 h -3.17 a 1,1 0 0 0 -0.77,0.38 0.78,0.78 0 0 0 -0.1,0.15 l -0.06,0.14 -0.26,0.06 v 0 a 3.69,3.69 0 0 0 -2.1,1.78 l -0.08,0.14 a 7.78,7.78 0 0 0 -3.57,6 c -0.5,0.21 -0.77,0.73 -0.86,1.52 a 0.55,0.55 0 0 1 0,0.18 v 0.05 l -0.09,1.18 a 1.52,1.52 0 0 0 0.37,1.21 l 0.11,0.11 v 0 0 h 0.07 0.06 v 0 l 0.1,0.06 h 0.07 0.11 0.09 a 22.42,22.42 0 0 0 1.13,5 12.29,12.29 0 0 0 2.2,3.91 3.39,3.39 0 0 1 -1,1.64 3.49,3.49 0 0 1 -1.73,0.85 10.66,10.66 0 0 0 -8.61,10.41 z M 31.64,4.51 32.38,1.68 h 0.87 0.18 l -0.79,3.06 a 0.52,0.52 0 0 1 -0.5,0.38 H 32.01 A 0.51,0.51 0 0 1 31.64,4.5 Z M 28.77,1.68 h 0.86 l 0.74,2.83 a 0.5153882,0.5153882 0 0 1 -1,0.25 l -0.8,-3 h 0.19 z M 25.23,7.2 H 26 a 0.49,0.49 0 0 1 0.28,0.09 l 1.48,0.94 H 34.2 L 35.68,7.29 A 0.46,0.46 0 0 1 36,7.2 h 0.81 a 0.52,0.52 0 0 1 0,1 H 36.16 L 34.67,9.15 A 0.5,0.5 0 0 1 34.4,9.23 H 27.64 A 0.54,0.54 0 0 1 27.36,9.15 L 25.87,8.2 h -0.64 a 0.52,0.52 0 0 1 0,-1 z',
                        id: 'ppic-16-facilitator-path8',
                    }),
                    createElement('path', {
                        className: 'ppic-16-facilitator-cls-3',
                        d: 'm 23.66,13.37 h 0.65 l 1.9,0.81 a 1.15,1.15 0 0 0 0.41,0.08 h 8.78 a 1.1,1.1 0 0 0 0.4,-0.08 l 1.9,-0.81 h 0.61 c -1.62,10 -6.87,10.14 -7.16,10.14 H 31 c -5.57,0 -7,-7.39 -7.33,-10.14 z',
                        id: 'ppic-16-facilitator-path10',
                    }),
                    createElement('path', {
                        className: 'ppic-16-facilitator-cls-2',
                        d: 'm 29.31,20.19 a 0.5,0.5 0 0 0 0.11,0.71 l 0.2,0.15 v 0 a 2.49,2.49 0 0 0 1.35,0.41 2.43,2.43 0 0 0 1.34,-0.41 l 0.28,-0.2 v 0 A 0.52,0.52 0 0 0 32,20 l -0.28,0.2 v 0 a 1.38,1.38 0 0 1 -0.74,0.21 1.44,1.44 0 0 1 -0.75,-0.21 l -0.2,-0.14 a 0.5,0.5 0 0 0 -0.71,0.12 z',
                        id: 'ppic-16-facilitator-path12',
                    }),
                    createElement('path', {
                        className: 'ppic-16-facilitator-cls-2',
                        d: 'm 27.67,15.94 h 1.54 a 0.52,0.52 0 0 0 0,-1 h -1.54 a 0.52,0.52 0 1 0 0,1 z',
                        id: 'ppic-16-facilitator-path14',
                    }),
                    createElement('path', {
                        className: 'ppic-16-facilitator-cls-2',
                        d: 'm 30.49,18.76 h 1 a 0.52,0.52 0 0 0 0,-1 h -1 a 0.52,0.52 0 0 0 0,1 z',
                        id: 'ppic-16-facilitator-path16',
                    }),
                    createElement('path', {
                        className: 'ppic-16-facilitator-cls-2',
                        d: 'm 32.8,15.94 h 1.55 a 0.52,0.52 0 0 0 0,-1 H 32.8 a 0.52,0.52 0 0 0 0,1 z',
                        id: 'ppic-16-facilitator-path18',
                    }),
                    createElement('path', {
                        className: 'ppic-16-facilitator-cls-4',
                        d: 'M 26.21,45.25 H 15.44 A 2.33,2.33 0 0 1 13.27,43.46 L 10.36,29.76 A 1.41,1.41 0 0 1 11.77,28 h 10.76 a 2.35,2.35 0 0 1 2.18,1.8 l 2.91,13.7 a 1.4,1.4 0 0 1 -1.41,1.75 z',
                        id: 'ppic-16-facilitator-path20',
                    }),
                    createElement('polygon', {
                        className: 'ppic-16-facilitator-cls-4',
                        points: '14.9,27.2 18.81,27.2 18.51,29.11 14.6,29.11 ',
                        id: 'ppic-16-facilitator-polygon22',
                    }),
                    createElement('path', {
                        className: 'ppic-16-facilitator-cls-4',
                        d: 'm 10.74,38.06 c 0,-0.23 -0.21,-0.38 -0.29,-0.78 a 6.52,6.52 0 0 1 0.05,-0.86 c 0,-0.19 -0.2,-0.38 -0.27,-0.79 A 5.7,5.7 0 0 1 10.3,35 c 0,-0.16 -0.18,-0.38 -0.18,-0.7 a 0.54,0.54 0 0 1 0.58,-0.53 h 1.59 a 1,1 0 0 1 0.9,0.72 c 0.09,0.41 0.07,0.69 -0.33,0.69 H 13 a 1,1 0 0 1 0.9,0.73 c 0.09,0.4 0,0.64 -0.35,0.64 0.4,0 0.6,0.41 0.69,0.81 a 0.56,0.56 0 0 1 -0.55,0.73 h -0.37 c 0.4,0 0.35,0.33 0.45,0.73 0.1,0.4 -0.64,0.73 -0.89,0.73 h -1.26 a 1,1 0 0 1 -0.9,-0.73 c -0.07,-0.21 0.06,-0.53 0.02,-0.76 z',
                        id: 'ppic-16-facilitator-path24',
                    }),
                    createElement('line', {
                        className: 'ppic-16-facilitator-cls-4',
                        x1: '25.379999',
                        y1: '32.970001',
                        x2: '30.299999',
                        y2: '28.870001',
                        id: 'ppic-16-facilitator-line26',
                    }),
                    createElement('path', {
                        className: 'ppic-16-facilitator-cls-3',
                        d: 'm 18.53,34.26 c -0.8,2 -1.33,3.18 -1,4.37 a 3.07,3.07 0 0 0 2.8,2.15 1.6,1.6 0 0 0 1.61,-2.15 c -0.33,-1.19 -1.53,-2.41 -3.41,-4.37 z',
                        id: 'ppic-16-facilitator-path28',
                    }),
                    createElement('path', {
                        className: 'ppic-16-facilitator-cls-2',
                        d: 'm 18.9,35.65 a 2.32,2.32 0 0 1 0.54,0.9 c 0.29,1.07 -0.76,1.24 -0.94,0.59 -0.13,-0.44 0.24,-1.09 0.4,-1.49 z',
                        id: 'ppic-16-facilitator-path30',
                    })
                )
            )
        ),
    'pic-arrow-b-body': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 39.02 36.39', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-arrow-b-body-cls-1{fill:#cee8da;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-arrow-b-body-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    {
                        id: 'ppic-arrow-b-body-Calque_2-2',
                        'data-name': 'Calque 2',
                    },
                    createElement('rect', {
                        className: 'ppic-arrow-b-body-cls-1',
                        width: '39.02',
                        height: '36.39',
                    })
                )
            )
        ),
    'pic-circle-no': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 30.33 30.33', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-circle-no-cls-1{fill:none;stroke:#e30613;stroke-miterlimit:10;stroke-width:3px;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-circle-no-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    {
                        id: 'ppic-circle-no-Calque_2-2',
                        'data-name': 'Calque 2',
                    },
                    createElement('line', {
                        className: 'ppic-circle-no-cls-1',
                        x1: '9.99',
                        y1: '20.34',
                        x2: '20.34',
                        y2: '9.99',
                    }),
                    createElement('line', {
                        className: 'ppic-circle-no-cls-1',
                        x1: '20.34',
                        y1: '20.34',
                        x2: '9.99',
                        y2: '9.99',
                    }),
                    createElement('circle', {
                        className: 'ppic-circle-no-cls-1',
                        cx: '15.17',
                        cy: '15.17',
                        r: '13.67',
                    })
                )
            )
        ),
    'pic-arrow-b-tip': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 60.34 5.21', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-arrow-b-tip-cls-1{fill:#cee8da;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-arrow-b-tip-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    {
                        id: 'ppic-arrow-b-tip-Calque_2-2',
                        'data-name': 'Calque 2',
                    },
                    createElement('polygon', {
                        className: 'ppic-arrow-b-tip-cls-1',
                        points: '30.17 5.21 60.34 0 0 0 30.17 5.21',
                    })
                )
            )
        ),
    'pic-arrow-c-big-body': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            {
                viewBox: '0 0 149.84 13.619811',
                version: '1.1',
                id: 'ppic-arrow-c-big-body-svg10',
                width: '149.84',
                height: '13.619812',
                preserveAspectRatio,
            },
            createElement(
                'defs',
                { id: 'ppic-arrow-c-big-body-defs4' },
                createElement(
                    'style',
                    { id: 'ppic-arrow-c-big-body-style2' },
                    '.ppic-arrow-c-big-body-cls-1{fill:#cee8da;}#rect870{ fill:#cee8da;fill-opacity:1;stroke-width:1.00157;stroke-linecap:round;stroke-linejoin:round } #rect1719{ fill:#cee8da;fill-opacity:0.00888576;stroke-width:1.00157;stroke-linecap:round;stroke-linejoin:round }'
                )
            ),
            createElement(
                'g',
                {
                    id: 'ppic-arrow-c-big-body-Calque_2',
                    'data-name': 'Calque 2',
                },
                createElement(
                    'g',
                    {
                        id: 'ppic-arrow-c-big-body-Calque_2-2',
                        'data-name': 'Calque 2',
                    },
                    createElement('rect', {
                        id: 'rect870',
                        width: '64.032013',
                        height: '13.619812',
                        x: '0',
                        y: '0',
                    }),
                    createElement('rect', {
                        id: 'rect1719',
                        width: '85.807983',
                        height: '13.619812',
                        x: '64.032013',
                        y: '0',
                    })
                )
            )
        ),
    'pic-drop': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 11.59 17.15', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-drop-cls-1{fill:#00799c;}.ppic-drop-cls-2{fill:#fff;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-drop-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    { id: 'ppic-drop-Calque_2-2', 'data-name': 'Calque 2' },
                    createElement('path', {
                        className: 'ppic-drop-cls-1',
                        d: 'M5.79,0C2.27,5.14,0,8.36,0,11.49a5.72,5.72,0,0,0,5.79,5.66,5.72,5.72,0,0,0,5.8-5.66C11.59,8.36,9.32,5.14,5.79,0Z',
                    }),
                    createElement('path', {
                        className: 'ppic-drop-cls-2',
                        d: 'M5.77,3.66A4.59,4.59,0,0,1,6.52,6c0,2.79-2.9,3.25-2.9,1.55C3.62,6.42,5.06,4.69,5.77,3.66Z',
                    })
                )
            )
        ),
    'pic-06-rcu-shadow': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 64.66 54.27', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-06-rcu-shadow-cls-1{fill:#d9d9d9;}.ppic-06-rcu-shadow-cls-2{fill:#289084;stroke-miterlimit:10;}.ppic-06-rcu-shadow-cls-2,.ppic-06-rcu-shadow-cls-3{stroke:#fff;stroke-width:1.5px;}.ppic-06-rcu-shadow-cls-3{fill:none;stroke-linecap:round;stroke-linejoin:round;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-06-rcu-shadow-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    {
                        id: 'ppic-06-rcu-shadow-Calque_2-2',
                        'data-name': 'Calque 2',
                    },
                    createElement('ellipse', {
                        className: 'ppic-06-rcu-shadow-cls-1',
                        cx: '32.33',
                        cy: '33.48',
                        rx: '32.33',
                        ry: '20.78',
                    }),
                    createElement('path', {
                        className: 'ppic-06-rcu-shadow-cls-2',
                        d: 'M51.16,14.48V7.66H7.45V40.11H51.16c3.34,0,6.05-2,6.05-4.58v-21Z',
                    }),
                    createElement('path', {
                        className: 'ppic-06-rcu-shadow-cls-2',
                        d: 'M45.12,35.53c0-2.53,2.71-4.58,6-4.58s6.05,2.05,6.05,4.58V5.33C57.21,2.8,54.5.75,51.16.75s-6,2.05-6,4.58Z',
                    }),
                    createElement('polygon', {
                        className: 'ppic-06-rcu-shadow-cls-3',
                        points: '35.66 33.02 37.63 25.59 36.37 19.26 31.87 17.57 28.63 14.48 23.71 14.48 17.66 19.82 15.13 25.59 19.49 28.96 23.43 29.1 25.68 33.02 35.66 33.02',
                    })
                )
            )
        ),
    'pic-00-env-shadow': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            {
                version: '1.1',
                docname: '03-rru-shadow.svg',
                x: '0px',
                y: '0px',
                viewBox: '0 0 64.7 54.3',
                id: 'svg16',
                space: 'preserve',
                preserveAspectRatio,
            },
            createElement(
                'style',
                { type: 'text/css' },
                '	.ppic-00-env-shadow-st0{fill:#D9D9D9;}	.ppic-00-env-shadow-st1{fill:#289084;stroke:#FFFFFF;stroke-width:1.5;stroke-miterlimit:10;}	.ppic-00-env-shadow-st2{fill:#00799C;stroke:#FFFFFF;stroke-width:4;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}	.ppic-00-env-shadow-st3{fill:#FFFFFF;stroke:#FFFFFF;stroke-width:4;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}	.ppic-00-env-shadow-st4{fill:#00799C;}	.ppic-00-env-shadow-st5{fill:#FFFFFF;}#svg16{ enable-background:new 0 0 64.7 54.3; }'
            ),
            createElement(
                'namedview',
                {
                    bordercolor: '#666666',
                    borderopacity: '1.0',
                    id: 'ppic-00-env-shadow-namedview18',
                    'current-layer': 'Calque_2-2',
                    cx: '22.330287',
                    cy: '39.121959',
                    pagecheckerboard: '0',
                    pageopacity: '0.0',
                    pageshadow: '2',
                    'window-height': '1146',
                    'window-maximized': '0',
                    'window-width': '1971',
                    'window-x': '194',
                    'window-y': '97',
                    zoom: '5.6873431',
                    pagecolor: '#ffffff',
                    showgrid: 'false',
                },
                '\n	'
            ),
            createElement(
                'g',
                {
                    id: 'ppic-00-env-shadow-Calque_2_00000095333827134841325070000015100296952872034483_',
                },
                createElement(
                    'g',
                    { id: 'ppic-00-env-shadow-Calque_2-2' },
                    createElement('ellipse', {
                        id: 'ppic-00-env-shadow-ellipse6',
                        className: 'ppic-00-env-shadow-st0',
                        cx: '32.3',
                        cy: '33.5',
                        rx: '32.3',
                        ry: '20.8',
                    }),
                    createElement('path', {
                        id: 'ppic-00-env-shadow-path8',
                        className: 'ppic-00-env-shadow-st1',
                        d: 'M51.2,14.5V7.7H7.4v32.5h43.7c3.3,0,6-2,6-4.6v-21L51.2,14.5z',
                    }),
                    createElement('path', {
                        id: 'ppic-00-env-shadow-path10',
                        className: 'ppic-00-env-shadow-st1',
                        d: 'M45.1,35.5c0-2.5,2.7-4.6,6-4.6s6,2.1,6,4.6V5.3c0-2.5-2.7-4.6-6-4.6s-6,2-6,4.6L45.1,35.5z',
                    })
                )
            ),
            createElement(
                'g',
                {},
                createElement(
                    'g',
                    {},
                    createElement(
                        'g',
                        {},
                        createElement('path', {
                            className: 'ppic-00-env-shadow-st2',
                            d: 'M27.4,2.1c-3.8,5.6-6.3,9.1-6.3,12.5c0,3.4,2.8,6.1,6.3,6.1c3.5,0,6.3-2.8,6.3-6.1     C33.6,11.2,31.2,7.7,27.4,2.1z',
                        })
                    )
                ),
                createElement('path', {
                    className: 'ppic-00-env-shadow-st3',
                    d: 'M27.3,6.1c0.4,0.6,0.8,1.5,0.8,2.6c0,3-3.1,3.5-3.1,1.7C25,9.1,26.6,7.2,27.3,6.1z',
                })
            ),
            createElement(
                'g',
                {},
                createElement(
                    'g',
                    {},
                    createElement(
                        'g',
                        {},
                        createElement('path', {
                            className: 'ppic-00-env-shadow-st4',
                            d: 'M27.4,2.1c-3.8,5.6-6.3,9.1-6.3,12.5c0,3.4,2.8,6.1,6.3,6.1c3.5,0,6.3-2.8,6.3-6.1     C33.6,11.2,31.2,7.7,27.4,2.1z',
                        })
                    )
                ),
                createElement('path', {
                    className: 'ppic-00-env-shadow-st5',
                    d: 'M27.3,6.1c0.4,0.6,0.8,1.5,0.8,2.6c0,3-3.1,3.5-3.1,1.7C25,9.1,26.6,7.2,27.3,6.1z',
                })
            )
        ),
    'pic-arrow-c-to-b-right': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            {
                viewBox: '0 0 149.84 57.729997',
                version: '1.1',
                id: 'ppic-arrow-c-to-b-right-svg10',
                docname: 'arrow-c-to-b-right.svg',
                width: '149.84',
                height: '57.73',
                preserveAspectRatio,
            },
            createElement('namedview', {
                id: 'ppic-arrow-c-to-b-right-namedview12',
                pagecolor: '#ffffff',
                bordercolor: '#666666',
                borderopacity: '1.0',
                pageshadow: '2',
                pageopacity: '0.0',
                pagecheckerboard: '0',
                showgrid: 'false',
                'fit-margin-top': '0',
                'fit-margin-left': '0',
                'fit-margin-right': '0',
                'fit-margin-bottom': '0',
                zoom: '8.4089698',
                cx: '74.860538',
                cy: '23.843587',
                'window-width': '3440',
                'window-height': '1376',
                'window-x': '0',
                'window-y': '27',
                'window-maximized': '1',
                'current-layer': 'svg10',
            }),
            createElement(
                'defs',
                { id: 'ppic-arrow-c-to-b-right-defs4' },
                createElement(
                    'style',
                    { id: 'ppic-arrow-c-to-b-right-style2' },
                    '.ppic-arrow-c-to-b-right-cls-1{fill:#cee8da;}'
                )
            ),
            createElement(
                'g',
                {
                    id: 'ppic-arrow-c-to-b-right-Calque_2',
                    'data-name': 'Calque 2',
                    transform: 'translate(0,-10)',
                },
                createElement(
                    'g',
                    {
                        id: 'ppic-arrow-c-to-b-right-Calque_2-2',
                        'data-name': 'Calque 2',
                    },
                    createElement('path', {
                        className: 'ppic-arrow-c-to-b-right-cls-1',
                        d: 'm 138.93,62.52 v -2.29 c 0,-14.06 -16.35,-23 -52.81,-23 H 74.65 C 68.780513,37.219039 64.021994,32.469456 64,26.6 V 10 H 0 v 1.83 c 0,20.78 16.35,37.7 52.81,37.7 h 36.64 c 5.877268,0.01101 10.63899,4.772732 10.65,10.65 v 2.34 H 89.5 l 30.17,5.21 30.17,-5.21 z',
                        id: 'ppic-arrow-c-to-b-right-path6',
                        nodetypes: 'cssccccssccccccc',
                    })
                )
            )
        ),
    'pic-arrow-a-tip': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 41.62 3.59', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-arrow-a-tip-cls-1{fill:#cee8da;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-arrow-a-tip-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    {
                        id: 'ppic-arrow-a-tip-Calque_2-2',
                        'data-name': 'Calque 2',
                    },
                    createElement('polygon', {
                        className: 'ppic-arrow-a-tip-cls-1',
                        points: '41.62 0 0 0 20.81 3.59 41.62 0',
                    })
                )
            )
        ),
    'pic-arrow-c-tip': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 102.15 8.82', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-arrow-c-tip-cls-1{fill:#cee8da;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-arrow-c-tip-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    {
                        id: 'ppic-arrow-c-tip-Calque_2-2',
                        'data-name': 'Calque 2',
                    },
                    createElement('polygon', {
                        className: 'ppic-arrow-c-tip-cls-1',
                        points: '102.15 0 0 0 51.07 8.82 102.15 0',
                    })
                )
            )
        ),
    'pic-10-water': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 64.66 41.57', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-10-water-cls-1{fill:#289084;}.ppic-10-water-cls-2{fill:#9fd1b6;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-10-water-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    { id: 'ppic-10-water-Calque_2-2', 'data-name': 'Calque 2' },
                    createElement('ellipse', {
                        className: 'ppic-10-water-cls-1',
                        cx: '32.33',
                        cy: '20.78',
                        rx: '32.33',
                        ry: '20.78',
                    }),
                    createElement('path', {
                        className: 'ppic-10-water-cls-2',
                        d: 'M39.51,7.55c-5.85-5.25-11-1.9-12,2.83-.66,3.38,2.11,8.72,8.94,9.59,5.12.66,8.87.07,14.37-2.68s2.25-6.06-1.37-6.19C46.57,11,43.09,10.77,39.51,7.55Z',
                    }),
                    createElement('path', {
                        className: 'ppic-10-water-cls-2',
                        d: 'M39.18,29.54c-3.68-2.17-11.5-7.3-17.29-10.19C16.75,16.78,7,15.62,1.15,15.3a14.29,14.29,0,0,0-.71,2.09c5.88.45,14.21,1.63,18.82,3.93,5.79,2.9,13.61,8,17.29,10.19,3.29,1.94,5.33,4.93,17.46,4.69a29.07,29.07,0,0,0,3-2C44.54,34.53,42.5,31.5,39.18,29.54Z',
                    })
                )
            )
        ),
    'pic-checked': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 36.69 29.5', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-checked-cls-1{fill:none;stroke:#289084;stroke-miterlimit:10;stroke-width:8px;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-checked-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    { id: 'ppic-checked-Calque_2-2', 'data-name': 'Calque 2' },
                    createElement('path', {
                        className: 'ppic-checked-cls-1',
                        d: 'M2.85,13.43,13.09,23.81l20.76-21',
                    })
                )
            )
        ),
    'pic-07-dpc-shadow': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 64.66 54.27', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-07-dpc-shadow-cls-1{fill:#d9d9d9;}.ppic-07-dpc-shadow-cls-2{fill:#289084;stroke-miterlimit:10;}.ppic-07-dpc-shadow-cls-2,.ppic-07-dpc-shadow-cls-3{stroke:#fff;stroke-width:1.5px;}.ppic-07-dpc-shadow-cls-3{fill:none;stroke-linecap:round;stroke-linejoin:round;}.ppic-07-dpc-shadow-cls-4{fill:#fff;}.ppic-07-dpc-shadow-cls-5{fill:#75b626;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-07-dpc-shadow-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    {
                        id: 'ppic-07-dpc-shadow-Calque_2-2',
                        'data-name': 'Calque 2',
                    },
                    createElement('ellipse', {
                        className: 'ppic-07-dpc-shadow-cls-1',
                        cx: '32.33',
                        cy: '33.48',
                        rx: '32.33',
                        ry: '20.78',
                    }),
                    createElement('path', {
                        className: 'ppic-07-dpc-shadow-cls-2',
                        d: 'M51.16,14.48V7.66H7.45V40.11H51.16c3.34,0,6.05-2,6.05-4.58v-21Z',
                    }),
                    createElement('path', {
                        className: 'ppic-07-dpc-shadow-cls-2',
                        d: 'M45.12,35.53c0-2.53,2.71-4.58,6-4.58s6.05,2.05,6.05,4.58V5.33C57.21,2.8,54.5.75,51.16.75s-6,2.05-6,4.58Z',
                    }),
                    createElement('polygon', {
                        className: 'ppic-07-dpc-shadow-cls-3',
                        points: '22.37 17.13 25.82 15.28 29.53 16.07 32.27 13.6 34.39 15.63 34.66 18.55 33.86 20.67 37.48 22.96 38.72 27.47 37.04 27.56 36.16 29.24 39.34 31.27 30.24 35.78 30.06 34.54 24.85 33.48 23.79 30.48 22.55 30.21 23.08 28.27 20.78 29.06 17.34 27.38 18.66 25.09 20.96 24.64 21.31 21.82 20.34 20.67 22.11 18.55 22.37 17.13',
                    }),
                    createElement('path', {
                        className: 'ppic-07-dpc-shadow-cls-4',
                        d: 'M23.45,17.28a1,1,0,1,0,0-2V13.43a2.71,2.71,0,0,1,2.72,2.71c0,2.93-2.72,4.42-2.72,5.94',
                    }),
                    createElement('path', {
                        className: 'ppic-07-dpc-shadow-cls-4',
                        d: 'M23.45,22.08c0-1.52-2.72-3-2.72-5.94a2.71,2.71,0,0,1,2.72-2.71v1.86a1,1,0,1,0,0,2',
                    }),
                    createElement('circle', {
                        className: 'ppic-07-dpc-shadow-cls-5',
                        cx: '23.45',
                        cy: '16.31',
                        r: '1.19',
                    }),
                    createElement('path', {
                        className: 'ppic-07-dpc-shadow-cls-4',
                        d: 'M30.16,14.69a1,1,0,0,0,0-2V10.83a2.71,2.71,0,0,1,2.71,2.72c0,2.93-2.71,4.41-2.71,5.93',
                    }),
                    createElement('path', {
                        className: 'ppic-07-dpc-shadow-cls-4',
                        d: 'M30.16,19.48c0-1.52-2.72-3-2.72-5.93a2.72,2.72,0,0,1,2.72-2.72V12.7a1,1,0,1,0,0,2',
                    }),
                    createElement('circle', {
                        className: 'ppic-07-dpc-shadow-cls-5',
                        cx: '30.16',
                        cy: '13.72',
                        r: '1.19',
                    }),
                    createElement('path', {
                        className: 'ppic-07-dpc-shadow-cls-4',
                        d: 'M28.64,23.61a1,1,0,0,0,0-2V19.75a2.71,2.71,0,0,1,2.71,2.72c0,2.92-2.71,4.41-2.71,5.93',
                    }),
                    createElement('path', {
                        className: 'ppic-07-dpc-shadow-cls-4',
                        d: 'M28.64,28.4c0-1.52-2.72-3-2.72-5.93a2.72,2.72,0,0,1,2.72-2.72v1.87a1,1,0,1,0,0,2',
                    }),
                    createElement('circle', {
                        className: 'ppic-07-dpc-shadow-cls-5',
                        cx: '28.64',
                        cy: '22.64',
                        r: '1.19',
                    }),
                    createElement('path', {
                        className: 'ppic-07-dpc-shadow-cls-4',
                        d: 'M34.26,27.38a1,1,0,1,0,0-2V23.52A2.72,2.72,0,0,1,37,26.24c0,2.92-2.72,4.41-2.72,5.93',
                    }),
                    createElement('path', {
                        className: 'ppic-07-dpc-shadow-cls-4',
                        d: 'M34.26,32.17c0-1.52-2.71-3-2.71-5.93a2.71,2.71,0,0,1,2.71-2.72v1.87a1,1,0,0,0,0,2',
                    }),
                    createElement('circle', {
                        className: 'ppic-07-dpc-shadow-cls-5',
                        cx: '34.26',
                        cy: '26.41',
                        r: '1.19',
                    }),
                    createElement('path', {
                        className: 'ppic-07-dpc-shadow-cls-4',
                        d: 'M26.32,25.82a1,1,0,1,0,0-2V22A2.71,2.71,0,0,1,29,24.68c0,2.93-2.72,4.41-2.72,5.94',
                    }),
                    createElement('path', {
                        className: 'ppic-07-dpc-shadow-cls-4',
                        d: 'M26.32,30.62c0-1.53-2.71-3-2.71-5.94A2.71,2.71,0,0,1,26.32,22v1.86a1,1,0,0,0,0,2',
                    }),
                    createElement('circle', {
                        className: 'ppic-07-dpc-shadow-cls-5',
                        cx: '26.32',
                        cy: '24.85',
                        r: '1.19',
                    })
                )
            )
        ),
    'pic-02-permit-urb-shadow': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            {
                viewBox: '0 0 64.66 72.78',
                version: '1.1',
                id: 'ppic-02-permit-urb-shadow-svg40',
                preserveAspectRatio,
            },
            createElement(
                'defs',
                { id: 'ppic-02-permit-urb-shadow-defs4' },
                createElement(
                    'style',
                    { id: 'ppic-02-permit-urb-shadow-style2' },
                    '.ppic-02-permit-urb-shadow-cls-1{isolation:isolate;}.ppic-02-permit-urb-shadow-cls-2{fill:#d9d9d9;}.ppic-02-permit-urb-shadow-cls-3,.ppic-02-permit-urb-shadow-cls-6{fill:#289084;}.ppic-02-permit-urb-shadow-cls-4,.ppic-02-permit-urb-shadow-cls-7{fill:none;}.ppic-02-permit-urb-shadow-cls-4,.ppic-02-permit-urb-shadow-cls-6,.ppic-02-permit-urb-shadow-cls-7{stroke:#fff;stroke-width:1.5px;}.ppic-02-permit-urb-shadow-cls-4{stroke-linecap:round;stroke-linejoin:round;}.ppic-02-permit-urb-shadow-cls-5{fill:#b6b6b6;mix-blend-mode:multiply;}.ppic-02-permit-urb-shadow-cls-6,.ppic-02-permit-urb-shadow-cls-7{stroke-miterlimit:10;}#g32{ isolation:isolate } #rect8{ fill:#e30613;fill-opacity:1 }'
                )
            ),
            createElement(
                'g',
                {
                    className: 'ppic-02-permit-urb-shadow-cls-1',
                    id: 'ppic-02-permit-urb-shadow-g38',
                },
                createElement(
                    'g',
                    {
                        id: 'ppic-02-permit-urb-shadow-Calque_2',
                        'data-name': 'Calque 2',
                    },
                    createElement(
                        'g',
                        {
                            id: 'ppic-02-permit-urb-shadow-Calque_3',
                            'data-name': 'Calque 3',
                        },
                        createElement(
                            'g',
                            {
                                className: 'ppic-02-permit-urb-shadow-cls-1',
                                id: 'g32',
                                transform: 'translate(-4.6519267,6.5585029)',
                            },
                            createElement(
                                'g',
                                {
                                    id: 'ppic-02-permit-urb-shadow-Calque_2-26',
                                    'data-name': 'Calque 2',
                                },
                                createElement(
                                    'g',
                                    {
                                        id: 'ppic-02-permit-urb-shadow-Calque_2-2',
                                        'data-name': 'Calque 2',
                                    },
                                    createElement('ellipse', {
                                        className:
                                            'ppic-02-permit-urb-shadow-cls-2',
                                        cx: '36.908833',
                                        cy: '45.3288',
                                        rx: '32.330002',
                                        ry: '20.780001',
                                        id: 'ppic-02-permit-urb-shadow-ellipse6',
                                    }),
                                    createElement('rect', {
                                        className:
                                            'ppic-02-permit-urb-shadow-cls-3',
                                        x: '19.408833',
                                        y: '9.1887989',
                                        width: '35',
                                        height: '46.189999',
                                        id: 'rect8',
                                    }),
                                    createElement('line', {
                                        className:
                                            'ppic-02-permit-urb-shadow-cls-4',
                                        x1: '24.218832',
                                        y1: '21.1388',
                                        x2: '49.848831',
                                        y2: '21.1388',
                                        id: 'ppic-02-permit-urb-shadow-line10',
                                    }),
                                    createElement('line', {
                                        className:
                                            'ppic-02-permit-urb-shadow-cls-4',
                                        x1: '24.218832',
                                        y1: '29.6388',
                                        x2: '49.848831',
                                        y2: '29.6388',
                                        id: 'ppic-02-permit-urb-shadow-line12',
                                    }),
                                    createElement('line', {
                                        className:
                                            'ppic-02-permit-urb-shadow-cls-4',
                                        x1: '24.218832',
                                        y1: '33.548801',
                                        x2: '49.848831',
                                        y2: '33.548801',
                                        id: 'ppic-02-permit-urb-shadow-line14',
                                    }),
                                    createElement('line', {
                                        className:
                                            'ppic-02-permit-urb-shadow-cls-4',
                                        x1: '24.218832',
                                        y1: '37.448799',
                                        x2: '49.848831',
                                        y2: '37.448799',
                                        id: 'ppic-02-permit-urb-shadow-line16',
                                    }),
                                    createElement('line', {
                                        className:
                                            'ppic-02-permit-urb-shadow-cls-4',
                                        x1: '24.218832',
                                        y1: '41.348801',
                                        x2: '49.848831',
                                        y2: '41.348801',
                                        id: 'ppic-02-permit-urb-shadow-line18',
                                    }),
                                    createElement('line', {
                                        className:
                                            'ppic-02-permit-urb-shadow-cls-4',
                                        x1: '24.218832',
                                        y1: '45.258801',
                                        x2: '49.848831',
                                        y2: '45.258801',
                                        id: 'ppic-02-permit-urb-shadow-line20',
                                    }),
                                    createElement('rect', {
                                        className:
                                            'ppic-02-permit-urb-shadow-cls-5',
                                        x: '0.63739103',
                                        y: '22.098127',
                                        width: '13.18',
                                        height: '3.04',
                                        transform: 'rotate(-45)',
                                        id: 'ppic-02-permit-urb-shadow-rect22',
                                    }),
                                    createElement('rect', {
                                        className:
                                            'ppic-02-permit-urb-shadow-cls-5',
                                        x: '-51.558895',
                                        y: '27.049572',
                                        width: '13.18',
                                        height: '3.04',
                                        transform: 'rotate(-135)',
                                        id: 'ppic-02-permit-urb-shadow-rect24',
                                    }),
                                    createElement('rect', {
                                        className:
                                            'ppic-02-permit-urb-shadow-cls-5',
                                        x: '46.293579',
                                        y: '20.498901',
                                        width: '13.18',
                                        height: '3.04',
                                        transform: 'rotate(45)',
                                        id: 'ppic-02-permit-urb-shadow-rect26',
                                    }),
                                    createElement('rect', {
                                        className:
                                            'ppic-02-permit-urb-shadow-cls-5',
                                        x: '-5.9097795',
                                        y: '-75.743706',
                                        width: '13.18',
                                        height: '3.04',
                                        transform: 'rotate(135)',
                                        id: 'ppic-02-permit-urb-shadow-rect28',
                                    })
                                )
                            )
                        )
                    )
                )
            )
        ),
    'pic-circle-yes': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 30.33 30.33', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-circle-yes-cls-1{fill:none;stroke:#289084;stroke-miterlimit:10;stroke-width:3px;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-circle-yes-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    {
                        id: 'ppic-circle-yes-Calque_2-2',
                        'data-name': 'Calque 2',
                    },
                    createElement('polyline', {
                        className: 'ppic-circle-yes-cls-1',
                        points: '8.39 16.17 13.04 20.82 22.46 11.41',
                    }),
                    createElement('circle', {
                        className: 'ppic-circle-yes-cls-1',
                        cx: '15.17',
                        cy: '15.17',
                        r: '13.67',
                    })
                )
            )
        ),
    'pic-18-prime-communal': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            {
                id: 'ppic-18-prime-communal-Calque_2',
                viewBox: '0 0 64.66 52.82',
                preserveAspectRatio,
            },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-18-prime-communal-cls-1{fill:none;}.ppic-18-prime-communal-cls-1,.ppic-18-prime-communal-cls-2,.ppic-18-prime-communal-cls-3{stroke:#fff;stroke-width:1.5px;}.ppic-18-prime-communal-cls-1,.ppic-18-prime-communal-cls-3{stroke-linecap:round;stroke-linejoin:round;}.ppic-18-prime-communal-cls-2,.ppic-18-prime-communal-cls-4{stroke-miterlimit:10;}.ppic-18-prime-communal-cls-2,.ppic-18-prime-communal-cls-3,.ppic-18-prime-communal-cls-5{fill:#289084;}.ppic-18-prime-communal-cls-6{fill:#d9d9d9;}.ppic-18-prime-communal-cls-4{fill:#fff;stroke:#289084;stroke-width:1.7px;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-18-prime-communal-Calque_2-2' },
                createElement(
                    'g',
                    {},
                    createElement(
                        'g',
                        {},
                        createElement('ellipse', {
                            className: 'ppic-18-prime-communal-cls-6',
                            cx: '32.33',
                            cy: '32.04',
                            rx: '32.33',
                            ry: '20.78',
                        }),
                        createElement('polygon', {
                            className: 'ppic-18-prime-communal-cls-2',
                            points: '39.8 11.91 39.8 3.42 17.75 3.42 17.75 43.88 47.34 43.88 47.34 11.91 39.8 11.91',
                        })
                    ),
                    createElement('polygon', {
                        className: 'ppic-18-prime-communal-cls-3',
                        points: '39.8 3.42 47.34 11.91 39.8 11.91 39.8 3.42',
                    }),
                    createElement('circle', {
                        className: 'ppic-18-prime-communal-cls-4',
                        cx: '17.16',
                        cy: '10.48',
                        r: '9.63',
                    }),
                    createElement('path', {
                        className: 'ppic-18-prime-communal-cls-5',
                        d: 'M15.07,12.21c-.09,0-.12,.04-.12,.1,0,.04,.01,.09,.03,.12,.43,.93,1.15,1.41,2.44,1.41,2.41,0,2.19-1.75,3.39-1.75,.56,0,.9,.22,.9,.87,0,1.11-1.42,2.81-4.41,2.81-2.71,0-4.31-1.38-4.81-3.36-.03-.12-.06-.16-.19-.16h-.59c-.36,0-.43-.15-.43-.53,0-.47,.16-.53,.46-.53h.38c.12,0,.15-.03,.15-.19s-.02-.46-.02-.62c0-.12-.03-.18-.13-.18h-.25c-.35,0-.43-.15-.43-.53,0-.47,.16-.53,.46-.53h.34c.1,0,.18-.06,.21-.19,.09-.42,.28-.89,.5-1.29,.84-1.57,2.38-2.37,4.22-2.37,2.59,0,4.57,1.27,4.57,3.27,0,.64-.34,1.11-1.1,1.11-.67,0-.95-.43-1.26-1.04-.38-.75-.9-1.38-2.12-1.38-.99,0-1.58,.36-2.09,1.1-.13,.21-.28,.5-.28,.67,0,.07,.03,.1,.12,.1,.83-.01,1.76-.03,2.8-.03,.34,0,.53,.1,.53,.5s-.1,.58-.58,.58c-.13,0-.42-.01-2.28-.01h-.64c-.19,0-.22,.06-.22,.24,0,.19,0,.46,.01,.64,.01,.12,.06,.15,.19,.15,.77,0,1.67-.03,2.65-.03,.46,0,.53,.1,.53,.5s-.1,.56-.58,.56h-2.38Z',
                    }),
                    createElement('polygon', {
                        className: 'ppic-18-prime-communal-cls-1',
                        points: '39.86 36.15 41.38 30.4 40.4 25.51 36.92 24.2 34.42 21.81 30.61 21.81 25.93 25.94 23.98 30.4 27.35 33.01 30.39 33.12 32.13 36.15 39.86 36.15',
                    })
                )
            )
        ),
    'pic-04-pad': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            {
                viewBox: '0 0 64.66 54.27',
                version: '1.1',
                id: 'ppic-04-pad-svg24',
                preserveAspectRatio,
            },
            createElement(
                'defs',
                { id: 'ppic-04-pad-defs4' },
                createElement(
                    'style',
                    { id: 'ppic-04-pad-style2' },
                    '.ppic-04-pad-cls-1{fill:#d9d9d9;}.ppic-04-pad-cls-2{fill:#289084;}.ppic-04-pad-cls-2,.ppic-04-pad-cls-4{stroke:#fff;stroke-miterlimit:10;stroke-width:1.5px;}.ppic-04-pad-cls-3{fill:#fff;}.ppic-04-pad-cls-4{fill:none;stroke-linecap:round;}#ellipse6{ fill:#d9d9d9;fill-opacity:0 }'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-04-pad-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    { id: 'ppic-04-pad-Calque_2-2', 'data-name': 'Calque 2' },
                    createElement('ellipse', {
                        className: 'ppic-04-pad-cls-1',
                        cx: '32.33',
                        cy: '33.48',
                        rx: '32.33',
                        ry: '20.78',
                        id: 'ellipse6',
                    }),
                    createElement('path', {
                        className: 'ppic-04-pad-cls-2',
                        d: 'M51.16,14.48V7.66H7.45V40.11H51.16c3.34,0,6.05-2,6.05-4.58v-21Z',
                        id: 'ppic-04-pad-path8',
                    }),
                    createElement('path', {
                        className: 'ppic-04-pad-cls-2',
                        d: 'M45.12,35.53c0-2.53,2.71-4.58,6-4.58s6.05,2.05,6.05,4.58V5.33C57.21,2.8,54.5.75,51.16.75s-6,2.05-6,4.58Z',
                        id: 'ppic-04-pad-path10',
                    }),
                    createElement('polygon', {
                        className: 'ppic-04-pad-cls-3',
                        points: '18.14 29.48 23.94 14.88 38.84 22.13 36.51 34.42 18.14 29.48',
                        id: 'ppic-04-pad-polygon12',
                    }),
                    createElement('polyline', {
                        className: 'ppic-04-pad-cls-4',
                        points: '27.34 10.7 23.94 14.88 18.14 29.48 14.65 33.09',
                        id: 'ppic-04-pad-polyline14',
                    }),
                    createElement('polyline', {
                        className: 'ppic-04-pad-cls-4',
                        points: '18.18 15.15 23.94 14.88 38.84 22.13 44.95 17.41',
                        id: 'ppic-04-pad-polyline16',
                    }),
                    createElement('polyline', {
                        className: 'ppic-04-pad-cls-4',
                        points: '34.78 10.84 38.84 22.13 36.51 34.42 39.9 36.35',
                        id: 'ppic-04-pad-polyline18',
                    }),
                    createElement('polyline', {
                        className: 'ppic-04-pad-cls-4',
                        points: '14.46 24.79 18.14 29.48 36.51 34.42',
                        id: 'ppic-04-pad-polyline20',
                    })
                )
            )
        ),
    'pic-arrow-c-body': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 63.29 63.29', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-arrow-c-body-cls-1{fill:#cee8da;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-arrow-c-body-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    {
                        id: 'ppic-arrow-c-body-Calque_2-2',
                        'data-name': 'Calque 2',
                    },
                    createElement('rect', {
                        className: 'ppic-arrow-c-body-cls-1',
                        width: '63.29',
                        height: '63.29',
                    })
                )
            )
        ),
    'pic-12-depth': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            { viewBox: '0 0 66 73.5', preserveAspectRatio },
            createElement(
                'defs',
                {},
                createElement(
                    'style',
                    {},
                    '.ppic-12-depth-cls-1,.ppic-12-depth-cls-4,.ppic-12-depth-cls-5,.ppic-12-depth-cls-6,.ppic-12-depth-cls-7{fill:none;}.ppic-12-depth-cls-1{stroke:#1d1d1b;stroke-dasharray:2.98 1.99;}.ppic-12-depth-cls-1,.ppic-12-depth-cls-7{stroke-miterlimit:10;}.ppic-12-depth-cls-1,.ppic-12-depth-cls-4,.ppic-12-depth-cls-5,.ppic-12-depth-cls-7{stroke-width:1.5px;}.ppic-12-depth-cls-2{fill:#9fd1b6;}.ppic-12-depth-cls-3{fill:#289084;}.ppic-12-depth-cls-4,.ppic-12-depth-cls-5{stroke:#fff;stroke-linecap:round;stroke-linejoin:round;}.ppic-12-depth-cls-5{stroke-dasharray:0 4.94;}.ppic-12-depth-cls-7{stroke:#00789c;}.ppic-12-depth-cls-8{fill:#00789c;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-12-depth-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    { id: 'ppic-12-depth-Calque_2-2', 'data-name': 'Calque 2' },
                    createElement('path', {
                        className: 'ppic-12-depth-cls-1',
                        d: 'M.75,52c0-11.48,14.48-20.79,32.33-20.79C76,32.3,76,71.64,33.08,72.75,15.23,72.75.75,63.45.75,52Z',
                    }),
                    createElement('path', {
                        className: 'ppic-12-depth-cls-2',
                        d: 'M39.93,60.72c-3.68-2.17-11.5-7.29-17.29-10.18C17.5,48,7.74,46.8,1.9,46.48a14.83,14.83,0,0,0-.71,2.1C7.07,49,15.4,50.2,20,52.51c5.79,2.89,13.61,8,17.29,10.18,3.29,2,5.33,4.94,17.46,4.7a30.81,30.81,0,0,0,3-2C45.29,65.72,43.25,62.68,39.93,60.72Z',
                    }),
                    createElement('ellipse', {
                        className: 'ppic-12-depth-cls-3',
                        cx: '33.08',
                        cy: '20.78',
                        rx: '32.33',
                        ry: '20.78',
                    }),
                    createElement('line', {
                        className: 'ppic-12-depth-cls-4',
                        x1: '11.71',
                        y1: '36.38',
                        x2: '11.71',
                        y2: '36.38',
                    }),
                    createElement('path', {
                        className: 'ppic-12-depth-cls-5',
                        d: 'M16.17,34.26a46.18,46.18,0,0,1,16.91-3.08,45.09,45.09,0,0,1,19.18,4.06',
                    }),
                    createElement('line', {
                        className: 'ppic-12-depth-cls-4',
                        x1: '54.45',
                        y1: '36.38',
                        x2: '54.45',
                        y2: '36.38',
                    }),
                    createElement('path', {
                        className: 'ppic-12-depth-cls-6',
                        d: 'M11.71,36.38C5,40.19.75,45.76.75,52c0,11.48,14.48,20.78,32.33,20.78S65.41,63.45,65.41,52c0-6.21-4.24-11.78-11-15.59',
                    }),
                    createElement('line', {
                        className: 'ppic-12-depth-cls-7',
                        x1: '33.08',
                        y1: '45.37',
                        x2: '33.08',
                        y2: '67.54',
                    }),
                    createElement('polygon', {
                        className: 'ppic-12-depth-cls-8',
                        points: '38.88 46.22 33.08 42.56 27.29 46.22 27.29 46.26 38.88 46.26 38.88 46.22',
                    }),
                    createElement('polygon', {
                        className: 'ppic-12-depth-cls-8',
                        points: '38.88 66.69 33.08 70.34 27.29 66.69 27.29 66.65 38.88 66.65 38.88 66.69',
                    })
                )
            )
        ),
    'pic-03-rru-shadow': (preserveAspectRatio: PreserveAspectRatio) =>
        createElement(
            'svg',
            {
                viewBox: '0 0 64.66 54.27',
                version: '1.1',
                id: 'ppic-03-rru-shadow-svg16',
                docname: '03-rru-shadow.svg',
                preserveAspectRatio,
            },
            createElement('namedview', {
                id: 'ppic-03-rru-shadow-namedview18',
                pagecolor: '#ffffff',
                bordercolor: '#666666',
                borderopacity: '1.0',
                pageshadow: '2',
                pageopacity: '0.0',
                pagecheckerboard: '0',
                showgrid: 'false',
                zoom: '5.6873431',
                cx: '22.330287',
                cy: '39.121959',
                'window-width': '1971',
                'window-height': '1146',
                'window-x': '194',
                'window-y': '97',
                'window-maximized': '0',
                'current-layer': 'Calque_2-2',
            }),
            createElement(
                'defs',
                { id: 'ppic-03-rru-shadow-defs4' },
                createElement(
                    'style',
                    { id: 'ppic-03-rru-shadow-style2' },
                    '.ppic-03-rru-shadow-cls-1{fill:#d9d9d9;}.ppic-03-rru-shadow-cls-2{fill:#289084;stroke-miterlimit:10;}.ppic-03-rru-shadow-cls-2,.ppic-03-rru-shadow-cls-3{stroke:#fff;stroke-width:1.5px;}.ppic-03-rru-shadow-cls-3{fill:#fff;stroke-linecap:round;stroke-linejoin:round;}'
                )
            ),
            createElement(
                'g',
                { id: 'ppic-03-rru-shadow-Calque_2', 'data-name': 'Calque 2' },
                createElement(
                    'g',
                    {
                        id: 'ppic-03-rru-shadow-Calque_2-2',
                        'data-name': 'Calque 2',
                    },
                    createElement('ellipse', {
                        className: 'ppic-03-rru-shadow-cls-1',
                        cx: '32.330002',
                        cy: '33.48',
                        rx: '32.330002',
                        ry: '20.780001',
                        id: 'ppic-03-rru-shadow-ellipse6',
                    }),
                    createElement('path', {
                        className: 'ppic-03-rru-shadow-cls-2',
                        d: 'M51.16,14.48V7.66H7.45V40.11H51.16c3.34,0,6.05-2,6.05-4.58v-21Z',
                        id: 'ppic-03-rru-shadow-path8',
                    }),
                    createElement('path', {
                        className: 'ppic-03-rru-shadow-cls-2',
                        d: 'M45.12,35.53c0-2.53,2.71-4.58,6-4.58s6.05,2.05,6.05,4.58V5.33C57.21,2.8,54.5.75,51.16.75s-6,2.05-6,4.58Z',
                        id: 'ppic-03-rru-shadow-path10',
                    }),
                    createElement('polygon', {
                        className: 'ppic-03-rru-shadow-cls-3',
                        points: '22.37 16.16 25.82 14.31 29.53 15.1 32.27 12.63 34.39 14.66 34.66 17.58 33.86 19.7 37.48 22 38.72 26.5 37.04 26.59 36.16 28.27 39.34 30.3 30.24 34.81 30.06 33.57 24.85 32.51 23.79 29.51 22.55 29.24 23.08 27.3 20.78 28.09 17.34 26.42 18.66 24.12 20.96 23.68 21.31 20.85 20.34 19.7 22.11 17.58 22.37 16.16',
                        id: 'ppic-03-rru-shadow-polygon12',
                    })
                )
            )
        ),
};

type Picto = typeof pictos;

export const renderPicto = (
    key: keyof Picto,
    preserveAspectRatio = 'xMidYMid' as PreserveAspectRatio
) =>
    DIV(
        { className: `picto-infiltration ${key}`, key },
        pictos[key](preserveAspectRatio)
    );

export default renderPicto;
