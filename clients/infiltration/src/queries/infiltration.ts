/*
 *  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { fromNullable, some, none, Option } from 'fp-ts/lib/Option';
import tr from 'sdi/locale';
import { Coord2D } from 'sdi/map';

import { query } from 'sdi/shape';
import { remoteToOption } from 'sdi/source';
import { Nullable } from 'sdi/util';

const logger = debug('sdi:infiltration');

// to get a 'None' when content of the option in 'false'
const falseOptToNone = (bo: Option<boolean>) =>
    bo.chain(b => (b ? some(b) : none));

export const getAddress = () => fromNullable(query('infiltration/address'));

export const streetName = () =>
    getAddress().fold('--', ({ street }) => street.name);

export const streetNumber = () =>
    getAddress().fold('--', ({ number }) => number);

export const locality = () =>
    getAddress().fold('--', ({ street }) => street.municipality);

export const getCoord = () =>
    fromNullable(query('infiltration/coord') as Nullable<Coord2D>);

export const getX = (c: Coord2D) => c[0];
export const getY = (c: Coord2D) => c[1];

export const getAddressStr = () =>
    `${streetNumber()} ${streetName()}, ${tr.infiltr('in')} ${locality()}`;

export const isUserProfessional = () => query('infiltration/professionnal');
export const hasEnvPermits = () => query('infiltration/env-permits');
export const hasUrbaPermits = () => query('infiltration/urba-permits');
// export const hasEnvAndUrbaPermits = () => hasEnvPermits() && hasUrbaPermits();
// export const hasNoPermits = () => !hasEnvPermits() && !hasUrbaPermits();

export type PermitSelection = 'env' | 'urba' | 'envAndUrba' | 'none';
export const getPermitSelection = (
    env: boolean,
    urba: boolean
): PermitSelection => {
    if (env && !urba) {
        return 'env';
    } else if (urba && !env) {
        return 'urba';
    } else if (env && urba) {
        return 'envAndUrba';
    } else {
        return 'none';
    }
};

export const getPointDataRemote = () => query('infiltration/point/data');

export const getPointData = () => remoteToOption(getPointDataRemote());

export const getCatNb = () =>
    getPointData().chain(p => fromNullable(p.category));
export const inPPAS = () => getPointData().map(p => p.ppas);

export const inRCU = () => getPointData().map(p => p.rcu);

export const inPAD = () => getPointData().map(p => p.pad);

export const nearWaterCapture = () => getPointData().map(p => p.waterCapture);
// export const isNearWaterCaptureOpt = () => falseOptToNone(nearWaterCapture());

export const getSurfaceWaterDist = () =>
    getPointData().map(p => p.surfaceWaterProx);

export const withEasterEgg = () =>
    getSurfaceWaterDist()
        .chain(dist => fromNullable(dist).map(d => d <= 0))
        .getOrElse(false);

export const inNatura2000 = () => getPointData().map(p => p.natura2000Prox);

export const nearPhreatic = () =>
    getPointData().chain(p => fromNullable(p.phreaticProx));

export const inFloodAlea = () => getPointData().map(p => p.floodAlea);

export const isHeritage = () => getPointData().map(p => p.heritage);
// export const isHeritageOpt = () => falseOptToNone(isHeritage());

export const hasFundingLocality = () =>
    getPointData().chain(p => fromNullable(p.municipalPremium));

export const hasFundingLocalityOpt = () => falseOptToNone(hasFundingLocality());

export interface LoadingStatus {
    loading: boolean;
    total: number;
    loaded: number;
    remaining: number;
}

const defaultLoading = (): LoadingStatus => ({
    loading: false,
    total: 0,
    loaded: 0,
    remaining: 0,
});

export const getLoading = () => {
    const loading = query('infiltration/loading').length;
    const loaded = query('infiltration/loaded').length;
    if (loading > 0) {
        return {
            loading: true,
            total: loading + loaded,
            loaded,
            remaining: loading,
        };
    }

    return defaultLoading();
};

logger('loaded');
