/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { assign } from 'sdi/shape';
import { IUgWsAddress, IUgWsQualificationCode } from 'sdi/ports/geocoder';
import { Coord2D } from 'sdi/map';
import { PointData } from '../remote';
import { remoteSuccess } from 'sdi/source';

export const setAddress = (a: IUgWsAddress, qc: IUgWsQualificationCode) => {
    switch (qc.policeNumber) {
        case '1':
            assign('infiltration/address', a);
            break;
        default: {
            const addr: IUgWsAddress = { street: a.street, number: '' };
            assign('infiltration/address', addr);
            break;
        }
    }
};

export const setAddressFromPoint = (a: IUgWsAddress) =>
    assign('infiltration/address', a);

export const clearAddress = () => assign('infiltration/address', null);

export const setCoord = (c: Coord2D) => assign('infiltration/coord', c);
export const clearCoord = () => assign('infiltration/coord', null);

export const setUserProfessional = (isPro: boolean) =>
    assign('infiltration/professionnal', isPro);

export const setEnvPermits = (hasEnvPermit: boolean) =>
    assign('infiltration/env-permits', hasEnvPermit);

export const setUrbaPermits = (hasUrbaPermit: boolean) =>
    assign('infiltration/urba-permits', hasUrbaPermit);

export const setPointData = (pd: PointData) =>
    assign('infiltration/point/data', remoteSuccess(pd));
