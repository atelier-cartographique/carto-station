## Infiltration

# Documentation

## Configuration

The base map for the infiltration application must be defined in the django admin (`/admin/infiltration/config/`). Choose a map in the list and set it as 'current' to be the configuration to use.
