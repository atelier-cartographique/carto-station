! [illustration](../assets/anim/solar-demo.gif)

The solar map of the Brussels-Capital Region responds to a specific request from the energy department of Environment.brussels. It is specific to a data schema of BE.

It is an application aimed at the general public, in order to provide synthetic and scientifically valid information on the solar energy potential per building in the region.

This application can be used at this address: https://geodata.environnement.brussels/client/solar/.

An FAQ is available at this address: https://environnement.brussels/content/carte-solaire-de-la-region-bruxelloise-faq
