import { DIV, H1, LABEL, NodeOrOptional } from 'sdi/components/elements';
import {
    inputNumber,
    Getter,
    Setter,
    AllInputAttributes,
    renderRadioIn,
    attrOptions,
} from 'sdi/components/input';
import tr from 'sdi/locale';

import {
    getNumInputF,
    GetNumKeyOfInputs,
    streetName,
    streetNumber,
    locality,
} from '../../queries/simulation';

import { setInputF, SetNumKeyOfInputs } from '../../events/simulation';
import { SolarMessageKey } from 'solar/src/locale';

// export const checkBox =
//     (label: SolarMessageKey) =>
//         DIV({ className: 'wrapper-checkbox' },
//             DIV({ className: 'checkbox' }),
//             DIV({ className: 'checkbox-label' }, tr.solar(label)));

export const inputSelect =
    <T>(get: () => T, set: (v: T) => void) =>
    (msg: SolarMessageKey, v: T) => {
        if (get() === v) {
            return DIV(
                { className: 'wrapper-checkbox' },
                DIV({ className: 'checkbox active' }),
                DIV({ className: 'checkbox-label' }, tr.solar(msg))
            );
        }
        return DIV(
            {
                className: 'wrapper-checkbox',
                onClick: () => set(v),
            },
            DIV({ className: 'checkbox' }),
            DIV({ className: 'checkbox-label' }, tr.solar(msg))
        );
    };

const inputK = (
    k: GetNumKeyOfInputs | SetNumKeyOfInputs,
    attrs: AllInputAttributes
) => {
    const get = getNumInputF(k);
    const set = setInputF(k);
    return inputNumber(attrOptions(`input-${k}`, get, set, attrs));
};

const label = (labelKey: SolarMessageKey) =>
    LABEL(
        { className: 'input-label', htmlFor: `id-${labelKey}` },
        tr.solar(labelKey)
    );

export const vertInputItemFn = (
    labelKey: SolarMessageKey,
    get: Getter<number>,
    set: Setter<number>,
    attrs: AllInputAttributes,
    ...ns: React.ReactNode[]
) =>
    DIV(
        { className: 'input-box-vertical' },
        DIV(
            { className: 'input-and-unit' },
            inputNumber(
                attrOptions(`vert-input-${labelKey}`, get, set, {
                    ...attrs,
                    id: `id-${labelKey}`,
                })
            ),
            ...ns
        ),
        label(labelKey)
    );

export const vertInputItem = (
    labelKey: SolarMessageKey,
    k: GetNumKeyOfInputs | SetNumKeyOfInputs,
    attrs: AllInputAttributes,
    ...ns: React.ReactNode[]
) =>
    DIV(
        { className: 'input-box-vertical' },
        DIV(
            { className: 'input-and-unit' },
            inputK(k, { ...attrs, id: `id-${labelKey}` }),
            ...ns
        ),
        label(labelKey)
    );

export const inputItem = (
    labelKey: SolarMessageKey,
    k: GetNumKeyOfInputs | SetNumKeyOfInputs,
    attrs: AllInputAttributes,
    ...ns: React.ReactNode[]
) => DIV({ className: 'input-box' }, inputK(k, attrs), ...ns, label(labelKey));

const activeClass = (a: boolean) =>
    a ? '' /* 'active' hits a greeny CSS rule */ : 'inactive';

export const toggle =
    <T>(get: () => T, set: (a: T) => void) =>
    (value1: T, value2: T, renderValue: (v: T) => NodeOrOptional) =>
        renderRadioIn(
            'toggle',
            renderValue,
            set,
            'switch'
        )([value1, value2], get());
// DIV({
//     className: 'toggle',
//     onClick: () => set(!get()),
// },
//     DIV({ className: `value first-value ${activeClass(get())}` }, tr.solar(value1) + ' '),
//     DIV({ className: `value second-value ${activeClass(!get())}` }, ' ' + tr.solar(value2)),
// );

export const toggleWithInfo =
    <T>(get: () => T, set: (a: T) => void) =>
    (
        value1: T,
        value2: T,
        info1: SolarMessageKey,
        info2: SolarMessageKey,
        renderValue: (v: T) => NodeOrOptional
    ) =>
        DIV(
            { className: 'toggle-wrapper' },
            renderRadioIn(
                'toggle',
                renderValue,
                set,
                'switch'
            )([value1, value2], get()),
            DIV(
                { className: 'toggle-infos' },
                DIV(
                    {
                        className: `info first-value ${activeClass(
                            get() == value1
                        )}`,
                    },
                    tr.solar(info1)
                ),
                DIV(
                    {
                        className: `info first-value ${activeClass(
                            get() == value2
                        )}`,
                    },
                    tr.solar(info2)
                )
            )
        );

export const buildingAdress = () =>
    DIV(
        { className: 'adress' },
        H1(
            { className: 'street-name' },
            `${streetName()} ${streetNumber()} ${tr.solar('in')} ${locality()}`
        )
    );
