import { DIV, BR, A, BUTTON } from 'sdi/components/elements';
import tr from 'sdi/locale';

import { context } from '../context';
import { actionChange, actionInfo } from '../action';
import {
    disclaimer,
    contactLinks,
    urbisReward,
    createdBy,
} from '../footer-infos';
import { summary as summaryPv } from '../summary/summary-pv';
import { summary as summaryThermal } from '../summary/summary-thermal';
import { getSystem, getPanelUnits } from '../../queries/simulation';
import { navigateDetail } from '../../events/route';
import { getCapakey } from '../../queries/app';
import { buildingAdress, toggleWithInfo } from '../item-factory';
import { setSystem } from '../../events/simulation';

import { renderPDF } from '../summary/print';
import { trackedLink } from '../tracked-link';
import { System } from 'solar/src/shape/solar';
import { activityLogger } from 'solar/src/events/app';
import { navigateSolarContactAction } from 'sdi/activity/index';

const actionStepAdjust = () =>
    BUTTON(
        {
            className: 'action action-step step-adjust',
            onClick: () => getCapakey().map(navigateDetail),
        },
        DIV(
            {
                className: 'step-number',
            },
            '1'
        ),
        DIV(
            {
                className: 'step-label',
                id: 'step-1',
            },
            tr.solar('solAdjustStr1'),
            BR({}),
            tr.solar('solAdjustStr2')
        )
    );

const actionStepPrint = () =>
    BUTTON(
        {
            className: 'action action-step',
            onClick: () => renderPDF(),
        },
        DIV(
            {
                className: 'step-number',
            },
            '2'
        ),
        DIV(
            {
                className: 'step-label',
                id: 'step-2',
            },
            tr.solar('solPrintStr1'),
            BR({}),
            tr.solar('solPrintStr2')
        )
    );

const actionStepContact = () =>
    DIV(
        {
            className: 'action action-step',
            onClick: () =>
                activityLogger(navigateSolarContactAction(getSystem())),
        },
        trackedLink(
            'installateur',
            {
                href: switchLinkPVTH(),
                target: '_blank',
            },
            DIV({ className: 'step-number' }, '3'),
            DIV(
                {
                    className: 'step-label',
                    id: 'step-3',
                },
                tr.solar('solContactStr1'),
                BR({}),
                tr.solar('solContactStr2')
            )
        )
    );

const switchLinkPVTH = () => {
    const system = getSystem();
    switch (system) {
        case 'photovoltaic':
            return tr.solar('solLinkInstallateurPV');
        case 'thermal':
            return tr.solar('solLinkInstallateurTH');
    }
};

const renderSolarSystem = (s: System) =>
    s == 'photovoltaic'
        ? tr.solar('solPhotovoltaic')
        : tr.solar('solSolarWaterHeater');

const toggleSystem = toggleWithInfo(getSystem, setSystem);

const action = () =>
    DIV(
        { className: 'actions' },
        DIV({ className: 'action-primary' }, actionStepAdjust()),
        DIV(
            { className: 'action-secondary' },
            actionStepPrint(),
            actionStepContact()
        )
    );

const summary = () => {
    const system = getSystem();
    switch (system) {
        case 'photovoltaic':
            return summaryPv();
        case 'thermal':
            return summaryThermal();
    }
};

const sidebar = () =>
    DIV(
        { className: 'sidebar' },
        summary(),
        DIV(
            { className: 'sidebar-tools' },
            toggleSystem(
                'photovoltaic',
                'thermal',
                'solTogglePV',
                'solToggleThermal',
                renderSolarSystem
            ),
            DIV(
                { className: 'sidebar-action-wrapper' },
                actionChange(),
                actionInfo()
            )
        )
    );

const sidebarNoSol = () =>
    DIV(
        { className: 'sidebar' },
        DIV(
            { className: 'sol-no-preview' },
            buildingAdress(),
            DIV(
                { className: 'sol-no-preview-msg' },
                DIV({}, tr.solar('solNoSolSTR1')),
                DIV({}, tr.solar('solNoSolSTR2')),
                DIV(
                    {},
                    ` ${tr.solar('solNoSolSTR3')} (`,
                    A(
                        { href: tr.solar('solHeatPumpLink') },
                        tr.solar('solHeatPumpLabel')
                    ),
                    `, `,
                    A(
                        { href: tr.solar('solBuyGreenEnergyLink') },
                        tr.solar('solBuyGreenEnergyLabel')
                    ),
                    `, `,
                    A(
                        { href: tr.solar('solSharedRoofLink') },
                        tr.solar('solSharedRoof')
                    ),
                    `, ${tr.solar('solNoSolSTR4')} `,
                    A(
                        { href: tr.solar('solLinkContactBE') },
                        tr.solar('solLinkContactBELabel')
                    ),
                    ` ${tr.solar('solNoSolSTR5')} `,
                    A(
                        { href: tr.solar('solHomegradeLink') },
                        tr.solar('solHomegradeLabel')
                    ),
                    ` ${tr.solar('solNoSolSTR6')} `,
                    A(
                        { href: tr.solar('solFacilitatorLink') },
                        tr.solar('solFacilitatorLabel')
                    ),
                    ` ${tr.solar('solNoSolSTR7')}.`
                )
            )
        )
    );

// const sidebarNoCalc =
//     () =>
//         DIV({ className: 'sidebar' },
//             DIV({ className: 'sol-no-preview' },
//                 buildingAdress(),
//                 DIV({ className: 'sol-no-preview-msg' },
//                     DIV({}, tr.solar('solNoCalcSTR1')),
//                     DIV({},
//                         ` ${tr.solar('solNoCalcSTR2')} (`,
//                         A({ href: tr.solar('solHomegradeLink') }, tr.solar('solHomegradeLabel')),
//                         ` ${tr.solar('solNoSolSTR6')} `,
//                         A({ href: tr.solar('solFacilitatorLink') }, tr.solar('solFacilitatorLabel')),
//                         ` ${tr.solar('solNoSolSTR7')}.`,
//                     ),
//                 ),
//             ),
//         );

const contentFooter = () =>
    DIV(
        { className: 'footer-infos' },
        contactLinks(),
        disclaimer(),
        urbisReward(),
        createdBy()
    );

const content = () =>
    DIV(
        { className: 'content' },
        context(),
        sidebar(),
        action(),
        contentFooter()
    );

const contentNoSun = () =>
    DIV({ className: 'content' }, context(), sidebarNoSol(), contentFooter());

// const contentNoCalc =
//     () =>
//         DIV({ className: 'content' },
//             context(),
//             sidebarNoCalc(),
//             contentFooter());

const withContent = () => {
    /* if (notComputed()) {
        return contentNoCalc();
    }
    else */ if (getPanelUnits() < 1) {
        return contentNoSun();
    }
    return content();
};

const render = () =>
    DIV({ className: 'solar-main-and-right-sidebar' }, withContent());

export default render;
