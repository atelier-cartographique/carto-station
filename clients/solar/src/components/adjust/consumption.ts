import { DIV, H2, SPAN } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { withKWhY } from 'sdi/util';

// import { inputItem } from '../item-factory';
import { annualConsumption, getInputF } from '../../queries/simulation';
import { setInputF } from '../../events/simulation';
import { note } from './note';
import { SolarMessageKey } from 'solar/src/locale';
import { fromNullable } from 'fp-ts/lib/Option';
import { renderRadioIn } from 'sdi/components/input';

const ranks = {
    first: 600,
    second: 1200,
    third: 2036,
    fourth: 3500,
    fifth: 7500,
};

type Rank = typeof ranks;

type rank = keyof Rank;
const rankValues = Object.keys(ranks).map<[rank, number]>((k: rank) => [
    k,
    ranks[k],
]);
const findRankKey = (rankValue: number) =>
    fromNullable(rankValues.find(([_, v]) => v === rankValue)).map(([k]) => k);

const legends: { [k in rank]: SolarMessageKey } = {
    first: 'solLegendConsRank1',
    second: 'solLegendConsRank2',
    third: 'solLegendConsRank3',
    fourth: 'solLegendConsRank4',
    fifth: 'solLegendConsRank5',
};

const setConsumption = setInputF('annualConsumptionKWh');
const getConsumption = getInputF('annualConsumptionKWh');

const titleAndPicto = () =>
    DIV(
        { className: 'adjust-item-header' },
        H2(
            { className: 'adjust-item-title' },
            '4. ' + tr.solar('consumptionYElectricity')
        ),
        DIV({ className: 'adjust-picto picto-home-conso' })
    );

const isActive = (rank: rank) =>
    annualConsumption() < ranks[rank] + 10 &&
    annualConsumption() > ranks[rank] - 10;

// Consumption rank selector widget
// const selectItem =
//     (rank: rank) => {
//         if (isActive(rank)) {
//             return DIV({ className: `select-item ${rank} active` });
//         }
//         return DIV({
//             className: `select-item ${rank}`,
//             onClick: () => setConsumption(ranks[rank]),
//         });
//     };
const renderRank = (rank: rank) =>
    DIV(
        {
            className: `select-item ${rank} invisible`,
            // "aria-labelledby": `rank-legend-${rank}`
        },
        `${tr.solar(legends[rank])}: ${withKWhY(ranks[rank])} ${tr.solar(
            'solConsumed'
        )}`
    );
const selectRank = (rank: rank) => setConsumption(ranks[rank]);
const ranksArray = Object.keys(ranks) as rank[];
const renderRadio = renderRadioIn(
    'consumption-selector',
    renderRank,
    selectRank
);

const selectWidget = () =>
    DIV(
        { className: 'adjust-item-widget consumption-select' },
        renderRadio(
            ranksArray,
            findRankKey(getConsumption()).getOrElse('second')
        )
        // selectItem('first'),
        // selectItem('second'),
        // selectItem('third'),
        // selectItem('fourth'),
        // selectItem('fifth'),
    );

const consumptionValue = () =>
    SPAN(
        { className: 'item-legend legend-output' },
        SPAN({ className: 'output-value' }, withKWhY(annualConsumption())),
        SPAN({}, tr.solar('solConsumed'))
    );

// Legend text per selected rank
const rankedLegend = (rank: rank) =>
    DIV(
        {
            className: `rank-legend ${rank} ${isActive(rank) ? 'active' : ''}`,
        },
        SPAN(
            {
                id: `rank-legend-${rank}`,
            },
            `${tr.solar(legends[rank])}, `,
            consumptionValue()
        )
    );

const adjustLegend = () =>
    DIV(
        {
            className: 'adjust-item-legend',
            'aria-hidden': true,
        },
        rankedLegend('first'),
        rankedLegend('second'),
        rankedLegend('third'),
        rankedLegend('fourth'),
        rankedLegend('fifth')
    );

export const calcConsumption = () =>
    DIV(
        { className: 'adjust-item consumption' },
        titleAndPicto(),
        note('pv_consumption'),
        selectWidget(),
        adjustLegend()
    );
