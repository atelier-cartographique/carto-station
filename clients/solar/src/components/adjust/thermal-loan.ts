import { DIV, SPAN, NODISPLAY, H2 } from 'sdi/components/elements';
import tr from 'sdi/locale';

import { toggle, vertInputItem, vertInputItemFn } from '../item-factory';
import { getInputF } from '../../queries/simulation';
import { setInputF } from '../../events/simulation';
import { note } from './note';

const hasLoan = getInputF('loan');
const toggleLoan = toggle(hasLoan, setInputF('loan'));
const getLoanPeriod = getInputF('loanPeriod');

const withLoan = () =>
    DIV(
        { className: 'inputs' },
        // vertInputItem('amountBorrowed'),
        vertInputItem(
            'loanDuration',
            'loanPeriod',
            { min: 1, max: 99, step: 1 },
            SPAN(
                { className: 'unit' },
                tr.solar('unitYear', { value: getLoanPeriod() })
            )
        ),
        vertInputItemFn(
            'loanRate',
            () => getInputF('loanRate')() * 100,
            r => {
                if (r > 0) {
                    setInputF('loanRate')(r / 100);
                }
            },
            { min: 0.1, max: 99, step: 0.1 },
            SPAN({ className: 'unit' }, tr.solar('unitPercent'))
        )
    );

const renderLoan = (hasLoan: boolean) =>
    hasLoan ? SPAN('', tr.solar('loanYes')) : SPAN('', tr.solar('loanNo'));
export const calcLoanThermal = () =>
    DIV(
        { className: 'adjust-item loan' },
        DIV(
            { className: 'adjust-item-header' },
            H2({ className: 'adjust-item-title' }, '5. ' + tr.solar('loan'))
        ),
        note('thermal_loan'),
        DIV(
            { className: 'adjust-item-widget' },
            toggleLoan(true, false, renderLoan),
            hasLoan() ? withLoan() : NODISPLAY()
        )
    );
