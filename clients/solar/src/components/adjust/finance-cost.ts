import { DIV, H2, SPAN } from 'sdi/components/elements';
import tr from 'sdi/locale';

import { getInputF } from '../../queries/simulation';
import { setInputF } from '../../events/simulation';
import { note } from './note';
import { renderRadioIn } from 'sdi/components/input';

// const vatSelect = inputSelect(getNumInputF('VATrate'), setInputF('VATrate'));
const renderVat = (vat: number) => {
    switch (vat) {
        case 0.21:
            return SPAN('', tr.solar('solVAT21'));
        case 0.06:
            return SPAN('', tr.solar('solVAT6'));
        case 0:
            return SPAN('', tr.solar('solVAT0'));
        default:
            return SPAN('', 'unknown');
    }
};
const vatArray = [0.21, 0.06, 0];

const renderVatRadioBtn = renderRadioIn(
    'vat-install-radio',
    renderVat,
    setInputF('VATrate'),
    'radio'
);

const expenses = () =>
    DIV(
        { className: 'cost' },
        DIV(
            { className: 'vat-installation' },
            DIV(
                { className: 'wrapper-multi-checkbox' },
                // vatSelect('solVAT21', 0.21),
                // vatSelect('solVAT6', 0.06),
                // vatSelect('solVAT0', 0))),
                renderVatRadioBtn(vatArray, getInputF('VATrate')())
            )
        )
    );

export const calcFinanceCost = () =>
    DIV(
        { className: 'adjust-item finance' },
        DIV(
            { className: 'adjust-item-header' },
            H2(
                { className: 'adjust-item-title' },
                '6. ' + tr.solar('solFinanceVAT')
            ),
            DIV({ className: 'adjust-picto picto-spend' })
        ),
        note('pv_vat'),
        DIV({ className: 'adjust-item-widget' }, expenses())
    );
