import { DIV, H2, SPAN } from 'sdi/components/elements';
import tr from 'sdi/locale';
import { withLiterDay } from 'sdi/util/index';

import { getInputF } from '../../queries/simulation';
import { setInputF } from '../../events/simulation';
import { note } from './note';
import { SolarMessageKey } from 'solar/src/locale';
import { renderRadioIn } from 'sdi/components/input';
import { fromNullable } from 'fp-ts/lib/Option';

// Petit consommateur : 60 l/jour
// Moyen : 90 l/j
// Consommateur médian: 120 l/j
// Ménage moyen: 150 l/j
// Grand ménage: 180 l/j
// Gros consomateur: 210 l/j

const ranks = {
    first: 60,
    second: 90,
    third: 120,
    fourth: 150,
    fifth: 180,
    sixth: 210,
};

type Rank = typeof ranks;

type rank = keyof Rank;

const rankValues = Object.keys(ranks).map<[rank, number]>((k: rank) => [
    k,
    ranks[k],
]);

const findRankKey = (rankValue: number) =>
    fromNullable(rankValues.find(([_, v]) => v === rankValue)).map(([k]) => k);

const legends: { [k in rank]: SolarMessageKey } = {
    first: 'solLegendConsWaterRank1',
    second: 'solLegendConsWaterRank2',
    third: 'solLegendConsWaterRank3',
    fourth: 'solLegendConsWaterRank4',
    fifth: 'solLegendConsWaterRank5',
    sixth: 'solLegendConsWaterRank6',
};

const setConsumption = setInputF('thermicLiterByDay');
const getConsumption = getInputF('thermicLiterByDay');

const titleAndPicto = () =>
    DIV(
        { className: 'adjust-item-header' },
        H2(
            { className: 'adjust-item-title' },
            '2. ' + tr.solar('solHotWaterConsumption')
        ),
        DIV({ className: 'adjust-picto picto-home-conso-water' })
    );

const isActive = (rank: rank) =>
    getConsumption() < ranks[rank] + 10 && getConsumption() > ranks[rank] - 10;

// Consumption rank selector widget
// const selectItem =
//     (rank: rank) => {
//         if (isActive(rank)) {
//             return DIV({ className: `select-item ${rank} active` });
//         }
//         return DIV({
//             className: `select-item ${rank}`,
//             onClick: () => setConsumption(ranks[rank]),
//         });
//     };

const renderRank = (rank: rank) =>
    DIV(
        {
            className: `select-item ${rank} invisible`,
            // "aria-labelledby": `rank-legend-${rank}`
        },
        `${tr.solar(legends[rank])} ${withLiterDay(ranks[rank])} ${tr.solar(
            'solConsumed'
        )}`
    );
const selectRank = (rank: rank) => setConsumption(ranks[rank]);
const ranksArray = Object.keys(ranks) as rank[];
const renderRadio = renderRadioIn(
    'thermal-consumption-selector',
    renderRank,
    selectRank
);

const selectWidget = () =>
    DIV(
        { className: 'consumption-select' },
        renderRadio(
            ranksArray,
            findRankKey(getConsumption()).getOrElse('first')
        )
        // selectItem('first'),
        // selectItem('second'),
        // selectItem('third'),
        // selectItem('fourth'),
        // selectItem('fifth'),
        // selectItem('sixth'),
    );

const consumptionValue = () =>
    SPAN(
        { className: 'item-legend legend-output' },
        SPAN(
            { className: 'output-value' },
            withLiterDay(getInputF('thermicLiterByDay')())
        ),
        SPAN({}, tr.solar('solConsumed'))
    );

// Legend text per selected rank
const rankedLegend = (rank: rank) =>
    DIV(
        {
            className: `rank-legend ${rank} ${isActive(rank) ? 'active' : ''}`,
        },
        SPAN(
            {
                id: `rank-legend-${rank}`,
            },
            `${tr.solar(legends[rank])} `,
            consumptionValue()
        )
    );

const adjustLegend = () =>
    DIV(
        {
            className: 'adjust-item-legend',
            'aria-hidden': true,
        },
        rankedLegend('first'),
        rankedLegend('second'),
        rankedLegend('third'),
        rankedLegend('fourth'),
        rankedLegend('fifth'),
        rankedLegend('sixth')
    );

export const calcConsumptionThermal = () =>
    DIV(
        { className: 'adjust-item consumption thermal' },
        titleAndPicto(),
        note('thermal_consumption'),
        DIV({ className: 'adjust-item-widget' }, selectWidget()),
        adjustLegend()
    );
