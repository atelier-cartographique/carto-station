import { fromPredicate } from 'fp-ts/lib/Option';
import { thermicHotWaterProducerEnum } from 'solar-sim';

import { DIV, H2, SPAN } from 'sdi/components/elements';
import tr from 'sdi/locale';

import {
    thermicTechnology,
    thermalTechnologyLabels,
    getInputF,
} from '../../queries/simulation';
import { setInputF } from '../../events/simulation';
import { note } from './note';
import { renderRadioIn } from 'sdi/components/input';

const technologies: thermicHotWaterProducerEnum[] = ['electric', 'fuel', 'gas'];

const icon = (t: thermicHotWaterProducerEnum) =>
    DIV({ className: `adjust-picto picto-water-${t}` });

const condTech = fromPredicate(
    (t: thermicHotWaterProducerEnum) => t === thermicTechnology()
);

const titleAndPicto = () => {
    const icons: React.ReactNode[] = [];
    technologies.map(condTech).forEach(i => i.map(t => icons.push(icon(t))));
    return DIV(
        { className: 'adjust-item-header' },
        H2(
            { className: 'adjust-item-title' },
            '1. ' + tr.solar('solHeatProdSys')
        ),
        ...icons
    );
};

const renderTech = (t: thermicHotWaterProducerEnum) =>
    SPAN('', tr.solar(thermalTechnologyLabels[t]));
const renderThermalTechRadio = renderRadioIn(
    'thermal-technology-radio',
    renderTech,
    setInputF('thermicHotWaterProducer'),
    'radio'
);

const renderSelect = () => {
    // const checkBox = inputSelect(thermicTechnology, setInputF('thermicHotWaterProducer'));
    return DIV(
        { className: 'wrapper-multi-checkbox' },
        DIV(
            {},
            // ...technologies.map(t => checkBox(thermalTechnologyLabels[t], t))
            renderThermalTechRadio(
                technologies,
                getInputF('thermicHotWaterProducer')()
            )
        )
    );
};

export const calcTechnologyThermal = () =>
    DIV(
        { className: 'adjust-item installation' },
        titleAndPicto(),
        note('thermal_sys'),
        DIV({ className: 'adjust-item-widget' }, renderSelect())
    );
