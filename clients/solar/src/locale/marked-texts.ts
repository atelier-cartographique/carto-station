export const largeTextStr1FR = `
À l'adresse
`;

export const largeTextStr1NL = `
Op het adres 
`;

export const largeTextStr2FR = `
environ
`;

export const largeTextStr2NL = `
heeft ongeveer
`;

export const largeTextStr3FR = `
 des surfaces de toitures ont un potentiel exploitable, compte tenu des obstacles et des ombres portées. Ils sont donc utilisables pour placer des panneaux solaires.

Une telle surface vous permet d’envisager une installation solaire photovoltaïque qui dépasse les besoins résidentiels individuels.

Avec de tels espaces, vous pouvez envisager différentes options:

**Équiper la toiture dans l’optique d’un partage d’énergie**, que ce soit au sein du même bâtiment ou à l’échelle de votre voisinage par exemple.

  - En consultant la page [Partage et Communautés d’Énergie](https://environnement.brussels/partage), vous en découvrirez les modalités, les avantages et les démarches à entreprendre.
  - Le [Facilitateur Partage et Communautés d’Énergie](https://environnement.brussels/citoyen/services-et-demandes/conseils-et-accompagnement/facilitateur-partage-et-communautes-denergie) pourra également répondre à vos questions ainsi que vous accompagner dans les étapes de la réalisation de votre projet

**Équiper collectivement** la toiture dans le cadre d’une dynamique de «Toiture partagée». Dans ce cas, la copropriété met la toiture à disposition des investisseurs.  

  - Des renseignements sont disponibles sur le site web de Bruxelles Environnement vous permettant d’identifier [les étapes de votre projet](https://environnement.brussels/citoyen/lenvironnement-bruxelles/renover-et-construire/quest-ce-que-lenergie-verte#partager-un-toit).
  - Homegrade peut ensuite vous aider pour aller plus loin: https://homegrade.brussels/ 


**Équiper la toiture** pour des besoins de type **industriel** ou des **grands espaces de bureaux**.

  - Le [facilitateur bâtiment durable](https://environnement.brussels/thematiques/batiment-et-energie/accompagnements-gratuits/le-facilitateur-batiment-durable) sera votre soutien idéal : [facilitateur@environnement.brussels](facilitateur@environnement.brussels) 

`;

export const largeTextStr3NL = `
 van de oppervlakte van het dak bruikbaar voor zonnepanelen, rekening houdend met de schaduwen en obstakels. 

Een dergelijke oppervlakte maakt het mogelijk om een fotovoltaïsche installatie te overwegen die de individuele behoeften overstijgt.

U kunt verschillende opties overwegen:

**Zonnepanelen installeren om energie te delen**, bijvoorbeeld binnen hetzelfde gebouw of met de buren.
  
  - Bezoek de pagina [Energiedelen en Energiegemeenschappen](https://leefmilieu.brussels/energiedelen) voor meer informatie over de voorwaarden, de voordelen en de maatregelen die moeten worden genomen
  - De [Facilitator Energiedelen en Energiegemeenschappen](https://leefmilieu.brussels/burgers/diensten-en-aanvragen/advies-en-begeleiding/facilitator-energiedelen-en-energiegemeenschappen) kan ook je vragen beantwoorden en je helpen bij de verschillende fasen van je project.

Het dak **gezamenlijk uitrusten** als onderdeel van een "Deel je dak"-dynamiek. In dit geval stelt de mede-eigendom het dak ter beschikking van de investeerders.

  - Op de pagina's van Leefmilieu Brussel is informatie beschikbaar waarmee u de [fasen van uw project](https://leefmilieu.brussels/burgers/het-milieu-brussel/renoveren-en-bouwen/wat-groene-energie#deel-een-dak) kunt identificeren.
  - Homegrade kan u helpen: https://homegrade.brussels/ 

Het **uitrusten van het dak** voor **industriële** of **grote kantoorruimtes**.

  - De [Facilitator Duurzame Gebouwen](https://leefmilieu.brussels/themas/gebouwen-en-energie/gratis-begeleiding/facilitator-duurzame-gebouwen) zal u de nodige ondersteuning bieden: [facilitator@leefmilieu.brussels](facilitator@leefmilieu.brussels)

`;
