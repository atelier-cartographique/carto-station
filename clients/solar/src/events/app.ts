/*
 *  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import * as debug from 'debug';
import { fromNullable } from 'fp-ts/lib/Option';
import pointOnFeature from '@turf/point-on-feature';
import bbox from '@turf/bbox';

import { dispatch, query, assign } from 'sdi/shape';
import { queryReverseGeocoder } from 'sdi/ports/geocoder';
import { getLang } from 'sdi/app';
import { defaultInteraction } from 'sdi/map';
import { activity } from 'sdi/activity';

import {
    FeatureCollection,
    Feature,
    getFeatureProp,
    Properties,
} from 'sdi/source';

import { AppLayout } from '../app';
import {
    fetchGeom,
    fetchBuilding,
    fetchBaseLayerAll,
    fetchKey,
    fetchRoofs,
} from '../remote';
import { updateRoofs, clearPerspective, clearInputs } from './simulation';
import { Extent } from 'ol/extent';
import { Coordinate } from 'ol/coordinate';
import { updateGeocoderResponse, addRoofLayer, clearRoofLayer } from './map';
import { navigatePreview } from './route';
import {
    prodThresholdMedium,
    prodThresholdHigh,
    Tag,
    flatRoofTilt,
    usableRoofArea,
} from '../queries/simulation';
import { getCapakey, getBranchLargeArea } from '../queries/app';
import { isNotNullNorUndefined } from 'sdi/util';

const logger = debug('sdi:events/app');

export const activityLogger = activity('solar');

export const setLayout = (l: AppLayout) => {
    dispatch('app/layout', state => state.concat([l]));
    if ('Locate:Map' === l) {
        dispatch('port/map/interaction', () => ({
            label: 'singleclick',
            state: null,
        }));
    } else {
        dispatch('port/map/interaction', defaultInteraction);
    }
};

const centerMap = (capakey: string) => {
    const geoms = query('solar/data/geoms');
    if (capakey in geoms) {
        const [minx, miny, maxx, maxy] = bbox(geoms[capakey]);
        const width = maxx - minx;
        const height = maxy - miny;
        const sideMax = Math.max(width, height);
        const sideMin = Math.min(width, height);
        const abbox: Extent = [
            minx - (sideMax - sideMin) / 2,
            miny - (sideMax - sideMin) / 2,
            minx + sideMax,
            miny + sideMax,
        ];

        logger(`bbox: ${[minx, miny, maxx, maxy]}`);
        logger(`extent: ${abbox}`);

        dispatch('port/map/view', state => ({
            ...state,
            dirty: 'geo/extent',
            extent: abbox,
        }));
    }
};

export const reCenterMap = () => getCapakey().map(centerMap);

const createCollection = (fs: Feature[]): FeatureCollection => ({
    type: 'FeatureCollection',
    crs: {
        type: 'name',
        properties: {
            name: 'urn:ogc:def:crs:EPSG::31370',
        },
    },
    features: fs,
});

type roofFetcher = (c: string) => Promise<Feature>;

const tagFeature = (f: Feature): Properties => {
    const area = getFeatureProp(f, 'area', 0.000001);
    const irradiance = getFeatureProp(f, 'irradiance', 0);
    const productivity = getFeatureProp(f, 'productivity', 0);
    const tilt = getFeatureProp(f, 'tilt', 35);
    const tag = 'great';
    const andFlat = (t: Tag): Tag => {
        if (tilt > flatRoofTilt()) {
            return t;
        }
        switch (t) {
            case 'great':
                return 'great-flat';
            case 'good':
                return 'good-flat';
            case 'unusable':
                return 'unusable-flat';
            default:
                return t;
        }
    };

    if (0 === irradiance) {
        return { tag: 'not-computed' };
    } else if (area < 5) {
        return { tag: andFlat('unusable') };
    } else if (productivity < prodThresholdMedium()) {
        return { tag: andFlat('unusable') };
    } else if (
        productivity >= prodThresholdMedium() &&
        productivity < prodThresholdHigh()
    ) {
        return { tag: andFlat('good') };
    }
    return { tag: andFlat(tag) };
};

const loadRoof = (
    fetchRoof: roofFetcher,
    capakey: string,
    r: (a: FeatureCollection) => void,
    s: () => void
) => {
    const rids = query('solar/loading');
    if (rids.length > 0) {
        const rid = rids[0];
        fetchRoof(rid)
            .then(feature => {
                dispatch('solar/loading', ids => ids.filter(id => id !== rid));
                dispatch('solar/loaded', ids => [...ids, rid]);
                dispatch('solar/data/roofs', state => {
                    const ns = { ...state };
                    const props = feature.properties || {};
                    const tag = tagFeature(feature);
                    feature.properties = { ...props, ...tag };
                    if (capakey in ns) {
                        ns[capakey].features = state[capakey].features.concat([
                            feature,
                        ]);
                    } else {
                        ns[capakey] = createCollection([feature]);
                    }
                    return ns;
                });
            })
            .then(() => loadRoof(fetchRoof, capakey, r, s))
            .catch(s);
    } else {
        r(query('solar/data/roofs')[capakey]);
    }
};

const loadRoofs = (capakey: string) =>
    fromNullable(query('solar/data/roofs')[capakey]).foldL(
        () =>
            fetchRoofs(capakey).then(roofs => {
                dispatch('solar/loading', () =>
                    roofs.features.map(f => f.id.toString())
                );
                const fr = (c: string) =>
                    new Promise<Feature>((rs, rj) => {
                        setTimeout(() => {
                            const f = roofs.features.find(f => f.id === c);
                            if (f !== undefined) {
                                rs(f);
                            } else {
                                rj();
                            }
                        }, 2600 / roofs.features.length);
                    });
                return new Promise((solve, ject) =>
                    loadRoof(fr, capakey, solve, ject)
                );
            }),
        fc => Promise.resolve(fc)
    );

const loadGeometry = (capakey: string) =>
    fromNullable(query('solar/data/geoms')[capakey]).foldL(
        () =>
            fetchGeom(capakey).then(fc => {
                dispatch('solar/data/geoms', state => ({
                    ...state,
                    [capakey]: fc,
                }));
                return fc;
            }),
        fc => Promise.resolve(fc)
    );

const loadBuildings = (capakey: string) =>
    fromNullable(query('solar/data/buildings')[capakey]).foldL(
        () =>
            fetchBuilding(capakey).then(fc => {
                dispatch('solar/data/buildings', state => ({
                    ...state,
                    [capakey]: fc,
                }));
                return fc;
            }),
        fc => Promise.resolve(fc)
    );

const checkAddress = (ck: string) => {
    logger('checkAddress');
    const a = query('solar/address');
    if (a) {
        return Promise.resolve({});
    }
    const gs = query('solar/data/geoms');
    const fc = gs[ck];
    if (!isNotNullNorUndefined(fc)) {
        return Promise.reject();
    }
    const { geometry } = pointOnFeature(fc);
    if (null === geometry) {
        return Promise.reject();
    }
    const [x, y] = geometry.coordinates;
    return new Promise((resolve, reject) => {
        queryReverseGeocoder(x, y, getLang())
            .then(response => {
                if (!response.error) {
                    logger(`Got address ${response.result.address}`);
                    dispatch('solar/address', () => response.result.address);
                    resolve({});
                } else {
                    reject();
                }
            })
            .catch(reject);
    });
};

export const loadCoordinate = (coord: Coordinate) => {
    queryReverseGeocoder(coord[0], coord[1], getLang()).then(response => {
        if (!response.error) {
            dispatch('solar/address', () => response.result.address);
            updateGeocoderResponse(null);
            fetchKey(coord[0], coord[1])
                .then(({ capakey }) => navigatePreview(capakey))
                .catch((err: string) => {
                    logger(`Could not fetch a capakey: ${err}`);
                });
        }
    });
};

export const loadCapakey = (capakey: string) => {
    clearRoofLayer();
    clearPerspective();
    clearInputs();
    dispatch('app/capakey', () => capakey);
    dispatch('solar/loading', () => []);
    dispatch('solar/loaded', () => []);
    const loaders = [
        // loadGeometry(capakey),
        loadRoofs(capakey),
        loadBuildings(capakey),
    ];

    return loadGeometry(capakey)
        .then(() => checkAddress(capakey))
        .then(() => centerMap(capakey))
        .then(() => {
            return Promise.all(loaders)
                .then(() => updateRoofs(capakey))
                .then(() => addRoofLayer(capakey));
        });
};

export const loadAllBaseLayers = (url: string) => {
    fetchBaseLayerAll(url).then(blc => {
        dispatch('data/baselayers', () => blc);
    });
};

export const set3dWidth = (n: number) =>
    dispatch('solar/perspective/width', width => {
        if (n !== width) {
            assign('solar/perspective/src', null);
        }
        return n;
    });

export const toggleBranchLargeArea = () =>
    dispatch('solar/large-area/branch', v => !v);

export const LARGE_ROOF = 200;
export const branchLargeRoof = () => {
    if (usableRoofArea() > LARGE_ROOF && getBranchLargeArea()) {
        setLayout('LargeArea');
        toggleBranchLargeArea();
        return true;
    }
    return false;
};

logger('loaded');
