import { getUserData, tryLogout } from 'sdi/app';

import { DIV, H1 } from 'sdi/components/elements';
import tr from 'sdi/locale';

import { makeLabelAndIcon } from '../button';

const logoutButton = makeLabelAndIcon('logout', 1, 'sign-out-alt', () =>
    tr.login('logout')
);
const username = () =>
    DIV(
        { className: 'logout-username' },
        getUserData().fold('', u => u.name)
    );

const render = () =>
    DIV(
        { className: 'login-wrapper' },
        H1({}, tr.login('connectionSDI')),
        DIV({ className: 'login-widget' }, username(), logoutButton(tryLogout))
    );

export default render;
