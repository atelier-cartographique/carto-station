from pathlib import PosixPath
from json import loads
from django.http import HttpResponseBadRequest

ROOT = PosixPath("/")


def find_version(dir: PosixPath):
    path = dir.joinpath("version.json")
    if path.exists():
        return path.read_text()
    if dir == ROOT:
        raise Exception(
            "Reached the root of the filesystem without finding a version file"
        )

    return find_version(dir.parent.absolute())


def version(get_response):
    CARTOSTATION_VERSION = loads(find_version(PosixPath(__file__).parent.absolute()))

    MAJOR = CARTOSTATION_VERSION["major"]
    MINOR = CARTOSTATION_VERSION["minor"]
    PATCH = CARTOSTATION_VERSION["patch"]

    CS_HEADER = "Carto-Station"
    VERSION_STRING = f"{MAJOR}.{MINOR}.{PATCH}"

    def middleware(request):
        try:  # there is no header when working on api points
            if (
                CS_HEADER in request.headers
                and request.headers.get(CS_HEADER) != VERSION_STRING
            ):
                response = HttpResponseBadRequest(
                    f"Version Mismatch: server {VERSION_STRING}"
                )
            else:
                response = get_response(request)

            response.headers["Carto-Station"] = VERSION_STRING

        except Exception:
            response = get_response(request)
        return response

    return middleware
