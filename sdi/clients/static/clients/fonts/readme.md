le principe est d'avoir deux fonts par défaut :

- une font carto
- une font interface

Ces fonts par défaut sont dans `/sdi/clients/static/clients/fonts`.

il faudrait :

- Enlever tout ce qui ne sert à rien dans `/clients/platform` à ce sujet
- Vérifier et supprimer les dépendances à FontAwesome
- Analyser si on conserve un lien avec ForkAwesome pour la font d'interface
- Optimiser les fonts, avec un subset adéquat.

---

12/07/2024

- le fichier forawesome.ttf à été optimisé une première fois (suppression des icones liées à des marques)
