#########################################################################
#  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#########################################################################

from pathlib import PosixPath
from django.contrib.staticfiles.finders import find as find_static
from django.contrib.auth.models import Group
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from collections import OrderedDict

import json
import logging


logger = logging.getLogger(__name__)
CLIENTS = getattr(settings, "CLIENTS")
STATIC_ROOT = getattr(settings, "STATIC_ROOT", None)


def get_version(name):
    """looking for a version file for an app

    Note that depending on settings and workflow,
    those files might end up in different places
    with different names.
    We'll try to account for all of them.
    """

    # out of source builds with STATICFILES_DIRS
    # this comes first as it should be the production env setup
    version_path = find_static("apps/{}.version".format(name), False)

    if version_path is None and STATIC_ROOT is not None:
        # out of source builds without STATICFILES_DIRS
        vp_attempt = PosixPath(STATIC_ROOT).joinpath(
            "clients/apps/{}.version".format(name)
        )
        if vp_attempt.exists():
            version_path = vp_attempt.as_posix()

    if version_path is None:
        # we might be on a dev machine with in-source
        # builds which `AppDirectoriesFinder` will find.
        version_path = find_static("clients/apps/{}.version".format(name))

    try:
        with open(version_path) as version_file:
            return json.load(version_file)
    except Exception as ex:
        logger.error("Could not get a version for {}:\n\t{}".format(name, ex))
        return None


class ClientConfig:
    configured_clients = None  # configure_clients()

    @classmethod
    def get_items(cls):
        if cls.configured_clients is not None:
            return cls.configured_clients.items()

        cls.configured_clients = OrderedDict()
        for client in CLIENTS:
            info = dict()
            route = client["route"]
            module = client.get("module", route)
            name = client.get("name", None)
            login = client.get("login", False)
            groups = client.get("groups", None)
            poster = client.get("poster", None)
            display_option = client.get("display", None)
            version = get_version(module)

            display = dict(name=name, option=display_option)

            try:
                if groups is not None:
                    info["groups"] = [Group.objects.get(name=gn) for gn in groups]
            except ImproperlyConfigured:
                # Can happen when processing commands (e.g. collectstatic)
                logger.warning("could not get groups because of a lack of DB config")
                info["groups"] = []

            if name is not None:
                info["name"] = name

            info["version"] = version
            info["login"] = login
            info["route"] = route
            info["module"] = module
            info["poster"] = poster
            info["display"] = display

            cls.configured_clients[route] = info

            logger.info("configured client - {} ==============".format(route))
            logger.info("name:\t{}".format(name))
            logger.info("groups:\t{}".format(groups))
            logger.info("login:\t{}".format(login))
            logger.info("version:\t{}".format(version))

        return cls.configured_clients.items()

    @classmethod
    def get(cls, app_name):
        if cls.configured_clients is None:
            cls.get_items()

        return cls.configured_clients[app_name]
