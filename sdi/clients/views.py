#
#  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from collections import namedtuple
import json

from django.shortcuts import render, redirect
from django.urls import reverse
from django.conf import settings
from django.http import (
    Http404,
)
from django.middleware.csrf import get_token
from django.views.decorators.clickjacking import xframe_options_exempt
from django.contrib.auth.models import Group
from api.models import HighlightedBaseLayer
from api.serializers.map import (
    serialize_highlight_base_layer,
)
from api.serializers.user import serialize_user
from maintenance.models import MaintenanceEvent, AppMessage
from maintenance.serializers import serialize_maintenance_event, serialize_appmessage
from .models import ClientConfig, get_version

import logging

_logger = logging.getLogger(__name__)

Resource = namedtuple("Resource", ["mtime", "data"])
Client = namedtuple("Client", ["module", "display", "route", "url", "poster"])

DEBUG = getattr(settings, "DEBUG", False)
CLIENTS_DEFAULT = getattr(settings, "CLIENTS_DEFAULT")
CLIENTS_LOGIN = getattr(settings, "CLIENTS_LOGIN", "login")
CLIENTS_URLS = getattr(settings, "CLIENTS_URLS", [])
ADMIN_QUERY_NAME = getattr(settings, "ADMIN_QUERY_GROUP", None)
ADMIN_QUERY = (
    Group.objects.get(name=ADMIN_QUERY_NAME) if ADMIN_QUERY_NAME is not None else None
)
CLIENTS_FONT_PICTO = getattr(
    settings,
    "CLIENTS_FONT_PICTO",
    settings.STATIC_URL + "clients/fonts/forkawesome.woff2",
)
CLIENTS_FONT_LABEL = getattr(
    settings,
    "CLIENTS_FONT_LABEL",
    settings.STATIC_URL + "clients/fonts/u001-regular.woff2",
)


def in_group(user, group_list):
    groups = set([g.id for g in group_list])
    user_groups = set([g.id for g in user.groups.all()])
    diff = groups.difference(user_groups)

    return len(diff) < len(groups)


def filter_clients(request):
    clients = []

    for route, info in ClientConfig.get_items():
        groups = info.get("groups", None)
        display = info.get("display", None)
        module = info.get("module", route)
        poster = info.get("poster", None)
        url = reverse("clients.root", args=(route, ""))
        if groups is None or in_group(request.user, groups):
            clients.append(Client(module, display, route, url, poster))

    return clients


@xframe_options_exempt
def render_index(request, app_name, path):
    user = None
    require_login = ClientConfig.get(app_name)["login"]
    if request.user.is_authenticated:
        user = serialize_user(request.user)
    elif require_login:
        next = "{}/{}".format(app_name, path)
        login_url = reverse("clients.root", args=(CLIENTS_LOGIN, next))
        return redirect(login_url)

    client_config = ClientConfig.get(app_name)
    if DEBUG:
        version = get_version(client_config["module"])
    else:
        version = client_config["version"]

    if version is None:
        raise Http404('There\'s no version for "{}"'.format(app_name))

    highlighted_baselayers = [
        serialize_highlight_base_layer(bl) for bl in HighlightedBaseLayer.objects.all()
    ]

    maintenance = [
        serialize_maintenance_event(e) for e in MaintenanceEvent.objects.announced()
    ]
    app_messages = [serialize_appmessage(am) for am in AppMessage.objects.announced()]
    # _logger.debug(app_messages)

    root_url = request.build_absolute_uri(reverse("clients.index"))
    api_url = request.build_absolute_uri(reverse("api-root"))
    return render(
        request,
        "clients/app_index.html",
        context=dict(
            user=user,
            highlighted_baselayers=json.dumps(highlighted_baselayers),
            path=path,
            root=root_url,
            api=api_url,
            apps=json.dumps(filter_clients(request)),
            csrf_token=get_token(request),
            app_name=app_name,
            bundle_url="clients/apps/" + version["bundle"],
            style_url="clients/apps/" + version["style"],
            urls=json.dumps(CLIENTS_URLS),
            fonts=json.dumps(
                {
                    "picto": CLIENTS_FONT_PICTO,
                    "label": CLIENTS_FONT_LABEL,
                }
            ),
            admin_query=ADMIN_QUERY,
            maintenance=json.dumps(maintenance),
            app_messages=json.dumps(app_messages),
        ),
    )


def app_index(request, app_name, path):
    for client in filter_clients(request):
        if client.route == app_name:
            return render_index(request, app_name, path)

    raise Http404("{} not configured".format(app_name))


def index(request):
    for client in filter_clients(request):
        if client.route == CLIENTS_DEFAULT:
            return render_index(request, CLIENTS_DEFAULT, "")

    raise Http404("Missing default application")
