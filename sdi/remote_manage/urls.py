from django.urls import re_path as url
from . import views

urlpatterns = [
    url(
        r"^api/metadata/sync/$",
        views.api_metadata_sync,
        name="manage.api_metadata_sync",
    ),
]
