from django.urls import path
from .views import get_online_meter

urlpatterns = [
    path('meter/<int:source>',
        get_online_meter,
        name='online.get_online_meter'),
]