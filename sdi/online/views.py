from django.http import JsonResponse
from datetime import datetime

def get_online_meter(request, source):
    now = datetime.now()
    return JsonResponse({
        'source': source,
        'serverTime': int(now.timestamp() * 1000),
    })
