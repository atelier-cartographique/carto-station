from datetime import datetime, timedelta
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.admin.models import LogEntry


def index(request):
    return render(request, "main/index.html", context=dict())


def serialize_log_entry(entry: LogEntry):
    user = entry.user.get_full_name()
    if len(user) < 2:
        user = entry.user.username
    return {
        "date": entry.action_time,
        "user": user,
        "action": entry.get_action_flag_display(),
        "object_type": entry.content_type.app_labeled_name,
        "object_id": entry.object_id,
        "object_repr": entry.object_repr,
    }


@login_required
def get_admin_logs(request, days=10):
    delta = timedelta(days=days)
    start = datetime.now() - delta
    logs = LogEntry.objects.filter(action_time__gt=start).order_by("-action_time")
    context = {"entries": [serialize_log_entry(e) for e in logs]}

    return render(request, "main/admin_log.html", context)
