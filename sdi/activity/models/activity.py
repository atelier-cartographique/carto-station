from django.db import models
from datetime import datetime
from lingua.models.record import LANGUAGES
import itertools

class Activity(models.Model):
 
    id=models.BigAutoField(primary_key = True)
    datetime = models.DateTimeField(auto_now=False, auto_now_add=False)
    lang = models.CharField(max_length=6)
    namespace = models.CharField(max_length=128)
    action = models.CharField(max_length=128)
    parameter = models.JSONField()

    # class Meta:
    #     verbose_name = _("")
    #     verbose_name_plural = _("s")

    def __str__(self):
        return f'{self.id} {self.action} ({self.namespace})'

    def get_absolute_url(self):
        return reverse("_detail", kwargs={"pk": self.pk})



class ActivityValidationError(Exception):
    def __init__(self, key, message):
        self.key = key
        self.message = message

    def __str__(self):
        return f'activity validation error: {self.key} \n {self.message}'
    

def validate_key(data, key):
    try:
        return data[key]
    except KeyError:
        raise ActivityValidationError(key, 'missing key from activity data')

def validate_activity(data):
    a = Activity()

    datetime_value = validate_key(data, 'datetime')
    try:
        dt = datetime.fromtimestamp(datetime_value/1000)
        a.datetime = dt
    except Exception:
        raise ActivityValidationError('datetime', f'could not build datetime from "{datetime_value}"')

    lang_value = validate_key(data, 'lang')
    try:
        lang_value in LANGUAGES
        a.lang = lang_value
    except Exception:
        raise ActivityValidationError('lang', f'language ("{lang_value}") not in LANGUAGES')

    namespace_value = validate_key(data, 'namespace')
    try:
        type(namespace_value) == str
        a.namespace = namespace_value
    except Exception:
        raise ActivityValidationError('namespace', f'namespace "{namespace_value}" is not a str')

    action_value = validate_key(data, 'action')
    try:
        type(action_value) == str
        a.action = action_value
    except Exception:
        raise ActivityValidationError('action', f'action "{action_value}" not found or is not a str')


    a.parameter = validate_key(data, 'parameter')

    return a
