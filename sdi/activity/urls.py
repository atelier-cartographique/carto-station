from django.urls import path, include
from rest_framework import routers
from .views import (
    ActivityProfileViewSet,
    get_all_activity,
    post_activity,
    get_activity_token,
    get_activity,
    get_default_dashboard,
)


# router = routers.DefaultRouter(trailing_slash=False)

profile_router = routers.SimpleRouter()
profile_router.register("profiles", ActivityProfileViewSet)

urlpatterns = [
    path("token", get_activity_token, name="activity.get_token"),
    path("post/", post_activity, name="activity.post"),
    path(
        "<namespace>/<int:begin>/<int:end>", get_activity, name="activity.get_activity"
    ),
    path("<int:begin>/<int:end>", get_all_activity, name="activity.get_all_activity"),
    path("", include(profile_router.urls)),
    path(
        "default_dashboard",
        get_default_dashboard,
        name="activity.default_dashboard",
    ),
]
