import logging

_logger = logging.getLogger(__name__)
i = 10000
with open("./pathogens.txt") as f:
    for f in f.readlines():
        _logger.debug("- model: sgpa.TreePathogen")
        _logger.debug("  pk: " + str(i))
        _logger.debug("  fields:")
        _logger.debug("    label: {fr: " + f.rstrip() + "}")
        i = i + 1
