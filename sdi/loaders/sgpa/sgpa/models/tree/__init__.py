import datetime
import decimal
from typing import Any, Optional, List
from django.contrib.gis.db import models
from django.db import transaction
from django.db.models import QuerySet
from psycopg2.extras import DateTimeTZRange, NumericRange
from .classification import (
    TreeKind,
    TreeStatus,
    TreeStructure,
    TreeEnvironment,
    TreeSymptomRoot,
    TreeSymptomTrunk,
    TreeSymptomLeave,
    TreeSymptomBranch,
    TreeSymptomCollar,
    TreePathogen,
    TreePest,
    TreeEpiphyte,
)
from .histories import (
    TreeStatusHistory,
    TreeOntogenesisHistory,
    TreeDendrometryHistory,
    TreeStructureHistory,
    TreeEnvironmentHistory,
    TreeVitalityHistory,
    TreeSymptomRootHistory,
    TreeSymptomTrunkHistory,
    TreeSymptomLeaveHistory,
    TreeSymptomBranchHistory,
    TreeSymptomCollarHistory,
    TreePathogenHistory,
    TreePestHistory,
    TreeEpiphyteHistory,
    TreeRiskAnalysisHistory,
)
from ..report import TreeReport
from ..park import Park
from .comment import Comment
from .owner import TreeOwner


class Dendrometry:
    def __init__(
        self,
        height: decimal,
        circumference150: decimal,
        branches_count: int,
        crown_diameter_meter: Optional[decimal.Decimal] = None,
        crown_diameter_range: Optional[NumericRange] = None,
    ):
        self.height: int = height
        self.circumference150: int = circumference150
        self.branches_count: int = branches_count
        self.crown_diameter_meter: NumericRange

        if crown_diameter_meter is not None:
            self.crown_diameter_range = self._build_range(crown_diameter_meter)
        elif crown_diameter_range is not None:
            self.crown_diameter_range = crown_diameter_range
        else:
            raise ValueError("Crown diameter")

    @staticmethod
    def from_history(history: TreeDendrometryHistory):
        return Dendrometry(
            history.height,
            history.circumference150,
            history.branches_count,
            crown_diameter_range=history.crown_diameter_range,
        )

    @staticmethod
    def _build_range(crown_diameter: decimal.Decimal) -> NumericRange:
        if crown_diameter == 0:
            return NumericRange(lower=0, upper=5)
        if crown_diameter > 30:
            return NumericRange(lower=30)

        ranges = [
            (0.0, 5.0),
            (5.0, 10.0),
            (10.0, 15.0),
            (15.0, 20.0),
            (20.0, 25.0),
            (25.0, 30.0),
        ]
        for l, u in ranges:
            if l < crown_diameter <= u:
                return NumericRange(lower=l, upper=u)

        raise ValueError("Crown diameter is not in range")


class RiskAnalysis:
    def __init__(self, break_risk, size_risk, target_risk):
        self.break_risk = break_risk
        self.size_risk = size_risk
        self.target_risk = target_risk

    @property
    def hazard(self):
        return self.break_risk + self.size_risk + self.target_risk


def _is_current(objects: QuerySet) -> Optional[Any]:
    for o in objects.all():
        if o.validity.upper_inf:
            return o
    return None


@transaction.atomic
def _set_current_item(objects: QuerySet, report: Optional[TreeReport], **kwargs):
    current = _is_current(objects)
    if current is not None:
        current.validity = DateTimeTZRange(
            current.validity.lower, upper=datetime.datetime.now()
        )
        current.save()
    objects.create(
        report=report, validity=DateTimeTZRange(lower=datetime.datetime.now()), **kwargs
    )

def get_tree_park(point) -> Optional[Park]:
    qs = Park.objects.filter(multipolygon__intersects=point)
    if qs.exists():
        return qs.first()
    else:
        return None

class Tree(models.Model):
    """A single tree"""
    point = models.PointField(null=True, srid=31370)
    kind = models.ForeignKey(TreeKind, models.RESTRICT)
    park = models.ForeignKey(
        Park, on_delete=models.PROTECT, related_name="trees", null=True, blank=True
    )
    pin = models.IntegerField(null=True, blank=True)
    remarkable_tree_id = models.IntegerField(null=True, blank=True)
    cut = models.IntegerField(null=True, blank=True)
    owner = models.ForeignKey(TreeOwner, on_delete=models.RESTRICT, null=True, default=None)

    def _set_in_history(self, object_set, **kwargs):
        pass

    @property
    def current_status_history(self) -> Optional[TreeStatusHistory]:
        return _is_current(self.status_history)

    @property
    def current_status(self) -> Optional[TreeStatus]:
        current_status_h = self.current_status_history
        return current_status_h.status if current_status_h is not None else None

    def set_status(self, status: TreeStatus, report: Optional[TreeReport] = None):
        _set_current_item(self.status_history, report, status=status)

    @property
    def current_ontogenesis_history(self) -> Optional[TreeOntogenesisHistory]:
        return _is_current(self.ontogenesis_history)

    @property
    def current_ontogenesis(self):
        current_ontogenesis_h = self.current_ontogenesis_history
        return current_ontogenesis_h.ontogenesis if current_ontogenesis_h is not None else None

    def set_ontogenesis(self, ontogenesis: int, report: Optional[TreeReport] = None):
        _set_current_item(self.ontogenesis_history, report, ontogenesis=ontogenesis)

    @property
    def current_dendrometry_history(self) -> Optional[TreeDendrometryHistory]:
        return _is_current(self.dendrometry_history)

    @property
    def current_dendrometry(self) -> Optional[Dendrometry]:
        h = self.current_dendrometry_history
        if h is not None:
            return Dendrometry.from_history(h)
        return None

    def set_dendrometry(
        self, dendrometry: Dendrometry, report: Optional[TreeReport] = None
    ):
        _set_current_item(
            self.dendrometry_history,
            report,
            height=dendrometry.height,
            circumference150=dendrometry.circumference150,
            crown_diameter_range=dendrometry.crown_diameter_range,
            branches_count=dendrometry.branches_count,
        )

    @property
    def current_structure_history(self) -> Optional[TreeStructureHistory]:
        return _is_current(self.structure_history)

    @property
    def current_structure(self) -> Optional[TreeStructure]:
        c = self.current_structure_history
        return c.structure if c is not None else None

    def set_structure(
        self, structure: TreeStructure, report: Optional[TreeReport] = None
    ):
        _set_current_item(self.structure_history, report, structure=structure)

    @property
    def current_environment_history(self) -> Optional[TreeEnvironmentHistory]:
        return _is_current(self.environment_history)

    @property
    def current_environment(self):
        c = self.current_environment_history
        return c.environments if c is not None else None

    def set_environment(
        self, environments: List[TreeEnvironment], report: Optional[TreeReport] = None
    ):
        """Add a set of environments to the tree"""
        current = _is_current(self.environment_history)
        if current is not None:
            current.validity = DateTimeTZRange(
                current.validity.lower, upper=datetime.datetime.now()
            )
            current.save()

        e = self.environment_history.create(
            report=report, validity=DateTimeTZRange(lower=datetime.datetime.now())
        )
        e.environments.set(environments)

    @property
    def current_vitality_history(self) -> Optional[TreeVitalityHistory]:
        return _is_current(self.vitality_history)

    @property
    def current_vitality(self) -> Optional[int]:
        c = self.current_vitality_history
        return self.current_vitality_history.vitality if c is not None else None

    def set_vitality(self, vitality: int, report: Optional[TreeReport] = None):
        _set_current_item(self.vitality_history, report, vitality=vitality)

    @property
    def current_symptom_root_history(self) -> Optional[TreeSymptomRootHistory]:
        return _is_current(self.symptom_root_history)

    @property
    def current_symptom_root(self) -> Optional[TreeSymptomRoot]:
        c = self.current_symptom_root_history
        return c.symptoms if c is not None else None

    def set_symptom_root(
        self, symptoms: List[TreeSymptomRoot], report: Optional[TreeReport] = None
    ):
        """Add a set of symptoms"""
        current = _is_current(self.symptom_root_history)
        if current is not None:
            current.validity = DateTimeTZRange(
                current.validity.lower, upper=datetime.datetime.now()
            )
            current.save()

        e = self.symptom_root_history.create(
            report=report, validity=DateTimeTZRange(lower=datetime.datetime.now())
        )
        e.symptoms.set(symptoms)

    @property
    def current_symptom_trunk_history(self) -> Optional[TreeSymptomTrunkHistory]:
        return _is_current(self.symptom_trunk_history)

    @property
    def current_symptom_trunk(self) -> Optional[TreeSymptomTrunk]:
        c = self.current_symptom_trunk_history
        return c.symptoms if c is not None else None

    def set_symptom_trunk(
        self, symptoms: List[TreeSymptomTrunk], report: Optional[TreeReport] = None
    ):
        """Add a set of symptoms"""
        current = _is_current(self.symptom_trunk_history)
        if current is not None:
            current.validity = DateTimeTZRange(
                current.validity.lower, upper=datetime.datetime.now()
            )
            current.save()

        e = self.symptom_trunk_history.create(
            report=report, validity=DateTimeTZRange(lower=datetime.datetime.now())
        )
        e.symptoms.set(symptoms)

    @property
    def current_symptom_leave_history(self) -> Optional[TreeSymptomLeaveHistory]:
        return _is_current(self.symptom_leave_history)

    @property
    def current_symptom_leave(self) -> Optional[TreeSymptomLeave]:
        c = self.current_symptom_leave_history
        return c.symptoms if c is not None else None

    def set_symptom_leave(
        self, symptoms: List[TreeSymptomLeave], report: Optional[TreeReport] = None
    ):
        """Add a set of symptoms"""
        current = _is_current(self.symptom_leave_history)
        if current is not None:
            current.validity = DateTimeTZRange(
                current.validity.lower, upper=datetime.datetime.now()
            )
            current.save()

        e = self.symptom_leave_history.create(
            report=report, validity=DateTimeTZRange(lower=datetime.datetime.now())
        )
        e.symptoms.set(symptoms)

    @property
    def current_symptom_branch_history(self) -> Optional[TreeSymptomBranchHistory]:
        return _is_current(self.symptom_branch_history)

    @property
    def current_symptom_branch(self) -> Optional[TreeSymptomBranch]:
        c = self.current_symptom_branch_history
        return c.symptoms if c is not None else None

    def set_symptom_branch(
        self, symptoms: List[TreeSymptomBranch], report: Optional[TreeReport] = None
    ):
        """Add a set of symptoms"""
        current = _is_current(self.symptom_branch_history)
        if current is not None:
            current.validity = DateTimeTZRange(
                current.validity.lower, upper=datetime.datetime.now()
            )
            current.save()

        e = self.symptom_branch_history.create(
            report=report, validity=DateTimeTZRange(lower=datetime.datetime.now())
        )
        e.symptoms.set(symptoms)

    @property
    def current_symptom_collar_history(self) -> Optional[TreeSymptomCollarHistory]:
        return _is_current(self.symptom_collar_history)

    @property
    def current_symptom_collar(self) -> Optional[TreeSymptomCollar]:
        c = self.current_symptom_collar_history
        return c.symptoms if c is not None else None

    def set_symptom_collar(
        self, symptoms: List[TreeSymptomCollar], report: Optional[TreeReport] = None
    ):
        """Add a set of symptoms"""
        current = _is_current(self.symptom_collar_history)
        if current is not None:
            current.validity = DateTimeTZRange(
                current.validity.lower, upper=datetime.datetime.now()
            )
            current.save()

        e = self.symptom_collar_history.create(
            report=report, validity=DateTimeTZRange(lower=datetime.datetime.now())
        )
        e.symptoms.set(symptoms)

    @property
    def current_pathogen_history(self) -> Optional[TreePathogenHistory]:
        return _is_current(self.pathogen_history)

    @property
    def current_pathogen(self) -> Optional[TreePathogen]:
        c = self.current_pathogen_history
        return c.pathogens if c is not None else None

    def set_pathogen(
        self, pathogens: List[TreePathogen], report: Optional[TreeReport] = None
    ):
        """Add a set of pathogens"""
        current = _is_current(self.pathogen_history)
        if current is not None:
            current.validity = DateTimeTZRange(
                current.validity.lower, upper=datetime.datetime.now()
            )
            current.save()

        e = self.pathogen_history.create(
            report=report, validity=DateTimeTZRange(lower=datetime.datetime.now())
        )
        e.pathogens.set(pathogens)

    @property
    def current_pest_history(self) -> Optional[TreePestHistory]:
        return _is_current(self.pest_history)

    @property
    def current_pest(self) -> Optional[TreePest]:
        c = self.current_pest_history
        return c.pests if c is not None else None

    def set_pest(self, pests: List[TreePest], report: Optional[TreeReport] = None):
        """Add a set of pests"""
        current = _is_current(self.pathogen_history)
        if current is not None:
            current.validity = DateTimeTZRange(
                current.validity.lower, upper=datetime.datetime.now()
            )
            current.save()

        e = self.pest_history.create(
            report=report, validity=DateTimeTZRange(lower=datetime.datetime.now())
        )
        e.pests.set(pests)

    @property
    def current_epiphyte_history(self) -> Optional[TreeEpiphyteHistory]:
        return _is_current(self.epiphyte_history)

    @property
    def current_epiphyte(self) -> Optional[TreeEpiphyte]:
        c = self.current_epiphyte_history
        return c.epiphytes if c is not None else None

    def set_epiphyte(
        self, epiphytes: TreeEpiphyte, report: Optional[TreeReport] = None
    ):
        """Add a set of epiphytes"""
        current = _is_current(self.epiphyte_history)
        if current is not None:
            current.validity = DateTimeTZRange(
                current.validity.lower, upper=datetime.datetime.now()
            )
            current.save()

        e = self.epiphyte_history.create(
            report=report, validity=DateTimeTZRange(lower=datetime.datetime.now())
        )
        e.epiphytes.set(epiphytes)

    @property
    def current_risk_analysis_history(self) -> Optional[TreeRiskAnalysisHistory]:
        return _is_current(self.risk_analysis_history)

    @property
    def current_risk_analysis(self) -> Optional[RiskAnalysis]:
        c = self.current_risk_analysis_history
        return (
            RiskAnalysis(
                break_risk=c.break_risk,
                size_risk=c.size_risk,
                target_risk=c.target_risk,
            )
            if c is not None
            else None
        )

    def set_risk_analysis(
        self, risk_analysis: RiskAnalysis, report: Optional[TreeReport] = None
    ):
        _set_current_item(
            self.risk_analysis_history,
            report,
            break_risk=risk_analysis.break_risk,
            size_risk=risk_analysis.size_risk,
            target_risk=risk_analysis.target_risk,
        )

    @property
    def code_site(self) -> Optional[str]:
        return get_tree_park(self.point)



class CachedTree(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    data = models.JSONField()