#########################################################################
#  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#########################################################################

from pathlib import PosixPath
from django.db import models
from django.contrib.auth.models import User
from django.utils.text import slugify
from sorl.thumbnail import get_thumbnail
from sgpa.models import Tree


def upload_target(instance, filename):
    path = PosixPath(filename)
    ext = path.suffix
    name = slugify(path.stem)
    park = instance.tree.park.id if instance.tree.park else "rbc"
    user = instance.user.get_username()
    return f"sgpa/{park}/{user}/{name}{ext}"


class Image(models.Model):
    SIZE_SMALL = "small"
    SIZE_MEDIUM = "medium"
    SIZE_LARGE = "large"
    SIZES = {SIZE_SMALL: "300x300", SIZE_MEDIUM: "600x600", SIZE_LARGE: "1200x1200"}

    id = models.AutoField(primary_key=True)
    created_at = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to=upload_target)
    tree = models.ForeignKey(Tree, related_name="images", on_delete=models.CASCADE)
    user = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        related_name="+",
    )

    def thumbnail(self, size):
        if size in Image.SIZES:
            thumbnail = get_thumbnail(
                self.image, Image.SIZES[size], crop="center", quality=50
            )
            return open(thumbnail.storage.path(thumbnail.name), "rb")
        raise Exception(
            "thumbnail size not supporter. Should be one of: small medium large"
        )
