from django.test import TestCase
from psycopg2.extras import NumericRange
from sgpa.models import Dendrometry
from decimal import Decimal


class TestDendrometry(TestCase):
    def test_build_range(self):
        d = Dendrometry(height=10, circumference150=100, branches_count=1, crown_diameter_meter=Decimal('3.0'))
        self.assertIsNotNone(d.crown_diameter_range)
        self.assertIsInstance(d.crown_diameter_range, NumericRange)
        self.assertEqual(0, d.crown_diameter_range.lower)
        self.assertEqual(5, d.crown_diameter_range.upper)
