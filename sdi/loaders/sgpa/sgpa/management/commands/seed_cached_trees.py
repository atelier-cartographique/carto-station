from django.core.management.base import BaseCommand
from sgpa.models import Tree, CachedTree
from sgpa.serializers import serialize_tree
import logging

class Command(BaseCommand):
    def handle(self, *args, **options):
        CachedTree.objects.all().delete()
        for tree in Tree.objects.all():
            logging.info(f"processing {tree.id}")
            CachedTree.objects.create(id=tree.id, data=serialize_tree(tree))
