from django.contrib import admin
from django.contrib.gis.admin.options import OSMGeoAdmin
from django.contrib.gis.forms import OSMWidget
from django.db.models import Value
from django.db.models.functions import Concat
from sgpa.models import (
    Park,
    ParkColor,
    RecommendationKind,
    Selection,
    TreeKind,
    TreeEnvironment,
    TreeEpiphyte,
    TreePathogen,
    TreePest,
    TreeStatus,
    TreeStructure,
    TreeSymptomBranch,
    TreeSymptomCollar,
    TreeSymptomLeave,
    TreeSymptomRoot,
    TreeSymptomTrunk,
    UserPreferences,
    WorkKind
)

admin.site.register(ParkColor)
admin.site.register(RecommendationKind)
admin.site.register(TreeEnvironment)
admin.site.register(TreeEpiphyte)
admin.site.register(TreePathogen)
admin.site.register(TreePest)
admin.site.register(TreeStatus)
admin.site.register(TreeStructure)
admin.site.register(TreeSymptomBranch)
admin.site.register(TreeSymptomCollar)
admin.site.register(TreeSymptomLeave)
admin.site.register(TreeSymptomRoot)
admin.site.register(TreeSymptomTrunk)
admin.site.register(WorkKind)


@admin.register(Park)
class ParkAdmin(admin.ModelAdmin):
    list_display = ("id", "trigramme", "name")
    search_fields = ["name"]


@admin.register(TreeKind)
class TreeKindAdmin(admin.ModelAdmin):
    list_display = ("id", "latin_name", "common_name")
    search_fields = ["label", "common_name", "parent__label"]
    list_per_page = 700
    ordering = ('parent__label', 'label')



@admin.register(Selection)
class SelectionAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "date_created")
    search_fields = ["name"]


@admin.register(UserPreferences)
class UserPreferencesAdmin(admin.ModelAdmin):
    list_display = ("id", "user")
    search_fields = ["user"]