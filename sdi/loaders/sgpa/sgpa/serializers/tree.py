from typing import Optional, Dict
from typing_extensions import deprecated

from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.contrib.gis.geos import Point
from math import isnan
from django.utils.datetime_safe import datetime

from sgpa.models.image import Image
from sgpa.serializers.image import serialize_tree_image

from ..models import (
    Tree,
    TreeKind,
    TreeStatus,
    Dendrometry,
    TreeStructure,
    TreeEnvironment,
    TreeSymptomBranch,
    TreeSymptomCollar,
    TreeSymptomLeave,
    TreeSymptomRoot,
    TreeSymptomTrunk,
    TreePathogen,
    TreePest,
    TreeEpiphyte,
    Recommendation,
    RecommendationKind,
    RiskAnalysis,
    Permit,
    WorkTree,
    WorkKind,
    TreeOwner,
    get_tree_park,
    Comment,
)
from .functions import (
    geojson,
    serialize_history,
    serialize_nullable_history_obj,
    serialize_nullable_history_multi_obj,
    serialize_datetime,
    serialize_date,
    deserialize_datetime,
)
from sgpa.models.tree.classification import get_tree_kind_hierarchy


def serialize_tree_kind(tk: TreeKind):
    return {
        "id": tk.id,
        "latin_name": tk.latin_name,
        "label": tk.common_name if tk.common_name is not None else {},
    }


def serialize_dict_item(item):
    return {
        "id": item.id,
        "label": item.label,
    }


def with_validity(value, validity):
    return {"value": value, "validity": serialize_datetime(validity)}


def serialize_reference():
    status = [
        serialize_dict_item(s)
        for s in TreeStatus.objects.exclude(pk=9999).order_by("label")
    ]
    if TreeStatus.objects.filter(pk=9999).exists():
        status += [serialize_dict_item(TreeStatus.objects.get(pk=9999))]

    structure = [
        serialize_dict_item(s)
        for s in TreeStructure.objects.exclude(pk=9999).order_by("label")
    ]
    if TreeStructure.objects.filter(pk=9999).exists():
        structure += [serialize_dict_item(TreeStructure.objects.get(pk=9999))]

    return {
        "status": status,
        "kind": [
            serialize_tree_kind(tk)
            for tk in TreeKind.objects.all().order_by("parent__label", "label")
        ],
        "structure": structure,
        "environment": [
            serialize_dict_item(s)
            for s in TreeEnvironment.objects.all().order_by("label")
        ],
        "symptoms": {
            "branch": [
                serialize_dict_item(s)
                for s in TreeSymptomBranch.objects.all().order_by("label")
            ],
            "collar": [
                serialize_dict_item(s)
                for s in TreeSymptomCollar.objects.all().order_by("label")
            ],
            "leave": [
                serialize_dict_item(s)
                for s in TreeSymptomLeave.objects.all().order_by("label")
            ],
            "root": [
                serialize_dict_item(s)
                for s in TreeSymptomRoot.objects.all().order_by("label")
            ],
            "trunk": [
                serialize_dict_item(s)
                for s in TreeSymptomTrunk.objects.all().order_by("label")
            ],
        },
        "pathogen": [
            serialize_dict_item(s) for s in TreePathogen.objects.all().order_by("label")
        ],
        "pest": [
            serialize_dict_item(s) for s in TreePest.objects.all().order_by("label")
        ],
        "epiphyte": [
            serialize_dict_item(s) for s in TreeEpiphyte.objects.all().order_by("label")
        ],
        "recommendation": [
            serialize_dict_item(s)
            for s in RecommendationKind.objects.all().order_by("label")
        ],
        "work": [
            serialize_dict_item(s) for s in WorkKind.objects.all().order_by("label")
        ],
    }


def serialize_dendrometry(tree: Tree):
    dendrometry = tree.current_dendrometry_history
    if dendrometry:
        return {
            "height": with_validity(
                float(dendrometry.height), dendrometry.validity.lower
            ),
            "circumference150": with_validity(
                float(dendrometry.circumference150), dendrometry.validity.lower
            ),
            "crown_diameter_lower": with_validity(
                float(dendrometry.crown_diameter_range.lower),
                dendrometry.validity.lower,
            ),
            "crown_diameter_upper": with_validity(
                float(dendrometry.crown_diameter_range.upper),
                dendrometry.validity.lower,
            ),
            "sprigs": with_validity(
                dendrometry.branches_count, dendrometry.validity.lower
            ),
        }
    else:
        return {
            "height": None,
            "circumference150": None,
            "crown_diameter_lower": None,
            "crown_diameter_upper": None,
            "branches": None,
            "validity": None,
            "sprigs": None,
        }


def serialize_symptoms(tree: Tree):
    if (
        tree.current_symptom_branch
        or tree.current_symptom_collar
        or tree.current_symptom_leave
        or tree.current_symptom_root
        or tree.current_symptom_trunk
    ):
        return {
            "branch": serialize_nullable_history_multi_obj(
                tree.current_symptom_branch_history, "symptoms"
            ),
            "collar": serialize_nullable_history_multi_obj(
                tree.current_symptom_collar_history, "symptoms"
            ),
            "leave": serialize_nullable_history_multi_obj(
                tree.current_symptom_leave_history, "symptoms"
            ),
            "root": serialize_nullable_history_multi_obj(
                tree.current_symptom_root_history, "symptoms"
            ),
            "trunk": serialize_nullable_history_multi_obj(
                tree.current_symptom_trunk_history, "symptoms"
            ),
        }
    else:
        return {
            "branch": None,
            "collar": None,
            "leave": None,
            "root": None,
            "trunk": None,
        }


def serialize_comments(tree: Tree):
    results = []
    try:
        comments = Comment.objects.filter(tree=tree.id).order_by("-date")
        if comments:
            for c in comments:
                results.append(
                    {
                        "id": c.id,
                        "content": c.content,
                        "date": serialize_datetime(c.date),
                        "user": c.author.id,
                    }
                )
        return results
    except Exception:
        return results


def serialize_recommendation(tree: Tree):
    try:
        recommendations = Recommendation.objects.filter(tree=tree.id)
        if recommendations:
            results = []
            for r in recommendations:
                results.append(
                    {
                        "value": r.recommendation.id,
                        "date": serialize_date(r.date),
                        "do_not_follow": r.do_not_follow,
                        "application_range_lower": (
                            serialize_date(r.application_range.lower)
                            if r.application_range
                            else None
                        ),
                        "application_range_upper": (
                            serialize_date(r.application_range.upper)
                            if r.application_range
                            else None
                        ),
                    }
                )
            return results

    except Exception:
        return None


def serialize_work(tree: Tree):
    try:
        work_trees = WorkTree.objects.filter(tree=tree.id)
        if work_trees:
            return {
                "value": [wt.work_kind.id for wt in work_trees],
                "date": [wt.due_date for wt in work_trees],
                "comment": [
                    wt.comment if wt.comment != "None" else None for wt in work_trees
                ],
            }
    except Exception:
        return None


def serialize_permit(tree: Tree):
    try:
        work_trees = WorkTree.objects.filter(tree=tree.id)
        if work_trees:
            return [wt.permit.reference for wt in work_trees]
    except Exception:
        return None


def serialize_risk(tree: Tree):
    risk = tree.current_risk_analysis_history
    if risk:
        return {
            "break": with_validity(risk.break_risk, risk.validity.lower),
            "size": with_validity(risk.size_risk, risk.validity.lower),
            "target": with_validity(risk.target_risk, risk.validity.lower),
            "hazard": with_validity(risk.hazard(), risk.validity.lower),
        }
    else:
        return {
            "break": None,
            "size": None,
            "target": None,
            "hazard": None,
            "validity": None,
        }


def serialize_owner(owner: TreeOwner) -> dict:
    return {"id": owner.id, "name": owner.name, "code": owner.code}


def serialize_tree(tree: Tree):
    serialized_tree = {
        ##
        ## without history
        "id": tree.id,
        "pin": tree.pin,
        "park": tree.park.trigramme if tree.park is not None else None,
        "kind": tree.kind.id,
        "geometry": geojson(tree.point) if not tree.point.empty else None,
        "owner": serialize_owner(tree.owner) if tree.owner is not None else None,
        "remarkable": tree.remarkable_tree_id,
        "cut": tree.cut,
        ##
        ## with history
        "status": serialize_nullable_history_obj(tree.current_status_history),
        "ontogenesis": serialize_history(tree.current_ontogenesis_history),
        "structure": serialize_nullable_history_obj(tree.current_structure_history),
        "environment": serialize_nullable_history_multi_obj(
            tree.current_environment_history, "environments"
        ),
        "vitality": serialize_history(tree.current_vitality_history),
        "pathogen": serialize_nullable_history_multi_obj(
            tree.current_pathogen_history, "pathogens"
        ),
        "pest": serialize_nullable_history_multi_obj(
            tree.current_pest_history, "pests"
        ),
        "epiphyte": serialize_nullable_history_multi_obj(
            tree.current_epiphyte_history, "epiphytes"
        ),
        "comments": serialize_comments(tree),
        "recommendation": serialize_recommendation(tree),
        "work": serialize_work(tree),
        "permit": serialize_permit(tree),
        "images": [serialize_tree_image(i) for i in tree.images.all()],
    }

    serialized_tree.update(serialize_dendrometry(tree))
    serialized_tree.update(serialize_symptoms(tree))
    serialized_tree.update(serialize_risk(tree))

    return serialized_tree


SYMPTOM_TREE_PARTS_MODELS = {
    "branch": TreeSymptomBranch,
    "collar": TreeSymptomCollar,
    "leave": TreeSymptomLeave,
    "trunk": TreeSymptomTrunk,
    "root": TreeSymptomRoot,
}


def deserialize_tree(data: Dict) -> Optional[Tree]:
    if data["id"]:
        tree = Tree.objects.get(pk=data["id"])
    else:
        tree = Tree()

    if tree:
        if "pin" in data:
            tree.pin = data.get("pin")
        if "remarkable" in data:
            tree.remarkable_tree_id = data.get("remarkable")
        if "cut" in data:
            tree.cut = data.get("cut")
        if "kind" in data:
            if data.get("kind"):
                tk = get_object_or_404(TreeKind, pk=data.get("kind"))
                if tk:
                    tree.kind = tk
            else:
                tree.kind = get_tree_kind_hierarchy("Indeterminatum")
        if "geometry" in data:
            tree.point = Point(data["geometry"]["coordinates"])
            tree.park = get_tree_park(tree.point)
        if "status" in data:
            try:
                s = get_object_or_404(TreeStatus, pk=data.get("status").get("value"))
                tree.set_status(s)
            except Exception:
                pass
        if "structure" in data:
            try:
                s = get_object_or_404(
                    TreeStructure, pk=data.get("structure").get("value")
                )
                tree.set_structure(s)
            except Exception:
                pass
        if "ontogenesis" in data:
            if data.get("ontogenesis"):
                o = data.get("ontogenesis").get("value")
                if o or o == 0:
                    if int(o) in [0, 1, 2, 3, 4]:
                        tree.set_ontogenesis(int(o))
        if "environment" in data:
            try:
                l = list()
                for environment in data.get("environment").get("value"):
                    e = get_object_or_404(TreeEnvironment, pk=environment)
                    l.append(e)
                tree.set_environment(l)
            except Exception:
                pass
        if "vitality" in data:
            if data.get("vitality"):
                v = data.get("vitality").get("value")
                if v:
                    if int(v) in range(0, 11):
                        tree.set_vitality(int(v))
        if (
            "height" in data
            or "circumference150" in data
            or "sprigs" in data
            or "crown_diameter_lower" in data
        ):  # TODO single assignment
            try:
                dendrometry = Dendrometry(
                    height=data.get("height").get("value"),
                    circumference150=data.get("circumference150").get("value"),
                    branches_count=data.get("sprigs").get("value"),
                    crown_diameter_range=[
                        data.get("crown_diameter_lower").get("value"),
                        data.get("crown_diameter_upper").get("value"),
                    ],
                )
                tree.set_dendrometry(dendrometry)
            except Exception:
                pass

        if "branch" in data:
            try:
                l = list()
                for symptoms in data.get("branch").get("value"):
                    ts = get_object_or_404(TreeSymptomBranch, pk=symptoms)
                    l.append(ts)
                tree.set_symptom_branch(l)
            except Exception:
                pass
        if "collar" in data:
            try:
                l = list()
                for symptoms in data.get("collar").get("value"):
                    ts = get_object_or_404(TreeSymptomCollar, pk=symptoms)
                    l.append(ts)
                tree.set_symptom_collar(l)
            except Exception:
                pass
        if "leave" in data:
            try:
                l = list()
                for symptoms in data.get("leave").get("value"):
                    ts = get_object_or_404(TreeSymptomLeave, pk=symptoms)
                    l.append(ts)
                tree.set_symptom_leave(l)
            except Exception:
                pass
        if "root" in data:
            try:
                l = list()
                for symptoms in data.get("root").get("value"):
                    ts = get_object_or_404(TreeSymptomRoot, pk=symptoms)
                    l.append(ts)
                tree.set_symptom_root(l)
            except Exception:
                pass
        if "trunk" in data:
            try:
                l = list()
                for symptoms in data.get("trunk").get("value"):
                    ts = get_object_or_404(TreeSymptomTrunk, pk=symptoms)
                    l.append(ts)
                tree.set_symptom_trunk(l)
            except Exception:
                pass

        if "pathogen" in data:
            try:
                l = list()
                for pathogen in data.get("pathogen").get("value"):
                    e = get_object_or_404(TreePathogen, pk=pathogen)
                    l.append(e)
                tree.set_pathogen(l)
            except Exception:
                pass

        if "pest" in data:
            try:
                l = list()
                for pest in data.get("pest").get("value"):
                    e = get_object_or_404(TreePest, pk=pest)
                    l.append(e)
                tree.set_pest(l)
            except Exception:
                pass

        if "epiphyte" in data:
            try:
                l = list()
                for epiphyte in data.get("epiphyte").get("value"):
                    e = get_object_or_404(TreeEpiphyte, pk=epiphyte)
                    l.append(e)
                tree.set_epiphyte(l)
            except Exception:
                pass

        if (
            "size" in data or "break" in data or "target" in data
        ):  # TODO single assignment
            try:
                risk_analysis = RiskAnalysis(
                    break_risk=data.get("break").get("value"),
                    size_risk=data.get("size").get("value"),
                    target_risk=data.get("target").get("value"),
                )
                tree.set_risk_analysis(risk_analysis)
            except Exception:
                pass

        if "recommendation" in data:
            if data.get("recommendation"):
                if len(data.get("recommendation")) == 0:
                    for r in Recommendation.objects.filter(tree_id=tree.id).all():
                        r.delete()  # TODO better managing of history
            try:
                for recommendation in data.get("recommendation"):
                    rk = get_object_or_404(
                        RecommendationKind, pk=recommendation.get("value")
                    )
                    r = Recommendation(
                        tree=tree,
                        date=(
                            deserialize_datetime(recommendation.get("date"))
                            if recommendation.get("date")
                            else datetime.now()
                        ),
                        recommendation=rk,
                        do_not_follow=recommendation.get("do_not_follow"),
                        # TODO parse other values
                    )
                    # TODO delete recommendation

                    r.save()

            except Exception:
                pass

        return tree
