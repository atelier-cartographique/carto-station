from ..models import Park
from .functions import serialize_lingua, geojson
from django.core.serializers import serialize


def serialize_park(park: Park):
    return {
        "id": park.id,
        "trigramme": park.trigramme,
        "name": serialize_lingua(park.name),
        "geometry": geojson(park.multipolygon),
        "center": geojson(park.multipolygon.centroid),
        "color": park.color.color if park.color else None,
    }
