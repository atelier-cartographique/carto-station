from json import loads

from django.http import FileResponse, JsonResponse, HttpResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.http import require_http_methods

from sgpa.models.image import Image
from sgpa.serializers.image import serialize_tree_image

from .models import Park, Tree, Selection, UserPreferences, Comment, CachedTree
from .serializers import (
    serialize_park,
    serialize_tree,
    deserialize_tree,
    serialize_tree_recommendation,
    serialize_tree_work,
    deserialize_selection,
    serialize_selection,
    serialize_reference,
    serialize_user_preferences,
)


READ_METHODS = ["OPTIONS", "HEAD", "GET"]
WRITE_METHODS = ["POST", "PUT", "PATCH", "DELETE"]


class HTTPResponseNoContent(HttpResponse):
    status_code = 204


def not_found_resource():
    return JsonResponse(
        {"detail": "The resource was not found", "status_code": 404}, status=404
    )


def get_version(request):
    return JsonResponse(
        {
            "major": 1,
            "minor": 0,
            "patch": 0,
        }
    )


@require_http_methods(READ_METHODS)
def get_all_parks(request):
    parks = Park.objects.all()
    data = [serialize_park(p) for p in parks]
    return JsonResponse(data, safe=False)


def trees_by_park(park: Park):
    return Tree.objects.filter(point__intersects=park.multipolygon)


def trees_by_park_trigramme(park: Park):
    return CachedTree.objects.filter(data__park=park.trigramme)


def comments_by_tree(tree: Tree):
    return Comment.objects.filter(tree=tree)


@require_http_methods(READ_METHODS)
def get_trees_park(request, park_id):
    try:
        park = get_object_or_404(Park, trigramme=park_id)
        trees = trees_by_park_trigramme(park)
        data = [t.data for t in trees]
        return JsonResponse(data, safe=False)
    except Exception:
        return not_found_resource()


@require_http_methods(READ_METHODS)
def get_all_trees(request):
    trees = CachedTree.objects.all()
    data = [t.data for t in trees]
    return JsonResponse(data, safe=False)


@require_http_methods(READ_METHODS)
def get_all_trees_id(request):
    trees = CachedTree.objects.exclude(data__park=None).exclude(data__pin=None)
    data = [{"id": t.id, "pin": t.data["pin"], "park": t.data["park"]} for t in trees]
    return JsonResponse(data, safe=False)


@require_http_methods(READ_METHODS)
def view_tree(request, tree_id: int):
    try:
        tree = get_object_or_404(CachedTree, pk=tree_id)
        return JsonResponse(tree.data, safe=False)
    except Exception:
        return not_found_resource()


def update_cached_tree(tree):
    cached_tree = CachedTree.objects.get(id=tree.id)
    cached_tree.data = serialize_tree(tree)
    cached_tree.save()


@require_http_methods(WRITE_METHODS)
def edit_tree(request, tree_id: int):
    data = loads(request.body.decode("utf-8"))
    tree = deserialize_tree(data)
    tree.save()
    if "comments" in data:
        update_comments(request, tree, data.get("comments"))

    update_cached_tree(tree)

    return JsonResponse(serialize_tree(tree), safe=False)


@require_http_methods(WRITE_METHODS)
def create_tree(request):
    data = loads(request.body.decode("utf-8"))
    data["id"] = None
    tree = deserialize_tree(data)
    #try:
    tree.save()
    if "comments" in data:
        update_comments(request, tree, data.get("comments"))

    CachedTree.objects.create(id=tree.id, data=serialize_tree(tree))

    return JsonResponse(serialize_tree(tree), safe=False)
    # except:
    #     return JsonResponse(
    #         {"detail": "The tree cannot be saved", "status_code": 500}, status=500
    #     )


@require_http_methods(WRITE_METHODS)
def update_comments(request, tree, comments):
    try:
        for stored_tree_comment in comments_by_tree(tree).all():
            if stored_tree_comment.id not in [c["id"] for c in comments]:
                stored_tree_comment.delete()
        for c in comments:
            if c.get("id") < 0:
                comment = Comment(
                    tree=tree,
                    author=request.user,
                    content=c.get("content"),
                )
                comment.save()
            else:
                comment = Comment.objects.get(pk=int(c["id"]))
                comment.content = c.get("content")
                comment.author = request.user
                comment.save()

    except Exception:
        pass


@require_http_methods(READ_METHODS)
def view_tree_recommendation(request, tree_id):
    try:
        tree = get_object_or_404(Tree, pk=tree_id)
        return JsonResponse(serialize_tree_recommendation(tree), safe=False)
    except Exception:
        return not_found_resource()


@require_http_methods(READ_METHODS)
def view_tree_work(request, tree_id):
    try:
        tree = get_object_or_404(Tree, pk=tree_id)
        return JsonResponse(serialize_tree_work(tree), safe=False)
    except Exception:
        return not_found_resource()


@require_http_methods(READ_METHODS)
def view_selection(request, selection_id):
    try:
        selection = get_object_or_404(Selection, pk=selection_id)
        return JsonResponse(serialize_selection(selection), safe=False)
    except Exception:
        return not_found_resource()


@require_http_methods(READ_METHODS)
def view_user_selections(request, user_id):
    try:
        selections = list()
        qs = Selection.objects.filter(user=user_id)
        if qs.exists():
            for s in qs.all():
                selections.append(s)
        return JsonResponse([serialize_selection(s) for s in selections], safe=False)
    except Exception:
        return not_found_resource()


@require_http_methods(READ_METHODS)
def get_trees_by_selection(s: Selection):
    return [t for t in s.trees.all()]


@require_http_methods(READ_METHODS)
def get_trees_selection(request, selection_id):
    try:
        selection = get_object_or_404(Selection, id=selection_id)
        trees = get_trees_by_selection(selection)
        data = [serialize_tree(t) for t in trees]
        return JsonResponse(data, safe=False)
    except Exception:
        return not_found_resource()


@require_http_methods(WRITE_METHODS)
def create_selection(request):
    data = loads(request.body.decode("utf-8"))
    instance = deserialize_selection(data)
    instance.user = request.user
    instance.save()

    parks = list()
    for p in data.get("parks"):
        parks.append(Park.objects.get(trigramme=p))
    instance.parks.set(parks)

    trees = list()
    for t in data.get("trees"):
        trees.append(Tree.objects.get(pk=t))
    instance.trees.set(trees)

    instance.refresh_from_db()
    return JsonResponse(serialize_selection(instance), status=201)


@require_http_methods(WRITE_METHODS)
def edit_selection(request, selection_id: int):
    data = loads(request.body.decode("utf-8"))

    instance = Selection.objects.get(pk=data["id"])

    if "name" in data:
        instance.name = data.get("name")

    if "parks" in data:
        parks = list()
        for p in data.get("parks"):
            parks.append(Park.objects.get(trigramme=p))
        instance.parks.set(parks)

    if "trees" in data:
        trees = list()
        for t in data.get("trees"):
            trees.append(Tree.objects.get(pk=t))
        instance.trees.set(trees)

    instance.save()
    return JsonResponse(serialize_selection(instance), safe=False)


@require_http_methods(READ_METHODS)
def get_references(request):
    data = serialize_reference()
    return JsonResponse(data, safe=False)


@require_http_methods(READ_METHODS)
def get_user_preferences(request, user_id):
    try:
        userPreferences = UserPreferences.objects.get(user=user_id)
        return JsonResponse(serialize_user_preferences(userPreferences), safe=False)
    except:
        return JsonResponse([], safe=False)


@require_http_methods(WRITE_METHODS)
def post_tree_image(request, tree_id):
    from django import forms

    tree = get_object_or_404(Tree, id=tree_id)
    file = request.FILES["file"]
    form = forms.ImageField()
    form.run_validators(file)
    user = request.user
    image = Image.objects.create(image=file, tree=tree, user=user)
    update_cached_tree(tree)

    return JsonResponse(serialize_tree_image(image))


@require_http_methods(READ_METHODS)
def get_tree_image(request, id):
    image = Image.objects.get(id=id)
    size = request.GET.get("size")

    if size is not None:
        file = image.thumbnail(size)
        return FileResponse(streaming_content=file)

    return FileResponse(streaming_content=image.image)
