from django.urls import path

from geothermie import views

urlpatterns = [
    path(
        "geothermie/tile/<int:base>/<int:minx>/<int:miny>/<int:maxx>/<int:maxy>/",
        views.tile,
        name="geothermie-tile",
    ),
    path(
        "geothermie/point/<int:x>/<int:y>/",
        views.point,
        name="geothermie-point",
    ),
    path(
        "geothermie/line/<int:base>/<int:x0>/<int:y0>/<int:x1>/<int:y1>/",
        views.line,
        name="geothermie-line",
    ),
    path(
        "geothermie/multi-points-line/<int:base>/",
        views.multi_points_line,
        name="geothermie-line",
    ),
    path(
        "geothermie/constraint/<int:x>/<int:y>/",
        views.constraint,
        name="geothermie-constraint",
    ),
    path(
        "geothermie/maps/",
        views.map_selection,
        name="geothermie-maps",
    ),
    path(
        "geothermie/capakey/<int:x>/<int:y>",
        views.get_from_position,
        name="geothermie-capakey",
    ),
    path(
        "geothermie/capakey/<capakey>",
        views.get_from_capakey,
        name="geothermie-capakey",
    ),
    path(
        "geothermie/info/<key>",
        views.get_info,
        name="geothermie-info",
    ),
    path(
        "geothermie/bulles/",
        views.get_info_bulles,
        name="geothermie-info-bulles",
    ),
]
