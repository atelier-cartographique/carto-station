from django.apps import AppConfig


class GeothermieConfig(AppConfig):
    name = 'geothermie'
    label = 'geothermie'
    verbose_name = 'Géothermie'
    geodata = True
