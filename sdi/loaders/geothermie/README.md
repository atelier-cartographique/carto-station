## Geothermie-loader

# Constraints file

Original constraints layers are shapefiles that we can import in a `geothermie_gis` database using `shp2pgsql`.

```bash
shp2pgsql -d -t 2D -s 31370 -I natura_2000_buffer.shp natura_2000_buffer | psql -h localhost -p 5434 -U postgres -d geothermie_gis
shp2pgsql -d -t 2D -s 31370 -I inventaire_etat_sol_public.shp inventaire_etat_sol_public | psql -h localhost -p 5434 -U postgres -d geothermie_gis
shp2pgsql -d -t 2D -s 31370 -I water_protectedsite_drinking_water.shp water_protectedsite_drinking_water | psql -h localhost -p 5434 -U postgres -d geothermie_gis
shp2pgsql -d -t 2D -s 31370 -I region_buffer_constraint.shp region_buffer_constraint | psql -h localhost -p 5434 -U postgres -d geothermie_gis
```

For convenience, there is a raw sql file `add_constraints_layers.sql` that creates the `geothermie_gis` database, enable PostGIS and load these layers.

```bash
psql -h localhost -p 5434 -U postgres -a -f "add_constraints_layers.sql"
```
# Raster data

The geothermie-loader loads a lot of raster file for use in the geothermie application. these raster files are provided by Bruxelles-Environnement.

These raster files must be stored in a repository whose path is set in the `GEOTHERMIE_SOURCE_DIR` django settings.

These raster files must be sorted according to the following arborescence (see `SOURCES` in `geology.py`):

```
├── phreatic_head
│   └── phreatic_head_normtopolidar_rbc.tif
├── piezo
│   ├── uhrbc1b_quaternaire_limons_sables_graviers_alluviaux_bpsm.tif
│   ├── uhrbc2_systeme_perche_bpsm.tif
│   ├── uhrbc4_bruxellien_bpsm.tif
│   ├── uhrbc6_tielt_bpsm.tif
│   ├── uhrbc7b_moen_bpsm.tif
│   └── uhrbc8a_landenien_hydroland.tif
├── quat_thickness
│   ├── usrbc11_12_remblais_limons_epa.tif
│   ├── usrbc11_13_remblais_limons_argiles_epa.tif
│   └── usrbc14_graviers_alluviaux_epa.tif
├── saturation_rate
│   ├── usrbc11_12_remblais_limons_sat_rate.tif
│   ├── usrbc11_13_remblais_limons_argiles_sat_rate.tif
│   ├── usrbc14_graviers_alluviaux_sat_rate.tif
│   ├── usrbc21_diest_sat_rate.tif
│   ├── usrbc22_bolderberg_sat_rate.tif
│   ├── usrbc23_st_huibrechts_hern_sat_rate.tif
│   ├── usrbc25_onderdale_sat_rate.tif
│   ├── usrbc31_ursel_asse_sat_rate.tif
│   ├── usrbc41_wemmel_sat_rate.tif
│   ├── usrbc42_lede_sat_rate.tif
│   ├── usrbc43_bruxelles_sat_rate.tif
│   ├── usrbc44_vlierzele_sat_rate.tif
│   ├── usrbc51_merelbeke_sat_rate.tif
│   ├── usrbc61_tielt_sat_rate.tif
│   ├── usrbc71_aalbeke_sat_rate.tif
│   ├── usrbc72_moen_sat_rate.tif
│   ├── usrbc73_st_maur_sat_rate.tif
│   ├── usrbc81_grandglise_sat_rate.tif
│   ├── usrbc82_lincent_sat_rate.tif
│   └── usrbc91_cretace_sat_rate.tif
└── thickness
    ├── mnt_brustrati3dv1_1_masked_rbc.tif
    ├── uhrbc1_quaternaire_epa.tif
    ├── usrbc21_diest_epa.tif
    ├── usrbc22_bolderberg_epa.tif
    ├── usrbc23_st_huibrechts_hern_epa.tif
    ├── usrbc25_onderdale_epa.tif
    ├── usrbc31_ursel_asse_epa.tif
    ├── usrbc41_wemmel_epa.tif
    ├── usrbc42_lede_epa.tif
    ├── usrbc43_bruxelles_epa.tif
    ├── usrbc44_vlierzele_epa.tif
    ├── usrbc51_merelbeke_epa.tif
    ├── usrbc61_tielt_epa.tif
    ├── usrbc71_aalbeke_epa.tif
    ├── usrbc72_moen_epa.tif
    ├── usrbc73_st_maur_epa.tif
    ├── usrbc81_grandglise_epa.tif
    ├── usrbc82_lincent_epa.tif
    └── usrbc91_cretace_epa.tif
```

# Layers

## Geological and hydrogeologic layers

The mapping between geological (or stratigraphic) and hydrogeological layers is as follows:

|                                                          |                                                                                                                  |
| -------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------- |
| US                                                       | UH                                                                                                               |
| US/RBC 11 à 14 Quaternaire indifférencié                 | UH/RBC_1a Système aquifère quaternaire superficiel - UH/RBC_1b Aquifère des limons, sables et graviers alluviaux |
| US/RBC_21 Sables de Diest                                | UH/RBC_2 Système aquifère sableux perché                                                                         |
| US/RBC_22 Sables de Bolderberg                           | =                                                                                                                |
| US/RBC_23 Sables et argiles de Sint-Huilbrechts-Hern     | =                                                                                                                |
| US/RBC_25 Sables de Maldegem (membre de Onderdale)       | =                                                                                                                |
| US/RBC_31 Argiles de Maldegem (membre de Ursel et Asse   | UH/RBC_3 Aquiclude des argiles de Ursel et Asse                                                                  |
| US/RBC_41 Sables de Maldegem (membre de Wemmel)          | UH/RBC_4 Système aquifère des sables de Wemmel, Lede, Bruxelles et Vlierzele                                     |
| US/RBC_42 Sables de Lede                                 | =                                                                                                                |
| US/RBC_43 Sables de Bruxelles                            | =                                                                                                                |
| US/RBC_44 Sables de Gent (membre de Vlierzele)           | =                                                                                                                |
| US/RBC_51 Argiles de Gent (membre de Merelbeke)          | UH/RBC_5 Aquiclude des argiles de Gent                                                                           |
| US/RBC_61 Sables et argiles de Tielt                     | UH/RBC_6 Aquitard des sables et argiles de Tielt                                                                 |
| US/RBC_71 Argiles de Kortrijk (membre d'Aalbeke)         | UH/RBC_7a Aquiclude des argiles de Aalbeke                                                                       |
| US/RBC_72 Sables et argiles de Kortrijk (membre de Moen) | UH/RBC_7b Aquitard des sables et argiles de Moen                                                                 |
| US/RBC_73 Argiles de Kortrijk (membre de Saint Maur)     | UH/RBC_7c Aquiclude des argiles de Saint-Maur                                                                    |
| US/RBC_81 Sables de Hannut (Membre de Grandglise)        | UH/RBC_8a Aquifère des sables du Landénien                                                                       |
| US/RBC_82 Argiles de Hannut (Membre de Lincent)          | UH/RBC_8b Aquiclude des argiles du Landénien                                                                     |
| US/RBC_91 Craies de Gulpen                               | UH/RBC_9a Aquifère des craies du Crétacé                                                                         |
| US/RBC_92 Socle Paléozoïque                              | UH/RBC_9b Système aquifère du socle Paléozoïque                                                                  |

This mapping is coded in the `getHydroLayersThicknesses` function.

## Quaternaire layers

The first top layer (US/RBC1), namely the Quaternaire layer, is subdivided in 4 geological (or stratigraphic) and 2 hydrogeologic layers, as follows:

|           |           |
| --------- | --------- |
| US/RBC 11 | UH/RBC 11 |
| US/RBC 12 |           |
| US/RBC 13 |           |

|           |          |
| --------- | -------- |
| US/RBC 14 | UH/RBC12 |

But available data are as follows:

-   US_RBC11_12: indicate the thickness of the US/RBC11 + US/RBC12
-   US_RBC11_13: indicate the thickness of the US/RBC11 + US/RBC12 + US/RBC13
-   US_RBC14: indicate the thickness of US/RBC14

The first 2 layers never overlap. So actually US_RBC11_13 indicate the presence of the US/RBC13 layer. It was decided that the first 3 layers won't be divided in the table.

For the geologic layer, the first layer is subdivided using data from the Quaternaire thicknesses (`getQuaternaireThicknesses`) where necessary (not in all tables).

However, for the hydrogeologic layer, the first layer is **always** subdivided in two in the mapping function `getHydroLayersThicknesses`.

# Spatial constraints

Spatial constraints work with specific layers, namely, at this time of writing: natura_2000_buffer, inventaire_etat_sol_public, water_protectedsite_drinking_water and region_buffer_constraint. The frist three layers are all available on https://wfs.environnement.brussels/belb?. The layer "region_buffer_constraint" can be created from the limits of Bruxelles region (downloadable from Urbis) with a buffer of 500m.

To enable the spatial constraints, you can download these layers in shp (using QGIS for instance) and then import them in a postgis db (here in the schema `geothermie`):

```
shp2pgsql -d -t 2D -s 31370 -I natura_2000_buffer.shp geothermie.natura_2000_buffer | psql -U postgres -d geothermie-gis
shp2pgsql -d -t 2D -s 31370 -I inventaire_etat_sol_public.shp geothermie.inventaire_etat_sol_public | psql -U postgres -d geothermie-gis
shp2pgsql -d -t 2D -s 31370 -I water_protectedsite_drinking_water.shp geothermie.water_protectedsite_drinking_water | psql -U postgres -d geothermie-gis
shp2pgsql -d -t 2D -s 31370 -I region_buffer_constraint.shp geothermie.region_buffer_constraint | psql -U postgres -d geothermie-gis
```

Then, you should point the `geothermie-gis` (or whatever database name you used) in your `settings.py`.
And provides the `constraints` module with its own configuration.

```
LAYERS_DB = {
    'ENGINE': 'django.contrib.gis.db.backends.postgis',
    'HOST': '127.0.0.1',
    'PORT': 5432,
    'NAME': 'geothermie-gis',
    'PASSWORD': '',
    'USER': 'postgres',
}

LAYERS_SCHEMAS = [
    'geothermie',
]

GEOTHERMIE_CONTRAINTS_TABLES = {
    "natura": ("geothermie", "natura_2000_buffer"),
    "soil": ("geothermie", "inventaire_etat_sol_public"),
    "water": ("geothermie", "water_protectedsite_drinking_water"),
    "region": ("geothermie", "region_buffer_constraint"),
    "metro_nord": ("geothermie", "zone_metro_nord"),
}

```

# Settings loaded from a database

The application loads settings from a given database (see `DATABASES['groundwater']` in your `settings.py` file).

If the application is well configured,

- the url `/api/geodata/geothermie/info/strati` gives access to data from the db table (or view) `gw_bgt_table_strati` ;
- the url `/api/geodata/geothermie/info/open` gives access to data from the db table (or view) `gw_bgt_table_open` ;
- the url `/api/geodata/geothermie/info/hgeol` gives access to data from the db table (or view) `gw_bgt_table_hgeol` ;
- the url `/api/geodata/geothermie/info/closed` give access to data from the db table (or view) `gw_bgt_table_closed`.

# Lithological patterns

Each geological layers (US) is characterised by a lithological pattern that is displayed on the geological column (aka the "carrot"). These patterns were designed as SVG files. However, for performance issue, they were translated in [data URI](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Data_URIs) and stored as plain text in the geothermie client (in `carto-station/clients/geothermie/src/components/carrot/img.ts`). For information, the SVG files were translated using this website:
https://dopiaza.org/tools/datauri/index.php.

# Installation tip

Do not forget Django migration to create MapSelection table.
