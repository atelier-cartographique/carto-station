# Données Sitex2

Dans carpedata sur STA-SVOLNXAPP02 (IP:, port:5433)

## fichiers de référence

## tables et vues de base

```
carpedata=# \dt sitex.
                     List of relations
 Schema |              Name              | Type  |  Owner
--------+--------------------------------+-------+----------
 sitex  | blocks                         | table | gisadmin
 sitex  | level_info                     | table | gisadmin
 sitex  | urbadm_building2d_31370_source | table | gisadmin
 sitex  | urdadm_address_point2d_31370   | table | gisadmin
(4 rows)

carpedata=# \dv sitex.
                 List of relations
 Schema |          Name           | Type |  Owner
--------+-------------------------+------+----------
 sitex  | urbadm_building2d_31370 | view | gisadmin
(1 row)

carpedata=# \det sitex.
            List of foreign tables
 Schema |          Table          |   Server
--------+-------------------------+------------
 sitex  | sitex_building          | sitex_prod
 sitex  | sitex_buildingfile      | sitex_prod
 sitex  | sitex_buildinggroup     | sitex_prod
 sitex  | sitex_buildinglocal     | sitex_prod
 sitex  | sitex_buildingoccupancy | sitex_prod
 sitex  | sitex_dictionaryterm    | sitex_prod
 sitex  | sitex_file              | sitex_prod
 sitex  | sitex_keyword           | sitex_prod
 sitex  | sitex_occupancy         | sitex_prod
 sitex  | sitex_parcel            | sitex_prod
 sitex  | sitex_parcelfile        | sitex_prod
 sitex  | sitex_parcellocal       | sitex_prod
 sitex  | sitex_parceloccupancy   | sitex_prod
 sitex  | sitex_publicarea        | sitex_prod
 sitex  | sitex_publicareaequip   | sitex_prod
 sitex  | sitex_publicareafile    | sitex_prod
 sitex  | sitex_publicarealocal   | sitex_prod
(17 rows)
```

## vues de raportage

```
carpedata=# \dv sitex.
                      List of relations
 Schema |               Name               | Type |  Owner
--------+----------------------------------+------+----------
 sitex  | building_occupation_summary_agg  | view | gisadmin
 sitex  | building_occupation_summary_base | view | gisadmin
 sitex  | building_occupation_summary_fmt  | view | gisadmin
 sitex  | report_sitex_building_agg        | view | gisadmin
 sitex  | report_sitex_building_fmt        | view | gisadmin
```
