# Données

Les donnés de la sitex sont de plusieurs ordres:

- les donnés de référence;
- les données de soutien;
- les données résultantes de l'enquête;
- les vues basées sur les résultats de l'enquête.

## donnés de référence

Elles sont hébergées sur la DB `carpedata` (-h 10.128.64.15 -p 5433)

### Table "sitex.blocks"

Une donnée issue d'un tarvail de composition des données Urbis, PRDD et modifications manuelles ([script de constitution de cette couche](../prdd_urbis_pg.py)).

| Column        | Type                         | Collation | Nullable | Default |
| ------------- | ---------------------------- | --------- | -------- | ------- |
| id            | integer                      |           | not null |
| geom          | geometry(MultiPolygon,31370) |           |          |
| urbis_source  | integer[]                    |           |          |
| prdd_source   | integer[]                    |           |          |
| manual_source | integer[]                    |           |          |

### Table "sitex.level_info"

Les données de niveaux de Patrick.

| Column     | Type    | Collation | Nullable | Default |
| ---------- | ------- | --------- | -------- | ------- |
| id         | integer |           | not null |
| nblvl      | real    |           |          |
| retrait    | real    |           |          |
| comment    | text    |           |          |
| dif_lvl    | real    |           |          |
| vide       | real    |           |          |
| demoli     | real    |           |          |
| abrijardin | real    |           |          |
| certitude  | text    |           |          |
| typemod    | text    |           |          |

### Table "sitex.urbadm_building2d_31370_source"

Les données bâtiments Urbis brutes.

| Column     | Type                         | Collation | Nullable | Default                                                           |
| ---------- | ---------------------------- | --------- | -------- | ----------------------------------------------------------------- |
| gid        | integer                      |           | not null | nextval('sitex.urbadm_building2d_31370_source_gid_seq'::regclass) |
| id         | double precision             |           |          |
| versionid  | double precision             |           |          |
| category   | character varying(10)        |           |          |
| status     | character varying(20)        |           |          |
| capakey    | character varying(20)        |           |          |
| area       | double precision             |           |          |
| inspire_id | character varying(50)        |           |          |
| begin_life | date                         |           |          |
| end_life   | date                         |           |          |
| geom       | geometry(MultiPolygon,31370) |           |          |

### View "sitex.urbadm_building2d_31370"

Les données bâtiments Urbis associées aux données de niveaux collectées par Patrick.

| Column            | Type                         | Collation | Nullable | Default |
| ----------------- | ---------------------------- | --------- | -------- | ------- |
| gid               | integer                      |           |          |
| id                | double precision             |           |          |
| versionid         | double precision             |           |          |
| category          | character varying(10)        |           |          |
| status            | character varying(20)        |           |          |
| capakey           | character varying(20)        |           |          |
| area              | double precision             |           |          |
| inspire_id        | character varying(50)        |           |          |
| begin_life        | date                         |           |          |
| end_life          | date                         |           |          |
| geom              | geometry(MultiPolygon,31370) |           |          |
| levelinfo_nblv    | real                         |           |          |
| levelinfo_retrait | real                         |           |          |

### Table "sitex.urdadm_address_point2d_31370"

Données d'addresses Urbis.

| Column     | Type                  | Collation | Nullable | Default                                                         |
| ---------- | --------------------- | --------- | -------- | --------------------------------------------------------------- |
| gid        | integer               |           | not null | nextval('sitex.urdadm_address_point2d_31370_gid_seq'::regclass) |
| id         | double precision      |           |          |
| versionid  | double precision      |           |          |
| si_id      | double precision      |           |          |
| bu_id      | double precision      |           |          |
| mu_id      | double precision      |           |          |
| pz_id      | double precision      |           |          |
| pn_id      | double precision      |           |          |
| adrn       | character varying(20) |           |          |
| planchenum | integer               |           |          |
| angle      | double precision      |           |          |
| capakey    | character varying(20) |           |          |
| x          | double precision      |           |          |
| y          | double precision      |           |          |
| pz_nat_cod | character varying(4)  |           |          |
| mu_nat_cod | character varying(15) |           |          |
| mu_name_fr | character varying(60) |           |          |
| mu_name_du | character varying(60) |           |          |
| pn_name_fr | character varying(60) |           |          |
| pn_name_du | character varying(60) |           |          |
| inspire_id | character varying(50) |           |          |
| begin_life | date                  |           |          |
| end_life   | date                  |           |          |
| geom       | geometry(Point,31370) |           |          |

## données de soutien

Ceci concerne les données qui fournissent le matériel d'encodage (e.g.: dictionnaires). Elles existent sur la DB `angled` (-h 10.128.64.20 -p 5432) et sont reprisent sous forme de _Foreign Data Wrapper_ sur `carpedata`.

### Foreign table "sitex.sitex_dictionaryterm"

Les termes.

| Column     | Type                     | Collation | Nullable | Default | FDW options                |
| ---------- | ------------------------ | --------- | -------- | ------- | -------------------------- |
| created_at | timestamp with time zone |           | not null |         | (column_name 'created_at') |
| updated_at | timestamp with time zone |           | not null |         | (column_name 'updated_at') |
| name_fr    | text                     |           | not null |         | (column_name 'name_fr')    |
| name_nl    | text                     |           | not null |         | (column_name 'name_nl')    |
| name_en    | text                     |           |          |         | (column_name 'name_en')    |
| active     | boolean                  |           | not null |         | (column_name 'active')     |
| category   | character varying(12)    |           | not null |         | (column_name 'category')   |
| id         | integer                  |           | not null |         | (column_name 'id')         |
| order      | integer                  |           | not null |         | (column_name 'order')      |

### Foreign table "sitex.sitex_keyword"

Les mots clés associés aux codes d'occupations.

| Column     | Type                     | Collation | Nullable | Default | FDW options                |
| ---------- | ------------------------ | --------- | -------- | ------- | -------------------------- |
| created_at | timestamp with time zone |           | not null |         | (column_name 'created_at') |
| updated_at | timestamp with time zone |           | not null |         | (column_name 'updated_at') |
| name_fr    | text                     |           | not null |         | (column_name 'name_fr')    |
| name_nl    | text                     |           | not null |         | (column_name 'name_nl')    |
| name_en    | text                     |           |          |         | (column_name 'name_en')    |
| active     | boolean                  |           | not null |         | (column_name 'active')     |
| id         | integer                  |           | not null |         | (column_name 'id')         |
| code_id    | character varying(11)    |           | not null |         | (column_name 'code_id')    |
| order      | integer                  |           | not null |         | (column_name 'order')      |

### Foreign table "sitex.sitex_occupancy"

Catalogue des occupations.

| Column     | Type                     | Collation | Nullable | Default | FDW options                |
| ---------- | ------------------------ | --------- | -------- | ------- | -------------------------- |
| created_at | timestamp with time zone |           | not null |         | (column_name 'created_at') |
| updated_at | timestamp with time zone |           | not null |         | (column_name 'updated_at') |
| name_fr    | text                     |           | not null |         | (column_name 'name_fr')    |
| name_nl    | text                     |           | not null |         | (column_name 'name_nl')    |
| name_en    | text                     |           |          |         | (column_name 'name_en')    |
| active     | boolean                  |           | not null |         | (column_name 'active')     |
| code       | character varying(11)    |           | not null |         | (column_name 'code')       |
| order      | integer                  |           | not null |         | (column_name 'order')      |

## données résultantes

Les données encodées lors de l'enquête. Elles existent sur la DB `angled` (-h 10.128.64.20 -p 5432) et sont reprisent sous forme de Foreign Data Wrapper sur `carpedata`.

### Foreign table "sitex.sitex_building"

La table pricipale pour les bâtiments.

| Column                | Type                         | Collation | Nullable | Default | FDW options                           |
| --------------------- | ---------------------------- | --------- | -------- | ------- | ------------------------------------- |
| created_at            | timestamp with time zone     |           | not null |         | (column_name 'created_at')            |
| updated_at            | timestamp with time zone     |           | not null |         | (column_name 'updated_at')            |
| id                    | integer                      |           | not null |         | (column_name 'id')                    |
| idbuild               | uuid                         |           |          |         | (column_name 'idbuild')               |
| startdate             | timestamp with time zone     |           |          |         | (column_name 'startdate')             |
| enddate               | timestamp with time zone     |           |          |         | (column_name 'enddate')               |
| description           | text                         |           |          |         | (column_name 'description')           |
| internaldesc          | text                         |           |          |         | (column_name 'internaldesc')          |
| versiondesc           | text                         |           |          |         | (column_name 'versiondesc')           |
| geom                  | geometry(MultiPolygon,31370) |           | not null |         | (column_name 'geom')                  |
| nblevel               | double precision             |           |          |         | (column_name 'nblevel')               |
| nblevelrec            | double precision             |           |          |         | (column_name 'nblevelrec')            |
| typologycode_id       | integer                      |           |          |         | (column_name 'typologycode_id')       |
| groundarea            | double precision             |           |          |         | (column_name 'groundarea')            |
| nbparkcar             | integer                      |           |          |         | (column_name 'nbparkcar')             |
| nbparkbike            | integer                      |           |          |         | (column_name 'nbparkbike')            |
| u2block               | integer                      |           |          |         | (column_name 'u2block')               |
| u2municipality        | integer                      |           |          |         | (column_name 'u2municipality')        |
| datastate             | character varying(1)         |           | not null |         | (column_name 'datastate')             |
| geostate              | character varying(1)         |           | not null |         | (column_name 'geostate')              |
| recordstate           | character varying(1)         |           | not null |         | (column_name 'recordstate')           |
| creator_id            | integer                      |           | not null |         | (column_name 'creator_id')            |
| u2building            | integer                      |           |          |         | (column_name 'u2building')            |
| statecode_id          | integer                      |           |          |         | (column_name 'statecode_id')          |
| buildgroup_id         | uuid                         |           |          |         | (column_name 'buildgroup_id')         |
| observation_status_id | integer                      |           |          |         | (column_name 'observation_status_id') |

### Foreign table "sitex.sitex_parcel"

La table principale pour les parcelles.

| Column         | Type                         | Collation | Nullable | Default | FDW options                    |
| -------------- | ---------------------------- | --------- | -------- | ------- | ------------------------------ |
| created_at     | timestamp with time zone     |           | not null |         | (column_name 'created_at')     |
| updated_at     | timestamp with time zone     |           | not null |         | (column_name 'updated_at')     |
| id             | integer                      |           | not null |         | (column_name 'id')             |
| idparcel       | uuid                         |           |          |         | (column_name 'idparcel')       |
| startdate      | timestamp with time zone     |           |          |         | (column_name 'startdate')      |
| enddate        | timestamp with time zone     |           |          |         | (column_name 'enddate')        |
| description    | text                         |           |          |         | (column_name 'description')    |
| internaldesc   | text                         |           |          |         | (column_name 'internaldesc')   |
| versiondesc    | text                         |           |          |         | (column_name 'versiondesc')    |
| fenced         | boolean                      |           | not null |         | (column_name 'fenced')         |
| project        | character varying(255)       |           |          |         | (column_name 'project')        |
| geom           | geometry(MultiPolygon,31370) |           | not null |         | (column_name 'geom')           |
| groundarea     | double precision             |           |          |         | (column_name 'groundarea')     |
| situation      | character varying(16)        |           |          |         | (column_name 'situation')      |
| lining         | character varying(16)        |           |          |         | (column_name 'lining')         |
| imperviousarea | double precision             |           |          |         | (column_name 'imperviousarea') |
| u2block        | integer                      |           |          |         | (column_name 'u2block')        |
| u2municipality | integer                      |           |          |         | (column_name 'u2municipality') |
| datastate      | character varying(1)         |           | not null |         | (column_name 'datastate')      |
| geostate       | character varying(1)         |           | not null |         | (column_name 'geostate')       |
| recordstate    | character varying(1)         |           | not null |         | (column_name 'recordstate')    |
| creator_id     | integer                      |           | not null |         | (column_name 'creator_id')     |
| u2parcel       | integer                      |           |          |         | (column_name 'u2parcel')       |

### Foreign table "sitex.sitex_buildingoccupancy"

Les occupations de bâtiments.

| Column            | Type                     | Collation | Nullable | Default | FDW options                       |
| ----------------- | ------------------------ | --------- | -------- | ------- | --------------------------------- |
| created_at        | timestamp with time zone |           | not null |         | (column_name 'created_at')        |
| updated_at        | timestamp with time zone |           | not null |         | (column_name 'updated_at')        |
| id                | integer                  |           | not null |         | (column_name 'id')                |
| level             | integer                  |           | not null |         | (column_name 'level')             |
| vacant            | boolean                  |           | not null |         | (column_name 'vacant')            |
| nbhousing         | integer                  |           |          |         | (column_name 'nbhousing')         |
| description       | text                     |           |          |         | (column_name 'description')       |
| owner             | character varying(16)    |           |          |         | (column_name 'owner')             |
| area              | double precision         |           |          |         | (column_name 'area')              |
| idbuildversion_id | integer                  |           | not null |         | (column_name 'idbuildversion_id') |
| occupcode_id      | character varying(11)    |           |          |         | (column_name 'occupcode_id')      |
| back_level        | boolean                  |           | not null |         | (column_name 'back_level')        |

### Foreign table "sitex.sitex_parceloccupancy"

Les occupations de parcelles.

| Column             | Type                     | Collation | Nullable | Default | FDW options                        |
| ------------------ | ------------------------ | --------- | -------- | ------- | ---------------------------------- |
| created_at         | timestamp with time zone |           | not null |         | (column_name 'created_at')         |
| updated_at         | timestamp with time zone |           | not null |         | (column_name 'updated_at')         |
| id                 | integer                  |           | not null |         | (column_name 'id')                 |
| occupcode_id       | character varying(11)    |           |          |         | (column_name 'occupcode_id')       |
| area               | double precision         |           |          |         | (column_name 'area')               |
| vacant             | boolean                  |           | not null |         | (column_name 'vacant')             |
| owner              | character varying(16)    |           |          |         | (column_name 'owner')              |
| description        | text                     |           |          |         | (column_name 'description')        |
| nbparkcar          | integer                  |           |          |         | (column_name 'nbparkcar')          |
| nbparkpmr          | integer                  |           |          |         | (column_name 'nbparkpmr')          |
| nbelecplug         | integer                  |           |          |         | (column_name 'nbelecplug')         |
| nbparkbike         | integer                  |           |          |         | (column_name 'nbparkbike')         |
| idparcelversion_id | integer                  |           | not null |         | (column_name 'idparcelversion_id') |

### Foreign table "sitex.sitex_buildinggroup"

Les groupes

| Column     | Type                     | Collation | Nullable | Default | FDW options                |
| ---------- | ------------------------ | --------- | -------- | ------- | -------------------------- |
| created_at | timestamp with time zone |           | not null |         | (column_name 'created_at') |
| updated_at | timestamp with time zone |           | not null |         | (column_name 'updated_at') |
| id         | uuid                     |           | not null |         | (column_name 'id')         |
| name       | text                     |           | not null |         | (column_name 'name')       |

## Les vues de reportage (en cours de dev)

### View "sitex.building_occupation_summary_base"

| Column   | Type             | Collation | Nullable | Default |
| -------- | ---------------- | --------- | -------- | ------- |
| building | integer          |           |          |
| level    | text             |           |          |
| code     | text             |           |          |
| area     | double precision |           |          |

### View "sitex.building_occupation_summary_agg"

| Column      | Type             | Collation | Nullable | Default |
| ----------- | ---------------- | --------- | -------- | ------- |
| building    | integer          |           |          |
| occupations | text[]           |           |          |
| total       | double precision |           |          |

### View "sitex.building_occupation_summary_fmt"

| Column      | Type             | Collation | Nullable | Default |
| ----------- | ---------------- | --------- | -------- | ------- |
| building    | integer          |           |          |
| occupations | text             |           |          |
| total       | double precision |           |          |

### View "sitex.report_sitex_building_agg"

| Column      | Type                         | Collation | Nullable | Default |
| ----------- | ---------------------------- | --------- | -------- | ------- |
| id          | integer                      |           |          |
| emprise     | double precision             |           |          |
| niveaux     | double precision             |           |          |
| retraits    | double precision             |           |          |
| occupations | text[]                       |           |          |
| geom        | geometry(MultiPolygon,31370) |           |          |

### View "sitex.report_sitex_building_fmt"

| Column      | Type                         | Collation | Nullable | Default |
| ----------- | ---------------------------- | --------- | -------- | ------- |
| id          | integer                      |           |          |
| emprise     | double precision             |           |          |
| niveaux     | double precision             |           |          |
| retraits    | double precision             |           |          |
| occupations | text                         |           |          |
| geom        | geometry(MultiPolygon,31370) |           |          |
