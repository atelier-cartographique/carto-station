## Généralités


L'application *Sitex2* est intégrée à la plateforme *geodata* de *perspective.brussels*. Cette plateforme se compose d'un serveur écrit en *Python* sur base de *Django*, et d'une collection d'applicatifs web (*single page applications*) écrits en *Typescript*. 

La *Sitex2* se compose donc d'un module *Python* pour l'implémentation du serveur, [sitex-loader](https://gitlab.com/atelier-cartographique/perspective/sitex-loader), et d'une [application cliente](https://gitlab.com/atelier-cartographique/carto-station/-/tree/master/clients/sitex) correspondante. Ces deux modules sont destinés à être déployés de la même manière que le reste de la plateforme *geodata*.


## Données

Les données relatives à la *Sitex2* se composent de trois ensembles distincts. Celles ayant servies à l'initialisation de la platforme, celles constituant les références géo-spatiales de l'enquête et finalement les données produites par l'enquête elle-même.

Un document de reprise de toutes les tables en présence se trouve [sous forme de PDF](./Sitex_v2_DB.pdf) accompagnant ce fichier.

### initialisation

Les données initiales consistent principalement en une liste de termes et codes référençants des occupations. Cette liste a été livrée sous forme de fichier *Excel* pour l'import duquel une commande Django (`prepare_occupations`) a été écrite, qui prend en charge la création des objets `Occupancy` et `Keyword` dans la base de données. Outre cela, des "dictionnaires" (ou topologies) ont fait l'objet d'une procédure manuelle de création dans le site d'administration.

### références

Les couches de référence principales proviennent d'URBIS et sont regroupées par une connection *Django* dédiée --concrètement pour le déploiement présent sous un schéma *PostgreSQL* spécifique (`urbis`).

  - [urb_p_capa_31370](../sitex/management/files/urbis_parcel.sql)                      
  - [urbadm_building2d_31370](../sitex/management/files/urbis_building.sql)      
  - [urdadm_address_point2d_31370](../sitex/management/files/urbis_address.sql) 
  - [blocks](../sitex/management/files/blocks.sql)

Pour la couche des îlots (originellement venant de urbadm_block), un travail spécifique de [traitement de données](../prdd_urbis_pg.py) a été effectué pour produire une couche plus complète et adaptée. Cette dernière est par ailleurs à la source [d'une vue](../sitex/management/files/blocks_view.sql) (dans le schéma `public`) qui intègre la proportion de bâtiments et parcelles traités par îlot.

### produit

Les objets résultants de l'enquête sont enregistrés dans deux tables manipulées à travers *Django*:

 - bâtiments: `public.sitex_building`
 - parcelles: `public.sitex_parcel`

Ces tables sont mises à disposition des opérateurs de l'application *Sitex2* dans des vues sous le schéma `sitex`, filtrées pour n'intégrer que les objets "actifs" (`recordstate = 'O'`).

```
                                              Table "public.sitex_building"
        Column         |             Type             | Collation | Nullable |                  Default                   
-----------------------+------------------------------+-----------+----------+--------------------------------------------
 created_at            | timestamp with time zone     |           | not null | 
 updated_at            | timestamp with time zone     |           | not null | 
 id                    | integer                      |           | not null | nextval('sitex_building_id_seq'::regclass)
 idbuild               | uuid                         |           |          | 
 startdate             | timestamp with time zone     |           |          | 
 enddate               | timestamp with time zone     |           |          | 
 description           | text                         |           |          | 
 internaldesc          | text                         |           |          | 
 versiondesc           | text                         |           |          | 
 geom                  | geometry(MultiPolygon,31370) |           | not null | 
 nblevel               | integer                      |           |          | 
 nblevelrec            | double precision             |           |          | 
 typologycode_id       | integer                      |           |          | 
 groundarea            | double precision             |           |          | 
 nbparkcar             | integer                      |           |          | 
 nbparkbike            | integer                      |           |          | 
 u2block               | integer                      |           |          | 
 u2municipality        | integer                      |           |          | 
 datastate             | character varying(1)         |           | not null | 
 geostate              | character varying(1)         |           | not null | 
 recordstate           | character varying(1)         |           | not null | 
 creator_id            | integer                      |           | not null | 
 u2building            | integer                      |           |          | 
 statecode_id          | integer                      |           |          | 
 buildgroup_id         | uuid                         |           |          | 
 observation_status_id | integer                      |           |          | 

Foreign-key constraints:
    "sitex_building_buildgroup_id_13376f00_fk_sitex_buildinggroup_id" FOREIGN KEY (buildgroup_id) REFERENCES sitex_buildinggroup(id) DEFERRABLE INITIALLY DEFERRED
    "sitex_building_creator_id_65737443_fk_auth_user_id" FOREIGN KEY (creator_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED
    "sitex_building_observation_status_i_a281dc8e_fk_sitex_dic" FOREIGN KEY (observation_status_id) REFERENCES sitex_dictionaryterm(id) DEFERRABLE INITIALLY DEFERRED
    "sitex_building_statecode_id_4d085bb5_fk_sitex_dictionaryterm_id" FOREIGN KEY (statecode_id) REFERENCES sitex_dictionaryterm(id) DEFERRABLE INITIALLY DEFERRED
    "sitex_building_typologycode_id_a25b9aa7_fk_sitex_dic" FOREIGN KEY (typologycode_id) REFERENCES sitex_dictionaryterm(id) DEFERRABLE INITIALLY DEFERRED
Referenced by:
    TABLE "sitex_buildingfile" CONSTRAINT "sitex_buildingfile_idbuildversion_id_70fef41e_fk_sitex_bui" FOREIGN KEY (idbuildversion_id) REFERENCES sitex_building(id) DEFERRABLE INITIALLY DEFERRED
    TABLE "sitex_buildinglocal" CONSTRAINT "sitex_buildinglocal_idbuildversion_id_a4a8eec4_fk_sitex_bui" FOREIGN KEY (idbuildversion_id) REFERENCES sitex_building(id) DEFERRABLE INITIALLY DEFERRED
    TABLE "sitex_buildingoccupancy" CONSTRAINT "sitex_buildingoccupa_idbuildversion_id_b69f671e_fk_sitex_bui" FOREIGN KEY (idbuildversion_id) REFERENCES sitex_building(id) DEFERRABLE INITIALLY DEFERRED

```

```
                                           Table "public.sitex_parcel"
     Column     |             Type             | Collation | Nullable |                 Default                  
----------------+------------------------------+-----------+----------+------------------------------------------
 created_at     | timestamp with time zone     |           | not null | 
 updated_at     | timestamp with time zone     |           | not null | 
 id             | integer                      |           | not null | nextval('sitex_parcel_id_seq'::regclass)
 idparcel       | uuid                         |           |          | 
 startdate      | timestamp with time zone     |           |          | 
 enddate        | timestamp with time zone     |           |          | 
 description    | text                         |           |          | 
 internaldesc   | text                         |           |          | 
 versiondesc    | text                         |           |          | 
 fenced         | boolean                      |           | not null | 
 project        | character varying(255)       |           |          | 
 geom           | geometry(MultiPolygon,31370) |           | not null | 
 groundarea     | double precision             |           |          | 
 situation      | character varying(16)        |           |          | 
 lining         | character varying(16)        |           |          | 
 imperviousarea | double precision             |           |          | 
 u2block        | integer                      |           |          | 
 u2municipality | integer                      |           |          | 
 datastate      | character varying(1)         |           | not null | 
 geostate       | character varying(1)         |           | not null | 
 recordstate    | character varying(1)         |           | not null | 
 creator_id     | integer                      |           | not null | 
 u2parcel       | integer                      |           |          | 

Foreign-key constraints:
    "sitex_parcel_creator_id_2574e429_fk_auth_user_id" FOREIGN KEY (creator_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED
Referenced by:
    TABLE "sitex_parcelfile" CONSTRAINT "sitex_parcelfile_idparcelversion_id_02e630d7_fk_sitex_parcel_id" FOREIGN KEY (idparcelversion_id) REFERENCES sitex_parcel(id) DEFERRABLE INITIALLY DEFERRED
    TABLE "sitex_parcellocal" CONSTRAINT "sitex_parcellocal_idparcelversion_id_e8d9e98b_fk_sitex_par" FOREIGN KEY (idparcelversion_id) REFERENCES sitex_parcel(id) DEFERRABLE INITIALLY DEFERRED
    TABLE "sitex_parceloccupancy" CONSTRAINT "sitex_parceloccupanc_idparcelversion_id_27587ef5_fk_sitex_par" FOREIGN KEY (idparcelversion_id) REFERENCES sitex_parcel(id) DEFERRABLE INITIALLY DEFERRED

```





## Configuration

Quelques paramètres du service sont configurables par le biais de *settings Django*. Ils sont présentés ici avec leurs valeurs par défaut. 


identifiant de la connection pour les couches de références URBIS
```python
SITEX_URBIS_CONNECTION_ALIAS = 'urbis'
```


Identifiants de groupes utiles pour les permissions.
```python
SITEX_MANAGER_GROUP = 'sitex-manager'
SITEX_SURVEY_GROUP = 'sitex-survey'
```
