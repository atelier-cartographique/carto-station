from django.urls import path

from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

from sitex.views.info_layer import info_layer_data, info_layer_list
from sitex.views.block import get_block_list
from sitex.views.building import (
    BuildingDetailUpdateAPIView,
    BuildingGroupInBlockSuggestionView,
    BuildingHistoryListApiView,
    BuildingListCreateApiView,
)
from sitex.views.building_file import BuildingFileDetailView, BuildingFileView
from sitex.views.building_group import BuildingGroupListCreateView
from sitex.views.building_local import BuildingLocalDetailView, BuildingLocalView
from sitex.views.building_occupancy import (
    BuildingOccupancyDetailView,
    BuildingOccupancyView,
)
from sitex.views.dictionary_observation import ObservationAPIView
from sitex.views.dictionary_occupancy import OccupancyAPIView
from sitex.views.dictionary_statecode import StateCodeAPIView
from sitex.views.dictionary_topology import TopologyAPIView
from sitex.views.file import (
    FileDetailAPIView,
    FileDetailByEntityAPIView,
    FileUploadAPIView,
)
from sitex.views.parcel import ParcelDetailUpdateAPIView, ParcelListCreateAPIView
from sitex.views.parcel_file import ParcelFileDetailUpdateView, ParcelFileView
from sitex.views.parcel_local import ParcelLocalAPIView, ParcelLocalDetailAPIView
from sitex.views.parcel_occupancy import ParcelOccupancyDetailView, ParcelOccupancyView
from sitex.views.public_area import (
    PublicAreaDetailUpdateAPIView,
    PublicAreaListCreateAPIView,
)
from sitex.views.public_area_equip import (
    PublicAreaEquipAPIView,
    PublicAreaEquipDetailAPIView,
)
from sitex.views.public_area_file import PublicAreaFileDetailView, PublicAreaFileView
from sitex.views.public_area_local import (
    PublicAreaLocalAPIView,
    PublicAreaLocalDetailAPIView,
)
from sitex.views.urbis_address import UrbisAddressView
from sitex.views.urbis_building import UrbisBuildingView
from sitex.views.urbis_parcel import UrbisParcelView

schema_view = get_schema_view(
    openapi.Info(
        title="SITEX RESTful API",
        default_version="v1",
        description="Private SITEX Restful API",
        contact=openapi.Contact(email="contact@voxteneo.be"),
        license=openapi.License(name="BSD License"),
    ),
    public=False,
    permission_classes=(permissions.AllowAny,),
)


urlpatterns = [
    # Docs
    path(
        "sitex/",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    path(
        "sitex/redoc/",
        schema_view.with_ui("redoc", cache_timeout=0),
        name="schema-redoc-ui",
    ),
    # Building
    path("sitex/building", BuildingListCreateApiView.as_view(), name="sitex.buildings"),
    path(
        "sitex/building/group",
        BuildingGroupListCreateView.as_view(),
        name="sitex.buildings-group",
    ),
    path(
        "sitex/building/group/<int:block_id>/suggestion",
        BuildingGroupInBlockSuggestionView.as_view(),
        name="sitex.buildings-group-suggestion",
    ),
    path(
        "sitex/building/history/<uuid:idbuild>",
        BuildingHistoryListApiView.as_view(),
        name="sitex.building-history",
    ),
    path(
        "sitex/building/<int:id>",
        BuildingDetailUpdateAPIView.as_view(),
        name="sitex.building-details",
    ),
    # Building Local
    path(
        "sitex/building_local", BuildingLocalView.as_view(), name="sitex.building-local"
    ),
    path(
        "sitex/building_local/<int:id>",
        BuildingLocalDetailView.as_view(),
        name="sitex.building-local-detail",
    ),
    # Building Occupancy
    path(
        "sitex/building_occupancy",
        BuildingOccupancyView.as_view(),
        name="sitex.building-occupancy",
    ),
    path(
        "sitex/building_occupancy/<int:id>",
        BuildingOccupancyDetailView.as_view(),
        name="sitex.building-occupancy-detail",
    ),
    # Building File
    path("sitex/building_file", BuildingFileView.as_view(), name="sitex.building-file"),
    path(
        "sitex/building_file/<int:id>",
        BuildingFileDetailView.as_view(),
        name="sitex.building-file-details",
    ),
    # Parcel
    path("sitex/parcel", ParcelListCreateAPIView.as_view(), name="sitex.parcels"),
    path(
        "sitex/parcel/<int:id>",
        ParcelDetailUpdateAPIView.as_view(),
        name="sitex.parcel",
    ),
    # Parcel Local
    path("sitex/parcel_local", ParcelLocalAPIView.as_view(), name="sitex.parcel-local"),
    path(
        "sitex/parcel_local/<int:id>",
        ParcelLocalDetailAPIView.as_view(),
        name="sitex.parcel-local-details",
    ),
    # Parcel Occupancy
    path(
        "sitex/parcel_occupancy",
        ParcelOccupancyView.as_view(),
        name="sitex.parcel-occupancy",
    ),
    path(
        "sitex/parcel_occupancy/<int:id>",
        ParcelOccupancyDetailView.as_view(),
        name="sitex.parcel-occupancy-details",
    ),
    # Parcel File
    path("sitex/parcel_file", ParcelFileView.as_view(), name="sitex.parcel-file"),
    path(
        "sitex/parcel_file/<int:id>",
        ParcelFileDetailUpdateView.as_view(),
        name="sitex.parcel-file-details",
    ),
    # File
    path("sitex/file", FileUploadAPIView.as_view(), name="sitex.upload-file"),
    path("sitex/file/<int:id>", FileDetailAPIView.as_view(), name="sitex.detail-file"),
    path(
        "sitex/file/<str:entity>/<int:entity_id>",
        FileDetailByEntityAPIView.as_view(),
        name="sitex.detail-file-by-entity",
    ),
    # Public area
    path(
        "sitex/public_area", PublicAreaListCreateAPIView.as_view(), name="sitex.parcels"
    ),
    path(
        "sitex/public_area/<int:id>",
        PublicAreaDetailUpdateAPIView.as_view(),
        name="sitex.public-area",
    ),
    # Public area File
    path(
        "sitex/public_area_file",
        PublicAreaFileView.as_view(),
        name="sitex.parcel-area-file",
    ),
    path(
        "sitex/public_area_file/<int:id>",
        PublicAreaFileDetailView.as_view(),
        name="sitex.public-area-file-details",
    ),
    # Public area Local
    path(
        "sitex/public_area_local",
        PublicAreaLocalAPIView.as_view(),
        name="sitex.public-area-local",
    ),
    path(
        "sitex/public_area_local/<int:id>",
        PublicAreaLocalDetailAPIView.as_view(),
        name="sitex.parcel-area-local-details",
    ),
    # Public area equip
    path(
        "sitex/public_area_equip",
        PublicAreaEquipAPIView.as_view(),
        name="sitex.public-area-equip",
    ),
    path(
        "sitex/public_area_equip/<int:id>",
        PublicAreaEquipDetailAPIView.as_view(),
        name="sitex.public-area-equip-details",
    ),
    # Urbis
    # Block
    path("sitex/block", get_block_list, name="sitex.blocks"),
    # path('sitex/block/<int:gid>', BlockDetailAPIView.as_view(), name='sitex.block-details'),
    # Building
    path(
        "sitex/urbis_building",
        UrbisBuildingView.as_view(),
        name="sitex.urbis-buildings",
    ),
    # Urbis address
    path("sitex/urbis_address", UrbisAddressView.as_view(), name="sitex.urbis-address"),
    # Urbis parcel
    path("sitex/urbis_parcel", UrbisParcelView.as_view(), name="sitex.urbis-parcel"),
    # Dictionary
    path("sitex/occupancy", OccupancyAPIView.as_view(), name="sitex.occupancy"),
    path("sitex/topology", TopologyAPIView.as_view(), name="sitex.topology"),
    path("sitex/statecode", StateCodeAPIView.as_view(), name="sitex.statecode"),
    path(
        "sitex/observationcode",
        ObservationAPIView.as_view({"get": "list"}),
        name="sitex.observationcode",
    ),
    path("sitex/info_layer_list/", info_layer_list),
    path("sitex/info_layer_data/<schema>/<table>.geojson", info_layer_data),
]
