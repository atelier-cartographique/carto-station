import os

from django.db.models.signals import post_save
from django.dispatch import receiver

from sitex.models.file import File
from sitex.utils import images
from sitex.utils.tools import generate_file_name


@receiver(post_save, sender=File)
def post_save_file(instance, created, **kwargs):  # pylint: disable=unused-argument
    if created:
        filename = os.path.basename(instance.file.file.name)
        instance.name = filename

        if instance.type in ('image/png', 'image/jpeg'):
            image_name = filename.split('__')[1]
            if not instance.image_small:
                filename_small = f'small-{image_name}'
                name_small = generate_file_name(
                    filename=filename_small,
                    created_at=instance.created_at,
                )
                img = images.ImageSmall(source=instance.file)
                img_generate = img.generate()
                instance.image_small.save(filename_small, img_generate, save=False)
                instance.image_small_name = name_small
            if not instance.image_medium:
                filename_medium = f'medium-{image_name}'
                name_medium = generate_file_name(
                    filename=filename_medium,
                    created_at=instance.created_at,
                )
                img = images.ImageMedium(source=instance.file)
                img_generate = img.generate()
                instance.image_medium.save(filename_medium, img_generate, save=False)
                instance.image_medium_name = name_medium
            if not instance.image_large:
                filename_large = f'large-{image_name}'
                name_large = generate_file_name(
                    filename=filename_large,
                    created_at=instance.created_at,
                )
                img = images.ImageLarge(source=instance.file)
                img_generate = img.generate()
                instance.image_large.save(filename_large, img_generate, save=False)
                instance.image_large_name = name_large

        instance.save()
