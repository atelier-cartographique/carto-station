from datetime import datetime

from django.db.models.signals import pre_save
from django.dispatch import receiver

import pytz

from sitex.models.dictionary import DictionaryTerm, Keyword, Occupancy


@receiver(pre_save, sender=DictionaryTerm)
@receiver(pre_save, sender=Occupancy)
@receiver(pre_save, sender=Keyword)
def create_dictionaries_from_fixture(sender, instance, **kwargs):  # pylint: disable=unused-argument
    if kwargs.get('raw'):
        instance.updated_at = datetime.now(pytz.utc)
        instance.created_at = datetime.now(pytz.utc)
