from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils import timezone

from sitex.models.building import Building
from sitex.models.parcel import Parcel
from sitex.utils.choices import _ARCHIVE, _DELETED, _ONLINE


@receiver(pre_save, sender=Building)
@receiver(pre_save, sender=Parcel)
def update_startdate_enddate_receiver(sender, instance, **kwargs):  # pylint: disable=unused-argument
    if instance.recordstate == _ONLINE:
        instance.startdate = timezone.now()
    elif instance.recordstate in (_ARCHIVE, _DELETED):
        instance.enddate = timezone.now()
