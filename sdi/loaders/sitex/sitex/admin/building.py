from django.conf import settings
from django.contrib import admin
from django.contrib.gis import admin as gis_admin

from sitex.models.building import Building
from sitex.models.building_file import BuildingFile
from sitex.models.building_local import BuildingLocal
from sitex.models.building_occupancy import BuildingOccupancy
from sitex.utils.admin_model_save_mixin import AdminModelSaveMixin


@admin.register(BuildingFile)
class BuildingFileAdmin(AdminModelSaveMixin, admin.ModelAdmin):
    readonly_fields = ['filetype', 'creator', 'date']
    fieldsets = (
        (
            None,
            {
                'fields': (
                    'idbuildversion',
                    'online',
                    'date',
                    'deleteddate',
                    'creator',
                    'filetype',
                    'file',
                )
            },
        ),
    )

    @admin.display(description='File Type')
    def filetype(self, obj):  # pylint: disable=no-self-use
        return obj.file.type


@admin.register(BuildingOccupancy)
class BuildingOcccupancyAdmin(admin.ModelAdmin):
    pass


@admin.register(BuildingLocal)
class BuildingLocalAdmin(admin.ModelAdmin):
    pass


@admin.register(Building)
class BuildingAdmin(AdminModelSaveMixin, gis_admin.OSMGeoAdmin):
    map_width = getattr(settings, 'GEOADMIN_WIDTH', 1000)
    map_height = getattr(settings, 'GEOADMIN_HEIGHT', 800)
    default_zoom = getattr(settings, 'GEOADMIN_ZOOM', 4)
    list_display = ('idbuild', 'enddate', 'short_description', 'datastate', 'geostate', 'recordstate', 'created_at')
    list_filter = ('startdate', 'enddate', 'creator', 'datastate', 'geostate', 'recordstate')
    search_fields = ('description', 'creator__first_name', 'creator__last_name', 'creator__email', 'idbuild')
    readonly_fields = ('creator', 'created_at', 'updated_at')
    fieldsets = (
        (
            None,
            {
                'fields': (
                    'idbuild',
                    'creator',
                    'buildgroup',
                    ('nblevel', 'nblevelrec'),
                    'statecode',
                    'typologycode',
                    'observation_status',
                    'levelinfo_diflvl',
                    'groundarea',
                    ('nbparkcar', 'nbparkbike'),
                    ('u2block', 'u2municipality', 'u2building'),
                    ('datastate', 'geostate', 'recordstate'),
                    'description',
                    'internaldesc',
                    'versiondesc',
                    (
                        'startdate',
                        'enddate',
                    ),
                )
            },
        ),
        (
            'Maps',
            {
                'classes': ('wide', 'extrapretty'),
                'fields': ('geom',),
            },
        ),
    )
