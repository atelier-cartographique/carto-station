from django.contrib import admin
from django.core.exceptions import ValidationError
from django.forms import BaseInlineFormSet

from sitex.models.dictio import Dictio
from sitex.models.dictio_lg import DictioLg
from sitex.models.dictio_term import DictioTermEN, DictioTermFR, DictioTermNL
from sitex.utils.admin_readonly import AdminReadonlyMixin


class InlineDictio(admin.TabularInline):
    model = Dictio
    extra = 0


class DictioTermInlineFormSet(BaseInlineFormSet):
    def clean(self):
        super().clean()
        count = 0
        for form in self.forms:
            try:
                if form.cleaned_data:
                    if not form.cleaned_data['isValue']:
                        count += 1
            except AttributeError:
                # annoyingly, if a subform is invalid Django explicity raises
                # an AttributeError for cleaned_data
                pass
        if count > 1:
            raise ValidationError(
                'Each language must have only one isValue with false value, Because it will use for label term'
            )


class InlineDictioTermEN(admin.TabularInline):
    model = DictioTermEN
    formset = DictioTermInlineFormSet
    exclude = ('lang',)
    ordering = ('isValue', 'term')
    classes = ['collapse']
    extra = 0


class InlineDictioTermFR(admin.TabularInline):
    model = DictioTermFR
    formset = DictioTermInlineFormSet
    exclude = ('lang',)
    ordering = ('isValue', 'term')
    classes = ['collapse']
    extra = 0


class InlineDictioTermNL(admin.TabularInline):
    model = DictioTermNL
    formset = DictioTermInlineFormSet
    exclude = ('lang',)
    ordering = ('isValue', 'term')
    classes = ['collapse']
    extra = 0


@admin.register(DictioLg)
class DictioLgAdmin(AdminReadonlyMixin):
    inlines = [InlineDictio]
    list_display = ('id', 'name', 'dtype')
    search_fields = ('name', 'dtype')
    fieldsets = (
        (
            None,
            {'fields': ('dtype', 'lang', 'name')},
        ),
    )
    ordering = ('name',)


@admin.register(Dictio)
class DictioAdmin(admin.ModelAdmin):
    inlines = [InlineDictioTermEN, InlineDictioTermFR, InlineDictioTermNL]
    list_display = ('id', 'dcode', 'dtype')
    search_fields = ('dcode', 'dtype__name')
    list_filter = ('dtype',)
    fieldsets = (
        (
            None,
            {'fields': ('dtype', 'dcode')},
        ),
    )
    ordering = ('dcode',)
