from django.contrib import admin

from sitex.models.urbis_address_point import UrbisAddressPoint
from sitex.models.urbis_block import UrbisBlock
from sitex.models.urbis_building import UrbisBuilding
from sitex.models.urbis_parcel import UrbisParcel
from sitex.utils.gis_admin_readonly import GidAdminReadonlyMixin


@admin.register(UrbisBuilding)
class UrbisBuildingAdmin(GidAdminReadonlyMixin):
    list_display = ('gid', 'status', 'category', 'inspire_id', 'versionid')
    search_fields = ('gid', 'status', 'category', 'inspire_id', 'versionid')
    list_filter = ('category', 'status', 'versionid')


@admin.register(UrbisBlock)
class UrbisBlockAdmin(GidAdminReadonlyMixin):
    list_display = ('gid', 'name_fre', 'mu_name_fr', 'type', 'versionid')
    search_fields = ('gid', 'name_fre', 'mu_name_fr', 'type', 'versionid')
    list_filter = ('type', 'versionid')


@admin.register(UrbisAddressPoint)
class UrbisAddressPointAdmin(GidAdminReadonlyMixin):
    list_display = ('gid', 'versionid', 'mu_name_fr', 'mu_name_du', 'pn_name_fr', 'pn_name_du', 'inspire_id')
    search_fields = ('gid', 'versionid')
    list_filter = ('versionid',)


@admin.register(UrbisParcel)
class UrbisParcelAdmin(GidAdminReadonlyMixin):
    list_display = ('gid', 'capakey', 'inspire_id', 'begin_life', 'end_life')
    search_fields = ('gid', 'versionid', 'capakey')
    list_filter = ('versionid',)
