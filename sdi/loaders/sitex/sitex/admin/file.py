from django.contrib import admin

from sitex.models.file import File
from sitex.utils.admin_model_save_mixin import AdminModelSaveMixin


@admin.register(File)
class FileAdmin(AdminModelSaveMixin, admin.ModelAdmin):
    readonly_fields = ('type', 'created_at', 'creator')
    list_display = (
        'id',
        'type',
        'creator',
    )
    list_filter = ('type', 'creator')
    search_fields = (
        'id',
        'type',
        'creator',
    )
    fieldsets = (
        (
            None,
            {
                'fields': (
                    'creator',
                    'type',
                    'created_at',
                )
            },
        ),
        (
            'Images',
            {
                'fields': (
                    ('file', 'name', 'url'),
                    ('image_small', 'image_small_name', 'image_small_url'),
                    ('image_medium', 'image_medium_name', 'image_medium_url'),
                    ('image_large', 'image_large_name', 'image_large_url'),
                )
            },
        ),
    )

    def save_model(self, request, obj, form, change):
        if obj.file:
            obj.type = obj.file.file.content_type

        super().save_model(request, obj, form, change)
