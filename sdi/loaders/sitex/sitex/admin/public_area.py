from django.conf import settings
from django.contrib import admin
from django.contrib.gis import admin as gis_admin

from sitex.models.public_area import PublicArea
from sitex.models.public_area_equip import PublicAreaEquip
from sitex.models.public_area_file import PublicAreaFile
from sitex.models.public_area_local import PublicAreaLocal
from sitex.utils.admin_model_save_mixin import AdminModelSaveMixin


@admin.register(PublicArea)
class PublicAreaAdmin(AdminModelSaveMixin, gis_admin.OSMGeoAdmin):
    map_width = getattr(settings, 'GEOADMIN_WIDTH', 1000)
    map_height = getattr(settings, 'GEOADMIN_HEIGHT', 600)
    default_zooms = getattr(settings, 'GEOADMIN_ZOOM', 4)
    list_display = (
        'idarea',
        'creator',
        'areatype',
        'datastate',
        'geostage',
        'created_at',
        'updated_at',
    )
    list_filter = ('areatype', 'datastate', 'geostage', 'creator')
    search_fields = (
        'idarea',
        'creator',
        'areatype',
        'datastate',
        'geostage',
    )
    readonly_fields = ('creator',)


@admin.register(PublicAreaEquip)
class PublicAreaEquipAdmin(admin.ModelAdmin):
    list_display = (
        'idarea_version',
        'equip_code',
    )
    list_filter = ('idarea_version',)
    search_fields = ('idarea_version__idarea', 'equip_code')


@admin.register(PublicAreaLocal)
class PublicAreaLocalAdmin(admin.ModelAdmin):
    list_display = (
        'idarea_version',
        'address',
    )
    list_filter = ('idarea_version',)
    search_fields = ('idarea_version__idarea', 'address')


@admin.register(PublicAreaFile)
class PublicAreaFileAdmin(AdminModelSaveMixin, admin.ModelAdmin):
    readonly_fields = ['get_file_type', 'creator']
    fieldsets = (
        (
            None,
            {
                'fields': (
                    'idarea_version',
                    'fileonline',
                    'filedate',
                    'deleteddate',
                    'creator',
                    'get_file_type',
                    'filelocation',
                )
            },
        ),
    )

    @admin.display(description='File Type')
    def get_file_type(self, obj):  # pylint: disable=no-self-use
        return obj.filelocation.filetype
