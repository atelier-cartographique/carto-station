from django.conf import settings
from django.contrib import admin
from django.contrib.gis import admin as gis_admin

from sitex.models.parcel import Parcel
from sitex.models.parcel_file import ParcelFile
from sitex.models.parcel_local import ParcelLocal
from sitex.models.parcel_occupancy import ParcelOccupancy
from sitex.utils.admin_model_save_mixin import AdminModelSaveMixin


@admin.register(ParcelLocal)
class ParcelLocalAdmin(admin.ModelAdmin):
    pass


@admin.register(ParcelFile)
class ParcelFileAdmin(AdminModelSaveMixin, admin.ModelAdmin):
    readonly_fields = ['filetype', 'creator', 'date']
    fieldsets = (
        (
            None,
            {
                'fields': (
                    'idparcelversion',
                    'online',
                    'date',
                    'deleteddate',
                    'creator',
                    'filetype',
                    'file',
                )
            },
        ),
    )

    @admin.display(description='File Type')
    def filetype(self, obj):  # pylint: disable=no-self-use
        return obj.file.type


@admin.register(ParcelOccupancy)
class ParcelOccupancyAdmin(admin.ModelAdmin):
    pass


@admin.register(Parcel)
class ParcelAdmin(AdminModelSaveMixin, gis_admin.OSMGeoAdmin):
    map_width = getattr(settings, 'GEOADMIN_WIDTH', 1000)
    map_height = getattr(settings, 'GEOADMIN_HEIGHT', 800)
    default_zoom = getattr(settings, 'GEOADMIN_ZOOM', 4)
    list_display = ('idparcel', 'startdate', 'enddate', 'short_description', 'creator', 'created_at', 'updated_at')
    list_filter = ('startdate', 'enddate', 'creator', 'geostate')
    search_fields = ('description', 'creator__first_name', 'creator__last_name', 'creator__email')
    readonly_fields = ('creator', 'created_at', 'updated_at')
    fieldsets = (
        (
            None,
            {
                'fields': (
                    'idparcel',
                    'creator',
                    'fenced',
                    'project',
                    'situation',
                    'lining',
                    ('groundarea', 'imperviousarea'),
                    ('u2block', 'u2municipality', 'u2parcel'),
                    ('datastate', 'geostate', 'recordstate'),
                    'description',
                    'internaldesc',
                    'versiondesc',
                    (
                        'startdate',
                        'enddate',
                    ),
                )
            },
        ),
        (
            'Maps',
            {
                'classes': ('wide', 'extrapretty'),
                'fields': ('geom',),
            },
        ),
    )
