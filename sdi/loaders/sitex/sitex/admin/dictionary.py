from django.contrib import admin
from django.db import models
from django.forms.widgets import TextInput
from sitex.models.dictionary import Keyword, Observation, Occupancy, StateCode, Topology


class KeywordInlineAdmin(admin.TabularInline):
    model = Keyword
    extra = 1
    fields = ["name_fr", "name_nl"]
    # formfield_overrides = {
    #     models.TextField: {"widget": TextInput},
    # }


@admin.register(Occupancy)
class OccupancyAdmin(admin.ModelAdmin):
    list_display = ("code", "name_fr", "name_nl", "created_at", "updated_at")
    list_filter = ("active", "code")
    search_fields = ("code", "name_fr", "name_nl", "name_en")
    fields = ("code", "name_fr", "name_nl", "name_en", "active")
    inlines = [
        KeywordInlineAdmin,
    ]


@admin.register(Topology)
class TopologyAdmin(admin.ModelAdmin):
    list_display = ("name_fr", "name_nl", "created_at", "updated_at", "order")
    list_filter = ("active",)
    search_fields = ("name_fr", "name_nl", "name_en", "id")
    fields = ("order", "name_fr", "name_nl", "name_en", "active")


@admin.register(StateCode)
class StateCodeAdmin(admin.ModelAdmin):
    list_display = ("name_fr", "name_nl", "created_at", "updated_at")
    list_filter = ("active",)
    search_fields = ("name_fr", "name_nl", "name_en", "id")
    fields = ("name_fr", "name_nl", "name_en", "active")


@admin.register(Observation)
class ObservationCodeAdmin(admin.ModelAdmin):
    list_display = ("name_fr", "name_nl", "created_at", "updated_at", "order")
    list_filter = ("active",)
    search_fields = ("name_fr", "name_nl", "name_en", "id")
    fieldsets = (
        (None, {"fields": ("name_fr", "name_nl", "name_en")}),
        ("Setting", {"fields": ("active", "order")}),
    )
