from django.contrib import admin

from sitex.models.building_group import BuildingGroup


@admin.register(BuildingGroup)
class BuildingGroupModelAdmin(admin.ModelAdmin):

    list_display = ('name', 'created_at', 'updated_at')
    search_fields = ('name', 'id')
    readonly_fields = ('id', 'created_at', 'updated_at')
