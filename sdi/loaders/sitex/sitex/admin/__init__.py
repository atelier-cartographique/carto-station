from django.conf import settings

from .building import *  # noqa F401
from .building_group import *  # noqa F401
from .dictionary import *  # noqa F401
from .file import *  # noqa F401
from .parcel import *  # noqa F401
from .public_area import *  # noqa F401

if getattr(settings, 'DEBUG', False):
    from .urbis import *  # noqa F401
