# Generated by Django 3.2.7 on 2021-11-15 03:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sitex', '0006_auto_20211115_0407'),
    ]

    operations = [
        migrations.AlterField(
            model_name='buildingoccupancy',
            name='level',
            field=models.FloatField(blank=True, default=0.0, verbose_name='Level'),
        ),
    ]
