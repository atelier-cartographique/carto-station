# Generated by Django 3.2.7 on 2021-11-10 13:22

from django.db import migrations, models
import django.db.models.deletion
import sitex.utils.validators


class Migration(migrations.Migration):

    dependencies = [
        ('sitex', '0004_auto_20211105_0855'),
    ]

    operations = [
        migrations.CreateModel(
            name='Keyword',
            fields=[
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('name_fr', models.TextField(verbose_name='Name (French)')),
                ('name_nl', models.TextField(verbose_name='Name (Dutch)')),
                ('name_en', models.TextField(blank=True, null=True, verbose_name='Name (English)')),
                ('active', models.BooleanField(default=True, help_text='Active', verbose_name='Active')),
                ('id', models.AutoField(primary_key=True, serialize=False)),
            ],
            options={
                'verbose_name': 'Keyword',
                'verbose_name_plural': 'Keywords',
                'ordering': ['code'],
            },
        ),
        migrations.CreateModel(
            name='Occupancy',
            fields=[
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('name_fr', models.TextField(verbose_name='Name (French)')),
                ('name_nl', models.TextField(verbose_name='Name (Dutch)')),
                ('name_en', models.TextField(blank=True, null=True, verbose_name='Name (English)')),
                ('active', models.BooleanField(default=True, help_text='Active', verbose_name='Active')),
                ('code', models.CharField(max_length=11, primary_key=True, serialize=False, validators=[sitex.utils.validators.validate_dict_code], verbose_name='Code')),
            ],
            options={
                'verbose_name': 'Occupancy',
                'verbose_name_plural': 'Occupancies',
                'ordering': ['code'],
            },
        ),
        migrations.RemoveField(
            model_name='dictionarykeyword',
            name='code',
        ),
        migrations.DeleteModel(
            name='DictionaryTopology',
        ),
        migrations.CreateModel(
            name='StateCode',
            fields=[
            ],
            options={
                'verbose_name': 'State code',
                'verbose_name_plural': 'State codes',
                'proxy': True,
                'indexes': [],
                'constraints': [],
            },
            bases=('sitex.dictionaryterm',),
        ),
        migrations.CreateModel(
            name='Topology',
            fields=[
            ],
            options={
                'verbose_name': 'Topology',
                'verbose_name_plural': 'Topologies',
                'proxy': True,
                'indexes': [],
                'constraints': [],
            },
            bases=('sitex.dictionaryterm',),
        ),
        migrations.AlterModelOptions(
            name='dictionaryterm',
            options={'verbose_name': 'Dictionary term', 'verbose_name_plural': 'Dictionary terms'},
        ),
        migrations.RemoveField(
            model_name='dictionaryterm',
            name='code',
        ),
        migrations.AddField(
            model_name='dictionaryterm',
            name='category',
            field=models.CharField(choices=[('topology', 'Typology'), ('statecode', 'State code'), ('general', 'General')], default='general', max_length=12, verbose_name='Category'),
        ),
        migrations.AddField(
            model_name='dictionaryterm',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='building',
            name='statecode',
            field=models.ForeignKey(blank=True, limit_choices_to={'category': 'statecode'}, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='statecode', to='sitex.dictionaryterm', verbose_name='State code'),
        ),
        migrations.AlterField(
            model_name='building',
            name='typologycode',
            field=models.ForeignKey(blank=True, limit_choices_to={'category': 'topology'}, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='typologycode', to='sitex.dictionaryterm', verbose_name='Typology code'),
        ),
        migrations.AddIndex(
            model_name='dictionaryterm',
            index=models.Index(fields=['category', 'active'], name='sitex_dicti_categor_149d57_idx'),
        ),
        migrations.DeleteModel(
            name='DictionaryKeyword',
        ),
        migrations.AddField(
            model_name='keyword',
            name='code',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='keywords', to='sitex.occupancy', verbose_name='Occupancy'),
        ),
        migrations.AlterField(
            model_name='buildingoccupancy',
            name='occupcode',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.RESTRICT, to='sitex.occupancy', verbose_name='Occupancy Code'),
        ),
        migrations.AddIndex(
            model_name='keyword',
            index=models.Index(fields=['code'], name='sitex_keywo_code_id_9edf3b_idx'),
        ),
    ]
