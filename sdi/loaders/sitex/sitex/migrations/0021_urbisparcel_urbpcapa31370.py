# Generated by Django 3.2.7 on 2021-12-17 03:34

import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sitex', '0020_auto_20211207_0551'),
    ]

    operations = [
        migrations.CreateModel(
            name='UrbPCapa31370',
            fields=[
                ('gid', models.AutoField(primary_key=True, serialize=False)),
                ('id', models.DecimalField(blank=True, decimal_places=65535, max_digits=65535, null=True)),
                ('versionid', models.DecimalField(blank=True, decimal_places=65535, max_digits=65535, null=True)),
                ('capaty', models.CharField(blank=True, max_length=2, null=True)),
                ('capakey', models.CharField(blank=True, max_length=18, null=True)),
                ('shape_area', models.DecimalField(blank=True, decimal_places=65535, max_digits=65535, null=True)),
                ('sheet', models.CharField(blank=True, max_length=18, null=True)),
                ('mu_nat_cod', models.CharField(blank=True, max_length=5, null=True)),
                ('cd5c', models.CharField(blank=True, max_length=5, null=True)),
                ('cdnc', models.CharField(blank=True, max_length=7, null=True)),
                ('csnc', models.CharField(blank=True, max_length=1, null=True)),
                ('shnc', models.CharField(blank=True, max_length=6, null=True)),
                ('shnc_file', models.CharField(blank=True, max_length=25, null=True)),
                ('rad_num', models.CharField(blank=True, max_length=4, null=True)),
                ('exp_alpha', models.CharField(blank=True, max_length=1, null=True)),
                ('exp_num', models.CharField(blank=True, max_length=3, null=True)),
                ('apnc_mapc', models.CharField(blank=True, max_length=50, null=True)),
                ('apnc_cadc', models.CharField(blank=True, max_length=17, null=True)),
                ('apnc_cad', models.CharField(blank=True, max_length=11, null=True)),
                ('apnc_map', models.CharField(blank=True, max_length=11, null=True)),
                ('div_num', models.CharField(blank=True, max_length=2, null=True)),
                ('inspire_id', models.CharField(blank=True, max_length=50, null=True)),
                ('begin_life', models.DateField(blank=True, null=True)),
                ('end_life', models.DateField(blank=True, null=True)),
                ('geom', django.contrib.gis.db.models.fields.MultiPolygonField(blank=True, null=True, srid=31370)),
            ],
            options={
                'db_table': 'urb_p_capa_31370',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='UrbisParcel',
            fields=[
            ],
            options={
                'verbose_name': 'Urbis Parcel',
                'verbose_name_plural': 'Urbis Parcels',
                'proxy': True,
                'indexes': [],
                'constraints': [],
            },
            bases=('sitex.urbpcapa31370',),
        ),
    ]
