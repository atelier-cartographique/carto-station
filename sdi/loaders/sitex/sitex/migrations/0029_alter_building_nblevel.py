# Generated by Django 3.2.7 on 2022-02-21 05:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sitex', '0028_auto_20220215_0901'),
    ]

    operations = [
        migrations.AlterField(
            model_name='building',
            name='nblevel',
            field=models.IntegerField(blank=True, null=True, verbose_name='Nb level'),
        ),
    ]
