# Generated by Django 3.2.7 on 2021-11-05 07:55

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('sitex', '0003_rename_geostage_building_geostate'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='building',
            name='builddate',
        ),
        migrations.AddField(
            model_name='building',
            name='u2building',
            field=models.IntegerField(blank=True, null=True, verbose_name='Urbis building'),
        ),
        migrations.AlterField(
            model_name='building',
            name='creator',
            field=models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to=settings.AUTH_USER_MODEL, verbose_name='Creator'),
        ),
        migrations.AlterField(
            model_name='building',
            name='description',
            field=models.TextField(blank=True, null=True, verbose_name='Description'),
        ),
        migrations.AlterField(
            model_name='building',
            name='enddate',
            field=models.DateTimeField(blank=True, null=True, verbose_name='End date'),
        ),
        migrations.AlterField(
            model_name='building',
            name='internaldesc',
            field=models.TextField(blank=True, null=True, verbose_name='Internal description'),
        ),
        migrations.AlterField(
            model_name='building',
            name='startdate',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Start date'),
        ),
        migrations.AlterField(
            model_name='building',
            name='statecode',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='sitex.dictionaryterm'),
        ),
        migrations.AlterField(
            model_name='building',
            name='u2block',
            field=models.IntegerField(blank=True, null=True, verbose_name='Urbis block'),
        ),
        migrations.AlterField(
            model_name='building',
            name='u2municipality',
            field=models.IntegerField(blank=True, null=True, verbose_name='Urbis municipality'),
        ),
        migrations.AlterField(
            model_name='building',
            name='versiondesc',
            field=models.TextField(blank=True, null=True, verbose_name='Version description'),
        ),
    ]
