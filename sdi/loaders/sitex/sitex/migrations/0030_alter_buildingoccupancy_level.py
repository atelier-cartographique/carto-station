# Generated by Django 3.2.7 on 2022-02-21 09:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sitex', '0029_alter_building_nblevel'),
    ]

    operations = [
        migrations.AlterField(
            model_name='buildingoccupancy',
            name='level',
            field=models.IntegerField(blank=True, default=0.0, verbose_name='Level'),
        ),
    ]
