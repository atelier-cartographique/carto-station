# Generated by Django 3.2.5 on 2023-09-04 14:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sitex', '0033_auto_20221101_1710'),
    ]

    operations = [
        migrations.AlterField(
            model_name='building',
            name='nblevel',
            field=models.FloatField(blank=True, null=True, verbose_name='Nb level'),
        ),
        migrations.AlterModelTable(
            name='urbadmbuilding2d31370',
            table='urbadm_building2d_31370v',
        ),
    ]
