import os
import re

from django.conf import settings
from django.core.exceptions import ValidationError
from django.template.defaultfilters import filesizeformat
from django.utils.translation import gettext_lazy as _


def validate_file_extension(value):
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.pdf', '.doc', '.docx', '.jpg', '.jpeg', '.xlsx', '.xls', '.png']
    if not ext.lower() in valid_extensions:
        raise ValidationError(
            _('Unsupported file extension. allowed extension [.pdf, .jpg, .jpeg .png, .doc, .docx, .xls, .xlsx]')
        )


def validate_file_max_size(value):
    if value.size > int(getattr(settings, 'MAX_UPLOAD_SIZE', '10485760')):
        raise ValidationError(
            _('Please keep filesize under %s. Current filesize %s')
            % (filesizeformat(getattr(settings, 'MAX_UPLOAD_SIZE', '10485760')), filesizeformat(value.size))
        )


def validate_dict_code(value):
    is_valid = re.fullmatch(r'\d{2}\.\d{2}\.\d{2}\.\d{2}', value)
    if not is_valid:
        raise ValidationError(_('Dict code must be in format XX.XX.XX.XX'))
