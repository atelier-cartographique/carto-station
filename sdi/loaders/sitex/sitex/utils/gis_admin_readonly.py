from django.conf import settings
from django.contrib.gis import admin as gis_admin


class GidAdminReadonlyMixin(gis_admin.OSMGeoAdmin):
    modifiable = False
    map_width = getattr(settings, 'GEOADMIN_WIDTH', 1000)
    map_height = getattr(settings, 'GEOADMIN_HEIGHT', 800)
    default_zoom = getattr(settings, 'GEOADMIN_ZOOM', 4)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_readonly_fields(self, request, obj=None):
        if obj:
            self.readonly_fields = [field.name for field in obj.__class__._meta.fields if field.name != 'geom']
        return self.readonly_fields
