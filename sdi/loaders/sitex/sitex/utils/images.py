from imagekit import ImageSpec
from imagekit.processors import ResizeToFit


class ImageSmall(ImageSpec):
    processors = [ResizeToFit(128, 128)]
    options = {'quality': 75}


class ImageMedium(ImageSpec):
    processors = [ResizeToFit(620, 620)]
    options = {'quality': 75}


class ImageLarge(ImageSpec):
    processors = [ResizeToFit(1024, 1024)]
    options = {'quality': 75}
