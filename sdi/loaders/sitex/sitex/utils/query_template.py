GEOJSON_QUERY = """
SELECT row_to_json(fc)
  FROM (
    SELECT
      'FeatureCollection' AS type,
      COALESCE(array_to_json(array_agg(f)), '[]') AS features
    FROM (
        SELECT
          'Feature' AS type,
          ST_AsGeoJSON(sd.{geometry_column})::json AS geometry,
          {pk_column} as id,
          row_to_json((
            SELECT prop FROM (SELECT {field_names} {json_column}) AS prop
           )) AS properties
        FROM "{table}" AS sd
        {where_clause}
    ) AS f
  ) AS fc;
"""

COLUMN_QUERY = """
SELECT COALESCE(array_to_json(array_agg(fc)), '[]')
  FROM (
    SELECT {field_names} {json_column}
    FROM "{table}" as sd
    {join_table}
    {where_clause}
    {orders_by}
  ) AS fc;
"""

JOIN_TABLE_QUERY = """
JOIN "{table}" as {alias} ON ST_Intersects("sd"."geom", "{alias}"."geom")
"""

JOIN_TABLE_COLUMN_QUERY = """
, {field_names}
"""

JSON_COLUMN_QUERY = """
, row_to_json(
    (
      SELECT
        data
      FROM
        (SELECT {field_names}) AS data
    )
  ) AS {index}
"""

RELATION_JSON_COLUMN_QUERY = """
, row_to_json(
    (
      SELECT
        {alias}data
      FROM
        (
          SELECT
            {field_names}
            {json_column}
          FROM
            {table}
          AS {alias}
          WHERE "{alias}"."id" = "{parent_alias}"."{field_relation}"
        ) AS {alias}data
    )
  ) AS {index}
"""

RELATION_ARRAY_JSON_COLUMN_QUERY = """
, COALESCE((
    SELECT
      array_to_json(array_agg(row_to_json({alias}data)))
    FROM (
      SELECT {field_names} {json_column} FROM {table} AS {alias} WHERE "{alias}"."{field_relation}" = "sd"."id"
    ) AS {alias}data
  ), '[]') AS {index}
"""

BOUNDINGBOX_CLAUSE = """
sd.{geometry_column} && ST_MakeEnvelope({bbox}, 31370)
"""

INTERSECTS_CLAUSE = """
ST_Intersects(
  sd.{geometry_column}, CONCAT('SRID=31370;', ST_AsText(ST_GeomFromEWKB({intersects_geom}), 31370))::geometry
)
"""

INTERSECTS_BY_GEOM_CLAUSE = """
ST_Intersects(sd.{geometry_column}, ST_makeValid('{intersects_geom}'))
"""

FILTER_CLAUSE = """
sd.{field_name} {operator} {field_value}
"""

WITH_GEOMETRY_CLAUSE = """
sd.{geometry_column} IS NOT NULL
"""
