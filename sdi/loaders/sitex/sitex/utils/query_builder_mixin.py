from django.contrib.gis.geos import GEOSGeometry

from psycopg2 import Binary

from sitex.utils.query_template import (
    BOUNDINGBOX_CLAUSE,
    COLUMN_QUERY,
    FILTER_CLAUSE,
    GEOJSON_QUERY,
    INTERSECTS_BY_GEOM_CLAUSE,
    INTERSECTS_CLAUSE,
    JOIN_TABLE_COLUMN_QUERY,
    JOIN_TABLE_QUERY,
    JSON_COLUMN_QUERY,
    RELATION_ARRAY_JSON_COLUMN_QUERY,
    RELATION_JSON_COLUMN_QUERY,
)


class QueryBuilderMixin:
    def get_raw_query(self, *args, **kwargs):  # pylint: disable=too-many-locals, too-many-branches
        list_fields = list(self.Meta.fields)
        field_names = []
        json_column, remove_field = self.get_json_query()

        # Handle if has relation field
        relation_column = self.get_relation_query()
        if kwargs.get('details'):
            if kwargs.get('details') in ('N', 'n'):
                relation_column = []

        json_column = json_column + relation_column

        # Handle if foreign key field
        if hasattr(self.Meta, 'fk_field'):
            if len(self.Meta.fk_field) > 0:
                for item in self.Meta.fk_field:
                    remove_field.append(item)
                    list_fields.append(f'{item}_id')

        list_fields = filter(lambda i: i not in remove_field, list_fields)

        for field in list_fields:
            if field != self.Meta.geo_field:
                if field in (
                    'id',
                    'description',
                    'creator_id',
                    'statecode_id',
                    'typologycode_id',
                    'buildgroup_id',
                    'observation_status_id',
                ):
                    alias = field.replace('_id', '') if field.endswith('_id') else field
                    field_names.append(f'"sd"."{field}" AS {alias}')
                else:
                    field_names.append(f'"{field}"')

        # Where clause for restrict select query
        clauses = []
        or_clauses = []
        if kwargs.get('in_bbox'):
            clauses.append(self._get_bbox_query(self.Meta.geo_field, kwargs.get('in_bbox')))
        if kwargs.get('intersects_geom'):
            clauses.append(
                self._get_intersects_query(self.Meta.geo_field, self._converter(kwargs.get('intersects_geom')))
            )
        if kwargs.get('filter'):
            for key, value in kwargs.get('filter').items():
                operator = '='
                if key == 'history':
                    key = 'recordstate'
                    operator = 'IN'
                    value = "('A')" if value in ('Y', 'y') else "('O')"
                elif key in ('idbuild', 'idarea', 'idparcel'):
                    value = f"'{value}'"
                clauses.append(self._get_filter_query(key, operator, value))

        if kwargs.get('blocks_gid'):
            for geom in kwargs.get('blocks_gid'):
                or_clauses.append(self._get_intersects_by_geom_query(self.Meta.geo_field, geom))

        if kwargs.get('buildings_gid'):
            for geom in kwargs.get('buildings_gid'):
                or_clauses.append(self._get_intersects_by_geom_query(self.Meta.geo_field, geom))

        # Handle for join OR clauses with AND in where clauses
        if len(or_clauses) > 0:
            clauses.append(self._get_or_clauses(or_clauses))

        return GEOJSON_QUERY.format(
            geometry_column=self.Meta.geo_field,
            pk_column=self.Meta.id_field,
            field_names=', '.join(field_names),
            where_clause=self._get_where_clauses(clauses),
            json_column=self._get_json_tables(json_column),
            table=self.Meta.model.objects.model._meta.db_table,
        )

    def get_json_raw_query(self, *args, **kwargs):  # pylint: disable=too-many-locals, too-many-branches
        list_fields = list(self.Meta.fields)
        field_names = []
        orders_by = []
        json_column, remove_field = self.get_json_query()

        # Handle if has join field
        join_query, join_column = self.get_join_table_query()

        # Handle if has relation field
        relation_column = self.get_relation_query()
        if kwargs.get('details'):
            if kwargs.get('details') in ('N', 'n'):
                relation_column = []

        json_column = json_column + join_column + relation_column

        # Handle if foreign key field
        if hasattr(self.Meta, 'fk_field'):
            if len(self.Meta.fk_field) > 0:
                for item in self.Meta.fk_field:
                    remove_field.append(item)
                    list_fields.append(f'{item}_id')

        list_fields = filter(lambda i: i not in remove_field, list_fields)

        for field in list_fields:
            if field != self.Meta.geo_field:
                if field in (
                    'id',
                    'description',
                    'creator_id',
                    'statecode_id',
                    'typologycode_id',
                    'file_id',
                    'buildgroup_id',
                    'observation_status_id',
                ):
                    alias = field.replace('_id', '') if field.endswith('_id') else field
                    field_names.append(f'"sd"."{field}" AS {alias}')
                else:
                    field_names.append(f'"sd"."{field}"')

        if self.Meta.model.objects.model._meta.db_table in ('sitex_building', 'sitex_parcel'):
            field_names.append('ST_AsGeoJSON(sd.geom)::json AS geom')
            orders_by.append('sd.enddate DESC')

        # Where clause for restrict select query
        clauses = []
        or_clauses = []
        if kwargs.get('in_bbox'):
            clauses.append(self._get_bbox_query(self.Meta.geo_field, kwargs.get('in_bbox')))
        if kwargs.get('intersects_geom'):
            clauses.append(
                self._get_intersects_query(self.Meta.geo_field, self._converter(kwargs.get('intersects_geom')))
            )
        if kwargs.get('filter'):
            for key, value in kwargs.get('filter').items():
                operator = '='
                if key == 'history':
                    key = 'recordstate'
                    operator = 'IN'
                    value = "('A')" if value in ('Y', 'y') else "('O')"
                elif key in ('idbuild', 'idarea', 'idparcel'):
                    value = f"'{value}'"
                clauses.append(self._get_filter_query(key, operator, value))

        if kwargs.get('blocks_gid'):
            for geom in kwargs.get('blocks_gid'):
                or_clauses.append(self._get_intersects_by_geom_query(self.Meta.geo_field, geom))

        if kwargs.get('buildings_gid'):
            for geom in kwargs.get('buildings_gid'):
                or_clauses.append(self._get_intersects_by_geom_query(self.Meta.geo_field, geom))

        # Handle for join OR clauses with AND in where clauses
        if len(or_clauses) > 0:
            clauses.append(self._get_or_clauses(or_clauses))

        return COLUMN_QUERY.format(
            field_names=', '.join(field_names),
            where_clause=self._get_where_clauses(clauses),
            json_column=self._get_json_tables(json_column),
            join_table=self._get_joins_tables(join_query),
            orders_by=self._get_order_by(orders_by),
            table=self.Meta.model.objects.model._meta.db_table,
        )

    def get_json_query(self):
        if not hasattr(self.Meta, 'json_field') or not isinstance(self.Meta.json_field, dict):
            return [], []
        json_query = []
        remove_fields = []
        for keys, values in self.Meta.json_field.items():
            json_fields = []
            for item in values:
                json_fields.append(f'sd."{item["field"]}" AS {item["alias"]}')
                remove_fields.append(f'sd."{item["field"]}"')
            json_query.append(JSON_COLUMN_QUERY.format(field_names=', '.join(json_fields), index=keys))
        return json_query, remove_fields

    def get_join_table_query(self):
        if not hasattr(self.Meta, 'join_field') or not isinstance(self.Meta.join_field, dict):
            return [], []
        join_fields = []
        join_query = []
        for k, v in self.Meta.join_field.items():
            list_fields = v['fields']
            table_alias = v['alias']
            field_prefix = v['field_prefix']
            field_names = []

            for field in list_fields:
                field_names.append(f'"{table_alias}"."{field}" AS {field_prefix}_{field}')

            join_fields.append(
                JOIN_TABLE_COLUMN_QUERY.format(
                    field_names=', '.join(field_names),
                )
            )
            join_query.append(
                JOIN_TABLE_QUERY.format(
                    table=v['table'],
                    alias=v['alias'],
                )
            )

        return join_query, join_fields

    def get_relation_query(self):
        if not hasattr(self.Meta, 'relation_field') or not isinstance(self.Meta.relation_field, dict):
            return []
        relation_query = []
        for k, v in self.Meta.relation_field.items():
            list_fields = v['fields']
            table_alias = v['alias']
            remove_field = []
            field_names = []
            relation_column = []
            json_column = []

            # Handle if foreign key field
            if v.get('fk_fields'):
                if len(v['fk_fields']) > 0:
                    for item in v['fk_fields']:
                        remove_field.append(item)
                        list_fields.append(f'{item}_id')

            # Handle if relation with detail field
            if v.get('relation_detail_field'):
                if len(v.get('relation_detail_field').keys()) > 0:
                    for key in v.get('relation_detail_field').keys():
                        remove_field.append(key)
                        # Handle if relation details has json column
                        json_column += self._get_json_detail_query(v.get('relation_detail_field')[key])
                        # Handle if relation details has concat column
                        json_column += self._get_concat_detail_query(v.get('relation_detail_field')[key])

                relation_column = self._get_relation_detail_query(v, self._get_json_tables(json_column))

            list_fields = filter(lambda i: i not in remove_field, list_fields)
            for field in list_fields:
                if '_id' in field:
                    alias = field.replace('_id', '') if field.endswith('_id') else field
                    field_names.append(f'"{table_alias}"."{field}" AS {alias}')
                else:
                    field_names.append(f'"{table_alias}"."{field}"')
            relation_query.append(
                RELATION_ARRAY_JSON_COLUMN_QUERY.format(
                    table=v['table'],
                    alias=v['alias'],
                    field_relation=v['relation'],
                    json_column=self._get_json_tables(relation_column),
                    index=k,
                    field_names=' , '.join(field_names),
                )
            )
        return relation_query

    @staticmethod
    def _get_relation_detail_query(model, json_column):
        if 'relation_detail_field' not in model.keys() or type(model) is not dict:
            return []
        relation_query = []
        parent_alias = model['alias']
        for k, v in model['relation_detail_field'].items():
            list_fields = v['fields']
            table_alias = v['alias']
            remove_field = []
            field_names = []

            # Handle if foreign key field
            if v.get('fk_fields'):
                if len(v['fk_fields']) > 0:
                    for item in v['fk_fields']:
                        remove_field.append(item)
                        list_fields.append(f'{item}_id')

            list_fields = filter(lambda i: i not in remove_field, list_fields)
            for field in list_fields:
                if '_id' in field:
                    alias = field.replace('_id', '') if field.endswith('_id') else field
                    field_names.append(f'"{table_alias}"."{field}" AS "{alias}"')
                else:
                    field_names.append(f'"{table_alias}"."{field}"')
            relation_query.append(
                RELATION_JSON_COLUMN_QUERY.format(
                    table=v['table'],
                    alias=v['alias'],
                    field_relation=v['relation'],
                    parent_alias=parent_alias,
                    json_column=json_column,
                    index=k,
                    field_names=' , '.join(field_names),
                )
            )

        return relation_query

    @staticmethod
    def _get_json_detail_query(model):
        if 'json_field' not in model.keys() or type(model) is not dict:
            return []
        json_query = []
        for keys, values in model['json_field'].items():
            json_fields = []
            for item in values:
                json_fields.append(f'"{item["field"]}" AS {item["alias"]}')
            json_query.append(JSON_COLUMN_QUERY.format(field_names=', '.join(json_fields), index=keys))
        return json_query

    @staticmethod
    def _get_concat_detail_query(model):
        if 'concat_field' not in model.keys() or type(model) is not dict:
            return []
        concat_query = []
        for keys, values in model['concat_field'].items():
            concat_fields = []
            if isinstance(values, dict):
                concat_fields.append(f"CONCAT('{values['str_concat']}', {values['field']}) AS {values['alias']}")
                concat_query.append(JOIN_TABLE_COLUMN_QUERY.format(field_names=', '.join(concat_fields)))
            if isinstance(values, (tuple, list)):
                for item in values:
                    concat_fields.append(f"CONCAT('{item['str_concat']}', {item['field']}) AS {item['alias']}")
                concat_query.append(JSON_COLUMN_QUERY.format(field_names=', '.join(concat_fields), index=keys))
        return concat_query

    @staticmethod
    def _get_query_fields(fields):
        return '' if len(fields) == 0 else ' , '.join(map(lambda x: f'{x}', fields))

    @staticmethod
    def _get_json_tables(json_tables):
        return '' if len(json_tables) == 0 else ' '.join(map(lambda x: f'{x}', json_tables))

    @staticmethod
    def _get_joins_tables(join_tables):
        return '' if len(join_tables) == 0 else ' '.join(map(lambda x: f'{x}', join_tables))

    @staticmethod
    def _get_where_clauses(clauses):
        return '' if len(clauses) == 0 else f"WHERE {' AND '.join(map(lambda x: f'({x})', clauses))}"

    @staticmethod
    def _get_or_clauses(clauses):
        return '' if len(clauses) == 0 else f"({' OR '.join(map(lambda x: f'({x})', clauses))})"

    @staticmethod
    def _get_bbox_query(geo_field, bbox):
        return BOUNDINGBOX_CLAUSE.format(geometry_column=geo_field, bbox=bbox)

    @staticmethod
    def _get_order_by(orders):
        return '' if len(orders) == 0 else f"ORDER BY {' , '.join(map(lambda x: f'{x}', orders))}"

    @staticmethod
    def _get_intersects_query(geo_field, intersects_geom):
        return (
            None
            if intersects_geom is None
            else INTERSECTS_CLAUSE.format(geometry_column=geo_field, intersects_geom=intersects_geom)
        )

    @staticmethod
    def _get_intersects_by_geom_query(geo_field, intersects_geom):
        return (
            None
            if intersects_geom is None
            else INTERSECTS_BY_GEOM_CLAUSE.format(geometry_column=geo_field, intersects_geom=intersects_geom)
        )

    @staticmethod
    def _get_filter_query(key, operator, value):
        return FILTER_CLAUSE.format(field_name=key, operator=operator, field_value=value)

    @staticmethod
    def _converter(value):
        return None if value is None else Binary(bytes(GEOSGeometry(value).ewkb))
