from django.utils.translation import gettext_lazy as _

_LANG_EN = 'en'
_LANG_FR = 'fr'
_LANG_NL = 'nl'

_ONLINE = 'O'
_ARCHIVE = 'A'
_DELETED = 'X'
_DRAFT = 'D'
_BE_CONFIRMED = 'O'
_VALIDATED = 'V'

LANG_CHOICES = (
    (_LANG_EN, _('English')),
    (_LANG_FR, _('France')),
    (_LANG_NL, _('Netherlands')),
)

RECORD_STATE_CHOICES = (
    (_ONLINE, _('Online')),
    (_ARCHIVE, _('Archive')),
    (_DELETED, _('Deleted')),
)

DATA_STATE_CHOICES = (
    (_DRAFT, _('Draft')),
    (_BE_CONFIRMED, _('To be confirmed')),
    (_VALIDATED, _('Validated')),
)

GEO_STATE_CHOICES = (
    (_DRAFT, _('Draft')),
    (_BE_CONFIRMED, _('To be confirmed')),
    (_VALIDATED, _('Validated')),
)


DICT_TOPOLOGY = 'topology'
DICT_STATECODE = 'statecode'
DICT_GENERAL = 'general'

DICTIONARY_CHOICES = (
    (DICT_TOPOLOGY, _('Typology')),
    (DICT_STATECODE, _('State code')),
    (DICT_GENERAL, _('General')),
)
