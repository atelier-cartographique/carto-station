import os

from django.utils.text import slugify


def generate_file_name(filename, created_at):
    """
    Generates a file name
    """
    unique_val = created_at.strftime('%Y%m%d%H%M%S')
    name, extension = os.path.splitext(filename)
    return f'{unique_val}__{slugify(name[0:200])}{extension}'


def generate_file_path(creator_username, created_at, filename):
    """
    Generates a file path
    """
    name_file = generate_file_name(filename, created_at)
    return os.path.join('sitex', creator_username, name_file)


def create_file_path(instance, filename):
    """
    Creates a file path
    """
    username = instance.creator.username if instance.creator else 'unknown'
    file_path = generate_file_path(username, instance.created_at, filename)
    return file_path
