class ApiModelCreateMixin:
    """
    Add creator to the serializer
    """

    def perform_create(self, serializer):
        serializer.save(creator=self.request.user)


class ApiModelUpdateMixin:
    """
    Add creator to the serializer
    """

    def perform_update(self, serializer):
        serializer.save(creator=self.request.user)
