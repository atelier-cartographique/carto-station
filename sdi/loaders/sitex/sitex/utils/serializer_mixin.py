class GeoCreateSerializerMixin:
    """
    Mixin to add srid to geojson data
    This is a hardcoded srid, but should be configurable
    """

    def to_internal_value(self, data):
        if hasattr(self.Meta, 'geo_field'):
            geom_field = self.Meta.geo_field
            if isinstance(data.get(geom_field), dict):
                data[geom_field]['crs'] = {'type': 'name', 'properties': {'name': 'EPSG:31370'}}
        return super().to_internal_value(data)
