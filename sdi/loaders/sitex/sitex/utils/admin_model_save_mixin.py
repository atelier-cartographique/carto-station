class AdminModelSaveMixin:
    """
    Mixin to add creator to a model.
    """

    def save_model(self, request, obj, form, change):
        obj.creator = request.user
        super().save_model(request, obj, form, change)
