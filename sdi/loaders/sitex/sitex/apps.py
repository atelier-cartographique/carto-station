from django.apps import AppConfig


class SitexConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sitex'
    geodata = True

    def ready(self):
        import sitex.signals.dictionary  # noqa
        import sitex.signals.file  # noqa
        import sitex.signals.startdate_enddate  # noqa
