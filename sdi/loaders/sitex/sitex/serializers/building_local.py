from rest_framework import serializers

from sitex.models.building_local import BuildingLocal


class BuildingLocalSerializer(serializers.ModelSerializer):
    class Meta:
        model = BuildingLocal
        fields = ['id', 'idbuildversion', 'address', 'cadastralplot']
        id_fields = ['id']
        read_only_fields = ['id']


class BuildingLocalCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = BuildingLocal
        fields = ['address', 'cadastralplot']
