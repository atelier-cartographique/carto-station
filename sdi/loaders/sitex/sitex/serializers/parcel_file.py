from rest_framework import serializers

from sitex.models.parcel_file import ParcelFile
from sitex.serializers.file import FileUploadReadSerializer


class ParcelFileSerializer(serializers.ModelSerializer):
    content = serializers.SerializerMethodField()
    idparcel = serializers.CharField(source='idparcelversion.idparcel')

    class Meta:
        model = ParcelFile
        id_field = 'id'
        fields = ['id', 'idparcel', 'file', 'creator', 'content']
        read_only_fields = ['id', 'creator', 'content', 'online', 'date', 'idparcel']

    def get_content(self, obj):
        contents = FileUploadReadSerializer(obj.file)
        return contents.data


class ParcelFileCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ParcelFile
        fields = (
            'id',
            'idparcelversion',
            'date',
            'creator',
            'file',
            'online',
        )
        read_only_fields = [
            'id',
            'idparcelversion',
            'date',
            'creator',
            'online',
        ]
