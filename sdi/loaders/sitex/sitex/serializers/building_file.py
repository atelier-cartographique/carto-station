from rest_framework import serializers

from sitex.models.building_file import BuildingFile
from sitex.serializers.file import FileUploadReadSerializer


class BuildingFileSerializer(serializers.ModelSerializer):
    content = serializers.SerializerMethodField()
    idbuild = serializers.CharField(source='idbuildversion.idbuild')

    class Meta:
        model = BuildingFile
        id_field = 'id'
        fields = [
            'id',
            'idbuild',
            'file',
            'creator',
            'content',
        ]
        read_only_fields = ['id', 'creator', 'content', 'online', 'date', 'idbuild']

    def get_content(self, obj):
        contents = FileUploadReadSerializer(obj.file)
        return contents.data


class BuildingFileCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = BuildingFile
        fields = (
            'id',
            'idbuildversion',
            'date',
            'creator',
            'file',
            'online',
        )
        read_only_fields = [
            'id',
            'idbuildversion',
            'date',
            'creator',
            'online',
        ]
