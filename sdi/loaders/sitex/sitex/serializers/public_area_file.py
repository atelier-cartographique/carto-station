from rest_framework import serializers

from sitex.models.public_area_file import PublicAreaFile


class PublicAreaFileSerializer(serializers.ModelSerializer):
    filetype = serializers.CharField(source='filelocation.filetype', read_only=True)

    class Meta:
        model = PublicAreaFile
        id_fields = 'id'
        fields = [
            'id',
            'idarea_version',
            'fileonline',
            'filedate',
            'deleteddate',
            'creator',
            'filetype',
            'filelocation',
        ]
        read_only_fields = ['creator', 'filetype']
