from rest_framework_gis.serializers import GeoFeatureModelSerializer

from sitex.models.urbis_building import UrbisBuilding
from sitex.utils.query_builder_mixin import QueryBuilderMixin


class UrbisBuildingSerializer(GeoFeatureModelSerializer, QueryBuilderMixin):
    class Meta:
        model = UrbisBuilding
        geo_field = 'geom'
        id_field = 'id'
        fields = [
            'gid',
            'id',
            'status',
            'category',
            'versionid',
            'capakey',
            'area',
            'inspire_id',
            'begin_life',
            'end_life',
            'levelinfo_nblv',
            'levelinfo_retrait',
            'levelinfo_diflvl',
        ]
        read_only_fields = fields
