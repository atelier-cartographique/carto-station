from rest_framework import serializers

from sitex.models.building_group import BuildingGroup
from sitex.utils.query_builder_mixin import QueryBuilderMixin


class BuildingGroupSerializer(serializers.ModelSerializer, QueryBuilderMixin):
    class Meta:
        model = BuildingGroup
        fields = ['id', 'name']
        read_only_fields = ['id', 'created_at', 'updated_at']
