from rest_framework import serializers

from sitex.models.parcel_occupancy import ParcelOccupancy


class ParcelOccupancySerializer(serializers.ModelSerializer):
    class Meta:
        model = ParcelOccupancy
        fields = [
            'idparcelversion',
            'occupcode',
            'vacant',
            'owner',
            'description',
            'nbparkcar',
            'nbparkpmr',
            'nbelecplug',
            'nbparkbike',
            'area',
        ]


class ParcelOccupancyCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ParcelOccupancy
        fields = [
            'occupcode',
            'vacant',
            'owner',
            'description',
            'nbparkcar',
            'nbparkpmr',
            'nbelecplug',
            'nbparkbike',
            'area',
        ]
