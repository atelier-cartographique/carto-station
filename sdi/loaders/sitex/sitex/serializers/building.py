from django.conf import settings

from rest_framework_gis.serializers import GeoFeatureModelSerializer

from sitex.models.building import Building
from sitex.models.building_file import BuildingFile
from sitex.models.building_local import BuildingLocal
from sitex.models.building_occupancy import BuildingOccupancy
from sitex.serializers.building_file import (
    BuildingFileCreateSerializer,
    BuildingFileSerializer,
)
from sitex.serializers.building_local import (
    BuildingLocalCreateSerializer,
    BuildingLocalSerializer,
)
from sitex.serializers.building_occupancy import (
    BuildingOccupancyCreateSerializer,
    BuildingOccupancySerializer,
)
from sitex.utils.choices import _ARCHIVE, _ONLINE
from sitex.utils.query_builder_mixin import QueryBuilderMixin
from sitex.utils.serializer_mixin import GeoCreateSerializerMixin

MEDIA_URL = getattr(settings, 'MEDIA_URL', '')


class BuildingSerializer(GeoFeatureModelSerializer, QueryBuilderMixin):
    class Meta:
        model = Building
        geo_field = 'geom'
        id_field = 'idbuild'
        fk_field = ['creator', 'statecode', 'typologycode', 'buildgroup', 'observation_status']
        relation_field = {
            'occupancys': {
                'table': 'sitex_buildingoccupancy',
                'alias': 'bo',
                'relation': 'idbuildversion_id',
                'fk_fields': ['occupcode'],
                'fields': [
                    'occupcode',
                    'level',
                    'vacant',
                    'nbhousing',
                    'description',
                    'owner',
                    'area',
                    'back_level',
                ],
            },
            'locals': {
                'table': 'sitex_buildinglocal',
                'alias': 'bl',
                'relation': 'idbuildversion_id',
                'fields': [
                    'address',
                    'cadastralplot',
                ],
            },
            'files': {
                'table': 'sitex_buildingfile',
                'alias': 'bf',
                'relation': 'idbuildversion_id',
                'relation_detail_field': {
                    'file': {
                        'table': 'sitex_file',
                        'alias': 'sf',
                        'relation': 'file_id',
                        'concat_field': {
                            'url': {'field': 'file', 'alias': 'url', 'str_concat': MEDIA_URL},
                            'images': (
                                {'field': 'image_small', 'alias': 'small', 'str_concat': MEDIA_URL},
                                {'field': 'image_medium', 'alias': 'medium', 'str_concat': MEDIA_URL},
                                {'field': 'image_large', 'alias': 'large', 'str_concat': MEDIA_URL},
                            ),
                        },
                        'fk_fields': [
                            'creator',
                        ],
                        'fields': [
                            'id',
                            'type',
                            'name',
                            'creator',
                        ],
                    },
                },
                'fk_fields': [
                    'creator',
                    'idbuildversion',
                ],
                'fields': [
                    'id',
                    'idbuildversion',
                    'date',
                    'creator',
                    'file',
                    'online',
                ],
            },
        }
        read_only_fields = ['creator']
        fields = [
            'idbuild',
            'startdate',
            'enddate',
            'description',
            'internaldesc',
            'versiondesc',
            'creator',
            'nblevel',
            'nblevelrec',
            'statecode',
            'typologycode',
            'buildgroup',
            'groundarea',
            'nbparkcar',
            'nbparkbike',
            'u2block',
            'u2municipality',
            'u2building',
            'datastate',
            'geostate',
            'recordstate',
            'observation_status',
            'levelinfo_diflvl',
        ]


class BuildingWithDetailSerializer(GeoFeatureModelSerializer):
    occupancys = BuildingOccupancySerializer(many=True, read_only=True)
    locals = BuildingLocalSerializer(many=True, read_only=True)
    files = BuildingFileSerializer(many=True, read_only=True)

    class Meta:
        model = Building
        geo_field = 'geom'
        id_field = 'idbuild'
        fields = [
            'id',
            'idbuild',
            'startdate',
            'enddate',
            'description',
            'internaldesc',
            'versiondesc',
            'creator',
            'nblevel',
            'nblevelrec',
            'statecode',
            'typologycode',
            'buildgroup',
            'groundarea',
            'nbparkcar',
            'nbparkbike',
            'u2block',
            'u2municipality',
            'u2building',
            'datastate',
            'geostate',
            'recordstate',
            'occupancys',
            'locals',
            'files',
            'observation_status',
            'levelinfo_diflvl',
        ]
        read_only_fields = ['creator']


class BuildingCreateSerializer(GeoCreateSerializerMixin, GeoFeatureModelSerializer, QueryBuilderMixin):
    occupancys = BuildingOccupancyCreateSerializer(many=True, required=False)
    locals = BuildingLocalCreateSerializer(many=True, required=False)
    files = BuildingFileCreateSerializer(many=True, required=False)

    class Meta:
        model = Building
        geo_field = 'geom'
        id_field = 'idbuild'
        # read_only_fields = ['creator']
        fields = [
            'idbuild',
            'startdate',
            'enddate',
            'description',
            'internaldesc',
            'versiondesc',
            'creator',
            'nblevel',
            'nblevelrec',
            'statecode',
            'typologycode',
            'buildgroup',
            'groundarea',
            'nbparkcar',
            'nbparkbike',
            'u2block',
            'u2municipality',
            'u2building',
            'datastate',
            'geostate',
            'occupancys',
            'locals',
            'files',
            'observation_status',
            'levelinfo_diflvl',
        ]

    def create(self, validated_data):
        occupancys_data = validated_data.pop('occupancys', [])
        locals_data = validated_data.pop('locals', [])
        files_data = validated_data.pop('files', [])

        # change old record with same idbuild into recordstate archive
        for item in Building.objects.filter(idbuild=validated_data['idbuild'], recordstate=_ONLINE):
            item.recordstate = _ARCHIVE
            item.save()

        # Create new record into recordstate O
        building = Building.objects.create(**validated_data)
        for occupancy_data in occupancys_data:
            BuildingOccupancy.objects.create(idbuildversion=building, **occupancy_data)
        for local_data in locals_data:
            BuildingLocal.objects.create(idbuildversion=building, **local_data)
        for file_data in files_data:
            BuildingFile.objects.create(idbuildversion=building, creator=building.creator, **file_data)

        return building
