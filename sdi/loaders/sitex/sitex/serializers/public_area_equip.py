from rest_framework import serializers

from sitex.models.public_area_equip import PublicAreaEquip


class PublicAreaEquipSerializer(serializers.ModelSerializer):
    class Meta:
        model = PublicAreaEquip
        fields = [
            'id',
            'idarea_version',
            'equip_code',
        ]
