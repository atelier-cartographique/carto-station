from rest_framework import serializers

from sitex.models.parcel_local import ParcelLocal


class ParcelLocalSerializer(serializers.ModelSerializer):
    class Meta:
        model = ParcelLocal
        id_field = 'id'
        fields = ['id', 'idparcelversion', 'address', 'cadastralplot']
        read_only_fields = ['id']


class ParcelLocalCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ParcelLocal
        fields = ['address', 'cadastralplot']
