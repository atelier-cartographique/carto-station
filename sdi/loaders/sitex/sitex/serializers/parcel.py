from django.conf import settings

from rest_framework_gis.serializers import GeoFeatureModelSerializer

from sitex.models.parcel import Parcel
from sitex.models.parcel_file import ParcelFile
from sitex.models.parcel_local import ParcelLocal
from sitex.models.parcel_occupancy import ParcelOccupancy
from sitex.serializers.parcel_file import (
    ParcelFileCreateSerializer,
    ParcelFileSerializer,
)
from sitex.serializers.parcel_local import (
    ParcelLocalCreateSerializer,
    ParcelLocalSerializer,
)
from sitex.serializers.parcel_occupancy import (
    ParcelOccupancyCreateSerializer,
    ParcelOccupancySerializer,
)
from sitex.utils.choices import _ARCHIVE, _ONLINE
from sitex.utils.query_builder_mixin import QueryBuilderMixin
from sitex.utils.serializer_mixin import GeoCreateSerializerMixin

MEDIA_URL = getattr(settings, 'MEDIA_URL', '')


class ParcelSerializer(GeoFeatureModelSerializer, QueryBuilderMixin):
    class Meta:
        model = Parcel
        geo_field = 'geom'
        id_field = 'idparcel'
        fk_field = ['creator']
        relation_field = {
            'occupancys': {
                'table': 'sitex_parceloccupancy',
                'alias': 'po',
                'relation': 'idparcelversion_id',
                'fk_fields': ['occupcode'],
                'fields': [
                    'occupcode',
                    'vacant',
                    'description',
                    'nbparkcar',
                    'nbparkpmr',
                    'nbelecplug',
                    'nbparkbike',
                    'owner',
                    'area',
                ],
            },
            'locals': {
                'table': 'sitex_parcellocal',
                'alias': 'pl',
                'relation': 'idparcelversion_id',
                'fields': [
                    'address',
                    'cadastralplot',
                ],
            },
            'files': {
                'table': 'sitex_parcelfile',
                'alias': 'pf',
                'relation': 'idparcelversion_id',
                'relation_detail_field': {
                    'file': {
                        'table': 'sitex_file',
                        'alias': 'sf',
                        'relation': 'file_id',
                        'concat_field': {
                            'url': {'field': 'file', 'alias': 'url', 'str_concat': MEDIA_URL},
                            'images': (
                                {'field': 'image_small', 'alias': 'small', 'str_concat': MEDIA_URL},
                                {'field': 'image_medium', 'alias': 'medium', 'str_concat': MEDIA_URL},
                                {'field': 'image_large', 'alias': 'large', 'str_concat': MEDIA_URL},
                            ),
                        },
                        'fk_fields': [
                            'creator',
                        ],
                        'fields': [
                            'id',
                            'type',
                            'name',
                            'creator',
                        ],
                    },
                },
                'fk_fields': [
                    'creator',
                    'idparcelversion',
                ],
                'fields': [
                    'id',
                    'idparcelversion',
                    'date',
                    'creator',
                    'file',
                    'online',
                ],
            },
        }
        read_only_fields = ['id', 'creator']
        fields = [
            'idparcel',
            'startdate',
            'enddate',
            'description',
            'internaldesc',
            'versiondesc',
            'creator',
            'fenced',
            'project',
            'geom',
            'groundarea',
            'situation',
            'lining',
            'imperviousarea',
            'u2block',
            'u2municipality',
            'u2parcel',
            'datastate',
            'geostate',
            'recordstate',
        ]


class ParcelWithDetailSerializer(GeoFeatureModelSerializer):
    occupancys = ParcelOccupancySerializer(many=True, read_only=True)
    locals = ParcelLocalSerializer(many=True, read_only=True)
    files = ParcelFileSerializer(many=True, read_only=True)

    class Meta:
        model = Parcel
        geo_field = 'geom'
        id_field = 'idparcel'
        fields = [
            'id',
            'idparcel',
            'startdate',
            'enddate',
            'description',
            'internaldesc',
            'versiondesc',
            'creator',
            'fenced',
            'project',
            'geom',
            'groundarea',
            'situation',
            'lining',
            'imperviousarea',
            'u2block',
            'u2municipality',
            'u2parcel',
            'datastate',
            'geostate',
            'recordstate',
            'occupancys',
            'locals',
            'files',
        ]
        read_only_fields = ['id', 'creator']


class ParcelCreateSerializer(GeoCreateSerializerMixin, GeoFeatureModelSerializer, QueryBuilderMixin):
    occupancys = ParcelOccupancyCreateSerializer(many=True, required=False)
    locals = ParcelLocalCreateSerializer(many=True, required=False)
    files = ParcelFileCreateSerializer(many=True, required=False)

    class Meta:
        model = Parcel
        geo_field = 'geom'
        id_field = 'idparcel'
        fields = [
            'idparcel',
            'startdate',
            'enddate',
            'description',
            'internaldesc',
            'versiondesc',
            'creator',
            'fenced',
            'project',
            'geom',
            'groundarea',
            'situation',
            'lining',
            'imperviousarea',
            'u2block',
            'u2municipality',
            'u2parcel',
            'datastate',
            'geostate',
            'occupancys',
            'locals',
            'files',
        ]

    def create(self, validated_data):
        occupancys_data = validated_data.pop('occupancys', [])
        locals_data = validated_data.pop('locals', [])
        files_data = validated_data.pop('files', [])
        # change all record with same idbuild into recordstate archive
        # It was an elegant way to do it, but looks like it's somewhat buggy and doesnt seem to signal itself,
        # thus skipping end date update
        # Parcel.objects.filter(idparcel=validated_data['idparcel'], recordstate='O').update(recordstate='A')
        for item in Parcel.objects.filter(idparcel=validated_data['idparcel'], recordstate=_ONLINE):
            item.recordstate = _ARCHIVE
            item.save()

        # Create new record into recordstate O
        parcel = Parcel.objects.create(**validated_data)
        for occupancy_data in occupancys_data:
            ParcelOccupancy.objects.create(idparcelversion=parcel, **occupancy_data)
        for local_data in locals_data:
            ParcelLocal.objects.create(idparcelversion=parcel, **local_data)
        for file_data in files_data:
            ParcelFile.objects.create(idparcelversion=parcel, creator=parcel.creator, **file_data)

        return parcel
