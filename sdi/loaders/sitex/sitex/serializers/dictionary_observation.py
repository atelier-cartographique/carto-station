from rest_framework import serializers

from sitex.models.dictionary import Observation


class ObservationSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    def get_name(self, obj):
        return {
            'fr': obj.name_fr,
            'nl': obj.name_nl,
        }

    class Meta:
        model = Observation
        fields = [
            'id',
            'name',
        ]
        read_only_fields = [
            'id',
            'name',
        ]
