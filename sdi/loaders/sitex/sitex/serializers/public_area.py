# -*- coding: utf-8 -*-
from rest_framework_gis.serializers import GeoFeatureModelSerializer

from sitex.models.public_area import PublicArea
from sitex.serializers.public_area_equip import PublicAreaEquipSerializer
from sitex.serializers.public_area_file import PublicAreaFileSerializer
from sitex.serializers.public_area_local import PublicAreaLocalSerializer
from sitex.utils.query_builder_mixin import QueryBuilderMixin


class PublicAreaSerializer(GeoFeatureModelSerializer, QueryBuilderMixin):
    class Meta:
        model = PublicArea
        geo_field = 'geom'
        id_field = 'idarea'
        fk_field = ['creator']
        relation_field = {
            'equips': {'table': 'sitex_publicareaequip', 'alias': 'pae', 'relation': 'idarea_version_id'},
            'locals': {'table': 'sitex_publicarealocal', 'alias': 'pal', 'relation': 'idarea_version_id'},
            'files': {'table': 'sitex_publicareafile', 'alias': 'paf', 'relation': 'idarea_version_id'},
        }
        fields = [
            'id',
            'startdate',
            'enddate',
            'creator',
            'areatype',
            'groundarea',
            'lining',
            'liningcolor',
            'imperviousarea',
            'vegetype',
            'pieceart',
            'nbparkbike',
            'nbparkcar',
            'u2block',
            'u2municipality',
            'datastate',
            'geostage',
            'description',
            'internaldesc',
            'versiondesc',
            'recordstate',
            'idarea',
        ]
        read_only_fields = ['creator']


class PublicAreaWithDetailSerializer(GeoFeatureModelSerializer):
    equips = PublicAreaEquipSerializer(many=True, read_only=True)
    locals = PublicAreaLocalSerializer(many=True, read_only=True)
    files = PublicAreaFileSerializer(many=True, read_only=True)

    class Meta:
        model = PublicArea
        geo_field = 'geom'
        id_field = 'idarea'
        fields = [
            'id',
            'startdate',
            'enddate',
            'creator',
            'areatype',
            'groundarea',
            'lining',
            'liningcolor',
            'imperviousarea',
            'vegetype',
            'pieceart',
            'nbparkbike',
            'nbparkcar',
            'u2block',
            'u2municipality',
            'datastate',
            'geostage',
            'description',
            'internaldesc',
            'versiondesc',
            'recordstate',
            'equips',
            'locals',
            'files',
            'idarea',
        ]
        read_only_fields = ['creator']
