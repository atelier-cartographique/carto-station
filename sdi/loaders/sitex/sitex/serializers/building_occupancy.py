from rest_framework import serializers

from sitex.models.building_occupancy import BuildingOccupancy


class BuildingOccupancySerializer(serializers.ModelSerializer):
    class Meta:
        model = BuildingOccupancy
        fields = [
            'idbuildversion',
            'occupcode',
            'level',
            'vacant',
            'nbhousing',
            'description',
            'owner',
            'area',
            'back_level',
        ]


class BuildingOccupancyCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = BuildingOccupancy
        fields = [
            'occupcode',
            'level',
            'vacant',
            'nbhousing',
            'description',
            'owner',
            'area',
            'back_level',
        ]
