from rest_framework import serializers

from sitex.models.public_area_local import PublicAreaLocal


class PublicAreaLocalSerializer(serializers.ModelSerializer):
    class Meta:
        model = PublicAreaLocal
        fields = [
            'id',
            'idarea_version',
            'address',
        ]
