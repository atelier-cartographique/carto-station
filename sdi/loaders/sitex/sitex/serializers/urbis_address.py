from rest_framework_gis.serializers import GeoFeatureModelSerializer

from sitex.models.urbis_address_point import UrbisAddressPoint
from sitex.utils.query_builder_mixin import QueryBuilderMixin


class UrbisAddressSerializers(GeoFeatureModelSerializer, QueryBuilderMixin):
    class Meta:
        model = UrbisAddressPoint
        geo_field = "geom"
        id_field = "gid"
        json_field = {
            "mu_name": (
                {"field": "mu_name_fr", "alias": "fr"},
                {"field": "mu_name_du", "alias": "nl"},
            ),
            "pn_name": (
                {"field": "pn_name_fr", "alias": "fr"},
                {"field": "pn_name_du", "alias": "nl"},
            ),
        }
        join_field = {
            "building": {
                "table": "urbadm_building2d_31370",
                "alias": "bu",
                "field_prefix": "building",
                "fields": ["id"],
            },
            "parcel": {
                "table": "urb_p_capa_31370",
                "alias": "pa",
                "field_prefix": "parcel",
                "fields": ["id"],
            },
        }
        fields = (
            "gid",
            "adrn",
            "capakey",
            "mu_name_fr",
            "mu_name_du",
            "pn_name_fr",
            "pn_name_du",
        )
        read_only_fields = fields
