from rest_framework_gis.serializers import GeoFeatureModelSerializer
from rest_framework import serializers
from sitex.models.block import Block


class BlockSerializers(GeoFeatureModelSerializer):
    class Meta:
        model = Block
        geo_field = "geom"
        id_field = "id"
        fields = (
            "id",
            "gid",
            "urbis_building_count",
            "urbis_parcel_count",
            "sitex_building_count",
            "sitex_parcel_count",
            "building_occupancy_percentage",
            "parcel_occupancy_percentage",
        )
        read_only_fields = fields

    gid = serializers.SerializerMethodField()

    def get_gid(self, instance):
        return instance.id
