from drf_base64 import serializers
from rest_framework import serializers as rest_serializers

from sitex.models.file import File


class FileUploadSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = ['id', 'type', 'name', 'url', 'file', 'creator']


class FileSerializer(serializers.ModelSerializer):
    file = serializers.Base64FileField()

    class Meta:
        model = File
        fields = ['file']


class FileUploadReadSerializer(rest_serializers.ModelSerializer):
    images = rest_serializers.SerializerMethodField()
    url = rest_serializers.SerializerMethodField()

    class Meta:
        model = File
        fields = ['id', 'type', 'name', 'url', 'creator', 'images']
        read_only_fields = ['type', 'name', 'url', 'creator', 'images']

    def get_images(self, obj):
        return {
            'small': obj.url('small'),
            'medium': obj.url('medium'),
            'large': obj.url('large'),
        }

    def get_url(self, obj):
        return obj.url()
