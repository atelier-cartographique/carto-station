from rest_framework import serializers

from sitex.models.dictionary import Occupancy


class OccupancySerializer(serializers.ModelSerializer):
    keywords = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()

    def get_name(self, obj):
        return {
            'fr': obj.name_fr,
            'nl': obj.name_nl,
        }

    def get_keywords(self, obj):
        values = [
            {'fr': kywrd.name_fr, 'nl': kywrd.name_nl}
            for kywrd in obj.keywords.filter(active=True).only('name_fr', 'name_nl')
        ]
        return values

    class Meta:
        model = Occupancy
        fields = [
            'code',
            'keywords',
            'name',
        ]
        read_only_fields = [
            'code',
            'keywords',
            'name',
        ]
