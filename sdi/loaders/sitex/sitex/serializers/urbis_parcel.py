from rest_framework_gis.serializers import GeoFeatureModelSerializer

from sitex.models.urbis_parcel import UrbisParcel
from sitex.utils.query_builder_mixin import QueryBuilderMixin


class UrbisParcelSerializers(GeoFeatureModelSerializer, QueryBuilderMixin):
    class Meta:
        model = UrbisParcel
        geo_field = 'geom'
        id_field = 'id'
        fields = (
            'gid',
            'id',
            'capaty',
            'capakey',
            'shape_area',
            'sheet',
            'mu_nat_cod',
            'cd5c',
            'cdnc',
            'csnc',
            'shnc',
            'shnc_file',
            'rad_num',
            'exp_alpha',
            'exp_num',
            'apnc_mapc',
            'apnc_cadc',
            'apnc_cad',
            'apnc_map',
            'div_num',
        )
        read_only_fields = fields
