from django.db import models


class DictionaryManager(models.Manager):
    """
    Dictionary Manager
    """

    def get_queryset(self):
        return super().get_queryset().filter(active=True)


class DictionaryTermManager(models.Manager):
    """
    Custom manager for the DictionaryTerm model.
    """

    def __init__(self, category):
        super().__init__()
        self.dictionary_category = category

    def get_queryset(self):
        """
        Returns a queryset of the dictionary terms.
        """
        return super().get_queryset().filter(category=self.dictionary_category)
