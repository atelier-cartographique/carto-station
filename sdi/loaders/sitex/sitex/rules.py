from functools import partial
from rules import (add_perm, predicate)
from django.conf import settings
from django.contrib.auth.models import Group

from sitex.models.building import Building


MANAGER_GROUP = getattr(settings, 'SITEX_MANAGER_GROUP', 'sitex-manager')
SURVEY_GROUP = getattr(settings, 'SITEX_SURVEY_GROUP', 'sitex-survey')


def permission_name(perm_type, model):
    meta = model._meta
    return 'sitex.{}_{}'.format(perm_type, meta.model_name)


view = partial(permission_name, 'view')
change = partial(permission_name, 'change')
add = partial(permission_name, 'add')
delete = partial(permission_name, 'delete')


@predicate
def is_surveyor(user, _any_model):
    try:
        user.groups.get(name=SURVEY_GROUP)
        return True
    except Group.DoesNotExist:
        return False


@predicate
def is_manager(user, _any_model):
    try:
        user.groups.get(name=MANAGER_GROUP)
        return True
    except Group.DoesNotExist:
        return False


def hook():
    add_perm(view(Building), is_surveyor | is_manager)
    add_perm(change(Building), is_manager)
    add_perm(add(Building), is_manager)
    add_perm(delete(Building), is_manager)

