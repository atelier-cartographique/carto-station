# -*- coding: utf-8 -*-
from rest_framework_gis.filters import GeometryFilter
from rest_framework_gis.filterset import GeoFilterSet

from sitex.models.public_area import PublicArea


class PublicAreaFilter(GeoFilterSet):
    intersects_geom = GeometryFilter(field_name='geom', lookup_expr='intersects')

    class Meta:
        model = PublicArea
        fields = ('id', 'idarea', 'u2block')
