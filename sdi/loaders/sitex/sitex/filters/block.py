from rest_framework_gis.filters import GeometryFilter
from rest_framework_gis.filterset import GeoFilterSet

from sitex.models.urbis_block import UrbisBlock


class BlockFilter(GeoFilterSet):
    contains_geom = GeometryFilter(field_name='geom', lookup_expr='contains')

    class Meta:
        model = UrbisBlock
        fields = ('gid', 'type', 'name_fre', 'name_dut', 'inspire_id')
