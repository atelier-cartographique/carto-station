# -*- coding: utf-8 -*-
from rest_framework_gis.filters import GeometryFilter
from rest_framework_gis.filterset import GeoFilterSet

from sitex.models.parcel import Parcel


class ParcelFilter(GeoFilterSet):
    intersects_geom = GeometryFilter(field_name='geom', lookup_expr='intersects')

    class Meta:
        model = Parcel
        fields = ('id', 'idparcel', 'u2block')
