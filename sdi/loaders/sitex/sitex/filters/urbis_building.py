import json
import re

from django.db.models import Q

from django_filters.filters import CharFilter
from rest_framework_gis.filterset import GeoFilterSet

from sitex.models.urbis_block import UrbisBlock
from sitex.models.urbis_building import UrbisBuilding


class UrbisBuildingFilter(GeoFilterSet):
    intersects_geom = CharFilter(method='filter_intersects_geom')
    # this filter allows to filter building by block gid
    blocks_gid = CharFilter(method='filter_blocks_gid')

    def filter_blocks_gid(self, queryset, name, value):  # pylint: disable=unused-argument
        blocks_gid = []
        sanitize_value = re.sub('[^0-9,]', '', value)
        for val in sanitize_value.split(','):
            if val:
                blocks_gid.append(int(val))

        if not blocks_gid:
            return queryset.model.objects.none()

        blocks = UrbisBlock.objects.filter(gid__in=blocks_gid)
        # if blocks not found, return empty queryset
        if not blocks:
            return queryset.model.objects.none()

        search_block_query = Q()
        for block in blocks:
            search_block_query |= Q(geom__intersects=block.geom)

        return queryset.filter(search_block_query)

    def filter_intersects_geom(self, queryset, name, value):  # pylint: disable=unused-argument
        """Handle filter manually. Due to the GeometryFilter lookup_exp `intersects` cannot find the expected data"""
        assert json.loads(value), 'Please enter valid GeoJSON data'
        return queryset.extra(
            where=["ST_Intersects(geom, CONCAT('SRID=31370;', ST_AsText(ST_GeomFromGeoJSON(%s), 31370))::geometry)"],
            params=[value],
        )

    class Meta:
        model = UrbisBuilding
        fields = ('id', 'gid', 'blocks_gid')
