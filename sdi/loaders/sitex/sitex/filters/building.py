import re

from django.db.models import Q

from django_filters.filters import CharFilter
from rest_framework_gis.filters import GeometryFilter
from rest_framework_gis.filterset import GeoFilterSet

from sitex.models.building import Building
from sitex.models.urbis_block import UrbisBlock


class BuildingFilter(GeoFilterSet):
    intersects_geom = GeometryFilter(field_name='geom', lookup_expr='intersects')
    # this filter allows to filter building by block gid
    blocks_gid = CharFilter(method='filter_blocks_gid')

    def filter_blocks_gid(self, queryset, name, value):  # pylint: disable=unused-argument
        blocks_gid = []
        invalid_params = []
        sanitize_value = re.sub('[^0-9,]', '', value)
        for val in sanitize_value.split(','):
            if val:
                if val.isdigit():
                    blocks_gid.append(int(val))
                else:
                    invalid_params.append(True)

        if not blocks_gid or invalid_params:
            return queryset.model.objects.none()

        blocks = UrbisBlock.objects.filter(gid__in=blocks_gid)
        # if blocks not found, return empty queryset
        if not blocks:
            return queryset.model.objects.none()

        search_block_query = Q()
        for block in blocks:
            search_block_query |= Q(geom__intersects=block.geom)

        return queryset.filter(search_block_query)

    class Meta:
        model = Building
        fields = ('id', 'idbuild', 'u2block', 'blocks_gid')
