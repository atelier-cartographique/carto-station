from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from sitex.utils.timestamp_mixin import TimestampModelMixin


class BuildingOccupancy(TimestampModelMixin):
    id = models.AutoField(primary_key=True)
    idbuildversion = models.ForeignKey(
        "Building",
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        verbose_name=_("Building Version"),
        related_name="occupancys",
    )
    occupcode = models.ForeignKey(
        "sitex.Occupancy",
        verbose_name=_("Occupancy Code"),
        on_delete=models.RESTRICT,
        blank=True,
        null=True,
    )
    level = models.IntegerField(
        verbose_name=_("Level"),
        default=0,
        blank=True,
    )
    vacant = models.BooleanField(
        verbose_name=_("Vacant"),
        default=False,
    )
    nbhousing = models.IntegerField(
        verbose_name=_("Nb housing"),
        blank=True,
        null=True,
    )
    description = models.TextField(
        verbose_name=_("Description"),
        blank=True,
        null=True,
    )
    owner = models.CharField(
        verbose_name=_("Owner"),
        max_length=16,
        blank=True,
        null=True,
    )
    area = models.FloatField(
        verbose_name=_("Area"),
        blank=True,
        null=True,
    )
    back_level = models.BooleanField(
        verbose_name=_("Back Level"),
        default=False,
    )

    class Meta:
        verbose_name = _("Building Occupancy")
        verbose_name_plural = _("Building Occupancies")

    def __str__(self):
        return f"{self.id}-{self.idbuildversion}-{self.occupcode}"
