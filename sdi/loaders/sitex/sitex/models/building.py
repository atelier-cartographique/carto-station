import uuid

from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.gis.db import models
from django.template.defaultfilters import truncatechars
from django.utils.translation import gettext_lazy as _

from sitex.utils.choices import (
    _DRAFT,
    _ONLINE,
    DATA_STATE_CHOICES,
    GEO_STATE_CHOICES,
    RECORD_STATE_CHOICES,
)
from sitex.utils.timestamp_mixin import TimestampModelMixin

User = get_user_model()


class Building(TimestampModelMixin):
    id = models.AutoField(primary_key=True)
    idbuild = models.UUIDField(
        verbose_name=_('Build ID'),
        null=True,
        blank=True,
        default=uuid.uuid4,
    )
    # can be empty on recordstate = draft
    # required on recordstate = online
    startdate = models.DateTimeField(
        verbose_name=_('Start date'),
        null=True,
        blank=True,
    )
    # required on recordstate = archive
    enddate = models.DateTimeField(
        verbose_name=_('End date'),
        null=True,
        blank=True,
    )
    description = models.TextField(
        verbose_name=_('Description'),
        blank=True,
        null=True,
    )
    internaldesc = models.TextField(
        verbose_name=_('Internal description'),
        blank=True,
        null=True,
    )
    versiondesc = models.TextField(
        verbose_name=_('Version description'),
        blank=True,
        null=True,
    )
    creator = models.ForeignKey(
        User,
        on_delete=models.RESTRICT,
        verbose_name=_('Creator'),
        null=False,
        blank=False,
    )
    geom = models.MultiPolygonField(verbose_name=_('Geometry'), srid=31370)
    nblevel = models.FloatField(
        verbose_name=_('Nb level'),
        null=True,
        blank=True,
    )
    nblevelrec = models.FloatField(
        verbose_name=_('Nb level rec'),
        null=True,
        blank=True,
    )
    statecode = models.ForeignKey(
        'sitex.DictionaryTerm',
        limit_choices_to={'category': 'statecode'},
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name=_('State code'),
        related_name='statecodes',
    )
    typologycode = models.ForeignKey(
        'sitex.DictionaryTerm',
        limit_choices_to={'category': 'topology'},
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name=_('Typology code'),
        related_name='typologycode',
    )
    buildgroup = models.ForeignKey(
        'sitex.BuildingGroup',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name=_('Group'),
    )
    groundarea = models.FloatField(
        verbose_name=_('Ground area'),
        null=True,
        blank=True,
    )
    nbparkcar = models.IntegerField(
        verbose_name=_('Nb park car'),
        null=True,
        blank=True,
    )
    nbparkbike = models.IntegerField(
        verbose_name=_('Nb park bike'),
        null=True,
        blank=True,
    )
    u2block = models.IntegerField(
        verbose_name=_('Urbis block'),
        null=True,
        blank=True,
    )
    u2municipality = models.IntegerField(
        verbose_name=_('Urbis municipality'),
        null=True,
        blank=True,
    )
    u2building = models.IntegerField(
        verbose_name=_('Urbis building'),
        null=True,
        blank=True,
    )
    datastate = models.CharField(
        verbose_name=_('Data state'),
        max_length=1,
        choices=DATA_STATE_CHOICES,
        default=_DRAFT,
    )
    geostate = models.CharField(
        verbose_name=_('Geo state'),
        max_length=1,
        choices=GEO_STATE_CHOICES,
        default=_DRAFT,
    )
    recordstate = models.CharField(
        verbose_name=_('Record state'),
        max_length=1,
        choices=RECORD_STATE_CHOICES,
        default=_ONLINE,
    )
    observation_status = models.ForeignKey(
        'sitex.DictionaryTerm',
        limit_choices_to={'category': 'observation'},
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name=_('Observation code'),
        related_name='observationcodes',
    )
    levelinfo_diflvl = models.BooleanField(
        default=False,
        null=True,
        blank=True,
    )

    @property
    @admin.display(
        ordering='description',
        description='Description',
    )
    def short_description(self):
        return (
            truncatechars(self.description, 100)
            if self.description and len(self.description) > 100
            else self.description
        )

    def __str__(self):
        return f'{self.id}.{self.idbuild}'

    class Meta:
        verbose_name = _('Building')
        verbose_name_plural = _('Buildings')
