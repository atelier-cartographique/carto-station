from django.contrib.auth import get_user_model
from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _
from django.conf import settings


from sitex.utils.timestamp_mixin import TimestampModelMixin
from sitex.utils.tools import create_file_path

User = get_user_model()
MEDIA_URL = getattr(settings, 'MEDIA_URL', '')
MEDIA_ROOT = getattr(settings, 'MEDIA_ROOT')


class File(TimestampModelMixin):
    id = models.AutoField(primary_key=True)
    type = models.CharField(
        verbose_name=_('File Type'),
        max_length=500,
        null=True,
        blank=True,
    )
    creator = models.ForeignKey(
        User,
        on_delete=models.RESTRICT,
        verbose_name=_('Creator'),
        null=True,
        blank=True,
    )
    # File
    # any file can be stored
    # Original File
    file = models.FileField(
        upload_to=create_file_path,
        null=False,
        blank=False,
    )
    name = models.CharField(
        verbose_name=_('File Name'),
        max_length=255,
        null=True,
        blank=True,
    )
    # we cannot store the URL, it's bound to settings that may change => MEDIA_URL
    # url = models.TextField(
    #     verbose_name=_('File Url'),
    #     null=True,
    #     blank=True,
    # )

    # Image small
    image_small = models.ImageField(
        upload_to=create_file_path,
        null=True,
        blank=True,
    )
    image_small_name = models.CharField(
        verbose_name=_('File Name'),
        max_length=255,
        null=True,
        blank=True,
    )

    # Image Medium
    image_medium = models.ImageField(
        upload_to=create_file_path,
        null=True,
        blank=True,
    )
    image_medium_name = models.CharField(
        verbose_name=_('File Name'),
        max_length=255,
        null=True,
        blank=True,
    )

    # Image Large
    image_large = models.ImageField(
        upload_to=create_file_path,
        null=True,
        blank=True,
    )
    image_large_name = models.CharField(
        verbose_name=_('File Name'),
        max_length=255,
        null=True,
        blank=True,
    )

    def url(self, qualifier=None):
        if qualifier is None:
            return f'{MEDIA_URL}{self.file.name}'
        elif qualifier == 'small':
            return f'{MEDIA_URL}{self.image_small.name}'
        elif qualifier == 'medium':
            return f'{MEDIA_URL}{self.image_medium.name}'
        elif qualifier == 'large':
            return f'{MEDIA_URL}{self.image_large.name}'

    class Meta:
        verbose_name = _('File')
        verbose_name_plural = _('Files')

    def __str__(self):
        return f'{self.id}/{self.file.name}'
