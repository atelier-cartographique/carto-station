from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from sitex.utils.timestamp_mixin import TimestampModelMixin


class PublicAreaLocal(TimestampModelMixin):
    id = models.AutoField(primary_key=True)
    idarea_version = models.ForeignKey(
        'PublicArea',
        on_delete=models.CASCADE,
        verbose_name=_('Area version ID'),
        related_name='locals',
    )
    address = models.CharField(
        verbose_name=_('Address'),
        max_length=255,
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = _('Public Area Local')
        verbose_name_plural = _('Public Areas Locals')

    def __str__(self):
        return f'{self.id}.{self.idarea_version}'
