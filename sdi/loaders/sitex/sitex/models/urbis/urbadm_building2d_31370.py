# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.contrib.gis.db import models


class UrbadmBuilding2D31370(models.Model):
    gid = models.AutoField(primary_key=True)
    id = models.FloatField()
    versionid = models.FloatField()
    category = models.CharField(max_length=10)
    status = models.CharField(max_length=20)
    capakey = models.CharField(max_length=20)
    area = models.FloatField()
    inspire_id = models.CharField(max_length=50)
    begin_life = models.DateField()
    end_life = models.DateField()
    geom = models.MultiPolygonField(srid=31370)

    # level info additions
    levelinfo_nblv = models.FloatField()
    levelinfo_retrait = models.FloatField()
    levelinfo_diflvl = models.FloatField()

    class Meta:
        managed = False
        db_table = 'urbadm_building2d_31370'
