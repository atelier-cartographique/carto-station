# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.contrib.gis.db import models


class UrbPCapa31370(models.Model):
    gid = models.AutoField(primary_key=True)
    id = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    versionid = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    capaty = models.CharField(max_length=2, blank=True, null=True)
    capakey = models.CharField(max_length=18, blank=True, null=True)
    shape_area = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    sheet = models.CharField(max_length=18, blank=True, null=True)
    mu_nat_cod = models.CharField(max_length=5, blank=True, null=True)
    cd5c = models.CharField(max_length=5, blank=True, null=True)
    cdnc = models.CharField(max_length=7, blank=True, null=True)
    csnc = models.CharField(max_length=1, blank=True, null=True)
    shnc = models.CharField(max_length=6, blank=True, null=True)
    shnc_file = models.CharField(max_length=25, blank=True, null=True)
    rad_num = models.CharField(max_length=4, blank=True, null=True)
    exp_alpha = models.CharField(max_length=1, blank=True, null=True)
    exp_num = models.CharField(max_length=3, blank=True, null=True)
    apnc_mapc = models.CharField(max_length=50, blank=True, null=True)
    apnc_cadc = models.CharField(max_length=17, blank=True, null=True)
    apnc_cad = models.CharField(max_length=11, blank=True, null=True)
    apnc_map = models.CharField(max_length=11, blank=True, null=True)
    div_num = models.CharField(max_length=2, blank=True, null=True)
    inspire_id = models.CharField(max_length=50, blank=True, null=True)
    begin_life = models.DateField(blank=True, null=True)
    end_life = models.DateField(blank=True, null=True)
    geom = models.MultiPolygonField(srid=31370, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'urb_p_capa_31370'
