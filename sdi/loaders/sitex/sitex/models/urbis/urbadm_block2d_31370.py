# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.contrib.gis.db import models


class UrbadmBlock2D31370(models.Model):
    gid = models.AutoField(primary_key=True)
    id = models.FloatField()
    versionid = models.FloatField()
    type = models.CharField(max_length=5, blank=True, null=True)
    mu_id = models.FloatField(blank=True, null=True)
    pn_id = models.FloatField(blank=True, null=True)
    level_z = models.FloatField(blank=True, null=True)
    name_fre = models.CharField(max_length=60, blank=True, null=True)
    name_dut = models.CharField(max_length=60, blank=True, null=True)
    mu_name_fr = models.CharField(max_length=60, blank=True, null=True)
    mu_name_du = models.CharField(max_length=60, blank=True, null=True)
    area = models.FloatField(blank=True, null=True)
    inspire_id = models.CharField(max_length=50, blank=True, null=True)
    begin_life = models.DateField(blank=True, null=True)
    end_life = models.DateField(blank=True, null=True)
    geom = models.MultiPolygonField(srid=31370, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'urbadm_block2d_31370'
