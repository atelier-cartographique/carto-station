from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from sitex.utils.timestamp_mixin import TimestampModelMixin


class ParcelOccupancy(TimestampModelMixin):
    id = models.AutoField(primary_key=True)
    idparcelversion = models.ForeignKey(
        'Parcel',
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        verbose_name=_('Parcel Version'),
        related_name='occupancys',
    )
    occupcode = models.ForeignKey(
        'sitex.Occupancy',
        verbose_name=_('Occupancy Code'),
        on_delete=models.RESTRICT,
        blank=True,
        null=True,
    )
    vacant = models.BooleanField(
        verbose_name=_('Vacant'),
        default=False,
    )
    description = models.TextField(
        verbose_name=_('Description'),
        null=True,
        blank=True,
    )
    nbparkcar = models.IntegerField(
        verbose_name=_('Nb. park car'),
        default=0,
        blank=True,
        null=True,
    )
    nbparkpmr = models.IntegerField(
        verbose_name=_('Nb. park pmr'),
        default=0,
        blank=True,
        null=True,
    )
    nbelecplug = models.IntegerField(
        verbose_name=_('Nb. elec. plug'),
        default=0,
        blank=True,
        null=True,
    )
    nbparkbike = models.IntegerField(
        verbose_name=_('Nb. park bike'),
        default=0,
        blank=True,
        null=True,
    )
    owner = models.CharField(
        verbose_name=_('Owner'),
        max_length=16,
        null=True,
        blank=True,
    )
    area = models.FloatField(
        verbose_name=_('Area'),
        max_length=50,
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = _('Parcel Occupancy')
        verbose_name_plural = _('Parcel Occupancies')

    def __str__(self):
        return f'{self.id}.{self.idparcelversion}.{self.occupcode}'
