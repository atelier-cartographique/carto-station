from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from sitex.utils.timestamp_mixin import TimestampModelMixin


class Dictio(TimestampModelMixin):
    id = models.AutoField(primary_key=True)
    dtype = models.ForeignKey(
        'DictioLg',
        verbose_name=_('Dictionary'),
        on_delete=models.CASCADE,
        blank=False,
        null=False,
    )
    dcode = models.CharField(
        verbose_name=_('Code'),
        max_length=16,
    )

    class Meta:
        verbose_name = _('Dictionary Code')
        verbose_name_plural = _('Dictionaries Code')

    def __str__(self):
        return f'{self.dcode}'
