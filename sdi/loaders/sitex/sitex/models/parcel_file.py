from django.contrib.auth import get_user_model
from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from sitex.utils.timestamp_mixin import TimestampModelMixin

User = get_user_model()


class ParcelFile(TimestampModelMixin):
    id = models.AutoField(primary_key=True)
    idparcelversion = models.ForeignKey(
        'Parcel',
        verbose_name=_('Parcel'),
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        related_name='files',
    )
    online = models.BooleanField(
        verbose_name=_('Is published?'),
        default=True,
    )
    date = models.DateTimeField(verbose_name=_('File Date'), auto_now_add=True)
    deleteddate = models.DateTimeField(
        verbose_name=_('Deleted Date'),
        null=True,
        blank=True,
    )
    creator = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name=_('Creator'),
        null=False,
        blank=False,
    )
    file = models.ForeignKey(
        'File',
        verbose_name=_('File'),
        on_delete=models.DO_NOTHING,
        blank=False,
        null=False,
        related_name='file+',
    )

    class Meta:
        verbose_name = _('Parcel File')
        verbose_name_plural = _('Parcel Files')

    def __str__(self):
        return f'{self.id}.{self.file}'
