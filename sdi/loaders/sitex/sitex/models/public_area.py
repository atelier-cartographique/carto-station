import uuid

from django.contrib.auth import get_user_model
from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from sitex.utils.choices import (
    _DRAFT,
    _ONLINE,
    DATA_STATE_CHOICES,
    GEO_STATE_CHOICES,
    RECORD_STATE_CHOICES,
)
from sitex.utils.timestamp_mixin import TimestampModelMixin

User = get_user_model()


class PublicArea(TimestampModelMixin):
    id = models.AutoField(primary_key=True)
    idarea = models.UUIDField(
        verbose_name=_('Area ID'),
        blank=True,
        null=True,
        default=uuid.uuid4,
    )
    startdate = models.DateTimeField(verbose_name=_('Start Date'))
    enddate = models.DateTimeField(
        verbose_name=_('End Date'),
        blank=True,
        null=True,
    )
    creator = models.ForeignKey(
        User,
        verbose_name=_('Creator'),
        on_delete=models.CASCADE,
    )
    geom = models.MultiPolygonField(verbose_name=_('Geometry'), srid=31370)
    areatype = models.CharField(
        verbose_name=_('Area Type'),
        max_length=16,
    )
    groundarea = models.FloatField(verbose_name=_('Ground Area'))
    lining = models.CharField(
        verbose_name=_('Lining'),
        max_length=16,
    )
    liningcolor = models.IntegerField(verbose_name=_('Lining Color'))
    imperviousarea = models.FloatField(verbose_name=_('Impervious Area'))
    vegetype = models.CharField(
        verbose_name=_('Vegetype'),
        max_length=16,
    )
    pieceart = models.BooleanField(verbose_name=_('Piece Art'))
    nbparkbike = models.IntegerField(verbose_name=_('Nb Park Bike'))
    nbparkcar = models.IntegerField(verbose_name=_('Nb Park Car'))
    u2block = models.IntegerField(verbose_name=_('U2Block'))
    u2municipality = models.IntegerField(verbose_name=_('U2Municipality'))
    description = models.TextField(
        verbose_name=_('Description'),
        blank=True,
        null=True,
    )
    internaldesc = models.TextField(
        verbose_name=_('Internal Description'),
        blank=True,
        null=True,
    )
    versiondesc = models.TextField(
        verbose_name=_('Version Description'),
        blank=True,
        null=True,
    )
    datastate = models.CharField(
        verbose_name=_('Data state'),
        max_length=1,
        choices=DATA_STATE_CHOICES,
        default=_DRAFT,
    )
    geostage = models.CharField(
        verbose_name=_('Geo state'),
        max_length=1,
        choices=GEO_STATE_CHOICES,
        default=_DRAFT,
    )
    recordstate = models.CharField(
        verbose_name=_('Record state'),
        max_length=1,
        choices=RECORD_STATE_CHOICES,
        default=_ONLINE,
    )

    class Meta:
        verbose_name = _('Public Area')
        verbose_name_plural = _('Public Areas')

    def __str__(self):
        return f'{self.id}.{self.idarea}'
