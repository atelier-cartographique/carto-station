import uuid

from django.db import models
from django.utils.translation import gettext_lazy as _

from sitex.utils.timestamp_mixin import TimestampModelMixin


class BuildingGroup(TimestampModelMixin):
    """
    Building groups model
    """

    id = models.UUIDField(
        primary_key=True,
        verbose_name=_('Groups ID'),
        default=uuid.uuid4,
    )
    name = models.TextField(
        verbose_name=_('Name'),
        blank=False,
        null=False,
    )

    def __str__(self):
        return f'{self.id}-{self.name}'

    class Meta:
        verbose_name = _('Building group')
        verbose_name_plural = _('Building groups')
        ordering = ['name']
