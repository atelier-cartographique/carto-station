from django.conf import settings
from django.contrib.gis.db import models
from django.utils.translation import gettext as _

from sitex.models.urbis.urb_p_capa_31370 import UrbPCapa31370

CONNECTION_ALIAS = getattr(settings, 'SITEX_URBIS_CONNECTION_ALIAS', 'urbis')


class UrbisParcel(UrbPCapa31370):
    """
    Class for Urbis parcel proxy
    """

    objects = models.Manager().db_manager(CONNECTION_ALIAS)

    def __str__(self):
        return f'{self.id}.{self.versionid}'

    class Meta:
        proxy = True
        verbose_name = _('Urbis Parcel')
        verbose_name_plural = _('Urbis Parcels')
