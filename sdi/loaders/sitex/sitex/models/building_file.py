from django.contrib.auth import get_user_model
from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from sitex.utils.timestamp_mixin import TimestampModelMixin

User = get_user_model()


class BuildingFile(TimestampModelMixin):
    id = models.AutoField(primary_key=True)
    idbuildversion = models.ForeignKey(
        'Building',
        verbose_name=_('Building'),
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        related_name='files',
    )
    online = models.BooleanField(
        verbose_name=_('Is published?'),
        default=True,
    )
    date = models.DateTimeField(verbose_name=_('File Date'), auto_now_add=True)
    deleteddate = models.DateTimeField(
        verbose_name=_('Deleted Date'),
        null=True,
        blank=True,
    )
    creator = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name=_('Creator'),
        null=False,
        blank=False,
    )
    file = models.ForeignKey(
        'File',
        verbose_name=_('File'),
        on_delete=models.DO_NOTHING,
        blank=False,
        null=False,
        related_name='file+',
    )

    class Meta:
        verbose_name = _('Building File')
        verbose_name_plural = _('Building Files')

    def __str__(self):
        return f'{self.id}.{self.file}'
