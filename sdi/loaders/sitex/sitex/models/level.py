from django.db import models
from django.conf import settings

CONNECTION_ALIAS = getattr(settings, 'SITEX_LEVEL_INFO_CONNECTION_ALIAS', 'default')


class LevelInfo(models.Model):
    class Meta:
        managed = False
        db_table = 'level_info'

    objects = models.Manager().db_manager(CONNECTION_ALIAS)

    id = models.IntegerField(primary_key=True)
    nblvl = models.FloatField(blank=True, null=True)
    retrait = models.FloatField(blank=True, null=True)
