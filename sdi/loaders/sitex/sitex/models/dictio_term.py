from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from sitex.utils.choices import LANG_CHOICES
from sitex.utils.timestamp_mixin import TimestampModelMixin


class ENTermManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(lang='EN')


class FRTermManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(lang='FR')


class NLTermManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(lang='NL')


class DictioTerm(TimestampModelMixin):
    id = models.AutoField(primary_key=True)
    did = models.ForeignKey(
        'Dictio',
        verbose_name=_('Dictionary'),
        on_delete=models.CASCADE,
        blank=False,
        null=False,
    )
    isValue = models.BooleanField(
        default=True,
        verbose_name=_('Is value'),
    )
    lang = models.CharField(
        verbose_name=_('Language'),
        max_length=6,
        choices=LANG_CHOICES,
    )
    term = models.CharField(
        verbose_name=_('Term'),
        max_length=50,
    )

    class Meta:
        verbose_name = _('Dictionary Term')
        verbose_name_plural = _('Dictionary Terms')

    def __str__(self):
        return f'{self.term} - {self.lang}'


class DictioTermEN(DictioTerm):
    object = ENTermManager()

    class Meta:
        proxy = True
        verbose_name = _('English Term')
        verbose_name_plural = _('English Terms')

    def save(self, *args, **kwargs):
        self.lang = 'EN'
        super(DictioTermEN, self).save(*args, **kwargs)


class DictioTermFR(DictioTerm):
    object = FRTermManager()

    class Meta:
        proxy = True
        verbose_name = _('France Term')
        verbose_name_plural = _('France Terms')

    def save(self, *args, **kwargs):
        self.lang = 'FR'
        super().save(*args, **kwargs)


class DictioTermNL(DictioTerm):
    object = NLTermManager()

    class Meta:
        proxy = True
        verbose_name = _('Netherlands Term')
        verbose_name_plural = _('Netherlands Terms')

    def save(self, *args, **kwargs):
        self.lang = 'NL'
        super().save(*args, **kwargs)
