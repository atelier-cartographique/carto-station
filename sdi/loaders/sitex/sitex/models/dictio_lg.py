from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from sitex.utils.timestamp_mixin import TimestampModelMixin


class DictioLg(TimestampModelMixin):
    LANG_EN = 'en-US'
    LANG_FR = 'fr-BE'
    LANG_NL = 'nl-NL'

    LANG_CHOICES = (
        (LANG_EN, 'English'),
        (LANG_FR, 'France'),
        (LANG_NL, 'Netherlands'),
    )

    id = models.AutoField(primary_key=True)
    dtype = models.CharField(
        verbose_name=_('Type'),
        max_length=10,
        unique=True,
    )
    lang = models.CharField(
        verbose_name=_('Language'),
        max_length=6,
        choices=LANG_CHOICES,
    )
    name = models.CharField(
        verbose_name=_('Name'),
        max_length=50,
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = _('Dictionary')
        verbose_name_plural = _('Dictionaries')
        unique_together = [['dtype', 'lang']]

    def __str__(self):
        return f'{self.name}'
