from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from sitex.utils.timestamp_mixin import TimestampModelMixin


class BuildingLocal(TimestampModelMixin):
    id = models.AutoField(primary_key=True)
    idbuildversion = models.ForeignKey(
        'Building',
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        verbose_name=_('Building version'),
        related_name='locals',
    )
    address = models.CharField(
        verbose_name=_('Address'),
        max_length=255,
        blank=True,
        null=True,
    )
    cadastralplot = models.CharField(
        verbose_name=_('Cadastral plot'),
        max_length=50,
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = _('Building Local')
        verbose_name_plural = _('Building Locals')

    def __str__(self):
        return f'{self.id}.{self.idbuildversion}.{self.address}'
