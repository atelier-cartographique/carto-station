from django.conf import settings
from django.contrib.gis.db import models
from django.utils.translation import gettext as _

from sitex.models.building import Building
from sitex.models.urbis.urbadm_block2d_31370 import UrbadmBlock2D31370
from sitex.models.urbis_building import UrbisBuilding

CONNECTION_ALIAS = getattr(settings, 'SITEX_URBIS_CONNECTION_ALIAS', 'urbis')


class UrbisBlock(UrbadmBlock2D31370):
    """
    Class for Urbis block proxy
    """

    objects = models.Manager().db_manager(CONNECTION_ALIAS)

    def __str__(self):
        return f'{self.id}.{self.versionid}'

    @property
    def urbis_building(self):
        if not self.geom:
            return []

        return UrbisBuilding.objects.filter(geom__intersects=self.geom)

    @property
    def sitex_building(self):
        if not self.geom:
            return []

        return Building.objects.filter(geom__intersects=self.geom)

    class Meta:
        proxy = True
        verbose_name = _('Urbis Block')
        verbose_name_plural = _('Urbis Blocks')
