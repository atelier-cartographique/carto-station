from typing import Any
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.forms import CharField
from sitex.managers.dictionary import DictionaryTermManager
from sitex.utils.timestamp_mixin import TimestampModelMixin
from sitex.utils.validators import validate_dict_code


class SmallTexField(models.TextField):

    def formfield(self, **kwargs: Any) -> Any:
        return CharField(max_length=1024, required=not self.blank)


class DictionaryAbstract(TimestampModelMixin):
    """
    Dictionary base model
    """

    name_fr = SmallTexField(verbose_name=_("Name (French)"))
    name_nl = SmallTexField(verbose_name=_("Name (Dutch)"))
    name_en = SmallTexField(verbose_name=_("Name (English)"), blank=True, null=True)
    active = models.BooleanField(
        default=True,
        verbose_name=_("Active"),
        help_text=_("Active"),
    )

    order = models.IntegerField(default=1)

    class Meta:
        abstract = True


class Occupancy(DictionaryAbstract):
    """
    Occupancy model
    """

    code = models.CharField(
        primary_key=True,
        validators=[validate_dict_code],
        max_length=11,
        verbose_name=_("Code"),
    )

    def __str__(self):
        return f"{self.code}/{self.name_fr or self.name_nl or self.name_en}"

    class Meta:
        verbose_name = _("Occupancy")
        verbose_name_plural = _("Occupancies")
        ordering = ["code"]


class Keyword(DictionaryAbstract):
    """
    Keyword model
    """

    id = models.AutoField(primary_key=True)
    code = models.ForeignKey(
        "sitex.Occupancy",
        on_delete=models.CASCADE,
        verbose_name=_("Occupancy"),
        related_name="keywords",
    )

    def __str__(self):
        return f"{self.code}/{self.name_fr or self.name_nl or self.name_en}"

    class Meta:
        verbose_name = _("Keyword")
        verbose_name_plural = _("Keywords")
        indexes = [
            models.Index(fields=["code"]),
        ]
        ordering = ["code"]


class DictionaryTerm(DictionaryAbstract):
    """
    Dictionary term model
    """

    class CategoryChoices(models.TextChoices):
        TOPOLOGY = ("topology", _("Topology"))
        STATECODE = ("statecode", _("Statecode"))
        GENERAL = ("general", _("General"))
        OBSERVATION = ("observation", _("Observation"))

    id = models.AutoField(primary_key=True)
    category = models.CharField(
        max_length=12,
        verbose_name=_("Category"),
        choices=CategoryChoices.choices,
        default=CategoryChoices.GENERAL,
        null=False,
        blank=False,
    )

    def __str__(self):
        return f"{self.name_fr or self.name_nl or self.name_en}"

    class Meta:
        verbose_name = _("Dictionary term")
        verbose_name_plural = _("Dictionary terms")
        indexes = [
            models.Index(fields=["category", "active"]),
        ]


class Topology(DictionaryTerm):
    """
    Topology model
    """

    objects = DictionaryTermManager(DictionaryTerm.CategoryChoices.TOPOLOGY)

    def save(self, *args, **kwargs):
        self.category = DictionaryTerm.CategoryChoices.TOPOLOGY
        super().save(*args, **kwargs)

    class Meta:
        proxy = True
        verbose_name = _("Topology")
        verbose_name_plural = _("Topologies")
        ordering = ["order"]


class StateCode(DictionaryTerm):
    """
    State code model
    """

    objects = DictionaryTermManager(DictionaryTerm.CategoryChoices.STATECODE)

    def save(self, *args, **kwargs):
        self.category = DictionaryTerm.CategoryChoices.STATECODE
        super().save(*args, **kwargs)

    class Meta:
        proxy = True
        verbose_name = _("State code")
        verbose_name_plural = _("State codes")


class Observation(DictionaryTerm):
    """
    Observation code
    """

    objects = DictionaryTermManager(DictionaryTerm.CategoryChoices.OBSERVATION)

    def save(self, *args, **kwargs):
        self.category = DictionaryTerm.CategoryChoices.OBSERVATION
        super().save(*args, **kwargs)

    class Meta:
        proxy = True
        verbose_name = _("Observation code")
        verbose_name_plural = _("Observation codes")
        ordering = ["order"]
