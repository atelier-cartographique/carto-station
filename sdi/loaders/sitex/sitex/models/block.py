from django.contrib.gis.db import models
from django.contrib.postgres.fields import ArrayField

from sitex.models.building import Building
from sitex.models.urbis_building import UrbisBuilding


class Block(models.Model):
    id = models.AutoField(primary_key=True)
    geom = models.MultiPolygonField(srid=31370, blank=True, null=True)
    urbis_source = ArrayField(base_field=models.IntegerField())
    prdd_source = ArrayField(base_field=models.IntegerField())
    manual_source = ArrayField(base_field=models.IntegerField())
    urbis_building_count = models.BigIntegerField(blank=True, null=True)
    urbis_parcel_count = models.BigIntegerField(blank=True, null=True)
    sitex_building_count = models.BigIntegerField(blank=True, null=True)
    sitex_parcel_count = models.BigIntegerField(blank=True, null=True)
    building_occupancy_percentage = models.DecimalField(
        max_digits=65535, decimal_places=2, blank=True, null=True
    )
    parcel_occupancy_percentage = models.DecimalField(
        max_digits=65535, decimal_places=2, blank=True, null=True
    )

    """
    Table 'blocks' exists on both schema 'public' and 'urbis'
    The table we are using here is the table view.

    Switching connection between two scheme won't working because there are more column on the table view
    objects = models.Manager().db_manager('default')
    """

    def __str__(self):
        if len(self.urbis_source):
            source = "urbis"
        elif len(self.prdd_source):
            source = "prdd"
        elif len(self.manual_source):
            source = "manual"

        return f"{self.id} <{source}>"

    @property
    def urbis_building(self):
        if not self.geom:
            return []

        return UrbisBuilding.objects.filter(geom__intersects=self.geom)

    @property
    def sitex_building(self):
        return Building.objects.filter(geom__intersects=self.geom)

    class Meta:
        managed = False
        db_table = "blocks"
