from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from sitex.utils.timestamp_mixin import TimestampModelMixin


class PublicAreaEquip(TimestampModelMixin):
    id = models.AutoField(primary_key=True)
    idarea_version = models.ForeignKey(
        'PublicArea',
        on_delete=models.CASCADE,
        verbose_name=_('Area version ID'),
        related_name='equips',
    )
    equip_code = models.CharField(
        max_length=16,
        verbose_name=_('Equip Code'),
    )

    class Meta:
        verbose_name = _('Public Area Equipment')
        verbose_name_plural = _('Public Areas Equipments')

    def __str__(self):
        return f'{self.id}.{self.idarea_version}'
