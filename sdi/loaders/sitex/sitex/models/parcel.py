import uuid

from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.gis.db import models
from django.template.defaultfilters import truncatechars
from django.utils.translation import gettext_lazy as _

from sitex.utils.choices import (
    _DRAFT,
    _ONLINE,
    DATA_STATE_CHOICES,
    GEO_STATE_CHOICES,
    RECORD_STATE_CHOICES,
)
from sitex.utils.timestamp_mixin import TimestampModelMixin

User = get_user_model()


class Parcel(TimestampModelMixin):
    id = models.AutoField(primary_key=True)
    idparcel = models.UUIDField(
        verbose_name=_('Parcel ID'),
        blank=True,
        null=True,
        default=uuid.uuid4,
    )
    startdate = models.DateTimeField(
        verbose_name=_('Start date'),
        null=True,
        blank=True,
    )
    enddate = models.DateTimeField(
        verbose_name=_('End date'),
        null=True,
        blank=True,
    )
    description = models.TextField(
        verbose_name=_('Description'),
        blank=True,
        null=True,
    )
    internaldesc = models.TextField(
        verbose_name=_('Internal description'),
        blank=True,
        null=True,
    )
    versiondesc = models.TextField(
        verbose_name=_('Version description'),
        blank=True,
        null=True,
    )
    creator = models.ForeignKey(
        User,
        on_delete=models.RESTRICT,
        verbose_name=_('Creator'),
        null=False,
        blank=False,
    )
    fenced = models.BooleanField(
        verbose_name=_('Fenced'),
        default=False,
    )
    project = models.CharField(
        verbose_name=_('Project'),
        max_length=255,
        blank=True,
        null=True,
    )
    geom = models.MultiPolygonField(verbose_name=_('Geometry'), srid=31370)
    groundarea = models.FloatField(
        verbose_name=_('Ground area'),
        null=True,
        blank=True,
    )
    situation = models.CharField(
        verbose_name=_('Situation'),
        max_length=16,
        blank=True,
        null=True,
    )
    lining = models.CharField(
        verbose_name=_('Lining'),
        max_length=16,
        blank=True,
        null=True,
    )
    imperviousarea = models.FloatField(
        verbose_name=_('Impervious area'),
        null=True,
        blank=True,
    )
    u2block = models.IntegerField(
        verbose_name=_('U2 block'),
        null=True,
        blank=True,
    )
    u2municipality = models.IntegerField(
        verbose_name=_('U2 municipality'),
        null=True,
        blank=True,
    )
    u2parcel = models.IntegerField(
        verbose_name=_('Urbis parcel'),
        null=True,
        blank=True,
    )
    datastate = models.CharField(
        verbose_name=_('Data state'),
        max_length=1,
        choices=DATA_STATE_CHOICES,
        default=_DRAFT,
    )
    geostate = models.CharField(
        verbose_name=_('Geo state'),
        max_length=1,
        choices=GEO_STATE_CHOICES,
        default=_DRAFT,
    )
    recordstate = models.CharField(
        verbose_name=_('Record state'),
        max_length=1,
        choices=RECORD_STATE_CHOICES,
        default=_ONLINE,
    )

    @property
    @admin.display(
        ordering='description',
        description='Description',
    )
    def short_description(self):
        return (
            truncatechars(self.description, 100)
            if self.description and len(self.description) > 100
            else self.description
        )

    def __str__(self):
        return f'{self.id}.{self.idparcel}'

    class Meta:
        verbose_name = _('Parcel')
        verbose_name_plural = _('Parcels')
