from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from sitex.utils.timestamp_mixin import TimestampModelMixin


class ParcelLocal(TimestampModelMixin):
    id = models.AutoField(primary_key=True)
    idparcelversion = models.ForeignKey(
        'Parcel',
        verbose_name=_('Parcel'),
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        related_name='locals',
    )
    address = models.CharField(
        verbose_name=_('Address'),
        max_length=255,
        null=False,
        blank=False,
    )
    cadastralplot = models.IntegerField(
        verbose_name=_('Cadastral plot'),
        null=False,
        blank=False,
    )

    class Meta:
        verbose_name = _('Parcel Local')
        verbose_name_plural = _('Parcels Local')

    def __str__(self):
        return f'{self.id}.{self.idparcelversion}.{self.address}'
