from django.contrib.auth import get_user_model
from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from sitex.utils.timestamp_mixin import TimestampModelMixin

User = get_user_model()


class PublicAreaFile(TimestampModelMixin):
    id = models.AutoField(primary_key=True)
    idarea_version = models.ForeignKey(
        'PublicArea',
        verbose_name=_('Area version ID'),
        on_delete=models.CASCADE,
        related_name='files',
    )
    fileonline = models.BooleanField(
        verbose_name=_('File Online'),
        null=False,
        blank=False,
    )
    filedate = models.DateTimeField(
        verbose_name=_('File Date'),
        null=False,
        blank=False,
    )
    deleteddate = models.DateTimeField(
        verbose_name=_('Deleted Date'),
        null=True,
        blank=True,
    )
    creator = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name=_('Creator'),
        null=False,
        blank=False,
    )
    filelocation = models.ForeignKey(
        'File',
        verbose_name=_('File'),
        on_delete=models.DO_NOTHING,
        blank=False,
        null=False,
        related_name='file_locations+',
    )

    class Meta:
        verbose_name = _('Public Area File')
        verbose_name_plural = _('Public Areas Files')

    def __str__(self):
        return f'{self.filelocation}'
