from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from sitex.models.building_occupancy import BuildingOccupancy
from sitex.serializers.building_occupancy import BuildingOccupancySerializer
from sitex.utils.api_model_write_mixin import ApiModelCreateMixin, ApiModelUpdateMixin


class BuildingOccupancyView(ApiModelCreateMixin, ListCreateAPIView):
    """
    API endpoint that allows users to be viewed or edited.
    """

    queryset = BuildingOccupancy.objects.all()
    serializer_class = BuildingOccupancySerializer


class BuildingOccupancyDetailView(ApiModelUpdateMixin, RetrieveUpdateDestroyAPIView):
    """
    API endpoint that allows users to be viewed or edited.
    """

    queryset = BuildingOccupancy.objects.all()
    serializer_class = BuildingOccupancySerializer
    lookup_field = 'id'
