import re

from django.conf import settings
from django.db import connections

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.generics import ListAPIView
from rest_framework.response import Response

from sitex.models.block import Block
from sitex.models.urbis_building import UrbisBuilding
from sitex.serializers.urbis_address import UrbisAddressSerializers

CONNECTION_ALIAS = getattr(settings, 'SITEX_URBIS_CONNECTION_ALIAS', 'urbis')


class UrbisAddressView(ListAPIView):
    """
    API endpoint that allows users to be viewed or edited.
    """

    serializer_class = UrbisAddressSerializers

    # Swagger config parameter
    id_config = openapi.Parameter(
        name='id',
        in_=openapi.IN_QUERY,
        type=openapi.TYPE_INTEGER,
    )
    gid_config = openapi.Parameter(
        name='gid',
        in_=openapi.IN_QUERY,
        type=openapi.TYPE_INTEGER,
    )
    intersects_geom_config = openapi.Parameter(
        name='intersects_geom',
        in_=openapi.IN_QUERY,
        description='Filter using intersect GeoJSON e.g: {"type": "String", "coordinates":[]}',
        type=openapi.TYPE_OBJECT,
    )
    in_bbox_config = openapi.Parameter(
        name='in_bbox',
        in_=openapi.IN_QUERY,
        description='Specify a bounding box as filter: in_bbox=min_lon,min_lat,max_lon,max_lat',
        type=openapi.TYPE_STRING,
    )
    blocks_gid_config = openapi.Parameter(
        name='blocks_gid',
        in_=openapi.IN_QUERY,
        description='Filter using one or multiple block gid separated by comma e.g: 33,34,35',
        type=openapi.TYPE_STRING,
    )
    buildings_gid_config = openapi.Parameter(
        name='buildings_gid',
        in_=openapi.IN_QUERY,
        description='Filter using one or multiple building id separated by comma e.g: 33,34,35',
        type=openapi.TYPE_STRING,
    )

    def get_queryset(self):
        params = {}
        params['filter'] = {}
        for key, value in self.request.GET.items():
            if key == 'blocks_gid':
                blocks_gid = []
                blocks_geom = []
                sanitize_value = re.sub('[^0-9,]', '', self.request.GET.get('blocks_gid'))
                for val in sanitize_value.split(','):
                    if val:
                        blocks_gid.append(int(val))
                blocks = Block.objects.filter(id__in=blocks_gid)
                if blocks:
                    for block in blocks:
                        blocks_geom.append(block.geom)
                params[key] = blocks_geom
            elif key == 'buildings_gid':
                buildings_gid = []
                buildings_geom = []
                sanitize_value = re.sub('[^0-9,]', '', self.request.GET.get('buildings_gid'))
                for val in sanitize_value.split(','):
                    if val:
                        buildings_gid.append(int(val))
                buildings = UrbisBuilding.objects.filter(gid__in=buildings_gid)
                if buildings:
                    for building in buildings:
                        buildings_geom.append(building.geom)
                params[key] = buildings_geom
            elif key in ('gid', 'id'):
                params['filter'][key] = value
            else:
                params[key] = value

        raw_query = self.serializer_class().get_json_raw_query(**params)
        connection = connections[CONNECTION_ALIAS]
        with connection.cursor() as cursor:
            cursor.execute(raw_query)
            row = cursor.fetchone()
        return row

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()[0]
        return Response(queryset)

    @swagger_auto_schema(
        manual_parameters=[id_config, gid_config, intersects_geom_config, in_bbox_config, blocks_gid_config]
    )
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
