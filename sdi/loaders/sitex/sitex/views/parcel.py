# -*- coding: utf-8 -*-
import re

from django.db import connection

from api.permissions import ObjectPermissions
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView
from rest_framework.response import Response

from sitex.models.file import File
from sitex.models.parcel import Parcel
from sitex.models.block import Block
from sitex.serializers.file import FileUploadReadSerializer
from sitex.serializers.parcel import (
    ParcelCreateSerializer,
    ParcelSerializer,
    ParcelWithDetailSerializer,
)
from sitex.utils.api_model_write_mixin import ApiModelCreateMixin


class ParcelListCreateAPIView(ApiModelCreateMixin, ListCreateAPIView):
    """
    API endpoint that allows users to be viewed or edited.
    """

    serializer_class = ParcelSerializer
    permission_classes = (ObjectPermissions,)

    # Swagger config parameter
    id_config = openapi.Parameter(
        name='id',
        in_=openapi.IN_QUERY,
        type=openapi.TYPE_INTEGER,
    )
    idparcel_config = openapi.Parameter(
        name='idparcel',
        in_=openapi.IN_QUERY,
        type=openapi.TYPE_STRING,
    )
    u2block_config = openapi.Parameter(
        name='u2block',
        in_=openapi.IN_QUERY,
        type=openapi.TYPE_INTEGER,
    )
    history_config = openapi.Parameter(
        name='history',
        in_=openapi.IN_QUERY,
        description='Result include history data e.g: (Y/N)',
        type=openapi.TYPE_STRING,
    )
    details_config = openapi.Parameter(
        name='details',
        in_=openapi.IN_QUERY,
        description='Showing detail relations e.g: (Y/N)',
        type=openapi.TYPE_STRING,
    )
    in_bbox_config = openapi.Parameter(
        name='in_bbox',
        in_=openapi.IN_QUERY,
        description='Specify a bounding box as filter: in_bbox=min_lon,min_lat,max_lon,max_lat',
        type=openapi.TYPE_STRING,
    )
    intersects_geom_config = openapi.Parameter(
        name='intersects_geom',
        in_=openapi.IN_QUERY,
        description='Filter using intersect GeoJSON e.g: {"type": "String", "coordinates":[]}',
        type=openapi.TYPE_OBJECT,
    )
    blocks_gid_config = openapi.Parameter(
        name='blocks_gid',
        in_=openapi.IN_QUERY,
        description='Filter using one or multiple block gid separated by comma e.g: 33,34,35',
        type=openapi.TYPE_STRING,
    )

    def create(self, request, *args, **kwargs):
        files = request.data.pop('files', None)
        if files:
            request.data['files'] = [{'file': f['file']['id']} for f in files]
        serializer_parcel = ParcelCreateSerializer(data=request.data)
        serializer_parcel.is_valid(raise_exception=True)
        self.perform_create(serializer_parcel)

        # Handle relation to file
        files = serializer_parcel.data['properties']['files']
        if len(files) > 0:
            for i in range(len(files)):
                file_pk = serializer_parcel.data['properties']['files'][i]['file']
                file = File.objects.get(pk=file_pk)
                object_file = FileUploadReadSerializer(file)
                serializer_parcel.data['properties']['files'][i]['file'] = object_file.data

        headers = self.get_success_headers(serializer_parcel.data)
        return Response(serializer_parcel.data, status=status.HTTP_201_CREATED, headers=headers)

    def get_queryset(self):
        params = {}
        params['filter'] = {}
        params['filter']['history'] = 'N'
        for key, value in self.request.GET.items():
            if key == 'blocks_gid':
                blocks_gid = []
                blocks_geom = []
                sanitize_value = re.sub('[^0-9,]', '', self.request.GET.get('blocks_gid'))
                for val in sanitize_value.split(','):
                    if val:
                        blocks_gid.append(int(val))
                blocks = Block.objects.filter(id__in=blocks_gid)
                if blocks:
                    for block in blocks:
                        blocks_geom.append(block.geom)
                params[key] = blocks_geom
            elif key in ('idparcel', 'u2block', 'history', 'id'):
                params['filter'][key] = value
            else:
                params[key] = value

        if self.request.GET.get('id'):
            del params['filter']['history']

        raw_query = self.serializer_class().get_raw_query(**params)
        with connection.cursor() as cursor:
            cursor.execute(raw_query)
            row = cursor.fetchone()
        return row

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()[0]
        return Response(queryset)

    @swagger_auto_schema(
        manual_parameters=[
            id_config,
            idparcel_config,
            u2block_config,
            history_config,
            details_config,
            in_bbox_config,
            intersects_geom_config,
            blocks_gid_config,
        ]
    )
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ParcelDetailUpdateAPIView(RetrieveUpdateAPIView):
    serializer_class = ParcelSerializer
    lookup_field = 'id'

    # Swagger config parameter for bbox and contains_geom
    history_config = openapi.Parameter(
        name='history',
        in_=openapi.IN_QUERY,
        description='Result include history data e.g: (Y/N)',
        type=openapi.TYPE_STRING,
    )
    details_config = openapi.Parameter(
        name='details',
        in_=openapi.IN_QUERY,
        description='Showing detail relations e.g: (Y/N)',
        type=openapi.TYPE_STRING,
    )

    def get_queryset(self):
        args_query = {}
        if self.request.GET.get('history'):
            if self.request.GET.get('history') in ('Y', 'y'):
                args_query['recordstate__in'] = ['A', 'O', 'X']
            else:
                args_query['recordstate__in'] = ['O']
        else:
            args_query['recordstate__in'] = ['O']
        return Parcel.objects.filter(**args_query)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()

        if request.GET.get('details'):
            if request.GET.get('details') in ('Y', 'y'):
                serializer = ParcelWithDetailSerializer(instance)
                return Response(serializer.data)

        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    @swagger_auto_schema(manual_parameters=[history_config, details_config])
    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)
