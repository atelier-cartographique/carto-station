from rest_framework.generics import ListAPIView

from sitex.models.dictionary import StateCode
from sitex.serializers.dictionary_statecode import StateCodeSerializer


class StateCodeAPIView(ListAPIView):
    """
    API endpoint that allows dictionary state code to be viewed.
    """

    serializer_class = StateCodeSerializer
    pagination_class = None
    queryset = StateCode.objects.filter(active=True)
