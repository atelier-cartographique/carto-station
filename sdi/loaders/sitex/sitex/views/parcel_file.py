from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from sitex.models.parcel_file import ParcelFile
from sitex.serializers.parcel_file import ParcelFileSerializer
from sitex.utils.api_model_write_mixin import ApiModelCreateMixin, ApiModelUpdateMixin


class ParcelFileView(ApiModelCreateMixin, ListCreateAPIView):
    """
    API Endpoint that allows ParcelFile to be viewed or created.
    """

    queryset = ParcelFile.objects.all()
    serializer_class = ParcelFileSerializer


class ParcelFileDetailUpdateView(ApiModelUpdateMixin, RetrieveUpdateDestroyAPIView):
    """
    API endpoint that allows ParcelFile to be viewed, edited, or destroyed.
    """

    queryset = ParcelFile.objects.all()
    serializer_class = ParcelFileSerializer
    lookup_field = 'id'
