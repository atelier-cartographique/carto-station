from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from sitex.models.building_local import BuildingLocal
from sitex.serializers.building_local import BuildingLocalSerializer
from sitex.utils.api_model_write_mixin import ApiModelCreateMixin, ApiModelUpdateMixin


class BuildingLocalView(ApiModelCreateMixin, ListCreateAPIView):
    """
    API endpoint that allows Parcel File to be viewed or created.
    """

    queryset = BuildingLocal.objects.all()
    serializer_class = BuildingLocalSerializer


class BuildingLocalDetailView(ApiModelUpdateMixin, RetrieveUpdateDestroyAPIView):
    """
    API endpoint that allows Parcel File to be viewed, edited, or deleted.
    """

    queryset = BuildingLocal.objects.all()
    serializer_class = BuildingLocalSerializer
    lookup_field = 'id'
