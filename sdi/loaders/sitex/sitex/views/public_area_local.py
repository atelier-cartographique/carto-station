from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from sitex.models.public_area_local import PublicAreaLocal
from sitex.serializers.public_area_local import PublicAreaLocalSerializer
from sitex.utils.api_model_write_mixin import ApiModelCreateMixin, ApiModelUpdateMixin


class PublicAreaLocalAPIView(ApiModelCreateMixin, ListCreateAPIView):
    """
    API endpoint that allows Public area local to be viewed or edited.
    """

    queryset = PublicAreaLocal.objects.all()
    serializer_class = PublicAreaLocalSerializer


class PublicAreaLocalDetailAPIView(ApiModelUpdateMixin, RetrieveUpdateDestroyAPIView):
    """
    API endpoint that allows Public area local to be viewed or edited.
    """

    queryset = PublicAreaLocal.objects.all()
    serializer_class = PublicAreaLocalSerializer
    lookup_field = 'id'
