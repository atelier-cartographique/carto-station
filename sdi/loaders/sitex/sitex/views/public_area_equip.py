from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from sitex.models.public_area_equip import PublicAreaEquip
from sitex.serializers.public_area_equip import PublicAreaEquipSerializer
from sitex.utils.api_model_write_mixin import ApiModelCreateMixin, ApiModelUpdateMixin


class PublicAreaEquipAPIView(ApiModelCreateMixin, ListCreateAPIView):
    """
    API endpoint that allows Public area equip to be viewed or edited.
    """

    queryset = PublicAreaEquip.objects.all()
    serializer_class = PublicAreaEquipSerializer


class PublicAreaEquipDetailAPIView(ApiModelUpdateMixin, RetrieveUpdateDestroyAPIView):
    """
    API endpoint that allows Public area equip to be viewed or edited.
    """

    queryset = PublicAreaEquip.objects.all()
    serializer_class = PublicAreaEquipSerializer
    lookup_field = 'id'
