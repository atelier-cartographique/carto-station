# -*- coding: utf-8 -*-
import re

from django.db import connection

from api.permissions import ObjectPermissions
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.generics import (
    ListAPIView,
    ListCreateAPIView,
    RetrieveUpdateAPIView,
)
from rest_framework.response import Response

from sitex.models.building import Building
from sitex.models.building_group import BuildingGroup
from sitex.models.file import File
from sitex.models.block import Block
from sitex.serializers.building import (
    BuildingCreateSerializer,
    BuildingSerializer,
    BuildingWithDetailSerializer,
)
from sitex.serializers.building_group import BuildingGroupSerializer
from sitex.serializers.file import FileUploadReadSerializer
from sitex.utils.api_model_write_mixin import ApiModelCreateMixin, ApiModelUpdateMixin

import logging

_logger = logging.getLogger(__name__)


class BuildingListCreateApiView(ApiModelCreateMixin, ListCreateAPIView):
    """
    API endpoint that allows users to be viewed or edited.
    """

    serializer_class = BuildingSerializer
    permission_classes = (ObjectPermissions,)

    # Swagger config parameter
    id_config = openapi.Parameter(
        name="id",
        in_=openapi.IN_QUERY,
        type=openapi.TYPE_INTEGER,
    )
    idbuild_config = openapi.Parameter(
        name="idbuild",
        in_=openapi.IN_QUERY,
        type=openapi.TYPE_STRING,
    )
    u2block_config = openapi.Parameter(
        name="u2block",
        in_=openapi.IN_QUERY,
        type=openapi.TYPE_INTEGER,
    )
    history_config = openapi.Parameter(
        name="history",
        in_=openapi.IN_QUERY,
        description="Result include history data e.g: (Y/N)",
        type=openapi.TYPE_STRING,
    )
    details_config = openapi.Parameter(
        name="details",
        in_=openapi.IN_QUERY,
        description="Showing detail relations e.g: (Y/N)",
        type=openapi.TYPE_STRING,
    )
    in_bbox_config = openapi.Parameter(
        name="in_bbox",
        in_=openapi.IN_QUERY,
        description="Specify a bounding box as filter: in_bbox=min_lon,min_lat,max_lon,max_lat",
        type=openapi.TYPE_STRING,
    )
    intersects_geom_config = openapi.Parameter(
        name="intersects_geom",
        in_=openapi.IN_QUERY,
        description='Filter using intersect GeoJSON e.g: {"type": "String", "coordinates":[]}',
        type=openapi.TYPE_OBJECT,
    )
    blocks_gid_config = openapi.Parameter(
        name="blocks_gid",
        in_=openapi.IN_QUERY,
        description="Filter using one or multiple block gid separated by comma e.g: 33,34,35",
        type=openapi.TYPE_STRING,
    )

    def create(self, request, *args, **kwargs):
        # _logger.debug
        files = request.data.pop("files", None)
        if files:
            request.data["files"] = [{"file": f["file"]["id"]} for f in files]
        serializer_building = BuildingCreateSerializer(data=request.data)
        serializer_building.is_valid(raise_exception=True)
        self.perform_create(serializer_building)

        # handle relation to file
        files = serializer_building.data["properties"]["files"]
        if len(files) > 0:
            for i in range(len(files)):
                file_pk = serializer_building.data["properties"]["files"][i]["file"]
                file = File.objects.get(pk=file_pk)
                object_file = FileUploadReadSerializer(file)
                serializer_building.data["properties"]["files"][i]["file"] = (
                    object_file.data
                )

        headers = self.get_success_headers(serializer_building.data)
        return Response(
            serializer_building.data, status=status.HTTP_201_CREATED, headers=headers
        )

    def get_queryset(self):
        params = {}
        params["filter"] = {}
        params["filter"]["history"] = "N"
        for key, value in self.request.GET.items():
            if key == "blocks_gid":
                blocks_gid = []
                blocks_geom = []
                sanitize_value = re.sub(
                    "[^0-9,]", "", self.request.GET.get("blocks_gid")
                )
                for val in sanitize_value.split(","):
                    if val:
                        blocks_gid.append(int(val))
                blocks = Block.objects.filter(id__in=blocks_gid)
                if blocks:
                    for block in blocks:
                        blocks_geom.append(block.geom)
                params[key] = blocks_geom
            elif key in ("idbuild", "u2block", "history", "id"):
                params["filter"][key] = value
            else:
                params[key] = value

        if self.request.GET.get("id"):
            del params["filter"]["history"]

        raw_query = self.serializer_class().get_raw_query(**params)
        with connection.cursor() as cursor:
            cursor.execute(raw_query)
            row = cursor.fetchone()
        return row

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()[0]
        return Response(queryset)

    @swagger_auto_schema(
        manual_parameters=[
            id_config,
            idbuild_config,
            u2block_config,
            history_config,
            details_config,
            in_bbox_config,
            intersects_geom_config,
            blocks_gid_config,
        ]
    )
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class BuildingHistoryListApiView(ListAPIView):
    """
    Access history data of a building
    This endpoint requires `idbuild` type UUID
    """

    serializer_class = BuildingSerializer
    permission_classes = (ObjectPermissions,)

    def get_queryset(self):
        params = {
            "filter": {
                "history": "Y",
                "idbuild": self.kwargs.get("idbuild"),
            }
        }
        raw_query = self.serializer_class().get_json_raw_query(**params)
        with connection.cursor() as cursor:
            cursor.execute(raw_query)
            row = cursor.fetchone()
        return row

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()[0]
        return Response(queryset)


class BuildingDetailUpdateAPIView(ApiModelUpdateMixin, RetrieveUpdateAPIView):
    serializer_class = BuildingSerializer
    lookup_field = "id"

    # Swagger config parameter for bbox and contains_geom
    history_config = openapi.Parameter(
        name="history",
        in_=openapi.IN_QUERY,
        description="Result include history data e.g: (Y/N)",
        type=openapi.TYPE_STRING,
    )
    details_config = openapi.Parameter(
        name="details",
        in_=openapi.IN_QUERY,
        description="Showing detail relations e.g: (Y/N)",
        type=openapi.TYPE_STRING,
    )

    def get_queryset(self):
        args_query = {}
        if self.request.GET.get("history"):
            if self.request.GET.get("history") in ("Y", "y"):
                args_query["recordstate__in"] = ["A", "O", "X"]
            else:
                args_query["recordstate__in"] = ["O"]
        else:
            args_query["recordstate__in"] = ["O"]
        return Building.objects.filter(**args_query)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()

        if request.GET.get("details"):
            if request.GET.get("details") in ("Y", "y"):
                serializer = BuildingWithDetailSerializer(instance)
                return Response(serializer.data)

        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    @swagger_auto_schema(manual_parameters=[history_config, details_config])
    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class BuildingGroupInBlockSuggestionView(ListAPIView):
    """
    Suggestion of building groups in a block
    This endpoint requires `blockId` type integer
    """

    serializer_class = BuildingGroupSerializer
    pagination_class = None

    def get_queryset(self):
        block_id = self.kwargs.get("block_id")
        if block_id:
            try:
                urbis_block = Block.objects.get(pk=block_id)
            except Block.DoesNotExist:
                return BuildingGroup.objects.none()
            else:
                building = Building.objects.filter(
                    geom__contained=urbis_block.geom, recordstate="O"
                )
                building_group_ids = building.values_list(
                    "buildgroup", flat=True
                ).distinct()
                building_groups = BuildingGroup.objects.filter(
                    pk__in=building_group_ids
                )
                return building_groups

        return BuildingGroup.objects.none()
