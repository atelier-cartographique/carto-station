from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from sitex.models.building_file import BuildingFile
from sitex.serializers.building_file import BuildingFileSerializer
from sitex.utils.api_model_write_mixin import ApiModelCreateMixin, ApiModelUpdateMixin


class BuildingFileView(ApiModelCreateMixin, ListCreateAPIView):
    """
    API endpoint that allows users to be viewed or edited.
    """

    queryset = BuildingFile.objects.all()
    serializer_class = BuildingFileSerializer


class BuildingFileDetailView(ApiModelUpdateMixin, RetrieveUpdateDestroyAPIView):
    """
    API endpoint that allows users to be viewed or edited.
    """

    queryset = BuildingFile.objects.all()
    serializer_class = BuildingFileSerializer
    lookup_field = 'id'
