from rest_framework.generics import ListCreateAPIView

from sitex.models.building_group import BuildingGroup
from sitex.serializers.building_group import BuildingGroupSerializer


class BuildingGroupListCreateView(ListCreateAPIView):
    """
    API endpoint that allows users to be viewed or edited.
    """

    queryset = BuildingGroup.objects.all()
    serializer_class = BuildingGroupSerializer
    pagination_class = None
