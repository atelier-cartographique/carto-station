from drf_yasg import openapi
from rest_framework.viewsets import ReadOnlyModelViewSet
from django.http import JsonResponse

from sitex.models.block import Block
from sitex.serializers.block import BlockSerializers


class BlockListAPIView(ReadOnlyModelViewSet):
    serializer_class = BlockSerializers
    paginator_class = None
    queryset = Block.objects.all()

    # Swagger config parameter for bbox and contains_geom
    in_bbox_config = openapi.Parameter(
        name="in_bbox",
        in_=openapi.IN_QUERY,
        description="Specify a bounding box as filter: in_bbox=min_lon,min_lat,max_lon,max_lat",
        type=openapi.TYPE_STRING,
    )

    # def get_queryset(self):
    #     params = {}
    #     if self.request.GET.get('in_bbox'):
    #         params['in_bbox'] = self.request.GET.get('in_bbox')

    #     raw_query = self.serializer_class().get_raw_query(**params)
    #     connection = connections[CONNECTION_ALIAS]
    #     with connection.cursor() as cursor:
    #         cursor.execute(raw_query)
    #         row = cursor.fetchone()
    #     return row

    # def list(self, request, *args, **kwargs):
    #     queryset = self.get_queryset()[0]
    #     return Response(queryset)

    # @swagger_auto_schema(manual_parameters=[in_bbox_config])
    # def get(self, request, *args, **kwargs):
    #     return self.list(request, *args, **kwargs)


## here we're going nasty, but you know what, Im on holidays now, so sorry
from django.conf import settings

_default_db_connection = settings.DATABASES["default"]
settings.DATABASES["public"] = _default_db_connection


def get_block_list(request):
    from postgis_loader.serializer import get_geojson

    return JsonResponse(get_geojson("public", "blocks"))
