from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page

from rest_framework.generics import ListAPIView

from sitex.models.dictionary import Occupancy
from sitex.serializers.dictionary_occupancy import OccupancySerializer


class OccupancyAPIView(ListAPIView):
    """
    API endpoint that allows dictionary occupancy to be viewed.
    """

    serializer_class = OccupancySerializer
    pagination_class = None
    queryset = Occupancy.objects.filter(active=True)

    # @method_decorator(cache_page(60 * 60 * 2))
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)
