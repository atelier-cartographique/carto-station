from rest_framework.generics import ListAPIView

from sitex.models.dictionary import Topology
from sitex.serializers.dictionary_topology import TopologySerializer


class TopologyAPIView(ListAPIView):
    """
    API endpoint that allows dictionary topology to be viewed.
    """

    serializer_class = TopologySerializer
    pagination_class = None
    queryset = Topology.objects.filter(active=True)
