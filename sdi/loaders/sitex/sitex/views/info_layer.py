from django.http import JsonResponse
from django.conf import settings
from postgis_loader.models import get_layer
from postgis_loader.serializer import get_geojson

LAYER_INFO_LIST = getattr(settings, "SITEX_LAYER_INFO_LIST", [])


def info_layer_list(request):
    layers = []
    for info in LAYER_INFO_LIST:
        _Model, _geometry_field, geometry_field_type = get_layer(
            info["data"]["schema"], info["data"]["table"]
        )
        layers.append(
            dict(
                name=info["name"],
                style=info["style"],
                data=info["data"],
                geometryType=geometry_field_type[: -len("Field")],
            )
        )
    return JsonResponse(
        layers,
        safe=False,
    )


def info_layer_data(request, schema, table):
    # TODO: check permissions
    return JsonResponse(get_geojson(schema, table))
