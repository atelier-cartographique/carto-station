import base64

from django.conf import settings
from django.db.models import Q
from django.utils.translation import gettext_lazy as _

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView

from sitex.models.building_file import BuildingFile
from sitex.models.file import File
from sitex.models.parcel_file import ParcelFile
from sitex.models.public_area_file import PublicAreaFile
from sitex.serializers.file import (
    FileSerializer,
    FileUploadReadSerializer,
    FileUploadSerializer,
)


class FileUploadAPIView(APIView):
    """
    API endpoint that allows users to be upload file.
    """

    serializer_class = FileUploadSerializer
    parser_classes = [MultiPartParser, FormParser]

    file_config = openapi.Parameter(
        name='file',
        required=True,
        in_=openapi.IN_FORM,
        description='The file to upload',
        type=openapi.TYPE_FILE,
    )
    filelocation_config = openapi.Parameter(
        name='filelocation',
        required=False,
        in_=openapi.IN_QUERY,
        description='file location in system e.g: media_files/filename.ext',
        type=openapi.TYPE_STRING,
    )
    file_id_config = openapi.Parameter(
        name='file_id',
        required=False,
        in_=openapi.IN_QUERY,
        description='File ID',
        type=openapi.TYPE_INTEGER,
    )
    file_upload_response = openapi.Response('response description', FileUploadSerializer)
    file_response = openapi.Response('return base64 file encoded', FileSerializer)

    @swagger_auto_schema(
        operation_description='API endpoint that allows users to be upload file',
        manual_parameters=[file_config],
        responses={200: file_upload_response},
    )
    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data, context={'request': request})
        if serializer.is_valid():
            files = request.FILES.items()
            _key, file = next(files, (None, None))
            content_type = file.content_type if file else None
            res = serializer.save(creator=self.request.user, type=content_type)
            instance_serializer = FileUploadReadSerializer(res, context={'request': request})
            return Response(instance_serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(
        operation_description='API endpoint that allows users to get file.',
        manual_parameters=[filelocation_config, file_id_config],
    )
    def get(self, request, *args, **kwargs):
        if not request.GET.get('filelocation') and not request.GET.get('file_id'):
            return Response(
                {'message': 'At least of params filelocation or file ID is sent'},
                status=status.HTTP_400_BAD_REQUEST,
            )

        filelocation = request.GET.get('filelocation')
        file_id = request.GET.get('file_id')

        filter_params = Q()
        if filelocation:
            filter_params |= Q(file__contains=filelocation.strip('"').strip("'"))

        if file_id:
            filter_params |= Q(pk=file_id)

        query_set = File.objects.filter(filter_params).first()

        if query_set:
            read_serializer = FileUploadReadSerializer(query_set)
            return Response(read_serializer.data, status=status.HTTP_200_OK)

        return Response({'message': 'File not found'}, status=status.HTTP_404_NOT_FOUND)

    @swagger_auto_schema(
        operation_description='API endpoint that allows users to delete file.',
        manual_parameters=[filelocation_config],
    )
    def delete(self, request, format=None):
        if not request.GET.get('filelocation'):
            return Response({'message': 'Params filelocation not included'}, status=status.HTTP_400_BAD_REQUEST)
        filelocation = request.GET.get('filelocation')
        file = File.objects.filter(filelocation__contains=filelocation.strip('"').strip("'")).first()
        file.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class FileDetailAPIView(APIView):
    """
    API endpoint that allows users to get upload file by id.
    """

    serializer_class = FileUploadSerializer

    def get(self, request, id, *args, **kwargs):
        try:
            file = File.objects.get(pk=id)
            data = self.serializer_class(file).data
            file_path = f'{settings.BASE_DIR}{data["file"]}'
            file_64_encode = ''
            with open(file_path, 'rb') as file:
                file_64_encode = base64.encodebytes(file.read())
            return Response({'file': file_64_encode}, status=status.HTTP_200_OK)
        except File.DoesNotExist:
            return Response({'message': 'File not found'}, status=status.HTTP_404_NOT_FOUND)


class FileDetailByEntityAPIView(APIView):
    """
    API endpoint that allows users to get upload file by id.
    """

    serializer_class = FileUploadSerializer

    def get(self, request, entity, entity_id):
        switcher_entity = {
            'building': BuildingFile,
            'parcel': ParcelFile,
            'public_area': PublicAreaFile,
        }
        entity_model = switcher_entity.get(entity, None)

        if not entity_model:
            return Response({'detail': _("Entity doesn't exist")}, status=status.HTTP_404_NOT_FOUND)

        try:
            entity_file = entity_model.objects.get(pk=entity_id)
            if not entity_file.filelocation_id:
                return Response({'detail': _("Doesn't have file")}, status=status.HTTP_404_NOT_FOUND)
            try:
                file = File.objects.get(pk=entity_file.filelocation_id)
                data = self.serializer_class(file).data
                file_path = f'{settings.BASE_DIR}{data["file"]}'
                file_64_encode = ''
                with open(file_path, 'rb') as file:
                    file_64_encode = base64.encodebytes(file.read())
                return Response({'file': file_64_encode}, status=status.HTTP_200_OK)
            except File.DoesNotExist:
                return Response({'detail': _('File not found')}, status=status.HTTP_404_NOT_FOUND)
        except entity_model.DoesNotExist:
            return Response({'detail': _('Not found')}, status=status.HTTP_404_NOT_FOUND)
