from rest_framework.viewsets import ReadOnlyModelViewSet

from sitex.models.dictionary import Observation
from sitex.serializers.dictionary_observation import ObservationSerializer


class ObservationAPIView(ReadOnlyModelViewSet):
    serializer_class = ObservationSerializer
    pagination_class = None
    queryset = Observation.objects.filter(active=True)
