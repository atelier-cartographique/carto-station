from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from sitex.models.parcel_occupancy import ParcelOccupancy
from sitex.serializers.parcel_occupancy import ParcelOccupancySerializer
from sitex.utils.api_model_write_mixin import ApiModelCreateMixin, ApiModelUpdateMixin


class ParcelOccupancyView(ApiModelCreateMixin, ListCreateAPIView):
    """
    API endpoint that allows ParcelOccupancy to be viewed or created.
    """

    queryset = ParcelOccupancy.objects.all()
    serializer_class = ParcelOccupancySerializer


class ParcelOccupancyDetailView(ApiModelUpdateMixin, RetrieveUpdateDestroyAPIView):
    """
    API endpoint that allows ParcelOccupancy to be viewed, edited, or deleted.
    """

    queryset = ParcelOccupancy.objects.all()
    serializer_class = ParcelOccupancySerializer
    lookup_field = 'id'
