from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from sitex.models.public_area_file import PublicAreaFile
from sitex.serializers.public_area_file import PublicAreaFileSerializer
from sitex.utils.api_model_write_mixin import ApiModelCreateMixin, ApiModelUpdateMixin


class PublicAreaFileView(ApiModelCreateMixin, ListCreateAPIView):
    """
    API endpoint that allows users to be viewed or edited.
    """

    queryset = PublicAreaFile.objects.all()
    serializer_class = PublicAreaFileSerializer


class PublicAreaFileDetailView(ApiModelUpdateMixin, RetrieveUpdateDestroyAPIView):
    """
    API endpoint that allows users to be viewed or edited.
    """

    queryset = PublicAreaFile.objects.all()
    serializer_class = PublicAreaFileSerializer
    lookup_field = 'id'
