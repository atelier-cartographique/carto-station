from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from sitex.models.parcel_local import ParcelLocal
from sitex.serializers.parcel_local import ParcelLocalSerializer
from sitex.utils.api_model_write_mixin import ApiModelCreateMixin, ApiModelUpdateMixin


class ParcelLocalAPIView(ApiModelCreateMixin, ListCreateAPIView):
    """
    API endpoint that allows Parcel Local to be viewed or edited.
    """

    queryset = ParcelLocal.objects.all()
    serializer_class = ParcelLocalSerializer


class ParcelLocalDetailAPIView(ApiModelUpdateMixin, RetrieveUpdateDestroyAPIView):
    """
    API endpoint that allows Parcel Local to be viewed or edited.
    """

    queryset = ParcelLocal.objects.all()
    serializer_class = ParcelLocalSerializer
    lookup_field = 'id'
