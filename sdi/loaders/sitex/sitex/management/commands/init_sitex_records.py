from django.core.management.base import BaseCommand
from sitex.models.dictionary import Observation
from sitex.models.urbis_building import UrbisBuilding
from sitex.models.building import Building
from sitex.models.urbis_parcel import UrbisParcel
from sitex.models.parcel import Parcel
from sitex.utils.choices import _DRAFT, _ONLINE, _VALIDATED
from django.contrib.auth.models import User
from datetime import datetime

import logging

_logger = logging.getLogger(__name__)
FIRST_OBS = Observation.objects.first()
NOW = datetime.now()


def make_existing_building_set():
    return set([b.u2building for b in Building.objects.all()])


def make_building(urbis: UrbisBuilding):
    b = Building()
    b.recordstate = _ONLINE
    b.datastate = _DRAFT
    b.startdate = NOW
    b.geom = urbis.geom
    b.geostate = _VALIDATED
    b.u2building = urbis.id
    b.groundarea = urbis.area
    b.observation_status = FIRST_OBS
    b.nblevel = urbis.levelinfo_nblv
    b.nblevelrec = urbis.levelinfo_retrait
    b.levelinfo_diflvl = urbis.levelinfo_diflvl
    return b


def init_buildings(user: User):
    existing = make_existing_building_set()
    for urbis in UrbisBuilding.objects.all():
        if urbis.id not in existing:
            sitex = make_building(urbis)
            sitex.creator = user
            sitex.save()
            _logger.debug("created building", sitex.id)


def make_existing_parcel_set():
    return set([p.u2parcel for p in Parcel.objects.all()])


def make_parcel(urbis: UrbisParcel):
    p = Parcel()
    p.recordstate = _ONLINE
    p.datastate = _DRAFT
    p.startdate = NOW
    p.geom = urbis.geom
    p.geostate = _VALIDATED
    p.u2parcel = urbis.id
    p.groundarea = urbis.shape_area
    return p


def init_parcels(user: User):
    existing = make_existing_parcel_set()
    for urbis in UrbisParcel.objects.all():
        if urbis.id not in existing:
            sitex = make_parcel(urbis)
            sitex.creator = user
            sitex.save()
            _logger.debug("created parcel", sitex.id)


class Command(BaseCommand):
    """
    Init all sitex records
    """

    help = "Init all sitex records"

    def add_arguments(self, parser):
        parser.add_argument(
            "username",
            type=str,
            help="""
                A username
            """,
        )

    def handle(self, *args, **options):
        username = options.pop("username")
        user = User.objects.get(username=username)
        init_buildings(user)
        init_parcels(user)
