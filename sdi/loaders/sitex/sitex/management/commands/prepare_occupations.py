import re
from xlrd import open_workbook
from xlrd.sheet import Sheet
from collections import namedtuple
from django.db.models import RestrictedError
from django.core.management.base import BaseCommand
from sitex.models.dictionary import Occupancy, Keyword
import logging

_logger = logging.getLogger(__name__)

KEYWORD_SEP = re.compile(r"[,;]")

Row = namedtuple("Row", ["code", "name_fr", "name_nl", "keywords_fr", "keywords_nl"])


def dbg(name, x):
    # t = type(x)
    # _logger.debug(f"[dbg({name})]>> {t} >> {x}")
    return x


def get_keywords_base(s):
    if len(s.strip()) == 0:
        return []
    return list(map(str.lower, list(map(str.strip, KEYWORD_SEP.split(s)))))


def get_keywords(fr, nl):
    if len(fr) == len(nl):
        return list(zip(fr, nl))
    elif len(fr) == 0:
        return list(zip(list(map(lambda k: f"not translated: {k}", nl)), nl))
    else:
        return list(zip(fr, list(map(lambda k: f"not translated: {k}", fr))))


def get_name(code, maybe_name, lang):
    if len(maybe_name) > 0:
        return maybe_name
    return f"{code} ({lang})"


def get_value(sheet: Sheet, row, col):
    try:
        return sheet.cell_value(row, col)
    except IndexError:
        return ""


def get_row(sheet: Sheet, index):
    sheet.row(index)
    code = dbg("code", get_value(sheet, index, 0)[:11])
    name_fr = dbg("name_fr", get_name(code, get_value(sheet, index, 1), "fr"))
    keywords_fr = dbg("keywords_fr", get_keywords_base(get_value(sheet, index, 2)))
    name_nl = dbg("name_nl", get_name(code, get_value(sheet, index, 3), "nl"))
    keywords_nl = dbg("keywords_nl", get_keywords_base(get_value(sheet, index, 4)))

    return Row(
        code,
        name_fr,
        name_nl,
        keywords_fr,
        keywords_nl,
    )


def process_sheet(sheet: Sheet, existings, log_success, log_error):
    row_count = sheet.nrows
    for index in range(1, row_count):
        row = get_row(sheet, index)
        if row.code not in existings:
            try:
                occup = Occupancy.objects.create(
                    code=row.code, name_fr=row.name_fr, name_nl=row.name_nl
                )
                log_success(f"Created occupation <{row.code}; {row.name_fr}>")
            except Exception as ex:
                log_error(f"Failed to create <{row.code}; {row.name_fr}> due to: {ex}")
                continue
        else:
            occup = existings.pop(row.code)
            occup.name_fr = row.name_fr
            occup.name_nl = row.name_nl
            occup.save()
            log_success(f"Updated occupation {row.code}")

        for kfr, knl in get_keywords(row.keywords_fr, row.keywords_nl):
            Keyword.objects.create(code=occup, name_fr=kfr, name_nl=knl)
            log_success(f'Created keyword ["{kfr}", "{knl}"]')


def process_workbook(workbook, existings, log_success, log_error):
    """
    ID | Intitulé Fr | fr_key | Intitulé Nl | nl_key
    """

    process_sheet(workbook.sheet_by_index(0), existings, log_success, log_error)
    log_success("Processed codes: {}".format(Occupancy.objects.count()))
    # process_sheet(workbook.sheet_by_index(1), existings, log_success, log_error)
    # log_success("Processed parcel codes: {}".format(Keyword.objects.count()))


class Command(BaseCommand):
    """
    Import Occupancy and Keyword from a XLS file
    """

    help = "Import Occupancy and Keyword from a XLS file"

    def add_arguments(self, parser):
        parser.add_argument("file")

    def handle(self, *args, **options):
        file_path = options.pop("file")
        try:
            Keyword.objects.all().delete()
            existings = dict()
            for o in Occupancy.objects.all():
                try:
                    o.delete()
                except RestrictedError:
                    existings[o.code] = o
                    self.stdout.write(f"Could not delete {o.code}, will try update it")

            workbook = open_workbook(file_path)
            process_workbook(
                workbook,
                existings,
                lambda s: self.stdout.write(self.style.SUCCESS(s)),
                lambda e: self.stdout.write(self.style.ERROR(e)),
            )
            if len(existings) > 0:
                codes = "; ".join(existings.keys())
                self.stdout.write(self.style.ERROR(f"Failed to update {codes}"))

        except Exception as ex:
            self.stdout.write(self.style.ERROR(ex))
            raise ex
