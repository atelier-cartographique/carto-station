from django.core.management.base import BaseCommand
from sitex.models.urbis_building import UrbisBuilding
from sitex.models.building import Building
from sitex.models.urbis_parcel import UrbisParcel
from sitex.models.parcel import Parcel
from sitex.utils.choices import _DRAFT, _ONLINE, _VALIDATED
from django.contrib.auth.models import User
from datetime import datetime

import logging

_logger = logging.getLogger(__name__)


def find_urbis_building(b):
    u = UrbisBuilding.objects.filter(geom__distance_lt=(b.geom.centroid, 1))
    return u[0]


def find_urbis_parcel(b):
    u = UrbisParcel.objects.filter(geom__distance_lt=(b.geom.centroid, 2))
    return u[0]


def fix_buildings():
    start = datetime.now()
    qs = Building.objects.filter(u2building__isnull=False)
    count = qs.count()
    _logger.debug("About to update buildings:", count)
    updated_buildings = []
    for b in qs:
        try:
            b.u2building = find_urbis_building(b).id
            updated_buildings.append(b)
        except Exception:
            _logger.debug("Could not find urbis building for", b.id)
    Building.objects.bulk_update(updated_buildings, ["u2building"])
    t = datetime.now() - start
    _logger.debug(str(t), str(t / count))


def fix_parcels():
    start = datetime.now()
    qs = Parcel.objects.filter(u2parcel__isnull=False)
    count = qs.count()
    _logger.debug("About to update parcels:", count)
    updated_parcels = []
    for p in qs:
        try:
            p.u2parcel = find_urbis_parcel(p).id
            updated_parcels.append(p)
        except Exception:
            _logger.debug("Could not find urbis parcel for", p.id)
    Parcel.objects.bulk_update(updated_parcels, ["u2parcel"])
    t = datetime.now() - start
    _logger.debug(str(t), str(t / count))


class Command(BaseCommand):
    """
    Fix Urbis references
    """

    help = "Fix Urbis references"

    def handle(self, *args, **options):
        fix_buildings()
        fix_parcels()
