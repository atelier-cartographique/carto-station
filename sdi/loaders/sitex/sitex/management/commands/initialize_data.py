import os

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from sitex.management.initialize.excel_data import load_occupcode
from sitex.management.initialize.urbis_data import load_urbis_data

import logging

_logger = logging.getLogger(__name__)
DB_NAME = os.getenv("SQL_DATABASE") or "sitex_loader"
USER = os.getenv("SQL_USER") or "sitex"
PASSWORD = os.getenv("SQL_PASSWORD") or "sitex"
HOST = os.getenv("SQL_HOST") or "localhost"
PORT = os.getenv("SQL_PORT") or "5432"
BASE_DIR = getattr(settings, "BASE_DIR")
APP_ROOT = BASE_DIR.resolve().parent


def find_file(name, path):
    _logger.debug(f"Searching for {name} in {path}")
    file_path = os.path.join(path, name)
    if os.path.exists(file_path) and os.path.isfile(file_path):
        return file_path

    raise FileNotFoundError(f"File {name} not found in {path}")


class Command(BaseCommand):
    """Import Block, Building, Parcel, Adress From Urbis data in the form of an sql file
    Import Occupancy, Topology, Statecode in the form of XLS file

    Acceptable Mode:
    ['block', 'building', 'parcel', 'address', 'occupancy', 'topology', 'statecode', 'all']

    Acceptable File type each mode:
    ['block', 'building', 'parcel', 'address'] = SQL File for postgresql
    ['occupancy', 'topology', 'statecode'] = XLS File

    if you want choice all or multiple choices u need provide all file needed in this folder 'sitex/management/files'

    and for naming rule file is just add mode into prefix file name:
    example:
        - for mode block filename is block_{filename}.sql\n
        - for mode occupancy filename is occupancy_{filename}.xlsx\n
    """

    help = "Import data from Urbis or Excel File"

    def add_arguments(self, parser):
        parser.add_argument(
            "mode",
            nargs="*",
            type=str,
            default=["all"],
            help="""
                Choices to what initialize data to load, Acceptable choices:
                'block', 'building', 'parcel', 'address', 'occupancy', 'topology', 'statecode', 'all'
            """,
        )
        parser.add_argument(
            "-f",
            "--file",
            type=str,
            default="",
            help="File locations, will be ignore if using mode multiple choices and all",
        )

    def handle(self, *args, **options):
        accept_choices = [
            "block",
            "building",
            "parcel",
            "address",
            "occupancy",
            "topology",
            "statecode",
            "all",
        ]
        file_location = ""
        # validate choices first
        for values in options.get("mode"):
            if values not in accept_choices:
                raise CommandError("First validasi Not acceptable choices include")

        if "all" in options.get("mode"):
            accept_choices.remove("all")
            for values in accept_choices:
                self.load_data(values)
        else:
            if len(options.get("mode")) == 1:
                if "f" in options.keys() or "file" in options.keys():
                    file_location = options.get("f") or options.get("file")

            for values in options.get("mode"):
                self.load_data(values, file_location)

    def load_data(self, mode, file=""):
        default_file_location = os.path.join(
            *[APP_ROOT, "sitex", "management", "files"]
        )
        if not file:
            if mode == "block":
                file = find_file("urbis_block.sql", default_file_location)
            elif mode == "building":
                file = find_file("urbis_building.sql", default_file_location)
            elif mode == "parcel":
                file = find_file("urbis_parcel.sql", default_file_location)
            elif mode == "address":
                file = find_file("urbis_address.sql", default_file_location)
            elif mode in ["occupancy", "topology", "statecode"]:
                file = find_file("occupcode.xlsx", default_file_location)
            else:
                raise CommandError("Not acceptable choices include")

        if file:
            try:
                if mode in ["block", "building", "parcel", "address"]:
                    load_urbis_data(DB_NAME, HOST, PORT, USER, PASSWORD, file)
                    self.stdout.write(
                        self.style.SUCCESS(f"Load data from urbis {mode} done!")
                    )
                if mode in ["occupancy", "topology", "statecode"]:
                    load_occupcode(mode, file)
                    self.stdout.write(
                        self.style.SUCCESS(f"Load data from excel for {mode} done!")
                    )
            except Exception as e:
                self.stdout.write(self.style.ERROR(e))
        else:
            self.stdout.write(
                self.style.ERROR(
                    "File import not found on /sitex/management/files or argument -f/--file not provide"
                )
            )
