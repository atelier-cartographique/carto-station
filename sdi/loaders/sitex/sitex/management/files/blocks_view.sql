--
-- PostgreSQL database dump
--
-- Dumped from database version 11.13
-- Dumped by pg_dump version 11.13
SET
    statement_timeout = 0;

SET
    lock_timeout = 0;

SET
    idle_in_transaction_session_timeout = 0;

SET
    client_encoding = 'UTF8';

SET
    standard_conforming_strings = on;

SELECT
    pg_catalog.set_config('search_path', '', false);

SET
    check_function_bodies = false;

SET
    xmloption = content;

SET
    client_min_messages = warning;

SET
    row_security = off;

--
-- Name: blocks; Type: VIEW; Schema: public; Owner: sitex
--
DROP VIEW IF EXISTS public.blocks;

CREATE VIEW public.blocks AS
SELECT
    block_reporting.id,
    block_reporting.geom,
    block_reporting.urbis_source,
    block_reporting.prdd_source,
    block_reporting.manual_source,
    block_reporting.urbis_building_count,
    block_reporting.urbis_parcel_count,
    block_reporting.sitex_building_count,
    block_reporting.sitex_parcel_count,
    CASE
        WHEN (
            (block_reporting.urbis_building_count > 0)
            AND (block_reporting.sitex_building_count > 0)
        ) THEN round(
            (
                (
                    (block_reporting.sitex_building_count) :: numeric / (
                        (block_reporting.urbis_building_count) :: numeric * 1.0
                    )
                ) * (100) :: numeric
            ),
            2
        )
        ELSE (0) :: numeric
    END AS building_occupancy_percentage,
    CASE
        WHEN (
            (block_reporting.urbis_parcel_count > 0)
            AND (block_reporting.sitex_parcel_count > 0)
        ) THEN round(
            (
                (
                    (block_reporting.sitex_parcel_count) :: numeric / (
                        (block_reporting.urbis_parcel_count) :: numeric * 1.0
                    )
                ) * (100) :: numeric
            ),
            2
        )
        ELSE (0) :: numeric
    END AS parcel_occupancy_percentage
FROM
    (
        SELECT
            ubl.id,
            ubl.geom,
            ubl.urbis_source,
            ubl.prdd_source,
            ubl.manual_source,
            ubl.urbis_building_count,
            ubl.urbis_parcel_count,
            (
                SELECT
                    count(1) AS count
                FROM
                    public.sitex_building sbu
                WHERE
                    (
                        sbu.recordstate = 'O'
                        AND sbu.datastate != 'D'
                        AND public.st_intersects(ubl.geom, sbu.geom)
                    )
            ) AS sitex_building_count,
            (
                SELECT
                    count(1) AS count
                FROM
                    public.sitex_parcel spa
                WHERE
                    spa.recordstate = 'O'
                    AND spa.datastate != 'D'
                    AND public.st_intersects(ubl.geom, spa.geom)
            ) AS sitex_parcel_count
        FROM
            urbis.blocks ubl
    ) block_reporting;