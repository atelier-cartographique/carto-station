import sys

from xlrd import open_workbook

from sitex.models.dictionary import DictionaryTerm, Keyword, Occupancy


def parse_xls(tab, file_path):
    wb = open_workbook(file_path)
    sheets = wb.sheet_names()
    active_sheet = wb.sheet_by_name(sheets[tab])
    num_rows = active_sheet.nrows
    num_cols = active_sheet.ncols
    header = [active_sheet.cell_value(0, cell).lower() for cell in range(num_cols)]
    for row_idx in range(1, num_rows):
        row_cell = [active_sheet.cell_value(row_idx, col_idx) for col_idx in range(num_cols)]
        yield dict(zip(header, row_cell))


def load_occupcode(mode, file):
    if mode == 'occupancy':
        occupancy_datas = parse_xls(0, file)
        for occup in occupancy_datas:
            occup_code = occup.get('code')
            if occup_code and occup_code != 'NULL':
                occupancy, _ = Occupancy.objects.get_or_create(pk=occup_code)
                occupancy.name_fr = occup.get('french')
                occupancy.name_nl = occup.get('dutch')
                occupancy.save()

        keyword_datas = parse_xls(1, file)
        i = 1
        for key in keyword_datas:
            code = key.get('code')
            name_fr = key.get('french')
            name_nl = key.get('dutch')
            if code and code != 'NULL':
                occupancy = Occupancy.objects.filter(pk=code)
                if occupancy and (name_fr != 'NULL' and name_fr) and (name_nl != 'NULL' and name_nl):
                    keyword, _ = Keyword.objects.get_or_create(pk=i, code=occupancy[0])
                    keyword.name_fr = name_fr
                    keyword.name_nl = name_nl
                    keyword.save()
                    i += 1

        sys.stdout.write('Load Occupancy and keyword done!')

    if mode == 'statecode':
        statecode_datas = parse_xls(2, file)
        i = 1  # hardcoded
        for state in statecode_datas:
            statecode, _ = DictionaryTerm.objects.get_or_create(pk=i)
            statecode.name_fr = state.get('french')
            statecode.name_nl = state.get('dutch')
            statecode.category = 'statecode'
            statecode.save()
            i += 1

        sys.stdout.write('Load Statecode done!')

    if mode == 'topology':
        statecode_datas = parse_xls(2, file)
        topology_datas = parse_xls(3, file)
        i = len(list(statecode_datas)) + 1
        for topo in topology_datas:
            topology, _ = DictionaryTerm.objects.get_or_create(pk=i)
            topology.name_fr = topo.get('french')
            topology.name_nl = topo.get('dutch')
            topology.category = 'topology'
            topology.save()
            i += 1

        sys.stdout.write('Load Topology done!')
