import os
import shlex
import subprocess
import sys


def load_urbis_data(dbname, host, port, user, password, file):
    try:
        command = f'psql -U {user} -d {dbname} -h {host} -p {port} -f {file}'
        command = shlex.split(command)

        os.environ['PGPASSWORD'] = password

        sys.stdout.write(f'\n\t Load data from file {file} is processing...')
        process = subprocess.Popen(command, stdout=subprocess.PIPE)
        process.communicate()
        sys.stdout.write(f'\n\t *** Load data from file {file} is done! ***\n')
    except subprocess.CalledProcessError as e:
        raise e
