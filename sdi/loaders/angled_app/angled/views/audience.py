from rest_framework import viewsets
from angled.serializers.audience import AudienceSerializer
from angled.models.audience import Audience


class AudienceViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Views for Audiences
    """
    serializer_class = AudienceSerializer
    queryset = Audience.objects.all()
