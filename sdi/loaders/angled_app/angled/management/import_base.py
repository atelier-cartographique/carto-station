from enum import Enum
import sys
import traceback
import re
import logging
import locale
from pathlib import PosixPath
from csv import DictReader as CSVReader
from shapefile import Reader as SHPReader
from datetime import date
import sqlite3
import json

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.contrib.gis.geos import Point, Polygon, LineString

from angled.models import dyn
from angled.models import ref
from angled.models.source import Source
from angled.models.audience import Audience

from lingua.fields import simple_message

_logger = logging.getLogger(__name__)


class UnitError:
    def __init__(self, cell_name, error_message):
        self.cell_name = cell_name
        self.error_message = error_message

    def update(self, **kwargs):
        return self

    def __str__(self):
        return "<UnitError {}>".format(self.cell_name)


class UnitResult:
    def __init__(self, **kwargs):
        self.args = kwargs

    def update(self, **kwargs):
        self.args.update(**kwargs)
        return self

    def __str__(self):
        return "<UnitResult {}>".format(
            " ".join(["{}={}".format(k, v) for k, v in self.args.items()])
        )


class MultiResult:
    def __init__(self, results):
        if isinstance(results, UnitResult):
            self.results = [results]
        else:
            self.results = list(filter(lambda x: isinstance(x, UnitResult), results))

    def __str__(self):
        return "<MultiResult \n{}\n>".format(
            "\n".join(["\t{}".format(u) for u in self.results])
        )


class UnitSkip:
    def update(self, **kwargs):
        return self

    def __str__(self):
        return "<UnitSkip>"


NOT_DIGIT_RE = re.compile("\\D+")
NOT_DIGIT_DOT_RE = re.compile("[€\\s/,a-z]")


def only_digits(s):
    return NOT_DIGIT_RE.sub("", s)


def digits(s):
    return NOT_DIGIT_DOT_RE.sub("", s)


def parse_int(s):
    try:
        return int(only_digits(s))
    except:
        return 0


def parse_float(s):
    try:
        return float(digits(s))
    except Exception:
        return 0.0


DECIMAL_POINT = locale.localeconv()["decimal_point"]
THOUSANDS_SEP = locale.localeconv()["thousands_sep"]


def parse_money(s, dp=DECIMAL_POINT, ts=THOUSANDS_SEP):
    as_string = ""
    for l in s:
        if l in ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]:
            as_string = as_string + l
        elif l == dp:
            as_string = as_string + DECIMAL_POINT
        elif l == ts:
            pass
    if 0 == len(as_string):
        return 0.0
    else:
        return float(as_string)


def make_year(y0, y1=None):
    end_y = y0 if y1 is None else y1
    start = date(y0, 1, 1)
    end = date(end_y, 12, 31)
    return (start, end)


def get_cell_name(UNITS):
    """
    Crée une fonction qui prend un nom de unit, 'nom_unit' et
    retourne b pour ('nom_unit', b) dans UNIT
    """

    def inner(unit):
        for a, b in UNITS:
            if a == unit:
                return b
        raise Exception("Unit does not exist: {}".format(unit))

    return inner


def get_val(UNITS):
    """
    Crée une fonction ayant 2 params : row et field
        - cherche le 'cell_name' de field
        - et retourne la valeur de 'field' dans row
    """
    cell_name = get_cell_name(UNITS)

    def inner(row, field):
        val = row[cell_name(field)]
        if isinstance(val, str):
            val = val.strip()
        elif val is None:
            val = ""
        return val

    return inner


def get_label(UNITS):
    val = get_val(UNITS)

    def inner(row, field, key):
        v = val(row, field).strip()
        if 0 == len(v):
            return UnitSkip()

        # label_func = dyn.unit_uni_label(key)
        def label_func(x):
            return simple_message(key, x)

        kwargs = dict()
        kwargs[key] = label_func(v)
        _logger.debug("get_label", key, v, kwargs[key])
        return UnitResult(**kwargs)

    return inner


def get_text(UNITS):
    val = get_val(UNITS)

    def inner(row, field, key):
        v = val(row, field).strip()
        if 0 == len(v):
            return UnitSkip()

        label_func = dyn.unit_uni_text(key)
        kwargs = dict()
        kwargs[key] = label_func(v)
        return UnitResult(**kwargs)

    return inner


def parse_multiple_int(row, UNITS, columns, parser):
    val = get_val(UNITS)
    v = []
    for field in columns:
        current_val = val(row, field)
        if isinstance(current_val, int) == False:
            current_val = parse_int(current_val)
        if current_val == 0:
            continue
        if current_val == 99999 or current_val == -99999:
            continue
        v.append((field, current_val))
    return v


def map_multiple(UNITS):
    val = get_val(UNITS)

    def inner(row, columns, parser):
        return map(parser, [(field, val(row, field)) for field in columns])

    return inner


def get_contact(name, **kwargs):
    try:
        return ref.Contact.find_by_name(name)
    except ref.Contact.DoesNotExist:
        return ref.Contact.objects.create(name=name, **kwargs)


def get_nova(r):
    try:
        return ref.Nova.objects.get(ref__exact=r)
    except ref.Nova.DoesNotExist:
        return ref.Nova.objects.create(ref=r, data={})


def report(writer, cell_name, error_message, row_number, pid, **kwargs):
    writer.write(
        'Error in row {number} -> project({pid}), cell "{cell}":\n\t{message}\n\n'.format(
            number=row_number,
            pid=pid,
            cell=cell_name.replace("\n", " / "),
            message=error_message.format(**kwargs),
        )
    )


# def report_cell(cell_name):
#     def inner(error_message, row_number, **kwargs):
#         return report(cell_name, error_message, row_number, **kwargs)

#     return inner

# def report_error(cell_name, error_message):
#     def inner(row_number, **kwargs):
#         return report(cell_name, error_message, row_number, **kwargs)

#     return inner


def report_error(writer, ex, row_number, pid, **kwargs):
    report(writer, ex.cell_name, ex.error_message, row_number, pid, **kwargs)


class ReaderType(Enum):
    CSV = 1
    SHP = 2
    SQLITE = 3


def from_shape(shape, srid):
    gt = shape.shapeTypeName
    # _logger.debug('from_shape', gt, shape.points)
    if "POINT" == gt:
        x, y = shape.points[0]
        return Point(x, y, srid=srid)
    elif "POLYLINE" == gt:
        return LineString(shape.points, srid=srid)
    elif "POLYGON" == gt:
        coords = shape.points
        coords.append(coords[0])
        return Polygon(coords, [], srid=srid)
    else:
        raise ValueError('Unknown geometry type "{}"'.format(gt))


class ShapeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, (Point, LineString, Polygon)):
            return obj.json

        return json.JSONEncoder.default(self, obj)


def make_import_note(source, pid, row, imported):
    title = "# {} -> Project({})".format(source, pid)
    abstract_imported = "\n".join(map(lambda x: "  - {}".format(x), imported))
    # abstract_error = '\n'.join(map(lambda x: '```\n{}\n```'.format(x), errors))

    items = list(row.items())
    # import pudb; pudb.set_trace()
    cells = "\n".join(
        [
            "{}: {}".format(k.replace("\n", " / "), v)
            for k, v in items
            if len(str(v)) > 0
        ]
    )

    body = "\n\n".join(
        [
            title,
            "## imported",
            abstract_imported,
            # '## errors',
            # abstract_error,
            "## row",
            cells,
        ]
    )

    note_type = dyn.DomainReg.term("Type de note", "Import")

    return MultiResult(UnitResult(type=note_type, body=body))


class ImportCommand(BaseCommand):
    help = """Import Projects
    """

    output_transaction = True

    reader_type = ReaderType.CSV
    file_encoding = "utf8"
    srid = 31370

    sqlite_tablename = None
    source_id_field = None

    def add_arguments(self, parser):
        parser.add_argument(
            "-o", "--offset", type=int, help="start after offset rows", default=0
        )
        parser.add_argument("-l", "--limit", type=int, help="stop after limit rows")
        parser.add_argument(
            "-c",
            "--cache",
            type=str,
            help="directory path of domains and terms cache files",
        )
        parser.add_argument("csv_file")
        parser.add_argument("username")
        parser.add_argument("audience", type=int, help="specify default audience id")

    def handle(self, *args, **options):
        self.filename = options["csv_file"]
        csv_file_path = PosixPath(self.filename)
        self.user = User.objects.get(username=options["username"])
        self.audience = Audience.objects.get(pk=options["audience"])
        self.errors = csv_file_path.parent.joinpath(
            csv_file_path.stem + ".errors"
        ).open("w")

        offset = options["offset"]
        limit = options["limit"]
        processed = 0

        cache = options["cache"]
        if cache is not None:
            dyn.DomainReg.read_cache(cache)

        try:
            if self.reader_type is ReaderType.CSV:
                with csv_file_path.open(encoding=self.file_encoding) as f:
                    for i, row in enumerate(CSVReader(f)):
                        # index is row number, so we account for header row + starting at 1 rather than 0
                        self.index = i + 2
                        if self.index >= offset:
                            # logger.debug('Processing row {}'.format(i))
                            self._process_row(row)
                            processed += 1
                        if limit and processed >= limit:
                            break
            elif self.reader_type is ReaderType.SHP:
                with SHPReader(
                    csv_file_path.as_posix(), encoding=self.file_encoding
                ) as shp:
                    for i, rec in enumerate(shp.records()):
                        self.index = i
                        if self.index >= offset:
                            row = rec.as_dict()
                            row["__geometry__"] = from_shape(shp.shape(i), self.srid)
                            self._process_row(row)
                            processed += 1
                        if limit and processed >= limit:
                            break

            elif self.reader_type is ReaderType.SQLITE:
                assert self.sqlite_tablename is not None
                con = sqlite3.connect(
                    csv_file_path.as_posix(), detect_types=sqlite3.PARSE_COLNAMES
                )
                cur = con.cursor()
                rows = cur.execute(
                    'select * from "{}";'.format(self.sqlite_tablename)
                ).fetchall()
                names = [d[0] for d in cur.description]
                self.cursor = cur
                for i, rec in enumerate(rows):
                    self.index = i
                    if self.index >= offset:
                        row = dict(zip(names, rec))
                        self._process_row(row)
                        processed += 1
                    if limit and processed >= limit:
                        break
            else:
                raise ValueError("Unknown reader type")
        except Exception as ex:
            # traceback.print_exc(file=sys.stderr)
            _logger.debug(str(ex))

        if cache is not None:
            dyn.DomainReg.write_cache(cache)

        self.errors.close()

    def process_row(self, row, create_unit):
        raise NotImplementedError()

    def close_row(self, row):
        if len(self.imported_units):
            self.stdout.write(
                """Imported row {} as Project({}).
    units [{}]: {}
    errors: {}""".format(
                    self.style.SUCCESS(self.index),
                    self.style.SUCCESS(self.project.id),
                    len(self.imported_units),
                    ", ".join(self.imported_units),
                    self.error_count,
                )
            )

            source_id = self.index
            if self.source_id_field is not None:
                source_id = row[self.source_id_field]
            Source.objects.create(
                filename=self.filename,
                source_id=source_id,
                project_id=self.project.id,
                data=json.dumps(row, cls=ShapeEncoder),
            )

        else:
            self.stdout.write(
                """Imported row {} as Project({}). errors: {}""".format(
                    self.style.SUCCESS(self.index),
                    self.style.SUCCESS(self.project.id),
                    self.error_count,
                )
            )

    def get_project(self, row):
        return dyn.Project.objects.create()

    def _process_row(self, row):
        project = self.get_project(row)
        if project is None:
            return
        self.project = project
        self.tx = dyn.Transaction.objects.create(project=self.project, user=self.user)

        self.error_count = 0
        self.imported_units = []

        def process_result(unit, res, model):
            # self.stdout.write(format(res.args))
            # self.stdout.write('process_result({}): {}'.format(unit, str(res)))
            try:
                instance = model.objects.create(
                    user=self.user,
                    project=self.project,
                    transaction=self.tx,
                    **(res.args),
                )

                dyn.get_reading_model(unit).objects.create(
                    audience=self.audience,
                    unit=instance,
                )

                self.imported_units.append(unit)
                return instance
            except Exception as ex:
                self.error_count += 1

                import pudb

                pudb.set_trace()
                self.errors.write("Programming Error:\n\t{}\n".format(ex))
                self.errors.write("When processing: {}\n".format(unit))
                self.errors.write("With args: {}\n".format(res.args))
                raise ex

            return None

        self.multis = {}

        def process_multis():
            for unit, res in self.multis.items():
                model = dyn.get_unit_model(unit)
                list_model = dyn.get_multi_model(unit)
                list_index = dyn.ListIndex.objects.create(
                    unit=unit, transaction=self.tx
                )
                for r in res.results:
                    if not isinstance(r, UnitResult):
                        raise TypeError(
                            "MultiResult can hold only UnitResult"
                            "\n and you gave it {}".format(type(r))
                        )
                    instance = process_result(unit, r, model)
                    list_model.objects.create(index=list_index, unit=instance)

        def create_unit(unit, res):
            """
            A compléter
            unit -> le nom qui se trouve dans ui.py
            res soit UnitSkip, UnitError, UnitResult ou MultiResult
            """

            if isinstance(res, UnitSkip):
                self.stdout.write("Skipping {} for row {}".format(unit, self.index))
                return
            elif isinstance(res, UnitError):
                self.error_count += 1
                report_error(self.errors, res, self.index, project.id)
            elif isinstance(res, UnitResult):
                if dyn.is_multi(unit):
                    raise TypeError(
                        " {}\n ... Trying to create a unique instance of a Multi, bad :)".format(
                            unit
                        )
                    )
                model = dyn.get_unit_model(unit)
                process_result(unit, res, model)
            elif isinstance(res, MultiResult):
                if unit in self.multis:
                    self.multis[unit] = MultiResult(
                        self.multis[unit].results + res.results
                    )
                else:
                    self.multis[unit] = res
                # model = dyn.get_unit_model(unit)
                # list_model = dyn.get_multi_model(unit)
                # list_index = dyn.ListIndex.objects.create(
                #     unit=unit, transaction=self.tx)
                # for r in res.results:
                #     if not isinstance(r, UnitResult):
                #         raise TypeError('MultiResult can hold only UnitResult'
                #                         '\n and you gave it {}'.format(
                #                             type(r)))
                #     instance = process_result(unit, r, model)
                #     list_model.objects.create(index=list_index, unit=instance)

            else:
                raise TypeError(
                    "create_unit can only process UnitSkip, "
                    "UnitError, UnitResult or MultiResult"
                    "\n and you gave it {}".format(type(res))
                )

        try:
            self.process_row(row, create_unit)

            create_unit(
                "note",
                make_import_note(
                    "{}#{}".format(PosixPath(self.filename).name, self.index),
                    project.id,
                    row,
                    self.imported_units,
                ),
            )

            process_multis()
            self.close_row(row)

        except Exception as ex:
            self.stdout.write(self.style.ERROR("Failed to process row: {}".format(ex)))
            self.stdout.write("<{}>".format(row))
            raise ex


class ImportUtil(BaseCommand):
    help = """Base class to make a command that goes through all the rows
    """

    output_transaction = True

    def add_arguments(self, parser):
        parser.add_argument("csv_file")

    def handle(self, *args, **options):
        csv_file_path = Path(options["csv_file"])

        with open(csv_file_path.as_posix()) as f:
            for i, row in enumerate(CSVReader(f)):
                self.process_row(row, i)

        self.close()

    def process_row(self, row, index):
        raise NotImplementedError()

    def close(self):
        _logger.debug("EOC")
