import re
from datetime import date
from django.utils.text import slugify
from django.contrib.auth.models import User

from angled.models import ref
from angled.models import dyn

from angled.management.import_base import (
    ImportCommand,
    ReaderType,
    UnitError,
    UnitResult,
    UnitSkip,
    MultiResult,
    get_cell_name,
    get_val,
    parse_int,
    parse_money,
    get_contact,
)

_cells = [
    'Statut',
    'Projet',
    'Programme',
    'Zone',
    'Enjeux',
    'Id',
    'Porteur de projet',
    'Type',
    'Statut PAD',
]

UNITS = (
    ('type', 'TYPE'),
    ('mu_nat_cod', 'MU_NAT_COD'),
    ('capakey', 'CAPAKEY'),
    ('bu_id', 'BU_ID'),
    ('mu_name_fr', 'MU_NAME_FR'),
    ('adrn', 'ADRN'),
    ('mu_name_du', 'MU_NAME_DU'),
    ('pn_name_fr', 'PN_NAME_FR'),
    ('adptid', 'ADPTID'),
    ('pn_name_du', 'PN_NAME_DU'),
    ('x', 'X'),
    ('y', 'Y'),
    ('zone', 'ZONE'),
    ('comment', 'COMMENT'),
    ('pz_nat_cod', 'PZ_NAT_COD'),
    ('nom', 'NOM'),
    ('num_zone', 'NUM_ZONE'),
    ('num', 'NUM'),
    ('geom', '__geometry__'),
)

CELL_NAMES = [(cell, model) for model, cell in UNITS]

cell_name = get_cell_name(UNITS)
val = get_val(UNITS)


# def project_type(row):
#     v = val(row, 'type').lower()

#     res = []
#     for x in v.split('/'):
#         try:
#             res.append(
#                 UnitResult(component=dyn.DomainReg.term('Program', x.strip())))
#         except Exception:
#             return UnitError(cell_name('type'),
#                              'Could not create a term for "{}"'.format(x))

#     return MultiResult(res)

def description(row):
    v = val(row, 'type').lower()
    return UnitResult(body=dict(fr=v, nl=v, en=v))


def point(row):
    g = val(row, 'geom')
    return UnitResult(geom='SRID=31370;MULTIPOINT (({} {}))'.format(g.x, g.y))


def zone(row):
    v = val(row, 'zone')
    if len(v) == 0:
        return UnitSkip()

    refs = []
    for n in v.split('_'):
        try:
            return UnitResult(name=dyn.DomainReg.term('zone', n))
        except Exception:
            return UnitError(cell_name('zone'),
                             'Could not create a term for "{}"'.format(n))
    return UnitSkip()


def capakey(row):
    v = val(row, 'capakey')
    if len(v) == 0:
        return UnitSkip()

    return MultiResult(UnitResult(value=v))


def comment(row):
    v = val(row, 'comment')

    if len(v) == 0:
        return UnitSkip()
    try:
        t = dyn.DomainReg.term('note_type', 'comment')
    except Exception:
        return UnitError(
            cell_name('comment'),
            'Could not create a term for type of note: "{}"'.format(v))
    return MultiResult(UnitResult(body=v, type=t))


def name(row):
    v = val(row, 'nom')
    if len(v) == 0:
        return UnitSkip()

    return UnitResult(name=dict(fr=v, nl=v))


def address(row):
    town = val(row, 'mu_name_fr')
    street_fr = val(row, 'pn_name_fr')
    street_nl = val(row, 'pn_name_du')
    number = val(row, 'adrn')
    zipcode = val(row, 'pz_nat_cod')

    if (len(town) > 0 and len(street_fr) > 0 and len(street_nl) > 0
            and parse_int(zipcode) > 0):
        try:
            return UnitResult(
                town=dyn.DomainReg.term('Commune', town),
                street_name=dict(fr=street_fr, nl=street_nl),
                street_number=number,
                postal_code=zipcode,
            )
        except Exception as ex:
            return UnitError('address',
                             'could not build an address: {}'.format(ex))

    return UnitSkip()


emilie = User.objects.get(username='ehanson')
manager_type = ref.Term.objects.get(pk=309)  # Diagnostique


def manager(row):
    return MultiResult(UnitResult(context=manager_type, manager_user=emilie))


class Command(ImportCommand):

    reader_type = ReaderType.SHP
    file_encoding = 'iso-8859-15'
    ZONES = []

    source_id_field = 'NUM'

    def process_row(self, row, create_unit):
        create_unit('name', name(row))
        create_unit('point', point(row))
        create_unit('zone', zone(row))
        create_unit('capakey', capakey(row))
        create_unit('note', comment(row))
        create_unit('address_struct', address(row))
        create_unit('manager', manager(row))
        create_unit('description', description(row))
