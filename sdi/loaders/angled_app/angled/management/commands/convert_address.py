from json import dumps, loads
from django.core.management.base import BaseCommand
from angled.models import audience
from angled.models.dyn import get_unit_model, get_reading_model
from angled.models import ref
from angled.models import dyn
from angled.models.audience import Audience
from lookup.views import ServiceProxy
from django.contrib.auth.models import User
import re
from csv import DictWriter

Address = get_unit_model("address")
AddressStruct = get_unit_model("address_struct")
AddressStructReading = get_reading_model("address_struct")
Manager = get_unit_model("manager")
num_re = re.compile("\d+")

import logging

_logger = logging.getLogger(__name__)


fieldnames = [
    "project_id",
    "base",
    "urbis_street_name",
    "urbis_number",
    "urbis_post_code",
    "urbis_mun",
    "city",
    "street_name",
    "street_number",
]


cs_list = [
    ("Anderlecht", 356, 1070),
    ("Berchem-Sainte-Agathe", 357, 1082),
    ("Ville de Bruxelles", 358, 1000),
    ("Bruxelles", 358, 1000),
    ("Etterbeek", 359, 1040),
    ("Evere", 360, 1140),
    ("Forest", 361, 1190),
    ("Ganshoren", 362, 1083),
    ("Ixelles", 363, 1050),
    ("Jette", 364, 1090),
    ("Koekelberg", 365, 1081),
    ("Molenbeek-Saint-Jean", 366, 1080),
    ("Saint-Gilles", 367, 1060),
    ("Saint-Josse-ten-Noode", 368, 1210),
    ("Schaerbeek", 369, 1030),
    ("Uccle", 370, 1180),
    ("Watermael-Boitsfort", 371, 1170),
    ("Woluwe-Saint-Lambert", 372, 1200),
    ("Woluwe-Saint-Pierre", 373, 1150),
    ("Auderghem", 398, 1160),
]

cs_dict = dict(list(map(lambda t: (t[0], ref.Term.objects.get(pk=t[1])), cs_list)))

POSTCODE = dict(list(map(lambda t: (t[0], t[2]), cs_list)))

try:
    service = ServiceProxy(
        {"fr": "urbis", "nl": "urbis_nl"}, "", "", {"module": "lookup.urbis_full"}
    )

    service_reverse = ServiceProxy(
        {"fr": "urbis_rev", "nl": "urbis_rev_nl"},
        "",
        "",
        {"module": "lookup.urbis_reverse"},
    )
except Exception:
    _logger.debug("services unavailable, you can only load an existing conv file")


def interactive_city(address, city):
    print("\n~ {}".format(city))
    result = input("Ok [enter] Choice [c]> ")
    if len(result) == 0:
        return city

    if result == "c":
        choices = ["{} : {}".format(i + 1, t[0]) for i, t in enumerate(cs_list)]
        print("\n".join(choices))
        index = int(input("? ")) - 1
        return cs_list[index][0]


def interactive_number(address, number):
    print("\n~  {}".format(number))
    result = input("Ok [enter]> ")
    if len(result) == 0:
        return number

    return result


def interactive_street_name(address, street_name, number):
    print("\n~  {}".format(street_name))
    input_street_name = input("Ok [enter] Name[string] > ")
    base_name = street_name if len(input_street_name) == 0 else input_street_name

    input_lang = input("FR [f] NL [n]> ")
    base_lang = "fr" if input_lang.startswith("f") else "nl"
    rev_lang = "nl" if base_lang == "fr" else "fr"

    first_hit = service.lookup("{}, {}".format(base_name, number), base_lang)[
        "results"
    ][0]
    # urbis_street_name = first_hit['address']['street']['name']

    coords = [first_hit["point"]["x"], first_hit["point"]["y"]]
    # print('coords', coords)
    rev_hit = service_reverse.lookup(coords, rev_lang)["results"][0]
    urbis_rev_street_name = rev_hit["address"]["street"]["name"]

    # print('Urbis street_name in {}: {}'.format(base_lang, urbis_street_name))
    print("\n{}~ {}".format(rev_lang, urbis_rev_street_name))

    input_rev_street_name = input("Ok [enter] Name[string] > ")
    if len(input_rev_street_name) > 0:
        return {base_lang: base_name, rev_lang: input_rev_street_name}

    return {
        base_lang: base_name,
        rev_lang: urbis_rev_street_name,
    }


def row(a):
    # street = result['address']['street']
    base = a.address.fr.replace("\n", ", ")
    # urbis_street_name = street['name']
    # urbis_number = result['address']['number'] if 'number' in result['address'] else '-'
    # urbis_post_code = int(street['postCode'])
    # urbis_mun = street['municipality']
    address_parts = a.address.fr.split("\n")
    street_name_and_number = " ".join(address_parts[0:-1])
    city = address_parts[-1]
    street_parts = street_name_and_number.split(" ")
    if len(street_parts) > 1 and num_re.search(street_parts[-1]) is not None:
        street_name = " ".join(street_parts[0:-1])
        street_number = street_parts[-1]
    else:
        street_name = " ".join(street_parts)
        street_number = "-"

    result_row = dict(
        project_id=a.project.pk,
    )

    print(
        "\n\n*** {} ********************\n  {}\n*****************************\n".format(
            a.project.id, base
        )
    )
    ok = input("Ok [enter] NotOk [n]>")
    if len(ok) > 0:
        return None
    mun_result = interactive_city(a, city)
    number_result = interactive_number(a, street_number)
    street_result = interactive_street_name(a, street_name, number_result)

    result_row["street_number"] = number_result
    result_row["street_name"] = street_result
    result_row["municipality"] = mun_result

    return result_row
    # try:
    #     cs_dict[city]
    # except:
    #     print('Mun failed on city: {}'.format(city))
    # try:
    #     cs_dict[urbis_mun]
    # except:
    #     print('Mun failed on urbis_mun: {}'.format(urbis_mun))

    # print('Select a municipality:')
    # print('1 : {}'.format(city))
    # print('2 : {}'.format(urbis_mun))
    # input_mun = input('> ')
    # if input_mun == '1':
    #     result_row['municipality'] = cs_dict[city].id
    # if input_mun == '2':
    #     result_row['municipality'] = cs_dict[urbis_mun].id

    # return dict(
    #     project_id=a.project.pk,
    #     base=base,
    #     urbis_street_name=urbis_street_name,
    #     urbis_number=urbis_number,
    #     urbis_post_code=urbis_post_code,
    #     urbis_mun=urbis_mun,
    #     city=city,
    #     street_name=street_name,
    #     street_number=street_number,
    # )


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            "-o", "--offset", type=int, help="start after offset rows", default=0
        )

        parser.add_argument(
            "-i", "--input", type=str, help="An input JSON list file", default=None
        )

        parser.add_argument(
            "-u", "--user", type=int, help="A user ID (required if input)", default=None
        )

        parser.add_argument(
            "-a",
            "--audience",
            type=int,
            nargs="+",
            help="Audience ID (at least one is required if input)",
            default=None,
        )

    def with_input(self, input_path, user_id, audience_ids):
        user = User.objects.get(pk=user_id)
        audiences = [
            Audience.objects.get(pk=audience_id) for audience_id in audience_ids
        ]

        with open(input_path) as input_file:
            for line in input_file.readlines():
                data = loads(line)
                project_id = data["project_id"]
                street_number = data["street_number"]
                street_name = data["street_name"]
                municipality = data["municipality"]
                postcode = POSTCODE[municipality]

                print(
                    "+ {}\n{} --- {}  {}, {} {}".format(
                        project_id,
                        street_name["fr"],
                        street_name["nl"],
                        street_number,
                        postcode,
                        municipality,
                    )
                )

                project = dyn.Project.objects.get(pk=project_id)
                tx = dyn.Transaction.objects.create(project=project, user=user)
                instance = AddressStruct.objects.create(
                    user=user,
                    project=project,
                    transaction=tx,
                    street_name=street_name,
                    street_number=street_number,
                    postal_code=postcode,
                    town=cs_dict[municipality],
                )

                for audience in audiences:
                    AddressStructReading.objects.create(
                        audience=audience,
                        unit=instance,
                    )

    def handle(self, *args, **options):
        input_path = options["input"]
        if input_path is not None:
            return self.with_input(input_path, options["user"], options["audience"])
        offset = options["offset"]
        mon_proj_ecoles = ref.Term.objects.get(pk=336)
        queryset = Manager.objects.filter(context=mon_proj_ecoles).order_by("id")
        projects = [m.project for m in queryset[offset:]]

        # results = []
        addresses = []

        for project in projects:
            if project.info_unit_address_set.count() > 0:
                addresses.append(project.info_unit_address_set.first())

        erred = []
        processed = 0
        open_mode = "w" if offset == 0 else "a"
        with open("conv_addr.jsonl", open_mode) as sink:
            for address in addresses:
                try:
                    rec = row(address)
                    if rec is not None:
                        sink.write(dumps(rec))
                        sink.write("\n")
                        sink.flush()
                    else:
                        erred.append(address)
                except KeyboardInterrupt:
                    print(
                        "\n\nInterrupted after processing {} addresses\n".format(
                            processed
                        )
                    )
                    print(
                        "To start again from there, use the option `--offset {}`".format(
                            offset + processed
                        )
                    )
                    break
                except Exception:
                    print("\nFailed on pid {}\n".format(address.project_id))
                    erred.append(address)
                processed += 1

        if len(erred):
            print("Not processed:")
            for err in erred:
                print(
                    'pid: {}; aid: {}; address: "{}"'.format(
                        err.project.id, err.id, err.address.fr.replace("\n", ", ")
                    )
                )

            # try:
            #     first_hit = service.lookup(
            #         address.address.fr, 'fr')['results'][0]
            #     results.append((address, first_hit))
            # except Exception as e:
            #     print('Failed on {}: {} ({})'.format(
            #         address.project.pk, address.address.fr.replace('\n', ', '), e))

        # for result in results:
        #     row(*result)
        # with open('conv_addr.csv', 'w', newline='') as csvfile:
        #     writer = DictWriter(csvfile, fieldnames=fieldnames)
        #     writer.writeheader()
        #     for result in results:
        #         writer.writerow(row(*result))
