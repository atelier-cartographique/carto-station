from django.core.management.base import BaseCommand
from django.apps import apps

import logging

_logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def handle(self, *args, **options):
        config = apps.get_app_config("angled")
        for m in config.models:
            _logger.debug(m)
