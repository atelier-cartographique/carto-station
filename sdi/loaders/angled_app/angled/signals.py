from django.db.models.signals import post_save, post_delete
from django.db.models import Q

from angled.ui import UNITS_NAMES
from angled.models.subscription import (
    ProjectSubscription,
    ProjectNotification,
)

ACTION_CREATE = "create"
ACTION_DELETE = "delete"
ACTION_TAG = "tag"


def on_unit_save(unit_name):
    def on_unit_save_inner(sender, instance, created, **kwargs):
        if not created:
            return
        project_subs = ProjectSubscription.objects.filter(
            ~Q(user=instance.user), active=True, project=instance.project
        )
        for sub in project_subs:
            ProjectNotification.objects.create(
                subscription=sub,
                action=ACTION_CREATE,
                unit=unit_name,
                unit_id=instance.id,
            )

    return on_unit_save_inner


# def on_unit_delete(unit_name):
#     def on_unit_delete_inner(sender, instance, **kwargs):
#         project_subs = ProjectSubscription.objects.filter(
#             ~Q(user=instance.user), active=True, project=instance.project
#         )
#         for sub in project_subs:
#             ProjectNotification.objects.create(
#                 subscription=sub,
#                 action=ACTION_DELETE,
#                 unit=unit_name,
#                 unit_id=instance.id,
#             )

#     return on_unit_delete_inner


def on_tag_save(unit_name):
    def on_tag_save_inner(sender, instance, created, **kwargs):
        project_subs = ProjectSubscription.objects.filter(active=True)
        unit_instance = instance.unit
        for sub in project_subs:
            match_project = sub.project_id == unit_instance.project_id
            if match_project:
                ProjectNotification.objects.create(
                    subscription=sub,
                    action=ACTION_TAG,
                    unit=unit_name,
                    # that's ugly, but a way to get the tagged unit easily, yeah FIXME
                    unit_id=instance.id,
                )

    return on_tag_save_inner


# def on_transaction_save(sender, instance, created, **kwargs):
#     if not created:
#         return
#     for name in [u for u in UNITS_NAMES]:
#         field_name = f"info_unit_{name}_set"
#         query_set = getattr(instance, field_name)
#         if query_set.count() > 0:
#             Unit = get_unit_model(name)
#             current_units = Unit.objects.filter(
#                 ~Q(transaction=instance),
#                 project=instance.project,
#                 unit_status=BaseUnit.STATUS_CURRENT,
#             )
#             for unit in current_units:
#                 unit.eol = instance.created_at
#                 unit.unit_status = BaseUnit.STATUS_ARCHIVED
#                 unit.save(update_fields=["eol", "unit_status"])

#             query_set.all().update(unit_status=BaseUnit.STATUS_CURRENT)


def connect():
    for unit_name in UNITS_NAMES:
        model_name = "angled.info_unit_{}".format(unit_name)
        model_tag = "angled.Tagged_{}".format(unit_name)

        unit_save_receiver = on_unit_save(unit_name)
        # unit_delete_receiver = on_unit_delete(unit_name)
        tag_save_receiver = on_tag_save(unit_name)

        post_save.connect(
            unit_save_receiver,
            sender=model_name,
            weak=False,
            dispatch_uid="angled_signal_save_unit_{}".format(unit_name),
        )

        # post_delete.connect(
        #     unit_delete_receiver,
        #     sender=model_name,
        #     weak=False,
        #     dispatch_uid="angled_signal_delete_unit_{}".format(unit_name),
        # )

        post_save.connect(
            tag_save_receiver,
            sender=model_tag,
            weak=False,
            dispatch_uid="angled_signal_save_tag_{}".format(unit_name),
        )
