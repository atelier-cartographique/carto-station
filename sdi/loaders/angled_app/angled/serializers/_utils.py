from rest_framework import serializers
from datetime import (date, datetime, timedelta)
from psycopg2.extras import DateRange
import datetime


def year_to_range(year):
    start = date(year, 1, 1)
    end = date(year, 12, 31)
    dr = DateRange(start, end)
    return dr


def month_to_range(year, month):
    start = date(year, month, 1)
    if month == 12:
        next_year_start = date(year + 1, 1, 1)
    else:
        next_year_start = date(year, month + 1, 1)
    end = next_year_start - timedelta(days=1)
    dr = DateRange(start, end)
    return dr


def range_to_month_year_str(data, to_year=None):
    if to_year == None:
        year = delta.days == 364 or delta.days == 365
    else:
        year = to_year

    if year:
        return data.lower.year
    else:  # month
        return "{}-{:02d}".format(data.lower.year, data.lower.month)


class TimestampField(serializers.Field):
    """
    Serialize a python date to unix timestamp * 1000
    """

    def to_representation(self, value):
        try:
            return value.timestamp() * 1000
        except OverflowError as e:
            raise e
        except:
            raise serializers.ValidationError(
                "Impossible to translate date into timestamp")

    def to_internal_value(self, data):
        try:
            return datetime.fromtimestamp(data / 1000)
        except OverflowError as e:
            raise e
        except:
            raise serializers.ValidationError("Impossible to parse timestamp")


class MonthYearField(serializers.Field):
    """
    Serialize a python date to unix timestamp * 1000
    """

    def to_representation(self, value):
        # todo fixme pas utilisé pour le moment
        # voir angled/serializers/projects.py
        range_to_month_year_str(value)

    def to_internal_value(self, data):
        if type(data) == str and len(data) == 7 and data[
                4] == '-' and data[:4].isdigit() and data[5:7].isdigit():
            year = int(data[:4])
            month = int(data[5:7])
            return month_to_range(year, month)
        elif (type(data) == str and len(data) == 4
              and data.isdigit()) or (type(data) == int):
            year = int(data)
            return year_to_range(year)
        else:
            raise serializers.ValidationError(
                "Range must be in str (in YYYY or YYYY-MM format), got: {}".
                format(data))
