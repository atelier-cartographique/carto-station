from django.urls import path, include
from django.urls import reverse
from rest_framework import routers

from angled.ui import UNITS
from angled.views.project import (
    make_unit_viewset,
    make_tag_viewset,
    make_reading_viewset,
    # project_list,
    # project_single,
    # school_seats,
    ProjectViewSet,
    ProfileViewSet,
)
from angled.views.ref import (
    ContactViewSet,
    SiteViewSet,
    SchoolSiteViewSet,
    # NovaViewSet,
    FundingOrgViewSet,
    LocalityViewSet,
    TeamViewSet,
    DomainViewSet,
    DomainMappingViewSet,
    TermViewSet,
    TagViewSet,
)
from angled.views.audience import AudienceViewSet
from angled.views.query import (
    QueryViewSet,
    QueryShareViewSet,
    query_to_geojson,
    query_to_geojson_op,
    query_data_to_geojson,
)
from angled.views.subscription import (
    ProjectSubscriptionViewSet,
    CircleSubscriptionViewSet,
    ProjectNotificationViewSet,
    CircleNotificationViewSet,
)
from angled.views.nova import nova_proxy

import logging

_logger = logging.getLogger(__name__)
LAYER_ENDPOINT_NAME = "geodata.angled.query_layer"


def from_identifier(rid, request=None):
    """Transforms an internal "layer" identifier into an absolute URL

    It will be picked up by api.serializer.metadata.MetaDataSerializer
    """
    try:
        geometry_unit = rid.hostname
        parts = list(filter(lambda x: len(x) > 0, rid.path.split("/")))
        query_id = parts[0]
        if len(parts) == 1:
            rel_url = reverse(LAYER_ENDPOINT_NAME, args=[query_id, geometry_unit])
        else:
            rel_url = reverse(
                LAYER_ENDPOINT_NAME + "_op", args=[query_id, geometry_unit, parts[1]]
            )

        if request is not None:
            return request.build_absolute_uri(rel_url)
        return rel_url
    except Exception as ex:
        _logger.error("\n{}".format(ex))


unit_router = routers.SimpleRouter(trailing_slash=False)
reading_router = routers.SimpleRouter(trailing_slash=False)
tag_router = routers.SimpleRouter(trailing_slash=False)

for cat, units in UNITS.items():
    for unit in units:
        unit_name = unit["name"]
        unit_router.register(unit_name, make_unit_viewset(unit))
        tag_router.register(unit_name, make_tag_viewset(unit))
        reading_router.register(unit_name, make_reading_viewset(unit))

ref_router = routers.SimpleRouter(trailing_slash=False)

ref_router.register("domain", DomainViewSet)
ref_router.register("term", TermViewSet)
ref_router.register("domain_mapping", DomainMappingViewSet)
ref_router.register("audience", AudienceViewSet)

# do not use DefaultRouter outside the main API, it breaks things
pjt_router = routers.SimpleRouter()
pjt_router.register("project", ProjectViewSet)

profi_router = routers.SimpleRouter()
profi_router.register("profile", ProfileViewSet)

fk_router = routers.SimpleRouter()
fk_router.register("contact", ContactViewSet)
fk_router.register("site", SiteViewSet)
# fk_router.register('nova', NovaViewSet)
fk_router.register("funding_org", FundingOrgViewSet)
fk_router.register("locality", LocalityViewSet)
fk_router.register("team", TeamViewSet)
fk_router.register("tag", TagViewSet)
fk_router.register("school_site", SchoolSiteViewSet)

query_router = routers.SimpleRouter()
query_router.register("query", QueryViewSet)
query_router.register("share", QueryShareViewSet)

sub_router = routers.SimpleRouter()
sub_router.register("project-sub", ProjectSubscriptionViewSet)
sub_router.register("circle-sub", CircleSubscriptionViewSet)
sub_router.register("project-notif", ProjectNotificationViewSet)
sub_router.register("circle-notif", CircleNotificationViewSet)

urlpatterns = [
    path(
        "angled/l/<geometry_unit>",
        query_data_to_geojson,
        name=LAYER_ENDPOINT_NAME + "_data",
    ),
    path(
        "angled/l/<int:qid>/<geometry_unit>/<geo_op_name>",
        query_to_geojson_op,
        name=LAYER_ENDPOINT_NAME + "_op",
    ),
    path(
        "angled/l/<int:qid>/<geometry_unit>", query_to_geojson, name=LAYER_ENDPOINT_NAME
    ),
    path("angled/u/", include(unit_router.urls)),
    path("angled/t/", include(tag_router.urls)),
    path("angled/e/", include(reading_router.urls)),
    path("angled/r/", include(ref_router.urls)),
    path("angled/p/", include(pjt_router.urls)),
    path("angled/q/", include(query_router.urls)),
    path("angled/s/", include(sub_router.urls)),
    path("angled/ref/", include(fk_router.urls)),
    path("angled/profi/", include(profi_router.urls)),
    path("angled/nova/<key>/<val>", nova_proxy, name="angled-nova-proxy"),
]
