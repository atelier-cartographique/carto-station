from django.contrib.gis.db import models
from django.contrib.gis.db import models as gis_models
from django.conf import settings
from django.db.models import JSONField
from django.db.models import Q, Count
from urllib.request import urlopen
from urllib.parse import quote as parse_quote
from json import loads as json_loads

from lingua.fields import label_field, text_field, nullable_text_field, LinguaRecord


def label(message_type, **kwargs):
    return label_field(message_type, app_name="angled", **kwargs)


def text(message_type, **kwargs):
    return text_field(message_type, app_name="angled", **kwargs)


def nullable_text(message_type, **kwargs):
    return nullable_text_field(message_type, app_name="angled", **kwargs)


class Contact(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=256)
    info = models.TextField(default="")

    @classmethod
    def find_by_name(cls, name):
        return cls.objects.get(name=name)

    def __str__(self):
        return "{}".format(self.name)


class Kind(models.Model):
    id = models.AutoField(primary_key=True)

    name = label("KindName")

    def __str__(self):
        return "<Kind: {}>".format(self.name)


class Domain(models.Model):
    id = models.AutoField(primary_key=True)
    kind = models.ForeignKey(Kind, on_delete=models.CASCADE, blank=True, null=True)

    name = label("DomainName")
    description = nullable_text("DomainDescription")

    @classmethod
    def find_by_name(cls, name):
        # return cls.objects.get(
        #     Q(name__fr=name) | Q(name__nl=name) | Q(name__en=name))
        for dom in cls.objects.all():
            if isinstance(dom.name, LinguaRecord) and dom.name.contains(name):
                return dom

            elif isinstance(dom.name, dict):
                for k, v in dom.name.items():
                    if name == v:
                        return dom

        raise cls.DoesNotExist

    def __str__(self):
        return "{}".format(self.name)


class Term(models.Model):
    class Meta:
        ordering = ["sort_index"]

    id = models.AutoField(primary_key=True)
    domain = models.ForeignKey(Domain, on_delete=models.CASCADE)
    name = label("TermName")
    description = nullable_text("TermDescription")
    sort_index = models.IntegerField()

    @classmethod
    def find_by_name(cls, domain, name):
        # return cls.objects.get(
        #     Q(name__fr=name) | Q(name__nl=name) | Q(name__en=name))
        for t in cls.objects.filter(domain=domain):
            if isinstance(t.name, LinguaRecord) and t.name.contains(name):
                return t

            elif isinstance(t.name, dict):
                for k, v in t.name.items():
                    if name == v:
                        return t

        raise cls.DoesNotExist

    def __str__(self):
        return "{} ({})".format(self.name, self.domain.name)


class DomainFieldMapping(models.Model):
    id = models.AutoField(primary_key=True)
    domain = models.ForeignKey(Domain, on_delete=models.CASCADE)
    unit = models.CharField(max_length=64)
    field = models.CharField(max_length=64)


def make_term(name, **kwargs):
    return models.ForeignKey(
        Term, on_delete=models.CASCADE, related_name="of_{}".format(name), **kwargs
    )


class Site(models.Model):
    id = models.AutoField(primary_key=True)
    name = label("SiteName")


class Locality(models.Model):
    id = models.AutoField(primary_key=True)
    city = label("LocalityCity")
    postcode = models.CharField(max_length=32)
    ins = models.CharField(max_length=32)


class Nova(models.Model):
    """
    DEPRECATED

    'cause we're again in a rush, keeping it here to avoid a migration.
    but the thing is now angled.views.nova.nova_proxy
    """

    id = models.AutoField(primary_key=True)
    ref = models.CharField(max_length=32)
    time_of_fetch = models.DateTimeField(auto_now_add=True)
    data = JSONField(default=None, null=True, blank=True)

    # @classmethod
    # def get_or_create_uptodate(cls, nova_ref):
    #     """
    #     Get the Nova obj with ref=nova_ref that contains the
    #     up-to-date version of data

    #     If the the nova_ref is not valid, None is returned
    #     """
    #     novas_for_ref = cls.objects.filter(
    #         ref=nova_ref).order_by("-time_of_fetch")
    #     if novas_for_ref.count() >= 1:
    #         nova = novas_for_ref.first()
    #         json_nova_data = json_loads(nova.data)  # etrange / todo fixme

    #         # check if we have the last version of data
    #         last_data = nova.get_last_data()
    #         if last_data and last_data != json_nova_data:  # there is a new version
    #             ret_nova = Nova()
    #             ret_nova.ref = nova.ref
    #             ret_nova.data = last_data
    #             ret_nova.save()
    #         else:  # we already have the last version
    #             ret_nova = nova
    #     else:
    #         ret_nova = Nova()
    #         ret_nova.ref = nova_ref
    #         last_data = ret_nova.get_last_data()
    #         if not last_data:  # the value of nova is not good
    #             ret_nova = None
    #         else:
    #             ret_nova.data = last_data
    #             ret_nova.save()
    #     return ret_nova

    # def get_last_data(self):
    #     """
    #     Get from the WFS the last version of data.

    #     If all goes well, a dictionary is returned ottherwise None is returned
    #     """
    #     property_name = "novaseq"
    #     property_val = self.ref

    #     f_property_name = "<PropertyName>{}</PropertyName>".format(
    #         property_name)
    #     f_literal = "<Literal>{}</Literal>".format(property_val)
    #     f_prop_equals_lit = "<Filter><PropertyIsEqualTo>{}{}</PropertyIsEqualTo></Filter>".format(
    #         f_property_name, f_literal)

    #     url_base = settings.NOVA_URL_BASE
    #     params = settings.NOVA_URL_PARAMS
    #     params["FILTER"] = f_prop_equals_lit

    #     url_with_params = url_base
    #     for k, v in params.items():
    #         url_with_params = "{}&{}={}".format(url_with_params, k,
    #                                             parse_quote(v))

    #     with urlopen(url_with_params) as url:
    #         if url.getcode() == 200:
    #             data = json_loads(url.read().decode())

    #             if 'totalFeatures' in data and data['totalFeatures'] == 1:
    #                 return data['features'][0]['properties']
    #                 filtered_data = {}
    #                 for p in settings.NOVA_PROPERTIES:
    #                     filtered_data[p] = data['features'][0]['properties'][p]
    #                 return filtered_data
    #     return None  # not code 200 or no features


class SchoolSiteManager(gis_models.Manager):
    def get_queryset(self):
        return super().get_queryset().using("school_site")

    class Meta:
        ordering = ["-time_of_fetch"]


class SchoolSite(models.Model):
    class Meta:
        managed = False
        db_table = settings.SCHOOL_SITE_TABLE

    gid = models.AutoField(primary_key=True)
    name = models.TextField(db_column="nom_ecole")
    lang = models.CharField(db_column="langue", max_length=254)
    street = models.TextField(db_column="concat_adr")
    kind = models.CharField(db_column="ordinaire/", max_length=254)
    level = models.CharField(db_column="niveau", max_length=254)
    organizer = models.TextField(db_column="pouvoir or")
    reseau = models.CharField(db_column="reseau", max_length=254)
    general = models.IntegerField(db_column="general (s")
    technique = models.IntegerField(db_column="technique")
    technique_q = models.IntegerField(db_column="techniqu_1")
    profession = models.IntegerField(db_column="profession")
    artistique = models.IntegerField(db_column="artistique")
    artistique_q = models.IntegerField(db_column="artistiq_1")
    aso = models.IntegerField(db_column="aso (algem")
    tso = models.IntegerField(db_column="tso (techn")
    bso = models.IntegerField(db_column="bso (beroe")
    kso = models.IntegerField(db_column="kso (kunst")
    type1 = models.IntegerField(db_column="type 1 spe")
    type2 = models.IntegerField(db_column="type 2 spe")
    type3 = models.IntegerField(db_column="type 3 spe")
    type4 = models.IntegerField(db_column="type 4 spe")
    type5 = models.IntegerField(db_column="type 5 spe")
    type6 = models.IntegerField(db_column="type 6 spe")
    type7 = models.IntegerField(db_column="type 7 spe")
    type8 = models.IntegerField(db_column="type 8 spe")
    type9 = models.IntegerField(db_column="type 9 spe")
    form1 = models.IntegerField(db_column="forme 1 sp")
    form2 = models.IntegerField(db_column="forme 2 sp")
    form3 = models.IntegerField(db_column="forme 3 sp")
    form4 = models.IntegerField(db_column="forme 4 sp")
    clw = models.IntegerField(db_column="clw")
    cefa = models.IntegerField(db_column="cefa")
    cta = models.IntegerField(db_column="cta")
    rue = models.TextField(db_column="rue")
    locality = models.TextField(db_column="commune")
    postcode = models.TextField(db_column="code posta")
    point = gis_models.PointField(db_column="geom", srid=31370)

    objects = SchoolSiteManager()


class FundingOrg(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=256)

    @classmethod
    def find_by_name(cls, name):
        return cls.objects.get(name=name)

    def __str__(self):
        return "{}".format(self.name)


class Team(models.Model):
    id = models.AutoField(primary_key=True)

    def __str__(self):
        return ", ".join([str(m.member.name) for m in self.teammember_set.all()])


class TeamMember(models.Model):
    """Many to many table"""

    LEADER = "MAIN"
    REGULAR = "REGU"

    ROLE = (
        (LEADER, "Leader"),
        (REGULAR, "Regular"),
    )

    id = models.AutoField(primary_key=True)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    member = models.ForeignKey(Contact, on_delete=models.CASCADE)
    role = models.CharField(
        max_length=4,
        choices=ROLE,
        default=REGULAR,
    )


class Tag(models.Model):
    id = models.AutoField(primary_key=True)
    label = label("TagLabel")

    def __str__(self):
        return str(self.label)


# class Tagged(models.Model):
#     id = models.AutoField(primary_key=True)
#     tag = models.ForeignKey(Tag, on_delete=models.CASCADE)
#     unit_type = models.CharField(max_length=32)
#     unit_id = models.IntegerField()


class Document(models.Model):
    id = models.AutoField(primary_key=True)
    path = models.CharField(max_length=256)
    category = make_term("document_category")
