from django.db import models
from django.core.exceptions import ValidationError
from lingua.fields import label_field
from angled.profile.parser import (
    parse_profile,
    ProfileValidationError,
    IndentationError,
)


class JSONField(models.TextField):
    pass


class YAMLField(models.TextField):
    pass


def validate_profile(value):
    try:
        parse_profile(value, True)
    except ProfileValidationError as e:
        raise ValidationError("Error parsing profile: {}".format(e))
    except IndentationError as e:
        raise ValidationError("Indentation error: {}".format(e))
    except Exception as e:
        raise ValidationError("Unknown error: {}".format(e))


class ProfileTextField(models.TextField):
    default_validators = [validate_profile]
    description = "A profile description"


class Profile(models.Model):
    id = models.AutoField(primary_key=True)
    name = label_field("profile_name", default=dict)
    text = ProfileTextField("Profile Layout")

    def __str__(self):
        return "Profile #{}".format(self.name)

    # def clean(self):
    #     yaml = YAML(typ="safe")
    #     try:
    #         self.layout = json.dumps(yaml.load(self.layout_yaml))
    #     except ScannerError as e:
    #         raise ValidationError("Error reading Yaml")
