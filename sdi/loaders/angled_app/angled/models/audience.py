from collections import namedtuple
from django.contrib.auth.models import Group
from django.db import models
from django.db.models.expressions import RawSQL
from lingua.fields import label_field, text_field, lingua_string
from angled import ui
from angled.models import dyn


class Audience(models.Model):
    """An Audience stores informations related to an information unit target group

    Audiences are managed by site managers
    """

    id = models.AutoField(primary_key=True)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    name = label_field("AudienceName")
    description = text_field("AudienceDescription")

    def __str__(self):
        return lingua_string(self.name)


# Some functions need either audience id or group id
# depending of being concerned of display or permissions
ReadingTuple = namedtuple(
    "ReadingTuple",
    [
        "audience",
        "group",
        "id",
    ],
)


# def get_readings_old():
#     """Get Readings for all units

#     returns a dict of <unit_name><unit_id>[ReadingTuple]
#     """

#     qs = None
#     result = dict()
#     for unit_name in ui.UNITS_NAMES:
#         result[unit_name] = dict()
#         model = dyn.get_reading_model(unit_name)
#         annotation = RawSQL("'{}'".format(unit_name), ())
#         if qs is None:
#             qs = (
#                 model.objects.annotate(unit_name=annotation)
#                 .select_related("audience__group")
#                 .all()
#             )
#         else:
#             qs = qs.union(
#                 model.objects.annotate(unit_name=annotation)
#                 .select_related("audience__group")
#                 .all(),
#                 all=True,
#             )

#     for reading in qs:
#         if reading.unit_name not in result:
#             result[reading.unit_name] = dict()
#         elif reading.unit_id not in result[reading.unit_name]:
#             result[reading.unit_name][reading.unit_id] = []

#         result[reading.unit_name][reading.unit_id].append(
#             ReadingTuple(reading.audience_id, reading.audience.group_id, reading.id)
#         )

#     return result


Record = namedtuple(
    "Record",
    ["id", "unit_name", "unit_id", "audience_id", "audience__group_id"],
)


def _get_readings_queryset():
    qs = None
    for unit_name in ui.UNITS_NAMES:
        model = dyn.get_reading_model(unit_name)
        annotation = RawSQL("'{}'".format(unit_name), ())
        if qs is None:
            qs = (
                model.objects.annotate(unit_name=annotation)
                .select_related("audience__group")
                .all()
            )
        else:
            qs = qs.union(
                model.objects.annotate(unit_name=annotation)
                .select_related("audience__group")
                .all(),
                all=True,
            )
    return qs.values("id", "unit_name", "unit_id", "audience_id", "audience__group_id")


def get_readings():
    """Get Readings for all units

    returns a dict of <unit_name><unit_id>[ReadingTuple]
    """

    qs = _get_readings_queryset()
    result = dict()

    for reading in map(lambda r: Record(**r), qs):
        if reading.unit_name not in result:
            result[reading.unit_name] = dict()
        if reading.unit_id not in result[reading.unit_name]:
            result[reading.unit_name][reading.unit_id] = []

        result[reading.unit_name][reading.unit_id].append(
            ReadingTuple(reading.audience_id, reading.audience__group_id, reading.id)
        )

    return result
