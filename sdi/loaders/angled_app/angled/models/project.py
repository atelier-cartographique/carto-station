import json
from django.db import connection
from collections import namedtuple
from django.contrib.gis.db import models
from django.db.models.expressions import RawSQL

from lingua.fields import LinguaField
from angled import ui
from angled.models import (
    dyn,
    timeline,
)
from angled.serializers.transaction import serialize_transaction_qs

transaction_prefetch_names = [f"info_unit_{x}_set" for x in ui.UNITS_NAMES]


def get_all_tags():
    """Get all tags at once

    returns a dict of <unit_name><unit_id>[tag_id]
    """

    qs = None
    result = dict()
    for unit_name in ui.UNITS_NAMES:
        result[unit_name] = dict()
        model = dyn.get_tag_model(unit_name)
        annotation = RawSQL("'{}'".format(unit_name), ())
        if qs is None:
            qs = model.objects.annotate(unit_name=annotation).all()
        else:
            qs = qs.union(model.objects.annotate(unit_name=annotation).all(), all=True)

    for tagged in qs:
        if tagged.unit_name not in result:
            result[tagged.unit_name] = dict()
        elif tagged.unit_id not in result[tagged.unit_name]:
            result[tagged.unit_name][tagged.unit_id] = []

        result[tagged.unit_name][tagged.unit_id].append(
            dict(
                id=tagged.id,
                unit=tagged.unit_id,
                tag=tagged.tag_id,
            )
        )

    return result


def get_unit_tags(tags, unit_name, unit_id):
    if unit_name in tags and unit_id in tags[unit_name]:
        return tags[unit_name][unit_id]
    return []


def large_namedtuple(names):
    """This one is needed in lieu of a named tuple
    because of the 255 arguments limit prior to python 3.7
    see https://stackoverflow.com/questions/18550270/any-way-to-bypass-namedtuple-255-arguments-limitation
    """
    indices = {name: i for i, name in enumerate(names)}

    class NTuple:
        def __init__(self, row):
            self._row = row

        def __getattr__(self, attr):
            return self._row[indices[attr]]

    return NTuple


class FullLoaderManager(models.Manager):
    """
    Provide methods to load projects and UI with optimization:

    - performs one request for all 'single' units ;
    - performs one request per 'multi' units ;
    - gather all data, associate units and projects, and return data as dict
    """

    def get_all_as_dict(self, pids=[], names=ui.UNITS_NAMES):
        """
        Get all the projects with their units.

        The projects are returned as a list of dictionaries, and not instantiated
        as objects.
        """
        projects = []
        self.tags = get_all_tags()

        # add single units to projects
        with connection.cursor() as cursor:
            query = self._build_select_single(pids)
            cursor.execute(query)
            desc = cursor.description
            nt_result = large_namedtuple([col[0] for col in desc])
            for row in cursor.fetchall():
                result = nt_result(row)
                project = {"id": result.project_id}
                self._add_single_units_from_row(project, result, names)
                projects.append(project)

        # add multiple units to projects
        for unit_name in filter(dyn.is_multi, names):
            query = self._build_select_multi(unit_name, pids)
            with connection.cursor() as cursor:
                cursor.execute(query)
                desc = cursor.description
                nt_result = large_namedtuple([col[0] for col in desc])
                r = cursor.fetchone()
                if r is None:
                    continue
                model = dyn.get_unit_model(unit_name)
                row = nt_result(r)
                units = []
                # for performance reasons, we iterate over projects and
                # load data from cursor in the order they come. As the results are
                # ordered by project id, we continue to the next project when we
                # see the project id change in a cursor row
                for p in projects:
                    units = []
                    while getattr(row, "project_id") == p["id"]:
                        unit = dict()
                        for field in model._meta.fields:
                            property_name = field.name
                            _, db_name = field.get_attname_column()
                            value = getattr(row, db_name)
                            unit[property_name] = value

                        unit["tags"] = get_unit_tags(self.tags, unit_name, unit["id"])
                        units.append(unit)
                        r = cursor.fetchone()
                        if r is None:
                            break
                        row = nt_result(r)
                    if len(units) > 0:
                        p[unit_name] = units

        return projects

    def _add_single_units_from_row(self, project, nt, names=ui.UNITS_NAMES):
        """
        And single units to projects
        """
        for unit_name in [u for u in names if dyn.is_multi(u) is False]:
            if getattr(nt, "{}_id".format(unit_name)) is None:
                continue
            model = dyn.get_unit_model(unit_name)
            unit = dict()
            for field in model._meta.fields:
                property_name = field.name
                _, db_name = field.get_attname_column()
                s_name = "{}_{}".format(unit_name, db_name)
                value = getattr(nt, s_name)
                unit[property_name] = value

            unit["tags"] = get_unit_tags(self.tags, unit_name, unit["id"])
            project[unit_name] = unit

        return project

    def _get_projects(self):
        """
        return a list of all projects, ordered by id, with one single field: id
        """
        projects = []
        with connection.cursor() as cursor:
            cursor.execute("SELECT id FROM angled_project ORDER BY id")
            for row in cursor.fetchall():
                projects.append({id: row[0]})
        return projects

    def _build_select_multi(self, unit_name, pids=[]):
        """
        build a query to select a multi unit.

        The query contains an ORDER BY statement which orders results by project id
        """
        multi_model = dyn.get_multi_model(unit_name)
        model = dyn.get_unit_model(unit_name)
        order_by = " ORDER BY project_id "

        base_query = """
            WITH ordered AS (SELECT 
                l.id, rank() OVER (PARTITION BY transaction.project_id ORDER BY ts DESC) AS ranking 
                FROM angled_listindex AS l 
                JOIN angled_transaction AS transaction ON transaction.id = l.transaction_id  
                WHERE l.unit LIKE '{unit_name}' 
            ), 
            only_lasts AS (SELECT 
                id 
                FROM ordered 
                WHERE ranking = 1) 
            SELECT  
                * 
            FROM {unit_table}  
            WHERE id IN ( 
                SELECT unit_id FROM {multi_table} WHERE index_id IN (SELECT id FROM only_lasts) 
            )
        """.format(
            multi_table=multi_model._meta.db_table,
            unit_name=unit_name,
            unit_table=model._meta.db_table,
        )

        if len(pids) == 0:
            return base_query + order_by

        return (
            base_query
            + " AND project_id IN ({})".format(",".join(map(str, pids)))
            + order_by
        )

    def _build_select_single(self, pids=[]):
        """
        Return a single query to get all the single units in a single query.

        In this query, all units related to one project are gathered on a single row.

        The first row of this query **must** be the project id

        The query contains an ORDER BY statement which orders results by project id
        """
        joins = []
        selects = []
        order_by = " ORDER BY project.id "
        base_from = """ 
        LEFT JOIN (
            SELECT * FROM (
                SELECT rank() OVER (
                    PARTITION by project_id ORDER BY created_at DESC
                ) AS ranked, * FROM {table_name} 
            ) AS sb_{unit_name} 
		    WHERE ranked = 1 
        ) AS {unit_name} ON project.id = {unit_name}.project_id
        """

        for unit_name in [u for u in ui.UNITS_NAMES if dyn.is_multi(u) is False]:
            model = dyn.get_unit_model(unit_name)
            joins.append(
                base_from.format(table_name=model._meta.db_table, unit_name=unit_name)
            )
            for field in model._meta.fields:
                _, db_name = field.get_attname_column()
                # if property_name not in base_fields:
                #    continue
                selects.append(
                    "{unit_name}.{field} AS {unit_name}_{field}".format(
                        unit_name=unit_name, field=db_name
                    )
                )

        base_query = """
        SELECT project.id AS project_id, {} 
        FROM angled_project AS project {} 
        """.format(
            ", ".join(selects),
            " ".join(joins),
        )

        if len(pids) == 0:
            return base_query + order_by

        return (
            base_query
            + " WHERE project.id IN ({})".format(",".join(map(str, pids)))
            + order_by
        )


class TimelineManager(models.Manager):
    """
    Provide methods to handle timeline of associated units
    """

    def get_transactions(self, project):

        queryset = dyn.Transaction.objects.filter(project=project).prefetch_related(
            *transaction_prefetch_names
        )
        return serialize_transaction_qs(queryset)

    def ui_timeline(self, project, unit_name, max=50):
        unit = dyn.find_unit(unit_name)
        if unit["multi"]:
            # initiliaze events
            events = []
            # get all the indexes for the current project and unit_name
            indexes = dyn.ListIndex.objects.filter(
                transaction__project=project.id, unit=unit_name
            ).order_by("-ts")
            # pour des raisons des performances, on récupère tous les multis
            # liés aux index en une seule requete
            multis = dyn.get_multi_model(unit_name).objects.filter(index__in=indexes)
            multi_unit_ids = [m.unit_id for m in multis]
            # pour des raisons de performance, on récupère toutes les units
            # liées aux multis in one single query
            units = dyn.get_unit_model(unit_name).objects.filter(id__in=multi_unit_ids)
            for index in indexes:
                units_id = [m.unit_id for m in multis if m.index_id == index.id]
                units_for_index = [u for u in units if u.id in units_id]

                event = timeline.EventTimeline(
                    index.id, unit_name, index.ts, uis=units_for_index
                )
                events.append(event)

            return events
        else:
            model = dyn.get_unit_model(unit_name)
            return model.objects.filter(project__id=project.id).order_by("-created_at")[
                0:max
            ]
