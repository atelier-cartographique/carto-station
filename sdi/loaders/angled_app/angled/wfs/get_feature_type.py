from angled.models.query import Query
from angled.query import (
    deser_selects,
    field_type,
    get_select_info,
)
from angled.serializers.query import QuerySerializer
from basic_wfs.xml import (
    append,
    create,
    create_tree,
    rid_to_typename_ns,
    strip_ns,
    tree_to_string,
)
from django.shortcuts import get_object_or_404
from django.utils.text import slugify

MAP_TYPES = {
    "fk": "xsd:string",
    "term": "xsd:string",
    "label": "xsd:string",
    "text": "xsd:string",
    "raw_text": "xsd:string",
    "number": "xsd:float",
    "decimal": "xsd:decimal",
    "month": "xsd:nonNegativeInteger",
    "year": "xsd:nonNegativeInteger",
    "date": "xsd:date",
    "boolean": "xsd:boolean",
    "varchar": "xsd:string",
}


def make_field_base_type(t):
    return MAP_TYPES[t]


def describe_field(parent, select, unit, is_multi):
    # _logger.debug('describe_field', select.name, select.aggregate)
    if is_multi and select.aggregate.name == "concat":
        element_name = slugify(getattr(select, "name", "no-name"))
        list_name = "{}-list".format(element_name)
        multi_root = append(
            parent,
            "xsd:element",
            {
                "name": list_name,
                "maxOccurs": "1",
                "minOccurs": "0",
            },
        )
        complex_type = append(multi_root, "xsd:complexType")
        sequence = append(
            complex_type,
            "xsd:sequence",
            {
                "maxOccurs": "unbounded",
                "minOccurs": "0",
            },
        )
        append(
            sequence,
            "xsd:element",
            {
                "name": element_name,
                "type": make_field_base_type(field_type(select)),
            },
        )
        # append(
        #     parent, 'xsd:element', {
        #         'name': slugify(getattr(select, 'name', 'no-name')),
        #         'type': make_field_base_type(field_type(select)),
        #         'maxOccurs': 'unbounded',
        #         'minOccurs': '0',
        #     })
    else:
        append(
            parent,
            "xsd:element",
            {
                "name": slugify(getattr(select, "name", "no-name")),
                "type": make_field_base_type(field_type(select)),
                "maxOccurs": "1",
                "minOccurs": "0",
            },
        )


def describe_feature_type(typename, selects, lang):
    typename_name = strip_ns(typename)
    typename_type = "{}Type".format(typename_name)
    attributes = {
        # TODO:dead url
        "targetNamespace": "http://www.carto-station.com/angled",
        "xmlns:angled": "http://www.carto-station.com/angled",
        "xmlns:xsd": "http://www.w3.org/2001/XMLSchema",
        "xmlns:gml": "http://www.opengis.net/gml/3.2",
        "elementFormDefault": "qualified",
    }
    schema = create("xsd:schema", attributes)

    # gml
    append(
        schema,
        "xsd:import",
        {
            "namespace": "http://www.opengis.net/gml/3.2",
            "schemaLocation": "http://schemas.opengis.net/gml/3.2.1/gml.xsd",
        },
    )

    ct = append(
        schema,
        "xsd:complexType",
        {
            "name": typename_type,
        },
    )
    cc = append(ct, "xsd:complexContent")
    aft = append(cc, "xsd:extension", {"base": "gml:AbstractFeatureType"})
    seq = append(aft, "xsd:sequence")
    append(seq, "xsd:element", {"name": "geom", "type": "gml:GeometryPropertyType"})

    append(
        seq,
        "xsd:element",
        {
            "name": "id",
            "type": "xsd:integer",
            "maxOccurs": "1",
            "minOccurs": "1",
        },
    )

    for select, unit, is_multi, fields in get_select_info(selects):
        try:
            describe_field(seq, select, unit, is_multi)
        except Exception:
            # TODO:Errors should never pass silently.
            pass

    append(
        schema,
        "xsd:element",
        {
            "name": typename_name,
            "type": typename_type,
            "substitutionGroup": "gml:_Feature",
        },
    )

    return tree_to_string(create_tree(schema))


def get_feature_type(_request, lang, rid_url):
    qid = rid_url.path[1:]
    typename = rid_to_typename_ns(rid_url.geturl())
    query = get_object_or_404(Query, id=qid)
    query_data = QuerySerializer(query).data
    selects = deser_selects(query_data.get("statements", {}).get("select", []))

    return describe_feature_type(typename, selects, lang)
