from django import template

register = template.Library()


@register.filter
def pname(p):
    return ', '.join([str(n.name) for n in p.info_unit_name_set.all()])