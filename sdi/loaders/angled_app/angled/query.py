from collections import namedtuple
from functools import reduce, partial

from django.contrib.auth.models import User
from django.contrib.gis.gdal import OGRGeometry
from django.db.models import Q
from angled.models import dyn
from angled.models.ref import Team, TeamMember, FundingOrg, Contact
from json import dumps, loads
from angled.ui import get_field_type, get_unit_fields, find_field
from angled.models.query import Query
from angled.serializers.projects import (
    ProjectSerializer,
)
from angled.serializers._utils import year_to_range, month_to_range
from datetime import datetime

import logging

_logger = logging.getLogger(__name__)


def taggedtuple(cls):
    """
    Extends a class with
        - helping class methods : from_object, to_object
        - class property : tag

    Works well with namedtuple
    """
    name = cls.__name__

    def from_object(obj):
        try:
            args = [obj[k] for k in cls._fields]
            return cls(*args)
        except Exception as ex:
            _logger.error(str(ex))
            _logger.error(str(obj))

    def to_object(self):
        base = cls._asdict(self)
        base["tag"] = self.tag
        return base

    cls.tag = property(lambda _: name)
    cls.to_object = to_object
    cls.from_object = from_object

    return cls


def typ(name, *args):
    """
    Create a elem on the request.

    args -- is a the elem list of fields
    """
    return taggedtuple(namedtuple(name, args))


Conj = typ("conj", "conj")
Filter = typ("filter", "field", "op", "value", "negate")
Statement = typ("statement", "unit", "filters")
Aggregate = typ("aggregate", "name", "filter")
Select = typ("select", "name", "unit", "field", "aggregate", "filters")


def deser_conj(obj):
    """
    Deserialize a conjuction
    """
    return Conj.from_object(obj)


def deser_filter(obj):
    """
    Deserialize a filter
    """
    return Filter.from_object(obj)


def deser_statement(obj):
    """
    Deserialize a statement
    """
    unit = obj.get("unit")
    filters = [deser_filter(f) for f in obj.get("filters")]
    return Statement(unit, filters)


def join_field(f):
    """
    Create a field lookup from the a list :
        if f is a list : [a, b, c] -> "{}__{}__{}".format(a, b, c)

    (see https://docs.djangoproject.com/en/2.1/topics/db/queries/#field-lookups-intro)
    """
    if isinstance(f, list):
        return "__".join(f)
    return f


def parse_value(unit, f):
    """Here comes the ugly :/"""
    field_type = get_field_type(unit, f.field[0])

    if field_type == "geometry":
        return OGRGeometry(dumps(f.value), srs=31370).ewkt
    if field_type == "year":
        return year_to_range(f.value)
    if field_type == "month":
        year, month = list(map(int, f.value.split("-")))
        return month_to_range(year, month)
    if field_type == "date":
        # return datetime.fromtimestamp(f.value / 1000).date()
        return datetime.strptime(f.value, "%Y-%m-%d")
    return f.value


def parse_filter(unit, f):
    """
    Parse an object of class Filter
    """
    kwargs = dict()
    field = join_field(f.field)
    if f.op is None:  # in this case we use egals
        key = "info_unit_{}__{}".format(unit, field)

    else:
        key = "info_unit_{}__{}__{}".format(unit, field, f.op)

    kwargs[key] = parse_value(unit, f)

    if f.negate is True:
        return ~Q(**kwargs)

    return Q(**kwargs)


cond_current = lambda uname: Q(
    **{f"info_unit_{uname}__unit_status": dyn.BaseUnit.STATUS_CURRENT}
)


def parse_statement(unit, filters):
    """
    Parse an object of class Statement
    """
    return reduce(
        lambda acc, q: acc & q, [parse_filter(unit, f) for f in filters]
    ) & cond_current(unit)


def parse_conj(element):
    """
    Parse an object of class Conj
    """
    if "or" == element.conj.lower():
        return Q.OR
    else:
        return Q.AND


def is_duplicate(uis, ui_name):
    for name in uis:
        if name == ui_name:
            return True
    return False


def parse_input(input):
    """
    Parse the elements received as input.

    These elements must have been deserialized before.
    """
    # units = []
    combined = None
    conj = None
    for node in input:
        tag = node.tag
        if "statement" == tag:
            # if is_duplicate(units, name) is False:
            #     units.append(element.ui)
            if combined is None:
                combined = parse_statement(node.unit, node.filters)
            else:
                if conj is None:
                    combined = combined & parse_statement(node.unit, node.filters)
                else:
                    combined = combined._combine(
                        parse_statement(node.unit, node.filters), conj
                    )
                    conj = None

        elif "conj" == tag:
            conj = parse_conj(node)

        else:
            raise Exception('Unknown node tag "{}"'.format(node.tag))

    return combined


def deser_statements(input):
    """
    Transform "plain" objects into specialized classes
    """
    result = []
    for node in input:
        tag = node["tag"]
        if "statement" == tag:
            result.append(deser_statement(node))
        elif "conj" == tag:
            result.append(deser_conj(node))
        else:
            raise Exception('Unknown Tag "{}"'.format(tag))

    return tuple(result)


FILTER_OPS = {
    "equals": lambda ref, value: ref == value,
    "gt": lambda ref, value: value > ref,
    "lt": lambda ref, value: value < ref,
}


def make_filter(op):
    f = FILTER_OPS[op]

    def inner(ref, value):
        if ref is None or value is None:
            return False
        return f(ref, value)

    return inner


def identity(x, _):
    return x


def concat_final(x, _):
    if isinstance(x, list):
        return sorted(x)
    return sorted([x])


# Operatior for aggregation
# accumulator + value at the end
#
AGGREGATE_OPS = {
    "sum": (
        lambda acc, value: acc + value,
        identity,
        0,
    ),
    "mean": (
        lambda acc, value: acc + value,
        lambda x, units: x / len(units),
        0,
    ),
    "concat": (
        lambda acc, value: (acc if isinstance(acc, list) else [acc]) + [value],
        concat_final,
        [],
    ),
    "max": (
        lambda acc, value: (acc if acc > value else value),
        identity,
        0,
    ),
    "min": (
        lambda acc, value: (acc if acc < value else value),
        identity,
        0,
    ),
}


def get_path(path, o):
    key = path[0]
    value = None
    if o is None:
        return None
    elif isinstance(o, (dict,)):
        value = o.get(key)
    elif isinstance(key, int) and isinstance(o, (list, tuple)):
        value = o[key]
    else:
        getattr(o, key)

    if len(path) == 1:
        return value

    return get_path(path[1:], value)


def filter_units(units, filters):
    conds = [
        (partial(get_path, f.field), partial(make_filter(f.op), f.value))
        for f in filters
    ]

    results = []
    for unit in units:
        good = True
        for getter, checker in conds:
            good = checker(getter(unit))
            if good is False:
                break
        if good is True:
            results.append(unit)

    return results


FILTER_AGGREGATE_NUMBER_OPS = {
    "eq": lambda r, x: r == x,
    "lt": lambda r, x: x < r,
    "lte": lambda r, x: x <= r,
    "gt": lambda r, x: x > r,
    "gte": lambda r, x: x >= r,
}


def group_aggregate_filters(fs):
    if len(fs) == 0:
        return None, None, None, fs

    first = fs[0]
    tag = first["tag"]
    field = first["field"]
    base = [first]
    rest = []
    for f in fs[1:]:
        if field == f["field"]:
            base.append(f)
        else:
            rest.append(f)
    return tag, field, base, rest


def filter_aggregate_units(fs, units):
    tag, field, base, rest = group_aggregate_filters(fs)

    if tag == "term":
        term_ids = [f["termId"] for f in base]

        def filter_fn(u):
            return get_path([field], u) in term_ids

        return filter_aggregate_units(rest, list(filter(filter_fn, units)))

    elif tag == "number":

        def filter_fn(unit):
            for f in base:
                op = FILTER_AGGREGATE_NUMBER_OPS[f["op"]]
                filter_value = f["value"]
                unit_value = get_path([field], unit)
                if (
                    filter_value is None
                    or unit_value is None
                    or op(filter_value, unit_value) is False
                ):
                    return False
            return True

        return filter_aggregate_units(rest, list(filter(filter_fn, units)))

    return units


def aggregate_units(units, aggregate, output):
    getter = partial(get_path, output)
    reducer, finalizer, default = AGGREGATE_OPS[aggregate.name]

    if aggregate.filter is not None and len(aggregate.filter) > 0:
        units = filter_aggregate_units(aggregate.filter, units)

    data = list(filter(lambda u: u is not None, map(getter, units)))

    if len(data) > 0:
        reduced = reduce(reducer, data)
        result = finalizer(reduced, units)
        return result

    return default


def parse_select(select, units):
    filtered = filter_units(units, select.filters)
    output = aggregate_units(filtered, select.aggregate, select.field)

    return output


def simple_select(unit, field):
    return Select(unit, field, {"name": "concat"}, [])


def deser_select(obj):
    obj["filters"] = [deser_filter(f) for f in obj.get("filters")]
    if "aggregate" in obj and obj["aggregate"] is not None:
        obj["aggregate"] = Aggregate.from_object(obj.get("aggregate"))
    else:
        obj["aggregate"] = None
    return Select.from_object(obj)


def deser_selects(input):
    """
    Transform "plain" objects into specialized classes
    """
    result = []
    for node in input:
        tag = node["tag"]
        if "select" == tag:
            result.append(deser_select(node))
        else:
            raise Exception('Unknown Tag "{}"'.format(tag))

    return tuple(result)


def get_select_for_unit(unit, selects):
    for select in selects:
        if select.unit == unit:
            return select


# def process_return_data(unit_data, field, select):
#     field_type, field_name = field[:2]
#     is_record = 'label' == field_type or 'text' == field_type

#     if is_record:
#         lang = select.field[1]
#         return get_path([field_name, lang], unit_data)

#     return unit_data.get(field_name)


class MissingWhereError(Exception):
    pass


def get_select_info(selects):
    """takes deserialized select list and
    returns a list o=f tuples (select, unit, is_multi, fields)
    """
    return [(s, s.unit, dyn.is_multi(s.unit), get_unit_fields(s.unit)) for s in selects]


def field_type(select):
    return get_field_type(select.unit, select.field[0])


def ensure_list(x):
    if isinstance(
        x,
        (
            list,
            tuple,
        ),
    ):
        return x
    return [x]


def find_team(id, teams):
    entered = False
    results = []
    for tid, typ, name in teams:
        if tid == id:
            entered = True
            results.append(name)
        elif entered:
            return results
    return results


def get_fk(pk, select, contacts, orgs, users, teams):
    if pk is None:
        return ""
    field = find_field(select.unit, select.field[0])
    model = field[2]
    try:
        if model == "Contact":
            # return Contact.objects.get(pk=pk).name
            return contacts[pk]
        elif model == "FundingOrg":
            # return FundingOrg.objects.get(pk=pk).name
            return orgs[pk]
        elif model == "auth.User":
            # u = User.objects.get(pk=pk)
            # display_name = u.get_full_name()
            # if len(display_name) > 0:
            #     return display_name
            # else:
            #     return u.username
            return users[pk]
        elif model == "Team":
            # t = Team.objects.get(pk=pk)
            # results = [m.member.name for m in t.teammember_set.filter(
            #     role=TeamMember.LEADER)]
            # for m in t.teammember_set.filter(role=TeamMember.REGULAR):
            #     results.append(m.member.name)
            return "; ".join(find_team(pk, teams))

    except Exception:
        return "{}({})".format(model, pk)


def ser_fk(data, select, is_multi_output, contacts, orgs, users, teams):
    if is_multi_output:
        return [
            get_fk(pk, select, contacts, orgs, users, teams) for pk in ensure_list(data)
        ]

    return get_fk(data, select, contacts, orgs, users, teams)


def make_user_name(u):
    display_name = u.get_full_name()
    if len(display_name) > 0:
        return display_name
    else:
        return u.username


def exec_query(query, request):
    """This is the high level entrypoint of this module"""

    if isinstance(query, dict):
        data = query
    elif isinstance(query, Query):
        data = {"statements": loads(query.statements)}
    else:
        raise TypeError(
            "exec_query takes a dict or an angled.Query, got a {}".format(type(query))
        )

    wheres = deser_statements(data.get("statements", {}).get("where", []))
    selects = deser_selects(data.get("statements", {}).get("select", []))

    parsed = parse_input(wheres)
    if parsed is None:
        raise MissingWhereError()

    qs = dyn.Project.objects.filter(parsed).distinct()
    pids = [p.id for p in qs]
    results = []
    select_info = get_select_info(selects)

    fields_info = [("id", "number")]
    for select, _, _, _ in select_info:
        ty = field_type(select)
        if ty != "geometry":
            fields_info.append([select.name, ty])

    if len(pids) > 0:
        pdata = dyn.Project.full_loader.get_all_as_dict(pids)
        projects = ProjectSerializer(pdata, many=True, context=dict(request=request))

        # Here's a bit of caching, it works.
        contacts = {
            c["id"]: c["name"] for c in Contact.objects.all().values("id", "name")
        }
        orgs = {
            o["id"]: o["name"] for o in FundingOrg.objects.all().values("id", "name")
        }
        users = {u.id: make_user_name(u) for u in User.objects.all()}
        teams = [
            [t["team_id"], t["role"], t["member__name"]]
            for t in TeamMember.objects.all().values("team_id", "member__name", "role")
        ]

        for project in projects.data:
            row = dict(id=project["id"], __audiences__={})

            for select, unit, is_multi, fields in select_info:
                colname = select.name
                # filter out duplicates
                if colname in row:
                    continue

                unit_data = project.get(unit)
                if unit_data is None:
                    row[colname] = [] if is_multi else None
                    row["__audiences__"][colname] = []
                    continue

                if is_multi:
                    audiences = set()
                    for data in unit_data:
                        readings = data.get("readings", [])
                        audiences = audiences.union([r["audience"] for r in readings])
                    row["__audiences__"][colname] = list(audiences)
                else:
                    readings = unit_data.get("readings", [])
                    row["__audiences__"][colname] = [r["audience"] for r in readings]

                is_multi_output = (
                    select.aggregate is not None and select.aggregate.name == "concat"
                )
                is_fk = field_type(select) == "fk"

                if is_multi:
                    output = parse_select(select, unit_data)
                else:
                    output = get_path(select.field, unit_data)

                if is_fk:
                    row[colname] = ser_fk(
                        output, select, is_multi_output, contacts, orgs, users, teams
                    )
                else:
                    row[colname] = output

            results.append(row)

    return results, fields_info
