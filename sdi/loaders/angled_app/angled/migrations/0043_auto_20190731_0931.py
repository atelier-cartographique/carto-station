# Generated by Django 2.1.7 on 2019-07-31 09:31

from django.conf import settings
import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion
import django.db.models.manager


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('angled', '0042_auto_20190710_1459'),
    ]

    operations = [
        migrations.CreateModel(
            name='CircleNotification',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('mark_view', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
            managers=[
                ('recent_objects', django.db.models.manager.Manager()),
            ],
        ),
        migrations.CreateModel(
            name='CircleSubscription',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('active', models.BooleanField(default=False)),
                ('radius', models.IntegerField()),
                ('geom', django.contrib.gis.db.models.fields.PointField(srid=31370)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ProjectNotification',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('mark_view', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
            managers=[
                ('recent_objects', django.db.models.manager.Manager()),
            ],
        ),
        migrations.CreateModel(
            name='ProjectSubscription',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('active', models.BooleanField(default=False)),
                ('project', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='angled.Project')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='projectnotification',
            name='subscription',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='notifications', to='angled.ProjectSubscription'),
        ),
        migrations.AddField(
            model_name='circlenotification',
            name='subscription',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='notifications', to='angled.CircleSubscription'),
        ),
    ]
