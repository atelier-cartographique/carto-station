# Generated by Django 3.2.5 on 2024-10-23 14:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("angled", "0069_profile_name"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="profile",
            name="name",
        ),
    ]
