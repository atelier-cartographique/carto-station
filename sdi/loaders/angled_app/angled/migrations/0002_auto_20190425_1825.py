# Generated by Django 2.1.7 on 2019-04-25 18:25

import angled.models.profi
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('angled', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='json',
            field=angled.models.profi.JSONField(),
        ),
    ]
