from django.db import migrations

def addsortindex(apps, schema_editor):
    Term = apps.get_model('angled', 'Term')
    Domain = apps.get_model('angled', 'Domain')
    ds = Domain.objects.all()
    for d in ds: 
        termlist = Term.objects.filter(domain = d)
        for idx, term in enumerate(termlist):
            term.sort_index = idx + 1 
            term.save()


class Migration(migrations.Migration):

    dependencies = [
        ('angled', '0064_auto_20230213_1148'),
    ]

    operations = [
        migrations.RunPython(addsortindex),
    ]
