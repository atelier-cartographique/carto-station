from angled_app.settings import *


DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'HOST': 'db',
        'USER': 'postgres',
        'NAME': 'postgres',
        'PASSWORD': 'postgres',
        'PORT': '5432'
    }
}
