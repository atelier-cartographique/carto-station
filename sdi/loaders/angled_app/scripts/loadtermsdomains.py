import sys
import os, django

django.setup()
from angled.models import ref
from angled.serializers.universe import TermSerializer, DomainSerializer
from rest_framework.renderers import JSONRenderer
import json
import logging

_logger = logging.getLogger(__name__)

domain_id_map = {}

prefix = sys.argv[1]

ref.Term.objects.all().delete()
ref.Domain.objects.all().delete()

with open("{}domains.json".format(prefix)) as f:
    data = json.load(f)
    for d in data:
        id = d["id"]
        name = d["name"]
        description = d["description"]

        domain = ref.Domain.objects.create(name=name, description=description)
        domain_id_map[id] = domain.id
        _logger.debug("Domain {} ({} => {})".format(domain.name, id, domain.id))
        # break

with open("{}terms.json".format(prefix)) as f:
    data = json.load(f)
    for d in data:
        domain_id = domain_id_map[d["domain"]]
        domain = ref.Domain.objects.get(id=domain_id)
        name = d["name"]
        description = d["description"]
        term = ref.Term.objects.create(
            domain=domain, name=name, description=description
        )
        _logger.debug("Term {} {}".format(domain, term.name))
