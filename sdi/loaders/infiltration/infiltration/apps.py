from django.apps import AppConfig


class InfiltrationConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "infiltration"
    geodata = True

    def ready(self):
        from infiltration.signals import connect

        connect()
