from django.urls import path
from mdq.views import (
    get_dataset,
    get_reference_layer,
    get_extra_layers_list,
    get_extra_layer,
    get_wms_blank,
)
from mdq.exports.geojson import get_dataset_geojson
from mdq.exports.shapefile import get_dataset_shapefile
from mdq.exports.table import get_dataset_table


urlpatterns = [
    path("mdq/dataset/<dataset_id>", get_dataset, name="mdq.get_dataset"),
    path(
        "mdq/geojson/<dataset_id>/<lang>",
        get_dataset_geojson,
        name="mdq.get_dataset_geojson",
    ),
    path(
        "mdq/shapefile/<dataset_id>/<lang>",
        get_dataset_shapefile,
        name="mdq.get_dataset_shapefile",
    ),
    path(
        "mdq/table/<dataset_id>/<lang>", get_dataset_table, name="mdq.get_dataset_table"
    ),
    path("mdq/ref/<level>", get_reference_layer, name="mdq.get_reference_layer"),
    path("mdq/extra/<int:id>", get_extra_layer, name="mdq.get_extra_layer"),
    path("mdq/extra/", get_extra_layers_list, name="mdq.get_extra_layers_list"),
    path("mdq/wms/blank", get_wms_blank, name="mdq.get_wms_blank"),
]
