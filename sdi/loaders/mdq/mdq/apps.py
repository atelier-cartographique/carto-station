from django.apps import AppConfig


class MdqConfig(AppConfig):
    name = 'mdq'
    label = 'mdq'
    verbose_name = 'Monitoring Des Quartiers'
    geodata = True
