from django.conf import settings

LEVEL_NAMES = {
    "fr": {
        "M": "Commune",
        "D": "Quartier",
        "S": "Secteur statistique",
    },
    "nl": {
        "M": "Gemeente",
        "D": "Wijke",
        "S": "Statistisch sector",
    },
}


def level_name(level, lang):
    return LEVEL_NAMES[lang][level]


PUBLIC_DOMAIN_FR = getattr(
    settings, "MDQ_PUBLIC_DOMAIN_FR", "monitoringdesquartiers.brussels"
)
PUBLIC_DOMAIN_NL = getattr(settings, "MDQ_PUBLIC_DOMAIN_NL", "wijkmonitoring.brussels")

PUBLIC_DOMAIN = {
    "fr": PUBLIC_DOMAIN_FR,
    "nl": PUBLIC_DOMAIN_NL,
}


def base_url(lang):
    domain = PUBLIC_DOMAIN[lang]
    return f"https://{domain}"


def make_url(description, lang):
    if description.indicator_id is not None:
        domain = PUBLIC_DOMAIN[lang]
        return f"https://{domain}/Indicator/IndicatorPage/{description.indicator_id}?tab=Sheet"
    return None
