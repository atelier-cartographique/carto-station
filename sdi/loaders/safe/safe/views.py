from json import loads

from django.http import HttpResponseForbidden, JsonResponse, HttpResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required

from .models import Annotation, AnnotationTypeCategory, Case, AnnotationType
from .serializers import (
    deserialize_case,
    serialize_annotation,
    serialize_annotation_type,
    serialize_annotation_type_category,
    serialize_case,
    deserialize_annotation,
    handle_geometries,
)



class HTTPResponseNoContent(HttpResponse):
    status_code = 204

def get_all_cases(request):
    cases = Case.objects.all()
    data = [serialize_case(c) for c in cases]
    return JsonResponse(data, safe=False)


def get_all_annotation_types(request):
    data = [serialize_annotation_type(a) for a in AnnotationType.objects.all()]
    return JsonResponse(data, safe=False)


def get_all_annotation_type_categories(request):
    data = [
        serialize_annotation_type_category(c)
        for c in AnnotationTypeCategory.objects.all()
    ]
    return JsonResponse(data, safe=False)


def get_annotations_for(request, case_id):
    case = get_object_or_404(Case, pk=case_id)
    data = [serialize_annotation(a) for a in Annotation.objects.filter(case=case)]
    return JsonResponse(data, safe=False)


@login_required
@require_http_methods(["POST"])
def post_case(request):
    data = loads(request.body.decode("utf-8"))
    instance = deserialize_case(data)
    instance.user = request.user
    instance.save()
    instance.refresh_from_db()
    return JsonResponse(serialize_case(instance), status=201)


@login_required
@require_http_methods(["POST"])
def post_annotation(request):
    data = loads(request.body.decode("utf-8"))
    instance = deserialize_annotation(data)
    instance.user = request.user
    instance.save()
    instance.refresh_from_db()

    handle_geometries(data, instance)

    return JsonResponse(serialize_annotation(instance), status=201)


@login_required
@require_http_methods(["DELETE"])
def delete_annotation(request, annotation_id):
    instance = get_object_or_404(Annotation, id=annotation_id)
    user = request.user
#     if instance.user is not user:
#         return HttpResponseForbidden("Only author of the annotation can delete it")
    instance.delete()

    return HTTPResponseNoContent()
