from pathlib import PosixPath
from csv import DictReader
from django.core.management.base import BaseCommand
from django.utils.text import slugify
from safe.models import AnnotationType, AnnotationTypeCategory


def ident(row):
    return slugify(row["codename"])


def code(row):
    return int(row["codepoint"], base=16)


def lang_rec(row, prefix):
    fr = row[f"{prefix}_fr"]
    nl = row[f"{prefix}_nl"]
    en = row[f"{prefix}_en"]

    fr = fr if fr else en if en else nl
    nl = nl if nl else en if en else fr

    return dict(fr=fr, nl=nl)


def name(row):
    return lang_rec(row, "name")


NULL_CAT = dict(fr="", nl="")
GENERAL_REC = dict(
    name=dict(fr="general", nl="general"), description=dict(fr="general", nl="general")
)


def cat(row, categories):
    rec = lang_rec(row, "cat")
    if rec == NULL_CAT:
        return categories["general"]
    cat_hash = hash(str(rec))
    if cat_hash not in categories:
        categories[cat_hash] = AnnotationTypeCategory.objects.create(
            name=rec, description=rec
        )

    return categories[cat_hash]


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("mappings")

    def handle(self, *args, **options):
        mappings = PosixPath(options["mappings"])

        with mappings.open() as csv_file:
            reader = DictReader(csv_file)
            # In [11]: reader.fieldnames
            # Out[11]:
            # ['codepoint',
            # 'codename',
            # 'name_fr',
            # 'name_nl',
            # 'name_en',
            # 'cat_fr',
            # 'cat_nl',
            # 'cat_en']
            AnnotationType.objects.all().delete()
            AnnotationTypeCategory.objects.all().delete()
            categories = dict(
                general=AnnotationTypeCategory.objects.create(**GENERAL_REC)
            )
            for row in reader:
                AnnotationType.objects.create(
                    id=ident(row),
                    name=name(row),
                    description=name(row),
                    codepoint=code(row),
                    tag=AnnotationType.MARKER,
                    category=cat(row, categories),
                )
