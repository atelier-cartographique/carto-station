from api.models import UserMap
from django.contrib.auth.models import User
from lingua.fields import nullable_label_field, label_field, text_field
from django.contrib.gis.db import models
from django.contrib.auth.models import User

import uuid


class Case(models.Model):
    PLANIFICATION = "plan"
    MANAGEMENT = "manage"

    TYPE_KEYS = (
        (PLANIFICATION, "planification"),
        (MANAGEMENT, "management"),
    )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = nullable_label_field("case_name")
    source = nullable_label_field("case_source")
    created_at = models.DateTimeField(null=False, auto_now_add=True)
    start_date = models.DateTimeField(null=False, blank=True)
    end_date = models.DateTimeField(null=True, blank=True)
    description = nullable_label_field("case_description")
    informations = models.CharField(max_length=1024)  # FIXME
    case_type = models.CharField(
        max_length=20, choices=TYPE_KEYS, default=PLANIFICATION
    )
    base_map = models.ForeignKey(
        UserMap,
        on_delete=models.PROTECT,
        related_name="+",
    )
    user = models.ForeignKey(User, on_delete=models.PROTECT, related_name="+")

    class Meta:
        verbose_name = "Case"
        verbose_name_plural = "Cases"


class AnnotationTypeCategory(models.Model):
    name = label_field("annotation_type_category")
    description = text_field("annotation_type_description")

    def __str__(self) -> str:
        return str(self.name)


class AnnotationType(models.Model):
    MARKER = "marker"
    ZONE = "zone"
    PATH = "path"
    SEGMENT = "segment"
    CONE = "cone"
    CIRCLE = "circle"

    TAGS = (
        (MARKER, "Marker"),
        (ZONE, "Zone"),
        (PATH, "Path"),
        (SEGMENT, "Line"),
        (CONE, "Cone"),
        (CIRCLE, "Circle"),
    )

    id = models.CharField(max_length=64, primary_key=True)
    tag = models.CharField(max_length=12, choices=TAGS)
    name = label_field("annotation_name")
    description = text_field("annotation_description")
    codepoint = models.IntegerField()
    category = models.ForeignKey(
        AnnotationTypeCategory, on_delete=models.PROTECT, null=True, blank=True
    )

    def __str__(self) -> str:
        return f"{self.tag}/{self.id}"


class KeyWord(models.Model):
    annotation_type = models.ForeignKey(AnnotationType, on_delete=models.CASCADE)
    text = label_field("annotation_keyword")


class Annotation(models.Model):
    # EXPLOSION = "expl"
    # MANIFESTATION = "manif"
    # GAS_SPILL = "gas"

    # ANNOTATION_TYPE_KEYS = (
    #     (EXPLOSION, "explosion"),
    #     (MANIFESTATION, "manifestation"),
    #     (GAS_SPILL, "gas spill"),
    # )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    type = models.ForeignKey(
        AnnotationType, on_delete=models.CASCADE, null=True, blank=True
    )
    source = nullable_label_field("annotation_source")  # a catalog maybe?
    start_date = models.DateTimeField(null=True, blank=True)
    end_date = models.DateTimeField(null=True, blank=True)
    acknowledgement_date = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True, editable=False)
    information_annex = models.TextField(default=str, null=True)
    case = models.ForeignKey(Case, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.PROTECT, related_name="+")

    class Meta:
        verbose_name = "Annotation"
        verbose_name_plural = "Annotations"


class AnnotationGeometry(models.Model):
    POINT = "Point"
    POLYGON = "Polygon"
    LINESTRING = "LineString"

    GEOMETRY = (
        (POINT, POINT),
        (LINESTRING, LINESTRING),
        (POLYGON, POLYGON),
    )

    type = models.CharField(max_length=16, choices=GEOMETRY)
    annotation = models.ForeignKey(Annotation, on_delete=models.CASCADE)
    point = models.PointField(srid=31370, null=True, blank=True)
    line = models.LineStringField(srid=31370, null=True, blank=True)
    polygon = models.PolygonField(srid=31370, null=True, blank=True)
    properties = models.JSONField(default=dict)
