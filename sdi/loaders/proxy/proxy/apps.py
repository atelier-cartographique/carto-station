from django.apps import AppConfig


class ProxyConfig(AppConfig):
    name = 'proxy'
    label = 'proxy'
    verbose_name = 'Proxy'
    geodata = True
