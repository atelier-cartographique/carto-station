from django.contrib import admin

from .models import (
    Server,
    Layer,
    TransformProj,
    TransformPython,
)


class ServerAdmin(admin.ModelAdmin):
    list_display = ('hostname', 'secure')


class TransformProjInline(admin.TabularInline):
    model = TransformProj


class TransformPythonInline(admin.TabularInline):
    model = TransformPython


class LayerAdmin(admin.ModelAdmin):
    list_display = ('path', )
    inlines = [TransformProjInline, TransformPythonInline]


admin.site.register(Server, ServerAdmin)
admin.site.register(Layer, LayerAdmin)
admin.site.register(TransformProj)
admin.site.register(TransformPython)
