from urllib.parse import urlencode

from basic_wfs.xml import (
    append,
    create,
    create_tree,
    rid_to_typename_ns,
    strip_ns,
    tree_to_string,
)
from django.urls import reverse

from .models import get_layer
from .serializer import get_geojson
import logging

_logger = logging.getLogger(__name__)


def ensure_list(x):
    if isinstance(
        x,
        (
            list,
            tuple,
        ),
    ):
        return x
    return [x]


def make_field_type(field):
    """TODO"""
    return "xsd:string"


def describe_feature_type(request, typename, schema, table):
    typename_name = strip_ns(typename)
    typename_type = "{}Type".format(typename_name)
    model, geometry_field, geometry_field_type = get_layer(schema, table)
    attributes = {
        "targetNamespace": "http://www.carto-station.com/postgis",
        "xmlns:postgis": "http://www.carto-station.com/postgis",
        "xmlns:xsd": "http://www.w3.org/2001/XMLSchema",
        "xmlns:gml": "http://www.opengis.net/gml/3.2",
        "elementFormDefault": "qualified",
    }
    schema = create("xsd:schema", attributes)

    # gml
    append(
        schema,
        "xsd:import",
        {
            "namespace": "http://www.opengis.net/gml/3.2",
            "schemaLocation": "http://schemas.opengis.net/gml/3.2.1/gml.xsd",
        },
    )

    ct = append(
        schema,
        "xsd:complexType",
        {
            "name": typename_type,
        },
    )
    cc = append(ct, "xsd:complexContent")
    aft = append(cc, "xsd:extension", {"base": "gml:AbstractFeatureType"})
    seq = append(aft, "xsd:sequence")
    append(
        seq, "xsd:element", {"name": geometry_field, "type": "gml:GeometryPropertyType"}
    )

    for field in model._meta.fields:
        append(
            seq,
            "xsd:element",
            {
                "name": field.name,
                "type": make_field_type(field),
                "maxOccurs": "1",
                "minOccurs": "0",
            },
        )

    append(
        schema,
        "xsd:element",
        {
            "name": typename_name,
            "type": typename_type,
            "substitutionGroup": "gml:_Feature",
        },
    )

    return tree_to_string(create_tree(schema))


# TODO: other functions should be marked marked private
def get_feature_type(request, lang, rid_url):
    schema = rid_url.hostname
    table = rid_url.path[1:]
    typename = rid_to_typename_ns(rid_url.geturl())

    return describe_feature_type(request, typename, schema, table)


def encode_point(point):
    return "{},{}".format(point[0], point[1])


def encode_line(line):
    return " ".join(list(map(encode_point, line)))


def encode_geometry(feature, geometry):
    gt = geometry["type"]
    coordinates = geometry["coordinates"]

    # TODO: move type encoders to basic-wfs
    if "MultiPolygon" == gt:
        root = append(feature, "gml:MultiPolygon", dict(srsName="EPSG:31370"))
        for polygon in coordinates:
            member = append(root, "gml:polygonMember")
            poly = append(member, "gml:Polygon")
            outer = append(poly, "gml:outerBoundaryIs")
            ring = append(outer, "gml:LinearRing")
            coords = append(ring, "gml:coordinates")
            coords.text = encode_line(polygon[0])

    elif "MultiLineString" == gt:
        root = append(feature, "gml:MultiLineString", dict(srsName="EPSG:31370"))
        for linestring in coordinates:
            member = append(root, "gml:lineStringMember")
            line = append(member, "gml:LineString")
            coords = append(line, "gml:coordinates")
            coords.text = encode_line(linestring)

    elif "MultiPoint" == gt:
        root = append(feature, "gml:MultiPoint", dict(srsName="EPSG:31370"))
        for point in coordinates:
            member = append(root, "gml:pointMember")
            pt = append(member, "gml:Point")
            pos = append(pt, "gml:pos")
            pos.text = encode_point(point)

    elif "Polygon" == gt:
        root = append(feature, "gml:Polygon", dict(srsName="EPSG:31370"))
        outer = append(root, "gml:outerBoundaryIs")
        ring = append(outer, "gml:LinearRing")
        coords = append(ring, "gml:coordinates")
        coords.text = encode_line(coordinates[0])

    elif "LineString" == gt:
        root = append(feature, "gml:LineString", dict(srsName="EPSG:31370"))
        line = append(root, "gml:LineString")
        coords = append(line, "gml:coordinates")
        coords.text = encode_line(coordinates)

    elif "Point" == gt:
        pt = append(feature, "gml:Point", dict(srsName="EPSG:31370"))
        pos = append(pt, "gml:pos")
        pos.text = encode_point(coordinates)

    else:
        raise NotImplementedError("Unsupported Geometry Type <{}>".format(gt))


def ns_tag(ns, tag):
    return "{}:{}".format(ns, tag)


def encode_feature(coll, feature, ns, typename):
    element = append(coll, "gml:featureMember")
    root = append(
        element,
        typename,
        {
            ns_tag(ns, "id"): str(feature["id"]),
        },
    )

    props = feature["properties"]
    geom = feature["geometry"]
    for k, v in props.items():
        append(root, ns_tag(ns, k)).text = str(v)

    geom_root = append(root, ns_tag(ns, "geom"))
    encode_geometry(geom_root, geom)


def bbox_from_request(request):
    """Extract bbox from request
    'BBOX=143401.41840277760638855,170288.21006944443797693,146811.3420138887304347,172814.77256944443797693'
    """
    bbox_string = request.GET.get("BBOX")

    minx, miny, maxx, maxy = list(map(float, bbox_string.split(",")[0:4]))
    return (minx, miny, maxx, maxy)


def get_pk_name(schema, table):
    model, geometry_field, geometry_field_type = get_layer(schema, table)
    meta = model._meta
    return meta.pk.get_attname()


def _results_to_gml(request, lang, rid_url, schema, table):
    ns = rid_url.scheme
    typename = rid_to_typename_ns(rid_url.geturl())
    kwargs = {}
    try:
        kwargs["bbox"] = bbox_from_request(request)
    except Exception:
        # FIXME: is the request valid without a valid bbox ?
        #        should we allow a silend pass on this ?
        pass
    features = get_geojson(schema, table, **kwargs).get("features", [])
    pk = get_pk_name(schema, table)

    loc = "{}?{}".format(
        request.build_absolute_uri(reverse("basic-wfs", args=(lang,))),
        urlencode(
            {
                "SERVICE": "WFS",
                "REQUEST": "DescribeFeatureType",
                "TYPENAME": typename,
            }
        ),
    )

    attributes = {
        "xmlns:postgis": "http://www.carto-station.com/postgis",
        "xmlns:gml": "http://www.opengis.net/gml",
        "xmlns:wfs": "http://www.opengis.net/wfs",
        "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "xsi:schemaLocation": "http://www.carto-station.com/postgis {}".format(loc),
    }
    root = create("wfs:FeatureCollection", attributes)

    for feature in features:
        try:
            geom_data = feature.get("geometry")
            properties = feature.get("properties")
            feature_id = properties.get(pk) if properties is not None else None
            if geom_data is not None and feature_id is not None:
                encode_feature(
                    root,
                    dict(
                        id=feature_id,
                        geometry=geom_data,
                        properties=properties,
                    ),
                    ns,
                    typename,
                )

        except Exception as ex:
            # TODO:log rather than _logger.debuging, do not catch all exceptions
            _logger.debug("_results_to_gml#project", str(ex))

    return tree_to_string(create_tree(root))


# TODO: other functions should be marked marked private
def get_feature(request, lang, rid_url):
    schema = rid_url.hostname
    table = rid_url.path[1:]
    return _results_to_gml(request, lang, rid_url, schema, table)
