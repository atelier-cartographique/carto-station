import json
from django.contrib.gis.db import models
from django.contrib.postgres.fields import ArrayField
from django.contrib.auth.models import User
from django.utils.text import slugify
from datetime import datetime

from .reference import get_country_choices


class ReferenceAlias(models.Model):
    id = models.AutoField(primary_key=True)
    select = models.TextField(blank=True, unique=True)
    replace = models.TextField()

    def __str__(self):
        return f"{self.select}"


class FormProject(models.Model):
    name = models.CharField(max_length=512)
    country = models.CharField(max_length=30, choices=get_country_choices())

    def __str__(self) -> str:
        return self.name


class SurveyFile(models.Model):
    file = models.FileField()
    user = models.ForeignKey(User, related_name="+", on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    project = models.ForeignKey(
        FormProject,
        related_name="survey_file",
        on_delete=models.CASCADE,
        # TODO: unique?
    )


def project_config_path(instance, filename):
    project_name = slugify(instance.project.name)
    fn = slugify(filename)
    now = datetime.now().isoformat(timespec="hours")
    return f"asf-configs/{project_name}/{now}_{fn}"


FIELD_NUMBER = "number"
FIELD_TEXT = "text"
FIELD_BOOLEAN = "boolean"
FIELD_DATETIME = "datetime"
FIELD_GEOMETRY = "geom"
FIELD_ERROR = "error"
FIELD_TYPE_CHOICES = (
    (FIELD_NUMBER, "Number"),
    (FIELD_TEXT, "Text"),
    (FIELD_BOOLEAN, "Boolean"),
    (FIELD_DATETIME, "Date Time"),
    (FIELD_GEOMETRY, "Geometry"),
    (FIELD_ERROR, "Error"),
)

GEOMETRY_CELL_NAME = "__geometry__"


class ImportConfig(models.Model):
    project = models.OneToOneField(
        FormProject, related_name="config", on_delete=models.CASCADE
    )
    columns = ArrayField(models.CharField(max_length=128))
    types = ArrayField(models.CharField(max_length=8, choices=FIELD_TYPE_CHOICES))
    # file = models.FileField(upload_to=project_config_path, null=True, blank=True)
    survey_file = models.ForeignKey(
        SurveyFile,
        related_name="config",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )

    # @property
    # def file(self):
    #     self.survey_file.file

    def get_fields(self):
        fields = []
        types = self.types
        for i, column in enumerate(self.columns):
            fields.append((column.strip(), types[i]))
        return fields

    def get_geometry_field(self):
        for i, name in enumerate(self.types):
            if name == "geom":
                return self.columns[i]

        raise Exception("Geometry Field Not Found")

    def __str__(self) -> str:
        if (self.project is not None) and (self.survey_file is not None):
            return f"{self.project.name} / {self.survey_file.file.name}"
        return "Not ready"


class NameToCode(models.Model):
    config = models.ForeignKey(
        ImportConfig, related_name="alias", on_delete=models.CASCADE
    )
    name = models.CharField(max_length=256)
    code = models.CharField(max_length=16)


RowStatusCurrent = "current"
RowStatusDraft = "draft"


class Row(models.Model):
    class Meta:
        ordering = ["index"]

    STATUS_CHOICES = (
        (RowStatusCurrent, "Current"),
        (RowStatusDraft, "Draft"),
    )
    config = models.ForeignKey(
        ImportConfig, related_name="imports", on_delete=models.CASCADE
    )
    index = models.IntegerField()

    status = models.CharField(
        max_length=20,
        choices=STATUS_CHOICES,
    )

    def get_cell(self, name):
        return Cell.objects.get(row=self, name=name)

    def get_cells(self):
        return Cell.objects.filter(row=self)

    def get_geometry(self):
        # geom_field = self.config.get_geometry_field()
        return self.get_cell(GEOMETRY_CELL_NAME).geom

    def get_properties(self):
        results = {}
        # geom_field = self.config.get_geometry_field()
        for name, ty in self.config.get_fields():
            results[name] = getattr(self.get_cell(name), ty)
            # if name != geom_field:
            #     results[name] = getattr(self.get_cell(name), ty)

        return results

    def get_place(self):
        geom_field = self.config.get_geometry_field()
        return self.get_cell(geom_field).value

    def get_feature(self):

        try:
            properties = self.get_properties()
            geometry = json.loads(self.get_geometry().geojson)
            return dict(type="Feature", properties=properties, geometry=geometry)
        except Exception:
            return dict(
                type="Feature", properties=self.get_properties()
            )  # TODO: check if this makes any sense...

    def __str__(self) -> str:
        return f"{self.index} # {self.config}"


class Cell(models.Model):
    row = models.ForeignKey(Row, on_delete=models.CASCADE, related_name="cells")
    name = models.CharField(max_length=512)
    type = models.CharField(max_length=8, choices=FIELD_TYPE_CHOICES)
    number = models.FloatField(null=True, blank=True)
    text = models.TextField(null=True, blank=True)
    boolean = models.BooleanField(null=True, blank=True)
    datetime = models.DateTimeField(null=True, blank=True)
    geom = models.MultiPolygonField(srid=4326, null=True, blank=True)
    error = models.TextField(null=True, blank=True)

    def __str__(self) -> str:
        return f"{self.name} ({self.row})"

    @property
    def value(self):
        return getattr(self, self.type)


def make_cell_manager(kind):
    class Manager(models.Manager):
        def create(self, row, name, value):
            kwargs = {"type": kind, "row": row, "name": name, kind: value}
            return super().create(**kwargs)

        def get_queryset(self):
            return super().get_queryset().filter(type=kind)

    return Manager()


def make_cell_constructor(kind):
    def inner(self, name, value):
        super(Cell, self).__init__()
        self.type = kind
        self.name = name
        setattr(self, kind, value)

    return inner


def make_cell_model(kind: str):
    cap_kind = kind.capitalize()
    class_name = f"Cell{cap_kind}"
    cls = type(
        class_name,
        (Cell,),
        dict(
            Meta=type("Meta", (), dict(app_label="asf", proxy=True)),
            __module__="asf.models",
            objects=make_cell_manager(kind),
            __init__=make_cell_constructor(kind),
        ),
    )

    return cls


CellNumber = make_cell_model("number")
CellText = make_cell_model("text")
CellBoolean = make_cell_model("boolean")
CellDateTime = make_cell_model("datetime")
CellGeom = make_cell_model("geom")
CellError = make_cell_model("error")


# class CellNumber(Cell):
#     class Meta:
#         proxy = True

#     objects = make_cell_manager("number")


# class CellText(Cell):
#     class Meta:
#         proxy = True

#     objects = make_cell_manager("text")


# class CellBoolean(Cell):
#     class Meta:
#         proxy = True

#     objects = make_cell_manager("boolean")


# class CellDateTime(Cell):
#     class Meta:
#         proxy = True

#     objects = make_cell_manager("datetime")


# class CellGeom(Cell):
#     class Meta:
#         proxy = True

#     objects = make_cell_manager("geom")
