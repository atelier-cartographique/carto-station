from datetime import datetime
import json
from django.conf import settings

from django.contrib.gis.geos.geometry import GEOSGeometry
from postgis_loader.models import get_layer

import logging

_logger = logging.getLogger(__name__)

REFERENCE_LAYER = getattr(settings, "ASF_REFERENCE_LAYER")
CODE_FIELD = getattr(settings, "ASF_CODE_FIELD")
NAME_FIELD = getattr(settings, "ASF_NAME_FIELD")
COUNTRY_FIELD = getattr(settings, "ASF_COUNTRY_FIELD")
COUNTRIES = getattr(settings, "ASF_COUNTRIES")


def get_countries():
    schema, layer = REFERENCE_LAYER
    Model, geometry_field, geometry_field_type = get_layer(schema, layer)
    countries = Model.objects.all().distinct(COUNTRY_FIELD)
    _logger.debug(f"countries ----------- : {countries}")
    return [r[COUNTRY_FIELD] for r in countries.values(COUNTRY_FIELD)]


def get_country_choices():
    try:
        return [(code, code) for code in COUNTRIES]

    except:
        return [(code, code) for code in get_countries()]


class Reference:
    class NotFound(Exception):
        def __init__(self, what) -> None:
            super().__init__(what)

    _data = None
    _geometry_field = None

    def __init__(self, country):
        self._country_arg = {COUNTRY_FIELD: country}

    def get_data(self):
        if self._data is None:
            start = datetime.now()
            schema, layer = REFERENCE_LAYER
            Model, geometry_field, geometry_field_type = get_layer(schema, layer)
            self._data = Model.objects.filter(**self._country_arg)
            self._geometry_field = geometry_field
            diff = datetime.now() - start
            _logger.debug("Reference Loaded In ", diff.microseconds / 1000)
        return self._data

    def get_names(self):
        return sorted(
            list(set([v[NAME_FIELD] for v in self.get_data().values(NAME_FIELD)]))
        )

    def get_rows_for_code(self, code):
        kwargs = {CODE_FIELD: code}
        results = self.get_data().filter(**kwargs)
        if results.count() == 0:
            raise Reference.NotFound(code)

        return self.make_geometry(results)

    def lookup(self, name, lang=None):
        key = (
            f"{NAME_FIELD}__icontains"
            if lang is None
            else f"{NAME_FIELD}_{lang}__icontains"
        )
        kwargs = {key: name}
        results = self.get_data().filter(**kwargs)
        if results.count() == 0:
            raise Reference.NotFound(name)

        return self.make_geometry(results)

    def make_geometry(self, rows):
        """TODO: avoid this ser/deser pass"""
        coordinates = [getattr(r, self._geometry_field).tuple[0] for r in rows]
        return GEOSGeometry(
            json.dumps(dict(type="MultiPolygon", coordinates=coordinates))
        )
