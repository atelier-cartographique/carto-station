import csv
import json
import re
import tempfile
import zipfile
from datetime import date, datetime
from pathlib import PosixPath
from urllib.parse import urlparse
from collections import namedtuple

from django.http import (
    JsonResponse,
    HttpResponseBadRequest,
    HttpResponseNotFound,
    FileResponse,
    HttpResponseForbidden,
)
from django.shortcuts import get_object_or_404
from django.core.serializers import serialize
from django.utils.text import slugify
from django.db.models import OuterRef, Subquery, Q, Count
from django.conf import settings
from django.template.loader import render_to_string

from water import models, serializers
from water.cache import CacheMiss, datacount_cache, stationcount_cache
from water.config import parse_config
from api.models.map import UserMap
from api.models.metadata import MetaData
from api.serializers.map import LayerInfoSerializer

from water.stat import (
    get_statistics_surface,
    get_statistics_ground_quality,
    get_statistics_ground_quantity,
)

import logging

_logger = logging.getLogger(__name__)


def get_catalog_surface(request):
    ser = serializers.surface.serialize_parameter
    Parameter = models.surface.Parameter
    params = [
        ser(p)
        for p in Parameter.objects.prefetch_related(
            "parresult_set", "parresult_set__grouppar_set"
        ).all()
        # ser(p) for p in Parameter.objects.select_related('parresult').all()
    ]

    return JsonResponse(params, safe=False)


def get_catalog_ground_quality(request):
    ser = serializers.ground.serialize_parameter_quality
    Parameter = models.ground.GwQualParametre
    params = [ser(p) for p in Parameter.objects.all()]

    return JsonResponse(params, safe=False)


PIEZO_RELATIVE = 1
PIEZO_ABSOLUTE = 2


def get_catalog_ground_quantity(request):
    """Note that this is basically hardcoded here to make it consistent with the other catalogs, but client should know"""
    piezo_rel = {
        "id": PIEZO_RELATIVE,
        "code": "rel",
        "name": {
            "fr": "Profondeur du niveau d’eau par rapport à la surface du sol ",
            "nl": "Diepte van het waterpeil ten opzichte van het grondoppervlak",
            "en": "-",
        },
        "unit": "m",
        "groups": [],
        "norms": [],
    }
    piezo_abs = {
        "id": PIEZO_ABSOLUTE,
        "code": "abs",
        "name": {
            "fr": "Piézométrie absolue",
            "nl": "Absolute piëzometrie",
            "en": "-",
        },
        "unit": "m",
        "groups": [],
        "norms": [],
    }
    params = [piezo_rel, piezo_abs]

    return JsonResponse(params, safe=False)


def get_timeserie_surface_data(station, parameter, start, end):
    Results = models.surface.Results
    Site = models.surface.Site
    Parameter = models.surface.Parameter
    ser = serializers.surface.serialize_timeserie_record
    param = get_object_or_404(Parameter, pk=parameter)
    site = get_object_or_404(Site, pk=station)
    args = [Q(val_flag__id_val_flag="A") | Q(val_flag__id_val_flag="B")]
    kwargs = dict(
        id_par_result__id_par=param,
        id_sample__gid_point__gid_site=site,
    )
    if start is not None:
        kwargs["id_sample__sampling_date__gte"] = start
    if end is not None:
        kwargs["id_sample__sampling_date__lte"] = end

    queryset = (
        Results.objects.filter(*args, **kwargs)
        .exclude(validated_txt_value="NA")
        .order_by("id_sample__sampling_date")
    )

    return [ser(r) for r in queryset]


# to ease use of  serializers.surface.serialize_timeserie_record
IdSample = namedtuple("IdSample", ["sampling_date"])
SurfaceRecord = namedtuple("SurfaceRecord", ["validated_txt_value", "id_sample"])


def get_timeserie_surface_data_batch(stations, parameters, start, end):
    from functools import reduce

    Results = models.surface.Results
    Site = models.surface.Site
    Parameter = models.surface.Parameter
    ser = serializers.surface.serialize_timeserie_record
    params = reduce(
        lambda a, b: a | b,
        [
            Q(id_par_result__id_par=p)
            for p in Parameter.objects.filter(pk__in=parameters)
        ],
    )
    sites = reduce(
        lambda a, b: a | b,
        [
            Q(id_sample__gid_point__gid_site=s)
            for s in Site.objects.filter(pk__in=stations)
        ],
    )
    flags = Q(val_flag__id_val_flag="A") | Q(val_flag__id_val_flag="B")
    query = flags & sites & params
    kwargs = {}
    if start is not None:
        kwargs["id_sample__sampling_date__gte"] = start
    if end is not None:
        kwargs["id_sample__sampling_date__lte"] = end

    queryset = (
        Results.objects.filter(query, **kwargs)
        .exclude(validated_txt_value="NA")
        .order_by("id_sample__sampling_date")
        .values(
            "id_sample__gid_point__gid_site",
            "id_par_result__id_par",
            "validated_txt_value",
            "id_sample__sampling_date",
            "loq_bid",
        )
    )

    results = {}
    for row in queryset:
        station = row["id_sample__gid_point__gid_site"]
        param = row["id_par_result__id_par"]
        sample_date = row["id_sample__sampling_date"]
        record = SurfaceRecord(row["validated_txt_value"], IdSample(sample_date))
        loq_bid = row["loq_bid"]
        if station not in results:
            results[station] = {}
        if param not in results[station]:
            results[station][param] = {"timeserie": [], "current_loq": 0.0, "loqs": []}

        results[station][param]["timeserie"].append(ser(record))

        current_loq = results[station][param]["current_loq"]
        if loq_bid is not None:
            value = float(loq_bid)
            if abs(value - current_loq) > 0.001:
                current_loq = value
                results[station][param]["loqs"].append(
                    {"date": sample_date, "value": current_loq}
                )
                results[station][param]["current_loq"]

    return results


def get_timeserie_surface(request, station, parameter):
    query = request.GET
    start = None
    end = None
    if "start" in query:
        try:
            start = date.fromisoformat(query["start"])
        except Exception:
            return HttpResponseBadRequest(
                'start date is not well formed: "{}"'.format(query["start"])
            )
    if "end" in query:
        try:
            end = date.fromisoformat(query["end"])
        except Exception:
            return HttpResponseBadRequest(
                'end date is not well formed: "{}"'.format(query["start"])
            )

    rows = get_timeserie_surface_data(station, parameter, start, end)

    return JsonResponse(rows, safe=False)


def get_layer_surface(request):
    Site = models.surface.Site
    results = json.loads(
        serialize(
            "geojson",
            Site.objects.filter(sdi_sites=True),
            geometry_field="the_geom",
            srid=31370,
        )
    )
    for feature in results["features"]:
        feature["id"] = feature["properties"]["pk"]

    return JsonResponse(results)


# to ease use of  serializers.ground.serialize_timeserie_record_quality
IdEch = namedtuple("IdEch", ["date_ech"])
GroundQualRecord = namedtuple(
    "GroundQualRecord", ["valeurtexte", "valeurnum", "idechantillon"]
)


def get_timeserie_ground_quality_data_batch(stations, parameters, start, end):
    from functools import reduce

    Results = models.ground.GwQualResultat
    Site = models.ground.GwMnetQualDesc
    Parameter = models.ground.GwQualParametre
    ser = serializers.ground.serialize_timeserie_record_quality
    params = reduce(
        lambda a, b: a | b,
        [Q(idparametre=p) for p in Parameter.objects.filter(pk__in=parameters)],
    )
    sites = reduce(
        lambda a, b: a | b,
        [
            Q(idechantillon__code_station=s.no_station)
            for s in Site.objects.filter(id_ouvrage__in=stations)
        ],
    )
    query = sites & params
    kwargs = {}
    if start is not None:
        kwargs["idechantillon__date_ech__gte"] = start
    if end is not None:
        kwargs["idechantillon__date_ech__lte"] = end

    queryset = (
        Results.objects.filter(query, **kwargs)
        .exclude(valeurtexte="NA")
        .order_by("idechantillon__date_ech")
        .values(
            "idechantillon__code_station",
            "idparametre",
            "idechantillon__date_ech",
            "valeurtexte",
            "valeurnum",
        )
    )

    results = {}
    for row in queryset:
        station = getattr(
            get_object_or_404(Site, no_station=row["idechantillon__code_station"]),
            "id_ouvrage",
        )
        param = row["idparametre"]
        sample_date = row["idechantillon__date_ech"]
        record = GroundQualRecord(
            row["valeurtexte"], row["valeurnum"], IdEch(sample_date)
        )
        if station not in results:
            results[station] = {}
        if param not in results[station]:
            results[station][param] = {"timeserie": []}

        results[station][param]["timeserie"].append(ser(record))
    return results


def get_timeserie_ground_quality_data(station, parameter, start, end):
    Results = models.ground.GwQualResultat
    Site = models.ground.GwMnetQualDesc
    Parameter = models.ground.GwQualParametre
    ser = serializers.ground.serialize_timeserie_record_quality
    param = get_object_or_404(Parameter, pk=parameter)
    site = get_object_or_404(Site, id_ouvrage=station)
    kwargs = dict(idparametre=param, idechantillon__code_station=site.no_station)
    if start is not None:
        kwargs["idechantillon__date_ech__gte"] = start
    if end is not None:
        kwargs["idechantillon__date_ech__lte"] = end

    queryset = Results.objects.filter(**kwargs).order_by("idechantillon__date_ech")

    return [ser(r) for r in queryset]


def get_timeserie_ground_quality(request, station, parameter):
    query = request.GET
    start = None
    end = None
    if "start" in query:
        try:
            start = date.fromisoformat(query["start"])
        except Exception:
            return HttpResponseBadRequest(
                'start date is not well formed: "{}"'.format(query["start"])
            )
    if "end" in query:
        try:
            end = date.fromisoformat(query["end"])
        except Exception:
            return HttpResponseBadRequest(
                'end date is not well formed: "{}"'.format(query["start"])
            )

    rows = get_timeserie_ground_quality_data(station, parameter, start, end)

    return JsonResponse(rows, safe=False)


def get_layer_ground_quality(request):
    Site = models.ground.GwMnetQualDesc
    results = json.loads(
        serialize(
            "geojson",
            Site.objects.filter(the_geom__isnull=False, confstatus="N"),
            geometry_field="the_geom",
            srid=31370,
        )
    )
    for feature in results["features"]:
        feature["id"] = feature["properties"]["id_ouvrage"]

    return JsonResponse(results)


def get_timeserie_ground_quantity_data(station, parameter, start, end):
    Site = models.ground.GwMnetPiezoQuantDesc
    code = getattr(get_object_or_404(Site, id_ouvrage=station), "mon_be_code")
    Results = models.ground.piezo_model(code)

    absolute = parameter == PIEZO_ABSOLUTE
    ser = serializers.ground.serialize_timeserie_record_quantity(code, absolute)
    prefix = models.ground.make_prefix("data_{}".format(code))
    time_field = prefix("time")
    value_field = prefix("hp") if absolute else prefix("pfsol")
    kwargs = {"{}__isnull".format(value_field): False}

    if start is not None:
        kwargs["{}__gte".format(time_field)] = start
    if end is not None:
        kwargs["{}__lte".format(time_field)] = end

    queryset = Results.objects.filter(**kwargs).order_by(time_field)

    return [ser(r) for r in queryset]


def get_timeserie_ground_quantity(request, station, parameter):
    if parameter not in [PIEZO_RELATIVE, PIEZO_ABSOLUTE]:
        return HttpResponseBadRequest(
            "piezo is either {} or {}".format(PIEZO_RELATIVE, PIEZO_ABSOLUTE)
        )
    query = request.GET
    start = None
    end = None
    if "start" in query:
        try:
            start = date.fromisoformat(query["start"])
        except Exception:
            return HttpResponseBadRequest(
                'start date is not well formed: "{}"'.format(query["start"])
            )
    if "end" in query:
        try:
            end = date.fromisoformat(query["end"])
        except Exception:
            return HttpResponseBadRequest(
                'end date is not well formed: "{}"'.format(query["start"])
            )

    rows = get_timeserie_ground_quantity_data(station, parameter, start, end)

    return JsonResponse(rows, safe=False)


def get_layer_ground_quantity(request):
    Site = models.ground.GwMnetPiezoQuantDesc
    results = json.loads(
        serialize(
            "geojson",
            Site.objects.filter(
                the_geom__isnull=False, mon_be_code__isnull=False, confstatus="N"
            ),
            geometry_field="the_geom",
            srid=31370,
        )
    )
    for feature in results["features"]:
        feature["id"] = feature["properties"]["id_ouvrage"]
        # feature["id"] = feature["properties"]["mon_be_code"]

    return JsonResponse(results)


class TSSelector:
    def __init__(self, timeserie):
        self.timeserie = timeserie
        self.index = 0

    def incr_index(self, t):
        """This function is a workaround for duplicate timestamps"""
        self.index += 1
        if self.index < len(self.timeserie):
            next_row = self.timeserie[self.index]
            next_t = next_row[0]
            if next_t == t:
                self.incr_index(t)

    def select(self, t):
        if self.index < len(self.timeserie):
            row = self.timeserie[self.index]
            if row[0] == t:
                self.incr_index(t)
                value, factor = row[1:3]
                return value if factor >= 1 else "<{}".format(value)
            else:
                _logger.debug(
                    "<select {}> index {}, at_index {}".format(t, self.index, row[0])
                )
                pass

        return ""


def merge_timeseries(timeseries):
    times = []
    for ts in timeseries:
        for record in ts:
            times.append(record[0])

    selectors = [TSSelector(ts) for ts in timeseries]
    results = []
    for t in sorted(set(times)):
        row = [t] + [s.select(t) for s in selectors]
        results.append(row)

    return results


def time_name(lang):
    if lang == "fr":
        return "Horodatage"
    elif lang == "nl":
        return "Tijdstempel"
    return "Timestamp"


def get_csv_header_surface(lang, parameters):
    def fname(n, unit, frac):
        if frac is not None:
            return f"{n} ({frac}) {unit}"
        return f"{n} {unit}"

    def param_name(p):
        parres = p.parresult_set.first()
        frac = parres.frac if parres is not None else None
        if lang == "fr":
            return fname(p.par_fr, p.unit_param, frac)
        elif lang == "nl":
            return fname(p.par_nl, p.unit_param, frac)
        return fname(p.par_en, p.unit_param, frac)

    Parameter = models.surface.Parameter
    params = [get_object_or_404(Parameter, pk=p) for p in parameters]
    return [time_name(lang)] + [param_name(p) for p in params]


def get_csv_header_ground_quality(lang, parameters):
    def param_name(p):
        if lang == "fr":
            return "{} {}".format(p.nomparametre, p.uniteparametre)
        elif lang == "nl":
            return "{} {}".format(p.naamparametre, p.uniteparametre)
        return "{} {}".format(p.name_parametre, p.uniteparametre)

    Parameter = models.ground.GwQualParametre
    params = [get_object_or_404(Parameter, pk=p) for p in parameters]
    return [time_name(lang)] + [param_name(p) for p in params]


PIEZO_ABS_NAMES = {
    "fr": "Niveau piézométrique absolu",
    "nl": "Absoluut piëzometrisch niveau",
}
PIEZO_REL_NAMES = {
    "fr": "Niveau piézométrique relatif",
    "nl": "Relatief piëzometrisch niveau",
}


def get_piezo_name(lang, p):
    if p == PIEZO_ABSOLUTE:
        return PIEZO_ABS_NAMES[lang]
    return PIEZO_REL_NAMES[lang]


def get_csv_header_ground_quantity(lang, parameters):
    return [time_name(lang)] + [get_piezo_name(lang, p) for p in parameters]


def with_lang(lang, obj):
    return obj[lang]


INCR_NAME_RE = re.compile(r".*_\d+$")


def incr_name(name):
    if INCR_NAME_RE.match(name):
        parts = name.split("_")
        count = int(parts[-1])
        return "{}_{}".format("_".join(parts[0:-1]), count + 1)

    return "{}_{}".format(name, 1)


def make_uniq_path(root, name, ext):
    sname = slugify(name, allow_unicode=True)
    p = root.joinpath("{}.{}".format(sname, ext))
    if p.exists():
        return make_uniq_path(root, incr_name(name), ext)
    return p


def get_surface_limit_of_quantification(site, param, start, end):
    Results = models.surface.Results
    kwargs = dict(
        id_par_result__id_par=param,
        id_sample__gid_point__gid_site=site,
    )
    if start is not None:
        kwargs["id_sample__sampling_date__gte"] = start
    if end is not None:
        kwargs["id_sample__sampling_date__lte"] = end

    queryset = (
        Results.objects.filter(**kwargs)
        .exclude(validated_txt_value="NA")
        .order_by("id_sample__sampling_date")
        .values("id_sample__sampling_date", "loq_bid")
    )

    results = []

    current_loq = 0.0
    for row in queryset:
        loq_bid = row["loq_bid"]
        if loq_bid is not None:
            value = float(loq_bid)
            if abs(value - current_loq) > 0.001:
                current_loq = value
                results.append(
                    {"date": row["id_sample__sampling_date"], "value": current_loq}
                )
    return results


def make_surface_meta(lang, root, start, end, station, timeseries):
    Site = models.surface.Site
    Parameter = models.surface.Parameter
    template_spec = "surface"
    site = Site.objects.get(pk=station)
    meta_path = make_uniq_path(root, site.gid_site, "html")

    context = dict(
        parameters=[],
        site=site,
        start=start,
        end=end,
    )

    stat_start = datetime.now()
    for timeserie, param_id in timeseries:
        if len(timeserie["timeserie"]) > 0:
            parameter = get_object_or_404(Parameter, pk=param_id)
            # loqs_start = datetime.now()
            # loqs = get_surface_limit_of_quantification(site, parameter, start, end)
            # _logger.debug("   $loqs", str(datetime.now() - loqs_start))
            get_start = datetime.now()
            statistics = get_statistics_surface(
                site,
                parameter,
                timeserie["timeserie"],
            )
            context["parameters"].append(
                dict(parameter=parameter, statistics=statistics, loqs=timeserie["loqs"])
            )
    render_start = datetime.now()
    rendered = render_to_string(
        "water/meta_{}_{}.html".format(template_spec, lang), context
    )

    with meta_path.open("x", encoding="utf8") as f:
        f.write(rendered)

    return meta_path


def make_ground_quality_meta(lang, root, start, end, station, timeseries):
    Commune = models.ground.GwOuvTcommune.objects.filter(code_postal=OuterRef("ref_cp"))
    Site = models.ground.GwMnetQualDesc.objects.annotate(
        commune=Subquery(Commune.values("commune")[:1])
    )
    Parameter = models.ground.GwQualParametre
    template_spec = "ground-quality"
    site = get_object_or_404(Site, id_ouvrage=station)
    meta_path = make_uniq_path(root, site.no_code, "html")

    context = dict(
        parameters=[],
        site=site,
        start=start,
        end=end,
    )

    for timeserie, param_id in timeseries:
        if len(timeserie["timeserie"]) > 0:
            parameter = get_object_or_404(Parameter, pk=param_id)
            statistics = get_statistics_ground_quality(
                site,
                parameter,
                timeserie["timeserie"],
            )
            context["parameters"].append(
                dict(parameter=parameter, statistics=statistics)
            )
    rendered = render_to_string(
        "water/meta_{}_{}.html".format(template_spec, lang), context
    )

    with meta_path.open("x", encoding="utf8") as f:
        f.write(rendered)

    return meta_path


def do_not_stat_piezo(station):
    Site = models.ground.GwMnetPiezoQuantDesc
    site = getattr(get_object_or_404(Site, id_ouvrage=station), "mon_be_code")
    exclude_list = getattr(
        settings, "WATER_EXCLUDE_PIEZO_STAT", []
    )  # this list contains the 'mon_be_code's of stations
    return site in exclude_list


def make_ground_quantity_meta(lang, root, start, end, station, timeseries):
    Commune = models.ground.GwOuvTcommune.objects.filter(code_postal=OuterRef("ref_cp"))
    Site = models.ground.GwMnetPiezoQuantDesc.objects.annotate(
        commune=Subquery(Commune.values("commune")[:1])
    )
    template_spec = "ground-quantity"
    site = get_object_or_404(Site, id_ouvrage=station)
    meta_path = make_uniq_path(root, site.nom, "html")
    has_statistics = not do_not_stat_piezo(station)
    today = date.today()
    context = dict(
        parameters=[],
        site=site,
        start=start,
        end=end,
        has_statistics=has_statistics,
        today=today,
    )

    for timeserie, param_id in timeseries:
        parameter = get_piezo_name(lang, param_id)
        statistics = (
            get_statistics_ground_quantity(
                site,
                timeserie,
            )
            if has_statistics
            else []
        )
        context["parameters"].append(dict(parameter=parameter, statistics=statistics))

    rendered = render_to_string(
        "water/meta_{}_{}.html".format(template_spec, lang), context
    )

    with meta_path.open("x", encoding="utf8") as f:
        f.write(rendered)

    return meta_path


# def tempdir():
#     root = PosixPath('/tmp')
#     name = 'water-' + ''.join(
#         random.choice(string.ascii_letters) for i in range(12))
#     tmp = root.joinpath(name)
#     tmp.mkdir()
#     return tmp


def package_csv(config, lang, get_name, get_timeserie, get_header, make_meta):
    stations = config.stations
    parameters = config.parameters
    start = config.start
    end = config.end
    station_names = []

    with tempfile.TemporaryDirectory() as dirname:
        root = PosixPath(dirname)
        sheets = []
        metas = []
        for station in stations:
            name = get_name(lang, station)
            station_names.append(name)
            sheet_path = make_uniq_path(root, name, "csv")
            timeseries = []
            for parameter in parameters:
                ts = get_timeserie(station, parameter, start, end)
                timeseries.append((ts, parameter))

            with sheet_path.open("x", encoding="utf8") as f:
                # to force Excel to use utf8 encoding, bom:'ufeff'
                f.write("\ufeff")
                writer = csv.writer(f, delimiter=";")
                writer.writerow(get_header(lang, parameters))
                writer.writerows(merge_timeseries([ts[0] for ts in timeseries]))

            sheets.append(sheet_path)

            metas.append(make_meta(lang, root, start, end, station, timeseries))

        # here me have to avoid too long names when many stations are selected
        extra = (
            ""
            if len(station_names) == 1
            else "-{}".format(
                "".join(map(lambda x: slugify(x, True)[0], station_names[1:]))
            )
        )
        zip_name = "{}{}_{}.zip".format(
            slugify(station_names[0], True), extra, date.today()
        )
        zip_path = root.joinpath(zip_name)
        with zipfile.ZipFile(zip_path.as_posix(), mode="x") as zip:
            for sheet_path in sheets:
                zip.write(
                    sheet_path.as_posix(), sheet_path.relative_to(root).as_posix()
                )
            for meta_path in metas:
                zip.write(meta_path.as_posix(), meta_path.relative_to(root).as_posix())

        return FileResponse(zip_path.open("rb"), as_attachment=True, filename=zip_name)


def package_csv_batch(config, lang, get_name, get_timeseries, get_header, make_meta):
    stations = config.stations
    parameters = config.parameters
    start = config.start
    end = config.end
    station_names = []

    all_timeseries = get_timeseries(stations, parameters, start, end)

    with tempfile.TemporaryDirectory() as dirname:
        root = PosixPath(dirname)
        sheets = []
        metas = []
        times = {}
        for station in stations:
            stime = {}
            station_start = datetime.now()
            name = get_name(lang, station)
            station_names.append(name)
            sheet_path = make_uniq_path(root, name, "csv")
            timeseries = []
            param_start = datetime.now()
            for parameter in parameters:
                # ts = get_timeserie(station, parameter, start, end)
                try:
                    ts = all_timeseries[station][parameter]
                    timeseries.append((ts, parameter))
                except KeyError:
                    timeseries.append(({"timeserie": [], "loqs": []}, parameter))
                    # _logger.debug(f"No Data For <station {station}> <parameter {parameter}>")

            stime["parameters"] = str(datetime.now() - param_start)

            open_start = datetime.now()
            close_start = None
            with sheet_path.open("x", encoding="utf8") as f:
                stime["open"] = str(datetime.now() - open_start)
                merge_start = datetime.now()
                merged = merge_timeseries([t[0]["timeserie"] for t in timeseries])
                stime["merge"] = str(datetime.now() - merge_start)
                write_start = datetime.now()
                # to force Excel to use utf8 encoding, bom:'ufeff'
                f.write("\ufeff")
                writer = csv.writer(f, delimiter=";")
                writer.writerow(get_header(lang, parameters))
                writer.writerows(merged)
                stime["write"] = str(datetime.now() - write_start)
                close_start = datetime.now()
            stime["close"] = str(datetime.now() - close_start)

            sheets.append(sheet_path)
            meta_start = datetime.now()
            metas.append(make_meta(lang, root, start, end, station, timeseries))
            stime["meta"] = str(datetime.now() - meta_start)
            stime["total"] = str(datetime.now() - station_start)
            times[station] = stime

        # _logger.debug(json.dumps(times, indent=2))

        # here me have to avoid too long names when many stations are selected
        extra = (
            ""
            if len(station_names) == 1
            else "-{}".format(
                "".join(map(lambda x: slugify(x, True)[0], station_names[1:]))
            )
        )
        zip_name = "{}{}_{}.zip".format(
            slugify(station_names[0], True), extra, date.today()
        )
        zip_path = root.joinpath(zip_name)
        with zipfile.ZipFile(zip_path.as_posix(), mode="x") as zip:
            for sheet_path in sheets:
                zip.write(
                    sheet_path.as_posix(), sheet_path.relative_to(root).as_posix()
                )
            for meta_path in metas:
                zip.write(meta_path.as_posix(), meta_path.relative_to(root).as_posix())

        return FileResponse(zip_path.open("rb"), as_attachment=True, filename=zip_name)


def get_surface_name(lang, station):
    site = models.surface.Site.objects.get(pk=station)
    # return with_lang(lang, {
    #     'fr': site.site_fr,
    #     'nl': site.site_nl,
    #     'en': site.site_en,
    # })
    # https://gitlab.com/atelier-cartographique/be-lb/water-project/-/issues/124
    return site.gid_site


def get_ground_qual_name(lang, station):
    site = get_object_or_404(models.ground.GwMnetQualDesc, id_ouvrage=station)
    return site.no_code


def get_ground_quant_name(lang, station):
    site = get_object_or_404(models.ground.GwMnetPiezoQuantDesc, id_ouvrage=station)
    return site.nom


def get_csv(request, lang, config):
    parsed = parse_config(config)
    kind = parsed.kind
    if kind == "surface":
        return package_csv_batch(
            parsed,
            lang,
            get_surface_name,
            get_timeserie_surface_data_batch,
            get_csv_header_surface,
            make_surface_meta,
        )
    elif kind == "ground-quality":
        return package_csv_batch(
            parsed,
            lang,
            get_ground_qual_name,
            get_timeserie_ground_quality_data_batch,
            get_csv_header_ground_quality,
            make_ground_quality_meta,
        )
    elif kind == "ground-quantity":
        return package_csv(
            parsed,
            lang,
            get_ground_quant_name,
            get_timeserie_ground_quantity_data,
            get_csv_header_ground_quantity,
            make_ground_quantity_meta,
        )

    return HttpResponseBadRequest('Unknown kind: "{}"'.format(kind))


def get_norm_names(request):
    records = models.surface.NormNames.objects.all()
    result = {}
    for rec in records:
        name = rec.norm_names
        if name is not None:
            result[name] = {
                "fr": rec.fr if not None else name,
                "nl": rec.nl if not None else name,
                "en": rec.en if not None else name,
            }

    return JsonResponse(result)


def get_water_bodies(request):
    ground_records = models.ground.GwHgeolTgwb.objects.all()
    surface_records = models.surface.Waterbody.objects.all()
    result = []
    for rec in ground_records:
        result.append(
            {
                "kind": "ground",
                "id": rec.id_gwb,
                "name": {
                    "fr": rec.nom_gwb_fr,
                    "nl": rec.naam_gwb_nl,
                },
                "code": rec.code_gwb,
            }
        )
    for rec in surface_records:
        result.append(
            {
                "kind": "surface",
                "id": rec.gid,
                "name": {
                    "fr": rec.waterbody_fr,
                    "nl": rec.waterbody_nl,
                },
                "code": rec.id_waterbody,
            }
        )

    return JsonResponse(result, safe=False)


def get_hydro_info(request):
    map_id = getattr(settings, "WATER_HYDRO_MAP", None)
    if map_id is None:
        return HttpResponseNotFound("Misconfigured App")
    user_map = get_object_or_404(UserMap, pk=map_id)
    ser = LayerInfoSerializer(user_map.layers, many=True)
    return JsonResponse(ser.data, safe=False)


def get_hydro_layer(request, metadata_id):
    """we bypass normal layer loading to allow
    non published map to be used here.
    It only supports postgis loader
    """
    map_id = getattr(settings, "WATER_HYDRO_MAP", None)
    if map_id is None:
        return HttpResponseNotFound("Misconfigured App")
    user_map = get_object_or_404(UserMap, pk=map_id)
    allowed_mds = [str(l.metadata_id) for l in user_map.layers.all()]
    if metadata_id not in allowed_mds:
        return HttpResponseForbidden()
    metadata = get_object_or_404(MetaData, pk=metadata_id)

    from postgis_loader.serializer import get_geojson

    url = urlparse(metadata.resource_identifier)
    schema = url.hostname
    table = url.path[1:]

    return JsonResponse(get_geojson(schema, table))


def get_station_parameter_surface_count(request, station):
    Results = models.surface.Results
    Site = models.surface.Site
    site = get_object_or_404(Site, pk=station)
    args = [Q(val_flag__id_val_flag="A") | Q(val_flag__id_val_flag="B")]

    result = dict(tag="count", parameters=dict())
    kwargs = dict(
        id_sample__gid_point__gid_site=site,
    )
    queryset = Results.objects.filter(*args, **kwargs).exclude(validated_txt_value="NA")

    counts = filter(
        lambda r: r["id_par_result__id_par"] is not None,
        queryset.values("id_par_result__id_par").annotate(
            count=Count("id_par_result__id_par")
        ),
    )
    result["parameters"] = {
        count["id_par_result__id_par"]: count["count"] for count in counts
    }

    return JsonResponse(result)


def get_station_surface_count(request, parameter):
    Results = models.surface.Results
    Parameter = models.surface.Parameter
    param = get_object_or_404(Parameter, pk=parameter)
    args = [Q(val_flag__id_val_flag="A") | Q(val_flag__id_val_flag="B")]

    result = dict(tag="count", stations=dict())

    kwargs = dict(
        id_par_result__id_par=param,
    )
    queryset = Results.objects.filter(*args, **kwargs).exclude(validated_txt_value="NA")

    counts = filter(
        lambda r: r["id_sample__gid_point__gid_site"] is not None,
        queryset.values("id_sample__gid_point__gid_site").annotate(
            count=Count("id_sample__gid_point__gid_site")
        ),
    )
    result["stations"] = {
        count["id_sample__gid_point__gid_site"]: count["count"] for count in counts
    }

    return JsonResponse(result)


def get_station_parameter_ground_count(request, station):
    Results = models.ground.GwQualResultat
    Site = models.ground.GwMnetQualDesc
    site = get_object_or_404(Site, id_ouvrage=station)

    result = dict(tag="count", parameters=dict())

    kwargs = dict(idechantillon__code_station=site.no_station)
    queryset = Results.objects.filter(**kwargs).exclude(valeurtexte="NA")

    counts = filter(
        lambda r: r["idparametre"] is not None,
        queryset.values("idparametre").annotate(count=Count("idparametre")),
    )

    result["parameters"] = {count["idparametre"]: count["count"] for count in counts}

    return JsonResponse(result)


def lookup_site_id(sites):
    def inner(code_station):
        for no_station, id_ouvrage in sites:
            if no_station == code_station:
                return id_ouvrage
        raise Exception(f"{code_station} is not a site")

    return inner


def get_station_ground_count(request, parameter):
    Results = models.ground.GwQualResultat
    Parameter = models.ground.GwQualParametre
    Site = models.ground.GwMnetQualDesc
    param = get_object_or_404(Parameter, pk=parameter)

    result = dict(tag="count", stations=dict())

    kwargs = dict(
        idparametre=param,
    )
    queryset = Results.objects.filter(**kwargs).exclude(valeurtexte="NA")

    counts = filter(
        lambda r: r["idechantillon__code_station"] is not None,
        queryset.values("idechantillon__code_station").annotate(
            count=Count("idechantillon__code_station")
        ),
    )
    result["stations"] = {}

    id_ouvrage = lookup_site_id(
        [(s.no_station, s.id_ouvrage) for s in Site.objects.all()]
    )
    # for count in counts:
    #     result["stations"][pk(count["idechantillon__code_station"])] = count["count"]
    result["stations"] = {
        id_ouvrage(count["idechantillon__code_station"]): count["count"]
        for count in counts
    }

    return JsonResponse(result)
