from pathlib import PosixPath
import sys


def add_loader(name, loader_root, apps):
    root = PosixPath(__file__).parent.joinpath(loader_root).resolve().as_posix()
    sys.path.append(root)
    apps.append(name)


def add_angled(apps):
    add_loader("angled", "angled_app", apps)


def add_asf(apps):
    add_loader("asf", "asf", apps)


def add_geothermie(apps):
    add_loader("geothermie", "geothermie", apps)


def add_infiltration(apps):
    add_loader("infiltration", "infiltration", apps)


def add_mdq(apps):
    add_loader("mdq", "mdq", apps)


def add_postgis(apps):
    add_loader("postgis_loader", "postgis", apps)


def add_proxy(apps):
    add_loader("proxy", "proxy", apps)


def add_safe(apps):
    add_loader("safe", "safe", apps)


def add_sgpa(apps):
    add_loader("sgpa", "sgpa", apps)


def add_sitex(apps):
    add_loader("sitex", "sitex", apps)


def add_solar(apps):
    add_loader("solar_loader", "solar", apps)


def add_water(apps):
    add_loader("water", "water", apps)
