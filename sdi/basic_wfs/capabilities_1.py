from api.models.metadata import MetaData
from django.urls import reverse

from .xml import (
    append,
    create,
    create_tree,
    tree_to_string,
)

CONSTRAINTS = [
    "ImplementsBasicWFS",
    "ImplementsTransactionalWFS",
    "ImplementsLockingWFS",
    "KVPEncoding",
    "XMLEncoding",
    "SOAPEncoding",
    "ImplementsInheritance",
    "ImplementsRemoteResolve",
    "ImplementsResultPaging",
    "ImplementsStandardJoins",
    "ImplementsSpatialJoins",
    "ImplementsTemporalJoins",
    "ImplementsFeatureVersioning",
    "ManageStoredQueries",
]


BASIC_CONFORMANCE = ["ImplementsBasicWFS", "KVPEncoding"]

FILTER_CONSTRAINTS = [
    "ImplementsQuery",
    "ImplementsAdHocQuery",
    "ImplementsFunctions",
    "ImplementsMinStandardFilter",
    "ImplementsStandardFilter",
    "ImplementsMinSpatialFilter",
    "ImplementsSpatialFilter",
    "ImplementsMinTemporalFilter",
    "ImplementsTemporalFilter",
    "ImplementsVersionNav",
    "ImplementsSorting",
    "ImplementsExtendedOperators",
]

IMPLEMENTED_FILTER_CONSTRAINTS = ["ImplementsQuery"]


def capabilities():
    attributes = {
        "version": "1.0.0",
        "xmlns:ows": "http://www.opengis.net/ows",
        "xmlns:ogc": "http://www.opengis.net/ogc",
        "xmlns:wfs": "http://www.opengis.net/wfs",
        "xmlns:gml": "http://www.opengis.net/gml",
        "xmlns:xlink": "http://www.w3.org/1999/xlink",
        "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "xsi:schemaLocation": "http://www.opengis.net/wfs",
    }
    return create("WFS_Capabilities", attributes)


def service_identification(capabilities):
    ident = append(capabilities, "ows:ServiceIdentification")
    append(ident, "ows:Title").text = "carto-station"
    append(ident, "ows:Abstract").text = "carto-station"
    append(ident, "ows:ServiceType").text = "WFS"
    append(ident, "ows:ServiceTypeVersion").text = "1.0.0"
    append(ident, "ows:Fees").text = "NONE"
    append(ident, "ows:AccessConstraints").text = "NONE"


def service_provider(capabilities):
    ident = append(capabilities, "ows:ServiceProvider")
    # TODO:should we add more information ? website, email, ...
    # TODO:should provider info be filled based on a configuration ?
    append(ident, "ows:ProviderName").text = "carto-station"
    append(ident, "ows:ProviderSite").text = "bruxelles"


def make_operation_metadata(metadata, name, request):
    operation = append(metadata, "ows:Operation", dict(name=name))
    dcp = append(operation, "ows:DCP")

    http = append(dcp, "ows:HTTP")
    append(http, "ows:Get", {"xlink:href": request.get_raw_uri()})


def make_operation_parameter(metadata, name, values):
    parameter = append(metadata, "ows:Parameter", dict(name=name))
    for value in values:
        append(parameter, "ows:Value").text = value


def make_constraint(metadata, name, default):
    constraint = append(metadata, "ows:Constraint", dict(name=name))
    append(constraint, "ows:NoValues")
    append(constraint, "ows:DefaultValue").text = default


def operations_metadata(capabilities, request):
    metadata = append(capabilities, "ows:OperationsMetadata")
    make_operation_metadata(metadata, "GetCapabilities", request)
    make_operation_metadata(metadata, "DescribeFeatureType", request)
    make_operation_metadata(metadata, "GetFeature", request)
    make_operation_parameter(
        metadata, "srsName", ["EPSG:http://www.opengis/net/def/crs/epsg/0/31370"]
    )


def metadata_to_feature(ftl, metadata, request):
    feature = append(ftl, "wfs:FeatureType")
    append(feature, "wfs:Name").text = metadata.resource_identifier
    append(feature, "wfs:Title").text = metadata.title.fr
    append(feature, "wfs:Abstract").text = metadata.abstract.fr
    append(
        feature, "wfs:DefaultCRS"
    ).text = "http://www.opengis.net/def/crs/epsg/0/31370"
    append(feature, "wfs:MetadataURL").text = request.build_absolute_uri(
        reverse("metadata-detail", args=(metadata.id,))
    )
    append(
        append(feature, "wfs:OutputFormats"), "wfs:Format"
    ).text = "text/xml; subtype=gml/3.1.1"


def feature_type_list(capabilities, request):
    ftl = append(capabilities, "wfs:FeatureTypeList")
    for metadata in MetaData.objects.all():
        metadata_to_feature(ftl, metadata, request)


def filter_capabilities(capabilities):
    append(capabilities, "ogc:Filter_Capabilities")


def get_capabilities(request):
    # TODO: create nodes in sub-functions and append here instead
    #       of appending in place
    # TODO: add verb to functions if not rewritten in declarative style
    capabilities_ = capabilities()
    service_identification(capabilities_)
    service_provider(capabilities_)
    operations_metadata(capabilities_, request)
    feature_type_list(capabilities_, request)
    filter_capabilities(capabilities_)

    return tree_to_string(create_tree(capabilities_))
