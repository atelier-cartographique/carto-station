from base64 import b64decode
import logging

from django.conf import settings
from django.contrib.auth import authenticate
from django.http import HttpResponse, HttpResponseBadRequest
from django.http.response import HttpResponseNotFound
from django.views import View

from .capabilities_2 import get_capabilities
from .geodata import feature_loader, schema_loader
from .lingua import LinguaWFS
from .xml import typename_ns_to_rid

LANGUAGES = dict(getattr(settings, "LANGUAGES"))
_logger = logging.getLogger(__name__)


def attach_user(request):
    auth = request.META["HTTP_AUTHORIZATION"]
    auth_type, auth_string = auth.split(" ")
    if auth_type.lower() == "basic":
        decoded = b64decode(auth_string).decode("utf-8")
        username, password = decoded.split(":", 1)
        user = authenticate(username=username, password=password)
        _logger.info("Auth {} -> {}".format(username, user))

        if user is not None:
            request.user = user
        else:
            _logger.warning(f"failed auth attempt for {username}")
            raise Exception("Could not authenticate user.")
    else:
        raise NotImplementedError("Expect basic authentication.")


BASIC_AUTH_REQUIRED = """
<html>
    <head>
        <title>Basic auth required</title>
    </head>
    <body>
        <h1>Authorization Required</h1>
        <div>{message}</div>
    </body>
</html>
"""


class HttpResponseUnauthorized(HttpResponse):
    status_code = 401

    def __init__(self, message):
        super(HttpResponseUnauthorized, self).__init__(
            BASIC_AUTH_REQUIRED.format(message=message).encode("utf-8"),
        )
        self["WWW-Authenticate"] = 'Basic realm="CS", charset="UTF-8"'


class WFS(View):
    cap_req_parms_m = [
        LinguaWFS.Request.service,
    ]
    cap_req_parms_o = [
        LinguaWFS.Request.version,
        LinguaWFS.Request.format,
    ]

    def get_capabilities(self, request, lang):
        query = request.GET
        params = LinguaWFS.Request.gets(query, WFS.cap_req_parms_m)
        params.update(LinguaWFS.Request.gets(query, WFS.cap_req_parms_o, False))

        caps = get_capabilities(request, lang)
        return HttpResponse(caps.encode("utf-8"), content_type="application/xml")

    def describe_feature_type(self, request, lang):
        typename = LinguaWFS.Request.get(request.GET, LinguaWFS.Request.typename)
        ft = schema_loader.get_schema(request, lang, typename_ns_to_rid(typename))
        return HttpResponse(ft, content_type="application/xml")

    def get_feature(self, request, lang):
        typenames = LinguaWFS.Request.get(request.GET, LinguaWFS.Request.typenames)
        ft = feature_loader.get_feature(request, lang, typename_ns_to_rid(typenames))
        return HttpResponse(ft, content_type="application/xml")

    def get(self, request, lang):
        if lang not in LANGUAGES:
            message = "language not found".encode("utf-8")
            return HttpResponseNotFound(message)

        wfs_req = LinguaWFS.Request.get(request.GET, LinguaWFS.Request.request)
        try:
            attach_user(request)
        except Exception as e:
            return HttpResponseUnauthorized(str(e))

        # GET /basic-wfs/fr?SERVICE=WFS&VERSION=2.0.0&REQUEST=GetCapabilities
        if LinguaWFS.Token.GetCapabilities == wfs_req:
            return self.get_capabilities(request, lang)

        # GET /basic-wfs/fr?SERVICE=WFS&VERSION=2.0.0&REQUEST=DescribeFeatureType&typeName=topp:tasmania_roads
        elif LinguaWFS.Token.DescribeFeatureType == wfs_req:
            return self.describe_feature_type(request, lang)

        # GET /basic-wfs/fr?SERVICE=WFS&VERSION=2.0.0&REQUEST=GetFeature&typeNames=topp:tasmania_roads&featureID=tasmania_roads.875
        # GET /basic-wfs/fr?SERVICE=WFS&VERSION=2.0.0&REQUEST=GetFeature&typeNames=topp:tasmania_roads&BBOX=145192.74620387257891707,173434.9701656376128085,147169.40428129138308577,178144.50778191193239763
        elif LinguaWFS.Token.GetFeature == wfs_req:
            return self.get_feature(request, lang)

        return HttpResponseBadRequest("Wrong operation request".encode())
