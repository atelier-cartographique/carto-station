import logging
from urllib.parse import urlparse

from api.models.metadata import MetaData
from django.urls import reverse

from .geodata import feature_loader, schema_loader
from .xml import (
    append,
    create,
    create_tree,
    rid_to_typename_ns,
    tree_to_string,
)

CONSTRAINTS = [
    "ImplementsBasicWFS",
    "ImplementsTransactionalWFS",
    "ImplementsLockingWFS",
    "KVPEncoding",
    "XMLEncoding",
    "SOAPEncoding",
    "ImplementsInheritance",
    "ImplementsRemoteResolve",
    "ImplementsResultPaging",
    "ImplementsStandardJoins",
    "ImplementsSpatialJoins",
    "ImplementsTemporalJoins",
    "ImplementsFeatureVersioning",
    "ManageStoredQueries",
]

BASIC_CONFORMANCE = ["ImplementsBasicWFS", "KVPEncoding"]

FILTER_CONSTRAINTS = [
    "ImplementsQuery",
    "ImplementsAdHocQuery",
    "ImplementsFunctions",
    "ImplementsMinStandardFilter",
    "ImplementsStandardFilter",
    "ImplementsMinSpatialFilter",
    "ImplementsSpatialFilter",
    "ImplementsMinTemporalFilter",
    "ImplementsTemporalFilter",
    "ImplementsVersionNav",
    "ImplementsSorting",
    "ImplementsExtendedOperators",
]

IMPLEMENTED_FILTER_CONSTRAINTS = ["ImplementsQuery"]

_logger = logging.getLogger(__name__)

# example xml at
# http://docs.opengeospatial.org/is/09-025r2/09-025r2.html


def capabilities():
    attributes = {
        "version": "2.0.0",
        "xmlns": "http://www.opengis.net/wfs/2.0",
        "xmlns:gml": "http://www.opengis.net/gml/3.2",
        "xmlns:fes": "http://www.opengis.net/fes/2.0",
        "xmlns:xlink": "http://www.w3.org/1999/xlink",
        "xmlns:ows": "http://www.opengis.net/ows/1.1",
        "xmlns:xsd": "http://www.w3.org/2001/XMLSchema",
        "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "xsi:schemaLocation": "http://www.opengis.net/wfs/2.0 http://schemas.opengis.net/wfs/2.0.02.0/wfs.xsd http://www.opengis.net/ows/1.1 http://schemas.opengis.net/ows/1.1.0/owsAll.xsd",
    }
    return create("WFS_Capabilities", attributes)


def service_identification(capabilities):
    identification = append(capabilities, "ows:ServiceIdentification")
    append(identification, "ows:Title").text = "carto-station"
    append(identification, "ows:Abstract").text = "carto-station"
    append(identification, "ows:ServiceType").text = "WFS"
    append(identification, "ows:ServiceTypeVersion").text = "2.0.0"
    append(identification, "ows:ServiceTypeVersion").text = "2.0.0"
    append(identification, "ows:Fees").text = "NONE"
    append(identification, "ows:AccessConstraints").text = "NONE"


def service_provider(capabilities):
    provider = append(capabilities, "ows:ServiceProvider")
    append(provider, "ows:ProviderName").text = "carto-station"
    append(provider, "ows:ProviderSite").text = "bruxelles"


def make_operation_metadata(metadata, name, request):
    operation = append(metadata, "ows:Operation", dict(name=name))
    dcp = append(operation, "ows:DCP")

    http = append(dcp, "ows:HTTP")
    append(http, "ows:Get", {"xlink:href": request.get_raw_uri()})


def make_operation_parameter(metadata, name, values):
    parameter = append(metadata, "ows:Parameter", dict(name=name))
    allowed_values = append(parameter, "ows:AllowedValues")
    for value in values:
        append(allowed_values, "ows:Value").text = value


def make_constraint(metadata, name, default):
    constraint = append(metadata, "ows:Constraint", dict(name=name))
    append(constraint, "ows:NoValues")
    append(constraint, "ows:DefaultValue").text = default


def operations_metadata(capabilities, request):
    metadata = append(capabilities, "ows:OperationsMetadata")
    make_operation_metadata(metadata, "GetCapabilities", request)
    make_operation_metadata(metadata, "DescribeFeatureType", request)
    make_operation_metadata(metadata, "ListStoredQueries", request)
    make_operation_metadata(metadata, "DescribeStoredQueries", request)
    make_operation_metadata(metadata, "GetFeature", request)
    make_operation_parameter(metadata, "version", ["2.0.0", "2.0.0"])
    for con in CONSTRAINTS:
        val = "TRUE" if con in BASIC_CONFORMANCE else "FALSE"
        make_constraint(metadata, con, val)


def metadata_to_feature(ftl, metadata, request, lang):
    rid = metadata.resource_identifier
    try:
        schema_loader.get_loader(urlparse(rid).scheme)
        feature_loader.get_loader(urlparse(rid).scheme)
        feature = append(ftl, "FeatureType")
        append(feature, "Name").text = rid_to_typename_ns(rid)
        append(feature, "Title").text = getattr(metadata.title, lang)
        append(feature, "Abstract").text = getattr(metadata.abstract, lang)
        append(
            feature, "DefaultCRS"
        ).text = "http://www.opengis.net/def/crs/epsg/0/31370"
        append(feature, "MetadataURL").text = request.build_absolute_uri(
            reverse("metadata-detail", args=(metadata.id,))
        )
    except Exception as e:
        _logger.debug(str(e))
        pass


def feature_type_list(capabilities, request, lang):
    ftl = append(capabilities, "FeatureTypeList")
    for metadata in MetaData.objects.all():
        metadata_to_feature(ftl, metadata, request, lang)


def filter_capabilities(capabilities):
    fes = append(capabilities, "fes:Filter_Capabilities")
    conf = append(fes, "fes:Conformance")

    for con in FILTER_CONSTRAINTS:
        val = "TRUE" if con in IMPLEMENTED_FILTER_CONSTRAINTS else "FALSE"
        make_constraint(conf, con, val)


def get_capabilities(request, lang):
    # TODO: do not modify in place, use declarative style
    capabilities_ = capabilities()
    service_identification(capabilities_)
    service_provider(capabilities_)
    operations_metadata(capabilities_, request)
    feature_type_list(capabilities_, request, lang)
    filter_capabilities(capabilities_)

    return tree_to_string(create_tree(capabilities_))
