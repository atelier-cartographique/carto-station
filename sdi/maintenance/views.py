from datetime import datetime
from django.shortcuts import render
from .models import MaintenanceEvent


def get_index(request):
    now = datetime.now()
    events = MaintenanceEvent.objects.filter(start__lt=now, end__gt=now)

    return render(request, "maintenance/index.html", {"event": events[0]})
