from django.urls import path
from .views import get_index

urlpatterns = [
    path("index.html", get_index, name="maintenance.index"),
]
