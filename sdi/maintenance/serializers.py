from datetime import datetime
from .models import AppMessage, MaintenanceEvent


def serialize_datetime(dt: datetime):
    return int(dt.timestamp() * 1000)


def serialize_appmessage(app_message: AppMessage):
    return {
        "message": app_message.app_announcement,
        # "message": {
        #     "fr": app_message.app_announcement.fr,
        #     "nl": app_message.app_announcement.nl,
        # },
        # "event": app_message.event,
        "app": app_message.app,
        "start": serialize_datetime(app_message.start),
        "end": serialize_datetime(app_message.end),
    }


def serialize_maintenance_event(event: MaintenanceEvent):
    return {
        "announcement": event.announcement,
        "start": serialize_datetime(event.start),
        "end": serialize_datetime(event.end),
        # "apps": [serialize_appmessage(m) for m in event.appmessage_set.all()],
    }
