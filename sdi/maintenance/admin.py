from django.contrib import admin
from .models import MaintenanceEvent, AppMessage


class AppMessageInline(admin.StackedInline):
    model = AppMessage


class MaintenanceEventAdmin(admin.ModelAdmin):
    fields = ("start", "end", "announcement_date", "announcement")
    list_display = ["start", "end", "announcement_date", "announcement"]
    inlines = (AppMessageInline,)


admin.site.register(MaintenanceEvent, MaintenanceEventAdmin)
