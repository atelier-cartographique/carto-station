from datetime import datetime
from django.db import models
from django.db.models.query import QuerySet
from lingua.fields import lingua_string, nullable_text_field, text_field
from clients.models import ClientConfig


class AnnouncementManager(models.Manager):
    def announced(self) -> QuerySet:
        now = datetime.now()
        return self.get_queryset().filter(start__gt=now, announcement_date__lt=now)


class MaintenanceEvent(models.Model):
    class Meta:
        ordering = ["-end"]

    objects = AnnouncementManager()

    announcement = nullable_text_field("event_announcement")
    announcement_date = models.DateTimeField(verbose_name="Announcement start")
    start = models.DateTimeField(verbose_name="Maintenance start")
    end = models.DateTimeField(verbose_name="Maintenance end")

    def __str__(self) -> str:
        start = self.start.isoformat()
        return f"{start} # {self.announcement}"


def from_item_to_app(route, item):
    display_name_rec = item.get("display")["name"]
    if display_name_rec is not None:
        display_name = lingua_string(display_name_rec)
    else:
        display_name = route

    return (route, display_name)


class AppMessageManager(models.Manager):
    def announced(self) -> QuerySet:
        now = datetime.now()
        return self.get_queryset().filter(start__lt=now, end__gt=now)


class AppMessage(models.Model):
    APP_CHOICES = [
        from_item_to_app(route, info) for route, info in ClientConfig.get_items()
    ]

    class Meta:
        ordering = ["-end"]

    objects = AppMessageManager()

    app_announcement = text_field("app_message")
    app = models.CharField(max_length=256, choices=APP_CHOICES)
    event = models.ForeignKey(MaintenanceEvent, on_delete=models.CASCADE)
    start = models.DateTimeField(verbose_name="Announcement start")
    end = models.DateTimeField(verbose_name="Announcement end")
