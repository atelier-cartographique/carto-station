from datetime import datetime
from django.conf import settings
from django.contrib.auth.models import Group, User
from django.urls import resolve

from .models import MaintenanceEvent
from .views import get_index


MAINTENANCE_GROUP_NAME = getattr(settings, "MAINTENANCE_GROUP_NAME", None)


def events(get_response):
    group = None
    if MAINTENANCE_GROUP_NAME is not None:
        try:
            group = Group.objects.get(name=MAINTENANCE_GROUP_NAME)
        except Group.DoesNotExist:
            pass

    def middleware(request):
        nonlocal group
        if group is not None:
            view, args, kwargs = resolve(request.path)

            if not view.__module__.startswith("django.contrib.admin"):
                now = datetime.now()
                if (
                    MaintenanceEvent.objects.filter(start__lt=now, end__gt=now).count()
                    > 0
                    and not User.objects.filter(
                        pk=request.user.id, groups__name=MAINTENANCE_GROUP_NAME
                    ).exists()
                ):
                    return get_index(request)
        elif MAINTENANCE_GROUP_NAME is not None:
            try:
                group = Group.objects.get(name=MAINTENANCE_GROUP_NAME)
            except Group.DoesNotExist:
                pass

        return get_response(request)

    return middleware
