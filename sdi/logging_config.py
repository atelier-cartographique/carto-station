import logging

# add
# `LOGGING = logging_config.SDI_LOGGING`
# to your settings.py to use this logging configuration


class ColoredFormatter(logging.Formatter):
    green = "\x1b[32;20m"
    blue = "\x1b[34;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"

    fmt = "%(asctime)s {color}%(levelname)s{reset} %(name)s: %(message)s"

    FORMATS = {
        logging.DEBUG: fmt.format(color=green, reset=reset)
        + " (%(filename)s:%(lineno)d)",
        logging.INFO: fmt.format(color=blue, reset=reset),
        logging.WARNING: fmt.format(color=yellow, reset=reset),
        logging.ERROR: fmt.format(color=red, reset=reset),
        logging.CRITICAL: fmt.format(color=bold_red, reset=reset),
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)


SDI_LOGGING = {
    "version": 1,  # the dictConfig format version
    "disable_existing_loggers": False,  # retain the default loggers"
    "formatters": {
        "colored_formatter": {"class": "logging_config.ColoredFormatter"},
    },
    "filters": {
        "require_debug_false": {
            "()": "django.utils.log.RequireDebugFalse",
        },
        "require_debug_true": {
            "()": "django.utils.log.RequireDebugTrue",
        },
    },
    "handlers": {
        "console": {
            # "filters": ["require_debug_false"],
            "formatter": "colored_formatter",
            "class": "logging.StreamHandler",
        },
    },
    "loggers": {
        "": {
            "level": "DEBUG",
            "handlers": ["console"],
        },
    },
}
