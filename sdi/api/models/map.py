#
#  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import uuid

from django.conf import settings
from django.contrib.gis.db import models
from django.db.models import JSONField
from django.contrib.auth.models import User, Group
from webservice.models import WmsLayer
from django.contrib.postgres.fields import ArrayField


from lingua.fields import nullable_label_field, nullable_text_field, message
from .metadata import MetaData
from .map_link import MapLink

import logging

_logger = logging.getLogger(__name__)

LAYER_EXPORTABLE = getattr(settings, "LAYER_EXPORTABLE", True)


class Category(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = nullable_label_field("category_name")

    def __str__(self):
        return str(self.name)


class LayerGroup(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = nullable_label_field("layergroup_name")

    def update(self, data):
        self.name.update_record(**data)

    def clone(self):
        return LayerGroup.objects.create(name=self.name.clone())

    def __str__(self):
        return str(self.name)


class LayerInfo(models.Model):
    class Meta:
        ordering = ["user_map_layer__sort_index"]

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    metadata = models.ForeignKey(
        MetaData,
        on_delete=models.PROTECT,
        related_name="layer_info_md",
    )
    visible = models.BooleanField()

    style = JSONField()
    feature_view_options = JSONField()
    group = models.ForeignKey(
        LayerGroup,
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )

    legend = nullable_label_field("layer_legend_label")

    min_zoom = models.PositiveIntegerField(default=0, null=True, blank=True)
    max_zoom = models.PositiveIntegerField(default=30, null=True, blank=True)
    # True if legend is visible even if layer is not at a given zoom
    visible_legend = models.BooleanField(
        default=True, verbose_name="Keep legend visible when layer is not"
    )
    opacity_selector = models.BooleanField(
        default=False, verbose_name="Add opacity selector on layer"
    )
    visibility_switch = models.BooleanField(
        default=False, verbose_name="Add visibility button on layer categories"
    )  # visibility_switch: Add visibility switch button on layer categories (if true)
    exportable = models.BooleanField(
        default=LAYER_EXPORTABLE, verbose_name="Allow data export"
    )

    def update(self, data):
        self.metadata = data.pop("metadata")
        # DRF gives us a plain MetaData Model, weird
        self.visible = data.pop("visible")
        self.style = data.pop("style")
        self.feature_view_options = data.pop("feature_view_options")
        self.min_zoom = data.pop("min_zoom", 0)
        self.max_zoom = data.pop("max_zoom", 30)
        self.visible_legend = data.pop("visible_legend", True)
        self.exportable = data.pop("exportable", LAYER_EXPORTABLE)
        self.opacity_selector = data.pop("opacity_selector", False)
        self.visibility_switch = data.pop("visibility_switch", False)
        legend = data.pop("legend", None)
        if legend is not None:
            if self.legend is not None:
                self.legend.update_record(**legend)
            else:
                self.legend = message("layer_legend_label", **legend)

        self.save()

    def clone(self):
        obj = LayerInfo(
            metadata=self.metadata,
            visible=self.visible,
            style=self.style,
            feature_view_options=self.feature_view_options,
            min_zoom=self.min_zoom,
            max_zoom=self.max_zoom,
            visible_legend=self.visible_legend,
            exportable=self.exportable,
            opacity_selector=self.opacity_selector,
            visibility_switch=self.visibility_switch,
        )

        if self.legend is not None:
            obj.legend = self.legend.clone()
        if self.group is not None:
            obj.group = self.group
        obj.save()

        return obj

    def __str__(self):
        m = self.usermap_set.first()
        if m is not None:
            return "{} # {}".format(str(m.title), str(self.metadata.title))
        return "None # {}".format(str(self.metadata.title))


# class LayerInfoExtraManager(models.Manager):
#     def create(self, **kwargs):
#         exportable = kwargs.get("exportable", LAYER_EXPORTABLE)
#         kwargs["exportable"] = exportable
#         return super().create(**kwargs)


class LayerInfoExtra(models.Model):
    layer_info = models.OneToOneField(
        LayerInfo,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    exportable = models.BooleanField()

    # objects = LayerInfoExtraManager()


class BaseLayer(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = nullable_label_field("base_layer_name")
    srs = models.TextField()
    params = JSONField()
    url = nullable_label_field("base_layer_url")

    def __str__(self):
        return str(self.name)


def get_default_base_layer():
    base = BaseLayer.objects.first()
    if not base:
        default_base_dict = settings.DEFAULT_BASE_LAYER
        name = message("base_layer_name", **default_base_dict["name"])
        url = message("base_layer_url", **default_base_dict["url"])
        srs = default_base_dict["srs"]
        params = default_base_dict["params"]
        return BaseLayer.objects.create(
            name=name,
            srs=srs,
            params=params,
            url=url,
        )
    return base


class HighlightedBaseLayer(models.Model):
    class Meta:
        ordering = ["sort_index"]

    id = models.AutoField(primary_key=True)
    name = nullable_label_field("highlighted_layer_name")
    img = models.ImageField(upload_to="baselayers")
    sort_index = models.IntegerField()
    baselayer = models.ForeignKey(
        WmsLayer,
        on_delete=models.PROTECT,
        related_name="highlighted_baselayer",
    )


class UserMapManager(models.Manager):
    def create_map(
        self,
        id_origin,
        user,
        title_data,
        description_data,
        base_layer_list,
        base_layer_selection,
        image_url=None,
        extent=None,
    ):
        instance = UserMap.objects.create(
            id_origin=id_origin,
            user=user,
            title=message("map_title", **title_data),
            description=message("map_description", **description_data),
            base_layer_selection=base_layer_selection,
            image_url=image_url,
            extent=extent,
        )
        (instance.base_layer_list.set(base_layer_list),)
        return instance


class UserMap(models.Model):
    DRAFT = "draft"
    PUBLISHED = "published"
    SAVED = "saved"
    STATUS_CHOICES = ((DRAFT, "Draft"), (PUBLISHED, "Published"), (SAVED, "Saved"))

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    id_origin = models.UUIDField(editable=False)
    last_modified = models.DateTimeField(auto_now=True, editable=False)
    status = models.CharField(
        max_length=32,
        choices=STATUS_CHOICES,
        default=DRAFT,
    )
    user = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        related_name="maps",
        default=0,
    )

    title = nullable_label_field("map_title")
    description = nullable_text_field("map_description")

    image_url = models.CharField(
        max_length=512,
        null=True,
        blank=True,
    )
    categories = models.ManyToManyField(Category, through="CategoryLink")
    layers = models.ManyToManyField(LayerInfo, through="LayerLink")

    # default baselayers of the map
    base_layer_list = models.ManyToManyField(WmsLayer, through="api.BaseLayerLink")

    # baselayers that the user can select for this map (in the "View" app, in the "data" tab)
    # array of WmsLayerOrGroupLink ids
    base_layer_selection = ArrayField(base_field=models.IntegerField(), default=list)

    collaborators = models.ManyToManyField(
        User, related_name="maps_delegated", blank=True
    )

    extent = models.PolygonField(
        dim=2,
        srid=31370,
        null=True,
        blank=True,
    )

    objects = UserMapManager()

    class Meta:
        ordering = ["-last_modified"]
        unique_together = ["status", "id_origin"]

    @property
    def attachments(self):
        return [l.attachment for l in self.attachment_links.all()]

    def update_id_origin(self, id_origin):
        self.id_origin = id_origin

    # def update_status(self, status):
    #     self.status = status  # could be a nice to send notifications

    def update_title(self, data):
        self.title.update_record(**data)

    def update_description(self, data):
        self.description.update_record(**data)

    def update_image(self, image_url=None):
        self.image_url = image_url

    def update_categories(self, data=[]):
        # _logger.debug(f"cat: {data}")
        self.categories.clear()
        self.save()
        for cat in data:
            # cat = Category.objects.get(id=cat_id)
            CategoryLink.objects.create(category=cat, user_map=self)

    def add_collaborators(self, data):
        for col in data:
            self.collaborators.add(col.id)

    def update_layers(self, layers=[]):
        ids = [layer.id for layer in layers]
        for layer in self.layers.all():
            if layer.id not in ids:
                layer.delete()

        self.layers.clear()

        for idx, layer in enumerate(layers):
            try:
                LayerLink.objects.create(
                    layer=layer,
                    user_map=self,
                    sort_index=idx,
                )
                # _logger.debug("Attached Layer {} at index {}".format(layer, idx))
            except Exception as e:
                _logger.error("Failed to Link Layer {}: {}".format(layer.id, e))

    def update_layers_other_status(self, layers=[]):
        for l in self.layers.all():
            l.delete()
        self.layers.clear()
        for idx, layer in enumerate(layers):
            LayerLink.objects.create(layer=layer.clone(), user_map=self, sort_index=idx)

    def update_attachments(self, data):
        self.attachment_links.all().delete()
        for idx, attachment in enumerate(data):
            AttachmentLink.objects.create(
                attachment=attachment, user_map=self, sort_index=idx
            )

    # When links are made in a version, and this version is copied in another, we need make new links for this other version
    # self is here the new version, data_id the id of the version from which we'll copy links
    def update_links(self, data_id):
        sources = MapLink.objects.filter(target=data_id)
        targets = MapLink.objects.filter(source=data_id)

        # TODO: check this (deletion before make them again... otherwise we have to check if there has been a deletion) (nw)
        MapLink.objects.filter(target=self.id).delete()
        MapLink.objects.filter(source=self.id).delete()

        try:
            for target_link in targets:
                target_map = UserMap.objects.get(id=target_link.target.id)
                MapLink.objects.get_or_create(source=self, target=target_map)

            for source_link in sources:
                source_map = UserMap.objects.get(id=source_link.source.id)
                MapLink.objects.get_or_create(source=source_map, target=self)

        except UserMap.DoesNotExist:
            _logger.error("map does not exists")

    def update_base_layer(self, data):
        _logger.debug("update_base_layer", data)
        self.base_layer_list.clear()
        for index, layer in enumerate(data):
            BaseLayerLink.objects.create(user_map=self, layer=layer, sort_index=index)

    def update_base_layer_selection(self, data):
        self.base_layer_selection.clear()
        self.base_layer_selection = []
        for layer_or_group_id in data:
            self.base_layer_selection.append(layer_or_group_id)
            # tag = layer_or_group.tag
            # if tag == "layer":
            #     layer = WmsLayer.objects.get(pk=layer_or_group.id)
            #     (newLink, created) = WmsLayerOrGroupLink.objects.get_or_create(
            #         user_map=self,
            #         tag=tag,
            #         layer=layer,
            #     )
            #     self.base_layer_selection.append(newLink.id)

            #     if created == False:
            #         print(f"reused WmsLayerOrGroupLink for {tag} ({layer_or_group.id})")
            # elif tag == "group":
            #     group = WmsLayerGroup.objects.get(pk=layer_or_group.id)
            #     (newLink, created) = WmsLayerOrGroupLink.objects.get_or_create(
            #         user_map=self,
            #         tag=tag,
            #         group=group,
            #     )
            #     self.base_layer_selection.append(newLink.id)
            # else:
            #     print(f"Tag {tag} not found. It has to be 'layer' or 'group'")

    def update_extent(self, data):
        self.extent = data

    def clone(self):
        title = self.title.to_dict()
        for k in title:
            title[k] = "copy({})".format(title[k])

        obj = UserMap.objects.create_map(
            uuid.uuid4(),
            self.user,
            title,
            self.description.to_dict(),
            self.base_layer_selection,
            # self.base_layer,
            self.image_url,
            self.extent,
        )

        # for base_layer in self.base_layer_list.all()

        for idx, lyr in enumerate(self.layers.order_by("user_map_layer__sort_index")):
            LayerLink.objects.create(layer=lyr.clone(), user_map=obj, sort_index=idx)
            # LayerInfoExtra.objects.create(layer_info=lyr, exportable=LAYER_EXPORTABLE)
            # _logger.debug("Attached Layer {} at index {}".format(lyr, idx))

        return obj

    def new_version(self, status):
        obj = UserMap.objects.create(
            id=uuid.uuid4(),
            id_origin=self.id_origin,
            status=status,
            user=self.user,
            title=message("map_title", **self.title.to_dict()),
            description=message("map_description", **self.description.to_dict()),
            image_url=self.image_url,
            extent=self.extent,
        )

        obj.update_base_layer(self.base_layer_list.all())
        obj.update_base_layer_selection(self.base_layer_selection)
        obj.update_links(self.id)
        obj.update_attachments(self.attachments)
        obj.update_categories(self.categories.all())
        obj.add_collaborators(self.collaborators.all())

        for idx, lyr in enumerate(self.layers.order_by("user_map_layer__sort_index")):
            LayerLink.objects.create(layer=lyr.clone(), user_map=obj, sort_index=idx)
            # LayerInfoExtra.objects.create(layer_info=lyr, exportable=LAYER_EXPORTABLE)

            _logger.debug("Attached Layer {} at index {}".format(lyr, idx))

        return obj

    def __str__(self):
        return f"{str(self.title)} ({self.status})"


class AttachmentManager(models.Manager):
    def create_attachment(self, name_data, url_data, user_map):
        url = message("attachment_url", **url_data)
        name = message("attachment_name", **name_data)
        instance = Attachment(name=name, url=url, user_map=user_map)
        instance.save()

        return instance


class Attachment(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = nullable_label_field("attachment_name")
    url = nullable_label_field("attachment_url")
    # Deprecated TODO clean up everything
    user_map = models.ForeignKey(
        UserMap,
        on_delete=models.CASCADE,
        related_name="+",
    )

    objects = AttachmentManager()

    def update(self, data):
        self.name.update_record(**data.pop("name"))
        self.url.update_record(**data.pop("url"))

    def __str__(self):
        return str(self.name)


class AttachmentLink(models.Model):
    class Meta:
        ordering = ["sort_index"]

    id = models.AutoField(primary_key=True)
    attachment = models.ForeignKey(
        Attachment,
        on_delete=models.CASCADE,
        related_name="user_map_attachment",
    )
    user_map = models.ForeignKey(
        UserMap,
        on_delete=models.CASCADE,
        related_name="attachment_links",
    )
    sort_index = models.IntegerField()


class LayerLink(models.Model):
    class Meta:
        ordering = ["sort_index"]

    id = models.AutoField(primary_key=True)
    layer = models.ForeignKey(
        LayerInfo,
        on_delete=models.CASCADE,
        related_name="user_map_layer",
    )
    user_map = models.ForeignKey(
        UserMap,
        on_delete=models.CASCADE,
        related_name="layer_user_map",
    )
    sort_index = models.IntegerField()


class BaseLayerLink(models.Model):
    class Meta:
        ordering = ["sort_index"]

    id = models.AutoField(primary_key=True)
    layer = models.ForeignKey(
        WmsLayer,
        on_delete=models.CASCADE,
        related_name="map_base_layer",
    )
    user_map = models.ForeignKey(
        UserMap,
        on_delete=models.CASCADE,
        related_name="maps",
    )
    sort_index = models.IntegerField(default=1)


class CategoryLink(models.Model):
    id = models.AutoField(primary_key=True)
    category = models.ForeignKey(
        Category,
        on_delete=models.CASCADE,
        related_name="user_map_category",
    )
    user_map = models.ForeignKey(
        UserMap,
        on_delete=models.CASCADE,
        related_name="category_user_map",
    )

    def __str__(self):
        return str(self.category)


class PermissionGroup(models.Model):
    id = models.AutoField(primary_key=True)
    group = models.ForeignKey(
        Group,
        on_delete=models.CASCADE,
        related_name="user_map_permission_group",
    )

    user_map_origin = models.UUIDField()

    def __str__(self):
        return str(self.group.name)
