#
#  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import math
import uuid

from django.urls import reverse
from django.db import transaction
from rest_framework import serializers
from django.conf import settings
from django.contrib.gis.geos import Polygon

from api.models.map import PermissionGroup
from lingua.serializers import serialize_linguarecord
from api.models.map import LAYER_EXPORTABLE, PermissionGroup
from webservice.models import Service, WmsLayer, WmsLayerOrGroupLink
from webservice.serializers import WmsLayerOrGroupListField
from webservice.models import Service, WmsLayer

import logging

_logger = logging.getLogger(__name__)

from ..models import (
    BaseLayer,
    Category,
    LayerGroup,
    LayerInfo,
    MetaData,
    UserMap,
    Attachment,
    HighlightedBaseLayer,
)
from lingua.serializers import MessageRecordSerializer
from collections import OrderedDict


class NonNullModelSerializer(serializers.Serializer):
    def to_representation(self, instance):
        result = super().to_representation(instance)
        return OrderedDict(
            [(key, result[key]) for key in result if result[key] is not None]
        )


def serialize_category(cat: Category):
    return {"id": cat.id, "name": serialize_linguarecord(cat.name)}


def serialize_category_list(cats):
    return [serialize_category(cat) for cat in cats.iterator()]


class CategorySerializer(serializers.ModelSerializer):
    name = MessageRecordSerializer()

    class Meta:
        model = Category
        fields = ("id", "name")


def serialize_attachment(att: Attachment):
    return {
        "id": att.id,
        "name": serialize_linguarecord(att.name),
        "url": serialize_linguarecord(att.url),
        "mapId": att.user_map.id,
    }


def serialize_attachment_list(atts):
    return [serialize_attachment(att) for att in atts]


class AttachmentSerializer(serializers.ModelSerializer):
    id = serializers.UUIDField(read_only=True)
    name = MessageRecordSerializer()
    url = MessageRecordSerializer()
    mapId = serializers.PrimaryKeyRelatedField(
        many=False,
        source="user_map",
        pk_field=serializers.UUIDField(format="hex_verbose"),
        queryset=UserMap.objects,
    )

    class Meta:
        model = Attachment
        fields = ("id", "name", "url", "mapId")

    def update(self, instance, validated_data):
        instance.update(validated_data)
        instance.save()
        return instance

    def create(self, validated_data):
        name_data = validated_data.pop("name")
        url_data = validated_data.pop("url")
        user_map = validated_data.pop("user_map")
        return Attachment.objects.create_attachment(
            name_data,
            url_data,
            user_map,
        )


class BaseLayerSerializer(serializers.ModelSerializer):
    name = MessageRecordSerializer()
    url = MessageRecordSerializer()

    class Meta:
        model = BaseLayer
        fields = ("id", "name", "srs", "params", "url")


class LayerGroupSerializer(serializers.ModelSerializer):
    name = MessageRecordSerializer()

    class Meta:
        model = LayerGroup
        fields = ("id", "name")


class LayerInfoSerializerList(serializers.ListSerializer):
    def to_representation(self, data):
        """
        List of object instances -> List of dicts of primitive datatypes.
        """
        # Dealing with nested relationships, data can be a Manager,
        # so, first get a queryset from the Manager if needed
        # _logger.debug('list {} {}'.format(type(data), dir(self)))
        # iterable = data.all() if isinstance(data, models.Manager) else data
        iterable = data.order_by("user_map_layer__sort_index")

        return [self.child.to_representation(item) for item in iterable]

    # class LayerInfoExtraSerializer(serializers.ModelSerializer):
    #     exportable = serializers.BooleanField(default=LAYER_EXPORTABLE)

    #     class Meta:
    #         model = LayerInfoExtra
    #         fields = ("exportable",)


def serialize_layerinfo(layerinfo: LayerInfo):
    def serialize_layergroup(layergroup: LayerGroup):
        return {"id": layergroup.id, "name": serialize_linguarecord(layergroup.name)}

    def get_layer_group():
        layergroup = None
        if layerinfo.group is not None:
            layergroup = serialize_layergroup(layerinfo.group)
        return layergroup

    return {
        "id": layerinfo.id,
        "metadataId": layerinfo.metadata_id,
        "visible": layerinfo.visible,
        "style": layerinfo.style,
        "featureViewOptions": layerinfo.feature_view_options,
        "group": get_layer_group(),
        "legend": serialize_linguarecord(layerinfo.legend),
        "minZoom": layerinfo.min_zoom,
        "maxZoom": layerinfo.max_zoom,
        "visibleLegend": layerinfo.visible_legend,
        "switchVisibility": layerinfo.visibility_switch,
        "opacitySelector": layerinfo.opacity_selector,
        "exportable": layerinfo.exportable,
        "layerInfoExtra": {"exportable": True},
    }


# class LayerInfoExtraSerializer(serializers.ModelSerializer):
#     exportable = serializers.BooleanField(default=LAYER_EXPORTABLE)

#     class Meta:
#         model = LayerInfoExtra
#         fields = ("exportable",)


# def serialize_layerinfo_list(layerlist):
#     return [
#         serialize_layerinfo(l) for l in layerlist.order_by("user_map_layer__sort_index")
#     ]


class LayerInfoSerializer(serializers.ModelSerializer):
    id = serializers.UUIDField()
    metadataId = serializers.PrimaryKeyRelatedField(
        source="metadata",
        queryset=MetaData.objects,
        pk_field=serializers.UUIDField(format="hex_verbose"),
    )
    visible = serializers.BooleanField()
    style = serializers.JSONField()
    featureViewOptions = serializers.JSONField(source="feature_view_options")
    group = LayerGroupSerializer(allow_null=True, read_only=True)
    legend = MessageRecordSerializer(allow_null=True)
    minZoom = serializers.IntegerField(source="min_zoom")
    maxZoom = serializers.IntegerField(source="max_zoom")
    visibleLegend = serializers.BooleanField(source="visible_legend")
    opacitySelector = serializers.BooleanField(source="opacity_selector")
    switchVisibility = serializers.BooleanField(source="visibility_switch")
    exportable = serializers.BooleanField(default=LAYER_EXPORTABLE)
    # layerInfoExtra = LayerInfoExtraSerializer(
    #     source="layerinfoextra", allow_null=True, required=False
    # )

    def create(self, validated_data):
        # layer_info_extra_data = validated_data.pop('layerinfoextra')

        layer_info = LayerInfo.objects.create(**validated_data)

        # LayerInfoExtra.objects.create(
        #     layer_info=layer_info, exportable=LAYER_EXPORTABLE
        # )

        return layer_info

    class Meta:
        model = LayerInfo
        list_serializer_class = LayerInfoSerializerList
        fields = (
            "id",
            "metadataId",
            "visible",
            "style",
            "featureViewOptions",
            "group",
            "legend",
            "minZoom",
            "maxZoom",
            "visibleLegend",
            "opacitySelector",
            "switchVisibility",
            "exportable",
        )


def encode_wms_layer(layer):
    return f"{layer.service.id}/{layer.name}"


def decode_wms_layer(code):
    service_id, layer_name = code.split("/")
    service = Service.objects.get(id=service_id)
    return WmsLayer.objects.get(service=service, name=layer_name)


class BaseLayerField(serializers.RelatedField):
    def to_representation(self, iterable):
        return [encode_wms_layer(value) for value in iterable.all()]

    def to_internal_value(self, data):
        _logger.debug("BaseLayerField.to_internal_value", type(data), data)
        if isinstance(data, str):
            data = [data]

        return [decode_wms_layer(item) for item in data]


class UserMapSerializer(NonNullModelSerializer):
    id = serializers.UUIDField(read_only=True)
    id_origin = serializers.UUIDField(read_only=True)
    url = serializers.SerializerMethodField(read_only=True)
    lastModified = serializers.SerializerMethodField(read_only=True)
    user = serializers.PrimaryKeyRelatedField(read_only=True)

    status = serializers.ChoiceField(UserMap.STATUS_CHOICES)
    inPublicGroup = serializers.SerializerMethodField(read_only=True)
    title = MessageRecordSerializer()
    description = MessageRecordSerializer()
    baseLayer = BaseLayerField(
        source="base_layer_list", queryset=WmsLayer.objects.all()
    )
    imageUrl = serializers.CharField(
        source="image_url",
        required=False,
        max_length=1024,
    )

    categories = serializers.PrimaryKeyRelatedField(
        required=False,
        many=True,
        pk_field=serializers.UUIDField(format="hex_verbose"),
        queryset=Category.objects,
    )

    attachments = serializers.PrimaryKeyRelatedField(
        required=False,
        many=True,
        pk_field=serializers.UUIDField(format="hex_verbose"),
        queryset=Attachment.objects,
    )

    extent = serializers.SerializerMethodField(read_only=True)

    # attachments = serializers.SerializerMethodField(
    #     read_only=True,
    #     source='attachment_user_map')

    # layers = LayerInfoSerializer(many=True, default=[])
    layers = serializers.PrimaryKeyRelatedField(
        many=True, default=[], queryset=LayerInfo.objects.all()
    )
    baseLayersSelection = WmsLayerOrGroupListField(
        source="base_layer_selection", queryset=WmsLayerOrGroupLink.objects.all()
    )
    # serializers.ListField(
    #     child=serializers.IntegerField(), source="base_layer_selection"
    # )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        try:
            PUBLIC_GROUP = settings.PUBLIC_GROUP
        except AttributeError:
            PUBLIC_GROUP = None

        self.public_maps = [
            pg.user_map_origin
            for pg in PermissionGroup.objects.filter(group__name=PUBLIC_GROUP)
        ]

    def get_lastModified(self, instance):
        d = instance.last_modified
        return math.floor(d.timestamp() * 1000)

    def get_url(self, instance):
        return reverse("usermap-detail", args=[instance.id])

    def get_extent(self, instance):
        if instance.extent:
            return instance.extent.extent

        return None

    def get_inPublicGroup(self, instance):
        return instance.id_origin in self.public_maps

    def get_baseLayer(self, instance: UserMap):
        return [encode_wms_layer(b) for b in instance.base_layer_list.all()]

    # def get_attachments(self, instance):
    # return [formatAttachment(i) for i in instance.attachment_user_map.all()]

    # def get_image_url(sel, instance):
    #     url = instance.image_url
    #     _logger.debug('get_image_url {}'.format(url))
    #     if url:
    #         return url
    #     return None

    def create(self, validated_data):
        request = self.context["request"]
        user = request.user
        title_data = validated_data.pop("title")
        description_data = validated_data.pop("description")
        base_layer_list = validated_data.pop("base_layer_list")
        image_url = validated_data.get("image_url")
        base_layer_selection = validated_data.get("base_layer_selection", [])
        return UserMap.objects.create_map(
            uuid.uuid4(),
            user,
            title_data,
            description_data,
            base_layer_list,
            base_layer_selection,
            image_url,
        )

    def _get_posted_extent(self):
        """Because `extent` is read_only field,
        it doesn't show  up in validated data, so we're going
        to pick it up at the source.
        """
        try:
            minx, miny, maxx, maxy = self.initial_data["extent"]
            return Polygon(
                (
                    (minx, miny),
                    (minx, maxy),
                    (maxx, maxy),
                    (maxx, miny),
                    (minx, miny),
                ),
                (),
            )

        except Exception:
            return None

    def update(self, i, validated_data):
        origin_status = i.status

        status = validated_data.get("status")
        title_data = validated_data.get("title")
        description_data = validated_data.get("description")
        image_url = validated_data.get("image_url")
        categories = validated_data.get("categories", [])
        layers = validated_data.get("layers", [])
        base_layer_list = validated_data.get("base_layer_list")
        attachments = validated_data.get("attachments", [])
        extent = self._get_posted_extent()
        base_layer_selection = validated_data.get("base_layer_selection", [])
        # import pdb

        # pdb.set_trace()

        with transaction.atomic():
            try:
                instance = UserMap.objects.select_for_update().get(
                    id_origin=i.id_origin, status=status
                )
                origin_instance = UserMap.objects.get(
                    id_origin=i.id_origin, status=origin_status
                )

                instance.update_title(title_data)
                instance.update_description(description_data)
                instance.update_image(image_url)
                instance.update_categories(categories)
                instance.update_base_layer(base_layer_list)
                instance.update_base_layer_selection(base_layer_selection)
                instance.update_attachments(attachments)
                instance.update_extent(extent)

                if origin_status != status:
                    instance.update_links(origin_instance.id)
                    instance.update_layers_other_status(
                        list(
                            origin_instance.layers.order_by(
                                "user_map_layer__sort_index"
                            )
                        )
                    )

                else:
                    instance.update_layers(layers)

                instance.save()

                return instance

            except UserMap.DoesNotExist:
                return i.new_version(status)


def serialize_map(usermap: UserMap, public_maps):
    def get_lastModified():
        d = usermap.last_modified
        return math.floor(d.timestamp() * 1000)

    def get_extent():
        if usermap.extent:
            return usermap.extent.extent

        return None

    umap = {
        "id": usermap.id,
        "id_origin": usermap.id_origin,
        "lastModified": get_lastModified(),
        "status": usermap.status,
        "user": usermap.user_id,
        "title": serialize_linguarecord(usermap.title),
        "description": serialize_linguarecord(usermap.description),
        "categories": [cat.id for cat in usermap.categories.all()],
        "baseLayer": [
            encode_wms_layer(layer) for layer in usermap.base_layer_list.all()
        ],
        "attachments": [link.attachment_id for link in usermap.attachment_links.all()],
        # "layers": [serialize_layerinfo(info) for info in usermap.layers.all()],
        "layers": [info.id for info in usermap.layers.all()],
        "url": f"api/maps/{usermap.id}",  # TODO?
        "inPublicGroup": usermap.id_origin in public_maps,
    }

    if get_extent() is not None:
        umap["extent"] = get_extent()

    if usermap.image_url is not None:
        umap["imageUrl"] = usermap.image_url

    return umap


# def serialize_map_list(request):
#     try:
#         PUBLIC_GROUP = settings.PUBLIC_GROUP
#     except AttributeError:
#         PUBLIC_GROUP = None
#     public_groups = PermissionGroup.objects.filter(group__name=PUBLIC_GROUP)
#     public_maps = [pg.user_map.id for pg in public_groups]

#     map_list = [
#         serialize_map(um, public_maps) for um in UserMap.objects.prefetch_related()
#     ]

#     return JsonResponse(map_list, safe=False)


def serialize_highlight_base_layer(hb: HighlightedBaseLayer):
    return {
        "id": hb.id,
        "name": hb.name,
        "codename": hb.baselayer.codename,
        "sortIndex": hb.sort_index,
        "img": hb.img.url,
    }


#


class PermissionGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = PermissionGroup
        fields = "__all__"

    def create(self, validated_data):
        group = validated_data.pop("group")
        user_map = validated_data.pop("user_map")

        return PermissionGroup.objects.create(
            group=group, user_map_origin=user_map.id_origin
        )
