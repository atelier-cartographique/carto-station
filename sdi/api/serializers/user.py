#########################################################################
#  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#########################################################################

from django.contrib.auth.models import User
from lingua.models.record import LANGUAGES


def serialize_user(u: User):
    def get_name(instance):
        full_name = instance.get_full_name()
        if len(full_name) > 0:
            return full_name
        return instance.get_username()

    def get_maps(instance):
        maps_author = map(lambda m: m.id, instance.maps.all())
        maps_collaborators = map(lambda m: m.id, instance.maps_delegated.all())

        maps = []
        for mid in maps_author:
            maps.append(str(mid))
        for mid in maps_collaborators:
            maps.append(str(mid))

        return maps

    def get_roles(instance):
        roles = []
        for g in instance.groups.all():
            label_tr = dict()
            for l in LANGUAGES:
                label_tr[l] = g.name

            roles.append(dict(id=str(g.id), label=label_tr))
        return roles

    return {
        "id": str(u.id),
        "name": get_name(u),
        "maps": get_maps(u),
        "layers": [],
        "roles": get_roles(u),
    }
