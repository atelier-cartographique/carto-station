from django.db.models.signals import post_save
from django.core.cache import cache


def invalidate_metadata_cache(sender, instance, **kwargs):
    from api.views import MetaDataViewSet

    cache.delete(MetaDataViewSet.CACHE_KEY)


def connect():
    print("signals.connect")
    post_save.connect(
        invalidate_metadata_cache,
        sender="api.Metadata",
        dispatch_uid="api.Metadata.invalidate_metadata_cache",
    )
