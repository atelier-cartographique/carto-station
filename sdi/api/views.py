#
#  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from json import loads
from importlib import import_module

from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.http import (
    HttpResponseForbidden,
    HttpResponseNotAllowed,
    JsonResponse,
    Http404,
    HttpResponse,
)
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt

from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.renderers import JSONRenderer
from rest_framework.pagination import PageNumberPagination

from .models import (
    Alias,
    Attachment,
    Category,
    EditedRecord,
    Keyword,
    LayerInfo,
    MapLink,
    MetaData,
    PointOfContact,
    ResponsibleOrganisation,
    Topic,
    UserMap,
    PermissionGroup,
)
from .serializers import (
    AliasSerializer,
    AttachmentSerializer,
    CategorySerializer,
    EditedSerializer,
    KeywordSerializer,
    LayerInfoSerializer,
    MapLinkSerializer,
    MetaDataSerializer,
    PointOfContactSerializer,
    ResponsibleOrgSerializer,
    TopicSerializer,
    UserMapSerializer,
    serialize_map,
    serialize_user,
)

from .permissions import ViewSetWithPermissions, ViewSetWithPermissionsAndFilter
from .rules import SERVICE_VIEW_PERMISSION

from webservice.models import Service, WmsLayerGroup
from webservice.serializers import serialize_layer_groups, serialize_layer_or_group

renderer = JSONRenderer()


def render_json(data):
    return renderer.render(data)


class HTTPAuthenticationFailed(HttpResponse):
    status_code = 401


@csrf_exempt
def login_view(request):
    if request.method == "POST":
        data = loads(request.body.decode("utf-8"))
        username = data.get("username")
        password = data.get("password")
        # next = data.get('next', None)
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            json_data = serialize_user(user)
            return JsonResponse(json_data)
        else:
            return HTTPAuthenticationFailed()

    return HttpResponseNotAllowed(["POST"])


@csrf_exempt
def logout_view(request):
    if request.method == "POST":
        u = request.user
        id = u.id
        logout(request)
        return JsonResponse(
            {
                "logout": "OK",
                "id": id,
            }
        )
    return HttpResponseNotAllowed(["POST"])


def make_wms_config(service, layer):
    data = dict(
        url=reverse("webservice.wms_proxy", args=[service.id]),
        id=layer.id,
        codename=layer.name,
        name=layer.display_name.to_dict(),
        srs=layer.crs,
        params=dict(
            LAYERS=layer.layers.to_dict(),
            STYLES=layer.styles,
            VERSION=service.version,
        ),
        recommended=layer.recommended,
    )
    if service.username is None or len(service.username) == 0:
        data.update(url=service.provider)
        if service.version == "1.1.1":
            data.update(srs=layer.crs)
        else:
            data.update(crs=layer.crs)

    return data


def get_wms_config(request, id, name):
    user = request.user
    service = Service.objects.get(service="wms", id=id)
    if not user.has_perm(SERVICE_VIEW_PERMISSION, service):
        return HttpResponseForbidden()

    layer = service.wms_layers.get(name=name)
    return JsonResponse(make_wms_config(service, layer))


def make_service_dict(service, layers):
    return dict(id=service.id, layers=layers)


def get_wms_layers(request):
    user = request.user
    services = Service.objects.filter(service="wms")
    results = []

    for service in filter(
        lambda s: user.has_perm(SERVICE_VIEW_PERMISSION, s), services
    ):
        layers = []
        for layer in service.wms_layers.all():
            layers.append(make_wms_config(service, layer))

        results.append(make_service_dict(service, layers))

    return JsonResponse(results, safe=False)


def get_map_baselayers_selection(request, mid):
    usermap = UserMap.objects.get(pk=mid)
    baselayers = [serialize_layer_or_group(bl) for bl in usermap.base_layer_selection]

    return JsonResponse(baselayers, safe=False)


# TODO: check permissions?
def get_wms_layer_groups(request):
    groups = WmsLayerGroup.objects.all()
    results = serialize_layer_groups(groups)
    return JsonResponse(results, safe=False)


def is_metadata_admin(request):
    user = request.user
    try:
        admin_md = getattr(settings, "METADATA_ADMIN")
        for grp in user.groups.iterator():
            if grp.name == admin_md:
                return JsonResponse(True, safe=False)
        return JsonResponse(False, safe=False)
    except Exception:
        return JsonResponse(False, safe=False)


def get_user_list(request):
    users = User.objects.all().prefetch_related("maps", "maps_delegated", "groups")

    return JsonResponse([serialize_user(user) for user in users], safe=False)


PUBLIC_GROUP = getattr(settings, "PUBLIC_GROUP", None)


def public_maps():
    return [
        pg.user_map_origin
        for pg in PermissionGroup.objects.filter(group__name=PUBLIC_GROUP)
    ]


class UserMapViewSet(ViewSetWithPermissionsAndFilter):
    """
    API endpoint that allows user maps to be viewed or edited.
    """

    serializer_class = UserMapSerializer

    def list(self, request, *args, **kwargs):

        filtered = self.filter_queryset(
            UserMap.objects.select_related("user")
        ).prefetch_related(
            "categories",
            "layers",
            "layers__metadata",
            "layers__group",
            "attachment_links",
        )

        map_list = [serialize_map(um, public_maps()) for um in filtered]
        return JsonResponse(map_list, safe=False)

    @action(
        detail=False,
        methods=["get"],
        name="Compose",
        description="list maps that a user can edit",
    )
    def compose(self, request, **kwargs):
        user = request.user
        pms = public_maps()
        own_map = [serialize_map(m, pms) for m in UserMap.objects.filter(user=user)]
        delegated = [serialize_map(m, pms) for m in user.maps_delegated.all()]
        return JsonResponse(own_map + delegated, safe=False)

    @action(
        detail=False,
        methods=["get"],
        name="Published",
        description="list of published maps",
    )
    def published(self, request, **kwargs):
        pms = public_maps()
        published = [
            serialize_map(m, pms) for m in UserMap.objects.filter(status="published")
        ]
        return JsonResponse(published, safe=False)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        return JsonResponse(serialize_map(instance, public_maps()), safe=False)

    def get_queryset(self):
        if "pk" in self.kwargs:
            return UserMap.objects.filter(id=self.kwargs["pk"])

        return UserMap.objects.all()


# class MessageViewSet(ViewSetWithPermissions):
#     """
#     API endpoint that allows messages to be viewed or edited.
#     """
#     queryset = MessageRecord.objects.all()
#     serializer_class = MessageRecordSerializer


class CategoryViewSet(ViewSetWithPermissions):
    """
    API endpoint that allows user maps to be viewed or edited.
    """

    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class LayerInfoViewSet(ViewSetWithPermissions):
    """
    API endpoint that allows user maps to be viewed or edited.
    """

    queryset = LayerInfo.objects.all()
    serializer_class = LayerInfoSerializer

    def get_queryset(self):
        mid = self.request.query_params.get("mid", None)
        if mid is not None:
            layer_ids = UserMap.objects.get(id=mid).layers.values_list("id", flat=True)
            return LayerInfo.objects.filter(id__in=layer_ids)
        return LayerInfo.objects.all()


# def serialize_layerinfo_list(request, mid):
#     layer_ids = UserMap.objects.get(pk=mid).layers.values_list("id", flat=True)
#     layerinfo = LayerInfo.objects.filter(
#         id__in=layer_ids
#     )  # queryset of layerinfo of this map
#     return [serialize_layerinfo(li) for li in layerinfo]


class Pagination(PageNumberPagination):
    page_size = getattr(settings, "METADATA_PAGE_SIZE", 256 / 2)


class MetaDataViewSet(ViewSetWithPermissions):
    """
    API endpoint that allows MetaData to be viewed or edited.
    """

    queryset = MetaData.objects.fetch_bulk().order_by("resource_identifier")
    serializer_class = MetaDataSerializer
    # pagination_class = Pagination

    CACHE_KEY = "__METADATA_ALL__"

    def list(self, request, *args, **kwargs):
        from django.core.cache import cache

        if cache.has_key(MetaDataViewSet.CACHE_KEY) is False:
            queryset = self.filter_queryset(self.get_queryset())
            data = self.get_serializer(queryset, many=True).data
            cache.add(MetaDataViewSet.CACHE_KEY, data)
        else:
            data = cache.get(MetaDataViewSet.CACHE_KEY)

        return JsonResponse(data, safe=False)

    def get_alt_object(self, id):
        sources = getattr(settings, "METADATA_SOURCES", [])
        for source in sources:
            mod = import_module(source)
            try:
                return mod.get_object(id)
            except Exception:
                pass

        raise Http404("%s not found in metadata sources." % id)

    def get_object(self):
        """
        Returns the object the view is displaying.

        You may want to override this if you need to provide non-standard
        queryset lookups.  Eg if objects are referenced using multiple
        keyword arguments in the url conf.
        """
        queryset = self.filter_queryset(self.get_queryset())

        # Perform the lookup filtering.
        lookup_url_kwarg = "pk"
        id = self.kwargs[lookup_url_kwarg]
        try:
            obj = get_object_or_404(queryset, pk=id)
        except Http404:
            return self.get_alt_object(id)

        # May raise a permission denied
        self.check_object_permissions(self.request, obj)
        return obj

    @action(
        detail=False,
        methods=["get"],
    )
    def lookup(self, request, **kwargs):
        args = request.GET
        exact = int(args.get("exact", 1)) == 1
        pattern = args.get("q")

        if exact:
            instance = get_object_or_404(MetaData, resource_identifier=pattern)
            data = MetaDataSerializer(
                instance=instance, context=dict(request=request)
            ).data
            return JsonResponse(data)

        qs = MetaData.objects.filter(resource_identifier__icontains=pattern)
        data = MetaDataSerializer(
            instance=qs, many=True, context=dict(request=request)
        ).data
        return JsonResponse(data, safe=False)

    # @action(
    #     detail=False,
    #     methods=['get'],
    # )
    # def in_use(self, request, **kwargs):
    #     args = request.GET
    #     pattern = args.get('q')

    #     try:
    #         instance = get_object_or_404(MetaData, resource_identifier=pattern)
    #     except MetaData.DoesNotExist:
    #         # TODO
    #     data = MetaDataSerializer(instance=instance,
    #                                 context=dict(request=request)).data
    #     return JsonResponse(data)

    #     qs = MetaData.objects.filter(resource_identifier__icontains=pattern)
    #     data = MetaDataSerializer(instance=qs,
    #                               many=True,
    #                               context=dict(request=request)).data
    #     return JsonResponse(data, safe=False)


class TopicViewSet(ViewSetWithPermissions):
    queryset = Topic.objects.all()
    serializer_class = TopicSerializer


class KeywordViewSet(ViewSetWithPermissions):
    queryset = Keyword.objects.all().order_by("thesaurus")
    serializer_class = KeywordSerializer


class AttachmentViewSet(ViewSetWithPermissions):
    queryset = Attachment.objects.all()
    serializer_class = AttachmentSerializer


class AliasViewSet(ViewSetWithPermissions):
    """
    API endpoint that allows alias to be viewed or edited.
    """

    queryset = Alias.objects.all()
    serializer_class = AliasSerializer
    # pagination_class = Pagination


class PointOfContactViewSet(ViewSetWithPermissions):
    """
    API endpoint that allows point of contact to be viewed.
    """

    queryset = PointOfContact.objects.select_related("organisation")
    serializer_class = PointOfContactSerializer


class ResponsibleOrganisationViewSet(ViewSetWithPermissions):
    """
    API endpoint that allows organisation to be viewed.
    """

    queryset = ResponsibleOrganisation.objects
    serializer_class = ResponsibleOrgSerializer


class MapLinkSet(ViewSetWithPermissions):
    # queryset = MapLink.objects
    serializer_class = MapLinkSerializer

    def get_queryset(self):
        """
        List all links to or from an map
        """
        qs = MapLink.objects.all()
        mid = self.request.query_params.get("mid", None)
        if mid is not None:
            return MapLink.objects.filter(Q(source=mid) | Q(target=mid))
        return qs


class EditedViewSet(viewsets.ReadOnlyModelViewSet):
    """
    A simple ViewSet for viewing Edited records.
    """

    queryset = EditedRecord.objects.all()
    serializer_class = EditedSerializer
