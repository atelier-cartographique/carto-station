from django.contrib.admin import ModelAdmin


def name_fr(o):
    return getattr(o.name, "fr", "-")


def name_nl(o):
    return getattr(o.name, "nl", "-")


def url_fr(o):
    return getattr(o.url, "fr", "-")


def url_nl(o):
    return getattr(o.url, "nl", "-")


class AttachmentAdmin(ModelAdmin):
    list_display = (
        "user_map",
        name_fr,
        name_nl,
        url_fr,
        url_nl,
    )
