from django.contrib.admin import TabularInline
from django.contrib.auth.models import User
from ..models.map import CategoryLink, UserMap
from django.contrib.gis.admin.options import OSMGeoAdmin
from django.contrib.gis.forms import OSMWidget


def title_fr(o):
    return getattr(o.title, "fr", "-")


def title_nl(o):
    return getattr(o.title, "nl", "-")


def categories(o):
    return "; ".join([str(c.name) for c in o.categories.all()])


class CategoryInline(TabularInline):
    model = CategoryLink
    extra = 1


class UserMapAdmin(OSMGeoAdmin):

    # Information for OSMWidget
    default_zoom = 12
    default_lon = 485609
    default_lat = 6592740
    map_width = 600
    map_height = 600

    list_display = (title_fr, title_nl, "user", "status", "last_modified", categories)
    list_display_links = (
        title_fr,
        title_nl,
    )
    list_filter = ("status", "user__username")

    search_fields = ["title__fr", "title__nl"]

    inlines = (CategoryInline,)

    def clone_map(self, request, queryset):
        dones = []
        fails = []
        for obj in queryset:
            try:
                obj.clone()
                dones.append(str(obj.title))
            except Exception as ex:
                fails.append("{} because of: {}".format(obj.title, ex))

        self.message_user(request, "Successfully cloned {}.".format(", ".join(dones)))

    clone_map.short_description = "Clone selected map"

    actions = ("clone_map",)

    def save_model(self, request, obj, form, change):
        # When user is changed on a map version, change it on other versions too
        try:
            user_id = form.data["user"]
            user = User.objects.get(pk=user_id)
            for um in UserMap.objects.filter(id_origin=obj.id_origin):
                um.user = user
                um.save()
            super().save_model(request, obj, form, change)

        except Exception:
            super().save_model(request, obj, form, change)
