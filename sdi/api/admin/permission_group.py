from django.contrib.admin import ModelAdmin
from api.models import UserMap, PermissionGroup
from django import forms


def usermap_name(permission: PermissionGroup):
    um = UserMap.objects.get(id_origin=permission.user_map_origin, status="draft")
    return str(um.title)


class SelectMapField(forms.ModelChoiceField):
    def __init__(self, **kwargs):
        # queryset = kwargs.pop('queryset')
        kwargs["queryset"] = UserMap.objects.filter(status=UserMap.PUBLISHED)
        super().__init__(**kwargs)

    def clean(self, value: str):
        return UserMap.objects.get(id=value).id_origin


class PermissionGroupForm(forms.ModelForm):
    user_map_origin = SelectMapField()


class PermissionGroupAdmin(ModelAdmin):

    list_display = ("group", usermap_name, "user_map_origin")
    ordering = ("group_id", "user_map_origin")

    form = PermissionGroupForm
