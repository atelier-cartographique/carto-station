# import jsonfield

from django.db.models import Count
from django.contrib.admin import ModelAdmin, SimpleListFilter
from django.utils.translation import gettext_lazy as _

from api.models.map import UserMap, LayerInfo


# class JSONWidget(jsonfield.widgets.JSONWidget):
#     def render(self, name, value, **kwargs):
#         return super().render(name, value, kwargs.get('attrs'))


class AttachedLayerInfoListFilter(SimpleListFilter):
    title = _("attached to map")
    parameter_name = "attached"

    def lookups(self, request, model_admin):
        return (
            ("attached", _("attached")),
            ("detached", _("detached")),
        )

    def queryset(self, request, queryset):
        annotated = queryset.annotate(Count("usermap"))
        if self.value() == "attached":
            return annotated.filter(usermap__count__gte=1)
        if self.value() == "detached":
            return annotated.filter(usermap__count__exact=0)


class MapStatusLayerListFilter(SimpleListFilter):
    title = _("map status")
    parameter_name = UserMap.PUBLISHED

    def lookups(self, request, model_admin):
        return (
            ("draft", _("draft")),
            ("published", _("published")),
            ("saved", _("saved")),
        )

    def choices(self, cl):
        for lookup, title in self.lookup_choices:
            yield {
                "selected": self.value() == lookup,
                "query_string": cl.get_query_string(
                    {
                        self.parameter_name: lookup,
                    },
                    [],
                ),
                "display": title,
            }

    def queryset(self, request, queryset):
        results = []
        val = self.value()
        if self.value() is None:
            val = "draft"
        for l in queryset:
            if l.usermap_set.filter(status=val).exists():
                results.append(l.id)

        return LayerInfo.objects.filter(pk__in=results)


def title(o):
    return o.metadata.title


def map_(o):
    return o.usermap_set.first()


def user(o):
    m = o.usermap_set.first()
    if m:
        return m.user
    return None


def legend(o):
    return o.legend


class LayerInfoAdmin(ModelAdmin):
    # search_fields = ['usermap__title__fr', 'usermap__title__nl']
    search_fields = ["metadata__title__fr", "metadata__title__nl"]
    list_display = (title, user, map_, "visible", "group", legend)
    list_filter = (AttachedLayerInfoListFilter, MapStatusLayerListFilter)
    # formfield_overrides = {jsonfield.JSONField: {'widget': JSONWidget}}


class LayerGroupAdmin(ModelAdmin):
    search_fields = ["name"]
