# import jsonfield

from django.contrib.admin import ModelAdmin


def name(o):
    return o.name


class HighlightedBaseLayerAdmin(ModelAdmin):
    search_fields = ["name"]
    list_display = (name, "sort_index")
    ordering = ("sort_index",)
