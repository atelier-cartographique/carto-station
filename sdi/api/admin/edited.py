from django.contrib.admin import ModelAdmin


def record(o):
    return o.record


class EditedAdmin(ModelAdmin):
    list_display = (
        "key",
        record,
        "updated_at",
    )
