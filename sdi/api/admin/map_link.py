from django.contrib.admin import ModelAdmin


class MapLinkAdmin(ModelAdmin):
    list_display = ("source", "target")
