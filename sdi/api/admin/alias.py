from django.contrib.admin import ModelAdmin


def replace_fr(o):
    return getattr(o.replace, "fr", "-")


def replace_nl(o):
    return getattr(o.replace, "nl", "-")


class AliasAdmin(ModelAdmin):
    list_display = ("select", replace_fr, replace_nl)
