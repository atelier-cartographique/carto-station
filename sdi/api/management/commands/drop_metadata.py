#
#  Copyright (C) 2022 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from datetime import date, datetime
from django.core.management.base import BaseCommand
from api.models.metadata import MetaData
from api.models.map import LayerInfo

import logging

_logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument("date", help="An ISO format date before which we drop")

    def handle(self, *args, **options):
        d = date.fromisoformat(options["date"])
        dt = datetime.fromisoformat(d.isoformat())
        for md in MetaData.objects.filter(creation__lt=dt):
            resource_identifier = md.resource_identifier
            title = md.title
            if md.layers_count == 0:
                try:
                    md.responsible_organisation.clear()
                    md.point_of_contact.clear()
                    md.delete()
                    self.stdout.write(
                        self.style.SUCCESS(
                            "Deleted {} {}".format(resource_identifier, str(title))
                        )
                    )
                except Exception as ex:
                    _logger.error(str(ex))
                    raise ex
            else:
                self.stderr.write(
                    self.style.ERROR(
                        "Kept ({})  {} {}".format(
                            md.layers_count, resource_identifier, str(title)
                        )
                    )
                )
