#
#  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import uuid

from django.db import models
from django.contrib.auth.models import Group
from django.conf import settings

from lingua.fields import nullable_label_field, label_field

WITH_CREDENTIALS = getattr(settings, "WEBSERVICE_WITH_CREDENTIALS", True)


class ServiceGroup(models.Model):
    id = models.AutoField(primary_key=True)
    name = label_field("service_group")

    def __str__(self) -> str:
        return f"{self.name}"


class Service(models.Model):
    WMS = "wms"
    TMS = "tms"
    SERVICE_CHOICES = (
        (WMS, "Web Map Service"),
        (TMS, "Tile Map Service"),
    )

    id = models.CharField(max_length=126, primary_key=True)
    group = models.ForeignKey(
        ServiceGroup, on_delete=models.SET_NULL, null=True, blank=True
    )
    provider = models.URLField()
    version = models.CharField(max_length=6)
    service = models.CharField(max_length=6, choices=SERVICE_CHOICES)
    if WITH_CREDENTIALS:
        username = models.CharField(max_length=126, blank=True)
        password = models.CharField(max_length=126, blank=True)
    else:
        username = None
        password = None

    order = models.IntegerField(default=0)

    def __str__(self):
        return f"{self.id} <{self.provider}> ({self.service})"

    class Meta:
        ordering = ["order"]


class ServicePermission(models.Model):
    id = models.AutoField(primary_key=True)
    group = models.ForeignKey(
        Group,
        on_delete=models.CASCADE,
        related_name="service_permission_group",
    )

    service = models.ForeignKey(
        Service,
        on_delete=models.CASCADE,
        related_name="service_permission_service",
    )

    def __str__(self):
        return str(self.group.name)


class WmsLayer(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=126)
    display_name = nullable_label_field("wms_layer_name")
    layers = nullable_label_field("wms_layer_layers")
    crs = models.CharField(max_length=64)
    styles = models.CharField(max_length=1024)
    options = nullable_label_field("wms_layer_options")

    service = models.ForeignKey(
        Service,
        on_delete=models.CASCADE,
        related_name="wms_layers",
    )
    recommended = models.BooleanField(default=False)
    order = models.IntegerField(default=0)

    def __str__(self):
        return f"{self.name} [{self.layers}]"

    @property
    def codename(self):
        return f"{self.service.id}/{self.name}"

    class Meta:
        ordering = ["order"]


class WmsLayerGroup(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    display_name = nullable_label_field("wms_layergroup_name")
    recommended = models.BooleanField(default=False)

    def __str__(self) -> str:
        if self.display_name is not None:
            return f"{self.display_name}"


class WmsLayerGroupItem(models.Model):
    class Meta:
        ordering = ["sort_index"]

    id = models.AutoField(primary_key=True)
    layer = models.ForeignKey(
        WmsLayer,
        on_delete=models.CASCADE,
        related_name="group_wms_layer",
    )
    group = models.ForeignKey(
        WmsLayerGroup, on_delete=models.CASCADE, related_name="layers"
    )
    sort_index = models.IntegerField(default=1)

    def __str__(self) -> str:
        return f"{self.layer} - {self.group}"


class WmsLayerOrGroupLink(models.Model):

    WMSLAYER = "layer"
    WMSLAYERGROUP = "group"
    TAG_CHOICES = ((WMSLAYER, "Layer"), (WMSLAYERGROUP, "Group of layers"))

    id = models.AutoField(primary_key=True)
    tag = models.CharField(choices=TAG_CHOICES, max_length=20)

    layer = models.ForeignKey(
        WmsLayer, on_delete=models.CASCADE, null=True, blank=True, related_name="link"
    )
    group = models.ForeignKey(
        WmsLayerGroup,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name="link",
    )
    # sort_index = models.IntegerField(default=1)

    # def __str__(self):
    #     if self.wms_id is not None:
    #         return "{tag} ({self.wms_id})"
    #     elif self.wms_group_id is not None:
    #         return "{tag} ({self.wms_group_id})"
    #     else:
    #         return "no id for {tag}"
