# Generated by Django 3.2.5 on 2024-09-04 10:33

from django.db import migrations, models
import django.db.models.deletion
import lingua.fields
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('webservice', '0015_auto_20210831_1419'),
    ]

    operations = [
        migrations.CreateModel(
            name='ServiceGroup',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', lingua.fields.LinguaField(verbose_name='service_group')),
            ],
        ),
        migrations.CreateModel(
            name='WmsLayerGroup',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('display_name', lingua.fields.LinguaField(blank=True, null=True, verbose_name='wms_layergroup_name')),
            ],
        ),
        migrations.AddField(
            model_name='wmslayer',
            name='options',
            field=lingua.fields.LinguaField(blank=True, null=True, verbose_name='wms_layer_options'),
        ),
        migrations.CreateModel(
            name='WmsLayerOrGroupLink',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('tag', models.CharField(choices=[('layer', 'Layer'), ('group', 'Group of layers')], max_length=20)),
                ('group', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='link', to='webservice.wmslayergroup')),
                ('layer', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='link', to='webservice.wmslayer')),
            ],
        ),
        migrations.CreateModel(
            name='WmsLayerGroupItem',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('sort_index', models.IntegerField(default=1)),
                ('group', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='layers', to='webservice.wmslayergroup')),
                ('layer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='group_wms_layer', to='webservice.wmslayer')),
            ],
            options={
                'ordering': ['sort_index'],
            },
        ),
        migrations.AddField(
            model_name='service',
            name='group',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='webservice.servicegroup'),
        ),
    ]
