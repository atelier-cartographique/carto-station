#
#  Copyright (C) 2017 Atelier Cartographique <contact@atelier-cartographique.be>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from rest_framework import serializers

from lingua.serializers import serialize_linguarecord
from webservice.models import WmsLayer, WmsLayerOrGroupLink

from .models import WmsLayerGroup, WmsLayerOrGroupLink


def serialize_layer_group(group: WmsLayerGroup):
    layers = [w.codename for w in WmsLayer.objects.filter(group_wms_layer__group=group)]
    return dict(
        id=group.id,
        recommended=group.recommended,
        name=serialize_linguarecord(group.display_name),
        layers=layers,
    )


def serialize_layer_groups(groups):
    return [serialize_layer_group(g) for g in groups]


def serialize_layer_or_group(layer_or_group_id):
    try:
        obj = WmsLayerOrGroupLink.objects.get(id=layer_or_group_id)
        if obj.tag == "layer" and obj.layer is not None:
            return dict(
                tag=obj.tag,
                id=obj.layer.id,
            )
        elif obj.tag == "group" and obj.group is not None:
            return dict(tag=obj.tag, id=obj.group.id)
        else:
            print(f"Bad tag given or foreignkey missing for link with id {obj.id}")
            return dict(tag="error", id="bad_tag")
    except WmsLayerOrGroupLink.DoesNotExist:
        print(f"No link found for id : {layer_or_group_id}")
        return dict(tag="error", id="obj does not exist")


def deserizalize_layer_or_group(layer_or_group_dict):
    if layer_or_group_dict["tag"] == "layer":
        layer = WmsLayer.objects.get(pk=layer_or_group_dict["id"])
        (link, _) = WmsLayerOrGroupLink.objects.get_or_create(tag="layer", layer=layer)
    elif layer_or_group_dict["tag"] == "group":
        group = WmsLayerGroup.objects.get(pk=layer_or_group_dict["id"])
        (link, _) = WmsLayerOrGroupLink.objects.get_or_create(tag="group", group=group)
    return link.id


class WmsLayerOrGroupListField(serializers.RelatedField):
    def to_representation(self, array):
        return [serialize_layer_or_group(a) for a in array]

    def to_internal_value(self, array):
        return [deserizalize_layer_or_group(a) for a in array]
