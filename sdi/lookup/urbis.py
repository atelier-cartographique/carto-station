from django.utils.text import slugify


base_url = 'https://geoservices.irisnet.be/localization/Rest/Localize/getaddresses?=&spatialReference=31370&language=&address='


def get_url(url, lang, term):
    formatted_input = slugify(term)
    return f'{url}{formatted_input}'
    # return 'https://geoservices.irisnet.be/localization/Rest/Localize/getaddresses?=&spatialReference=31370&language={}&address={}'.format(lang, formated_input)

def fetch_result(response_body):
    return response_body["result"]

def format_response(response):
    result = []
    for r in response:
            addr = r['address']
            street = addr['street']
            result.append(dict(
                coord=r['point'],
                message="{} {}, {} {}".format(
                    street['name'],
                    addr['number'],
                    street['postCode'],
                    street['municipality']
                ),
            ))
    return result

def get_error(response_body):
    if response_body['error'] is True:
        return 'Urbis error: {}'.format(response_body['status']), []
    return None