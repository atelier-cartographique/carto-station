from django.urls import path
from .views import lookup, lookup_services

urlpatterns = [
    path("service", lookup, name="lookup.lookup"),
    path("services/<app>", lookup_services, name="lookup_services"),
]
