import urllib.parse
from django.contrib.gis.gdal import SpatialReference, CoordTransform
from django.contrib.gis.geos import Point


base_url = 'https://nominatim.openstreetmap.org/search?format=jsonv2&countrycodes=be&addressdetails=1&q='

def get_url(url, lang, term):
    formatted_input = urllib.parse.quote_plus(term)
    return f'{url}{formatted_input}'

def fetch_result(response_body):
    return response_body

def format_response(response):
    from_wsg_to_lambert72 = CoordTransform(SpatialReference(4326), SpatialReference(31370))

    result = []
    for r in response:
        point = Point(float(r['lon']), float(r['lat']), srid=4326)
        point.transform(from_wsg_to_lambert72)

        address = r['address']
        category = r['category']
        road = address.get('road', '')
        house_number = address.get('house_number', '')
        postcode = address.get('postcode', '')
        city = address.get('city', '')
        label = address.get(category, '')

        result.append(dict(
            coord = {'x': point.x, 'y': point.y},

            message="{r}{hn},{pc}{c}{l}".format(
                r = road,
                hn = " " + house_number if house_number else "",
                pc = " " + postcode if postcode else "",
                c = " " + city if city else "",
                l = " (" + label + ")" if label else ""
            )
        ))
    return result

def get_error(response_body):
    if not response_body:
        return 'No result found', []
    return None
