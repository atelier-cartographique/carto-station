import { makeRecord } from "sdi/source";
import { fromRecord } from "sdi/locale";

export const attribution = () => "Perspective.Brussels";

export const credits = () =>
  fromRecord(
    makeRecord(
      "Fond de plan: UrbIS ®© - Paradigm.brussels",
      "Achtergrond: UrbIS ®© - Paradigm.brussels"
    )
  );
