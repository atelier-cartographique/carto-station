import {
    PartialSpec,
    TemplateSpec,
    TemplateCollection,
} from 'mdq/src/components/print/template';

const makeSpec = (s: PartialSpec): TemplateSpec => ({
    fontSize: 10,
    strokeWidth: 1,
    textAlign: 'left',
    color: 'black',
    ...s,
});

export const templates: TemplateCollection = {
    'a4/portrait': {
        resolution: 200,
        title: makeSpec({
            rect: { x: 15, y: 15, width: 170, height: 26 },
            textAlign: 'left',
            fontSize: 18,
            color: '#d53e2a',
        }),

        map: makeSpec({
            rect: { x: 15, y: 35, width: 180, height: 180 },
            color: '#000',
            strokeWidth: 0.05,
        }),

        extraLegend: makeSpec({
            fontSize: 8,
            textAlign: 'right',
            rect: { x: 15, y: 256, width: 180, height: 6 },
        }),

        legend: makeSpec({ rect: { x: 15, y: 220, width: 180, height: 55 } }),

        legendItem: makeSpec({
            rect: { x: 0, y: 0, width: 68, height: 5 },
            fontSize: 8,
        }),

        attribution: makeSpec({
            rect: { x: 16, y: 212, width: 180, height: 10 },
            fontSize: 5.5,
        }),

        north: makeSpec({
            rect: { x: 188, y: 208, width: 6, height: 6 },
            strokeWidth: 0.5,
        }),

        scaleline: makeSpec({
            rect: { x: 146, y: 205, width: 40, height: 12 },
            strokeWidth: 0.5,
            fontSize: 8,
        }),

        sources: makeSpec({
            rect: { x: 15, y: 270, width: 120, height: 17 },
            fontSize: 5.5,
            textAlign: 'left',
        }),

        credits: makeSpec({
            rect: { x: 15, y: 280, width: 80, height: 10 },
            fontSize: 5.5,
            textAlign: 'left',
        }),

        logo: makeSpec({ rect: { x: 145, y: 264, width: 50, height: 18 } }),
    },

    'a4/landscape': {
        resolution: 200,
        title: makeSpec({
            rect: { x: 205, y: 15, width: 75, height: 30 },
            textAlign: 'left',
            fontSize: 18,
            color: '#d53e2a',
        }),

        extraLegend: makeSpec({
            fontSize: 8,
            rect: { x: 205, y: 160, width: 80, height: 6 },
        }),

        legend: makeSpec({ rect: { x: 205, y: 60, width: 80, height: 135 } }),

        legendItem: makeSpec({
            rect: { x: 0, y: 0, width: 80, height: 5 },
            fontSize: 8,
        }),

        map: makeSpec({
            rect: { x: 15, y: 15, width: 180, height: 180 },
            color: '#000',
            strokeWidth: 0.05,
        }),

        attribution: makeSpec({
            rect: { x: 16, y: 192, width: 180, height: 10 },
            fontSize: 5.5,
        }),

        north: makeSpec({
            rect: { x: 187, y: 188, width: 6, height: 6 },
            strokeWidth: 0.5,
        }),

        scaleline: makeSpec({
            rect: { x: 145, y: 185, width: 40, height: 12 },
            strokeWidth: 0.5,
            fontSize: 8,
        }),

        sources: makeSpec({
            rect: { x: 205, y: 185, width: 80, height: 17 },
            fontSize: 5.5,
            textAlign: 'left',
        }),

        credits: makeSpec({
            rect: { x: 205, y: 195, width: 80, height: 10 },
            fontSize: 5.5,
            textAlign: 'left',
        }),
        logo: makeSpec({ rect: { x: 205, y: 165, width: 50, height: 18 } }),
    },

    'a0/portrait': {
        resolution: 100,
    },
    'a0/landscape': {
        resolution: 150,
    },
};

export default templates;
