
This is a specific theme for _cartofixer_, which follows design rules for _smartwater_ project.

**This theme folder as to be at the same level as _cartofixer_ folder.**

## Core style definitions

It uses a collection of core style definitions that are defined in `cartofixer/client/platform/core`

Core style definitions are mainly about interface orgnization and structure.

`core` and `var` definitions are defined in `/style/sdi.less`
