import { makeRecord } from "sdi/source";
import { fromRecord } from "sdi/locale";

export const attribution = () =>
  fromRecord(makeRecord("Relais d'Action de Quartier", "Buurt Actie Relais"));

export const credits = () =>
  fromRecord(
    makeRecord(
      "Fond de plan: UrbIS ®© - Paradigm.brussels",
      "Achtergrond: UrbIS ®© - Paradigm.brussels"
    )
  );
